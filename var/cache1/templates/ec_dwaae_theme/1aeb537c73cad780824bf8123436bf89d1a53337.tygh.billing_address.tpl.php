<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 09:43:40
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/billing_address.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9544324196093820c8948e9-65325899%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1aeb537c73cad780824bf8123436bf89d1a53337' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/billing_address.tpl',
      1 => 1614139272,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '9544324196093820c8948e9-65325899',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'selected_card' => 0,
    'settings' => 0,
    'countries' => 0,
    '_country' => 0,
    'code' => 0,
    'country' => 0,
    'states' => 0,
    '_state' => 0,
    'state' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6093820c8f3605_64614028',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6093820c8f3605_64614028')) {function content_6093820c8f3605_64614028($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('fullname','phone_with_no','address','country','state','city','h_rfq.zipcode','fullname','phone_with_no','address','country','state','city','h_rfq.zipcode'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_checkout_billing_address hidden">
    <?php $_smarty_tpl->tpl_vars['_country'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['b_country'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'] : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['_state'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['b_state'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_state'] : $tmp), null, 0);?>
    <div class="ec_billing_addr_container">
    
        <div class="ty-control-group ec_full_name">
            <label for="b_firstname" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("fullname");?>
:
            </label>
            <input type="text" id="b_firstname"  name="billing_address[b_firstname]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_firstname'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
        <div class="ty-control-group ec_phone">
            <label for="b_phone" class="ty-control-group__title cm-required  cm-mask-phone-label ">
            <?php echo $_smarty_tpl->__("phone_with_no");?>
:
            </label>
            <input type="text" id="b_phone"  name="billing_address[b_phone]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_phone'], ENT_QUOTES, 'UTF-8');?>
" class="cm-mask-phone" />
        </div>


        <div class="ty-control-group ec_address1">
            <label for="b_address" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("address");?>
:
            </label>
            <input type="text" id="b_address"  name="billing_address[b_address]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_address'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>

        <div class="ty-control-group ec_country">
            <label for="b_country" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("country");?>
:
            </label>
            <select id="b_country" class="ty-profile-field__select-country cm-country cm-location-shipping" name="billing_address[b_country]">
                <?php  $_smarty_tpl->tpl_vars["country"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["country"]->_loop = false;
 $_smarty_tpl->tpl_vars["code"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["country"]->key => $_smarty_tpl->tpl_vars["country"]->value) {
$_smarty_tpl->tpl_vars["country"]->_loop = true;
 $_smarty_tpl->tpl_vars["code"]->value = $_smarty_tpl->tpl_vars["country"]->key;
?>
                    <option <?php if ($_smarty_tpl->tpl_vars['_country']->value==$_smarty_tpl->tpl_vars['code']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
        <div class="ty-control-group ec_state">
            <label for="b_state" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("state");?>
:
            </label>
            <select id="b_state" class="ty-profile-field__select-state cm-state cm-location-shipping" name="billing_address[b_state]">
                <?php if ($_smarty_tpl->tpl_vars['states']->value&&$_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]) {?>
                    <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                        <option <?php if ($_smarty_tpl->tpl_vars['_state']->value==$_smarty_tpl->tpl_vars['state']->value['code']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                    <?php } ?>
                <?php }?>
            </select>
            <input  type="text" id="b_state_d" name="billing_address[b_state]" size="32" maxlength="64" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_state']->value, ENT_QUOTES, 'UTF-8');?>
" disabled="disabled" class="cm-state cm-location-shipping ty-input-text hidden"/>
        </div>
        <div class="ty-control-group ec_city">
            <label for="b_city" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("city");?>
:
            </label>
            <input type="text" id="b_city"  name="billing_address[b_city]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_city'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
    
        <div class="ty-control-group ec_zipcode">
            <label for="b_zipcode" class="ty-control-group__title">
            <?php echo $_smarty_tpl->__("h_rfq.zipcode");?>
:
            </label>
            <input type="text" id="b_zipcode"  name="billing_address[b_zipcode]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_zipcode'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
    </div>

    <?php echo '<script'; ?>
 type="text/javascript">
    (function(_, $) {
        $.ceRebuildStates('init', {
            default_country: '<?php echo htmlspecialchars(strtr($_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" )), ENT_QUOTES, 'UTF-8');?>
',
            states: <?php echo json_encode($_smarty_tpl->tpl_vars['states']->value);?>

        });
    }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/views/ec_checkout/components/billing_address.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/views/ec_checkout/components/billing_address.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_checkout_billing_address hidden">
    <?php $_smarty_tpl->tpl_vars['_country'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['b_country'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'] : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['_state'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['b_state'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_state'] : $tmp), null, 0);?>
    <div class="ec_billing_addr_container">
    
        <div class="ty-control-group ec_full_name">
            <label for="b_firstname" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("fullname");?>
:
            </label>
            <input type="text" id="b_firstname"  name="billing_address[b_firstname]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_firstname'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
        <div class="ty-control-group ec_phone">
            <label for="b_phone" class="ty-control-group__title cm-required  cm-mask-phone-label ">
            <?php echo $_smarty_tpl->__("phone_with_no");?>
:
            </label>
            <input type="text" id="b_phone"  name="billing_address[b_phone]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_phone'], ENT_QUOTES, 'UTF-8');?>
" class="cm-mask-phone" />
        </div>


        <div class="ty-control-group ec_address1">
            <label for="b_address" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("address");?>
:
            </label>
            <input type="text" id="b_address"  name="billing_address[b_address]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_address'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>

        <div class="ty-control-group ec_country">
            <label for="b_country" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("country");?>
:
            </label>
            <select id="b_country" class="ty-profile-field__select-country cm-country cm-location-shipping" name="billing_address[b_country]">
                <?php  $_smarty_tpl->tpl_vars["country"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["country"]->_loop = false;
 $_smarty_tpl->tpl_vars["code"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["country"]->key => $_smarty_tpl->tpl_vars["country"]->value) {
$_smarty_tpl->tpl_vars["country"]->_loop = true;
 $_smarty_tpl->tpl_vars["code"]->value = $_smarty_tpl->tpl_vars["country"]->key;
?>
                    <option <?php if ($_smarty_tpl->tpl_vars['_country']->value==$_smarty_tpl->tpl_vars['code']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
        <div class="ty-control-group ec_state">
            <label for="b_state" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("state");?>
:
            </label>
            <select id="b_state" class="ty-profile-field__select-state cm-state cm-location-shipping" name="billing_address[b_state]">
                <?php if ($_smarty_tpl->tpl_vars['states']->value&&$_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]) {?>
                    <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                        <option <?php if ($_smarty_tpl->tpl_vars['_state']->value==$_smarty_tpl->tpl_vars['state']->value['code']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                    <?php } ?>
                <?php }?>
            </select>
            <input  type="text" id="b_state_d" name="billing_address[b_state]" size="32" maxlength="64" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_state']->value, ENT_QUOTES, 'UTF-8');?>
" disabled="disabled" class="cm-state cm-location-shipping ty-input-text hidden"/>
        </div>
        <div class="ty-control-group ec_city">
            <label for="b_city" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("city");?>
:
            </label>
            <input type="text" id="b_city"  name="billing_address[b_city]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_city'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
    
        <div class="ty-control-group ec_zipcode">
            <label for="b_zipcode" class="ty-control-group__title">
            <?php echo $_smarty_tpl->__("h_rfq.zipcode");?>
:
            </label>
            <input type="text" id="b_zipcode"  name="billing_address[b_zipcode]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['b_zipcode'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
    </div>

    <?php echo '<script'; ?>
 type="text/javascript">
    (function(_, $) {
        $.ceRebuildStates('init', {
            default_country: '<?php echo htmlspecialchars(strtr($_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" )), ENT_QUOTES, 'UTF-8');?>
',
            states: <?php echo json_encode($_smarty_tpl->tpl_vars['states']->value);?>

        });
    }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
</div><?php }?><?php }} ?>
