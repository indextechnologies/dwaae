<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:42
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/vendor_locations/blocks/product_filters/components/product_filter_location_zone.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6688108736092b8f6a29bc2-57035563%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b8454e8eff7244715f56b80d34cb2a3e522bdfd9' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/vendor_locations/blocks/product_filters/components/product_filter_location_zone.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '6688108736092b8f6a29bc2-57035563',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'addons' => 0,
    'filter' => 0,
    'location' => 0,
    'collapse' => 0,
    'filter_uid' => 0,
    'range_prefix' => 0,
    'value' => 0,
    'start_value' => 0,
    'range_suffix' => 0,
    'min' => 0,
    'max' => 0,
    'language_direction' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8f6a6f343_88469830',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8f6a6f343_88469830')) {function content_6092b8f6a6f343_88469830($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_modifier_to_json')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.to_json.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('clear','vendor_locations.search_nearby','choose','clear','vendor_locations.search_nearby','choose'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo smarty_function_script(array('src'=>"js/lib/jqueryuitouch/jquery.ui.touch-punch.min.js"),$_smarty_tpl);?>


<?php $_smarty_tpl->tpl_vars['min'] = new Smarty_variable(0, null, 0);?>
<?php $_smarty_tpl->tpl_vars['max'] = new Smarty_variable($_smarty_tpl->tpl_vars['addons']->value['vendor_locations']['max_search_radius'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['start_value'] = new Smarty_variable($_smarty_tpl->tpl_vars['addons']->value['vendor_locations']['start_search_radius'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['range_suffix'] = new Smarty_variable($_smarty_tpl->tpl_vars['addons']->value['vendor_locations']['distance_unit'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['range_prefix'] = new Smarty_variable('', null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['filter']->value['location']) {?>
    <?php $_smarty_tpl->tpl_vars['location'] = new Smarty_variable($_smarty_tpl->tpl_vars['filter']->value['location'], null, 0);?>
    <?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable($_smarty_tpl->tpl_vars['location']->value->getRadius(), null, 0);?>
<?php }?>

<div class="ty-product-filters__block">
    <div class="ty-product-filters <?php if ($_smarty_tpl->tpl_vars['collapse']->value) {?>hidden<?php }?>" id="content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
">
        <div class="ty-product-filters__search ty-filter-products-by-geolocation-filter-address">
            <input type="text"
                   id="elm_filter_geolocation_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                   name="vendor_search_geolocation"
                   class="cm-geocomplete ty-input-text-medium cm-filter-products-by-geolocation-geolocation-input ty-filter-products-by-geolocation-filter-address__input"
                   data-ca-geocomplete-place-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['location_place_id'], ENT_QUOTES, 'UTF-8');?>
"
                   data-ca-geocomplete-type="address"
                   data-ca-geocomplete-value-elem-id="elm_checkbox_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                   <?php if ($_smarty_tpl->tpl_vars['location']->value) {?>data-ca-filter-value="<?php echo htmlspecialchars(smarty_modifier_to_json($_smarty_tpl->tpl_vars['location']->value->toArray()), ENT_QUOTES, 'UTF-8');?>
"<?php }?>
                   data-ca-filter-slider-elem-id="slider_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                   data-ca-filter-type="zone">
            <i class="ty-product-filters__search-icon ty-icon-cancel-circle hidden" id="elm_search_clear_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("clear");?>
"></i>
            <i class="ty-icon-target cm-filter-geolocation-use-my-location-button ty-vendors-locations-use-my-location" data-ca-filter-geocomplete-elem-id="elm_filter_geolocation_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"></i>

            
            <p class="ty-price-slider__inputs ty-filter-products-by-geolocation-filter-address__input-text">
                <bdi class="ty-price-slider__bidi-container">
                    <span class="ty-price-slider__filter-prefix"><?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;?>
</span>
                    <span><?php echo $_smarty_tpl->__("vendor_locations.search_nearby");?>
</span>
                    <span id="slider_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
_right"><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['value']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['start_value']->value : $tmp), ENT_QUOTES, 'UTF-8');?>
</span>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['range_suffix']->value, ENT_QUOTES, 'UTF-8');?>
</span>
                </bdi>
            </p>
            <div id="slider_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                 class="cm-zone-radius-slider ty-range-slider"
                 data-ca-filter-geocomplete-elem-id="elm_filter_geolocation_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-geocomplete-value-elem-id="elm_checkbox_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-slider-disabled="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['disable'], ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-slider-value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['value']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['start_value']->value : $tmp), ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-slider-min="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['min']->value, ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-slider-max="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max']->value, ENT_QUOTES, 'UTF-8');?>
"
            >
                <ul class="ty-range-slider__wrapper">
                    <li class="ty-range-slider__item" style="left: 0%;">
                        <span class="ty-range-slider__num">
                            <?php if ($_smarty_tpl->tpl_vars['language_direction']->value!="rtl") {?>
                            <span>&lrm;<?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;?>
<bdi><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['min']->value, ENT_QUOTES, 'UTF-8');?>
 </span></bdi><?php echo $_smarty_tpl->tpl_vars['range_suffix']->value;?>
</span>
                            <?php } else { ?>
                            <span><bdi><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['min']->value, ENT_QUOTES, 'UTF-8');?>
 </span></bdi>&lrm;<?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;
echo $_smarty_tpl->tpl_vars['range_suffix']->value;?>
</span>
                            <?php }?>
                        </span>
                    </li>
                    <li class="ty-range-slider__item" style="left: 100%;">
                        <span class="ty-range-slider__num">
                            <?php if ($_smarty_tpl->tpl_vars['language_direction']->value!="rtl") {?>
                                <span>&lrm;<?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;?>
<bdi><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max']->value, ENT_QUOTES, 'UTF-8');?>
 </span></bdi><?php echo $_smarty_tpl->tpl_vars['range_suffix']->value;?>
</span>
                            <?php } else { ?>
                                <span><bdi><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max']->value, ENT_QUOTES, 'UTF-8');?>
 </span></bdi>&lrm;<?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;
echo $_smarty_tpl->tpl_vars['range_suffix']->value;?>
</span>
                            <?php }?>
                        </span>
                    </li>
                </ul>
            </div>
        </div>

        
        <input id="elm_checkbox_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
               data-ca-filter-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['filter_id'], ENT_QUOTES, 'UTF-8');?>
"
               data-ca-filter-uid="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
               class="cm-product-filters-checkbox hidden"
               type="checkbox"
               name="product_filters[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['filter_id'], ENT_QUOTES, 'UTF-8');?>
]"
               value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['location_hash'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['filter']->value['location_hash']) {?>checked="checked"<?php }?>>

        <button class="hidden set-distance ty-btn"><?php echo $_smarty_tpl->__("choose");?>
</button>
    </div>
</div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/vendor_locations/blocks/product_filters/components/product_filter_location_zone.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/vendor_locations/blocks/product_filters/components/product_filter_location_zone.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo smarty_function_script(array('src'=>"js/lib/jqueryuitouch/jquery.ui.touch-punch.min.js"),$_smarty_tpl);?>


<?php $_smarty_tpl->tpl_vars['min'] = new Smarty_variable(0, null, 0);?>
<?php $_smarty_tpl->tpl_vars['max'] = new Smarty_variable($_smarty_tpl->tpl_vars['addons']->value['vendor_locations']['max_search_radius'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['start_value'] = new Smarty_variable($_smarty_tpl->tpl_vars['addons']->value['vendor_locations']['start_search_radius'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['range_suffix'] = new Smarty_variable($_smarty_tpl->tpl_vars['addons']->value['vendor_locations']['distance_unit'], null, 0);?>
<?php $_smarty_tpl->tpl_vars['range_prefix'] = new Smarty_variable('', null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['filter']->value['location']) {?>
    <?php $_smarty_tpl->tpl_vars['location'] = new Smarty_variable($_smarty_tpl->tpl_vars['filter']->value['location'], null, 0);?>
    <?php $_smarty_tpl->tpl_vars['value'] = new Smarty_variable($_smarty_tpl->tpl_vars['location']->value->getRadius(), null, 0);?>
<?php }?>

<div class="ty-product-filters__block">
    <div class="ty-product-filters <?php if ($_smarty_tpl->tpl_vars['collapse']->value) {?>hidden<?php }?>" id="content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
">
        <div class="ty-product-filters__search ty-filter-products-by-geolocation-filter-address">
            <input type="text"
                   id="elm_filter_geolocation_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                   name="vendor_search_geolocation"
                   class="cm-geocomplete ty-input-text-medium cm-filter-products-by-geolocation-geolocation-input ty-filter-products-by-geolocation-filter-address__input"
                   data-ca-geocomplete-place-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['location_place_id'], ENT_QUOTES, 'UTF-8');?>
"
                   data-ca-geocomplete-type="address"
                   data-ca-geocomplete-value-elem-id="elm_checkbox_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                   <?php if ($_smarty_tpl->tpl_vars['location']->value) {?>data-ca-filter-value="<?php echo htmlspecialchars(smarty_modifier_to_json($_smarty_tpl->tpl_vars['location']->value->toArray()), ENT_QUOTES, 'UTF-8');?>
"<?php }?>
                   data-ca-filter-slider-elem-id="slider_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                   data-ca-filter-type="zone">
            <i class="ty-product-filters__search-icon ty-icon-cancel-circle hidden" id="elm_search_clear_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("clear");?>
"></i>
            <i class="ty-icon-target cm-filter-geolocation-use-my-location-button ty-vendors-locations-use-my-location" data-ca-filter-geocomplete-elem-id="elm_filter_geolocation_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"></i>

            
            <p class="ty-price-slider__inputs ty-filter-products-by-geolocation-filter-address__input-text">
                <bdi class="ty-price-slider__bidi-container">
                    <span class="ty-price-slider__filter-prefix"><?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;?>
</span>
                    <span><?php echo $_smarty_tpl->__("vendor_locations.search_nearby");?>
</span>
                    <span id="slider_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
_right"><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['value']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['start_value']->value : $tmp), ENT_QUOTES, 'UTF-8');?>
</span>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['range_suffix']->value, ENT_QUOTES, 'UTF-8');?>
</span>
                </bdi>
            </p>
            <div id="slider_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                 class="cm-zone-radius-slider ty-range-slider"
                 data-ca-filter-geocomplete-elem-id="elm_filter_geolocation_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-geocomplete-value-elem-id="elm_checkbox_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-slider-disabled="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['disable'], ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-slider-value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['value']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['start_value']->value : $tmp), ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-slider-min="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['min']->value, ENT_QUOTES, 'UTF-8');?>
"
                 data-ca-slider-max="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max']->value, ENT_QUOTES, 'UTF-8');?>
"
            >
                <ul class="ty-range-slider__wrapper">
                    <li class="ty-range-slider__item" style="left: 0%;">
                        <span class="ty-range-slider__num">
                            <?php if ($_smarty_tpl->tpl_vars['language_direction']->value!="rtl") {?>
                            <span>&lrm;<?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;?>
<bdi><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['min']->value, ENT_QUOTES, 'UTF-8');?>
 </span></bdi><?php echo $_smarty_tpl->tpl_vars['range_suffix']->value;?>
</span>
                            <?php } else { ?>
                            <span><bdi><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['min']->value, ENT_QUOTES, 'UTF-8');?>
 </span></bdi>&lrm;<?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;
echo $_smarty_tpl->tpl_vars['range_suffix']->value;?>
</span>
                            <?php }?>
                        </span>
                    </li>
                    <li class="ty-range-slider__item" style="left: 100%;">
                        <span class="ty-range-slider__num">
                            <?php if ($_smarty_tpl->tpl_vars['language_direction']->value!="rtl") {?>
                                <span>&lrm;<?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;?>
<bdi><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max']->value, ENT_QUOTES, 'UTF-8');?>
 </span></bdi><?php echo $_smarty_tpl->tpl_vars['range_suffix']->value;?>
</span>
                            <?php } else { ?>
                                <span><bdi><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max']->value, ENT_QUOTES, 'UTF-8');?>
 </span></bdi>&lrm;<?php echo $_smarty_tpl->tpl_vars['range_prefix']->value;
echo $_smarty_tpl->tpl_vars['range_suffix']->value;?>
</span>
                            <?php }?>
                        </span>
                    </li>
                </ul>
            </div>
        </div>

        
        <input id="elm_checkbox_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
               data-ca-filter-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['filter_id'], ENT_QUOTES, 'UTF-8');?>
"
               data-ca-filter-uid="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
"
               class="cm-product-filters-checkbox hidden"
               type="checkbox"
               name="product_filters[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['filter_id'], ENT_QUOTES, 'UTF-8');?>
]"
               value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter']->value['location_hash'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['filter']->value['location_hash']) {?>checked="checked"<?php }?>>

        <button class="hidden set-distance ty-btn"><?php echo $_smarty_tpl->__("choose");?>
</button>
    </div>
</div>
<?php }?><?php }} ?>
