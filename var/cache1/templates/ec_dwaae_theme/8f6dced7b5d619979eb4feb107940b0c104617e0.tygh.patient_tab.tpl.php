<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 22:38:51
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/components/patient_tab.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5439130106092e63b7ef626-30992673%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8f6dced7b5d619979eb4feb107940b0c104617e0' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/components/patient_tab.tpl',
      1 => 1619161414,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '5439130106092e63b7ef626-30992673',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'emirate_cards' => 0,
    'prescription_details' => 0,
    'emirate_card' => 0,
    'card_select' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092e63b814533_93365152',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092e63b814533_93365152')) {function content_6092e63b814533_93365152($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_upload_prescription','customer_note','emirates_id','add_new_id','ec_upload_prescription','customer_note','emirates_id','add_new_id'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_patient_tab_content" id="ec_patient_tab_content">
    <div class="ec_upload_form">
        <div class="ec_top_div">
            <div class="ty-control-group ec_upload_rescription_btn">
                <div id="ec_prescription__required">
                <label for="id_prescribed_attachment" class="ty-control-group__title cm-required">
                    <?php echo $_smarty_tpl->__("ec_upload_prescription");?>

                </label>
                <!--ec_prescription__required--></div>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"prescriptions[]",'multiupload'=>false,'label_id'=>"id_prescribed_attachment",'upload_file_text'=>$_smarty_tpl->__('ec_upload_prescription'),'upload_another_file_text'=>$_smarty_tpl->__('ec_upload_prescription')), 0);?>

            </div>

        
            <div class="upload_input_div">
                
            </div>
        </div>

        <?php if (!@constant('EC_MOBILE_APP')||(@constant('EC_MOBILE_APP')&&@constant('EC_APP_VERSION')==2)) {?>
        <div class="ty-control-group" >
            <label for="id_customer_note" class="ty-control-group__title">
            <?php echo $_smarty_tpl->__("customer_note");?>

            </label>
            <textarea name="prescription_details[customer_note]" style="width: 100%;height: 100px;"></textarea>
        </div>
        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/authorised_carer_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>




        <div id="ec_emirates_upload_block">
            <div class="ec_header"><?php echo $_smarty_tpl->__("emirates_id");?>
</div>
            <div class="ec_upload_form_blocks">
                <?php  $_smarty_tpl->tpl_vars['emirate_card'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['emirate_card']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['emirate_cards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["emirate_card"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['emirate_card']->key => $_smarty_tpl->tpl_vars['emirate_card']->value) {
$_smarty_tpl->tpl_vars['emirate_card']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["emirate_card"]['iteration']++;
?>
                    <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(false, null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['prescription_details']->value['selected_emirate']==$_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id']||$_smarty_tpl->getVariable('smarty')->value['foreach']['emirate_card']['iteration']==1) {?>
                        <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(true, null, 0);?>
                    <?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('emirate_card_selected'=>$_smarty_tpl->tpl_vars['card_select']->value,'emirate_edit_card'=>"d_prescription.edit_emirate",'emirate_render_ids'=>"ec_emirates_upload_block,ec_prescription__required",'emirate_redirect_url'=>"d_prescription.upload_form",'enable_card_select'=>true), 0);?>

                <?php } ?>
            </div>
            <a href="<?php echo htmlspecialchars(fn_url("d_prescription.edit_emirate"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render ec_add_card_btn" data-ca-target-id="ec_emirates_upload_block,ec_prescription__required,ec_next_div"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_id");?>
</a>
        <!--ec_emirates_upload_block--></div>
    </div>
<!--ec_patient_tab_content--></div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_prescription/views/d_prescription/components/patient_tab.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_prescription/views/d_prescription/components/patient_tab.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_patient_tab_content" id="ec_patient_tab_content">
    <div class="ec_upload_form">
        <div class="ec_top_div">
            <div class="ty-control-group ec_upload_rescription_btn">
                <div id="ec_prescription__required">
                <label for="id_prescribed_attachment" class="ty-control-group__title cm-required">
                    <?php echo $_smarty_tpl->__("ec_upload_prescription");?>

                </label>
                <!--ec_prescription__required--></div>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"prescriptions[]",'multiupload'=>false,'label_id'=>"id_prescribed_attachment",'upload_file_text'=>$_smarty_tpl->__('ec_upload_prescription'),'upload_another_file_text'=>$_smarty_tpl->__('ec_upload_prescription')), 0);?>

            </div>

        
            <div class="upload_input_div">
                
            </div>
        </div>

        <?php if (!@constant('EC_MOBILE_APP')||(@constant('EC_MOBILE_APP')&&@constant('EC_APP_VERSION')==2)) {?>
        <div class="ty-control-group" >
            <label for="id_customer_note" class="ty-control-group__title">
            <?php echo $_smarty_tpl->__("customer_note");?>

            </label>
            <textarea name="prescription_details[customer_note]" style="width: 100%;height: 100px;"></textarea>
        </div>
        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/authorised_carer_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>




        <div id="ec_emirates_upload_block">
            <div class="ec_header"><?php echo $_smarty_tpl->__("emirates_id");?>
</div>
            <div class="ec_upload_form_blocks">
                <?php  $_smarty_tpl->tpl_vars['emirate_card'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['emirate_card']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['emirate_cards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["emirate_card"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['emirate_card']->key => $_smarty_tpl->tpl_vars['emirate_card']->value) {
$_smarty_tpl->tpl_vars['emirate_card']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["emirate_card"]['iteration']++;
?>
                    <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(false, null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['prescription_details']->value['selected_emirate']==$_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id']||$_smarty_tpl->getVariable('smarty')->value['foreach']['emirate_card']['iteration']==1) {?>
                        <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(true, null, 0);?>
                    <?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('emirate_card_selected'=>$_smarty_tpl->tpl_vars['card_select']->value,'emirate_edit_card'=>"d_prescription.edit_emirate",'emirate_render_ids'=>"ec_emirates_upload_block,ec_prescription__required",'emirate_redirect_url'=>"d_prescription.upload_form",'enable_card_select'=>true), 0);?>

                <?php } ?>
            </div>
            <a href="<?php echo htmlspecialchars(fn_url("d_prescription.edit_emirate"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render ec_add_card_btn" data-ca-target-id="ec_emirates_upload_block,ec_prescription__required,ec_next_div"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_id");?>
</a>
        <!--ec_emirates_upload_block--></div>
    </div>
<!--ec_patient_tab_content--></div><?php }?><?php }} ?>
