<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 10:18:43
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/emirate_cards/components/card.tpl" */ ?>
<?php /*%%SmartyHeaderCode:145339104560962d43b4f120-63007733%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa5781a81875389befe22c11e881abf2c9951b44' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/emirate_cards/components/card.tpl',
      1 => 1607518879,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '145339104560962d43b4f120-63007733',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'emirate_card_selected' => 0,
    'emirate_edit_card' => 0,
    'emirate_render_ids' => 0,
    'emirate_redirect_url' => 0,
    'enable_card_select' => 0,
    'card_view_only' => 0,
    'emirate_card' => 0,
    'render_ids' => 0,
    'redirect_url' => 0,
    'curl' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60962d43b936a8_31444953',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60962d43b936a8_31444953')) {function content_60962d43b936a8_31444953($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('edit','front_side','back_side','edit','front_side','back_side'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars['emirate_card_selected'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_card_selected']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['emirate_edit_card'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_edit_card']->value)===null||$tmp==='' ? "emirate_cards.update" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['render_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_render_ids']->value)===null||$tmp==='' ? "ec_emirates_upload_block,ec_prescription__required" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_redirect_url']->value)===null||$tmp==='' ? "emirate_cards.manage" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['enable_card_select'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['enable_card_select']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['card_view_only'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['card_view_only']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<div class="ec_emirates_id_card">
    <label class="labl">
        <?php if ($_smarty_tpl->tpl_vars['enable_card_select']->value) {?>
        <input type="radio" name="selected_emirate" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['emirate_card_selected']->value) {?>checked="checked"<?php }?>/>
        <?php }?>
        <div>
            <div class="ec_header_row">
                <div class="ec_left_content">
                    <i class="ec-icon-union"></i>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_name'], ENT_QUOTES, 'UTF-8');?>
</span>
                </div>
                <?php if (!$_smarty_tpl->tpl_vars['card_view_only']->value) {?>
                <div class="ec_right_content">
                    <div class="">
                        <a href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['emirate_edit_card']->value)."&emirate_profile_id=".((string)$_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id'])), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax cm-ajax-full-render edit_address stop-cm-required" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_ids']->value, ENT_QUOTES, 'UTF-8');?>
">
                            <span><?php echo $_smarty_tpl->__("edit");?>
</span>
                            <i class="ec-icon-edit"></i>
                        </a>
                    </div>
                    
                    <div>
                        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['redirect_url']->value), null, 0);?>
                        <a href="<?php echo htmlspecialchars(fn_url("emirate_cards.delete&emirate_profile_id=".((string)$_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id'])."?redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value)), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_ids']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-post cm-ajax cm-ajax-full-render ec_delete_div">
                            <i class="ec-icon-delete"></i>
                        </a>
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="ec_card_content">
                <div class="d-upload-form-block">
                    <div>
                        <div class="ty-control-group">
                            <?php if ($_smarty_tpl->tpl_vars['emirate_card']->value['front_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['emirate_card']->value['front_side_path'],true), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>
                            <div><?php echo $_smarty_tpl->__("front_side");?>
</div>
                        </div>
                    </div>
                    <div>
                        <div class="ty-control-group">
                            <?php if ($_smarty_tpl->tpl_vars['emirate_card']->value['back_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['emirate_card']->value['back_side_path'],true), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>
                            <div><?php echo $_smarty_tpl->__("back_side");?>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </label>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_customer/views/emirate_cards/components/card.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_customer/views/emirate_cards/components/card.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars['emirate_card_selected'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_card_selected']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['emirate_edit_card'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_edit_card']->value)===null||$tmp==='' ? "emirate_cards.update" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['render_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_render_ids']->value)===null||$tmp==='' ? "ec_emirates_upload_block,ec_prescription__required" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_redirect_url']->value)===null||$tmp==='' ? "emirate_cards.manage" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['enable_card_select'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['enable_card_select']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['card_view_only'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['card_view_only']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<div class="ec_emirates_id_card">
    <label class="labl">
        <?php if ($_smarty_tpl->tpl_vars['enable_card_select']->value) {?>
        <input type="radio" name="selected_emirate" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['emirate_card_selected']->value) {?>checked="checked"<?php }?>/>
        <?php }?>
        <div>
            <div class="ec_header_row">
                <div class="ec_left_content">
                    <i class="ec-icon-union"></i>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_name'], ENT_QUOTES, 'UTF-8');?>
</span>
                </div>
                <?php if (!$_smarty_tpl->tpl_vars['card_view_only']->value) {?>
                <div class="ec_right_content">
                    <div class="">
                        <a href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['emirate_edit_card']->value)."&emirate_profile_id=".((string)$_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id'])), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax cm-ajax-full-render edit_address stop-cm-required" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_ids']->value, ENT_QUOTES, 'UTF-8');?>
">
                            <span><?php echo $_smarty_tpl->__("edit");?>
</span>
                            <i class="ec-icon-edit"></i>
                        </a>
                    </div>
                    
                    <div>
                        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['redirect_url']->value), null, 0);?>
                        <a href="<?php echo htmlspecialchars(fn_url("emirate_cards.delete&emirate_profile_id=".((string)$_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id'])."?redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value)), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_ids']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-post cm-ajax cm-ajax-full-render ec_delete_div">
                            <i class="ec-icon-delete"></i>
                        </a>
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="ec_card_content">
                <div class="d-upload-form-block">
                    <div>
                        <div class="ty-control-group">
                            <?php if ($_smarty_tpl->tpl_vars['emirate_card']->value['front_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['emirate_card']->value['front_side_path'],true), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>
                            <div><?php echo $_smarty_tpl->__("front_side");?>
</div>
                        </div>
                    </div>
                    <div>
                        <div class="ty-control-group">
                            <?php if ($_smarty_tpl->tpl_vars['emirate_card']->value['back_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['emirate_card']->value['back_side_path'],true), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>
                            <div><?php echo $_smarty_tpl->__("back_side");?>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </label>
</div><?php }?><?php }} ?>
