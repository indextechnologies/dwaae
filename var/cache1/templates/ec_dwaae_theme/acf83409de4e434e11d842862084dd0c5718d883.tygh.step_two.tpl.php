<?php /* Smarty version Smarty-3.1.21, created on 2021-06-15 14:13:52
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/steps/step_two.tpl" */ ?>
<?php /*%%SmartyHeaderCode:97554839960c87d608c52a1-20292431%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'acf83409de4e434e11d842862084dd0c5718d883' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/steps/step_two.tpl',
      1 => 1603098543,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '97554839960c87d608c52a1-20292431',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'redirect_url' => 0,
    'tabs_section' => 0,
    'uid' => 0,
    'insurance_type' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60c87d6092f809_42628915',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60c87d6092f809_42628915')) {function content_60c87d6092f809_42628915($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_patient_info_prescription','ec_i_am_patient','ec_authorised_carer','ec_patient_insurance_cards','do_not_use_insurance','close_dont_use_insurance_to_use_the_insurance_in_order','ec_insurance_linked_to_emirates','ec_insurance_linked_to_emirates_desc','ec_patient_info_prescription','ec_i_am_patient','ec_authorised_carer','ec_patient_insurance_cards','do_not_use_insurance','close_dont_use_insurance_to_use_the_insurance_in_order','ec_insurance_linked_to_emirates','ec_insurance_linked_to_emirates_desc'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_step_container" id="step_two">
<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=checkout.checkout&ec_edit_step=ec_step_three", null, 0);?>

<form  name="form_upload_prescription" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">

    <input type="hidden" name="result_ids" value="checkout_steps" />
    <input type="hidden" name="ec_edit_step" value="ec_step_three" />
    <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />

    <div class="ec_prescription_top_div">
        <div class="ec_header"><i class="ec-icon-id"></i><span><?php echo $_smarty_tpl->__("ec_patient_info_prescription");?>
</span></div>
        <div class="ec_content">
            <?php echo smarty_function_script(array('src'=>"js/tygh/tabs.js"),$_smarty_tpl);?>

            <div class="ty-tabs cm-j-tabs cm-j-tabs-disable-convertation clearfix">
                <ul class="ty-tabs__list" <?php if ($_smarty_tpl->tpl_vars['tabs_section']->value) {?>id="tabs_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tabs_section']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
                    <li id="ec_tab_patient" class="ty-tabs__item cm-js"><a class="ty-tabs__a"><i class="ec-icon-union"></i><?php echo $_smarty_tpl->__("ec_i_am_patient");?>
</a></li>
                    <li id="ec_tab_authorised_carer" class="ty-tabs__item cm-js"><a class="ty-tabs__a"><i class="ec-icon-union"></i><?php echo $_smarty_tpl->__("ec_authorised_carer");?>
</a></li>
                </ul>
            </div>
            <div class="cm-tabs-content ty-tabs__content clearfix" id="tabs_content">
                <div id="content_ec_tab_patient11" class="">
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/patient_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                </div>
            </div>
        </div>
    </div>
    <?php $_smarty_tpl->tpl_vars['uid'] = new Smarty_variable(uniqid(), null, 0);?>
    <div class="ec_prescription_bottom_div">
        <div class="ec_header">
            <div class="ec_left">
                <i class="ec-icon-id"></i><span><?php echo $_smarty_tpl->__("ec_patient_insurance_cards");?>
</span>
            </div>
            <div class="ec_right">
                <span><?php echo $_smarty_tpl->__("do_not_use_insurance");?>
</span>
                <label class="switch">
                    <input type="hidden" name="prescription_details[dont_use_insurance]" value="N">
                    <input type="checkbox" id="dont_use_insurance_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
" class="do_not_use_insurance" name="prescription_details[dont_use_insurance]" value="Y">
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
        <div class="ec_content">
            <div class="ec_top_check dont_use_insurance_text ec-imp-hidden">
                <span><?php echo $_smarty_tpl->__("close_dont_use_insurance_to_use_the_insurance_in_order");?>
</span>
            </div>
            <div id="emirate_is_insurance_block">
                <div class="ec_top_check">
                    <input type="hidden" name="prescription_details[linked_to_emirates]" value="N"> 
                    <input type="checkbox" id="linked_to_emirates_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ec_insurance_linked_to_emirates" name="prescription_details[linked_to_emirates]" value="Y"> 
                    <span><?php echo $_smarty_tpl->__("ec_insurance_linked_to_emirates");?>
</span>
                </div>
                <div class="ec_checked_info_text hidden"><?php echo $_smarty_tpl->__("ec_insurance_linked_to_emirates_desc");?>
</div>
            </div> 
            <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/insurance_cards.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            
        </div>
    </div>
    <?php echo '<script'; ?>
>
        (function(_, $) {
            $.ceEvent('on', 'ce.commoninit', function(context){
                $(document).on('click', '.ec_prescription_top_div .ty-tabs__item', function(){
                    if($(this).attr("id") == 'ec_tab_authorised_carer'){
                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').show();
                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').find('.ty-control-group__title').each(function(){
                            $(this).addClass('cm-required');
                        });
                    } else {
                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').hide();
                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').find('.ty-control-group__title').each(function(){
                            $(this).removeClass('cm-required');
                        });
                    }
                });
                $(".do_not_use_insurance").change(function() {
                    if(this.checked) {
                        $('#ec_insurance_upload_block .ec_upload_form_blocks').hide();
                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'hidden');
                        $('.dont_use_insurance_text').removeClass('ec-imp-hidden');
                        $('.ec_insurance_linked_to_emirates').prop('checked', false);
                        $('#emirate_is_insurance_block').hide();
                    } else {
                        $('#ec_insurance_upload_block .ec_upload_form_blocks').show();
                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'visible');
                        $('.dont_use_insurance_text').addClass('ec-imp-hidden');
                        $('#emirate_is_insurance_block').show();
                    }
                });
                $(".ec_insurance_linked_to_emirates").change(function() {
                    if(this.checked) {
                        $('.ec_checked_info_text').show();
                        $('#ec_insurance_upload_block .ec_upload_form_blocks').hide();
                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'hidden');      
                    } else {
                        $('.ec_checked_info_text').hide();
                        $('#ec_insurance_upload_block .ec_upload_form_blocks').show();
                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'visible');      
                    }
                });
                <?php if ($_smarty_tpl->tpl_vars['insurance_type']->value) {?>
                    <?php if ($_smarty_tpl->tpl_vars['insurance_type']->value=="E") {?>
                        $("#linked_to_emirates_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
").click();
                    <?php } elseif ($_smarty_tpl->tpl_vars['insurance_type']->value=="N") {?>
                        $("#dont_use_insurance_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
").click();
                    <?php } elseif ($_smarty_tpl->tpl_vars['insurance_type']->value=="S") {?>

                    <?php }?>
                <?php }?>
            });
            $(document).ready(function(){
                $('#dont_use_insurance_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
').click();
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
</form>
<!--step_two--></div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/views/ec_checkout/components/steps/step_two.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/views/ec_checkout/components/steps/step_two.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_step_container" id="step_two">
<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=checkout.checkout&ec_edit_step=ec_step_three", null, 0);?>

<form  name="form_upload_prescription" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">

    <input type="hidden" name="result_ids" value="checkout_steps" />
    <input type="hidden" name="ec_edit_step" value="ec_step_three" />
    <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />

    <div class="ec_prescription_top_div">
        <div class="ec_header"><i class="ec-icon-id"></i><span><?php echo $_smarty_tpl->__("ec_patient_info_prescription");?>
</span></div>
        <div class="ec_content">
            <?php echo smarty_function_script(array('src'=>"js/tygh/tabs.js"),$_smarty_tpl);?>

            <div class="ty-tabs cm-j-tabs cm-j-tabs-disable-convertation clearfix">
                <ul class="ty-tabs__list" <?php if ($_smarty_tpl->tpl_vars['tabs_section']->value) {?>id="tabs_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tabs_section']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>>
                    <li id="ec_tab_patient" class="ty-tabs__item cm-js"><a class="ty-tabs__a"><i class="ec-icon-union"></i><?php echo $_smarty_tpl->__("ec_i_am_patient");?>
</a></li>
                    <li id="ec_tab_authorised_carer" class="ty-tabs__item cm-js"><a class="ty-tabs__a"><i class="ec-icon-union"></i><?php echo $_smarty_tpl->__("ec_authorised_carer");?>
</a></li>
                </ul>
            </div>
            <div class="cm-tabs-content ty-tabs__content clearfix" id="tabs_content">
                <div id="content_ec_tab_patient11" class="">
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/patient_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                </div>
            </div>
        </div>
    </div>
    <?php $_smarty_tpl->tpl_vars['uid'] = new Smarty_variable(uniqid(), null, 0);?>
    <div class="ec_prescription_bottom_div">
        <div class="ec_header">
            <div class="ec_left">
                <i class="ec-icon-id"></i><span><?php echo $_smarty_tpl->__("ec_patient_insurance_cards");?>
</span>
            </div>
            <div class="ec_right">
                <span><?php echo $_smarty_tpl->__("do_not_use_insurance");?>
</span>
                <label class="switch">
                    <input type="hidden" name="prescription_details[dont_use_insurance]" value="N">
                    <input type="checkbox" id="dont_use_insurance_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
" class="do_not_use_insurance" name="prescription_details[dont_use_insurance]" value="Y">
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
        <div class="ec_content">
            <div class="ec_top_check dont_use_insurance_text ec-imp-hidden">
                <span><?php echo $_smarty_tpl->__("close_dont_use_insurance_to_use_the_insurance_in_order");?>
</span>
            </div>
            <div id="emirate_is_insurance_block">
                <div class="ec_top_check">
                    <input type="hidden" name="prescription_details[linked_to_emirates]" value="N"> 
                    <input type="checkbox" id="linked_to_emirates_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ec_insurance_linked_to_emirates" name="prescription_details[linked_to_emirates]" value="Y"> 
                    <span><?php echo $_smarty_tpl->__("ec_insurance_linked_to_emirates");?>
</span>
                </div>
                <div class="ec_checked_info_text hidden"><?php echo $_smarty_tpl->__("ec_insurance_linked_to_emirates_desc");?>
</div>
            </div> 
            <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/insurance_cards.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            
        </div>
    </div>
    <?php echo '<script'; ?>
>
        (function(_, $) {
            $.ceEvent('on', 'ce.commoninit', function(context){
                $(document).on('click', '.ec_prescription_top_div .ty-tabs__item', function(){
                    if($(this).attr("id") == 'ec_tab_authorised_carer'){
                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').show();
                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').find('.ty-control-group__title').each(function(){
                            $(this).addClass('cm-required');
                        });
                    } else {
                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').hide();
                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').find('.ty-control-group__title').each(function(){
                            $(this).removeClass('cm-required');
                        });
                    }
                });
                $(".do_not_use_insurance").change(function() {
                    if(this.checked) {
                        $('#ec_insurance_upload_block .ec_upload_form_blocks').hide();
                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'hidden');
                        $('.dont_use_insurance_text').removeClass('ec-imp-hidden');
                        $('.ec_insurance_linked_to_emirates').prop('checked', false);
                        $('#emirate_is_insurance_block').hide();
                    } else {
                        $('#ec_insurance_upload_block .ec_upload_form_blocks').show();
                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'visible');
                        $('.dont_use_insurance_text').addClass('ec-imp-hidden');
                        $('#emirate_is_insurance_block').show();
                    }
                });
                $(".ec_insurance_linked_to_emirates").change(function() {
                    if(this.checked) {
                        $('.ec_checked_info_text').show();
                        $('#ec_insurance_upload_block .ec_upload_form_blocks').hide();
                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'hidden');      
                    } else {
                        $('.ec_checked_info_text').hide();
                        $('#ec_insurance_upload_block .ec_upload_form_blocks').show();
                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'visible');      
                    }
                });
                <?php if ($_smarty_tpl->tpl_vars['insurance_type']->value) {?>
                    <?php if ($_smarty_tpl->tpl_vars['insurance_type']->value=="E") {?>
                        $("#linked_to_emirates_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
").click();
                    <?php } elseif ($_smarty_tpl->tpl_vars['insurance_type']->value=="N") {?>
                        $("#dont_use_insurance_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
").click();
                    <?php } elseif ($_smarty_tpl->tpl_vars['insurance_type']->value=="S") {?>

                    <?php }?>
                <?php }?>
            });
            $(document).ready(function(){
                $('#dont_use_insurance_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
').click();
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
</form>
<!--step_two--></div><?php }?><?php }} ?>
