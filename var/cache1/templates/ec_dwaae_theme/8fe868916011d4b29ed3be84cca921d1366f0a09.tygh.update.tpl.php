<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 15:45:37
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:339841150609679e1e4a7b2-12456927%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8fe868916011d4b29ed3be84cca921d1366f0a09' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/update.tpl',
      1 => 1618395486,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '339841150609679e1e4a7b2-12456927',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'prescription' => 0,
    'settings' => 0,
    'config' => 0,
    'curl' => 0,
    'no_hide_input_if_shared_product' => 0,
    'p' => 0,
    'product' => 0,
    'user_profiles' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_609679e205fae0_79337887',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_609679e205fae0_79337887')) {function content_609679e205fae0_79337887($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_enum')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.enum.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('order','status','d_prescription.pay_by_card','d_prescription.cash_on_delivery','h_rfq.rewise_prescription_comment','h_rfq.rewise_prescription_order','h_rfq.rewise_prescription','h_rfq.rewise_prescription','h_rfq.reject_prescription_comment','h_rfq.reject_prescription_order','h_rfq.reject_prescription','h_rfq.reject_prescription','d_prescription.patient_details','d_prescription.prescription_details','d_prescription.prescriptions_attachment','d_prescription.e_prescription_no','d_prescription.doctor_emirates','customer_note','h_rfq.product_name','h_rfq.covered_in_insurance','h_rfq.unit_price_with_tax','h_rfq.product_qty','h_rfq.total_price_with_vat','h_rfq.read_more','h_rfq.read_hide','h_rfq.product_description','yes','no','summary','h_rfq.recommended_payment_method','d_prescription.paid_by_card','d_prescription.transaction_id','d_prescription.cash_on_delivery_des','subtotal','including_tax','h_rfq.shipping_price','including_tax','total','including_tax','d_prescription.emirates_id_and_insurance_card','d_prescription.insurance_card','d_prescription.no_insurance','d_prescription.emirate_id_is_insurance','d_prescription.seprate_insurance','insurance_card','emirate_card','delivery_address','d_prescription.prescription_request','order','status','d_prescription.pay_by_card','d_prescription.cash_on_delivery','h_rfq.rewise_prescription_comment','h_rfq.rewise_prescription_order','h_rfq.rewise_prescription','h_rfq.rewise_prescription','h_rfq.reject_prescription_comment','h_rfq.reject_prescription_order','h_rfq.reject_prescription','h_rfq.reject_prescription','d_prescription.patient_details','d_prescription.prescription_details','d_prescription.prescriptions_attachment','d_prescription.e_prescription_no','d_prescription.doctor_emirates','customer_note','h_rfq.product_name','h_rfq.covered_in_insurance','h_rfq.unit_price_with_tax','h_rfq.product_qty','h_rfq.total_price_with_vat','h_rfq.read_more','h_rfq.read_hide','h_rfq.product_description','yes','no','summary','h_rfq.recommended_payment_method','d_prescription.paid_by_card','d_prescription.transaction_id','d_prescription.cash_on_delivery_des','subtotal','including_tax','h_rfq.shipping_price','including_tax','total','including_tax','d_prescription.emirates_id_and_insurance_card','d_prescription.insurance_card','d_prescription.no_insurance','d_prescription.emirate_id_is_insurance','d_prescription.seprate_insurance','insurance_card','emirate_card','delivery_address','d_prescription.prescription_request'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
<?php $_smarty_tpl->_capture_stack[0][] = array("order_actions", null, null); ob_start(); ?>
    <div class="ec_top_bar">
        <div class="ec_left_div">
            <span class="ty-product-filters__title"><?php echo $_smarty_tpl->__("order");?>
 #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
</span>
            <span class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['prescription']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
            <span class="ec_order_status ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['prescription']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                <span><?php echo $_smarty_tpl->__("status");?>
:</span><span> <?php echo htmlspecialchars(fn_d_prescription_get_statuses($_smarty_tpl->tpl_vars['prescription']->value['status']), ENT_QUOTES, 'UTF-8');?>
</span>
            </span>
        </div>
        <div class="ec_right_div">
        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['curl']->value), null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_UNDER_REVIEW")) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_meta'=>"ty-btn__primary ec-imp-white_color cm-no-ajax",'but_text'=>$_smarty_tpl->__("d_prescription.pay_by_card"),'but_href'=>"d_presc_paytab.pay_by_card?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value),'but_icon'=>''), 0);?>

            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_meta'=>"ty-btn__primary ec-imp-white_color cm-no-ajax cm-post",'but_text'=>$_smarty_tpl->__("d_prescription.cash_on_delivery"),'but_href'=>"d_prescription.order_action.accept?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value),'but_icon'=>''), 0);?>


            <?php $_smarty_tpl->_capture_stack[0][] = array("rewise_prescription", null, null); ob_start(); ?>
                <div class="">
                    <form name="rewise_prescription_form" action="<?php echo htmlspecialchars(fn_url(), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
                        <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
                        <div class="ty-control-group">
                            <label for="rewise_comment" class="ty-control-group__title cm-required">
                                <?php echo $_smarty_tpl->__("h_rfq.rewise_prescription_comment");?>

                            </label>
                            <textarea name="customer_comment" id="rewise_comment" style="width:320px;height:150px;"></textarea>
                        </div>
                        <div class="buttons-container ty-right">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit",'but_meta'=>"ty-btn__primary",'but_text'=>$_smarty_tpl->__("h_rfq.rewise_prescription_order"),'but_name'=>"dispatch[d_prescription.order_action.rewise]",'but_icon'=>''), 0);?>

                        </div>
                    </form>
                </div>
            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"rewise_order_with_comment",'link_text'=>$_smarty_tpl->__("h_rfq.rewise_prescription"),'text'=>$_smarty_tpl->__("h_rfq.rewise_prescription"),'content'=>Smarty::$_smarty_vars['capture']['rewise_prescription'],'act'=>"general",'link_meta'=>"ty-btn ty-btn__primary ec-imp-white_color cm-dialog-auto-size",'link_icon'=>"icon-plus"), 0);?>


            <?php $_smarty_tpl->_capture_stack[0][] = array("reject_prescription", null, null); ob_start(); ?>
                <div class="">
                    <form name="reject_prescription_form" action="<?php echo htmlspecialchars(fn_url(), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
                        <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
                        <div class="ty-control-group">
                            <label for="reject_comment" class="ty-control-group__title cm-required">
                                <?php echo $_smarty_tpl->__("h_rfq.reject_prescription_comment");?>

                            </label>
                            <textarea name="customer_comment" id="reject_comment" style="width:320px;height:150px;"></textarea>
                        </div>
                        <div class="buttons-container ty-right">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit",'but_meta'=>"ty-btn__primary",'but_text'=>$_smarty_tpl->__("h_rfq.reject_prescription_order"),'but_name'=>"dispatch[d_prescription.order_action.reject]",'but_icon'=>''), 0);?>

                        </div>
                    </form>
                </div>
            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"reject_order_with_comment",'link_text'=>$_smarty_tpl->__("h_rfq.reject_prescription"),'text'=>$_smarty_tpl->__("h_rfq.reject_prescription"),'content'=>Smarty::$_smarty_vars['capture']['reject_prescription'],'act'=>"general",'link_meta'=>"ty-btn ty-btn__primary ec-imp-white_color cm-dialog-auto-size",'link_icon'=>"icon-plus"), 0);?>

        <?php }?>
        </div>
    </div>
    
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<div class="ty-orders-detail">
    <?php echo Smarty::$_smarty_vars['capture']['order_actions'];?>


    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.patient_details"),'target'=>"#acc_patient_details"), 0);?>

    <div id="acc_patient_details" class="collapse in collapse-visible">
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['firstname'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['lastname'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['email'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['phone'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.prescription_details"),'target'=>"#acc_prescription_details"), 0);?>

    <div id="acc_prescription_details" class="collapse in collapse-visible">
        <div class="du-control-group">
            <label for="prescriptions_attachment" class="du-control-label"><?php echo $_smarty_tpl->__("d_prescription.prescriptions_attachment");?>
</label>
            <div class="du-controls">
                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescription']->value['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                    <?php if (strpos($_smarty_tpl->tpl_vars['p']->value,'prescriptions')!==false) {?>
                        <a href="<?php echo htmlspecialchars(fn_url_for_app("d_prescription.download&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="" target="_blank"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                        <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                            <br/>
                        <?php }?>
                    <?php }?>
                <?php } ?>
            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            <label for="e_prescription_no" class="du-control-label"><?php echo $_smarty_tpl->__("d_prescription.e_prescription_no");?>
</label>
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['e_prescription_no'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            <label for="doctor_emirates" class="du-control-label"><?php echo $_smarty_tpl->__("d_prescription.doctor_emirates");?>
</label>
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['doctor_emirates'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['customer_note']) {?>
        <div class="du-control-group">
            <label for="customer_note" class="du-control-label"><?php echo $_smarty_tpl->__("customer_note");?>
</label>
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['customer_note'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['prescription']->value['products']) {?>
        <h4>Products</h4>
        <table class="ty-orders-detail__table ty-table">
        <thead>
            <tr>
                <th class="ty-orders-detail__table-product"><?php echo $_smarty_tpl->__("h_rfq.product_name");?>
</th>
                <th class="ty-orders-detail__table-subtotal"><?php echo $_smarty_tpl->__("h_rfq.covered_in_insurance");?>
</th>
                <th class="ty-orders-detail__table-price"><?php echo $_smarty_tpl->__("h_rfq.unit_price_with_tax");?>
</th>
                <th class="ty-orders-detail__table-quantity"><?php echo $_smarty_tpl->__("h_rfq.product_qty");?>
</th>
                <th class="ty-orders-detail__table-subtotal"><?php echo $_smarty_tpl->__("h_rfq.total_price_with_vat");?>
</th>
            </tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescription']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
            <tr class="ty-valign-top">
                <td>
                    <div class="clearfix">
                        <div class="ty-overflow-hidden ty-orders-detail__table-description-wrapper">
                            <div class="ty-ml-s ty-orders-detail__table-description">
                                
                                    <strong><?php echo $_smarty_tpl->tpl_vars['product']->value['product_name'];?>
</strong>
                                    <div class="cm-show_hide_text h-rfq_details_description" data-ca-charqty="150" data-ca-ellipse-text=".." data-ca-more-text="<?php echo $_smarty_tpl->__("h_rfq.read_more");?>
" data-ca-less-text="<?php echo $_smarty_tpl->__("h_rfq.read_hide");?>
"><?php echo $_smarty_tpl->__("h_rfq.product_description");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['description'], ENT_QUOTES, 'UTF-8');?>
</div>
                                
                            </div>
                        </div>
                    </div>
                </td>
                <td class="ty-center"><?php if ($_smarty_tpl->tpl_vars['product']->value['covered_in_insurance']=='Y') {
echo $_smarty_tpl->__("yes");
} else {
echo $_smarty_tpl->__("no");
}?></td>
                <td class="ty-right">
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['tax_price']>0) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_price']), 0);
} else { ?>--<?php }?>
                </td>
                <td class="ty-center">&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
 (Unit: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity_unit'], ENT_QUOTES, 'UTF-8');?>
)</td>
                <td class="ty-right">
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['tax_price_total']) {?>&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_price_total']), 0);
} else { ?>--<?php }?>
                </td>
            </tr>
        <?php } ?>
        </table>
        <div class="ty-orders-summary clearfix">
            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("summary")), 0);?>


            <div class="ty-orders-summary__right">
                
            </div>

            <div class="ty-orders-summary__wrapper">
                <table class="ty-orders-summary__table">
                    
                    <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.recommended_payment_method");?>
:&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['paid_by_card']=="Y") {?>
                                <?php echo $_smarty_tpl->__("d_prescription.paid_by_card");
if ($_smarty_tpl->tpl_vars['prescription']->value['payment_response']['transaction_id']) {?>&nbsp;&nbsp;(<?php echo $_smarty_tpl->__("d_prescription.transaction_id");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['payment_response']['transaction_id'], ENT_QUOTES, 'UTF-8');?>
)<?php }?>
                            <?php } else { ?>
                                <?php echo $_smarty_tpl->__("d_prescription.cash_on_delivery_des");?>

                            <?php }?>
                        </td>
                    </tr>

                    <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("subtotal");?>
(<?php echo $_smarty_tpl->__("including_tax");?>
):&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['subtotal_with_tax']) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['subtotal_with_tax']), 0);?>

                        <?php } else { ?>
                            --
                        <?php }?>
                        </td>
                    </tr>

                    <?php if ($_smarty_tpl->tpl_vars['prescription']->value['shipping_tax_price']) {?>
                        <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.shipping_price");?>
(<?php echo $_smarty_tpl->__("including_tax");?>
):&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['shipping_tax_price']), 0);?>

                        </td>
                    </tr>
                    <?php }?>

                    <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("total");?>
(<?php echo $_smarty_tpl->__("including_tax");?>
):&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['total']) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['total']), 0);?>

                        <?php } else { ?>
                            --
                        <?php }?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php }?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.emirates_id_and_insurance_card"),'target'=>"#emirates_id_and_insurance_card"), 0);?>

    <div id="emirates_id_and_insurance_card" class="collapse in collapse-visible">
        
        <div class="du-control-group">
            <label for="insurance_card" class="du-control-label"><?php echo $_smarty_tpl->__("d_prescription.insurance_card");?>
</label>
            <div class="du-controls">
                <?php if ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='N') {?>
                    <?php echo $_smarty_tpl->__("d_prescription.no_insurance");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='E') {?>
                    <?php echo $_smarty_tpl->__("d_prescription.emirate_id_is_insurance");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='S') {?>
                    <?php echo $_smarty_tpl->__("d_prescription.seprate_insurance");?>

                <?php }?>
            </div>
        </div>

        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='S'&&$_smarty_tpl->tpl_vars['prescription']->value['order_data']['insurance_card']) {?>
            <div class="ec_prescription_top_div">
                <div class="ec_patient_tab_content">
                    <div  class="ec-header_prescription_order"><?php echo $_smarty_tpl->__("insurance_card");?>
</div>
                    <div class="ec_upload_form_blocks">
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/insurance_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card_view_only'=>true,'insurance_card'=>$_smarty_tpl->tpl_vars['prescription']->value['order_data']['insurance_card']), 0);?>

                    </div>
                </div>
            </div>  
        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['order_data']['emirate_card']) {?>
            <div class="ec_prescription_top_div">
                <div class="ec_patient_tab_content">
                    <div class="ec-header_prescription_order"><?php echo $_smarty_tpl->__("emirate_card");?>
</div>
                    <div class="ec_upload_form_blocks">
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card_view_only'=>true,'emirate_card'=>$_smarty_tpl->tpl_vars['prescription']->value['order_data']['emirate_card'],'enable_card_select'=>false), 0);?>

            </div>
                </div>
            </div>  
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']) {?>
            <div class="ec_prescription_top_div">
                <div class="ec-header_prescription_order"><?php echo $_smarty_tpl->__("delivery_address");?>
</div>
                <div class="ec_profile_container">
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card_view_only'=>true,'user_profile'=>$_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']), 0);?>

                </div>
            </div>
        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/user_profiles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('user_profiles'=>$_smarty_tpl->tpl_vars['user_profiles']->value), 0);?>
    
    </div>
</div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->__("d_prescription.prescription_request",array('[req_id]'=>$_smarty_tpl->tpl_vars['prescription']->value['req_id']));?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>



<div class="d-upload-form-details">
    <?php echo Smarty::$_smarty_vars['capture']['mainbox'];?>

</div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_prescription/views/d_prescription/update.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_prescription/views/d_prescription/update.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
<?php $_smarty_tpl->_capture_stack[0][] = array("order_actions", null, null); ob_start(); ?>
    <div class="ec_top_bar">
        <div class="ec_left_div">
            <span class="ty-product-filters__title"><?php echo $_smarty_tpl->__("order");?>
 #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
</span>
            <span class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['prescription']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
            <span class="ec_order_status ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['prescription']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                <span><?php echo $_smarty_tpl->__("status");?>
:</span><span> <?php echo htmlspecialchars(fn_d_prescription_get_statuses($_smarty_tpl->tpl_vars['prescription']->value['status']), ENT_QUOTES, 'UTF-8');?>
</span>
            </span>
        </div>
        <div class="ec_right_div">
        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['curl']->value), null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_UNDER_REVIEW")) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_meta'=>"ty-btn__primary ec-imp-white_color cm-no-ajax",'but_text'=>$_smarty_tpl->__("d_prescription.pay_by_card"),'but_href'=>"d_presc_paytab.pay_by_card?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value),'but_icon'=>''), 0);?>

            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_meta'=>"ty-btn__primary ec-imp-white_color cm-no-ajax cm-post",'but_text'=>$_smarty_tpl->__("d_prescription.cash_on_delivery"),'but_href'=>"d_prescription.order_action.accept?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value),'but_icon'=>''), 0);?>


            <?php $_smarty_tpl->_capture_stack[0][] = array("rewise_prescription", null, null); ob_start(); ?>
                <div class="">
                    <form name="rewise_prescription_form" action="<?php echo htmlspecialchars(fn_url(), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
                        <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
                        <div class="ty-control-group">
                            <label for="rewise_comment" class="ty-control-group__title cm-required">
                                <?php echo $_smarty_tpl->__("h_rfq.rewise_prescription_comment");?>

                            </label>
                            <textarea name="customer_comment" id="rewise_comment" style="width:320px;height:150px;"></textarea>
                        </div>
                        <div class="buttons-container ty-right">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit",'but_meta'=>"ty-btn__primary",'but_text'=>$_smarty_tpl->__("h_rfq.rewise_prescription_order"),'but_name'=>"dispatch[d_prescription.order_action.rewise]",'but_icon'=>''), 0);?>

                        </div>
                    </form>
                </div>
            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"rewise_order_with_comment",'link_text'=>$_smarty_tpl->__("h_rfq.rewise_prescription"),'text'=>$_smarty_tpl->__("h_rfq.rewise_prescription"),'content'=>Smarty::$_smarty_vars['capture']['rewise_prescription'],'act'=>"general",'link_meta'=>"ty-btn ty-btn__primary ec-imp-white_color cm-dialog-auto-size",'link_icon'=>"icon-plus"), 0);?>


            <?php $_smarty_tpl->_capture_stack[0][] = array("reject_prescription", null, null); ob_start(); ?>
                <div class="">
                    <form name="reject_prescription_form" action="<?php echo htmlspecialchars(fn_url(), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
                        <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
                        <div class="ty-control-group">
                            <label for="reject_comment" class="ty-control-group__title cm-required">
                                <?php echo $_smarty_tpl->__("h_rfq.reject_prescription_comment");?>

                            </label>
                            <textarea name="customer_comment" id="reject_comment" style="width:320px;height:150px;"></textarea>
                        </div>
                        <div class="buttons-container ty-right">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit",'but_meta'=>"ty-btn__primary",'but_text'=>$_smarty_tpl->__("h_rfq.reject_prescription_order"),'but_name'=>"dispatch[d_prescription.order_action.reject]",'but_icon'=>''), 0);?>

                        </div>
                    </form>
                </div>
            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"reject_order_with_comment",'link_text'=>$_smarty_tpl->__("h_rfq.reject_prescription"),'text'=>$_smarty_tpl->__("h_rfq.reject_prescription"),'content'=>Smarty::$_smarty_vars['capture']['reject_prescription'],'act'=>"general",'link_meta'=>"ty-btn ty-btn__primary ec-imp-white_color cm-dialog-auto-size",'link_icon'=>"icon-plus"), 0);?>

        <?php }?>
        </div>
    </div>
    
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<div class="ty-orders-detail">
    <?php echo Smarty::$_smarty_vars['capture']['order_actions'];?>


    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.patient_details"),'target'=>"#acc_patient_details"), 0);?>

    <div id="acc_patient_details" class="collapse in collapse-visible">
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['firstname'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['lastname'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['email'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['phone'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.prescription_details"),'target'=>"#acc_prescription_details"), 0);?>

    <div id="acc_prescription_details" class="collapse in collapse-visible">
        <div class="du-control-group">
            <label for="prescriptions_attachment" class="du-control-label"><?php echo $_smarty_tpl->__("d_prescription.prescriptions_attachment");?>
</label>
            <div class="du-controls">
                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescription']->value['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                    <?php if (strpos($_smarty_tpl->tpl_vars['p']->value,'prescriptions')!==false) {?>
                        <a href="<?php echo htmlspecialchars(fn_url_for_app("d_prescription.download&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="" target="_blank"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                        <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                            <br/>
                        <?php }?>
                    <?php }?>
                <?php } ?>
            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            <label for="e_prescription_no" class="du-control-label"><?php echo $_smarty_tpl->__("d_prescription.e_prescription_no");?>
</label>
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['e_prescription_no'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <div class="du-control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
            <label for="doctor_emirates" class="du-control-label"><?php echo $_smarty_tpl->__("d_prescription.doctor_emirates");?>
</label>
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['doctor_emirates'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['customer_note']) {?>
        <div class="du-control-group">
            <label for="customer_note" class="du-control-label"><?php echo $_smarty_tpl->__("customer_note");?>
</label>
            <div class="du-controls">
                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['customer_note'], ENT_QUOTES, 'UTF-8');?>

            </div>
        </div>
        <?php }?>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['prescription']->value['products']) {?>
        <h4>Products</h4>
        <table class="ty-orders-detail__table ty-table">
        <thead>
            <tr>
                <th class="ty-orders-detail__table-product"><?php echo $_smarty_tpl->__("h_rfq.product_name");?>
</th>
                <th class="ty-orders-detail__table-subtotal"><?php echo $_smarty_tpl->__("h_rfq.covered_in_insurance");?>
</th>
                <th class="ty-orders-detail__table-price"><?php echo $_smarty_tpl->__("h_rfq.unit_price_with_tax");?>
</th>
                <th class="ty-orders-detail__table-quantity"><?php echo $_smarty_tpl->__("h_rfq.product_qty");?>
</th>
                <th class="ty-orders-detail__table-subtotal"><?php echo $_smarty_tpl->__("h_rfq.total_price_with_vat");?>
</th>
            </tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescription']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
            <tr class="ty-valign-top">
                <td>
                    <div class="clearfix">
                        <div class="ty-overflow-hidden ty-orders-detail__table-description-wrapper">
                            <div class="ty-ml-s ty-orders-detail__table-description">
                                
                                    <strong><?php echo $_smarty_tpl->tpl_vars['product']->value['product_name'];?>
</strong>
                                    <div class="cm-show_hide_text h-rfq_details_description" data-ca-charqty="150" data-ca-ellipse-text=".." data-ca-more-text="<?php echo $_smarty_tpl->__("h_rfq.read_more");?>
" data-ca-less-text="<?php echo $_smarty_tpl->__("h_rfq.read_hide");?>
"><?php echo $_smarty_tpl->__("h_rfq.product_description");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['description'], ENT_QUOTES, 'UTF-8');?>
</div>
                                
                            </div>
                        </div>
                    </div>
                </td>
                <td class="ty-center"><?php if ($_smarty_tpl->tpl_vars['product']->value['covered_in_insurance']=='Y') {
echo $_smarty_tpl->__("yes");
} else {
echo $_smarty_tpl->__("no");
}?></td>
                <td class="ty-right">
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['tax_price']>0) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_price']), 0);
} else { ?>--<?php }?>
                </td>
                <td class="ty-center">&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
 (Unit: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity_unit'], ENT_QUOTES, 'UTF-8');?>
)</td>
                <td class="ty-right">
                    <?php if ($_smarty_tpl->tpl_vars['product']->value['tax_price_total']) {?>&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_price_total']), 0);
} else { ?>--<?php }?>
                </td>
            </tr>
        <?php } ?>
        </table>
        <div class="ty-orders-summary clearfix">
            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("summary")), 0);?>


            <div class="ty-orders-summary__right">
                
            </div>

            <div class="ty-orders-summary__wrapper">
                <table class="ty-orders-summary__table">
                    
                    <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.recommended_payment_method");?>
:&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['paid_by_card']=="Y") {?>
                                <?php echo $_smarty_tpl->__("d_prescription.paid_by_card");
if ($_smarty_tpl->tpl_vars['prescription']->value['payment_response']['transaction_id']) {?>&nbsp;&nbsp;(<?php echo $_smarty_tpl->__("d_prescription.transaction_id");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['payment_response']['transaction_id'], ENT_QUOTES, 'UTF-8');?>
)<?php }?>
                            <?php } else { ?>
                                <?php echo $_smarty_tpl->__("d_prescription.cash_on_delivery_des");?>

                            <?php }?>
                        </td>
                    </tr>

                    <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("subtotal");?>
(<?php echo $_smarty_tpl->__("including_tax");?>
):&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['subtotal_with_tax']) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['subtotal_with_tax']), 0);?>

                        <?php } else { ?>
                            --
                        <?php }?>
                        </td>
                    </tr>

                    <?php if ($_smarty_tpl->tpl_vars['prescription']->value['shipping_tax_price']) {?>
                        <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.shipping_price");?>
(<?php echo $_smarty_tpl->__("including_tax");?>
):&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['shipping_tax_price']), 0);?>

                        </td>
                    </tr>
                    <?php }?>

                    <tr class="ty-orders-summary__row">
                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("total");?>
(<?php echo $_smarty_tpl->__("including_tax");?>
):&nbsp;</td>
                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['total']) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['total']), 0);?>

                        <?php } else { ?>
                            --
                        <?php }?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php }?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.emirates_id_and_insurance_card"),'target'=>"#emirates_id_and_insurance_card"), 0);?>

    <div id="emirates_id_and_insurance_card" class="collapse in collapse-visible">
        
        <div class="du-control-group">
            <label for="insurance_card" class="du-control-label"><?php echo $_smarty_tpl->__("d_prescription.insurance_card");?>
</label>
            <div class="du-controls">
                <?php if ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='N') {?>
                    <?php echo $_smarty_tpl->__("d_prescription.no_insurance");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='E') {?>
                    <?php echo $_smarty_tpl->__("d_prescription.emirate_id_is_insurance");?>

                <?php } elseif ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='S') {?>
                    <?php echo $_smarty_tpl->__("d_prescription.seprate_insurance");?>

                <?php }?>
            </div>
        </div>

        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='S'&&$_smarty_tpl->tpl_vars['prescription']->value['order_data']['insurance_card']) {?>
            <div class="ec_prescription_top_div">
                <div class="ec_patient_tab_content">
                    <div  class="ec-header_prescription_order"><?php echo $_smarty_tpl->__("insurance_card");?>
</div>
                    <div class="ec_upload_form_blocks">
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/insurance_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card_view_only'=>true,'insurance_card'=>$_smarty_tpl->tpl_vars['prescription']->value['order_data']['insurance_card']), 0);?>

                    </div>
                </div>
            </div>  
        <?php }?>
        
        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['order_data']['emirate_card']) {?>
            <div class="ec_prescription_top_div">
                <div class="ec_patient_tab_content">
                    <div class="ec-header_prescription_order"><?php echo $_smarty_tpl->__("emirate_card");?>
</div>
                    <div class="ec_upload_form_blocks">
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card_view_only'=>true,'emirate_card'=>$_smarty_tpl->tpl_vars['prescription']->value['order_data']['emirate_card'],'enable_card_select'=>false), 0);?>

            </div>
                </div>
            </div>  
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']) {?>
            <div class="ec_prescription_top_div">
                <div class="ec-header_prescription_order"><?php echo $_smarty_tpl->__("delivery_address");?>
</div>
                <div class="ec_profile_container">
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card_view_only'=>true,'user_profile'=>$_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']), 0);?>

                </div>
            </div>
        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/user_profiles.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('user_profiles'=>$_smarty_tpl->tpl_vars['user_profiles']->value), 0);?>
    
    </div>
</div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->__("d_prescription.prescription_request",array('[req_id]'=>$_smarty_tpl->tpl_vars['prescription']->value['req_id']));?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>



<div class="d-upload-form-details">
    <?php echo Smarty::$_smarty_vars['capture']['mainbox'];?>

</div>

<?php }?><?php }} ?>
