<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 10:48:31
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/orders/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12791033946093913f375078-09870495%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '78e142a1535c05d593b2ddde74176fa50cc2fb96' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/orders/search.tpl',
      1 => 1607430806,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '12791033946093913f375078-09870495',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'search' => 0,
    'config' => 0,
    'orders' => 0,
    'o' => 0,
    'settings' => 0,
    'o_info' => 0,
    'cart_products' => 0,
    'product' => 0,
    'total_order_count' => 0,
    'obj_id' => 0,
    'key' => 0,
    'addons' => 0,
    'ec_wishlist' => 0,
    'selected' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6093913f42bb53_36442733',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6093913f42bb53_36442733')) {function content_6093913f42bb53_36442733($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('filter','order_status','order_id','order_id','total','period','search','ec_category_product_count','ec_category_product_count','order','status','ec_view_order_details','re_order','by','ec_add_to_wishlist','ec_search_product','ec_qty','ec_your_wishlist_empty','orders','filter','order_status','order_id','order_id','total','period','search','ec_category_product_count','ec_category_product_count','order','status','ec_view_order_details','re_order','by','ec_add_to_wishlist','ec_search_product','ec_qty','ec_your_wishlist_empty','orders'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_order_search_container">
    
    <div class="span4 side-grid">
        <div class="ty-sidebox-important ec_filters">
            <h3 class="ty-sidebox-important__title"><div class="ty-sidebox__title-wrapper"><?php echo $_smarty_tpl->__("filter");?>
</div></h3>
            <div class="ty-sidebox-important__body">
                <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" class="ty-orders-search-options" name="orders_search_form" method="get">

                    <div class="ec_filter_accordian_div">
                        <div id="sw_content_order_status" class="ty-order_filter__switch cm-combination-filter_order_status open cm-save-state">
                            <div class="order_filter_header">
                                <?php echo $_smarty_tpl->__("order_status");?>

                            </div>
                            <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                            <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                        </div>

                        <div class="ty-product-filters" id="content_order_status">
                            <?php echo $_smarty_tpl->getSubTemplate ("common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['search']->value['status'],'display'=>"checkboxes",'name'=>"status",'checkboxes_meta'=>"ty-orders-search__options-status"), 0);?>

                        </div>
                    </div>

                    <div class="ec_filter_accordian_div">
                        <div id="sw_content_order_id" class="ty-order_filter__switch cm-combination-filter_order_id open cm-save-state">
                            <div class="order_filter_header">
                                <?php echo $_smarty_tpl->__("order_id");?>

                            </div>
                            <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                            <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                        </div>

                        <div class="ty-product-filters" id="content_order_id">
                            <input type="text" name="order_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
" size="10" placeholder="<?php echo $_smarty_tpl->__("order_id");?>
" class="ty-search-form__input" />
                        </div>
                    </div>

                    <div class="ec_filter_accordian_div">
                        <div id="sw_content_total" class="ty-order_filter__switch cm-combination-filter_total open cm-save-state">
                            <div class="order_filter_header">
                                <?php echo $_smarty_tpl->__("total");?>

                            </div>
                            <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                            <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                        </div>

                        <div class="ty-product-filters" id="content_total">
                            <input type="text" name="total_sec_from" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['total_sec_from'], ENT_QUOTES, 'UTF-8');?>
" size="3" class="ty-control-group__price" /><span>&#8211;</span><input type="text" name="total_sec_to" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['total_sec_to'], ENT_QUOTES, 'UTF-8');?>
" size="3" class="ty-control-group__price" />
                        </div>
                    </div>

                    <div class="ec_filter_accordian_div">
                        <div id="sw_content_period" class="ty-order_filter__switch cm-combination-filter_period open cm-save-state">
                            <div class="order_filter_header">
                                <?php echo $_smarty_tpl->__("period");?>

                            </div>
                            <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                            <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                        </div>

                        <div class="ty-product-filters" id="content_period">
                            <?php echo $_smarty_tpl->getSubTemplate ("common/period_selector.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('period'=>$_smarty_tpl->tpl_vars['search']->value['period'],'form_name'=>"orders_search_form"), 0);?>

                        </div>
                    </div>


                    <div class="buttons-container ty-search-form__buttons-container" style="background: initial;">
                        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_meta'=>"ty-btn__secondary ty-float-right",'but_text'=>$_smarty_tpl->__("search"),'but_name'=>"dispatch[orders.search]"), 0);?>

                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="span12">
        <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['search']->value['sort_order']=="asc") {?>
        <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"ty-icon-down-dir\"></i>", null, 0);?>
        <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"ty-icon-up-dir\"></i>", null, 0);?>
        <?php }?>
        <?php if (!$_smarty_tpl->tpl_vars['config']->value['tweaks']['disable_dhtml']) {?>
            <?php $_smarty_tpl->tpl_vars["ajax_class"] = new Smarty_variable("cm-ajax", null, 0);?>

        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            
                <?php if ($_smarty_tpl->tpl_vars['search']->value&&$_smarty_tpl->tpl_vars['search']->value['total_items']) {?>
                    <?php $_smarty_tpl->_capture_stack[0][] = array("ec_category_product_count", "ec_title_extra", null); ob_start(); ?>
                        <div class="ec_category_product_count"><?php echo $_smarty_tpl->__("ec_category_product_count",array("[count]"=>$_smarty_tpl->tpl_vars['search']->value['total_items']));?>
</div>
                    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                <?php } else { ?>
                    <?php $_smarty_tpl->_capture_stack[0][] = array("ec_category_product_count", "ec_title_extra", null); ob_start(); ?>
                        <div class="ec_category_product_count"><?php echo $_smarty_tpl->__("ec_category_product_count",array("[count]"=>0));?>
</div>
                    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/sorting.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('avail_sorting'=>false), 0);?>

            

            <?php  $_smarty_tpl->tpl_vars["o"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["o"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_orders"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars["o"]->key => $_smarty_tpl->tpl_vars["o"]->value) {
$_smarty_tpl->tpl_vars["o"]->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_orders"]['iteration']++;
?>

                <div class="ec_order_accordian_div">
                    <div id="sw_content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
" class="ty-order__switch cm-combination-filter_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value['order_id'], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->getVariable('smarty')->value['foreach']['ec_orders']['iteration']==1) {?> open<?php }?> cm-save-state">
                        <div class="order_header">
                            <div class="ec_left_content">
                                <span class="ty-product-filters__title"><a href="<?php echo htmlspecialchars(fn_url("orders.details?order_id=".((string)$_smarty_tpl->tpl_vars['o']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
" style="font-size: 16px; font-weight: bold; display: block; line-height: 1;color: black;"><?php echo $_smarty_tpl->__("order");?>
 #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
</a></span>
                                <span class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['o']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
                                <span class="ec_order_status ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['o']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                                    <span><?php echo $_smarty_tpl->__("status");?>
:</span><span><?php echo $_smarty_tpl->getSubTemplate ("common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['o']->value['status'],'display'=>"view"), 0);?>
</span>
                                </span>
                            </div>
                            <div class="ec_right_content">
                                <div>
                                    <a href="<?php echo htmlspecialchars(fn_url("orders.details?order_id=".((string)$_smarty_tpl->tpl_vars['o']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                        <span class="hidden-phone"><?php echo $_smarty_tpl->__("ec_view_order_details");?>
</span>
                                        <span class="ec-icon-order-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>  
                                    </a>  
                                </div>
                            </div>
                        
                        </div>
                        <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                        <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                    </div>

                    <div class="ty-product-filters <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['ec_orders']['iteration']!=1) {?>hidden<?php }?>" id="content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
">
                        <?php $_smarty_tpl->tpl_vars['o_info'] = new Smarty_variable(fn_get_order_info($_smarty_tpl->tpl_vars['o']->value['order_id']), null, 0);?>
                        <div class="ec_order_content_wrapper">
                            <div class="span12">
                                <div id="cart_items">
                                    <?php  $_smarty_tpl->tpl_vars["cart_products"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["cart_products"]->_loop = false;
 $_smarty_tpl->tpl_vars["k"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['o_info']->value['product_groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["cart_products"]->key => $_smarty_tpl->tpl_vars["cart_products"]->value) {
$_smarty_tpl->tpl_vars["cart_products"]->_loop = true;
 $_smarty_tpl->tpl_vars["k"]->value = $_smarty_tpl->tpl_vars["cart_products"]->key;
?>
                                        <div class="ec_order_table_container">
                                            <table class="ty-cart-content ty-table">
                                                <tbody>
                                                    <?php if ($_smarty_tpl->tpl_vars['cart_products']->value['products']) {?>
                                                        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                                                            <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                                                            <?php $_smarty_tpl->tpl_vars['total_order_count'] = new Smarty_variable($_smarty_tpl->tpl_vars['total_order_count']->value+$_smarty_tpl->tpl_vars['product']->value['amount'], null, 0);?>

                                                            <tr>
                                                                <td class="ec_reorder_div ec_middle" width="1%">
                                                                    <a href="<?php echo htmlspecialchars(fn_url("orders.reorder?order_id=".((string)$_smarty_tpl->tpl_vars['o_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("re_order");?>
"><i class="ec-icon-reorder-1"></i></a>
                                                                </td>
                                                                <td class="ty-cart-content__image-block" width="15%">
                                                                    <div class="ty-cart-content__image cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="product_image_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                                                                        <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                                                            <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('obj_id'=>$_smarty_tpl->tpl_vars['key']->value,'images'=>$_smarty_tpl->tpl_vars['product']->value['main_pair'],'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_height']), 0);?>

                                                                        </a>
                                                                    </div>
                                                                </td>

                                                                <td class="ty-cart-content__description" style="width: 30%;">
                                                                    <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" class="ty-cart-content__product-title"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],35);?>
</a>
                                                                    <div class="ec_cart_seller"><?php echo $_smarty_tpl->__("by");?>
 <a href="<?php echo htmlspecialchars(fn_url("companies.view?company_id=".((string)$_smarty_tpl->tpl_vars['product']->value['company_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');?>
</a></div>
                                                                    
                                                                    <?php if ($_smarty_tpl->tpl_vars['addons']->value['wishlist']['status']=='A') {?>
                                                                        <?php $_smarty_tpl->tpl_vars['ec_wishlist'] = new Smarty_variable(array(), null, 0);?>
                                                                        <?php if (!empty($_SESSION['wishlist']['products'])) {?>
                                                                            <?php $_smarty_tpl->tpl_vars['ec_wishlist'] = new Smarty_variable(fn_array_value_to_key($_SESSION['wishlist']['products'],'product_id'), null, 0);?>
                                                                        <?php }?>
                                                                        <?php if (array_key_exists($_smarty_tpl->tpl_vars['product']->value['product_id'],$_smarty_tpl->tpl_vars['ec_wishlist']->value)) {?>
                                                                            <?php $_smarty_tpl->tpl_vars['selected'] = new Smarty_variable('added', null, 0);?>
                                                                        <?php } else { ?>
                                                                            <?php $_smarty_tpl->tpl_vars['selected'] = new Smarty_variable('', null, 0);?>
                                                                        <?php }?>
                                                                        <div class="ec_save_later">
                                                                            <a class="cm-post cm-ajax1 cm-ajax-full-render ec_save_for_latter <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected']->value, ENT_QUOTES, 'UTF-8');?>
"  href="<?php echo htmlspecialchars(fn_url("wishlist.add..".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."&product_data[".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."][product_id]=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><i class="ec-icon-wishlist"></i><span><?php echo $_smarty_tpl->__("ec_add_to_wishlist");?>
</span></a>
                                                                        </div>
                                                                    <?php }?>
                                                                </td>

                                                                <td class="ec_order_search_div ec_middle hidden-phone" width="25%">
                                                                    <a href="<?php echo htmlspecialchars(fn_url("products.search?search_performed=Y&order_ids=".((string)$_smarty_tpl->tpl_vars['o_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                                                        <span><?php echo $_smarty_tpl->__("ec_search_product");?>
</span>
                                                                        <span class="ec-icon-find-item-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                                                                    </a>
                                                                </td>

                                                                <td class="ec_cart_amount_div ec_middle">
                                                                    <span><?php echo $_smarty_tpl->__("ec_qty");?>
</span> <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</span>
                                                                </td>

                                                                <td class="ec_order_product_price ec_middle">
                                                                    <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['price'],'span_id'=>"product_subtotal_".((string)$_smarty_tpl->tpl_vars['key']->value),'class'=>"price"), 0);?>

                                                                </td>
                                                            </tr>

                                                            <tr class="ec_break"><td colspan="6" class="ty-cart-content__product-elem"></td></tr>

                                                        <?php } ?>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
if (!$_smarty_tpl->tpl_vars["o"]->_loop) {
?>
            <style>
            .tygh-content > div {
                background-color: #ffffff;
            }
            #tygh_main_container{
                background-color: #ffffff;
            }
            </style>
             <div class="ec-container-cart ec_empty_cart ec_empty_wishlist">
                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/empty_order_list.svg" alt="<?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
" class="ec-empty-cart-image">
            </div>
            <?php } ?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("orders");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php if (@constant('EC_MOBILE_APP')) {?>
    <?php echo '<script'; ?>
>
        $(document).on('input', function() {
            $('[name="dispatch[orders.search]"]').click()
        });
    <?php echo '</script'; ?>
>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/orders/search.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/orders/search.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_order_search_container">
    
    <div class="span4 side-grid">
        <div class="ty-sidebox-important ec_filters">
            <h3 class="ty-sidebox-important__title"><div class="ty-sidebox__title-wrapper"><?php echo $_smarty_tpl->__("filter");?>
</div></h3>
            <div class="ty-sidebox-important__body">
                <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" class="ty-orders-search-options" name="orders_search_form" method="get">

                    <div class="ec_filter_accordian_div">
                        <div id="sw_content_order_status" class="ty-order_filter__switch cm-combination-filter_order_status open cm-save-state">
                            <div class="order_filter_header">
                                <?php echo $_smarty_tpl->__("order_status");?>

                            </div>
                            <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                            <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                        </div>

                        <div class="ty-product-filters" id="content_order_status">
                            <?php echo $_smarty_tpl->getSubTemplate ("common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['search']->value['status'],'display'=>"checkboxes",'name'=>"status",'checkboxes_meta'=>"ty-orders-search__options-status"), 0);?>

                        </div>
                    </div>

                    <div class="ec_filter_accordian_div">
                        <div id="sw_content_order_id" class="ty-order_filter__switch cm-combination-filter_order_id open cm-save-state">
                            <div class="order_filter_header">
                                <?php echo $_smarty_tpl->__("order_id");?>

                            </div>
                            <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                            <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                        </div>

                        <div class="ty-product-filters" id="content_order_id">
                            <input type="text" name="order_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
" size="10" placeholder="<?php echo $_smarty_tpl->__("order_id");?>
" class="ty-search-form__input" />
                        </div>
                    </div>

                    <div class="ec_filter_accordian_div">
                        <div id="sw_content_total" class="ty-order_filter__switch cm-combination-filter_total open cm-save-state">
                            <div class="order_filter_header">
                                <?php echo $_smarty_tpl->__("total");?>

                            </div>
                            <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                            <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                        </div>

                        <div class="ty-product-filters" id="content_total">
                            <input type="text" name="total_sec_from" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['total_sec_from'], ENT_QUOTES, 'UTF-8');?>
" size="3" class="ty-control-group__price" /><span>&#8211;</span><input type="text" name="total_sec_to" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['total_sec_to'], ENT_QUOTES, 'UTF-8');?>
" size="3" class="ty-control-group__price" />
                        </div>
                    </div>

                    <div class="ec_filter_accordian_div">
                        <div id="sw_content_period" class="ty-order_filter__switch cm-combination-filter_period open cm-save-state">
                            <div class="order_filter_header">
                                <?php echo $_smarty_tpl->__("period");?>

                            </div>
                            <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                            <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                        </div>

                        <div class="ty-product-filters" id="content_period">
                            <?php echo $_smarty_tpl->getSubTemplate ("common/period_selector.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('period'=>$_smarty_tpl->tpl_vars['search']->value['period'],'form_name'=>"orders_search_form"), 0);?>

                        </div>
                    </div>


                    <div class="buttons-container ty-search-form__buttons-container" style="background: initial;">
                        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_meta'=>"ty-btn__secondary ty-float-right",'but_text'=>$_smarty_tpl->__("search"),'but_name'=>"dispatch[orders.search]"), 0);?>

                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="span12">
        <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['search']->value['sort_order']=="asc") {?>
        <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"ty-icon-down-dir\"></i>", null, 0);?>
        <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"ty-icon-up-dir\"></i>", null, 0);?>
        <?php }?>
        <?php if (!$_smarty_tpl->tpl_vars['config']->value['tweaks']['disable_dhtml']) {?>
            <?php $_smarty_tpl->tpl_vars["ajax_class"] = new Smarty_variable("cm-ajax", null, 0);?>

        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            
                <?php if ($_smarty_tpl->tpl_vars['search']->value&&$_smarty_tpl->tpl_vars['search']->value['total_items']) {?>
                    <?php $_smarty_tpl->_capture_stack[0][] = array("ec_category_product_count", "ec_title_extra", null); ob_start(); ?>
                        <div class="ec_category_product_count"><?php echo $_smarty_tpl->__("ec_category_product_count",array("[count]"=>$_smarty_tpl->tpl_vars['search']->value['total_items']));?>
</div>
                    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                <?php } else { ?>
                    <?php $_smarty_tpl->_capture_stack[0][] = array("ec_category_product_count", "ec_title_extra", null); ob_start(); ?>
                        <div class="ec_category_product_count"><?php echo $_smarty_tpl->__("ec_category_product_count",array("[count]"=>0));?>
</div>
                    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/sorting.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('avail_sorting'=>false), 0);?>

            

            <?php  $_smarty_tpl->tpl_vars["o"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["o"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['orders']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_orders"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars["o"]->key => $_smarty_tpl->tpl_vars["o"]->value) {
$_smarty_tpl->tpl_vars["o"]->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_orders"]['iteration']++;
?>

                <div class="ec_order_accordian_div">
                    <div id="sw_content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
" class="ty-order__switch cm-combination-filter_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value['order_id'], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->getVariable('smarty')->value['foreach']['ec_orders']['iteration']==1) {?> open<?php }?> cm-save-state">
                        <div class="order_header">
                            <div class="ec_left_content">
                                <span class="ty-product-filters__title"><a href="<?php echo htmlspecialchars(fn_url("orders.details?order_id=".((string)$_smarty_tpl->tpl_vars['o']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
" style="font-size: 16px; font-weight: bold; display: block; line-height: 1;color: black;"><?php echo $_smarty_tpl->__("order");?>
 #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
</a></span>
                                <span class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['o']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
                                <span class="ec_order_status ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['o']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                                    <span><?php echo $_smarty_tpl->__("status");?>
:</span><span><?php echo $_smarty_tpl->getSubTemplate ("common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['o']->value['status'],'display'=>"view"), 0);?>
</span>
                                </span>
                            </div>
                            <div class="ec_right_content">
                                <div>
                                    <a href="<?php echo htmlspecialchars(fn_url("orders.details?order_id=".((string)$_smarty_tpl->tpl_vars['o']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                        <span class="hidden-phone"><?php echo $_smarty_tpl->__("ec_view_order_details");?>
</span>
                                        <span class="ec-icon-order-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>  
                                    </a>  
                                </div>
                            </div>
                        
                        </div>
                        <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                        <i class="ec_icon_switch_minus ty-icon-up-open"></i>
                    </div>

                    <div class="ty-product-filters <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['ec_orders']['iteration']!=1) {?>hidden<?php }?>" id="content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['o']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
">
                        <?php $_smarty_tpl->tpl_vars['o_info'] = new Smarty_variable(fn_get_order_info($_smarty_tpl->tpl_vars['o']->value['order_id']), null, 0);?>
                        <div class="ec_order_content_wrapper">
                            <div class="span12">
                                <div id="cart_items">
                                    <?php  $_smarty_tpl->tpl_vars["cart_products"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["cart_products"]->_loop = false;
 $_smarty_tpl->tpl_vars["k"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['o_info']->value['product_groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["cart_products"]->key => $_smarty_tpl->tpl_vars["cart_products"]->value) {
$_smarty_tpl->tpl_vars["cart_products"]->_loop = true;
 $_smarty_tpl->tpl_vars["k"]->value = $_smarty_tpl->tpl_vars["cart_products"]->key;
?>
                                        <div class="ec_order_table_container">
                                            <table class="ty-cart-content ty-table">
                                                <tbody>
                                                    <?php if ($_smarty_tpl->tpl_vars['cart_products']->value['products']) {?>
                                                        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                                                            <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                                                            <?php $_smarty_tpl->tpl_vars['total_order_count'] = new Smarty_variable($_smarty_tpl->tpl_vars['total_order_count']->value+$_smarty_tpl->tpl_vars['product']->value['amount'], null, 0);?>

                                                            <tr>
                                                                <td class="ec_reorder_div ec_middle" width="1%">
                                                                    <a href="<?php echo htmlspecialchars(fn_url("orders.reorder?order_id=".((string)$_smarty_tpl->tpl_vars['o_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("re_order");?>
"><i class="ec-icon-reorder-1"></i></a>
                                                                </td>
                                                                <td class="ty-cart-content__image-block" width="15%">
                                                                    <div class="ty-cart-content__image cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="product_image_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                                                                        <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                                                            <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('obj_id'=>$_smarty_tpl->tpl_vars['key']->value,'images'=>$_smarty_tpl->tpl_vars['product']->value['main_pair'],'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_height']), 0);?>

                                                                        </a>
                                                                    </div>
                                                                </td>

                                                                <td class="ty-cart-content__description" style="width: 30%;">
                                                                    <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" class="ty-cart-content__product-title"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],35);?>
</a>
                                                                    <div class="ec_cart_seller"><?php echo $_smarty_tpl->__("by");?>
 <a href="<?php echo htmlspecialchars(fn_url("companies.view?company_id=".((string)$_smarty_tpl->tpl_vars['product']->value['company_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');?>
</a></div>
                                                                    
                                                                    <?php if ($_smarty_tpl->tpl_vars['addons']->value['wishlist']['status']=='A') {?>
                                                                        <?php $_smarty_tpl->tpl_vars['ec_wishlist'] = new Smarty_variable(array(), null, 0);?>
                                                                        <?php if (!empty($_SESSION['wishlist']['products'])) {?>
                                                                            <?php $_smarty_tpl->tpl_vars['ec_wishlist'] = new Smarty_variable(fn_array_value_to_key($_SESSION['wishlist']['products'],'product_id'), null, 0);?>
                                                                        <?php }?>
                                                                        <?php if (array_key_exists($_smarty_tpl->tpl_vars['product']->value['product_id'],$_smarty_tpl->tpl_vars['ec_wishlist']->value)) {?>
                                                                            <?php $_smarty_tpl->tpl_vars['selected'] = new Smarty_variable('added', null, 0);?>
                                                                        <?php } else { ?>
                                                                            <?php $_smarty_tpl->tpl_vars['selected'] = new Smarty_variable('', null, 0);?>
                                                                        <?php }?>
                                                                        <div class="ec_save_later">
                                                                            <a class="cm-post cm-ajax1 cm-ajax-full-render ec_save_for_latter <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected']->value, ENT_QUOTES, 'UTF-8');?>
"  href="<?php echo htmlspecialchars(fn_url("wishlist.add..".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."&product_data[".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."][product_id]=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><i class="ec-icon-wishlist"></i><span><?php echo $_smarty_tpl->__("ec_add_to_wishlist");?>
</span></a>
                                                                        </div>
                                                                    <?php }?>
                                                                </td>

                                                                <td class="ec_order_search_div ec_middle hidden-phone" width="25%">
                                                                    <a href="<?php echo htmlspecialchars(fn_url("products.search?search_performed=Y&order_ids=".((string)$_smarty_tpl->tpl_vars['o_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                                                        <span><?php echo $_smarty_tpl->__("ec_search_product");?>
</span>
                                                                        <span class="ec-icon-find-item-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                                                                    </a>
                                                                </td>

                                                                <td class="ec_cart_amount_div ec_middle">
                                                                    <span><?php echo $_smarty_tpl->__("ec_qty");?>
</span> <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</span>
                                                                </td>

                                                                <td class="ec_order_product_price ec_middle">
                                                                    <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['price'],'span_id'=>"product_subtotal_".((string)$_smarty_tpl->tpl_vars['key']->value),'class'=>"price"), 0);?>

                                                                </td>
                                                            </tr>

                                                            <tr class="ec_break"><td colspan="6" class="ty-cart-content__product-elem"></td></tr>

                                                        <?php } ?>
                                                    <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
if (!$_smarty_tpl->tpl_vars["o"]->_loop) {
?>
            <style>
            .tygh-content > div {
                background-color: #ffffff;
            }
            #tygh_main_container{
                background-color: #ffffff;
            }
            </style>
             <div class="ec-container-cart ec_empty_cart ec_empty_wishlist">
                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/empty_order_list.svg" alt="<?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
" class="ec-empty-cart-image">
            </div>
            <?php } ?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("orders");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php if (@constant('EC_MOBILE_APP')) {?>
    <?php echo '<script'; ?>
>
        $(document).on('input', function() {
            $('[name="dispatch[orders.search]"]').click()
        });
    <?php echo '</script'; ?>
>
<?php }
}?><?php }} ?>
