<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 00:18:12
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_emr/views/ec_doctor/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:528675606092fd84c0fba0-38682469%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '747bf3bd5734faa30297c5c809f5a88d5733d5ad' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_emr/views/ec_doctor/login.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '528675606092fd84c0fba0-38682469',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092fd84c3ee16_12381730',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092fd84c3ee16_12381730')) {function content_6092fd84c3ee16_12381730($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('sign_in_to_your_doctor_account','email','password','ec_emr.remember_me','login','sign_in_to_your_doctor_account','email','password','ec_emr.remember_me','login'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>
<div class="wrapper">
    <div class="auth-content">
        <div class="card">
            <div class="card-body text-center">
                <div class="mb-4">
                    <img class="brand" src="https://www.dwaae.com/images/logos/15/New_Logo_2x.png" alt="bootstraper logo" style="width: 100px;">
                </div>
                <h6 class="mb-4 text-muted"><?php echo $_smarty_tpl->__("sign_in_to_your_doctor_account");?>
</h6>
                
                <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
                    <div class="form-group">
                        <input name="username" type="email" class="form-control" placeholder="<?php echo $_smarty_tpl->__("email");?>
" required>
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="<?php echo $_smarty_tpl->__("password");?>
" required>
                    </div>
                    <div class="form-group text-left">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="remember" class="custom-control-input" id="remember-me">
                            <label class="custom-control-label" for="remember-me"><?php echo $_smarty_tpl->__("ec_emr.remember_me");?>
</label>
                        </div>
                    </div>
                    <button class="btn btn-primary shadow-2 mb-4" type="submit" name="dispatch[ec_doctor.login]"><?php echo $_smarty_tpl->__("login");?>
</button>
                </form>
                
                
            </div>
        </div>
    </div>
</div>

<?php echo smarty_function_script(array('src'=>"js/addons/ec_emr/bootstrap.min.js"),$_smarty_tpl);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_emr/views/ec_doctor/login.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_emr/views/ec_doctor/login.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>
<div class="wrapper">
    <div class="auth-content">
        <div class="card">
            <div class="card-body text-center">
                <div class="mb-4">
                    <img class="brand" src="https://www.dwaae.com/images/logos/15/New_Logo_2x.png" alt="bootstraper logo" style="width: 100px;">
                </div>
                <h6 class="mb-4 text-muted"><?php echo $_smarty_tpl->__("sign_in_to_your_doctor_account");?>
</h6>
                
                <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
                    <div class="form-group">
                        <input name="username" type="email" class="form-control" placeholder="<?php echo $_smarty_tpl->__("email");?>
" required>
                    </div>
                    <div class="form-group">
                        <input name="password" type="password" class="form-control" placeholder="<?php echo $_smarty_tpl->__("password");?>
" required>
                    </div>
                    <div class="form-group text-left">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="remember" class="custom-control-input" id="remember-me">
                            <label class="custom-control-label" for="remember-me"><?php echo $_smarty_tpl->__("ec_emr.remember_me");?>
</label>
                        </div>
                    </div>
                    <button class="btn btn-primary shadow-2 mb-4" type="submit" name="dispatch[ec_doctor.login]"><?php echo $_smarty_tpl->__("login");?>
</button>
                </form>
                
                
            </div>
        </div>
    </div>
</div>

<?php echo smarty_function_script(array('src'=>"js/addons/ec_emr/bootstrap.min.js"),$_smarty_tpl);
}?><?php }} ?>
