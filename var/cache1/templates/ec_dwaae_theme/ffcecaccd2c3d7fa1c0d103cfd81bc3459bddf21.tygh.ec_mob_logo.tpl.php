<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:01
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_mob_logo.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17634412676092b8cd56d833-73164310%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ffcecaccd2c3d7fa1c0d103cfd81bc3459bddf21' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_mob_logo.tpl',
      1 => 1608910847,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '17634412676092b8cd56d833-73164310',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'logo_link' => 0,
    'logos' => 0,
    'config' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8cd581079_53109101',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8cd581079_53109101')) {function content_6092b8cd581079_53109101($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ty-logo-container">
    <?php $_smarty_tpl->tpl_vars['logo_link'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['logo_link']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
    <?php if (!($_smarty_tpl->tpl_vars['runtime']->value['controller']=='index'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='index')) {?>
    <span class="ec_go_back"><svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg" style="
">
        <path d="M18.8758 6.58846L18.907 6.59461H5.52391L9.73106 2.79983C9.93708 2.61456 10.0501 2.36359 10.0501 2.10017C10.0501 1.83676 9.93708 1.58754 9.73106 1.40183L9.07643 0.812365C8.87057 0.627097 8.59626 0.524658 8.30374 0.524658C8.01106 0.524658 7.73659 0.626366 7.53074 0.811634L0.318866 7.30173C0.112199 7.48773 -0.000808652 7.73549 4.35632e-06 7.99905C-0.000808652 8.26407 0.112199 8.51198 0.318866 8.69768L7.53074 15.1884C7.73659 15.3735 8.0109 15.4753 8.30374 15.4753C8.59626 15.4753 8.87057 15.3733 9.07643 15.1884L9.73106 14.5989C9.93708 14.4139 10.0501 14.1669 10.0501 13.9035C10.0501 13.6402 9.93708 13.4062 9.73106 13.2211L5.47643 9.4051H18.8907C19.4935 9.4051 20 8.93754 20 8.39534V7.56163C20 7.01944 19.4785 6.58846 18.8758 6.58846Z" fill="black"></path>
        </svg>
    </span>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['logo_link']->value) {?>
        <a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logos']->value['theme']['image']['alt'], ENT_QUOTES, 'UTF-8');?>
">
    <?php }?>
    
    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/logo_mob.svg" class="ty-logo-container__image mob_logo">
    
    <?php if ($_smarty_tpl->tpl_vars['logo_link']->value) {?>
        </a>
    <?php }?>
</div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/static_templates/ec_mob_logo.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/static_templates/ec_mob_logo.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ty-logo-container">
    <?php $_smarty_tpl->tpl_vars['logo_link'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['logo_link']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
    <?php if (!($_smarty_tpl->tpl_vars['runtime']->value['controller']=='index'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='index')) {?>
    <span class="ec_go_back"><svg width="20" height="16" viewBox="0 0 20 16" fill="none" xmlns="http://www.w3.org/2000/svg" style="
">
        <path d="M18.8758 6.58846L18.907 6.59461H5.52391L9.73106 2.79983C9.93708 2.61456 10.0501 2.36359 10.0501 2.10017C10.0501 1.83676 9.93708 1.58754 9.73106 1.40183L9.07643 0.812365C8.87057 0.627097 8.59626 0.524658 8.30374 0.524658C8.01106 0.524658 7.73659 0.626366 7.53074 0.811634L0.318866 7.30173C0.112199 7.48773 -0.000808652 7.73549 4.35632e-06 7.99905C-0.000808652 8.26407 0.112199 8.51198 0.318866 8.69768L7.53074 15.1884C7.73659 15.3735 8.0109 15.4753 8.30374 15.4753C8.59626 15.4753 8.87057 15.3733 9.07643 15.1884L9.73106 14.5989C9.93708 14.4139 10.0501 14.1669 10.0501 13.9035C10.0501 13.6402 9.93708 13.4062 9.73106 13.2211L5.47643 9.4051H18.8907C19.4935 9.4051 20 8.93754 20 8.39534V7.56163C20 7.01944 19.4785 6.58846 18.8758 6.58846Z" fill="black"></path>
        </svg>
    </span>
    <?php }?>
    <?php if ($_smarty_tpl->tpl_vars['logo_link']->value) {?>
        <a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logos']->value['theme']['image']['alt'], ENT_QUOTES, 'UTF-8');?>
">
    <?php }?>
    
    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/logo_mob.svg" class="ty-logo-container__image mob_logo">
    
    <?php if ($_smarty_tpl->tpl_vars['logo_link']->value) {?>
        </a>
    <?php }?>
</div>
<?php }?><?php }} ?>
