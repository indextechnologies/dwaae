<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:03
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/products/ec_best_seller_grid.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9413711686092b8cfb03933-01977086%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90fd8ea063bc14ba1f49cb461f60f574ee112474' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/products/ec_best_seller_grid.tpl',
      1 => 1606204612,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '9413711686092b8cfb03933-01977086',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'items' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8cfb13653_24634552',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8cfb13653_24634552')) {function content_6092b8cfb13653_24634552($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_top_seller','ec_see_more','products','ec_top_seller','ec_see_more','products'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <div class="ec_best_seller_container">
        <div class="ec_seller_left_div">
            <div class="ec_best_seller_products">
                <?php echo $_smarty_tpl->getSubTemplate ("blocks/products/ec_product_short_grid.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
            <div class="ec_top_seller_header">
                <span>
                    <div class="ec_header"><?php echo $_smarty_tpl->__("ec_top_seller");?>
</div>
                    <a href="<?php echo htmlspecialchars(fn_url("products.bestsellers"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__secondary"><?php echo $_smarty_tpl->__("ec_see_more");?>
</a>
                </span>
                <i class="ec-icon-top-seller-big"></i>
            </div>
        </div>
        <div class="ec_seller_right_div">
            <div>
                <div class="icon_div"><i class="ec-icon-top-seller-big"></i></div>
                <span></span>
                <div class="ec_header"><?php echo $_smarty_tpl->__("products");?>
</div>
                <div class="ec_product_count"><?php echo htmlspecialchars(fn_ec_get_best_seller_count(), ENT_QUOTES, 'UTF-8');?>
</div>
            </div>
        </div>
    </div>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/products/ec_best_seller_grid.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/products/ec_best_seller_grid.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <div class="ec_best_seller_container">
        <div class="ec_seller_left_div">
            <div class="ec_best_seller_products">
                <?php echo $_smarty_tpl->getSubTemplate ("blocks/products/ec_product_short_grid.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
            <div class="ec_top_seller_header">
                <span>
                    <div class="ec_header"><?php echo $_smarty_tpl->__("ec_top_seller");?>
</div>
                    <a href="<?php echo htmlspecialchars(fn_url("products.bestsellers"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__secondary"><?php echo $_smarty_tpl->__("ec_see_more");?>
</a>
                </span>
                <i class="ec-icon-top-seller-big"></i>
            </div>
        </div>
        <div class="ec_seller_right_div">
            <div>
                <div class="icon_div"><i class="ec-icon-top-seller-big"></i></div>
                <span></span>
                <div class="ec_header"><?php echo $_smarty_tpl->__("products");?>
</div>
                <div class="ec_product_count"><?php echo htmlspecialchars(fn_ec_get_best_seller_count(), ENT_QUOTES, 'UTF-8');?>
</div>
            </div>
        </div>
    </div>
<?php }
}?><?php }} ?>
