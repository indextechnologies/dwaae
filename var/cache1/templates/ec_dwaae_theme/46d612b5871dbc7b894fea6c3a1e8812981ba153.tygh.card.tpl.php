<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 10:18:43
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/insurance_cards/components/card.tpl" */ ?>
<?php /*%%SmartyHeaderCode:117573132760962d43bb32d1-87319207%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '46d612b5871dbc7b894fea6c3a1e8812981ba153' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/insurance_cards/components/card.tpl',
      1 => 1607518820,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '117573132760962d43bb32d1-87319207',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'insurance_redirect_url' => 0,
    'insurance_result_ids' => 0,
    'insurance_edit_page' => 0,
    'enable_insurance_select' => 0,
    'insurance_card_selected' => 0,
    'card_view_only' => 0,
    'insurance_card' => 0,
    'result_ids' => 0,
    'redirect_url' => 0,
    'curl' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60962d43bdec46_16713025',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60962d43bdec46_16713025')) {function content_60962d43bdec46_16713025($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('edit','front_side','back_side','edit','front_side','back_side'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?> <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_redirect_url']->value)===null||$tmp==='' ? "insurance_cards.manage" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['result_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_result_ids']->value)===null||$tmp==='' ? "ec_insurance_upload_block" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['insurance_edit_page'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_edit_page']->value)===null||$tmp==='' ? "insurance_cards.update" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['enable_insurance_select'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['enable_insurance_select']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['insurance_card_selected'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_card_selected']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['card_view_only'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['card_view_only']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <div class="ec_emirates_id_card">
    <label class="labl">
        <?php if ($_smarty_tpl->tpl_vars['enable_insurance_select']->value) {?>
        <input type="radio" name="selected_insurance" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['insurance_card_selected']->value) {?>checked="checked"<?php }?>/>
        <?php }?>
        <div>
            <div class="ec_header_row">
                <div class="ec_left_content">
                    <i class="ec-icon-union"></i>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_name'], ENT_QUOTES, 'UTF-8');?>
</span>
                </div>
                <?php if (!$_smarty_tpl->tpl_vars['card_view_only']->value) {?>
                <div class="ec_right_content">
                    <div class="">
                        <a href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['insurance_edit_page']->value)."&insurance_profile_id=".((string)$_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_id'])), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax cm-ajax-full-render edit_address stop-cm-required" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
">
                            <span><?php echo $_smarty_tpl->__("edit");?>
</span>
                            <i class="ec-icon-edit"></i>
                        </a>
                    </div>
                    
                    <div>
                        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['redirect_url']->value), null, 0);?>
                        <a href="<?php echo htmlspecialchars(fn_url("insurance_cards.delete&insurance_profile_id=".((string)$_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_id'])."?redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value)), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-post cm-ajax cm-ajax-full-render ec_delete_div">
                            <i class="ec-icon-delete"></i>
                        </a>
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="ec_card_content">
                <div class="d-upload-form-block">
                    <div>
                        <div class="ty-control-group">
                            <?php if ($_smarty_tpl->tpl_vars['insurance_card']->value['front_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['insurance_card']->value['front_side_path'],true), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>
                            <div><?php echo $_smarty_tpl->__("front_side");?>
</div>
                        </div>
                    </div>
                    <div>
                        <div class="ty-control-group">
                            <?php if ($_smarty_tpl->tpl_vars['insurance_card']->value['back_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['insurance_card']->value['back_side_path'],true), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>
                            <div><?php echo $_smarty_tpl->__("back_side");?>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </label>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_customer/views/insurance_cards/components/card.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_customer/views/insurance_cards/components/card.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?> <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_redirect_url']->value)===null||$tmp==='' ? "insurance_cards.manage" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['result_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_result_ids']->value)===null||$tmp==='' ? "ec_insurance_upload_block" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['insurance_edit_page'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_edit_page']->value)===null||$tmp==='' ? "insurance_cards.update" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['enable_insurance_select'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['enable_insurance_select']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['insurance_card_selected'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_card_selected']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['card_view_only'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['card_view_only']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <div class="ec_emirates_id_card">
    <label class="labl">
        <?php if ($_smarty_tpl->tpl_vars['enable_insurance_select']->value) {?>
        <input type="radio" name="selected_insurance" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['insurance_card_selected']->value) {?>checked="checked"<?php }?>/>
        <?php }?>
        <div>
            <div class="ec_header_row">
                <div class="ec_left_content">
                    <i class="ec-icon-union"></i>
                    <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_name'], ENT_QUOTES, 'UTF-8');?>
</span>
                </div>
                <?php if (!$_smarty_tpl->tpl_vars['card_view_only']->value) {?>
                <div class="ec_right_content">
                    <div class="">
                        <a href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['insurance_edit_page']->value)."&insurance_profile_id=".((string)$_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_id'])), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax cm-ajax-full-render edit_address stop-cm-required" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
">
                            <span><?php echo $_smarty_tpl->__("edit");?>
</span>
                            <i class="ec-icon-edit"></i>
                        </a>
                    </div>
                    
                    <div>
                        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['redirect_url']->value), null, 0);?>
                        <a href="<?php echo htmlspecialchars(fn_url("insurance_cards.delete&insurance_profile_id=".((string)$_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_id'])."?redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value)), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-post cm-ajax cm-ajax-full-render ec_delete_div">
                            <i class="ec-icon-delete"></i>
                        </a>
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="ec_card_content">
                <div class="d-upload-form-block">
                    <div>
                        <div class="ty-control-group">
                            <?php if ($_smarty_tpl->tpl_vars['insurance_card']->value['front_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['insurance_card']->value['front_side_path'],true), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>
                            <div><?php echo $_smarty_tpl->__("front_side");?>
</div>
                        </div>
                    </div>
                    <div>
                        <div class="ty-control-group">
                            <?php if ($_smarty_tpl->tpl_vars['insurance_card']->value['back_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['insurance_card']->value['back_side_path'],true), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>
                            <div><?php echo $_smarty_tpl->__("back_side");?>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </label>
</div><?php }?><?php }} ?>
