<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 22:21:38
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15208048076092e232708b89-71505903%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9ca8f51b2e7fcc388d2a6905d156ba9fc07d5fd1' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/manage.tpl',
      1 => 1612694625,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '15208048076092e232708b89-71505903',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'config' => 0,
    'search' => 0,
    'quotations' => 0,
    'q' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092e23276a136_18581191',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092e23276a136_18581191')) {function content_6092e23276a136_18581191($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_quotations','h_rfq.rfq_id','status','ec_view_order_details','ec_your_wishlist_empty','h_rfq.quotations','ec_quotations','h_rfq.rfq_id','status','ec_view_order_details','ec_your_wishlist_empty','h_rfq.quotations'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_order_search_container ec_quotation">
    <div class="span12">
        <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['search']->value['sort_order']=="asc") {?>
        <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"ty-icon-down-dir\"></i>", null, 0);?>
        <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"ty-icon-up-dir\"></i>", null, 0);?>
        <?php }?>
        <?php if (!$_smarty_tpl->tpl_vars['config']->value['tweaks']['disable_dhtml']) {?>
            <?php $_smarty_tpl->tpl_vars["ajax_class"] = new Smarty_variable("cm-ajax", null, 0);?>

        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            

            <div class="ec_quotation_top hidden">
                <span class="ec_left">
                    <span class="ec-icon-quotation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span>
                    
                    <span class="header_text"><?php echo $_smarty_tpl->__("ec_quotations");?>
</span>
                </span>
            </div>

            <?php  $_smarty_tpl->tpl_vars["q"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["q"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['quotations']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["q"]->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_orders"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars["q"]->key => $_smarty_tpl->tpl_vars["q"]->value) {
$_smarty_tpl->tpl_vars["q"]->_loop = true;
 $_smarty_tpl->tpl_vars["q"]->index++;
 $_smarty_tpl->tpl_vars["q"]->first = $_smarty_tpl->tpl_vars["q"]->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_orders"]['iteration']++;
?>

                <div class="ec_order_accordian_div" <?php if ($_smarty_tpl->tpl_vars['q']->first&&!@constant('EC_MOBILE_APP')) {?>style="margin-top: 15px;"<?php }?>>
                    <div id="sw_content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" class="ty-order__switch cm-combination-filter_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
">
                        <div class="order_header">
                            <div class="ec_left_content">
                                <span class="ty-product-filters__title"><?php echo $_smarty_tpl->__("h_rfq.rfq_id");?>
 #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
</span>
                                <span class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['q']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
                                <span class="ec_order_status ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['q']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                                    <span><?php echo $_smarty_tpl->__("status");?>
:</span><span><?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['q']->value['status'],'display'=>"view",'name'=>"update_order[status]"), 0);?>
</span>
                                </span>
                            </div>
                            <div class="ec_right_content">
                                <div>
                                    <a href="<?php echo htmlspecialchars(fn_url("h_rfq.details?rfq_id=".((string)$_smarty_tpl->tpl_vars['q']->value['rfq_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                        <span><?php echo $_smarty_tpl->__("ec_view_order_details");?>
</span>
                                        <span class="ec-icon-order-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>  
                                    </a>  
                                </div>
                            </div>
                        
                        </div>
                        
                    </div>

                    
                </div>
            <?php }
if (!$_smarty_tpl->tpl_vars["q"]->_loop) {
?>
            <div class="ec-container-cart ec_empty_cart ec_empty_wishlist">
                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/empty_order_list.svg" alt="<?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
" class="ec-empty-cart-image">
            
            </div>
            <?php } ?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("h_rfq.quotations");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/h_rfq/views/h_rfq/manage.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/h_rfq/views/h_rfq/manage.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_order_search_container ec_quotation">
    <div class="span12">
        <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['search']->value['sort_order']=="asc") {?>
        <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"ty-icon-down-dir\"></i>", null, 0);?>
        <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"ty-icon-up-dir\"></i>", null, 0);?>
        <?php }?>
        <?php if (!$_smarty_tpl->tpl_vars['config']->value['tweaks']['disable_dhtml']) {?>
            <?php $_smarty_tpl->tpl_vars["ajax_class"] = new Smarty_variable("cm-ajax", null, 0);?>

        <?php }?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            

            <div class="ec_quotation_top hidden">
                <span class="ec_left">
                    <span class="ec-icon-quotation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span>
                    
                    <span class="header_text"><?php echo $_smarty_tpl->__("ec_quotations");?>
</span>
                </span>
            </div>

            <?php  $_smarty_tpl->tpl_vars["q"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["q"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['quotations']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["q"]->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_orders"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars["q"]->key => $_smarty_tpl->tpl_vars["q"]->value) {
$_smarty_tpl->tpl_vars["q"]->_loop = true;
 $_smarty_tpl->tpl_vars["q"]->index++;
 $_smarty_tpl->tpl_vars["q"]->first = $_smarty_tpl->tpl_vars["q"]->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_orders"]['iteration']++;
?>

                <div class="ec_order_accordian_div" <?php if ($_smarty_tpl->tpl_vars['q']->first&&!@constant('EC_MOBILE_APP')) {?>style="margin-top: 15px;"<?php }?>>
                    <div id="sw_content_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" class="ty-order__switch cm-combination-filter_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
">
                        <div class="order_header">
                            <div class="ec_left_content">
                                <span class="ty-product-filters__title"><?php echo $_smarty_tpl->__("h_rfq.rfq_id");?>
 #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
</span>
                                <span class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['q']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
                                <span class="ec_order_status ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['q']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                                    <span><?php echo $_smarty_tpl->__("status");?>
:</span><span><?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['q']->value['status'],'display'=>"view",'name'=>"update_order[status]"), 0);?>
</span>
                                </span>
                            </div>
                            <div class="ec_right_content">
                                <div>
                                    <a href="<?php echo htmlspecialchars(fn_url("h_rfq.details?rfq_id=".((string)$_smarty_tpl->tpl_vars['q']->value['rfq_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                        <span><?php echo $_smarty_tpl->__("ec_view_order_details");?>
</span>
                                        <span class="ec-icon-order-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>  
                                    </a>  
                                </div>
                            </div>
                        
                        </div>
                        
                    </div>

                    
                </div>
            <?php }
if (!$_smarty_tpl->tpl_vars["q"]->_loop) {
?>
            <div class="ec-container-cart ec_empty_cart ec_empty_wishlist">
                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/empty_order_list.svg" alt="<?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
" class="ec-empty-cart-image">
            
            </div>
            <?php } ?>

        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("h_rfq.quotations");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?><?php }} ?>
