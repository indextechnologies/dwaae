<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:03
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_share_bottom.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10716819086092b8cfde9d83-16612756%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08c29088c451cc252defeb80278c6cabd116a10b' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_share_bottom.tpl',
      1 => 1618387061,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '10716819086092b8cfde9d83-16612756',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8cfdf9035_79201703',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8cfdf9035_79201703')) {function content_6092b8cfdf9035_79201703($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('add_your_pharmacy_in_dwaae_network','ec_register_pharmacy','contact_us','we_are_available_on','ios_app','android_app','add_your_pharmacy_in_dwaae_network','ec_register_pharmacy','contact_us','we_are_available_on','ios_app','android_app'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_footer_bottom_share">
    <div class="ec_bottom_share">
        
        <div class="ec_share_left_div hidden-phone">
            <div class="ec_header"><?php echo $_smarty_tpl->__("add_your_pharmacy_in_dwaae_network");?>
</div>
            <div><a href="<?php echo htmlspecialchars(fn_url("companies.apply_for_vendor"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__primary" style="border-radius: 25px;"><?php echo $_smarty_tpl->__("ec_register_pharmacy");?>
</a></div>
        </div>
        
        <div class="ec_share_center_div hidden-phone">
            <div class="ec_header"><?php echo $_smarty_tpl->__("contact_us");?>
</div>
            <div class="ec_desc_text">
                Musaffah M15, Street 11 opposite GTA Car Care
                <br>Abu Dhabi, UAE
            </div>
            <div class="email_footer"><i class="ec-icon-email-c"></i><span>care@dwaae.com</span></div>
        </div>
        <div class="ec_share_right_div">
            <div class="ec_header"><?php echo $_smarty_tpl->__("we_are_available_on");?>
</div>
            <div class="moduleRow">
            <div class=" appLinksModule">
                <div class="badgesContainer"><a target="_blank" href="https://apps.apple.com/in/app/dwaae-%D8%AF%D9%88%D8%A7%D8%A6%D9%8A/id1522286328" rel="noopener noreferrer" aria-label="Visit the AppStore (opens in a new window)" class=" ios"><?php echo $_smarty_tpl->__("ios_app");?>
</a><a target="_blank" href="https://play.google.com/store/apps/details?id=com.dwaae.smartapp" rel="noopener noreferrer" aria-label="Visit the PlayStore (opens in a new window)" class=" android"><?php echo $_smarty_tpl->__("android_app");?>
</a></div>
            </div>
        </div>
        </div>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/static_templates/ec_share_bottom.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/static_templates/ec_share_bottom.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_footer_bottom_share">
    <div class="ec_bottom_share">
        
        <div class="ec_share_left_div hidden-phone">
            <div class="ec_header"><?php echo $_smarty_tpl->__("add_your_pharmacy_in_dwaae_network");?>
</div>
            <div><a href="<?php echo htmlspecialchars(fn_url("companies.apply_for_vendor"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__primary" style="border-radius: 25px;"><?php echo $_smarty_tpl->__("ec_register_pharmacy");?>
</a></div>
        </div>
        
        <div class="ec_share_center_div hidden-phone">
            <div class="ec_header"><?php echo $_smarty_tpl->__("contact_us");?>
</div>
            <div class="ec_desc_text">
                Musaffah M15, Street 11 opposite GTA Car Care
                <br>Abu Dhabi, UAE
            </div>
            <div class="email_footer"><i class="ec-icon-email-c"></i><span>care@dwaae.com</span></div>
        </div>
        <div class="ec_share_right_div">
            <div class="ec_header"><?php echo $_smarty_tpl->__("we_are_available_on");?>
</div>
            <div class="moduleRow">
            <div class=" appLinksModule">
                <div class="badgesContainer"><a target="_blank" href="https://apps.apple.com/in/app/dwaae-%D8%AF%D9%88%D8%A7%D8%A6%D9%8A/id1522286328" rel="noopener noreferrer" aria-label="Visit the AppStore (opens in a new window)" class=" ios"><?php echo $_smarty_tpl->__("ios_app");?>
</a><a target="_blank" href="https://play.google.com/store/apps/details?id=com.dwaae.smartapp" rel="noopener noreferrer" aria-label="Visit the PlayStore (opens in a new window)" class=" android"><?php echo $_smarty_tpl->__("android_app");?>
</a></div>
            </div>
        </div>
        </div>
    </div>
</div><?php }?><?php }} ?>
