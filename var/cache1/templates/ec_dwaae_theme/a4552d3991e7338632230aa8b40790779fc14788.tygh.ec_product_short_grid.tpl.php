<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:02
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/products/ec_product_short_grid.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18019478326092b8ce8ca140-46636032%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a4552d3991e7338632230aa8b40790779fc14788' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/products/ec_product_short_grid.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '18019478326092b8ce8ca140-46636032',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'items' => 0,
    'block' => 0,
    'product' => 0,
    'obj_prefix' => 0,
    'obj_id' => 0,
    'form_open' => 0,
    'obj_id_prefix' => 0,
    'product_labels' => 0,
    'old_price' => 0,
    'price' => 0,
    'clean_price' => 0,
    'list_discount' => 0,
    'redirect_url' => 0,
    'config' => 0,
    'settings' => 0,
    'addons' => 0,
    'form_close' => 0,
    'cat_id' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8ce920fb6_04929164',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8ce920fb6_04929164')) {function content_6092b8ce920fb6_04929164($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('by','add_to_comparison_list','add_to_wishlist','view_all_products','by','add_to_comparison_list','add_to_wishlist','view_all_products'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <?php $_smarty_tpl->tpl_vars['obj_prefix'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['block']->value['block_id'])."000", null, 0);?>
    <div class="ec_cat_list">
        <div class="ec_cat_grid">
            <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
                <div class="ec_inner_cat_block">
                    <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                    <?php $_smarty_tpl->tpl_vars["obj_id_prefix"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/product_data.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'show_name'=>true,'show_price'=>true,'show_add_to_cart'=>true,'show_list_buttons'=>true), 0);?>

                    
                    <?php $_smarty_tpl->tpl_vars["form_open"] = new Smarty_variable("form_open_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['form_open']->value];?>

                        <div class="ec_inner_cat_content">
                            <div class="ec_cat_list_image">
                                <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"50",'image_height'=>"50",'images'=>$_smarty_tpl->tpl_vars['product']->value['main_pair'],'obj_id'=>$_smarty_tpl->tpl_vars['obj_id_prefix']->value,'no_ids'=>true), 0);?>
</a>

                                <?php $_smarty_tpl->tpl_vars["product_labels"] = new Smarty_variable("product_labels_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_labels']->value];?>

                            </div>
                            <div class="ec_cat_right_div">  
                                <div class="ec_item_right_info">
                                    <div class="ec_cat_list_name">
                                        <a href="<?php echo htmlspecialchars(fn_url("products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
" class="product-title"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],23,"...",true);?>
</a>
                                    </div>

                                    <div class="ec_cat_list_price <?php if ($_smarty_tpl->tpl_vars['product']->value['price']==0) {?>ec_cat_list_no-price<?php }?>">
                                        <?php $_smarty_tpl->tpl_vars["old_price"] = new Smarty_variable("old_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])) {
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value];
}?>

                                        <?php $_smarty_tpl->tpl_vars["price"] = new Smarty_variable("price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['price']->value];?>


                                        <?php $_smarty_tpl->tpl_vars["clean_price"] = new Smarty_variable("clean_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value];?>


                                        <?php $_smarty_tpl->tpl_vars["list_discount"] = new Smarty_variable("list_discount_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value];?>

                                    </div>
                                    <div class="ec_seller_info">
                                        <span>
                                            <div class="ec_sold_by"><?php echo $_smarty_tpl->__("by");?>
</div>
                                            <div class="ec_seller_name"><?php echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');?>
</div>
                                        </span>
                                    </div>

                                    <a href="<?php echo htmlspecialchars(fn_url("products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" class="view_product_link"><i class="ec-icon-right-arrow"></i></a>
                                </div>
                                <div class="ec_right_btn_group">
                                    <?php $_smarty_tpl->tpl_vars['c_url'] = new Smarty_variable(rawurlencode((($tmp = @$_smarty_tpl->tpl_vars['redirect_url']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp)), null, 0);?>
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['enable_compare_products']=="Y") {?>
                                        <a href="<?php echo htmlspecialchars(fn_url("product_features.add_product?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['c_url']->value)), ENT_QUOTES, 'UTF-8');?>
"  title="<?php echo $_smarty_tpl->__("add_to_comparison_list");?>
" class="cm-ajax cm-ajax-full-render ec_list_compare" data-ca-target-id="comparision_list_icon"><i class="ec-icon-compare"></i></a>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['addons']->value['wishlist']['status']=='A') {?>
                                        

                                        <a class="cm-ajax cm-ajax-full-render ec_list_wishlist cm-submit" data-ca-dispatch="dispatch[wishlist.add..<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
]" title="<?php echo $_smarty_tpl->__("add_to_wishlist");?>
"><i class="ec-icon-wishlist"></i></a>
                                        
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    <?php $_smarty_tpl->tpl_vars["form_close"] = new Smarty_variable("form_close_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['form_close']->value];?>

                </div>
            <?php } ?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['block']->value['content']['items']['cid']) {?>
            <?php $_smarty_tpl->tpl_vars['cat_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['content']['items']['cid'], null, 0);?>
            <div class="ty-center explore_more_btn">
                <a href="<?php echo htmlspecialchars(fn_url("categories.view&category_id=".((string)$_smarty_tpl->tpl_vars['cat_id']->value)), ENT_QUOTES, 'UTF-8');?>
" class="ec_show_more_link"><?php echo $_smarty_tpl->__("view_all_products");?>
</a>
            </div>
        <?php }?>
    </div>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/products/ec_product_short_grid.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/products/ec_product_short_grid.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <?php $_smarty_tpl->tpl_vars['obj_prefix'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['block']->value['block_id'])."000", null, 0);?>
    <div class="ec_cat_list">
        <div class="ec_cat_grid">
            <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?>
                <div class="ec_inner_cat_block">
                    <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                    <?php $_smarty_tpl->tpl_vars["obj_id_prefix"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/product_data.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'show_name'=>true,'show_price'=>true,'show_add_to_cart'=>true,'show_list_buttons'=>true), 0);?>

                    
                    <?php $_smarty_tpl->tpl_vars["form_open"] = new Smarty_variable("form_open_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['form_open']->value];?>

                        <div class="ec_inner_cat_content">
                            <div class="ec_cat_list_image">
                                <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"50",'image_height'=>"50",'images'=>$_smarty_tpl->tpl_vars['product']->value['main_pair'],'obj_id'=>$_smarty_tpl->tpl_vars['obj_id_prefix']->value,'no_ids'=>true), 0);?>
</a>

                                <?php $_smarty_tpl->tpl_vars["product_labels"] = new Smarty_variable("product_labels_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_labels']->value];?>

                            </div>
                            <div class="ec_cat_right_div">  
                                <div class="ec_item_right_info">
                                    <div class="ec_cat_list_name">
                                        <a href="<?php echo htmlspecialchars(fn_url("products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
" class="product-title"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],23,"...",true);?>
</a>
                                    </div>

                                    <div class="ec_cat_list_price <?php if ($_smarty_tpl->tpl_vars['product']->value['price']==0) {?>ec_cat_list_no-price<?php }?>">
                                        <?php $_smarty_tpl->tpl_vars["old_price"] = new Smarty_variable("old_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])) {
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value];
}?>

                                        <?php $_smarty_tpl->tpl_vars["price"] = new Smarty_variable("price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['price']->value];?>


                                        <?php $_smarty_tpl->tpl_vars["clean_price"] = new Smarty_variable("clean_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value];?>


                                        <?php $_smarty_tpl->tpl_vars["list_discount"] = new Smarty_variable("list_discount_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value];?>

                                    </div>
                                    <div class="ec_seller_info">
                                        <span>
                                            <div class="ec_sold_by"><?php echo $_smarty_tpl->__("by");?>
</div>
                                            <div class="ec_seller_name"><?php echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');?>
</div>
                                        </span>
                                    </div>

                                    <a href="<?php echo htmlspecialchars(fn_url("products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" class="view_product_link"><i class="ec-icon-right-arrow"></i></a>
                                </div>
                                <div class="ec_right_btn_group">
                                    <?php $_smarty_tpl->tpl_vars['c_url'] = new Smarty_variable(rawurlencode((($tmp = @$_smarty_tpl->tpl_vars['redirect_url']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp)), null, 0);?>
                                    
                                    <?php if ($_smarty_tpl->tpl_vars['settings']->value['General']['enable_compare_products']=="Y") {?>
                                        <a href="<?php echo htmlspecialchars(fn_url("product_features.add_product?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['c_url']->value)), ENT_QUOTES, 'UTF-8');?>
"  title="<?php echo $_smarty_tpl->__("add_to_comparison_list");?>
" class="cm-ajax cm-ajax-full-render ec_list_compare" data-ca-target-id="comparision_list_icon"><i class="ec-icon-compare"></i></a>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['addons']->value['wishlist']['status']=='A') {?>
                                        

                                        <a class="cm-ajax cm-ajax-full-render ec_list_wishlist cm-submit" data-ca-dispatch="dispatch[wishlist.add..<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'UTF-8');?>
]" title="<?php echo $_smarty_tpl->__("add_to_wishlist");?>
"><i class="ec-icon-wishlist"></i></a>
                                        
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    <?php $_smarty_tpl->tpl_vars["form_close"] = new Smarty_variable("form_close_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['form_close']->value];?>

                </div>
            <?php } ?>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['block']->value['content']['items']['cid']) {?>
            <?php $_smarty_tpl->tpl_vars['cat_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['content']['items']['cid'], null, 0);?>
            <div class="ty-center explore_more_btn">
                <a href="<?php echo htmlspecialchars(fn_url("categories.view&category_id=".((string)$_smarty_tpl->tpl_vars['cat_id']->value)), ENT_QUOTES, 'UTF-8');?>
" class="ec_show_more_link"><?php echo $_smarty_tpl->__("view_all_products");?>
</a>
            </div>
        <?php }?>
    </div>
<?php }
}?><?php }} ?>
