<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 22:20:33
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_app_my_account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3530569916092e1f1a23f22-62279368%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f805c554445a2e02eafde7db791562ae477342d9' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_app_my_account.tpl',
      1 => 1607414765,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3530569916092e1f1a23f22-62279368',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'block' => 0,
    'sideviewer_id' => 0,
    'config' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092e1f1a5c9b3_66634203',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092e1f1a5c9b3_66634203')) {function content_6092e1f1a5c9b3_66634203($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_my_account','ec_track_order','orders','ec_prescriptions','ec_quotations','ec_track_order','track_my_order','track_my_order','order_id','email','go','ec_my_account','ec_track_order','orders','ec_prescriptions','ec_quotations','ec_track_order','track_my_order','track_my_order','order_id','email','go'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->_capture_stack[0][] = array("title", null, null); ob_start(); ?>
    <span></span>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->tpl_vars["sideviewer_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
<div class="ec_bottom_app_account header-left-block">
    <div class="fn-sideviewer">
    	<div class="fn-sideviewer__toggle ec_app_account_opener">
			<?php if (trim(Smarty::$_smarty_vars['capture']['title'])) {?>
				<?php echo Smarty::$_smarty_vars['capture']['title'];?>

			<?php }?>
		</div>
		<div id="sideviewer_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sideviewer_id']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" class="fn-sideviewer-wrapper">
			<div class="side-viewer-content">
				<div class="sideviewer-content-wrapper">
                    <div class="ec_top_info">
                        <div class="ec_top_div">
                            <?php echo $_smarty_tpl->getSubTemplate ("blocks/static_templates/ec_mob_logo.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                            <span class="sideviewer-close"><i class="ec-icon-close"></i></span>
                        </div>
                        <div class="ec_header_text"><?php echo $_smarty_tpl->__("ec_my_account");?>
</div>
                        <div class="ec_account_links">
                            <span>
                                <a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" class="ec_track_app_btn ec_links">
                                    <span class="ec-icon-Location"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>

                                    <div class="ec_menu_text"><?php echo $_smarty_tpl->__("ec_track_order");?>
</div>
                                </a>
                            </span>
                            <span>
                                <a href="<?php echo htmlspecialchars(fn_url("orders.search"), ENT_QUOTES, 'UTF-8');?>
">
                                    <span class="ec-icon-packages"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>

                                    <div class="ec_menu_text"><?php echo $_smarty_tpl->__("orders");?>
</div>
                                </a>
                            </span>
                            <span>
                                <a href="<?php echo htmlspecialchars(fn_url("d_prescription.manage"), ENT_QUOTES, 'UTF-8');?>
">
                                    <span class="ec-icon-prescription"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>

                                    <div class="ec_menu_text"><?php echo $_smarty_tpl->__("ec_prescriptions");?>
</div>
                                </a>
                            </span>
                            <span>
                                <a href="<?php echo htmlspecialchars(fn_url("h_rfq.manage"), ENT_QUOTES, 'UTF-8');?>
">
                                    <span class="ec-icon-quotation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span>

                                    <div class="ec_menu_text"><?php echo $_smarty_tpl->__("ec_quotations");?>
</div>
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="ec_category_content">
                        <?php echo $_smarty_tpl->getSubTemplate ("blocks/my_account.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('no_capture'=>true,'hide_captcha'=>true,'hide_track'=>true,'hide_order'=>true,'app_link'=>true), 0);?>

                        <div class="ec_lang_loc_account">
                        <?php echo $_smarty_tpl->getSubTemplate ("blocks/languages.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                        </div>
                    </div>
                </div>
			</div>

            <div class="ec_track_order_container">
                <div class="ty-account-info__orders updates-wrapper track-orders" id="track_orders_block_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
">
                    <div class="ec_app_track_top">
                        <span class="ec_left">
                            <span class="ec_icon_div">
                                <span class="ec-icon-Location"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                            </span>

                            <span class="header_text"><?php echo $_smarty_tpl->__("ec_track_order");?>
</span>
                        </span>
                        <span class="ec_track_close"><i class="ec-icon-close"></i></span>
                    </div>

                    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="POST" class="cm-ajax cm-post cm-ajax-full-render" name="track_order_quick">
                        <input type="hidden" name="result_ids" value="track_orders_block_*" />
                        <input type="hidden" name="return_url" value="<?php echo htmlspecialchars((($tmp = @$_REQUEST['return_url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />

                        <div class="ty-account-info__orders-txt"><?php echo $_smarty_tpl->__("track_my_order");?>
</div>

                        <div class="ty-account-info__orders-input ty-control-group ty-input-append">
                            <label for="track_order_item<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
" class="cm-required hidden"><?php echo $_smarty_tpl->__("track_my_order");?>
</label>
                            <input type="text" size="20" class="ty-input-text cm-hint" id="track_order_item<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
" name="track_data" value="<?php echo $_smarty_tpl->__("order_id");
if (!$_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>/<?php echo $_smarty_tpl->__("email");
}?>" />
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/go.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"orders.track_request",'alt'=>$_smarty_tpl->__("go")), 0);?>

                            
                        </div>

                        <span class="ec_icon_track_bottom">
                            <span class="ec-icon-Location"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                        </span>
                    </form>
                <!--track_orders_block_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
--></div>
            </div>
		</div>	
		<div class="sideviewer-overlay"></div>
	</div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/static_templates/ec_app_my_account.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/static_templates/ec_app_my_account.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->_capture_stack[0][] = array("title", null, null); ob_start(); ?>
    <span></span>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->tpl_vars["sideviewer_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
<div class="ec_bottom_app_account header-left-block">
    <div class="fn-sideviewer">
    	<div class="fn-sideviewer__toggle ec_app_account_opener">
			<?php if (trim(Smarty::$_smarty_vars['capture']['title'])) {?>
				<?php echo Smarty::$_smarty_vars['capture']['title'];?>

			<?php }?>
		</div>
		<div id="sideviewer_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sideviewer_id']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" class="fn-sideviewer-wrapper">
			<div class="side-viewer-content">
				<div class="sideviewer-content-wrapper">
                    <div class="ec_top_info">
                        <div class="ec_top_div">
                            <?php echo $_smarty_tpl->getSubTemplate ("blocks/static_templates/ec_mob_logo.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                            <span class="sideviewer-close"><i class="ec-icon-close"></i></span>
                        </div>
                        <div class="ec_header_text"><?php echo $_smarty_tpl->__("ec_my_account");?>
</div>
                        <div class="ec_account_links">
                            <span>
                                <a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" class="ec_track_app_btn ec_links">
                                    <span class="ec-icon-Location"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>

                                    <div class="ec_menu_text"><?php echo $_smarty_tpl->__("ec_track_order");?>
</div>
                                </a>
                            </span>
                            <span>
                                <a href="<?php echo htmlspecialchars(fn_url("orders.search"), ENT_QUOTES, 'UTF-8');?>
">
                                    <span class="ec-icon-packages"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>

                                    <div class="ec_menu_text"><?php echo $_smarty_tpl->__("orders");?>
</div>
                                </a>
                            </span>
                            <span>
                                <a href="<?php echo htmlspecialchars(fn_url("d_prescription.manage"), ENT_QUOTES, 'UTF-8');?>
">
                                    <span class="ec-icon-prescription"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>

                                    <div class="ec_menu_text"><?php echo $_smarty_tpl->__("ec_prescriptions");?>
</div>
                                </a>
                            </span>
                            <span>
                                <a href="<?php echo htmlspecialchars(fn_url("h_rfq.manage"), ENT_QUOTES, 'UTF-8');?>
">
                                    <span class="ec-icon-quotation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span>

                                    <div class="ec_menu_text"><?php echo $_smarty_tpl->__("ec_quotations");?>
</div>
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="ec_category_content">
                        <?php echo $_smarty_tpl->getSubTemplate ("blocks/my_account.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('no_capture'=>true,'hide_captcha'=>true,'hide_track'=>true,'hide_order'=>true,'app_link'=>true), 0);?>

                        <div class="ec_lang_loc_account">
                        <?php echo $_smarty_tpl->getSubTemplate ("blocks/languages.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                        </div>
                    </div>
                </div>
			</div>

            <div class="ec_track_order_container">
                <div class="ty-account-info__orders updates-wrapper track-orders" id="track_orders_block_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
">
                    <div class="ec_app_track_top">
                        <span class="ec_left">
                            <span class="ec_icon_div">
                                <span class="ec-icon-Location"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                            </span>

                            <span class="header_text"><?php echo $_smarty_tpl->__("ec_track_order");?>
</span>
                        </span>
                        <span class="ec_track_close"><i class="ec-icon-close"></i></span>
                    </div>

                    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="POST" class="cm-ajax cm-post cm-ajax-full-render" name="track_order_quick">
                        <input type="hidden" name="result_ids" value="track_orders_block_*" />
                        <input type="hidden" name="return_url" value="<?php echo htmlspecialchars((($tmp = @$_REQUEST['return_url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />

                        <div class="ty-account-info__orders-txt"><?php echo $_smarty_tpl->__("track_my_order");?>
</div>

                        <div class="ty-account-info__orders-input ty-control-group ty-input-append">
                            <label for="track_order_item<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
" class="cm-required hidden"><?php echo $_smarty_tpl->__("track_my_order");?>
</label>
                            <input type="text" size="20" class="ty-input-text cm-hint" id="track_order_item<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
" name="track_data" value="<?php echo $_smarty_tpl->__("order_id");
if (!$_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>/<?php echo $_smarty_tpl->__("email");
}?>" />
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/go.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"orders.track_request",'alt'=>$_smarty_tpl->__("go")), 0);?>

                            
                        </div>

                        <span class="ec_icon_track_bottom">
                            <span class="ec-icon-Location"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
                        </span>
                    </form>
                <!--track_orders_block_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
--></div>
            </div>
		</div>	
		<div class="sideviewer-overlay"></div>
	</div>
</div><?php }?><?php }} ?>
