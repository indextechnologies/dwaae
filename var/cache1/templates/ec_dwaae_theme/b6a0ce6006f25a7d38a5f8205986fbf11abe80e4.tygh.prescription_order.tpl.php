<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 16:39:23
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_custom/views/delivery_details/prescription_order.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7045102576096867b68c9f4-75208816%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6a0ce6006f25a7d38a5f8205986fbf11abe80e4' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_custom/views/delivery_details/prescription_order.tpl',
      1 => 1605420386,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '7045102576096867b68c9f4-75208816',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'order_info' => 0,
    'prescription_required' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6096867b6c3a43_69608572',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6096867b6c3a43_69608572')) {function content_6096867b6c3a43_69608572($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo smarty_function_script(array('src'=>"js/addons/d_custom/signature.js?fsdfdsds"),$_smarty_tpl);?>

<?php if ($_smarty_tpl->tpl_vars['order_info']->value['delivery_status']!='C') {?>
    <form name="add_delivery_details" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data" method="post" class="form-edit form-horizontal">
        <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
        <div class='js-signature'></div>
        <input type="hidden" name="signature" value="" class="save-signature"/>
        <div class="ty-control-group ec_upload_rescription_btn">          
            <label for="id_photos_delivered_order" class="ty-control-group__title <?php if ($_smarty_tpl->tpl_vars['prescription_required']->value) {?>cm-required<?php }?>">
                Photos of delivered order
            </label>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"delivery_photos[]",'multiupload'=>true,'label_id'=>"id_photos_delivered_order",'upload_file_text'=>"Photos of delivered order",'upload_another_file_text'=>"Photos of delivered order"), 0);?>

        </div>

        <div class="ty-control-group ec_upload_rescription_btn">          
            <label for="id_prescribed_attachment" class="ty-control-group__title <?php if ($_smarty_tpl->tpl_vars['prescription_required']->value) {?>cm-required<?php }?>">
                Upload Person Emirate ID
            </label>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"emirate_ids[]",'multiupload'=>true,'label_id'=>"id_prescribed_attachment",'upload_file_text'=>"Upload Person Emirate ID",'upload_another_file_text'=>"Upload Person Emirate ID"), 0);?>

        </div>

        <div class="buttons-container">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[delivery_details.submit_porder]",'but_target_form'=>"add_delivery_details",'but_meta'=>"ty-btn_primary ty-float-right",'but_text'=>"Upload Details",'but_title'=>"Upload all the details in the single time"), 0);?>

        </div>
    </form>
    <?php echo '<script'; ?>
>
        $(document).ready(function(){
            $('.js-signature').jqSignature({
                width: 380,
                height: 400
            });
        });
        (function(_, $) {
            $.ceEvent('on', 'ce.commoninit', function(context){
                $.ceEvent('on', 'ce.formpre_add_delivery_details', function (form, clicked_elm) {
                    var val = $('.js-signature').jqSignature('getDataURL');
                    $('.save-signature').val(val);
                });
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php } else { ?>
    <div style="height: 400px; display: flex; justify-content: center; align-items: center; font-size: 20px;"><h3 style="color: green;text-align: center;">Order Details Submit Successfully</h3></div>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_custom/views/delivery_details/prescription_order.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_custom/views/delivery_details/prescription_order.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo smarty_function_script(array('src'=>"js/addons/d_custom/signature.js?fsdfdsds"),$_smarty_tpl);?>

<?php if ($_smarty_tpl->tpl_vars['order_info']->value['delivery_status']!='C') {?>
    <form name="add_delivery_details" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data" method="post" class="form-edit form-horizontal">
        <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
        <div class='js-signature'></div>
        <input type="hidden" name="signature" value="" class="save-signature"/>
        <div class="ty-control-group ec_upload_rescription_btn">          
            <label for="id_photos_delivered_order" class="ty-control-group__title <?php if ($_smarty_tpl->tpl_vars['prescription_required']->value) {?>cm-required<?php }?>">
                Photos of delivered order
            </label>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"delivery_photos[]",'multiupload'=>true,'label_id'=>"id_photos_delivered_order",'upload_file_text'=>"Photos of delivered order",'upload_another_file_text'=>"Photos of delivered order"), 0);?>

        </div>

        <div class="ty-control-group ec_upload_rescription_btn">          
            <label for="id_prescribed_attachment" class="ty-control-group__title <?php if ($_smarty_tpl->tpl_vars['prescription_required']->value) {?>cm-required<?php }?>">
                Upload Person Emirate ID
            </label>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"emirate_ids[]",'multiupload'=>true,'label_id'=>"id_prescribed_attachment",'upload_file_text'=>"Upload Person Emirate ID",'upload_another_file_text'=>"Upload Person Emirate ID"), 0);?>

        </div>

        <div class="buttons-container">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[delivery_details.submit_porder]",'but_target_form'=>"add_delivery_details",'but_meta'=>"ty-btn_primary ty-float-right",'but_text'=>"Upload Details",'but_title'=>"Upload all the details in the single time"), 0);?>

        </div>
    </form>
    <?php echo '<script'; ?>
>
        $(document).ready(function(){
            $('.js-signature').jqSignature({
                width: 380,
                height: 400
            });
        });
        (function(_, $) {
            $.ceEvent('on', 'ce.commoninit', function(context){
                $.ceEvent('on', 'ce.formpre_add_delivery_details', function (form, clicked_elm) {
                    var val = $('.js-signature').jqSignature('getDataURL');
                    $('.save-signature').val(val);
                });
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php } else { ?>
    <div style="height: 400px; display: flex; justify-content: center; align-items: center; font-size: 20px;"><h3 style="color: green;text-align: center;">Order Details Submit Successfully</h3></div>
<?php }
}?><?php }} ?>
