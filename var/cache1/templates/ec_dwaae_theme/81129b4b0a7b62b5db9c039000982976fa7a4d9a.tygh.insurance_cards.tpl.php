<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 22:38:51
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/components/insurance_cards.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10626881846092e63b92fe43-00734198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81129b4b0a7b62b5db9c039000982976fa7a4d9a' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/components/insurance_cards.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '10626881846092e63b92fe43-00734198',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'insurance_cards' => 0,
    'prescription_details' => 0,
    'insurance_card' => 0,
    'card_selected' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092e63b945a91_18351091',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092e63b945a91_18351091')) {function content_6092e63b945a91_18351091($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('add_new_insurance','add_new_insurance'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="insurance_card_content" id="insurance_card_content">
    <div class="ec_upload_form">
        <div id="ec_insurance_upload_block">
            <div class="ec_upload_form_blocks">
                <?php  $_smarty_tpl->tpl_vars['insurance_card'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['insurance_card']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['insurance_cards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["insurance_card"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['insurance_card']->key => $_smarty_tpl->tpl_vars['insurance_card']->value) {
$_smarty_tpl->tpl_vars['insurance_card']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["insurance_card"]['iteration']++;
?>
                <?php $_smarty_tpl->tpl_vars['card_selected'] = new Smarty_variable(false, null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['prescription_details']->value['selected_insurance']==$_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_id']||$_smarty_tpl->getVariable('smarty')->value['foreach']['insurance_card']['iteration']==1) {?>
                    <?php $_smarty_tpl->tpl_vars['card_selected'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/insurance_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('enable_insurance_select'=>true,'insurance_redirect_url'=>"d_prescription.upload_form",'insurance_edit_page'=>"d_prescription.edit_insurance",'insurance_result_ids'=>"ec_insurance_upload_block,ec_prescription__required",'insurance_card_selected'=>$_smarty_tpl->tpl_vars['card_selected']->value), 0);?>

                <?php } ?>
            </div>
            <div class="ec_bottom_btn">
                <a href="<?php echo htmlspecialchars(fn_url("d_prescription.edit_insurance"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render ec_add_card_btn" data-ca-target-id="ec_insurance_upload_block,ec_prescription__required"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_insurance");?>
</a>
            </div>
        <!--ec_insurance_upload_block--></div>
    </div>
<!--insurance_card_content--></div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_prescription/views/d_prescription/components/insurance_cards.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_prescription/views/d_prescription/components/insurance_cards.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="insurance_card_content" id="insurance_card_content">
    <div class="ec_upload_form">
        <div id="ec_insurance_upload_block">
            <div class="ec_upload_form_blocks">
                <?php  $_smarty_tpl->tpl_vars['insurance_card'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['insurance_card']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['insurance_cards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["insurance_card"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['insurance_card']->key => $_smarty_tpl->tpl_vars['insurance_card']->value) {
$_smarty_tpl->tpl_vars['insurance_card']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["insurance_card"]['iteration']++;
?>
                <?php $_smarty_tpl->tpl_vars['card_selected'] = new Smarty_variable(false, null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['prescription_details']->value['selected_insurance']==$_smarty_tpl->tpl_vars['insurance_card']->value['insurance_profile_id']||$_smarty_tpl->getVariable('smarty')->value['foreach']['insurance_card']['iteration']==1) {?>
                    <?php $_smarty_tpl->tpl_vars['card_selected'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/insurance_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('enable_insurance_select'=>true,'insurance_redirect_url'=>"d_prescription.upload_form",'insurance_edit_page'=>"d_prescription.edit_insurance",'insurance_result_ids'=>"ec_insurance_upload_block,ec_prescription__required",'insurance_card_selected'=>$_smarty_tpl->tpl_vars['card_selected']->value), 0);?>

                <?php } ?>
            </div>
            <div class="ec_bottom_btn">
                <a href="<?php echo htmlspecialchars(fn_url("d_prescription.edit_insurance"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render ec_add_card_btn" data-ca-target-id="ec_insurance_upload_block,ec_prescription__required"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_insurance");?>
</a>
            </div>
        <!--ec_insurance_upload_block--></div>
    </div>
<!--insurance_card_content--></div><?php }?><?php }} ?>
