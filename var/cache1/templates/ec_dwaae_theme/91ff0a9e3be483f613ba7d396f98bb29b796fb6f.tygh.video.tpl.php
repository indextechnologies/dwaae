<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:02
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/banners/common/video.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19579827366092b8ce6bd8d7-85430609%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '91ff0a9e3be483f613ba7d396f98bb29b796fb6f' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/banners/common/video.tpl',
      1 => 1608010924,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '19579827366092b8ce6bd8d7-85430609',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'uid' => 0,
    'poster' => 0,
    'video_url' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8ce6d26b4_90174466',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8ce6d26b4_90174466')) {function content_6092b8ce6d26b4_90174466($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_REQUEST['from_app']!='Y') {?>
<?php $_smarty_tpl->tpl_vars['uid'] = new Smarty_variable(uniqid(), null, 0);?>
<video
    id="player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
"
    class="video-js owl-video-player d-video-player"
    preload="auto"
    <?php if ($_smarty_tpl->tpl_vars['poster']->value) {?>poster="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['poster']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>
    data-setup='{}'>
  <source src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video_url']->value, ENT_QUOTES, 'UTF-8');?>
"></source>
  <p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a
    web browser that
    <a href="https://videojs.com/html5-video-support/" target="_blank">
      supports HTML5 video
    </a>
  </p>
</video>

<?php echo '<script'; ?>
>
    if(typeof(videoIds) == 'undefined'){
        videoIds = [];
    }
    if(typeof(players) == 'undefined'){
        players = [];
    }
    var options = {
        controls:false
    };
    players['player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
'] = videojs('player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
', options, function onPlayerReady() {
        // In this context, `this` is the player that was created by Video.js.
        
        this.on('start', function() {
            $('#player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
 .vjs-big-play-button>.vjs-icon-placeholder').addClass('d-pause-button');
        });
        // How about an event listener?
        this.on('ended', function() {
            $('#player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
 .vjs-big-play-button>.vjs-icon-placeholder').removeClass('d-pause-button');
        });
    });
    videoIds.push('player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
');
<?php echo '</script'; ?>
>
<?php } else { ?>
&nbsp;
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/banners/common/video.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/banners/common/video.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_REQUEST['from_app']!='Y') {?>
<?php $_smarty_tpl->tpl_vars['uid'] = new Smarty_variable(uniqid(), null, 0);?>
<video
    id="player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
"
    class="video-js owl-video-player d-video-player"
    preload="auto"
    <?php if ($_smarty_tpl->tpl_vars['poster']->value) {?>poster="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['poster']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>
    data-setup='{}'>
  <source src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['video_url']->value, ENT_QUOTES, 'UTF-8');?>
"></source>
  <p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a
    web browser that
    <a href="https://videojs.com/html5-video-support/" target="_blank">
      supports HTML5 video
    </a>
  </p>
</video>

<?php echo '<script'; ?>
>
    if(typeof(videoIds) == 'undefined'){
        videoIds = [];
    }
    if(typeof(players) == 'undefined'){
        players = [];
    }
    var options = {
        controls:false
    };
    players['player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
'] = videojs('player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
', options, function onPlayerReady() {
        // In this context, `this` is the player that was created by Video.js.
        
        this.on('start', function() {
            $('#player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
 .vjs-big-play-button>.vjs-icon-placeholder').addClass('d-pause-button');
        });
        // How about an event listener?
        this.on('ended', function() {
            $('#player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
 .vjs-big-play-button>.vjs-icon-placeholder').removeClass('d-pause-button');
        });
    });
    videoIds.push('player<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uid']->value, ENT_QUOTES, 'UTF-8');?>
');
<?php echo '</script'; ?>
>
<?php } else { ?>
&nbsp;
<?php }
}?><?php }} ?>
