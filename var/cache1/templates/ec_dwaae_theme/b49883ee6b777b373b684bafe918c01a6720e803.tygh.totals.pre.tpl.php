<?php /* Smarty version Smarty-3.1.21, created on 2021-05-24 20:53:34
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_custom/hooks/orders/totals.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:25697302160abda0e95cb76-86904315%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b49883ee6b777b373b684bafe918c01a6720e803' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_custom/hooks/orders/totals.pre.tpl',
      1 => 1609221325,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '25697302160abda0e95cb76-86904315',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'prescriptions' => 0,
    'order_info' => 0,
    'p' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60abda0e96f540_50738841',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60abda0e96f540_50738841')) {function content_60abda0e96f540_50738841($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_custom.prescriptions','d_custom.prescriptions'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['prescriptions']->value) {?>
     <tr class="ty-orders-summary__row">
     <td><?php echo $_smarty_tpl->__("d_custom.prescriptions");?>
:</td>
     <td style="width: 57%">
        <div class="d-order-prescriptions">
        <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescriptions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
            <a target="_blank" href="<?php echo htmlspecialchars(fn_url_for_app("d_prescriptions.download&order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])."&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
            <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                <br/>
            <?php }?>
        <?php } ?>
        </div>
    </td>
    </tr>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_custom/hooks/orders/totals.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_custom/hooks/orders/totals.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['prescriptions']->value) {?>
     <tr class="ty-orders-summary__row">
     <td><?php echo $_smarty_tpl->__("d_custom.prescriptions");?>
:</td>
     <td style="width: 57%">
        <div class="d-order-prescriptions">
        <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescriptions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
            <a target="_blank" href="<?php echo htmlspecialchars(fn_url_for_app("d_prescriptions.download&order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])."&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
            <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                <br/>
            <?php }?>
        <?php } ?>
        </div>
    </td>
    </tr>
<?php }
}?><?php }} ?>
