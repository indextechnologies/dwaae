<?php /* Smarty version Smarty-3.1.21, created on 2021-05-09 02:10:15
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_emr/views/clinic_reg/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:178484665760970c47d1a591-13569144%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a839b305f856924bf033dba14e9753c178fae7c5' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_emr/views/clinic_reg/update.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '178484665760970c47d1a591-13569144',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'id' => 0,
    'config' => 0,
    'redirect_url' => 0,
    'states' => 0,
    'state' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60970c47d79782_18874225',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60970c47d79782_18874225')) {function content_60970c47d79782_18874225($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_clinic_registration','clinic_name','email','password','phone','facility_license_number','address','city','state','ec_emr.i_accept_the_terms_and_conditions','registration','ec_clinic_registration','clinic_name','email','password','phone','facility_license_number','address','city','state','ec_emr.i_accept_the_terms_and_conditions','registration'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars["id"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['id']->value)===null||$tmp==='' ? "clinic_reg" : $tmp), null, 0);?>

<?php $_smarty_tpl->_capture_stack[0][] = array("login", null, null); ob_start(); ?>
    <form name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" >
        <input type="hidden" name="return_url" value="<?php echo htmlspecialchars((($tmp = @$_REQUEST['return_url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['redirect_url']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
        <div class="ec_login_header"><?php echo $_smarty_tpl->__("ec_clinic_registration");?>
</div>


        <div class="ty-control-group ty-h_vendor_reg_field-half">
            <label for="clinic_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-clinic_name__filed-label ty-control-group__label cm-required "><?php echo $_smarty_tpl->__("clinic_name");?>
</label>
            <input type="text" id="clinic_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="name" size="30" value="" class="ty-login__input cm-focus" />
        </div>

        <div class="ty-control-group ty-h_vendor_reg_field-half" style="margin-right: 0px;">
            <label for="login_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-login__filed-label ty-control-group__label cm-required cm-trim cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
            <input type="text" id="login_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="email" size="30" value="" class="ty-login__input " />
        </div>

         <div class="ty-control-group ty-h_vendor_reg_field-half">
            <label for="psw_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-login__filed-label ty-control-group__label cm-required"><?php echo $_smarty_tpl->__("password");?>
</label>
            <input type="password" id="psw_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="password" size="30" value="" class="ty-login__input "  maxlength="32"/>
        </div>

         <div class="ty-control-group ty-h_vendor_reg_field-half" style="margin-right: 0px;">
            <label for="phone_no<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-phone_no_filed-label ty-control-group__label cm-required "><?php echo $_smarty_tpl->__("phone");?>
</label>
            <input type="text" id="phone_no<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="phone_no" size="30" value="" class="ty-login__input cm-focus" />
        </div>

        <div class="ty-control-group" style="width: 95%;">
            <label for="fln_no_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-fln_no__filed-label ty-control-group__label cm-required " style="width: 100%;"><?php echo $_smarty_tpl->__("facility_license_number");?>
</label>
            <input type="text" id="fln_no_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="facility_licence_no" size="30" value="" class="ty-login__input cm-focus" />
        </div>

        <div class="ty-control-group" style="width: 95%;">
            <label for="address<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-address_filed-label ty-control-group__label cm-required " ><?php echo $_smarty_tpl->__("address");?>
</label>
            <input type="text" id="address<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="address" size="30" value="" class="ty-login__input cm-focus" />
        </div>

        <div class="ty-control-group ty-h_vendor_reg_field-half">
            <label for="city<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-city_filed-label ty-control-group__label cm-required "><?php echo $_smarty_tpl->__("city");?>
</label>
            <input type="text" id="city<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="city" size="30" value="" class="ty-login__input cm-focus" />
        </div>
        
        <div class="ty-control-group ty-h_vendor_reg_field-half" style="margin-right: 0px;">
            <label for="state<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-state_filed-label ty-control-group__label cm-required "><?php echo $_smarty_tpl->__("state");?>
</label>
            <select id="state<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="state" class="ty-profile-field__select-state cm-state ">
                <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>

       <div class="ty-control-group ty-company__terms">
            <div class="d-terms-and-condition">
                <label for="accept_terms" id="accept_terms_label" class="cm-required">
                    <input type="checkbox" id="accept_terms" name="accept_terms" value="Y" class="cm-agreement checkbox">
                    <a href="https://www.dwaae.com/en/terms-and-conditions-ar.html" target="_blank"><?php echo $_smarty_tpl->__("ec_emr.i_accept_the_terms_and_conditions");?>
</a>
                </label>
            </div>
        </div>

        <?php echo $_smarty_tpl->getSubTemplate ("common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"login",'align'=>"left"), 0);?>

       
        <div class="ty-profile-field__buttons buttons-containerclearfix">
            
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/register_profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[clinic_reg.register]",'but_role'=>"submit"), 0);?>

            
        </div>
    </form>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

    <div class="ec_top_form_logo">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/new_logo.png">
    </div>
    <div class="ty-account">
        <?php echo Smarty::$_smarty_vars['capture']['login'];?>

    </div>

    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("registration");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_emr/views/clinic_reg/update.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_emr/views/clinic_reg/update.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars["id"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['id']->value)===null||$tmp==='' ? "clinic_reg" : $tmp), null, 0);?>

<?php $_smarty_tpl->_capture_stack[0][] = array("login", null, null); ob_start(); ?>
    <form name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" >
        <input type="hidden" name="return_url" value="<?php echo htmlspecialchars((($tmp = @$_REQUEST['return_url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['redirect_url']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
        <div class="ec_login_header"><?php echo $_smarty_tpl->__("ec_clinic_registration");?>
</div>


        <div class="ty-control-group ty-h_vendor_reg_field-half">
            <label for="clinic_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-clinic_name__filed-label ty-control-group__label cm-required "><?php echo $_smarty_tpl->__("clinic_name");?>
</label>
            <input type="text" id="clinic_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="name" size="30" value="" class="ty-login__input cm-focus" />
        </div>

        <div class="ty-control-group ty-h_vendor_reg_field-half" style="margin-right: 0px;">
            <label for="login_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-login__filed-label ty-control-group__label cm-required cm-trim cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
            <input type="text" id="login_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="email" size="30" value="" class="ty-login__input " />
        </div>

         <div class="ty-control-group ty-h_vendor_reg_field-half">
            <label for="psw_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-login__filed-label ty-control-group__label cm-required"><?php echo $_smarty_tpl->__("password");?>
</label>
            <input type="password" id="psw_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="password" size="30" value="" class="ty-login__input "  maxlength="32"/>
        </div>

         <div class="ty-control-group ty-h_vendor_reg_field-half" style="margin-right: 0px;">
            <label for="phone_no<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-phone_no_filed-label ty-control-group__label cm-required "><?php echo $_smarty_tpl->__("phone");?>
</label>
            <input type="text" id="phone_no<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="phone_no" size="30" value="" class="ty-login__input cm-focus" />
        </div>

        <div class="ty-control-group" style="width: 95%;">
            <label for="fln_no_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-fln_no__filed-label ty-control-group__label cm-required " style="width: 100%;"><?php echo $_smarty_tpl->__("facility_license_number");?>
</label>
            <input type="text" id="fln_no_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="facility_licence_no" size="30" value="" class="ty-login__input cm-focus" />
        </div>

        <div class="ty-control-group" style="width: 95%;">
            <label for="address<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-address_filed-label ty-control-group__label cm-required " ><?php echo $_smarty_tpl->__("address");?>
</label>
            <input type="text" id="address<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="address" size="30" value="" class="ty-login__input cm-focus" />
        </div>

        <div class="ty-control-group ty-h_vendor_reg_field-half">
            <label for="city<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-city_filed-label ty-control-group__label cm-required "><?php echo $_smarty_tpl->__("city");?>
</label>
            <input type="text" id="city<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="city" size="30" value="" class="ty-login__input cm-focus" />
        </div>
        
        <div class="ty-control-group ty-h_vendor_reg_field-half" style="margin-right: 0px;">
            <label for="state<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-state_filed-label ty-control-group__label cm-required "><?php echo $_smarty_tpl->__("state");?>
</label>
            <select id="state<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="state" class="ty-profile-field__select-state cm-state ">
                <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>

       <div class="ty-control-group ty-company__terms">
            <div class="d-terms-and-condition">
                <label for="accept_terms" id="accept_terms_label" class="cm-required">
                    <input type="checkbox" id="accept_terms" name="accept_terms" value="Y" class="cm-agreement checkbox">
                    <a href="https://www.dwaae.com/en/terms-and-conditions-ar.html" target="_blank"><?php echo $_smarty_tpl->__("ec_emr.i_accept_the_terms_and_conditions");?>
</a>
                </label>
            </div>
        </div>

        <?php echo $_smarty_tpl->getSubTemplate ("common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"login",'align'=>"left"), 0);?>

       
        <div class="ty-profile-field__buttons buttons-containerclearfix">
            
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/register_profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[clinic_reg.register]",'but_role'=>"submit"), 0);?>

            
        </div>
    </form>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

    <div class="ec_top_form_logo">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/new_logo.png">
    </div>
    <div class="ty-account">
        <?php echo Smarty::$_smarty_vars['capture']['login'];?>

    </div>

    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("registration");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?><?php }} ?>
