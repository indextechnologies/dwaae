<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 00:18:47
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/emirate_cards/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1609132756092fda7c10245-49312889%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08951314ac13f9b8635a9fec1dd29319ff4412c8' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/emirate_cards/update.tpl',
      1 => 1607432676,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1609132756092fda7c10245-49312889',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'upload_name' => 0,
    'selected_card' => 0,
    'make_prescription_required' => 0,
    'form_body' => 0,
    'emirate_cancel_url' => 0,
    'emirate_redirect_url' => 0,
    'emirate_result_ids' => 0,
    'emirate_profile_id' => 0,
    'redirect_url' => 0,
    'cancel_url' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092fda7c62483_60238985',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092fda7c62483_60238985')) {function content_6092fda7c62483_60238985($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('upload_emirate_card','done','front_side','front_side','back_side','back_side','emirate_card_name','emirate_card_name','is_default','upload_emirate_card','done','front_side','front_side','back_side','back_side','emirate_card_name','emirate_card_name','is_default'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars['upload_name'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['upload_name']->value)===null||$tmp==='' ? "emirate_cards.update" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['emirate_profile_id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['emirate_profile_id'])===null||$tmp==='' ? 0 : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['make_prescription_required'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['make_prescription_required']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['form_body'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['form_body']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['cancel_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_cancel_url']->value)===null||$tmp==='' ? "emirate_cards.manage" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_redirect_url']->value)===null||$tmp==='' ? "index.php?dispatch=emirate_cards.manage" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['result_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_result_ids']->value)===null||$tmp==='' ? "ec_emirates_upload_block" : $tmp), null, 0);?>
<div id="ec_emirates_upload_block">
    <?php if ($_smarty_tpl->tpl_vars['form_body']->value) {?>
    <form name="ec_emirates_upload_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">    
    <?php }?>
        <input type="hidden" name="emirate_card[emirate_profile_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emirate_profile_id']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="result_ids" value="ec_emirates_upload_block" />
        <div class="ec_upload_form ec_card_upload_block_m">
            <div class="ec_upload_form_blocks">
                <div class="ec_emirates_id_card"> 
                    <div class="ec_header_row">
                        <div class="ec_left_content">
                            <i class="ec-icon-union"></i>
                            <span class="hidden-phone"><?php echo $_smarty_tpl->__("upload_emirate_card");?>
</span>
                        </div>
                        <div class="ec_right_content">
                            <div class="">
                                <a data-ca-dispatch="dispatch[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['upload_name']->value, ENT_QUOTES, 'UTF-8');?>
]" class="cm-ajax cm-ajax-full-render edit_address cm-submit">
                                    <span><?php echo $_smarty_tpl->__("done");?>
</span>
                                    <i class="ec-icon-done"></i>
                                </a>
                            </div>
                            
                            <div>
                                <a href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['cancel_url']->value), ENT_QUOTES, 'UTF-8');?>
" 
                                data-ca-target-id="ec_emirates_upload_block" class="cm-ajax cm-ajax-full-render ec_delete_div">
                                    <i class="ec-icon-close"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="ec_card_content add_card">
                       <div class="d-upload-form-block">
                            <div>
                                <div class="ty-control-group">
                                    <label for="id_front_side" class="hidden <?php if (!$_smarty_tpl->tpl_vars['selected_card']->value['front_side_path']) {?>cm-required<?php }?>">
                                        <?php echo $_smarty_tpl->__("front_side");?>

                                    </label>

                                    <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['front_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['selected_card']->value['front_side_path']), ENT_QUOTES, 'UTF-8');?>
" /><?php }?>

                                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"front_side[]",'multiupload'=>false,'ec_advance_img'=>"Y",'file_accept_types'=>"image/*",'label_id'=>"id_front_side"), 0);?>


                                    <div><?php echo $_smarty_tpl->__("front_side");?>
</div>
                                </div>
                            </div>
                            <div>
                                <div class="ty-control-group">
                                    <label for="id_back_side" class="hidden <?php if (!$_smarty_tpl->tpl_vars['selected_card']->value['back_side_path']) {?>cm-required<?php }?>">
                                        <?php echo $_smarty_tpl->__("back_side");?>

                                    </label>

                                    <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['back_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['selected_card']->value['back_side_path']), ENT_QUOTES, 'UTF-8');?>
" /><?php }?>

                                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"back_side[]",'multiupload'=>false,'ec_advance_img'=>"Y",'file_accept_types'=>"image/*",'label_id'=>"id_back_side"), 0);?>

                                    
                                    <div><?php echo $_smarty_tpl->__("back_side");?>
</div>
                                </div>
                            </div>
                        </div>
                        <div class="ec_bottom">
                            <div>
                                <label for="emirate_profile_name" class="hidden cm-required">
                                    <?php echo $_smarty_tpl->__("emirate_card_name");?>

                                </label>
                                <input type="text" id="emirate_profile_name" name="emirate_card[emirate_profile_name]" class="ty-input-text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['emirate_profile_name'], ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo $_smarty_tpl->__("emirate_card_name");?>
"/>
                            </div>
                            <div class="ty-control-group">
                                <label for="is_default" class="ty-control-group__title">
                                    <?php echo $_smarty_tpl->__("is_default");?>

                                </label>
                                <input type="hidden"  name="emirate_card[is_default]" value="N"/>
                                <input type="checkbox" id="is_default" value="Y" name="emirate_card[is_default]" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['is_default']=='Y') {?>checked="checked"<?php }?>/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['form_body']->value) {?>
    </form>
    <?php }?>
<!--ec_emirates_upload_block--></div>



<div id="ec_next_div">
    &nbsp;
<!--ec_next_div--></div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_customer/views/emirate_cards/update.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_customer/views/emirate_cards/update.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars['upload_name'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['upload_name']->value)===null||$tmp==='' ? "emirate_cards.update" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['emirate_profile_id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['emirate_profile_id'])===null||$tmp==='' ? 0 : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['make_prescription_required'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['make_prescription_required']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['form_body'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['form_body']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['cancel_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_cancel_url']->value)===null||$tmp==='' ? "emirate_cards.manage" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_redirect_url']->value)===null||$tmp==='' ? "index.php?dispatch=emirate_cards.manage" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['result_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['emirate_result_ids']->value)===null||$tmp==='' ? "ec_emirates_upload_block" : $tmp), null, 0);?>
<div id="ec_emirates_upload_block">
    <?php if ($_smarty_tpl->tpl_vars['form_body']->value) {?>
    <form name="ec_emirates_upload_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">    
    <?php }?>
        <input type="hidden" name="emirate_card[emirate_profile_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['emirate_profile_id']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="result_ids" value="ec_emirates_upload_block" />
        <div class="ec_upload_form ec_card_upload_block_m">
            <div class="ec_upload_form_blocks">
                <div class="ec_emirates_id_card"> 
                    <div class="ec_header_row">
                        <div class="ec_left_content">
                            <i class="ec-icon-union"></i>
                            <span class="hidden-phone"><?php echo $_smarty_tpl->__("upload_emirate_card");?>
</span>
                        </div>
                        <div class="ec_right_content">
                            <div class="">
                                <a data-ca-dispatch="dispatch[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['upload_name']->value, ENT_QUOTES, 'UTF-8');?>
]" class="cm-ajax cm-ajax-full-render edit_address cm-submit">
                                    <span><?php echo $_smarty_tpl->__("done");?>
</span>
                                    <i class="ec-icon-done"></i>
                                </a>
                            </div>
                            
                            <div>
                                <a href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['cancel_url']->value), ENT_QUOTES, 'UTF-8');?>
" 
                                data-ca-target-id="ec_emirates_upload_block" class="cm-ajax cm-ajax-full-render ec_delete_div">
                                    <i class="ec-icon-close"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="ec_card_content add_card">
                       <div class="d-upload-form-block">
                            <div>
                                <div class="ty-control-group">
                                    <label for="id_front_side" class="hidden <?php if (!$_smarty_tpl->tpl_vars['selected_card']->value['front_side_path']) {?>cm-required<?php }?>">
                                        <?php echo $_smarty_tpl->__("front_side");?>

                                    </label>

                                    <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['front_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['selected_card']->value['front_side_path']), ENT_QUOTES, 'UTF-8');?>
" /><?php }?>

                                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"front_side[]",'multiupload'=>false,'ec_advance_img'=>"Y",'file_accept_types'=>"image/*",'label_id'=>"id_front_side"), 0);?>


                                    <div><?php echo $_smarty_tpl->__("front_side");?>
</div>
                                </div>
                            </div>
                            <div>
                                <div class="ty-control-group">
                                    <label for="id_back_side" class="hidden <?php if (!$_smarty_tpl->tpl_vars['selected_card']->value['back_side_path']) {?>cm-required<?php }?>">
                                        <?php echo $_smarty_tpl->__("back_side");?>

                                    </label>

                                    <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['back_side_path']) {?><img src="<?php echo htmlspecialchars(fn_d_customer_get_file_url($_smarty_tpl->tpl_vars['selected_card']->value['back_side_path']), ENT_QUOTES, 'UTF-8');?>
" /><?php }?>

                                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"back_side[]",'multiupload'=>false,'ec_advance_img'=>"Y",'file_accept_types'=>"image/*",'label_id'=>"id_back_side"), 0);?>

                                    
                                    <div><?php echo $_smarty_tpl->__("back_side");?>
</div>
                                </div>
                            </div>
                        </div>
                        <div class="ec_bottom">
                            <div>
                                <label for="emirate_profile_name" class="hidden cm-required">
                                    <?php echo $_smarty_tpl->__("emirate_card_name");?>

                                </label>
                                <input type="text" id="emirate_profile_name" name="emirate_card[emirate_profile_name]" class="ty-input-text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['emirate_profile_name'], ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php echo $_smarty_tpl->__("emirate_card_name");?>
"/>
                            </div>
                            <div class="ty-control-group">
                                <label for="is_default" class="ty-control-group__title">
                                    <?php echo $_smarty_tpl->__("is_default");?>

                                </label>
                                <input type="hidden"  name="emirate_card[is_default]" value="N"/>
                                <input type="checkbox" id="is_default" value="Y" name="emirate_card[is_default]" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['is_default']=='Y') {?>checked="checked"<?php }?>/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['form_body']->value) {?>
    </form>
    <?php }?>
<!--ec_emirates_upload_block--></div>



<div id="ec_next_div">
    &nbsp;
<!--ec_next_div--></div><?php }?><?php }} ?>
