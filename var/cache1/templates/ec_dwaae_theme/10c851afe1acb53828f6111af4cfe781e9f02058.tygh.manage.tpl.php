<?php /* Smarty version Smarty-3.1.21, created on 2021-05-15 18:51:39
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/emirate_cards/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1197488390609fdffbb0e1b2-01653329%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '10c851afe1acb53828f6111af4cfe781e9f02058' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/emirate_cards/manage.tpl',
      1 => 1614866659,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1197488390609fdffbb0e1b2-01653329',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'emirate_cards' => 0,
    'emirate_card' => 0,
    'enable_card_select' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_609fdffbb42c49_34535054',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_609fdffbb42c49_34535054')) {function content_609fdffbb42c49_34535054($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_emirates_cards','add_new_id','done','ec_emirates_cards','add_new_id','done'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_my_account_emirate ec_prescription_top_div">
    <div class="ec_header"><?php echo $_smarty_tpl->__("ec_emirates_cards");?>
</div>
    <div class="ec_patient_tab_content" id="ec_patient_tab_content">
        <div id="ec_emirates_upload_block">
            <div class="ec_upload_form_blocks">
            
                <?php  $_smarty_tpl->tpl_vars['emirate_card'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['emirate_card']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['emirate_cards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['emirate_card']->key => $_smarty_tpl->tpl_vars['emirate_card']->value) {
$_smarty_tpl->tpl_vars['emirate_card']->_loop = true;
?>
                    <?php $_smarty_tpl->tpl_vars['enable_card_select'] = new Smarty_variable(false, null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['emirate_card']->value['is_default']=="Y") {?>
                        <?php $_smarty_tpl->tpl_vars['enable_card_select'] = new Smarty_variable(true, null, 0);?>
                    <?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('emirate_edit_card'=>"emirate_cards.update",'emirate_render_ids'=>"ec_emirates_upload_block",'emirate_redirect_url'=>"emirate_cards.manage",'enable_card_select'=>$_smarty_tpl->tpl_vars['enable_card_select']->value,'emirate_card_selected'=>$_smarty_tpl->tpl_vars['enable_card_select']->value), 0);?>

                <?php } ?>
            </div>
            <div class="ec_bottom_btn">
               <a href="<?php echo htmlspecialchars(fn_url("emirate_cards.update"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render ec_add_card_btn" data-ca-target-id="ec_emirates_upload_block"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_id");?>
</a>

                <a href="<?php echo htmlspecialchars(fn_url("my_account.manage"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_next_btn"><?php echo $_smarty_tpl->__("done");?>
</a>
            </div>
        <!--ec_emirates_upload_block--></div>
    <!--ec_patient_tab_content--></div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_customer/views/emirate_cards/manage.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_customer/views/emirate_cards/manage.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_my_account_emirate ec_prescription_top_div">
    <div class="ec_header"><?php echo $_smarty_tpl->__("ec_emirates_cards");?>
</div>
    <div class="ec_patient_tab_content" id="ec_patient_tab_content">
        <div id="ec_emirates_upload_block">
            <div class="ec_upload_form_blocks">
            
                <?php  $_smarty_tpl->tpl_vars['emirate_card'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['emirate_card']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['emirate_cards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['emirate_card']->key => $_smarty_tpl->tpl_vars['emirate_card']->value) {
$_smarty_tpl->tpl_vars['emirate_card']->_loop = true;
?>
                    <?php $_smarty_tpl->tpl_vars['enable_card_select'] = new Smarty_variable(false, null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['emirate_card']->value['is_default']=="Y") {?>
                        <?php $_smarty_tpl->tpl_vars['enable_card_select'] = new Smarty_variable(true, null, 0);?>
                    <?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('emirate_edit_card'=>"emirate_cards.update",'emirate_render_ids'=>"ec_emirates_upload_block",'emirate_redirect_url'=>"emirate_cards.manage",'enable_card_select'=>$_smarty_tpl->tpl_vars['enable_card_select']->value,'emirate_card_selected'=>$_smarty_tpl->tpl_vars['enable_card_select']->value), 0);?>

                <?php } ?>
            </div>
            <div class="ec_bottom_btn">
               <a href="<?php echo htmlspecialchars(fn_url("emirate_cards.update"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render ec_add_card_btn" data-ca-target-id="ec_emirates_upload_block"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_id");?>
</a>

                <a href="<?php echo htmlspecialchars(fn_url("my_account.manage"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_next_btn"><?php echo $_smarty_tpl->__("done");?>
</a>
            </div>
        <!--ec_emirates_upload_block--></div>
    <!--ec_patient_tab_content--></div>
</div><?php }?><?php }} ?>
