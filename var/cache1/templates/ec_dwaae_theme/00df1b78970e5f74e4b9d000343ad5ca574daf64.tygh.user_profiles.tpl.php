<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 15:45:38
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/views/checkout/components/user_profiles.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1577823384609679e2094311-85356724%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '00df1b78970e5f74e4b9d000343ad5ca574daf64' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/views/checkout/components/user_profiles.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1577823384609679e2094311-85356724',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'user_profiles' => 0,
    'profile' => 0,
    'cart' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_609679e20d7c48_52302224',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_609679e20d7c48_52302224')) {function content_609679e20d7c48_52302224($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('checkout_select_profile_before_order','editing_profile_name','edit','editing_profile_name','edit','checkout_select_profile_before_order','editing_profile_name','edit','editing_profile_name','edit'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<label for="user_profiles_list"
       class="cm-required cm-multiple-radios hidden"
       data-ca-validator-error-message="<?php echo $_smarty_tpl->__("checkout_select_profile_before_order");?>
"></label>

<div id="user_profiles_list"
    class="litecheckout__group"
    data-ca-error-message-target-node="#user_profiles_list_error_message_target">
    <?php  $_smarty_tpl->tpl_vars['profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['profile']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user_profiles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['profile']->key => $_smarty_tpl->tpl_vars['profile']->value) {
$_smarty_tpl->tpl_vars['profile']->_loop = true;
?>
        <div class="ty-tiles litecheckout__field litecheckout__field--xsmall">
            <input type="radio"
                   name="profile_id"
                   id="user_profile_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                   class="ty-tiles__radio hidden js-lite-checkout-user-profile-radio <?php if (!$_smarty_tpl->tpl_vars['profile']->value['is_selectable']) {?>js-lite-checkout-edit-profile-popup-opener<?php } else { ?>js-lite-checkout-profile-selector<?php }?>"
                   data-ca-profile-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                   value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                   <?php if ($_smarty_tpl->tpl_vars['profile']->value['profile_id']==$_smarty_tpl->tpl_vars['cart']->value['profile_id']&&$_smarty_tpl->tpl_vars['profile']->value['is_selectable']) {?>checked<?php }?>
            />

            <label id="user_profiles_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                   class="ty-tiles__wrapper"
                   for="user_profile_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
            >
                <p class="ty-tiles__title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_address'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_address_2'], ENT_QUOTES, 'UTF-8');?>
</p>

                <?php if ($_smarty_tpl->tpl_vars['profile']->value['s_city']||$_smarty_tpl->tpl_vars['profile']->value['s_state_descr']||$_smarty_tpl->tpl_vars['profile']->value['s_zipcode']) {?>
                    <p class="ty-tiles__text"><?php if ($_smarty_tpl->tpl_vars['profile']->value['s_city']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_city'], ENT_QUOTES, 'UTF-8');?>
, <?php }
if ($_smarty_tpl->tpl_vars['profile']->value['s_state_descr']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_state_descr'], ENT_QUOTES, 'UTF-8');?>
, <?php }
if ($_smarty_tpl->tpl_vars['profile']->value['s_zipcode']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_zipcode'], ENT_QUOTES, 'UTF-8');
}?></p>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value['s_country_descr']) {?>
                    <p class="ty-tiles__text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_country_descr'], ENT_QUOTES, 'UTF-8');?>
</p>
                <?php }?>
                <div class="ty-tiles__actions">
                    <a
                        class="cm-dialog-opener cm-dialog-auto-size cm-dialog-destroy-on-close ty-tiles__link"
                        href="<?php echo htmlspecialchars(fn_url("checkout.update_profile?profile_id=".((string)$_smarty_tpl->tpl_vars['profile']->value['profile_id'])), ENT_QUOTES, 'UTF-8');?>
"
                        data-ca-target-id="edit_user_profile_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                        data-ca-dialog-title="<?php echo $_smarty_tpl->__("editing_profile_name",array('[name]'=>$_smarty_tpl->tpl_vars['profile']->value['profile_name']));?>
"
                    ><?php echo $_smarty_tpl->__("edit");?>
</a>

                    <?php if (!$_smarty_tpl->tpl_vars['profile']->value['is_selectable']) {?>
                        <a
                            class="cm-dialog-opener cm-dialog-auto-size cm-dialog-destroy-on-close hidden js-edit-profile-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                            href="<?php echo htmlspecialchars(fn_url("checkout.update_profile?profile_id=".((string)$_smarty_tpl->tpl_vars['profile']->value['profile_id'])."&switch_after_update=1"), ENT_QUOTES, 'UTF-8');?>
"
                            data-ca-target-id="edit_user_profile_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                            data-ca-dialog-title="<?php echo $_smarty_tpl->__("editing_profile_name",array('[name]'=>$_smarty_tpl->tpl_vars['profile']->value['profile_name']));?>
"
                        ><?php echo $_smarty_tpl->__("edit");?>
</a>
                    <?php }?>
                </div>
            </label>
        </div>
    <?php } ?>
</div>
<div class="litecheckout__group"><div id="user_profiles_list_error_message_target" class="litecheckout__item"></div></div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/checkout/components/user_profiles.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/checkout/components/user_profiles.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<label for="user_profiles_list"
       class="cm-required cm-multiple-radios hidden"
       data-ca-validator-error-message="<?php echo $_smarty_tpl->__("checkout_select_profile_before_order");?>
"></label>

<div id="user_profiles_list"
    class="litecheckout__group"
    data-ca-error-message-target-node="#user_profiles_list_error_message_target">
    <?php  $_smarty_tpl->tpl_vars['profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['profile']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user_profiles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['profile']->key => $_smarty_tpl->tpl_vars['profile']->value) {
$_smarty_tpl->tpl_vars['profile']->_loop = true;
?>
        <div class="ty-tiles litecheckout__field litecheckout__field--xsmall">
            <input type="radio"
                   name="profile_id"
                   id="user_profile_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                   class="ty-tiles__radio hidden js-lite-checkout-user-profile-radio <?php if (!$_smarty_tpl->tpl_vars['profile']->value['is_selectable']) {?>js-lite-checkout-edit-profile-popup-opener<?php } else { ?>js-lite-checkout-profile-selector<?php }?>"
                   data-ca-profile-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                   value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                   <?php if ($_smarty_tpl->tpl_vars['profile']->value['profile_id']==$_smarty_tpl->tpl_vars['cart']->value['profile_id']&&$_smarty_tpl->tpl_vars['profile']->value['is_selectable']) {?>checked<?php }?>
            />

            <label id="user_profiles_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                   class="ty-tiles__wrapper"
                   for="user_profile_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
            >
                <p class="ty-tiles__title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_address'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_address_2'], ENT_QUOTES, 'UTF-8');?>
</p>

                <?php if ($_smarty_tpl->tpl_vars['profile']->value['s_city']||$_smarty_tpl->tpl_vars['profile']->value['s_state_descr']||$_smarty_tpl->tpl_vars['profile']->value['s_zipcode']) {?>
                    <p class="ty-tiles__text"><?php if ($_smarty_tpl->tpl_vars['profile']->value['s_city']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_city'], ENT_QUOTES, 'UTF-8');?>
, <?php }
if ($_smarty_tpl->tpl_vars['profile']->value['s_state_descr']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_state_descr'], ENT_QUOTES, 'UTF-8');?>
, <?php }
if ($_smarty_tpl->tpl_vars['profile']->value['s_zipcode']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_zipcode'], ENT_QUOTES, 'UTF-8');
}?></p>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['profile']->value['s_country_descr']) {?>
                    <p class="ty-tiles__text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['s_country_descr'], ENT_QUOTES, 'UTF-8');?>
</p>
                <?php }?>
                <div class="ty-tiles__actions">
                    <a
                        class="cm-dialog-opener cm-dialog-auto-size cm-dialog-destroy-on-close ty-tiles__link"
                        href="<?php echo htmlspecialchars(fn_url("checkout.update_profile?profile_id=".((string)$_smarty_tpl->tpl_vars['profile']->value['profile_id'])), ENT_QUOTES, 'UTF-8');?>
"
                        data-ca-target-id="edit_user_profile_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                        data-ca-dialog-title="<?php echo $_smarty_tpl->__("editing_profile_name",array('[name]'=>$_smarty_tpl->tpl_vars['profile']->value['profile_name']));?>
"
                    ><?php echo $_smarty_tpl->__("edit");?>
</a>

                    <?php if (!$_smarty_tpl->tpl_vars['profile']->value['is_selectable']) {?>
                        <a
                            class="cm-dialog-opener cm-dialog-auto-size cm-dialog-destroy-on-close hidden js-edit-profile-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                            href="<?php echo htmlspecialchars(fn_url("checkout.update_profile?profile_id=".((string)$_smarty_tpl->tpl_vars['profile']->value['profile_id'])."&switch_after_update=1"), ENT_QUOTES, 'UTF-8');?>
"
                            data-ca-target-id="edit_user_profile_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile']->value['profile_id'], ENT_QUOTES, 'UTF-8');?>
"
                            data-ca-dialog-title="<?php echo $_smarty_tpl->__("editing_profile_name",array('[name]'=>$_smarty_tpl->tpl_vars['profile']->value['profile_name']));?>
"
                        ><?php echo $_smarty_tpl->__("edit");?>
</a>
                    <?php }?>
                </div>
            </label>
        </div>
    <?php } ?>
</div>
<div class="litecheckout__group"><div id="user_profiles_list_error_message_target" class="litecheckout__item"></div></div>
<?php }?><?php }} ?>
