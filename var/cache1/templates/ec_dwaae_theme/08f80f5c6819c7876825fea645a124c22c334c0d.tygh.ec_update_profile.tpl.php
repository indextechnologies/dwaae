<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 10:51:34
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/profiles/ec_update_profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:38988036609391f69a5647-82376734%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08f80f5c6819c7876825fea645a124c22c334c0d' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/profiles/ec_update_profile.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '38988036609391f69a5647-82376734',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'user_data' => 0,
    'dispatch' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_609391f69f9e83_58484608',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_609391f69f9e83_58484608')) {function content_609391f69f9e83_58484608($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('profile_details','edit_profile','go_back','firstname','lastname','password','confirm_password','email','phone_with_no','save','done','profile_details','edit_profile','go_back','firstname','lastname','password','confirm_password','email','phone_with_no','save','done'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_profile_billing_address" id="ec_profile_billing_address">
    <?php if ($_REQUEST['edit']) {?>
        <form name="ec_profile_update_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">   
    <?php }?>
    <div class="ec_top_header">
        <div class="ec_profile_header"><?php echo $_smarty_tpl->__("profile_details");?>
</div>
        
        <?php if (!$_REQUEST['edit']) {?>
        <a href="<?php echo htmlspecialchars(fn_url("profiles.update&edit=true"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_blue cm-ajax" data-ca-target-id="ec_profile_billing_address"><i class="ec-icon-edit"></i><?php echo $_smarty_tpl->__("edit_profile");?>
</a>
        <?php } else { ?>
        <a href="<?php echo htmlspecialchars(fn_url("profiles.update"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__secondary ec_go_back cm-ajax cm-ajax-full-render" data-ca-target-id="ec_profile_billing_address"><?php echo $_smarty_tpl->__("go_back");?>
</a>
        <?php }?>
    </div>
    
    <div class="ec_billing_addr_container">
    
        <div class="ty-control-group ec_first_name">
            <label for="firstname" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("firstname");?>
:
            </label>
            <input type="text" id="firstname" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?>  name="user_data[firstname]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
        <div class="ty-control-group ec_last_name">
            <label for="lastname" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("lastname");?>
:
            </label>
            <input type="text" id="lastname" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?> name="user_data[lastname]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
        <div>
        <div class="ty-control-group ec_password1">
            <label for="password1" class="ty-control-group__title cm-required cm-password">
            <?php echo $_smarty_tpl->__("password");?>
:
            </label>
            <input type="password" id="password1" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?>  name="user_data[password1]" maxlength="32" class="ty-input-text cm-autocomplete-off" value="**********" />
        </div>
        <div class="ty-control-group ec_password2">
            <label for="password1" class="ty-control-group__title cm-required cm-password">
            <?php echo $_smarty_tpl->__("confirm_password");?>
:
            </label>
            <input type="password" id="password2" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?> name="user_data[password2]" maxlength="32" class="ty-input-text cm-autocomplete-off" value="**********" />
        </div>
        </div>
        <div>
        <div class="ty-control-group ec_email">
            <label for="email" class="ty-control-group__title cm-required cm-email cm-trim">
            <?php echo $_smarty_tpl->__("email");?>
:
            </label>
            <input type="text" id="email" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?>  name="user_data[email]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['email'], ENT_QUOTES, 'UTF-8');?>
" class="" />
        </div>

        <div class="ty-control-group ec_phone">
            <label for="phone" class="ty-control-group__title cm-profile-field cm-mask-phone-label cm-required">
            <?php echo $_smarty_tpl->__("phone_with_no");?>
:
            </label>
            <input x-autocompletetype="tel" type="text" id="phone" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?>  name="user_data[phone]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['phone'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text cm-mask-phone js-mask-phone-inited" />
        </div>
        </div>
    </div>
    <?php if ($_REQUEST['edit']) {?>
    <div class="ec_bottom_view edit">
        <?php $_smarty_tpl->tpl_vars['dispatch'] = new Smarty_variable("profiles.update", null, 0);?>
        <a data-ca-dispatch="dispatch[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dispatch']->value, ENT_QUOTES, 'UTF-8');?>
]" class="cm-ajax cm-ajax-full-render cm-submit ty-btn ec_next_btn">
            <?php echo $_smarty_tpl->__("save");?>

        </a>
    </div>
    <?php } else { ?>
    <div class="ec_bottom_view">
        <a href="<?php echo htmlspecialchars(fn_url("my_account.manage"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_next_btn"><?php echo $_smarty_tpl->__("done");?>
</a>
    </div>
    <?php }?>
    

<!--ec_profile_billing_address--></div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/profiles/ec_update_profile.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/profiles/ec_update_profile.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_profile_billing_address" id="ec_profile_billing_address">
    <?php if ($_REQUEST['edit']) {?>
        <form name="ec_profile_update_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">   
    <?php }?>
    <div class="ec_top_header">
        <div class="ec_profile_header"><?php echo $_smarty_tpl->__("profile_details");?>
</div>
        
        <?php if (!$_REQUEST['edit']) {?>
        <a href="<?php echo htmlspecialchars(fn_url("profiles.update&edit=true"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_blue cm-ajax" data-ca-target-id="ec_profile_billing_address"><i class="ec-icon-edit"></i><?php echo $_smarty_tpl->__("edit_profile");?>
</a>
        <?php } else { ?>
        <a href="<?php echo htmlspecialchars(fn_url("profiles.update"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__secondary ec_go_back cm-ajax cm-ajax-full-render" data-ca-target-id="ec_profile_billing_address"><?php echo $_smarty_tpl->__("go_back");?>
</a>
        <?php }?>
    </div>
    
    <div class="ec_billing_addr_container">
    
        <div class="ty-control-group ec_first_name">
            <label for="firstname" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("firstname");?>
:
            </label>
            <input type="text" id="firstname" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?>  name="user_data[firstname]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
        <div class="ty-control-group ec_last_name">
            <label for="lastname" class="ty-control-group__title cm-required">
            <?php echo $_smarty_tpl->__("lastname");?>
:
            </label>
            <input type="text" id="lastname" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?> name="user_data[lastname]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
" />
        </div>
        <div>
        <div class="ty-control-group ec_password1">
            <label for="password1" class="ty-control-group__title cm-required cm-password">
            <?php echo $_smarty_tpl->__("password");?>
:
            </label>
            <input type="password" id="password1" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?>  name="user_data[password1]" maxlength="32" class="ty-input-text cm-autocomplete-off" value="**********" />
        </div>
        <div class="ty-control-group ec_password2">
            <label for="password1" class="ty-control-group__title cm-required cm-password">
            <?php echo $_smarty_tpl->__("confirm_password");?>
:
            </label>
            <input type="password" id="password2" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?> name="user_data[password2]" maxlength="32" class="ty-input-text cm-autocomplete-off" value="**********" />
        </div>
        </div>
        <div>
        <div class="ty-control-group ec_email">
            <label for="email" class="ty-control-group__title cm-required cm-email cm-trim">
            <?php echo $_smarty_tpl->__("email");?>
:
            </label>
            <input type="text" id="email" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?>  name="user_data[email]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['email'], ENT_QUOTES, 'UTF-8');?>
" class="" />
        </div>

        <div class="ty-control-group ec_phone">
            <label for="phone" class="ty-control-group__title cm-profile-field cm-mask-phone-label cm-required">
            <?php echo $_smarty_tpl->__("phone_with_no");?>
:
            </label>
            <input x-autocompletetype="tel" type="text" id="phone" <?php if (!$_REQUEST['edit']) {?>disabled<?php }?>  name="user_data[phone]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['phone'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text cm-mask-phone js-mask-phone-inited" />
        </div>
        </div>
    </div>
    <?php if ($_REQUEST['edit']) {?>
    <div class="ec_bottom_view edit">
        <?php $_smarty_tpl->tpl_vars['dispatch'] = new Smarty_variable("profiles.update", null, 0);?>
        <a data-ca-dispatch="dispatch[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dispatch']->value, ENT_QUOTES, 'UTF-8');?>
]" class="cm-ajax cm-ajax-full-render cm-submit ty-btn ec_next_btn">
            <?php echo $_smarty_tpl->__("save");?>

        </a>
    </div>
    <?php } else { ?>
    <div class="ec_bottom_view">
        <a href="<?php echo htmlspecialchars(fn_url("my_account.manage"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_next_btn"><?php echo $_smarty_tpl->__("done");?>
</a>
    </div>
    <?php }?>
    

<!--ec_profile_billing_address--></div><?php }?><?php }} ?>
