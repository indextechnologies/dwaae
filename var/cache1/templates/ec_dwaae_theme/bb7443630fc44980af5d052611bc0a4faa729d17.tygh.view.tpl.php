<?php /* Smarty version Smarty-3.1.21, created on 2021-05-07 20:59:01
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/wishlist/views/wishlist/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1480410168609571d54ed640-27535564%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bb7443630fc44980af5d052611bc0a4faa729d17' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/wishlist/views/wishlist/view.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1480410168609571d54ed640-27535564',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'wishlist_is_empty' => 0,
    'products' => 0,
    'columns' => 0,
    'config' => 0,
    'continue_url' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_609571d5511946_13248875',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_609571d5511946_13248875')) {function content_609571d5511946_13248875($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_your_wishlist_empty','ec_your_wishlist_empty','ec_what_are_you_waiting','wishlist_content','ec_your_wishlist_empty','ec_your_wishlist_empty','ec_what_are_you_waiting','wishlist_content'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars["columns"] = new Smarty_variable(4, null, 0);?>
<?php if (!$_smarty_tpl->tpl_vars['wishlist_is_empty']->value) {?>

    <?php echo smarty_function_script(array('src'=>"js/tygh/exceptions.js"),$_smarty_tpl);?>


    <?php $_smarty_tpl->tpl_vars["show_hr"] = new Smarty_variable(false, null, 0);?>
    <?php $_smarty_tpl->tpl_vars["location"] = new Smarty_variable("cart", null, 0);?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['products']->value||$_REQUEST['wishlist_filter']) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/wishlist/views/wishlist/components/wishlist_grid.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('columns'=>$_smarty_tpl->tpl_vars['columns']->value,'show_empty'=>true,'show_name'=>true,'show_old_price'=>true,'show_price'=>true,'show_clean_price'=>true,'show_list_discount'=>true,'no_pagination'=>false,'no_sorting'=>false,'show_features'=>true,'show_add_to_cart'=>true,'is_wishlist'=>true), 0);?>

<?php } else { ?>
    <div class="ec-container-cart ec_empty_cart ec_empty_wishlist">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/ec_empty_cart.svg" alt="<?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
" class="ec-empty-cart-image">
        <p class="ty-no-items_text"><?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
</p>
        <span class="ec_sub_text"><?php echo $_smarty_tpl->__("ec_what_are_you_waiting");?>
</span>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/continue_shopping.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>fn_url($_smarty_tpl->tpl_vars['continue_url']->value),'but_role'=>"text"), 0);?>

    </div>
<?php }?>



<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("wishlist_content");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/wishlist/views/wishlist/view.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/wishlist/views/wishlist/view.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars["columns"] = new Smarty_variable(4, null, 0);?>
<?php if (!$_smarty_tpl->tpl_vars['wishlist_is_empty']->value) {?>

    <?php echo smarty_function_script(array('src'=>"js/tygh/exceptions.js"),$_smarty_tpl);?>


    <?php $_smarty_tpl->tpl_vars["show_hr"] = new Smarty_variable(false, null, 0);?>
    <?php $_smarty_tpl->tpl_vars["location"] = new Smarty_variable("cart", null, 0);?>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['products']->value||$_REQUEST['wishlist_filter']) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/wishlist/views/wishlist/components/wishlist_grid.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('columns'=>$_smarty_tpl->tpl_vars['columns']->value,'show_empty'=>true,'show_name'=>true,'show_old_price'=>true,'show_price'=>true,'show_clean_price'=>true,'show_list_discount'=>true,'no_pagination'=>false,'no_sorting'=>false,'show_features'=>true,'show_add_to_cart'=>true,'is_wishlist'=>true), 0);?>

<?php } else { ?>
    <div class="ec-container-cart ec_empty_cart ec_empty_wishlist">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/ec_empty_cart.svg" alt="<?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
" class="ec-empty-cart-image">
        <p class="ty-no-items_text"><?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
</p>
        <span class="ec_sub_text"><?php echo $_smarty_tpl->__("ec_what_are_you_waiting");?>
</span>
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/continue_shopping.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>fn_url($_smarty_tpl->tpl_vars['continue_url']->value),'but_role'=>"text"), 0);?>

    </div>
<?php }?>



<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("wishlist_content");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?><?php }} ?>
