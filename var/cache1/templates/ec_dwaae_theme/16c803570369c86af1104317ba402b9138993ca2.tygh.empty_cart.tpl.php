<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 15:13:17
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/checkout/components/empty_cart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17097691536093cf4d2c1071-67756305%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '16c803570369c86af1104317ba402b9138993ca2' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/checkout/components/empty_cart.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '17097691536093cf4d2c1071-67756305',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'config' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6093cf4d2ceaf7_48488688',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6093cf4d2ceaf7_48488688')) {function content_6093cf4d2ceaf7_48488688($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('text_cart_empty','text_cart_empty','ec_no_items_sub_text','text_cart_empty','text_cart_empty','ec_no_items_sub_text'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec-container-cart ec_empty_cart">
    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/ec_empty_cart.svg" alt="<?php echo $_smarty_tpl->__("text_cart_empty");?>
" class="ec-empty-cart-image">
    <p class="ty-no-items_text"><?php echo $_smarty_tpl->__("text_cart_empty");?>
</p>
    <span class="ec_sub_text"><?php echo $_smarty_tpl->__("ec_no_items_sub_text");?>
</span>
    
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/checkout/components/empty_cart.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/checkout/components/empty_cart.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec-container-cart ec_empty_cart">
    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/ec_empty_cart.svg" alt="<?php echo $_smarty_tpl->__("text_cart_empty");?>
" class="ec-empty-cart-image">
    <p class="ty-no-items_text"><?php echo $_smarty_tpl->__("text_cart_empty");?>
</p>
    <span class="ec_sub_text"><?php echo $_smarty_tpl->__("ec_no_items_sub_text");?>
</span>
    
</div><?php }?><?php }} ?>
