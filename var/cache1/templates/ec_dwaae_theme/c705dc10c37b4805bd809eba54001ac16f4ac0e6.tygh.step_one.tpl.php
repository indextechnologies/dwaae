<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 09:43:11
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/steps/step_one.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1135126056609381ef896318-23485264%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c705dc10c37b4805bd809eba54001ac16f4ac0e6' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/steps/step_one.tpl',
      1 => 1615613845,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1135126056609381ef896318-23485264',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'cart' => 0,
    'redirect_url' => 0,
    'delivery_address' => 0,
    'profile_id' => 0,
    'card_select' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_609381ef8d2d88_91693269',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_609381ef8d2d88_91693269')) {function content_609381ef8d2d88_91693269($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('add_new_address','next','error','add_or_select_address_to_continue','error','selected_shipping_details_are_not_complete','add_new_address','next','error','add_or_select_address_to_continue','error','selected_shipping_details_are_not_complete'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_step_container" id="step_one">
    <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
    <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=checkout.checkout&ec_edit_step=ec_step_two", null, 0);?>
    <?php } else { ?>
    <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=checkout.checkout&ec_edit_step=ec_step_three", null, 0);?>
    <?php }?>

    <form name="ec_deliver_address_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
        <input type="hidden" name="result_ids" value="checkout_steps" />
        <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
        <input type="hidden" name="ec_edit_step" value="ec_step_two" />
        <?php } else { ?>
        <input type="hidden" name="ec_edit_step" value="ec_step_three" />
        <?php }?>
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />

        <div class="ec_profile_container">
            <?php  $_smarty_tpl->tpl_vars['user_profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user_profile']->_loop = false;
 $_smarty_tpl->tpl_vars['profile_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['delivery_address']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["shipping_addesss"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['user_profile']->key => $_smarty_tpl->tpl_vars['user_profile']->value) {
$_smarty_tpl->tpl_vars['user_profile']->_loop = true;
 $_smarty_tpl->tpl_vars['profile_id']->value = $_smarty_tpl->tpl_vars['user_profile']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["shipping_addesss"]['iteration']++;
?>
                <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(false, null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['selected_profile']==$_smarty_tpl->tpl_vars['profile_id']->value||$_smarty_tpl->getVariable('smarty')->value['foreach']['shipping_addesss']['iteration']==1) {?>
                    <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('enable_address_select'=>true,'address_card_selected'=>$_smarty_tpl->tpl_vars['card_select']->value,'address_edit_page'=>"checkout.edit_delivery",'address_redirect_url'=>"checkout.checkout&ec_edit_step=ec_step_one",'address_result_ids'=>"step_one"), 0);?>

            <?php } ?>
        </div>
    <div class="ec_bottom_buttons">
        <a href="<?php echo htmlspecialchars(fn_url("checkout.edit_delivery"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render" data-ca-target-id="step_one"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_address");?>
</a>

        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[checkout.checkout]",'but_meta'=>"ty-btn__secondary ec_next_btn cm-ajax cm-ajax-full-render",'but_text'=>$_smarty_tpl->__("next")), 0);?>

    </div>
    </form>
<!--step_one--></div>

<?php echo '<script'; ?>
>
    (function(_, $) {
        $.ceEvent('on', 'ce.commoninit', function(context){
            $.ceEvent('on', 'ce.formpre_ec_deliver_address_form', function (form, clicked_elm) {
                if(clicked_elm.attr('name') == "dispatch[checkout.checkout]"){
                    var selectedShippingObj = $('[name="selected_profile"]:checked');
                    var selected_shipping = selectedShippingObj.val();
                    if(!selected_shipping){
                        $.ceNotification('show', {
                            type: 'E',
                            title: "<?php echo $_smarty_tpl->__("error");?>
",
                            message: "<?php echo $_smarty_tpl->__("add_or_select_address_to_continue");?>
"
                        });
                        return false;
                    }else{
                        var c = selectedShippingObj.data('detailsCompleted');
                        if(c != 'Y'){
                            $.ceNotification('show', {
                                type: 'E',
                                title: "<?php echo $_smarty_tpl->__("error");?>
",
                                message: "<?php echo $_smarty_tpl->__("selected_shipping_details_are_not_complete");?>
"
                            });
                            return false;
                        }
                    }
                }
            });
        });
    }(Tygh, Tygh.$));
<?php echo '</script'; ?>
><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/views/ec_checkout/components/steps/step_one.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/views/ec_checkout/components/steps/step_one.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_step_container" id="step_one">
    <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
    <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=checkout.checkout&ec_edit_step=ec_step_two", null, 0);?>
    <?php } else { ?>
    <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=checkout.checkout&ec_edit_step=ec_step_three", null, 0);?>
    <?php }?>

    <form name="ec_deliver_address_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
        <input type="hidden" name="result_ids" value="checkout_steps" />
        <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
        <input type="hidden" name="ec_edit_step" value="ec_step_two" />
        <?php } else { ?>
        <input type="hidden" name="ec_edit_step" value="ec_step_three" />
        <?php }?>
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />

        <div class="ec_profile_container">
            <?php  $_smarty_tpl->tpl_vars['user_profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user_profile']->_loop = false;
 $_smarty_tpl->tpl_vars['profile_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['delivery_address']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["shipping_addesss"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['user_profile']->key => $_smarty_tpl->tpl_vars['user_profile']->value) {
$_smarty_tpl->tpl_vars['user_profile']->_loop = true;
 $_smarty_tpl->tpl_vars['profile_id']->value = $_smarty_tpl->tpl_vars['user_profile']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["shipping_addesss"]['iteration']++;
?>
                <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(false, null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['selected_profile']==$_smarty_tpl->tpl_vars['profile_id']->value||$_smarty_tpl->getVariable('smarty')->value['foreach']['shipping_addesss']['iteration']==1) {?>
                    <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('enable_address_select'=>true,'address_card_selected'=>$_smarty_tpl->tpl_vars['card_select']->value,'address_edit_page'=>"checkout.edit_delivery",'address_redirect_url'=>"checkout.checkout&ec_edit_step=ec_step_one",'address_result_ids'=>"step_one"), 0);?>

            <?php } ?>
        </div>
    <div class="ec_bottom_buttons">
        <a href="<?php echo htmlspecialchars(fn_url("checkout.edit_delivery"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render" data-ca-target-id="step_one"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_address");?>
</a>

        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[checkout.checkout]",'but_meta'=>"ty-btn__secondary ec_next_btn cm-ajax cm-ajax-full-render",'but_text'=>$_smarty_tpl->__("next")), 0);?>

    </div>
    </form>
<!--step_one--></div>

<?php echo '<script'; ?>
>
    (function(_, $) {
        $.ceEvent('on', 'ce.commoninit', function(context){
            $.ceEvent('on', 'ce.formpre_ec_deliver_address_form', function (form, clicked_elm) {
                if(clicked_elm.attr('name') == "dispatch[checkout.checkout]"){
                    var selectedShippingObj = $('[name="selected_profile"]:checked');
                    var selected_shipping = selectedShippingObj.val();
                    if(!selected_shipping){
                        $.ceNotification('show', {
                            type: 'E',
                            title: "<?php echo $_smarty_tpl->__("error");?>
",
                            message: "<?php echo $_smarty_tpl->__("add_or_select_address_to_continue");?>
"
                        });
                        return false;
                    }else{
                        var c = selectedShippingObj.data('detailsCompleted');
                        if(c != 'Y'){
                            $.ceNotification('show', {
                                type: 'E',
                                title: "<?php echo $_smarty_tpl->__("error");?>
",
                                message: "<?php echo $_smarty_tpl->__("selected_shipping_details_are_not_complete");?>
"
                            });
                            return false;
                        }
                    }
                }
            });
        });
    }(Tygh, Tygh.$));
<?php echo '</script'; ?>
><?php }?><?php }} ?>
