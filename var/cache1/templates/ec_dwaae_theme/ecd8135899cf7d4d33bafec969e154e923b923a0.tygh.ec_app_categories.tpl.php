<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 22:20:33
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/ec_dwaae_categories/ec_app_categories.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6085572896092e1f193df11-98670233%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ecd8135899cf7d4d33bafec969e154e923b923a0' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/ec_dwaae_categories/ec_app_categories.tpl',
      1 => 1605870209,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '6085572896092e1f193df11-98670233',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'block' => 0,
    'sideviewer_id' => 0,
    'items' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092e1f195c019_87355979',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092e1f195c019_87355979')) {function content_6092e1f195c019_87355979($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('categories','categories'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->_capture_stack[0][] = array("title", null, null); ob_start(); ?>
    <span></span>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->tpl_vars["sideviewer_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
<div class="ec_bottom_app_category header-left-block">
    <div class="fn-sideviewer">
    	<div class="fn-sideviewer__toggle ec_app_category_opener">
			<?php if (trim(Smarty::$_smarty_vars['capture']['title'])) {?>
				<?php echo Smarty::$_smarty_vars['capture']['title'];?>

			<?php }?>
		</div>
		<div id="sideviewer_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sideviewer_id']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" class="fn-sideviewer-wrapper">
			<div class="side-viewer-content">
				<div class="sideviewer-content-wrapper">
                    <div class="ec_app_category_top">
                        <span class="ec_left">
                            <span class="ec-icon-cat_all"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>

                            <span class="header_text"><?php echo $_smarty_tpl->__("categories");?>
</span>
                        </span>
                        <span class="sideviewer-close"><i class="ec-icon-close"></i></span>
                    </div>
                    <div class="ec_category_content">
                        <?php echo $_smarty_tpl->getSubTemplate ("blocks/ec_dwaae_categories/components/ec_category_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('items'=>$_smarty_tpl->tpl_vars['items']->value,'item1_url'=>true,'name'=>"category",'item_id'=>"param_id",'childs'=>"subcategories"), 0);?>

                    </div>
                </div>
			</div>
		</div>	
		<div class="sideviewer-overlay"></div>
	</div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/ec_dwaae_categories/ec_app_categories.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/ec_dwaae_categories/ec_app_categories.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->_capture_stack[0][] = array("title", null, null); ob_start(); ?>
    <span></span>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->tpl_vars["sideviewer_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
<div class="ec_bottom_app_category header-left-block">
    <div class="fn-sideviewer">
    	<div class="fn-sideviewer__toggle ec_app_category_opener">
			<?php if (trim(Smarty::$_smarty_vars['capture']['title'])) {?>
				<?php echo Smarty::$_smarty_vars['capture']['title'];?>

			<?php }?>
		</div>
		<div id="sideviewer_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sideviewer_id']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" class="fn-sideviewer-wrapper">
			<div class="side-viewer-content">
				<div class="sideviewer-content-wrapper">
                    <div class="ec_app_category_top">
                        <span class="ec_left">
                            <span class="ec-icon-cat_all"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>

                            <span class="header_text"><?php echo $_smarty_tpl->__("categories");?>
</span>
                        </span>
                        <span class="sideviewer-close"><i class="ec-icon-close"></i></span>
                    </div>
                    <div class="ec_category_content">
                        <?php echo $_smarty_tpl->getSubTemplate ("blocks/ec_dwaae_categories/components/ec_category_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('items'=>$_smarty_tpl->tpl_vars['items']->value,'item1_url'=>true,'name'=>"category",'item_id'=>"param_id",'childs'=>"subcategories"), 0);?>

                    </div>
                </div>
			</div>
		</div>	
		<div class="sideviewer-overlay"></div>
	</div>
</div><?php }?><?php }} ?>
