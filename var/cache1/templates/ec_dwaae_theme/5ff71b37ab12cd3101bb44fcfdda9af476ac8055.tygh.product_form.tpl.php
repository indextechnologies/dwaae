<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:36:33
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/components/product_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14983442466092bb81019f08-78315208%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5ff71b37ab12cd3101bb44fcfdda9af476ac8055' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/components/product_form.tpl',
      1 => 1607955884,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '14983442466092bb81019f08-78315208',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'sqid' => 0,
    'name_var' => 0,
    'name' => 0,
    'product' => 0,
    'quantity_unit' => 0,
    'unit' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092bb810711b4_86927490',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092bb810711b4_86927490')) {function content_6092bb810711b4_86927490($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('h_rfq.delete_product','h_rfq.product_name','h_rfq.product_name_placeholder','h_rfq.product_qty','h_rfq.product_qty_placeholder','h_rfq.unit','h_rfq.select_quantity_unit','none','h_rfq.product_description','h_rfq.type_any_extra_requirements','h_rfq.delete_product','h_rfq.product_name','h_rfq.product_name_placeholder','h_rfq.product_qty','h_rfq.product_qty_placeholder','h_rfq.unit','h_rfq.select_quantity_unit','none','h_rfq.product_description','h_rfq.type_any_extra_requirements'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (!$_smarty_tpl->tpl_vars['sqid']->value) {?>
    <?php $_smarty_tpl->tpl_vars['sqid'] = new Smarty_variable(rand(100000,999999), null, 0);?>
<?php }?>
<?php $_smarty_tpl->tpl_vars['name'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['name_var']->value)===null||$tmp==='' ? "quotation[product]" : $tmp), null, 0);?>
<div class="h-rfq_product" data-sqid="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <div class="h-rfq_product_header">
        <span class="h_product_heading">Product</span>
        <span class="h-rfq_product_actions_buttons">
            
            <span class="h-rfq_product_actions_button h_remove_product"><?php echo $_smarty_tpl->__("h_rfq.delete_product");?>
</span>
        </span>
    </div>
    
    <div class="h-rfq_product_data">
        <div class="ty-control-group">
            <label for="el_product_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.product_name");?>
</label>
            <input type="text" id="el_product_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][product_name]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_name'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text cm-product-list-search h-rfq_product_name" page-no="1" placeholder="<?php echo $_smarty_tpl->__("h_rfq.product_name_placeholder");?>
">
        </div>
        <div class="ty-control-group">
            <label for="el_product_qty_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.product_qty");?>
</label>
            <input type="number" min="1" id="el_product_qty_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][quantity]" size="20" maxlength="20" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['product']->value['quantity'])===null||$tmp==='' ? 1 : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" placeholder="<?php echo $_smarty_tpl->__("h_rfq.product_qty_placeholder");?>
">
        </div>
        <div class="ty-control-group">
            <label for="el_product_unit_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title "><?php echo $_smarty_tpl->__("h_rfq.unit");?>
</label>
            <select id="el_product_unit_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][quantity_unit]" class="cm-quanity-unit-selectbox h-rfq_selecbox_placeholder">
                <option value="" disabled selected hidden><?php echo $_smarty_tpl->__("h_rfq.select_quantity_unit");?>
</option>
                <option value=""><?php echo $_smarty_tpl->__("none");?>
</option>
                <?php  $_smarty_tpl->tpl_vars['unit'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['unit']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['quantity_unit']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['unit']->key => $_smarty_tpl->tpl_vars['unit']->value) {
$_smarty_tpl->tpl_vars['unit']->_loop = true;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unit']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['product']->value['quantity_unit']==$_smarty_tpl->tpl_vars['unit']->value) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unit']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
        <div class="ty-control-group">
            <label for="el_product_desc_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.product_description");?>
</label>
            <textarea id="el_product_desc_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" rows="4" cols="50" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][description]" placeholder="<?php echo $_smarty_tpl->__("h_rfq.type_any_extra_requirements");?>
" class="h-rfq_product_description"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['description'], ENT_QUOTES, 'UTF-8');?>
</textarea>
        </div>
        
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/h_rfq/views/h_rfq/components/product_form.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/h_rfq/views/h_rfq/components/product_form.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (!$_smarty_tpl->tpl_vars['sqid']->value) {?>
    <?php $_smarty_tpl->tpl_vars['sqid'] = new Smarty_variable(rand(100000,999999), null, 0);?>
<?php }?>
<?php $_smarty_tpl->tpl_vars['name'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['name_var']->value)===null||$tmp==='' ? "quotation[product]" : $tmp), null, 0);?>
<div class="h-rfq_product" data-sqid="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <div class="h-rfq_product_header">
        <span class="h_product_heading">Product</span>
        <span class="h-rfq_product_actions_buttons">
            
            <span class="h-rfq_product_actions_button h_remove_product"><?php echo $_smarty_tpl->__("h_rfq.delete_product");?>
</span>
        </span>
    </div>
    
    <div class="h-rfq_product_data">
        <div class="ty-control-group">
            <label for="el_product_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.product_name");?>
</label>
            <input type="text" id="el_product_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][product_name]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_name'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text cm-product-list-search h-rfq_product_name" page-no="1" placeholder="<?php echo $_smarty_tpl->__("h_rfq.product_name_placeholder");?>
">
        </div>
        <div class="ty-control-group">
            <label for="el_product_qty_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.product_qty");?>
</label>
            <input type="number" min="1" id="el_product_qty_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][quantity]" size="20" maxlength="20" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['product']->value['quantity'])===null||$tmp==='' ? 1 : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" placeholder="<?php echo $_smarty_tpl->__("h_rfq.product_qty_placeholder");?>
">
        </div>
        <div class="ty-control-group">
            <label for="el_product_unit_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title "><?php echo $_smarty_tpl->__("h_rfq.unit");?>
</label>
            <select id="el_product_unit_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][quantity_unit]" class="cm-quanity-unit-selectbox h-rfq_selecbox_placeholder">
                <option value="" disabled selected hidden><?php echo $_smarty_tpl->__("h_rfq.select_quantity_unit");?>
</option>
                <option value=""><?php echo $_smarty_tpl->__("none");?>
</option>
                <?php  $_smarty_tpl->tpl_vars['unit'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['unit']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['quantity_unit']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['unit']->key => $_smarty_tpl->tpl_vars['unit']->value) {
$_smarty_tpl->tpl_vars['unit']->_loop = true;
?>
                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unit']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['product']->value['quantity_unit']==$_smarty_tpl->tpl_vars['unit']->value) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unit']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
        <div class="ty-control-group">
            <label for="el_product_desc_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.product_description");?>
</label>
            <textarea id="el_product_desc_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" rows="4" cols="50" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][description]" placeholder="<?php echo $_smarty_tpl->__("h_rfq.type_any_extra_requirements");?>
" class="h-rfq_product_description"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['description'], ENT_QUOTES, 'UTF-8');?>
</textarea>
        </div>
        
    </div>
</div><?php }?><?php }} ?>
