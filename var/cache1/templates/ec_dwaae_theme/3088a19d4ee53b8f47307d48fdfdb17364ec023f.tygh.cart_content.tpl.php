<?php /* Smarty version Smarty-3.1.21, created on 2021-05-07 18:53:51
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/checkout/components/cart_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4854495736095547f6ad632-37271167%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3088a19d4ee53b8f47307d48fdfdb17364ec023f' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/checkout/components/cart_content.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '4854495736095547f6ad632-37271167',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'count' => 0,
    'redirect_url' => 0,
    'config' => 0,
    'result_ids' => 0,
    'payment_methods' => 0,
    'checkout_add_buttons' => 0,
    'checkout_add_button' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6095547f70cf78_93507404',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6095547f70cf78_93507404')) {function content_6095547f70cf78_93507404($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_my_cart','item_in_cart','ec_cart_bottom_text','or_use','ec_my_cart','item_in_cart','ec_cart_bottom_text','or_use'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars["result_ids"] = new Smarty_variable("cart*,checkout*,cart_items,checkout_totals,cart_status*,checkout_steps,checkout_cart", null, 0);?>
<div class="ec_cart_header" id="checkout_header">
    <div class="span12"><?php echo $_smarty_tpl->__("ec_my_cart");?>
</div>
    <div class="span4"><?php if ($_SESSION['cart']['amount']<9) {?> <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable($_SESSION['cart']['amount'], null, 0);?> <?php } else { ?> <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable('9+', null, 0);?> <?php }?>
        <?php echo $_smarty_tpl->__("item_in_cart",array("[count]"=>$_smarty_tpl->tpl_vars['count']->value));?>

    <!--checkout_header--></div>    
</div>
<div class="ec_cart_content_wrapper">
    <div class="span12">
    <form name="checkout_form" class="cm-check-changes cm-ajax cm-ajax-full-render" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data" id="checkout_form">
    <input type="hidden" name="redirect_mode" value="cart" />
    <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['redirect_url']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
    <input type="hidden" name="result_ids" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
" />


    <div class="buttons-container ty-cart-content__top-buttons clearfix hidden">
        <div class="ty-float-right ty-cart-content__right-buttons">
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/update_cart.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_id'=>"button_cart",'but_meta'=>"ty-btn--recalculate-cart hidden hidden-phone hidden-tablet",'but_name'=>"dispatch[checkout.update]"), 0);?>

            <?php if ($_smarty_tpl->tpl_vars['payment_methods']->value) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/proceed_to_checkout.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php }?>
        </div>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/cart_items.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('disable_ids'=>"button_cart"), 0);?>


    <!--checkout_form--></form>
    <div class="ec_cart_text"><?php echo $_smarty_tpl->__("ec_cart_bottom_text");?>
</div>
    </div>
    <div class="span4 ec_cart_total">
        <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/checkout_totals.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('location'=>"cart"), 0);?>

        
        <?php if ($_smarty_tpl->tpl_vars['checkout_add_buttons']->value) {?>
            <div class="ty-cart-content__payment-methods payment-methods" id="payment-methods">
                <span class="ty-cart-content__payment-methods-title payment-metgods-or"><?php echo $_smarty_tpl->__("or_use");?>
</span>
                <table class="ty-cart-content__payment-methods-block">
                    <tr>
                        <?php  $_smarty_tpl->tpl_vars["checkout_add_button"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["checkout_add_button"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['checkout_add_buttons']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["checkout_add_button"]->key => $_smarty_tpl->tpl_vars["checkout_add_button"]->value) {
$_smarty_tpl->tpl_vars["checkout_add_button"]->_loop = true;
?>
                            <td class="ty-cart-content__payment-methods-item"><?php echo $_smarty_tpl->tpl_vars['checkout_add_button']->value;?>
</td>
                        <?php } ?>
                    </tr>
            </table>
            <!--payment-methods--></div>
        <?php }?>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/checkout/components/cart_content.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/checkout/components/cart_content.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars["result_ids"] = new Smarty_variable("cart*,checkout*,cart_items,checkout_totals,cart_status*,checkout_steps,checkout_cart", null, 0);?>
<div class="ec_cart_header" id="checkout_header">
    <div class="span12"><?php echo $_smarty_tpl->__("ec_my_cart");?>
</div>
    <div class="span4"><?php if ($_SESSION['cart']['amount']<9) {?> <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable($_SESSION['cart']['amount'], null, 0);?> <?php } else { ?> <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable('9+', null, 0);?> <?php }?>
        <?php echo $_smarty_tpl->__("item_in_cart",array("[count]"=>$_smarty_tpl->tpl_vars['count']->value));?>

    <!--checkout_header--></div>    
</div>
<div class="ec_cart_content_wrapper">
    <div class="span12">
    <form name="checkout_form" class="cm-check-changes cm-ajax cm-ajax-full-render" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data" id="checkout_form">
    <input type="hidden" name="redirect_mode" value="cart" />
    <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['redirect_url']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
    <input type="hidden" name="result_ids" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
" />


    <div class="buttons-container ty-cart-content__top-buttons clearfix hidden">
        <div class="ty-float-right ty-cart-content__right-buttons">
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/update_cart.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_id'=>"button_cart",'but_meta'=>"ty-btn--recalculate-cart hidden hidden-phone hidden-tablet",'but_name'=>"dispatch[checkout.update]"), 0);?>

            <?php if ($_smarty_tpl->tpl_vars['payment_methods']->value) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/proceed_to_checkout.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php }?>
        </div>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/cart_items.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('disable_ids'=>"button_cart"), 0);?>


    <!--checkout_form--></form>
    <div class="ec_cart_text"><?php echo $_smarty_tpl->__("ec_cart_bottom_text");?>
</div>
    </div>
    <div class="span4 ec_cart_total">
        <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/checkout_totals.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('location'=>"cart"), 0);?>

        
        <?php if ($_smarty_tpl->tpl_vars['checkout_add_buttons']->value) {?>
            <div class="ty-cart-content__payment-methods payment-methods" id="payment-methods">
                <span class="ty-cart-content__payment-methods-title payment-metgods-or"><?php echo $_smarty_tpl->__("or_use");?>
</span>
                <table class="ty-cart-content__payment-methods-block">
                    <tr>
                        <?php  $_smarty_tpl->tpl_vars["checkout_add_button"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["checkout_add_button"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['checkout_add_buttons']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["checkout_add_button"]->key => $_smarty_tpl->tpl_vars["checkout_add_button"]->value) {
$_smarty_tpl->tpl_vars["checkout_add_button"]->_loop = true;
?>
                            <td class="ty-cart-content__payment-methods-item"><?php echo $_smarty_tpl->tpl_vars['checkout_add_button']->value;?>
</td>
                        <?php } ?>
                    </tr>
            </table>
            <!--payment-methods--></div>
        <?php }?>
    </div>
</div><?php }?><?php }} ?>
