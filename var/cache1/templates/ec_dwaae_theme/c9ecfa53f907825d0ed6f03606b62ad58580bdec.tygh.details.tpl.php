<?php /* Smarty version Smarty-3.1.21, created on 2021-06-03 12:45:46
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/details.tpl" */ ?>
<?php /*%%SmartyHeaderCode:36752582660b896ba3b40b2-47483860%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c9ecfa53f907825d0ed6f03606b62ad58580bdec' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/details.tpl',
      1 => 1607575547,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '36752582660b896ba3b40b2-47483860',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'rfq_id' => 0,
    'quotation' => 0,
    'settings' => 0,
    'print_quote' => 0,
    'print_order' => 0,
    'config' => 0,
    'bank_transfer_description' => 0,
    'curl' => 0,
    'selected_section' => 0,
    'delivery_location' => 0,
    'product' => 0,
    'attachment' => 0,
    'order_info' => 0,
    'discussion' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60b896ba4d7a11_33109649',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60b896ba4d7a11_33109649')) {function content_60b896ba4d7a11_33109649($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_modifier_enum')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.enum.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('h_rfq.quotation_detail_header','h_rfq.valid_till','status','h_rfq.rfq_id','print_invoice','h_rfq.print_quote','h_rfq.note','h_rfq.further_instuction_for_bank_tranfer','h_rfq.remittance_advice','h_rfq.upload_remittance_advice','h_rfq.do_bank_transfer','h_rfq.do_bank_transfer','h_rfq.note','h_rfq.provide_the_lpo_for_cod_order','h_rfq.upload_lpo','h_rfq.submit_lpo','h_rfq.submit_lpo','h_rfq.submit_lpo','h_rfq.customer_information','h_rfq.buyer_details','h_rfq.delivery_location','h_rfq.products','h_rfq.product_name','h_rfq.product_qty','h_rfq.price','h_rfq.total_before_vat','h_rfq.vat','h_rfq.vat','total','h_rfq.read_more','h_rfq.read_hide','h_rfq.product_description','sku','h_rfq.product_attachments','h_rfq.attachments','summary','payment_method','h_rfq.recommended_payment_method','h_rfq.payment_reference','h_rfq.remittance_advice','h_rfq.lpo','h_rfq.delivey_period','subtotal','h_rfq.vat','total','discussion_title_order','h_rfq.quotation_detail_header','h_rfq.valid_till','status','h_rfq.rfq_id','print_invoice','h_rfq.print_quote','h_rfq.note','h_rfq.further_instuction_for_bank_tranfer','h_rfq.remittance_advice','h_rfq.upload_remittance_advice','h_rfq.do_bank_transfer','h_rfq.do_bank_transfer','h_rfq.note','h_rfq.provide_the_lpo_for_cod_order','h_rfq.upload_lpo','h_rfq.submit_lpo','h_rfq.submit_lpo','h_rfq.submit_lpo','h_rfq.customer_information','h_rfq.buyer_details','h_rfq.delivery_location','h_rfq.products','h_rfq.product_name','h_rfq.product_qty','h_rfq.price','h_rfq.total_before_vat','h_rfq.vat','h_rfq.vat','total','h_rfq.read_more','h_rfq.read_hide','h_rfq.product_description','sku','h_rfq.product_attachments','h_rfq.attachments','summary','payment_method','h_rfq.recommended_payment_method','h_rfq.payment_reference','h_rfq.remittance_advice','h_rfq.lpo','h_rfq.delivey_period','subtotal','h_rfq.vat','total','discussion_title_order'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->__("h_rfq.quotation_detail_header",array('[id]'=>$_smarty_tpl->tpl_vars['rfq_id']->value));?>
</bdi>
    <em class="ty-date">(<?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
)</em>
    <em class="ty-status"> &nbsp;(<?php echo $_smarty_tpl->__("h_rfq.valid_till");?>
: <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['valid_till'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
)</em>
    <em class="ty-status"><?php echo $_smarty_tpl->__("status");?>
: <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['quotation']->value['status'],'display'=>"view",'name'=>"update_order[status]"), 0);?>
</em>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<style>
    .ty-orders-detail .ec_top_bar .ec_right_div a > span:first-child{
        display:block;
    }
</style>
<div class="ty-orders-detail">
    <?php if ($_smarty_tpl->tpl_vars['quotation']->value) {?>
        <?php $_smarty_tpl->_capture_stack[0][] = array("quotations_actions", null, null); ob_start(); ?>
            <div class="ec_top_bar">

                <div class="ec_left_div">
                    <span class="ty-product-filters__title"><?php echo $_smarty_tpl->__("h_rfq.rfq_id");?>
 #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
</span>
                    <span class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
                    
                </div>
                <?php $_smarty_tpl->tpl_vars["print_order"] = new Smarty_variable($_smarty_tpl->__("print_invoice"), null, 0);?>
                <?php $_smarty_tpl->tpl_vars["print_quote"] = new Smarty_variable($_smarty_tpl->__("h_rfq.print_quote"), null, 0);?>
                
                <div class="ec_right_div">
                    <?php if (!@constant('EC_MOBILE_APP')) {?>
                    <?php if (fn_document_to_access($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'],'default')) {?>
                        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_meta'=>"orders-print__pdf ty-btn__text cm-no-ajax cm-new-window",'but_text'=>$_smarty_tpl->tpl_vars['print_quote']->value,'but_href'=>"h_rfq_invoice.print_invoice&rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&format=pdf",'but_icon'=>"ty-icon-doc-text orders-print__icon"), 0);?>

                    <?php }?>
                    
                    <?php if (fn_document_to_access($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'],'invoice')) {?>
                        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_text'=>$_smarty_tpl->tpl_vars['print_order']->value,'but_href'=>"h_rfq_invoice.print_final_invoice&rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&format=pdf",'but_meta'=>" ty-btn__text cm-new-window",'but_icon'=>"ty-icon-print orders-print__icon"), 0);?>

                    <?php }?>
                    <?php }?>
                    <?php if (fn_h_rfq_allow_to_pay($_smarty_tpl->tpl_vars['quotation']->value['status'])) {?>
                        <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']==smarty_modifier_enum("RfqPaymentTypes::CREDIT_AND_DEBIT_CARD")) {?>
                            <?php $_smarty_tpl->_capture_stack[0][] = array("bank_transfer", null, null); ob_start(); ?>
                                <div class="">
                                    <form name="rfq_form" action="<?php echo htmlspecialchars(fn_url(), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
                                        <input type="hidden" name="rfq_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" />
                                        <div><?php echo $_smarty_tpl->tpl_vars['bank_transfer_description']->value;?>
</div>
                                        <div><strong><?php echo $_smarty_tpl->__("h_rfq.note");?>
: </strong><?php echo $_smarty_tpl->__("h_rfq.further_instuction_for_bank_tranfer");?>
</div>
                                        <div class="ty-control-group">
                                            <label for="bank_statements_id" class="ty-control-group__title cm-required">
                                            <?php echo $_smarty_tpl->__("h_rfq.remittance_advice");?>

                                            </label>
                                            <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"bank_statement[]",'multiupload'=>false,'label_id'=>"bank_statements_id"), 0);?>

                                        </div>
                                        <div class="buttons-container ty-right">
                                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit",'but_meta'=>"ty-btn__primary",'but_text'=>$_smarty_tpl->__("h_rfq.upload_remittance_advice"),'but_name'=>"dispatch[h_rfq.upload_bank_statement]",'but_icon'=>''), 0);?>

                                        </div>
                                    </form>
                                </div>
                            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"bank_transfer",'link_text'=>$_smarty_tpl->__("h_rfq.do_bank_transfer"),'text'=>$_smarty_tpl->__("h_rfq.do_bank_transfer"),'content'=>Smarty::$_smarty_vars['capture']['bank_transfer'],'act'=>"general",'link_meta'=>"ty-btn ty-btn__primary ec-imp-white_color cm-dialog-auto-size",'link_icon'=>"icon-plus"), 0);?>

                        
                            
                        <?php } elseif ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']==smarty_modifier_enum("RfqPaymentTypes::COD")) {?>
                            <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
                            <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['curl']->value), null, 0);?>
                            <?php $_smarty_tpl->_capture_stack[0][] = array("cod_popup", null, null); ob_start(); ?>
                                <div class="">
                                    <form name="rfq_form" action="<?php echo htmlspecialchars(fn_url(), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
                                        <input type="hidden" name="rfq_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" />
                                        <div><strong><?php echo $_smarty_tpl->__("h_rfq.note");?>
: </strong><?php echo $_smarty_tpl->__("h_rfq.provide_the_lpo_for_cod_order");?>
</div>
                                        <div class="ty-control-group">
                                            <label for="cod_lpo_id" class="ty-control-group__title cm-required">
                                            <?php echo $_smarty_tpl->__("h_rfq.upload_lpo");?>

                                            </label>
                                            <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"lpo_documents[]",'multiupload'=>false,'label_id'=>"cod_lpo_id"), 0);?>

                                        </div>
                                        <div class="buttons-container ty-right">
                                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit",'but_meta'=>"ty-btn__primary",'but_text'=>$_smarty_tpl->__("h_rfq.submit_lpo"),'but_name'=>"dispatch[h_rfq.upload_lpo]",'but_icon'=>''), 0);?>

                                        </div>
                                    </form>
                                </div>
                            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                            
                            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"bank_transfer",'link_text'=>$_smarty_tpl->__("h_rfq.submit_lpo"),'text'=>$_smarty_tpl->__("h_rfq.submit_lpo"),'content'=>Smarty::$_smarty_vars['capture']['cod_popup'],'act'=>"general",'link_meta'=>"ty-btn ty-btn__primary ec-imp-white_color cm-dialog-auto-size",'link_icon'=>"icon-plus"), 0);?>

                        <?php }?>
                    <?php }?>
                </div>
            </div>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

        <?php $_smarty_tpl->_capture_stack[0][] = array("tabsbox", null, null); ob_start(); ?>
            <div id="content_general" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value&&$_smarty_tpl->tpl_vars['selected_section']->value!="general") {?>hidden<?php }?>" style="background: white; border: 1px solid #c6c6c5; padding: 15px;">

                <div class="orders-customer" style="display:block;">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("h_rfq.customer_information")), 0);?>

                    <div class="ty-profiles-info">
                        <div class="ty-profiles-info__item ty-profiles-info__billing">
                            <h5 class="ty-profiles-info__title"><?php echo $_smarty_tpl->__("h_rfq.buyer_details");?>
</h5>
                            <div class="ty-profiles-info__field">
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['email'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                            </div>
                        </div>

                        <?php $_smarty_tpl->tpl_vars['delivery_location'] = new Smarty_variable(reset($_smarty_tpl->tpl_vars['quotation']->value['locations']), null, 0);?>
                        <div class="ty-profiles-info__item ty-profiles-info__shipping">
                            <h5 class="ty-profiles-info__title"><?php echo $_smarty_tpl->__("h_rfq.delivery_location");?>
</h5>
                            <div class="ty-profiles-info__field">
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['address'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['city'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars(fn_get_state_name($_smarty_tpl->tpl_vars['delivery_location']->value['state'],$_smarty_tpl->tpl_vars['delivery_location']->value['country']), ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars(fn_get_country_name($_smarty_tpl->tpl_vars['delivery_location']->value['country']), ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['zipcode'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php $_smarty_tpl->_capture_stack[0][] = array("group", null, null); ob_start(); ?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("h_rfq.products")), 0);?>

                    <table class="ty-orders-detail__table ty-table">
                        <thead>
                            <tr>
                                <th class="ty-orders-detail__table-product"><?php echo $_smarty_tpl->__("h_rfq.product_name");?>
</th>
                                <th class="ty-orders-detail__table-quantity"><?php echo $_smarty_tpl->__("h_rfq.product_qty");?>
</th>
                                <th class="ty-center"><?php echo $_smarty_tpl->__("h_rfq.price");?>
</th>
                                <th class="ty-center"><?php echo $_smarty_tpl->__("h_rfq.total_before_vat");?>
</th>
                                <th class="ty-center"><?php echo $_smarty_tpl->__("h_rfq.vat");?>
%</th>
                                <th class="ty-center"><?php echo $_smarty_tpl->__("h_rfq.vat");?>
</th>
                                <th class="ty-orders-detail__table-subtotal"><?php echo $_smarty_tpl->__("total");?>
</th>
                            </tr>
                        </thead>
                        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['quotation']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                            <?php if ($_smarty_tpl->tpl_vars['product']->value) {?>
                                <tr class="ty-valign-top">
                                    <td>
                                        <div class="clearfix">

                                            <div class="ty-overflow-hidden ty-orders-detail__table-description-wrapper">
                                                <div class="ty-ml-s ty-orders-detail__table-description">
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?><a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php }?>
                                                        <strong><?php echo $_smarty_tpl->tpl_vars['product']->value['product_name'];?>
</strong>
                                                        <div class="cm-show_hide_text h-rfq_details_description" data-ca-charqty="150" data-ca-ellipse-text=".." data-ca-more-text="<?php echo $_smarty_tpl->__("h_rfq.read_more");?>
" data-ca-less-text="<?php echo $_smarty_tpl->__("h_rfq.read_hide");?>
"><?php echo $_smarty_tpl->__("h_rfq.product_description");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['description'], ENT_QUOTES, 'UTF-8');?>
</div>
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?></a><?php }?>  
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['product_code']) {?>
                                                        <div class="ty-orders-detail__table-code"><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</div>
                                                    <?php }?>
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['attachments']) {?>
                                                        <?php $_smarty_tpl->tpl_vars['attachment'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['attachments'], null, 0);?>
                                                        <div class="ty-orders-detail__table-code"><?php echo $_smarty_tpl->__("h_rfq.product_attachments");?>
:&nbsp;<a target="_blank" href="<?php echo htmlspecialchars(fn_url("h_rfq_attachments.download_product?file=".((string)$_smarty_tpl->tpl_vars['attachment']->value['file'])), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attachment']->value['file'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['attachment']->value['name'],50), ENT_QUOTES, 'UTF-8');?>
</a></div>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="ty-center">&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity_unit'], ENT_QUOTES, 'UTF-8');?>
</td>
                                    <td class="ty-center">
                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['non_tax_price']>0) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['non_tax_price']), 0);
} else { ?>--<?php }?>
                                    </td>
                                    <td class="ty-center">
                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['non_tax_price_total']>0) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['non_tax_price_total']), 0);
} else { ?>--<?php }?>
                                    </td>
                                    <td class="ty-center">&nbsp;<?php if ($_smarty_tpl->tpl_vars['product']->value['vat_percentage']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['vat_percentage'], ENT_QUOTES, 'UTF-8');?>
%<?php } else { ?>--<?php }?></td>
                                    <td class="ty-center"><?php if ($_smarty_tpl->tpl_vars['product']->value['total_vat']) {?>&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['total_vat']), 0);
} else { ?>--<?php }?></td>
                                    <td class="ty-right">
                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['tax_price_total']) {?>&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_price_total']), 0);
} else { ?>--<?php }?>
                                    </td>
                                </tr>
                            <?php }?>
                        <?php } ?>
                    </table>

                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['attachments']) {?>
                    <div class="ty-quotation-attachments clearfix">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("h_rfq.attachments")), 0);?>

                        <div class="h-rfq_download_attachments">
                        <?php  $_smarty_tpl->tpl_vars['attachment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attachment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['quotation']->value['attachments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attachment']->key => $_smarty_tpl->tpl_vars['attachment']->value) {
$_smarty_tpl->tpl_vars['attachment']->_loop = true;
?>
                            <div class="h-rfq_download_attachment">
                                <a href="<?php echo htmlspecialchars(fn_url("h_rfq_attachments.download?file=".((string)$_smarty_tpl->tpl_vars['attachment']->value['file'])), ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attachment']->value['name'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['attachment']->value['name'],15), ENT_QUOTES, 'UTF-8');?>
</a>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    <?php }?>

                    <div class="ty-orders-summary clearfix">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("summary")), 0);?>


                        <div class="ty-orders-summary__right">
                            
                        </div>

                        <div class="ty-orders-summary__wrapper">
                            <table class="ty-orders-summary__table">
                                <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_id']) {?>
                                    <tr class="ty-orders-summary__row">
                                        <td><?php echo $_smarty_tpl->__("payment_method");?>
:</td>
                                        <td style="width: 57%" data-ct-orders-summary="summary-payment">
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['payment'], ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['description']) {?>(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['description'], ENT_QUOTES, 'UTF-8');?>
)<?php }?>
                                        </td>
                                    </tr>
                                <?php }?>

                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.recommended_payment_method");?>
:&nbsp;</td>
                                    <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                        <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']) {?>
                                            <?php echo htmlspecialchars(fn_rfq_payment_types($_smarty_tpl->tpl_vars['quotation']->value['payment_type']), ENT_QUOTES, 'UTF-8');?>

                                        <?php } else { ?>
                                            --
                                        <?php }?>
                                    </td>
                                </tr>

                                <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_details']) {?>
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['payment_reference']) {?>
                                    <tr class="ty-orders-summary__row">
                                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.payment_reference");?>
:&nbsp;</td>
                                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['payment_reference'], ENT_QUOTES, 'UTF-8');?>

                                        </td>
                                    </tr>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename']) {?>
                                    <tr class="ty-orders-summary__row">
                                        <td class="ty-orders-summary__total"><?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']==smarty_modifier_enum("RfqPaymentTypes::CREDIT_AND_DEBIT_CARD")) {
echo $_smarty_tpl->__("h_rfq.remittance_advice");
} else {
echo $_smarty_tpl->__("h_rfq.lpo");
}?>:&nbsp;</td>
                                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                            <a href="<?php echo htmlspecialchars(fn_url("h_rfq_attachments.download_file&id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&filename=".((string)$_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename'])."&pre_path=".((string)$_smarty_tpl->tpl_vars['quotation']->value['payment_details']['pre_path'])), ENT_QUOTES, 'UTF-8');?>
" target="_blank"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename'], ENT_QUOTES, 'UTF-8');?>
</a>
                                        </td>
                                    </tr>
                                    <?php }?>
                                <?php }?>

                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.delivey_period");?>
:&nbsp;</td>
                                    <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['delivery_period']) {?>
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['delivery_period'], ENT_QUOTES, 'UTF-8');?>
 Days
                                    <?php } else { ?>
                                        --
                                    <?php }?>
                                    </td>
                                </tr>

                               
                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("subtotal");?>
:&nbsp;</td>
                                    <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['subtotal_with_shipping']) {?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['subtotal_with_shipping']), 0);?>

                                    <?php } else { ?>
                                        --
                                    <?php }?>
                                    </td>
                                </tr>

                                 <?php if ($_smarty_tpl->tpl_vars['quotation']->value['total_tax']) {?>
                                    <tr class="ty-orders-summary__row">
                                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.vat");?>
:&nbsp;</td>
                                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['total_tax']), 0);?>

                                        </td>
                                    </tr>
                                <?php }?>

                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("total");?>
:&nbsp;</td>
                                    <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['total']) {?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['total']), 0);?>

                                    <?php } else { ?>
                                        --
                                    <?php }?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

                <div class="ty-orders-detail__products orders-product">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/group.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['group']), 0);?>

                </div>
            </div>

            <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/discussion.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('object_id'=>$_smarty_tpl->tpl_vars['discussion']->value['object_id'],'object_type'=>@constant('DISCUSSION_OBJECT_TYPE_RFQ'),'title'=>$_smarty_tpl->__("discussion_title_order"),'quotation'=>$_smarty_tpl->tpl_vars['quotation']->value,'discussion'=>$_smarty_tpl->tpl_vars['discussion']->value,'wrap'=>true), 0);?>

                
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/tabsbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('top_order_actions'=>Smarty::$_smarty_vars['capture']['quotations_actions'],'content'=>Smarty::$_smarty_vars['capture']['tabsbox'],'active_tab'=>$_REQUEST['selected_section']), 0);?>

    <?php }?>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/h_rfq/views/h_rfq/details.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/h_rfq/views/h_rfq/details.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->__("h_rfq.quotation_detail_header",array('[id]'=>$_smarty_tpl->tpl_vars['rfq_id']->value));?>
</bdi>
    <em class="ty-date">(<?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
)</em>
    <em class="ty-status"> &nbsp;(<?php echo $_smarty_tpl->__("h_rfq.valid_till");?>
: <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['valid_till'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
)</em>
    <em class="ty-status"><?php echo $_smarty_tpl->__("status");?>
: <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['quotation']->value['status'],'display'=>"view",'name'=>"update_order[status]"), 0);?>
</em>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<style>
    .ty-orders-detail .ec_top_bar .ec_right_div a > span:first-child{
        display:block;
    }
</style>
<div class="ty-orders-detail">
    <?php if ($_smarty_tpl->tpl_vars['quotation']->value) {?>
        <?php $_smarty_tpl->_capture_stack[0][] = array("quotations_actions", null, null); ob_start(); ?>
            <div class="ec_top_bar">

                <div class="ec_left_div">
                    <span class="ty-product-filters__title"><?php echo $_smarty_tpl->__("h_rfq.rfq_id");?>
 #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
</span>
                    <span class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
                    
                </div>
                <?php $_smarty_tpl->tpl_vars["print_order"] = new Smarty_variable($_smarty_tpl->__("print_invoice"), null, 0);?>
                <?php $_smarty_tpl->tpl_vars["print_quote"] = new Smarty_variable($_smarty_tpl->__("h_rfq.print_quote"), null, 0);?>
                
                <div class="ec_right_div">
                    <?php if (!@constant('EC_MOBILE_APP')) {?>
                    <?php if (fn_document_to_access($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'],'default')) {?>
                        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_meta'=>"orders-print__pdf ty-btn__text cm-no-ajax cm-new-window",'but_text'=>$_smarty_tpl->tpl_vars['print_quote']->value,'but_href'=>"h_rfq_invoice.print_invoice&rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&format=pdf",'but_icon'=>"ty-icon-doc-text orders-print__icon"), 0);?>

                    <?php }?>
                    
                    <?php if (fn_document_to_access($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'],'invoice')) {?>
                        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_text'=>$_smarty_tpl->tpl_vars['print_order']->value,'but_href'=>"h_rfq_invoice.print_final_invoice&rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&format=pdf",'but_meta'=>" ty-btn__text cm-new-window",'but_icon'=>"ty-icon-print orders-print__icon"), 0);?>

                    <?php }?>
                    <?php }?>
                    <?php if (fn_h_rfq_allow_to_pay($_smarty_tpl->tpl_vars['quotation']->value['status'])) {?>
                        <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']==smarty_modifier_enum("RfqPaymentTypes::CREDIT_AND_DEBIT_CARD")) {?>
                            <?php $_smarty_tpl->_capture_stack[0][] = array("bank_transfer", null, null); ob_start(); ?>
                                <div class="">
                                    <form name="rfq_form" action="<?php echo htmlspecialchars(fn_url(), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
                                        <input type="hidden" name="rfq_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" />
                                        <div><?php echo $_smarty_tpl->tpl_vars['bank_transfer_description']->value;?>
</div>
                                        <div><strong><?php echo $_smarty_tpl->__("h_rfq.note");?>
: </strong><?php echo $_smarty_tpl->__("h_rfq.further_instuction_for_bank_tranfer");?>
</div>
                                        <div class="ty-control-group">
                                            <label for="bank_statements_id" class="ty-control-group__title cm-required">
                                            <?php echo $_smarty_tpl->__("h_rfq.remittance_advice");?>

                                            </label>
                                            <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"bank_statement[]",'multiupload'=>false,'label_id'=>"bank_statements_id"), 0);?>

                                        </div>
                                        <div class="buttons-container ty-right">
                                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit",'but_meta'=>"ty-btn__primary",'but_text'=>$_smarty_tpl->__("h_rfq.upload_remittance_advice"),'but_name'=>"dispatch[h_rfq.upload_bank_statement]",'but_icon'=>''), 0);?>

                                        </div>
                                    </form>
                                </div>
                            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"bank_transfer",'link_text'=>$_smarty_tpl->__("h_rfq.do_bank_transfer"),'text'=>$_smarty_tpl->__("h_rfq.do_bank_transfer"),'content'=>Smarty::$_smarty_vars['capture']['bank_transfer'],'act'=>"general",'link_meta'=>"ty-btn ty-btn__primary ec-imp-white_color cm-dialog-auto-size",'link_icon'=>"icon-plus"), 0);?>

                        
                            
                        <?php } elseif ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']==smarty_modifier_enum("RfqPaymentTypes::COD")) {?>
                            <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
                            <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['curl']->value), null, 0);?>
                            <?php $_smarty_tpl->_capture_stack[0][] = array("cod_popup", null, null); ob_start(); ?>
                                <div class="">
                                    <form name="rfq_form" action="<?php echo htmlspecialchars(fn_url(), ENT_QUOTES, 'UTF-8');?>
" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
                                        <input type="hidden" name="rfq_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" />
                                        <div><strong><?php echo $_smarty_tpl->__("h_rfq.note");?>
: </strong><?php echo $_smarty_tpl->__("h_rfq.provide_the_lpo_for_cod_order");?>
</div>
                                        <div class="ty-control-group">
                                            <label for="cod_lpo_id" class="ty-control-group__title cm-required">
                                            <?php echo $_smarty_tpl->__("h_rfq.upload_lpo");?>

                                            </label>
                                            <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"lpo_documents[]",'multiupload'=>false,'label_id'=>"cod_lpo_id"), 0);?>

                                        </div>
                                        <div class="buttons-container ty-right">
                                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit",'but_meta'=>"ty-btn__primary",'but_text'=>$_smarty_tpl->__("h_rfq.submit_lpo"),'but_name'=>"dispatch[h_rfq.upload_lpo]",'but_icon'=>''), 0);?>

                                        </div>
                                    </form>
                                </div>
                            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                            
                            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"bank_transfer",'link_text'=>$_smarty_tpl->__("h_rfq.submit_lpo"),'text'=>$_smarty_tpl->__("h_rfq.submit_lpo"),'content'=>Smarty::$_smarty_vars['capture']['cod_popup'],'act'=>"general",'link_meta'=>"ty-btn ty-btn__primary ec-imp-white_color cm-dialog-auto-size",'link_icon'=>"icon-plus"), 0);?>

                        <?php }?>
                    <?php }?>
                </div>
            </div>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

        <?php $_smarty_tpl->_capture_stack[0][] = array("tabsbox", null, null); ob_start(); ?>
            <div id="content_general" class="<?php if ($_smarty_tpl->tpl_vars['selected_section']->value&&$_smarty_tpl->tpl_vars['selected_section']->value!="general") {?>hidden<?php }?>" style="background: white; border: 1px solid #c6c6c5; padding: 15px;">

                <div class="orders-customer" style="display:block;">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("h_rfq.customer_information")), 0);?>

                    <div class="ty-profiles-info">
                        <div class="ty-profiles-info__item ty-profiles-info__billing">
                            <h5 class="ty-profiles-info__title"><?php echo $_smarty_tpl->__("h_rfq.buyer_details");?>
</h5>
                            <div class="ty-profiles-info__field">
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['email'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                            </div>
                        </div>

                        <?php $_smarty_tpl->tpl_vars['delivery_location'] = new Smarty_variable(reset($_smarty_tpl->tpl_vars['quotation']->value['locations']), null, 0);?>
                        <div class="ty-profiles-info__item ty-profiles-info__shipping">
                            <h5 class="ty-profiles-info__title"><?php echo $_smarty_tpl->__("h_rfq.delivery_location");?>
</h5>
                            <div class="ty-profiles-info__field">
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['address'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['city'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars(fn_get_state_name($_smarty_tpl->tpl_vars['delivery_location']->value['state'],$_smarty_tpl->tpl_vars['delivery_location']->value['country']), ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars(fn_get_country_name($_smarty_tpl->tpl_vars['delivery_location']->value['country']), ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['zipcode'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['phone'], ENT_QUOTES, 'UTF-8');?>
</bdi>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php $_smarty_tpl->_capture_stack[0][] = array("group", null, null); ob_start(); ?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("h_rfq.products")), 0);?>

                    <table class="ty-orders-detail__table ty-table">
                        <thead>
                            <tr>
                                <th class="ty-orders-detail__table-product"><?php echo $_smarty_tpl->__("h_rfq.product_name");?>
</th>
                                <th class="ty-orders-detail__table-quantity"><?php echo $_smarty_tpl->__("h_rfq.product_qty");?>
</th>
                                <th class="ty-center"><?php echo $_smarty_tpl->__("h_rfq.price");?>
</th>
                                <th class="ty-center"><?php echo $_smarty_tpl->__("h_rfq.total_before_vat");?>
</th>
                                <th class="ty-center"><?php echo $_smarty_tpl->__("h_rfq.vat");?>
%</th>
                                <th class="ty-center"><?php echo $_smarty_tpl->__("h_rfq.vat");?>
</th>
                                <th class="ty-orders-detail__table-subtotal"><?php echo $_smarty_tpl->__("total");?>
</th>
                            </tr>
                        </thead>
                        <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['quotation']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                            <?php if ($_smarty_tpl->tpl_vars['product']->value) {?>
                                <tr class="ty-valign-top">
                                    <td>
                                        <div class="clearfix">

                                            <div class="ty-overflow-hidden ty-orders-detail__table-description-wrapper">
                                                <div class="ty-ml-s ty-orders-detail__table-description">
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?><a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php }?>
                                                        <strong><?php echo $_smarty_tpl->tpl_vars['product']->value['product_name'];?>
</strong>
                                                        <div class="cm-show_hide_text h-rfq_details_description" data-ca-charqty="150" data-ca-ellipse-text=".." data-ca-more-text="<?php echo $_smarty_tpl->__("h_rfq.read_more");?>
" data-ca-less-text="<?php echo $_smarty_tpl->__("h_rfq.read_hide");?>
"><?php echo $_smarty_tpl->__("h_rfq.product_description");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['description'], ENT_QUOTES, 'UTF-8');?>
</div>
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['is_accessible']) {?></a><?php }?>  
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['product_code']) {?>
                                                        <div class="ty-orders-detail__table-code"><?php echo $_smarty_tpl->__("sku");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'UTF-8');?>
</div>
                                                    <?php }?>
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['attachments']) {?>
                                                        <?php $_smarty_tpl->tpl_vars['attachment'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['attachments'], null, 0);?>
                                                        <div class="ty-orders-detail__table-code"><?php echo $_smarty_tpl->__("h_rfq.product_attachments");?>
:&nbsp;<a target="_blank" href="<?php echo htmlspecialchars(fn_url("h_rfq_attachments.download_product?file=".((string)$_smarty_tpl->tpl_vars['attachment']->value['file'])), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attachment']->value['file'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['attachment']->value['name'],50), ENT_QUOTES, 'UTF-8');?>
</a></div>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="ty-center">&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity_unit'], ENT_QUOTES, 'UTF-8');?>
</td>
                                    <td class="ty-center">
                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['non_tax_price']>0) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['non_tax_price']), 0);
} else { ?>--<?php }?>
                                    </td>
                                    <td class="ty-center">
                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['non_tax_price_total']>0) {
echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['non_tax_price_total']), 0);
} else { ?>--<?php }?>
                                    </td>
                                    <td class="ty-center">&nbsp;<?php if ($_smarty_tpl->tpl_vars['product']->value['vat_percentage']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['vat_percentage'], ENT_QUOTES, 'UTF-8');?>
%<?php } else { ?>--<?php }?></td>
                                    <td class="ty-center"><?php if ($_smarty_tpl->tpl_vars['product']->value['total_vat']) {?>&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['total_vat']), 0);
} else { ?>--<?php }?></td>
                                    <td class="ty-right">
                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['tax_price_total']) {?>&nbsp;<?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_price_total']), 0);
} else { ?>--<?php }?>
                                    </td>
                                </tr>
                            <?php }?>
                        <?php } ?>
                    </table>

                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['attachments']) {?>
                    <div class="ty-quotation-attachments clearfix">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("h_rfq.attachments")), 0);?>

                        <div class="h-rfq_download_attachments">
                        <?php  $_smarty_tpl->tpl_vars['attachment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['attachment']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['quotation']->value['attachments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['attachment']->key => $_smarty_tpl->tpl_vars['attachment']->value) {
$_smarty_tpl->tpl_vars['attachment']->_loop = true;
?>
                            <div class="h-rfq_download_attachment">
                                <a href="<?php echo htmlspecialchars(fn_url("h_rfq_attachments.download?file=".((string)$_smarty_tpl->tpl_vars['attachment']->value['file'])), ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['attachment']->value['name'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['attachment']->value['name'],15), ENT_QUOTES, 'UTF-8');?>
</a>
                            </div>
                        <?php } ?>
                        </div>
                    </div>
                    <?php }?>

                    <div class="ty-orders-summary clearfix">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("summary")), 0);?>


                        <div class="ty-orders-summary__right">
                            
                        </div>

                        <div class="ty-orders-summary__wrapper">
                            <table class="ty-orders-summary__table">
                                <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_id']) {?>
                                    <tr class="ty-orders-summary__row">
                                        <td><?php echo $_smarty_tpl->__("payment_method");?>
:</td>
                                        <td style="width: 57%" data-ct-orders-summary="summary-payment">
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['payment'], ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['description']) {?>(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['description'], ENT_QUOTES, 'UTF-8');?>
)<?php }?>
                                        </td>
                                    </tr>
                                <?php }?>

                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.recommended_payment_method");?>
:&nbsp;</td>
                                    <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                        <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']) {?>
                                            <?php echo htmlspecialchars(fn_rfq_payment_types($_smarty_tpl->tpl_vars['quotation']->value['payment_type']), ENT_QUOTES, 'UTF-8');?>

                                        <?php } else { ?>
                                            --
                                        <?php }?>
                                    </td>
                                </tr>

                                <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_details']) {?>
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['payment_reference']) {?>
                                    <tr class="ty-orders-summary__row">
                                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.payment_reference");?>
:&nbsp;</td>
                                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['payment_reference'], ENT_QUOTES, 'UTF-8');?>

                                        </td>
                                    </tr>
                                    <?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename']) {?>
                                    <tr class="ty-orders-summary__row">
                                        <td class="ty-orders-summary__total"><?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']==smarty_modifier_enum("RfqPaymentTypes::CREDIT_AND_DEBIT_CARD")) {
echo $_smarty_tpl->__("h_rfq.remittance_advice");
} else {
echo $_smarty_tpl->__("h_rfq.lpo");
}?>:&nbsp;</td>
                                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                            <a href="<?php echo htmlspecialchars(fn_url("h_rfq_attachments.download_file&id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&filename=".((string)$_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename'])."&pre_path=".((string)$_smarty_tpl->tpl_vars['quotation']->value['payment_details']['pre_path'])), ENT_QUOTES, 'UTF-8');?>
" target="_blank"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename'], ENT_QUOTES, 'UTF-8');?>
</a>
                                        </td>
                                    </tr>
                                    <?php }?>
                                <?php }?>

                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.delivey_period");?>
:&nbsp;</td>
                                    <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['delivery_period']) {?>
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['delivery_period'], ENT_QUOTES, 'UTF-8');?>
 Days
                                    <?php } else { ?>
                                        --
                                    <?php }?>
                                    </td>
                                </tr>

                               
                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("subtotal");?>
:&nbsp;</td>
                                    <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['subtotal_with_shipping']) {?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['subtotal_with_shipping']), 0);?>

                                    <?php } else { ?>
                                        --
                                    <?php }?>
                                    </td>
                                </tr>

                                 <?php if ($_smarty_tpl->tpl_vars['quotation']->value['total_tax']) {?>
                                    <tr class="ty-orders-summary__row">
                                        <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("h_rfq.vat");?>
:&nbsp;</td>
                                        <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['total_tax']), 0);?>

                                        </td>
                                    </tr>
                                <?php }?>

                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("total");?>
:&nbsp;</td>
                                    <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['total']) {?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['total']), 0);?>

                                    <?php } else { ?>
                                        --
                                    <?php }?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

                <div class="ty-orders-detail__products orders-product">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/group.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['group']), 0);?>

                </div>
            </div>

            <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/discussion.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('object_id'=>$_smarty_tpl->tpl_vars['discussion']->value['object_id'],'object_type'=>@constant('DISCUSSION_OBJECT_TYPE_RFQ'),'title'=>$_smarty_tpl->__("discussion_title_order"),'quotation'=>$_smarty_tpl->tpl_vars['quotation']->value,'discussion'=>$_smarty_tpl->tpl_vars['discussion']->value,'wrap'=>true), 0);?>

                
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/tabsbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('top_order_actions'=>Smarty::$_smarty_vars['capture']['quotations_actions'],'content'=>Smarty::$_smarty_vars['capture']['tabsbox'],'active_tab'=>$_REQUEST['selected_section']), 0);?>

    <?php }?>
</div><?php }?><?php }} ?>
