<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 22:38:51
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/authorised_carer_tab.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2851648676092e63b91b161-49465286%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a649c7b1dc656227c3158186213beea32cc4d833' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/authorised_carer_tab.tpl',
      1 => 1603100202,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '2851648676092e63b91b161-49465286',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092e63b9285e8_04385195',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092e63b9285e8_04385195')) {function content_6092e63b9285e8_04385195($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_prescription.firstname','d_prescription.lastname','d_prescription.email','d_prescription.phone','d_prescription.firstname','d_prescription.lastname','d_prescription.email','d_prescription.phone'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_authorised_carer_tab_content hidden">
    <div class="d-patient_details_block">
        <div class="ty-control-group">
            <label for="patient_firstname" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("d_prescription.firstname");?>

            </label>
        <input type="text" id="patient_firstname" name="patient_details[firstname]" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="patient_lastname" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("d_prescription.lastname");?>

            </label>
        <input type="text" id="patient_lastname" name="patient_details[lastname]" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="patient_email" class="ty-control-group__title cm-email">
                <?php echo $_smarty_tpl->__("d_prescription.email");?>

            </label>
        <input type="text" id="patient_email" name="patient_details[email]" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="patient_phone" class="ty-control-group__title cm-mask-phone-label">
                <?php echo $_smarty_tpl->__("d_prescription.phone");?>
 (+971-5*-***-****)
            </label>
        <input type="text" id="patient_phone" name="patient_details[phone]" class="ty-input-text cm-mask-phone" value=""/>
        </div>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/views/ec_checkout/components/authorised_carer_tab.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/views/ec_checkout/components/authorised_carer_tab.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_authorised_carer_tab_content hidden">
    <div class="d-patient_details_block">
        <div class="ty-control-group">
            <label for="patient_firstname" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("d_prescription.firstname");?>

            </label>
        <input type="text" id="patient_firstname" name="patient_details[firstname]" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="patient_lastname" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("d_prescription.lastname");?>

            </label>
        <input type="text" id="patient_lastname" name="patient_details[lastname]" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="patient_email" class="ty-control-group__title cm-email">
                <?php echo $_smarty_tpl->__("d_prescription.email");?>

            </label>
        <input type="text" id="patient_email" name="patient_details[email]" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="patient_phone" class="ty-control-group__title cm-mask-phone-label">
                <?php echo $_smarty_tpl->__("d_prescription.phone");?>
 (+971-5*-***-****)
            </label>
        <input type="text" id="patient_phone" name="patient_details[phone]" class="ty-input-text cm-mask-phone" value=""/>
        </div>
    </div>
</div><?php }?><?php }} ?>
