<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 09:43:40
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/steps/step_three.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8504690026093820c30cc49-96039621%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6b369dd63e68fdd74cf2271aff66904630875dd' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/steps/step_three.tpl',
      1 => 1607418808,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '8504690026093820c30cc49-96039621',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'addons' => 0,
    'payment' => 0,
    'count' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6093820c38dac9_60028732',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6093820c38dac9_60028732')) {function content_6093820c38dac9_60028732($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_billing_check_desc','item_in_cart','ec_billing_check_desc','item_in_cart'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_step_container" id="step_three">
<?php echo smarty_function_script(array('src'=>"js/tygh/checkout/lite_checkout.js"),$_smarty_tpl);?>
 
    <?php if (@constant('EC_MOBILE_APP')) {?>
    <style>
        .ec_step_container#step_three .ec_billing_check_div > span{
            font-size: 12px;
        }
        .ec_step_container#step_three .ec_checkout_bottom .ec_terms label{
            font-size: 12px;
        }
        .ec_step_container#step_three .ec_checkout_bottom .litecheckout__group.litecheckout__newsletters .ty-newsletters__item label{
            font-size: 12px;
        }
        .ec_step_container#step_three .ec_checkout_bottom .ec_terms a{
            font-size: 12px;
        }
    </style>
    <?php }?>
    <div class="ec_top_checkout_div">
        <div class="span12">
            <form  name="form_upload_prescription" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
                <div class="ec_shipping_methods">
                    <div class="litecheckout__container">
                        <div class="litecheckout__group litecheckout__step" id="litecheckout_step_shipping">
                            <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/shipping_rates.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('no_form'=>true), 0);?>

                        <!--litecheckout_step_shipping--></div>
                    </div>
                </div>
                <div class="ec_payment_methods">
                    <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/steps/payment.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('show_title'=>false), 0);?>

                </div>
                
                <div class="ec_billing_addr">
                    <div class="ec_billing_check_div">
                        <input type="checkbox" class="ec_billing_check checkbox" name="billing_address[ec_billing_same]" value="Y"> 
                        <span><?php echo $_smarty_tpl->__("ec_billing_check_desc");?>
</span>
                    </div>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/billing_address.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
                    
                </div>
                <div id="place_order_data" class="hidden">
                </div>

                <div class="ec_checkout_bottom">
                    <div class="ec_terms">
                        <?php echo $_smarty_tpl->getSubTemplate ("blocks/lite_checkout/terms_and_conditions.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['addons']->value['newsletters']['status']=='A') {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/newsletters/blocks/lite_checkout/newsletters.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                    <?php }?>

                    <div class="ec_place_order_div">
                    <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/final_section.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('is_payment_step'=>true,'suffix'=>$_smarty_tpl->tpl_vars['payment']->value['payment_id'],'but_meta'=>'cm-checkout-place-order'), 0);?>

                    </div>
                </div>
            </form>
        </div>
        <div class="span4">
            <div class="ec_cart_total">
                <div class="ec_cart_header" id="checkout_header">
                    <div><?php if ($_SESSION['cart']['amount']<9) {?> <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable($_SESSION['cart']['amount'], null, 0);?> <?php } else { ?> <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable('9+', null, 0);?> <?php }?>
                        <?php echo $_smarty_tpl->__("item_in_cart",array("[count]"=>$_smarty_tpl->tpl_vars['count']->value));?>

                    <!--checkout_header--></div>    
                </div>
                <?php echo $_smarty_tpl->getSubTemplate ("blocks/checkout/summary.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        </div>
    </div>
    <?php echo '<script'; ?>
>
        (function(_, $) {
            $.ceEvent('on', 'ce.commoninit', function(context){
                if($(".ec_billing_check").prop("checked")){
                    $(".ec_billing_check").parent().next('.ec_checkout_billing_address').show();
                    $(".ec_billing_check").parent().next('.ec_checkout_billing_address').find('.ty-control-group__title').each(function(){
                        $(this).addClass('cm-required');
                    });
                } else {
                    $(".ec_billing_check").parent().next('.ec_checkout_billing_address').hide();
                    $(".ec_billing_check").parent().next('.ec_checkout_billing_address').find('.ty-control-group__title').each(function(){
                        $(this).removeClass('cm-required');
                    });
                }
                $(".ec_billing_check").change(function() {
                    if(this.checked) {
                        $(this).parent().next('.ec_checkout_billing_address').show();
                        $(this).parent().next('.ec_checkout_billing_address').find('.ty-control-group__title').each(function(){
                            $(this).addClass('cm-required');
                        });
                    } else {
                        $(this).parent().next('.ec_checkout_billing_address').hide();
                        $(this).parent().next('.ec_checkout_billing_address').find('.ty-control-group__title').each(function(){
                            $(this).removeClass('cm-required');
                        });                      
                    }
                });
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
</form>
<!--step_three--></div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/views/ec_checkout/components/steps/step_three.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/views/ec_checkout/components/steps/step_three.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_step_container" id="step_three">
<?php echo smarty_function_script(array('src'=>"js/tygh/checkout/lite_checkout.js"),$_smarty_tpl);?>
 
    <?php if (@constant('EC_MOBILE_APP')) {?>
    <style>
        .ec_step_container#step_three .ec_billing_check_div > span{
            font-size: 12px;
        }
        .ec_step_container#step_three .ec_checkout_bottom .ec_terms label{
            font-size: 12px;
        }
        .ec_step_container#step_three .ec_checkout_bottom .litecheckout__group.litecheckout__newsletters .ty-newsletters__item label{
            font-size: 12px;
        }
        .ec_step_container#step_three .ec_checkout_bottom .ec_terms a{
            font-size: 12px;
        }
    </style>
    <?php }?>
    <div class="ec_top_checkout_div">
        <div class="span12">
            <form  name="form_upload_prescription" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
                <div class="ec_shipping_methods">
                    <div class="litecheckout__container">
                        <div class="litecheckout__group litecheckout__step" id="litecheckout_step_shipping">
                            <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/shipping_rates.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('no_form'=>true), 0);?>

                        <!--litecheckout_step_shipping--></div>
                    </div>
                </div>
                <div class="ec_payment_methods">
                    <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/steps/payment.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('show_title'=>false), 0);?>

                </div>
                
                <div class="ec_billing_addr">
                    <div class="ec_billing_check_div">
                        <input type="checkbox" class="ec_billing_check checkbox" name="billing_address[ec_billing_same]" value="Y"> 
                        <span><?php echo $_smarty_tpl->__("ec_billing_check_desc");?>
</span>
                    </div>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/billing_address.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
                    
                </div>
                <div id="place_order_data" class="hidden">
                </div>

                <div class="ec_checkout_bottom">
                    <div class="ec_terms">
                        <?php echo $_smarty_tpl->getSubTemplate ("blocks/lite_checkout/terms_and_conditions.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['addons']->value['newsletters']['status']=='A') {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/newsletters/blocks/lite_checkout/newsletters.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                    <?php }?>

                    <div class="ec_place_order_div">
                    <?php echo $_smarty_tpl->getSubTemplate ("views/checkout/components/final_section.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('is_payment_step'=>true,'suffix'=>$_smarty_tpl->tpl_vars['payment']->value['payment_id'],'but_meta'=>'cm-checkout-place-order'), 0);?>

                    </div>
                </div>
            </form>
        </div>
        <div class="span4">
            <div class="ec_cart_total">
                <div class="ec_cart_header" id="checkout_header">
                    <div><?php if ($_SESSION['cart']['amount']<9) {?> <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable($_SESSION['cart']['amount'], null, 0);?> <?php } else { ?> <?php $_smarty_tpl->tpl_vars['count'] = new Smarty_variable('9+', null, 0);?> <?php }?>
                        <?php echo $_smarty_tpl->__("item_in_cart",array("[count]"=>$_smarty_tpl->tpl_vars['count']->value));?>

                    <!--checkout_header--></div>    
                </div>
                <?php echo $_smarty_tpl->getSubTemplate ("blocks/checkout/summary.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        </div>
    </div>
    <?php echo '<script'; ?>
>
        (function(_, $) {
            $.ceEvent('on', 'ce.commoninit', function(context){
                if($(".ec_billing_check").prop("checked")){
                    $(".ec_billing_check").parent().next('.ec_checkout_billing_address').show();
                    $(".ec_billing_check").parent().next('.ec_checkout_billing_address').find('.ty-control-group__title').each(function(){
                        $(this).addClass('cm-required');
                    });
                } else {
                    $(".ec_billing_check").parent().next('.ec_checkout_billing_address').hide();
                    $(".ec_billing_check").parent().next('.ec_checkout_billing_address').find('.ty-control-group__title').each(function(){
                        $(this).removeClass('cm-required');
                    });
                }
                $(".ec_billing_check").change(function() {
                    if(this.checked) {
                        $(this).parent().next('.ec_checkout_billing_address').show();
                        $(this).parent().next('.ec_checkout_billing_address').find('.ty-control-group__title').each(function(){
                            $(this).addClass('cm-required');
                        });
                    } else {
                        $(this).parent().next('.ec_checkout_billing_address').hide();
                        $(this).parent().next('.ec_checkout_billing_address').find('.ty-control-group__title').each(function(){
                            $(this).removeClass('cm-required');
                        });                      
                    }
                });
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
</form>
<!--step_three--></div>
<?php }?><?php }} ?>
