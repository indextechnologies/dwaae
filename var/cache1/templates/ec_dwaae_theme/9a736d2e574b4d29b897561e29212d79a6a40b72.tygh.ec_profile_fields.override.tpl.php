<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 00:19:19
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_vendor_reg/hooks/profiles/ec_profile_fields.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16461893626092fdc7094055-75097394%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9a736d2e574b4d29b897561e29212d79a6a40b72' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_vendor_reg/hooks/profiles/ec_profile_fields.override.tpl',
      1 => 1609221365,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '16461893626092fdc7094055-75097394',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'settings' => 0,
    'field' => 0,
    'pref_field_name' => 0,
    'data_name' => 0,
    'data_id' => 0,
    'var_name' => 0,
    'for_label' => 0,
    'section' => 0,
    'file_object_id' => 0,
    'files_name' => 0,
    'file_details' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092fdc70e08b4_41468689',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092fdc70e08b4_41468689')) {function content_6092fdc70e08b4_41468689($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_enum')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.enum.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (($_smarty_tpl->tpl_vars['runtime']->value['mode']=="add"&&$_smarty_tpl->tpl_vars['settings']->value['General']['quick_registration']=="Y")||$_smarty_tpl->tpl_vars['runtime']->value['mode']=='apply_for_vendor'||($_smarty_tpl->tpl_vars['runtime']->value['mode']=='update'&&$_smarty_tpl->tpl_vars['runtime']->value['controller']=='profiles')) {?>


<?php if (($_smarty_tpl->tpl_vars['field']->value['registration_show']=='Y'&&$_smarty_tpl->tpl_vars['field']->value['field_type']==@constant('H_FILE_UPLOAD'))||($_smarty_tpl->tpl_vars['runtime']->value['mode']=='update'&&$_smarty_tpl->tpl_vars['runtime']->value['controller']=='profiles')) {?>
    <?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']=='H') {?>
        <div class="ty-subheader ty-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['class'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['description'], ENT_QUOTES, 'UTF-8');?>
</div>
    <?php } else { ?>
        <div class="ty-control-group ty-profile-field__item ty-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['class'], ENT_QUOTES, 'UTF-8');?>
 ec_vendor_file_div">
            <?php if (($_smarty_tpl->tpl_vars['pref_field_name']->value!=$_smarty_tpl->tpl_vars['field']->value['description']||$_smarty_tpl->tpl_vars['field']->value['required']=="Y")&&$_smarty_tpl->tpl_vars['field']->value['field_type']!=smarty_modifier_enum("ProfileFieldTypes::VENDOR_TERMS")) {?>
                <?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']==@constant('H_FILE_UPLOAD')) {?>
                    <?php $_smarty_tpl->tpl_vars['var_name'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['data_name']->value)."[".((string)$_smarty_tpl->tpl_vars['data_id']->value)."]", null, 0);?>
                    <?php ob_start();
echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['var_name']->value), ENT_QUOTES, 'UTF-8');
$_tmp1=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['for_label'] = new Smarty_variable("local_profile_".$_tmp1, null, 0);?>

                    <label
                        for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['for_label']->value, ENT_QUOTES, 'UTF-8');?>
"
                        class="ty-control-group__title cm-profile-field <?php if ($_smarty_tpl->tpl_vars['field']->value['autocomplete_type']=="phone-full"||$_smarty_tpl->tpl_vars['field']->value['field_type']==smarty_modifier_enum("ProfileFieldTypes::PHONE")) {?>cm-mask-phone-label<?php }?> <?php if ($_smarty_tpl->tpl_vars['field']->value['required']=="Y") {?>cm-required<?php }
if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="Z") {?> cm-zipcode<?php }
if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="E") {?> cm-email<?php }?> <?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="Z") {
if ($_smarty_tpl->tpl_vars['section']->value=="S") {?>cm-location-shipping<?php } else { ?>cm-location-billing<?php }
}?>"
                    ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['description'], ENT_QUOTES, 'UTF-8');?>
</label>
                <?php }?>
                
            <?php }?>
           
            <?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']==@constant('H_FILE_UPLOAD')) {?>
                <?php $_smarty_tpl->tpl_vars['file_details'] = new Smarty_variable(fn_h_get_profile_file($_smarty_tpl->tpl_vars['file_object_id']->value,$_smarty_tpl->tpl_vars['files_name']->value,$_smarty_tpl->tpl_vars['field']->value['field_name']), null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['file_details']->value['file']) {?>
                <span><a target="_blank" href="<?php echo htmlspecialchars(fn_url_for_app("h_vendor_reg.download&filename=".((string)$_smarty_tpl->tpl_vars['file_details']->value['file'])."&object_id=".((string)$_smarty_tpl->tpl_vars['file_details']->value['object_id'])."&object_type=".((string)$_smarty_tpl->tpl_vars['file_details']->value['object_type'])."&field_name=".((string)$_smarty_tpl->tpl_vars['file_details']->value['field_name'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file_details']->value['file'], ENT_QUOTES, 'UTF-8');?>
</a></span>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('prefix'=>"profile_",'var_name'=>((string)$_smarty_tpl->tpl_vars['data_name']->value)."[".((string)$_smarty_tpl->tpl_vars['data_id']->value)."]",'upload_file_text'=>$_smarty_tpl->tpl_vars['field']->value['description']), 0);?>


            <?php }?>

            <?php $_smarty_tpl->tpl_vars["pref_field_name"] = new Smarty_variable($_smarty_tpl->tpl_vars['field']->value['description'], null, 0);?>
        </div>
    <?php }?>
<?php } else { ?>
    <div style="display:none;">&nbsp;</div>
<?php }?>

<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/h_vendor_reg/hooks/profiles/ec_profile_fields.override.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/h_vendor_reg/hooks/profiles/ec_profile_fields.override.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (($_smarty_tpl->tpl_vars['runtime']->value['mode']=="add"&&$_smarty_tpl->tpl_vars['settings']->value['General']['quick_registration']=="Y")||$_smarty_tpl->tpl_vars['runtime']->value['mode']=='apply_for_vendor'||($_smarty_tpl->tpl_vars['runtime']->value['mode']=='update'&&$_smarty_tpl->tpl_vars['runtime']->value['controller']=='profiles')) {?>


<?php if (($_smarty_tpl->tpl_vars['field']->value['registration_show']=='Y'&&$_smarty_tpl->tpl_vars['field']->value['field_type']==@constant('H_FILE_UPLOAD'))||($_smarty_tpl->tpl_vars['runtime']->value['mode']=='update'&&$_smarty_tpl->tpl_vars['runtime']->value['controller']=='profiles')) {?>
    <?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']=='H') {?>
        <div class="ty-subheader ty-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['class'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['description'], ENT_QUOTES, 'UTF-8');?>
</div>
    <?php } else { ?>
        <div class="ty-control-group ty-profile-field__item ty-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['class'], ENT_QUOTES, 'UTF-8');?>
 ec_vendor_file_div">
            <?php if (($_smarty_tpl->tpl_vars['pref_field_name']->value!=$_smarty_tpl->tpl_vars['field']->value['description']||$_smarty_tpl->tpl_vars['field']->value['required']=="Y")&&$_smarty_tpl->tpl_vars['field']->value['field_type']!=smarty_modifier_enum("ProfileFieldTypes::VENDOR_TERMS")) {?>
                <?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']==@constant('H_FILE_UPLOAD')) {?>
                    <?php $_smarty_tpl->tpl_vars['var_name'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['data_name']->value)."[".((string)$_smarty_tpl->tpl_vars['data_id']->value)."]", null, 0);?>
                    <?php ob_start();
echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['var_name']->value), ENT_QUOTES, 'UTF-8');
$_tmp2=ob_get_clean();?><?php $_smarty_tpl->tpl_vars['for_label'] = new Smarty_variable("local_profile_".$_tmp2, null, 0);?>

                    <label
                        for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['for_label']->value, ENT_QUOTES, 'UTF-8');?>
"
                        class="ty-control-group__title cm-profile-field <?php if ($_smarty_tpl->tpl_vars['field']->value['autocomplete_type']=="phone-full"||$_smarty_tpl->tpl_vars['field']->value['field_type']==smarty_modifier_enum("ProfileFieldTypes::PHONE")) {?>cm-mask-phone-label<?php }?> <?php if ($_smarty_tpl->tpl_vars['field']->value['required']=="Y") {?>cm-required<?php }
if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="Z") {?> cm-zipcode<?php }
if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="E") {?> cm-email<?php }?> <?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="Z") {
if ($_smarty_tpl->tpl_vars['section']->value=="S") {?>cm-location-shipping<?php } else { ?>cm-location-billing<?php }
}?>"
                    ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['description'], ENT_QUOTES, 'UTF-8');?>
</label>
                <?php }?>
                
            <?php }?>
           
            <?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']==@constant('H_FILE_UPLOAD')) {?>
                <?php $_smarty_tpl->tpl_vars['file_details'] = new Smarty_variable(fn_h_get_profile_file($_smarty_tpl->tpl_vars['file_object_id']->value,$_smarty_tpl->tpl_vars['files_name']->value,$_smarty_tpl->tpl_vars['field']->value['field_name']), null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['file_details']->value['file']) {?>
                <span><a target="_blank" href="<?php echo htmlspecialchars(fn_url_for_app("h_vendor_reg.download&filename=".((string)$_smarty_tpl->tpl_vars['file_details']->value['file'])."&object_id=".((string)$_smarty_tpl->tpl_vars['file_details']->value['object_id'])."&object_type=".((string)$_smarty_tpl->tpl_vars['file_details']->value['object_type'])."&field_name=".((string)$_smarty_tpl->tpl_vars['file_details']->value['field_name'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file_details']->value['file'], ENT_QUOTES, 'UTF-8');?>
</a></span>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('prefix'=>"profile_",'var_name'=>((string)$_smarty_tpl->tpl_vars['data_name']->value)."[".((string)$_smarty_tpl->tpl_vars['data_id']->value)."]",'upload_file_text'=>$_smarty_tpl->tpl_vars['field']->value['description']), 0);?>


            <?php }?>

            <?php $_smarty_tpl->tpl_vars["pref_field_name"] = new Smarty_variable($_smarty_tpl->tpl_vars['field']->value['description'], null, 0);?>
        </div>
    <?php }?>
<?php } else { ?>
    <div style="display:none;">&nbsp;</div>
<?php }?>

<?php }
}?><?php }} ?>
