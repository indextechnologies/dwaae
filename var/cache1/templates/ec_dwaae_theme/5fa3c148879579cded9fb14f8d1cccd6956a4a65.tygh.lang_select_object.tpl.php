<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:01
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/common/lang_select_object.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5143644146092b8cd685023-13135998%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5fa3c148879579cded9fb14f8d1cccd6956a4a65' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/common/lang_select_object.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '5143644146092b8cd685023-13135998',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'text' => 0,
    'style' => 0,
    'selected_id' => 0,
    'suffix' => 0,
    'display_icons' => 0,
    'items' => 0,
    'link_tpl' => 0,
    'id' => 0,
    'item' => 0,
    'key_name' => 0,
    'addons' => 0,
    'block' => 0,
    'config' => 0,
    'location' => 0,
    'var_name' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8cd6d3529_33061618',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8cd6d3529_33061618')) {function content_6092b8cd6d3529_33061618($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('select_descr_lang','language','location','vendor_locations.selected_city','vendor_locations.customer_geolocation','vendor_locations.not_your_city','vendor_locations.search_city','ec_choose_location','select_descr_lang','language','location','vendor_locations.selected_city','vendor_locations.customer_geolocation','vendor_locations.not_your_city','vendor_locations.search_city','ec_choose_location'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars["language_text"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['text']->value)===null||$tmp==='' ? $_smarty_tpl->__("select_descr_lang") : $tmp), null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['style']->value=="graphic") {?>
    <?php if ($_smarty_tpl->tpl_vars['text']->value) {?><div class="ty-select-block__txt hidden-phone hidden-tablet"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text']->value, ENT_QUOTES, 'UTF-8');?>
:</div><?php }?>
    
    <a class="ty-select-block__a cm-combination" id="sw_select_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_id']->value, ENT_QUOTES, 'UTF-8');?>
_wrap_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
">
        <?php if ($_smarty_tpl->tpl_vars['display_icons']->value==true) {?>
            <i class="ty-select-block__a-flag ty-flag ty-flag-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['items']->value[$_smarty_tpl->tpl_vars['selected_id']->value]['country_code'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
" ></i>
        <?php }?>
        
        <i class="ty-select-block__arrow ty-icon-down-micro"></i>
    </a>
    <?php $_smarty_tpl->tpl_vars['key_name'] = new Smarty_variable('name', null, 0);?>
    <div id="select_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_id']->value, ENT_QUOTES, 'UTF-8');?>
_wrap_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-select-block cm-popup-box hidden ec_select_lang">
        <div class="ec_lang_header"><?php echo $_smarty_tpl->__("language");?>
</div>
        <ul class="cm-select-list ty-select-block__list ty-flags">
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                <li class="ty-select-block__list-item">
                    <a rel="nofollow" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['link_tpl']->value).((string)$_smarty_tpl->tpl_vars['id']->value)), ENT_QUOTES, 'UTF-8');?>
" class="ty-select-block__list-a <?php if ($_smarty_tpl->tpl_vars['selected_id']->value==$_smarty_tpl->tpl_vars['id']->value) {?>is-active<?php }?> <?php if ($_smarty_tpl->tpl_vars['suffix']->value=="live_editor_box") {?>cm-lang-link<?php }?>" <?php if ($_smarty_tpl->tpl_vars['display_icons']->value==true) {?>data-ca-country-code="<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['item']->value['country_code'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
"<?php }?> data-ca-name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <span class="ec_selector_icon"></span>
                    <?php if ($_smarty_tpl->tpl_vars['display_icons']->value==true) {?>
                        <i class="ty-flag ty-flag-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['item']->value['country_code'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
"></i>
                    <?php }?>
                    <span><?php echo $_smarty_tpl->tpl_vars['item']->value[$_smarty_tpl->tpl_vars['key_name']->value];
if ($_smarty_tpl->tpl_vars['item']->value['symbol']) {?> (<?php echo $_smarty_tpl->tpl_vars['item']->value['symbol'];?>
)<?php }?></span>
                    </a>
                </li>
            <?php } ?>
        </ul>
        <?php if ($_smarty_tpl->tpl_vars['addons']->value['vendor_locations']['status']=='A') {?>
        <div class="ec_location_div">
            <div class="ec_header"><?php echo $_smarty_tpl->__("location");?>
</div>
            <?php $_smarty_tpl->tpl_vars['location'] = new Smarty_variable($_SESSION['settings']['customer_geolocation']['value'], null, 0);?>
            <div class="cm-reload-on-geolocation-change" id="location_selector_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
">
                <div id="customer_geolocation_dialog">
                    <form name="geolocation_form" id="form_geolocation" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="cm-geolocation-form cm-ajax-full-render">
                        <input name="return_url" type="hidden" value="<?php echo htmlspecialchars((($tmp = @$_REQUEST['return_url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
">
                        <input name="location" id="elm_geolocation_data" type="hidden" data-ca-field-id="geolocation_data" value="">

                        <div class="ty-filter-products-by-geolocation-popup__container">
                            <p class="ty-filter-products-by-geolocation-popup__selected-city"><?php echo $_smarty_tpl->__("vendor_locations.selected_city");?>
:</p>
                            <h3 class="ty-filter-products-by-geolocation-popup__title cm-filter-products-by-geolocation__location">
                                <i class="ty-icon-location-arrow"></i><bdi><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['location']->value['locality_text'])===null||$tmp==='' ? $_smarty_tpl->__("vendor_locations.customer_geolocation") : $tmp), ENT_QUOTES, 'UTF-8');?>
</bdi>
                            </h3>
                            <p class="ty-filter-products-by-geolocation-popup__not-your-city"><?php echo $_smarty_tpl->__("vendor_locations.not_your_city");?>
</p>
                        </div>
                        <div class="ty-control-group">
                            <label class="ty-control-group__title" for="customer_geolocation"><?php echo $_smarty_tpl->__("vendor_locations.search_city");?>
</label>
                            <input id="customer_current_geolocation"
                                size="50"
                                class="cm-geocomplete ty-input-text-full cm-geolocation-search-current-location"
                                type="text"
                                name="customer_geolocation"
                                value=""
                                data-ca-field-id="customer_geolocation"
                                data-ca-geocomplete-type="address"
                                data-ca-geocomplete-value-elem-id="elm_geolocation_data"
                                data-ca-geocomplete-place-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value['place_id'], ENT_QUOTES, 'UTF-8');?>
"
                            >
                        </div>

                        <div class="buttons-container">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("ec_choose_location"),'but_role'=>"text",'but_meta'=>"ty-btn__primary ty-btn__big cm-form-dialog-closer ty-btn cm-geolocation-select-current-location"), 0);?>

                        </div>
                    </form>
                <!--customer_geolocation_dialog--></div>
            <!--location_selector_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
--></div>
        </div>
        <?php }?>
    </div>
<?php } else { ?>
    <?php if ($_smarty_tpl->tpl_vars['text']->value) {?><label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-select-block__txt hidden-phone hidden-tablet"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text']->value, ENT_QUOTES, 'UTF-8');?>
:</label><?php }?>
    <select id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
" onchange="Tygh.$.redirect(this.value);" class="ty-valign">
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <option value="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['link_tpl']->value).((string)$_smarty_tpl->tpl_vars['id']->value)), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['id']->value==$_smarty_tpl->tpl_vars['selected_id']->value) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value[$_smarty_tpl->tpl_vars['key_name']->value];?>
</option>
        <?php } ?>
    </select>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="common/lang_select_object.tpl" id="<?php echo smarty_function_set_id(array('name'=>"common/lang_select_object.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars["language_text"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['text']->value)===null||$tmp==='' ? $_smarty_tpl->__("select_descr_lang") : $tmp), null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['style']->value=="graphic") {?>
    <?php if ($_smarty_tpl->tpl_vars['text']->value) {?><div class="ty-select-block__txt hidden-phone hidden-tablet"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text']->value, ENT_QUOTES, 'UTF-8');?>
:</div><?php }?>
    
    <a class="ty-select-block__a cm-combination" id="sw_select_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_id']->value, ENT_QUOTES, 'UTF-8');?>
_wrap_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
">
        <?php if ($_smarty_tpl->tpl_vars['display_icons']->value==true) {?>
            <i class="ty-select-block__a-flag ty-flag ty-flag-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['items']->value[$_smarty_tpl->tpl_vars['selected_id']->value]['country_code'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
" ></i>
        <?php }?>
        
        <i class="ty-select-block__arrow ty-icon-down-micro"></i>
    </a>
    <?php $_smarty_tpl->tpl_vars['key_name'] = new Smarty_variable('name', null, 0);?>
    <div id="select_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_id']->value, ENT_QUOTES, 'UTF-8');?>
_wrap_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['suffix']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-select-block cm-popup-box hidden ec_select_lang">
        <div class="ec_lang_header"><?php echo $_smarty_tpl->__("language");?>
</div>
        <ul class="cm-select-list ty-select-block__list ty-flags">
            <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                <li class="ty-select-block__list-item">
                    <a rel="nofollow" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['link_tpl']->value).((string)$_smarty_tpl->tpl_vars['id']->value)), ENT_QUOTES, 'UTF-8');?>
" class="ty-select-block__list-a <?php if ($_smarty_tpl->tpl_vars['selected_id']->value==$_smarty_tpl->tpl_vars['id']->value) {?>is-active<?php }?> <?php if ($_smarty_tpl->tpl_vars['suffix']->value=="live_editor_box") {?>cm-lang-link<?php }?>" <?php if ($_smarty_tpl->tpl_vars['display_icons']->value==true) {?>data-ca-country-code="<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['item']->value['country_code'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
"<?php }?> data-ca-name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <span class="ec_selector_icon"></span>
                    <?php if ($_smarty_tpl->tpl_vars['display_icons']->value==true) {?>
                        <i class="ty-flag ty-flag-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['item']->value['country_code'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
"></i>
                    <?php }?>
                    <span><?php echo $_smarty_tpl->tpl_vars['item']->value[$_smarty_tpl->tpl_vars['key_name']->value];
if ($_smarty_tpl->tpl_vars['item']->value['symbol']) {?> (<?php echo $_smarty_tpl->tpl_vars['item']->value['symbol'];?>
)<?php }?></span>
                    </a>
                </li>
            <?php } ?>
        </ul>
        <?php if ($_smarty_tpl->tpl_vars['addons']->value['vendor_locations']['status']=='A') {?>
        <div class="ec_location_div">
            <div class="ec_header"><?php echo $_smarty_tpl->__("location");?>
</div>
            <?php $_smarty_tpl->tpl_vars['location'] = new Smarty_variable($_SESSION['settings']['customer_geolocation']['value'], null, 0);?>
            <div class="cm-reload-on-geolocation-change" id="location_selector_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
">
                <div id="customer_geolocation_dialog">
                    <form name="geolocation_form" id="form_geolocation" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="cm-geolocation-form cm-ajax-full-render">
                        <input name="return_url" type="hidden" value="<?php echo htmlspecialchars((($tmp = @$_REQUEST['return_url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
">
                        <input name="location" id="elm_geolocation_data" type="hidden" data-ca-field-id="geolocation_data" value="">

                        <div class="ty-filter-products-by-geolocation-popup__container">
                            <p class="ty-filter-products-by-geolocation-popup__selected-city"><?php echo $_smarty_tpl->__("vendor_locations.selected_city");?>
:</p>
                            <h3 class="ty-filter-products-by-geolocation-popup__title cm-filter-products-by-geolocation__location">
                                <i class="ty-icon-location-arrow"></i><bdi><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['location']->value['locality_text'])===null||$tmp==='' ? $_smarty_tpl->__("vendor_locations.customer_geolocation") : $tmp), ENT_QUOTES, 'UTF-8');?>
</bdi>
                            </h3>
                            <p class="ty-filter-products-by-geolocation-popup__not-your-city"><?php echo $_smarty_tpl->__("vendor_locations.not_your_city");?>
</p>
                        </div>
                        <div class="ty-control-group">
                            <label class="ty-control-group__title" for="customer_geolocation"><?php echo $_smarty_tpl->__("vendor_locations.search_city");?>
</label>
                            <input id="customer_current_geolocation"
                                size="50"
                                class="cm-geocomplete ty-input-text-full cm-geolocation-search-current-location"
                                type="text"
                                name="customer_geolocation"
                                value=""
                                data-ca-field-id="customer_geolocation"
                                data-ca-geocomplete-type="address"
                                data-ca-geocomplete-value-elem-id="elm_geolocation_data"
                                data-ca-geocomplete-place-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value['place_id'], ENT_QUOTES, 'UTF-8');?>
"
                            >
                        </div>

                        <div class="buttons-container">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("ec_choose_location"),'but_role'=>"text",'but_meta'=>"ty-btn__primary ty-btn__big cm-form-dialog-closer ty-btn cm-geolocation-select-current-location"), 0);?>

                        </div>
                    </form>
                <!--customer_geolocation_dialog--></div>
            <!--location_selector_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
--></div>
        </div>
        <?php }?>
    </div>
<?php } else { ?>
    <?php if ($_smarty_tpl->tpl_vars['text']->value) {?><label for="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-select-block__txt hidden-phone hidden-tablet"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['text']->value, ENT_QUOTES, 'UTF-8');?>
:</label><?php }?>
    <select id="id_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var_name']->value, ENT_QUOTES, 'UTF-8');?>
" onchange="Tygh.$.redirect(this.value);" class="ty-valign">
        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
            <option value="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['link_tpl']->value).((string)$_smarty_tpl->tpl_vars['id']->value)), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['id']->value==$_smarty_tpl->tpl_vars['selected_id']->value) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value[$_smarty_tpl->tpl_vars['key_name']->value];?>
</option>
        <?php } ?>
    </select>
<?php }?>
<?php }?><?php }} ?>
