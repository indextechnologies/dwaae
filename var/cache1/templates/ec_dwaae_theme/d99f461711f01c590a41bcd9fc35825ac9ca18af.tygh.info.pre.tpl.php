<?php /* Smarty version Smarty-3.1.21, created on 2021-05-24 20:53:34
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_custom/hooks/orders/info.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:41790970860abda0e8a9022-68483352%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd99f461711f01c590a41bcd9fc35825ac9ca18af' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_custom/hooks/orders/info.pre.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '41790970860abda0e8a9022-68483352',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'order_info' => 0,
    'addons' => 0,
    'config' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60abda0e8c01d8_16722942',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60abda0e8c01d8_16722942')) {function content_60abda0e8c01d8_16722942($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_custom.upload_prescription','d_custom.upload_new_prescription_from_here','d_custom.prescriptions','d_custom.upload_prescription','d_custom.upload_new_prescription_from_here','d_custom.prescriptions'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['order_info']->value['status']==$_smarty_tpl->tpl_vars['addons']->value['d_custom']['prescription_reject_order_status']) {?>
<div class="d-upload-new-prescription-box">
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_custom.upload_prescription")), 0);?>

    <h4><?php echo $_smarty_tpl->__("d_custom.upload_new_prescription_from_here");?>
</h4>
    <form name="upload_prescription" method="post" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data">
        <input type="hidden" name="order_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
        <div class="ty-control-group">
            <label for="id_prescribed_product_attachment" class="cm-required"><?php echo $_smarty_tpl->__("d_custom.prescriptions");?>
</label>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"prescriptions[]",'multiupload'=>true,'label_id'=>"id_prescribed_product_attachment"), 0);?>

        </div>
        <div class="d-order-buttons-container">
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/save.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[d_prescriptions.upload_prescription]",'but_meta'=>"ty-btn__primary ty-right"), 0);?>

        </div>
    </form>
</div>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_custom/hooks/orders/info.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_custom/hooks/orders/info.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['order_info']->value['status']==$_smarty_tpl->tpl_vars['addons']->value['d_custom']['prescription_reject_order_status']) {?>
<div class="d-upload-new-prescription-box">
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_custom.upload_prescription")), 0);?>

    <h4><?php echo $_smarty_tpl->__("d_custom.upload_new_prescription_from_here");?>
</h4>
    <form name="upload_prescription" method="post" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data">
        <input type="hidden" name="order_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
        <div class="ty-control-group">
            <label for="id_prescribed_product_attachment" class="cm-required"><?php echo $_smarty_tpl->__("d_custom.prescriptions");?>
</label>
            <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"prescriptions[]",'multiupload'=>true,'label_id'=>"id_prescribed_product_attachment"), 0);?>

        </div>
        <div class="d-order-buttons-container">
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/save.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[d_prescriptions.upload_prescription]",'but_meta'=>"ty-btn__primary ty-right"), 0);?>

        </div>
    </form>
</div>
<?php }
}?><?php }} ?>
