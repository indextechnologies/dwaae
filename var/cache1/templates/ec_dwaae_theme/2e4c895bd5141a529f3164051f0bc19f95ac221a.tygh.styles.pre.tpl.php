<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:00
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/hooks/index/styles.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15844537476092b8cc580824-78853659%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e4c895bd5141a529f3164051f0bc19f95ac221a' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/hooks/index/styles.pre.tpl',
      1 => 1614683876,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '15844537476092b8cc580824-78853659',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'statuses' => 0,
    'status' => 0,
    'status_data' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8cc5d6b14_48390054',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8cc5d6b14_48390054')) {function content_6092b8cc5d6b14_48390054($_smarty_tpl) {?><?php if (!is_callable('smarty_function_style')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.style.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">

<?php $_smarty_tpl->tpl_vars['statuses'] = new Smarty_variable(fn_get_statuses(@constant('STATUSES_ORDER')), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['statuses']->value) {?>
<?php $_smarty_tpl->_capture_stack[0][] = array("styles", null, null); ob_start(); ?>
    <?php  $_smarty_tpl->tpl_vars["status_data"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["status_data"]->_loop = false;
 $_smarty_tpl->tpl_vars["status"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["status_data"]->key => $_smarty_tpl->tpl_vars["status_data"]->value) {
$_smarty_tpl->tpl_vars["status_data"]->_loop = true;
 $_smarty_tpl->tpl_vars["status"]->value = $_smarty_tpl->tpl_vars["status_data"]->key;
?>
        .ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['status']->value, 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
 {
            color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['status_data']->value['params']['color'], ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php } ?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo smarty_function_style(array('content'=>Smarty::$_smarty_vars['capture']['styles'],'type'=>"less"),$_smarty_tpl);?>

<?php }?>

<?php if (!(($_smarty_tpl->tpl_vars['runtime']->value['controller']=='products'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='checkout'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='cart')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='profiles')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='auth')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='checkout'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='complete')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='product_features'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='compare')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='companies'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='apply_for_vendor')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='companies'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='sitemap'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='pages'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='checkout'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='checkout')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='d_prescription')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='h_rfq')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='clinic_reg')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='ec_pages')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='pages')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='delivery_address')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='otp_verify_register'))) {?>
    <style>
    #tygh_main_container{
        background: #f0f0f0 repeat scroll;
    }
    .tygh-content > div {
        background-color: #f0f0f0;
    }
    </style>
<?php } else { ?>
    <style>
        #tygh_main_container{
            background: #ffffff repeat scroll;
        }
        .tygh-content > div {
            background-color: #ffffff;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='index'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='index')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='categories'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')) {?>
    <style>
        .ec_footer_bottom_share {
            background: #f0f0f0 !important;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='orders'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='search')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='product_features'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='compare')) {?>
    <style>
        .main-content-grid{
            padding: 0 !important;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='pages'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')) {?>
    <style>
        .main-content-grid{
            padding: 10px 20px !important;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='index'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='index')) {?>
    <style>
        .ec_footer_bottom_share {
            border-top: 0px !important;
        }
    </style>
<?php } else { ?>
    <style>
        .ec_footer_bottom_share {
            border-top: 2px solid #c4c4c4 !important;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='profiles'&&($_smarty_tpl->tpl_vars['runtime']->value['mode']=='update'||$_smarty_tpl->tpl_vars['runtime']->value['mode']=='add'))||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='auth'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='login_form')) {?>
    <style>
        @media(max-width: 767px){
            .breadcrumbs-grid {
                display: none !important;
            }
            .ec_top_form_logo img{
                max-width: 105px !important;
            }
            .ty-mainbox-container{
                position: relative !important;
            }
            .ty-account{
                padding-top: 50px !important;
            }
            .ty-login{
                padding-top: 40px !important;
            }
            .ty-login .ec_login_header{
                display: none;
            }
            .ty-login .ty-login__filed-label{
                font-weight: 600;
            }
            .ty-mainbox-title{
                position: absolute;
                top: 95px;
                left: 10px;
            }
            .ty-login{
                max-width: 100% !important;
            }
        }
    </style>
<?php }?>


    <style>
        @media(max-width: 979px){
            .breadcrumbs-grid {
                display: none !important;
            }
        }
    </style>


<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/hooks/index/styles.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/hooks/index/styles.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">

<?php $_smarty_tpl->tpl_vars['statuses'] = new Smarty_variable(fn_get_statuses(@constant('STATUSES_ORDER')), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['statuses']->value) {?>
<?php $_smarty_tpl->_capture_stack[0][] = array("styles", null, null); ob_start(); ?>
    <?php  $_smarty_tpl->tpl_vars["status_data"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["status_data"]->_loop = false;
 $_smarty_tpl->tpl_vars["status"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["status_data"]->key => $_smarty_tpl->tpl_vars["status_data"]->value) {
$_smarty_tpl->tpl_vars["status_data"]->_loop = true;
 $_smarty_tpl->tpl_vars["status"]->value = $_smarty_tpl->tpl_vars["status_data"]->key;
?>
        .ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['status']->value, 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
 {
            color: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['status_data']->value['params']['color'], ENT_QUOTES, 'UTF-8');?>
 !important;
        }
    <?php } ?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo smarty_function_style(array('content'=>Smarty::$_smarty_vars['capture']['styles'],'type'=>"less"),$_smarty_tpl);?>

<?php }?>

<?php if (!(($_smarty_tpl->tpl_vars['runtime']->value['controller']=='products'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='checkout'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='cart')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='profiles')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='auth')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='checkout'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='complete')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='product_features'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='compare')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='companies'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='apply_for_vendor')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='companies'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='sitemap'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='pages'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='checkout'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='checkout')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='d_prescription')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='h_rfq')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='clinic_reg')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='ec_pages')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='pages')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='delivery_address')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='otp_verify_register'))) {?>
    <style>
    #tygh_main_container{
        background: #f0f0f0 repeat scroll;
    }
    .tygh-content > div {
        background-color: #f0f0f0;
    }
    </style>
<?php } else { ?>
    <style>
        #tygh_main_container{
            background: #ffffff repeat scroll;
        }
        .tygh-content > div {
            background-color: #ffffff;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='index'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='index')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='categories'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')) {?>
    <style>
        .ec_footer_bottom_share {
            background: #f0f0f0 !important;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='orders'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='search')||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='product_features'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='compare')) {?>
    <style>
        .main-content-grid{
            padding: 0 !important;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='pages'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view')) {?>
    <style>
        .main-content-grid{
            padding: 10px 20px !important;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='index'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='index')) {?>
    <style>
        .ec_footer_bottom_share {
            border-top: 0px !important;
        }
    </style>
<?php } else { ?>
    <style>
        .ec_footer_bottom_share {
            border-top: 2px solid #c4c4c4 !important;
        }
    </style>
<?php }?>

<?php if (($_smarty_tpl->tpl_vars['runtime']->value['controller']=='profiles'&&($_smarty_tpl->tpl_vars['runtime']->value['mode']=='update'||$_smarty_tpl->tpl_vars['runtime']->value['mode']=='add'))||($_smarty_tpl->tpl_vars['runtime']->value['controller']=='auth'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='login_form')) {?>
    <style>
        @media(max-width: 767px){
            .breadcrumbs-grid {
                display: none !important;
            }
            .ec_top_form_logo img{
                max-width: 105px !important;
            }
            .ty-mainbox-container{
                position: relative !important;
            }
            .ty-account{
                padding-top: 50px !important;
            }
            .ty-login{
                padding-top: 40px !important;
            }
            .ty-login .ec_login_header{
                display: none;
            }
            .ty-login .ty-login__filed-label{
                font-weight: 600;
            }
            .ty-mainbox-title{
                position: absolute;
                top: 95px;
                left: 10px;
            }
            .ty-login{
                max-width: 100% !important;
            }
        }
    </style>
<?php }?>


    <style>
        @media(max-width: 979px){
            .breadcrumbs-grid {
                display: none !important;
            }
        }
    </style>


<?php }?><?php }} ?>
