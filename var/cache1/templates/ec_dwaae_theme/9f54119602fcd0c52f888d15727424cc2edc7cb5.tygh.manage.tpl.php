<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 22:21:26
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/my_account/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10387707636092e226dda408-09935819%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9f54119602fcd0c52f888d15727424cc2edc7cb5' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/my_account/manage.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '10387707636092e226dda408-09935819',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092e226e1fd95_92079474',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092e226e1fd95_92079474')) {function content_6092e226e1fd95_92079474($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('profile_details_login__security','edit_profile_details_name_mobile_number_and_change_password','my_addresses','view_all_saved_shipping_addresses_edit_and_add_new','saved_insurance_cards','view_all_saved_insurance_cards_edit_and_add_new','saved_emirates_id','view_all_saved_emirates_ids_edit_and_add_new','my_orders','view_all_your_orders_print_and_reorder_again','subscriptions','manage_your_newsletter_subscriptions_and_social_networks','my_prescriptions','view_all_uploaded_prescriptions','my_quotations','view_all_your_requested_quotations','profile_details_login__security','edit_profile_details_name_mobile_number_and_change_password','my_addresses','view_all_saved_shipping_addresses_edit_and_add_new','saved_insurance_cards','view_all_saved_insurance_cards_edit_and_add_new','saved_emirates_id','view_all_saved_emirates_ids_edit_and_add_new','my_orders','view_all_your_orders_print_and_reorder_again','subscriptions','manage_your_newsletter_subscriptions_and_social_networks','my_prescriptions','view_all_uploaded_prescriptions','my_quotations','view_all_your_requested_quotations'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_my_account_container">
    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("profiles.update"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-profile_details"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("profile_details_login__security");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("edit_profile_details_name_mobile_number_and_change_password");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("delivery_address.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-my_address"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("my_addresses");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_saved_shipping_addresses_edit_and_add_new");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("insurance_cards.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-saved_insurance_card"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("saved_insurance_cards");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_saved_insurance_cards_edit_and_add_new");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("emirate_cards.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-saved_emirates_id"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("saved_emirates_id");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_saved_emirates_ids_edit_and_add_new");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("orders.search"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-my_orders"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("my_orders");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_your_orders_print_and_reorder_again");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("my_account.newsletter"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-subscriptions"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("subscriptions");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("manage_your_newsletter_subscriptions_and_social_networks");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("d_prescription.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-my_prescriptions"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("my_prescriptions");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_uploaded_prescriptions");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("h_rfq.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-my_quotation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("my_quotations");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_your_requested_quotations");?>

                </div>
            </div>
        </a>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/my_account/manage.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/my_account/manage.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_my_account_container">
    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("profiles.update"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-profile_details"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("profile_details_login__security");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("edit_profile_details_name_mobile_number_and_change_password");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("delivery_address.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-my_address"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("my_addresses");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_saved_shipping_addresses_edit_and_add_new");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("insurance_cards.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-saved_insurance_card"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("saved_insurance_cards");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_saved_insurance_cards_edit_and_add_new");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("emirate_cards.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-saved_emirates_id"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("saved_emirates_id");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_saved_emirates_ids_edit_and_add_new");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("orders.search"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-my_orders"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("my_orders");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_your_orders_print_and_reorder_again");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("my_account.newsletter"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-subscriptions"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("subscriptions");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("manage_your_newsletter_subscriptions_and_social_networks");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("d_prescription.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-my_prescriptions"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("my_prescriptions");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_uploaded_prescriptions");?>

                </div>
            </div>
        </a>
    </div>

    <div class="ec_inner_grid">
        <a href="<?php echo htmlspecialchars(fn_url("h_rfq.manage"), ENT_QUOTES, 'UTF-8');?>
">
            <div class="ec_icon_div">
                <span class="ec-icon-my_quotation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span></span>
            </div>
            <div class="ec_content_div">
                <div class="ec_header">
                    <?php echo $_smarty_tpl->__("my_quotations");?>

                </div>
                <div class="ec_description_div">
                    <?php echo $_smarty_tpl->__("view_all_your_requested_quotations");?>

                </div>
            </div>
        </a>
    </div>
</div><?php }?><?php }} ?>
