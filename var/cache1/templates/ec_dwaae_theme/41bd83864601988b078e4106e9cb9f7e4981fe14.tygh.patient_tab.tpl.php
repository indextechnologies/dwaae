<?php /* Smarty version Smarty-3.1.21, created on 2021-06-15 14:13:52
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/patient_tab.tpl" */ ?>
<?php /*%%SmartyHeaderCode:162759637860c87d609396e0-18272684%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '41bd83864601988b078e4106e9cb9f7e4981fe14' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/patient_tab.tpl',
      1 => 1613480871,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '162759637860c87d609396e0-18272684',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'cart' => 0,
    'prescription_required' => 0,
    'f' => 0,
    'emirate_cards' => 0,
    'emirate_card' => 0,
    'card_select' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60c87d60977e30_51557068',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60c87d60977e30_51557068')) {function content_60c87d60977e30_51557068($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_upload_prescription','d_prescription.e_prescription_no','d_prescription.doctor_emirates','emirates_id','add_new_id','ec_upload_prescription','d_prescription.e_prescription_no','d_prescription.doctor_emirates','emirates_id','add_new_id'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_patient_tab_content" id="ec_patient_tab_content">
    <div class="ec_upload_form">
        <div class="ec_top_div">
            <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
            <div class="ty-control-group ec_upload_rescription_btn">
                <?php $_smarty_tpl->tpl_vars['prescription_required'] = new Smarty_variable(false, null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']&&!$_smarty_tpl->tpl_vars['cart']->value['prescription_details']['files']) {?>
                    <?php $_smarty_tpl->tpl_vars['prescription_required'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                <div id="ec_prescription__required">
                <label for="id_prescribed_attachment" class="ty-control-group__title <?php if ($_smarty_tpl->tpl_vars['prescription_required']->value) {?>cm-required<?php }?>">
                    <?php echo $_smarty_tpl->__("ec_upload_prescription");?>

                </label>
                <!--ec_prescription__required--></div>
                <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['files']) {?>
                <?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart']->value['prescription_details']['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->_loop = true;
?>
                <p class="" style="display: block;">
                    <i id="" title="Remove this item" class="ty-icon-cancel-circle ty-fileuploader__icon ec-remove-uploaded-file"></i>
                    <span class="ty-fileuploader__filename ty-filename-link" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['f']->value, ENT_QUOTES, 'UTF-8');?>
" data-file-name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['f']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['f']->value, ENT_QUOTES, 'UTF-8');?>
</span>
                </p>
                <?php } ?>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"prescriptions[]",'multiupload'=>true,'label_id'=>"id_prescribed_attachment",'upload_file_text'=>$_smarty_tpl->__('ec_upload_prescription'),'upload_another_file_text'=>$_smarty_tpl->__('ec_upload_prescription')), 0);?>

            </div>
            <?php }?>
            <div class="upload_input_div">
                <div class="ty-control-group">
                    <label for="e_prescription_no" class="ty-control-group__title">
                        <?php echo $_smarty_tpl->__("d_prescription.e_prescription_no");?>

                    </label>
                    <input type="text" id="e_prescription_no" name="prescription_details[e_prescription_no]" class="ty-input-text " value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['e_prescription_no'], ENT_QUOTES, 'UTF-8');?>
"/>
                </div>
                <div class="ty-control-group">
                    <label for="doctor_emirates" class="ty-control-group__title">
                        <?php echo $_smarty_tpl->__("d_prescription.doctor_emirates");?>

                    </label>
                    <input type="text" id="doctor_emirates" name="prescription_details[doctor_emirates]" class="ty-input-text " value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['doctor_emirates'], ENT_QUOTES, 'UTF-8');?>
"/>
                </div>
            </div>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/authorised_carer_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
                    

        <div id="ec_emirates_upload_block">
            <div class="ec_header"><?php echo $_smarty_tpl->__("emirates_id");?>
</div>
            <div class="ec_upload_form_blocks">
                <?php  $_smarty_tpl->tpl_vars['emirate_card'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['emirate_card']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['emirate_cards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["emirate_card"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['emirate_card']->key => $_smarty_tpl->tpl_vars['emirate_card']->value) {
$_smarty_tpl->tpl_vars['emirate_card']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["emirate_card"]['iteration']++;
?>
                    <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(false, null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['selected_emirate']==$_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id']||$_smarty_tpl->getVariable('smarty')->value['foreach']['emirate_card']['iteration']==1) {?>
                        <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(true, null, 0);?>
                    <?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('emirate_card_selected'=>$_smarty_tpl->tpl_vars['card_select']->value,'emirate_edit_card'=>"checkout.edit_emirate",'emirate_render_ids'=>"ec_emirates_upload_block,ec_prescription__required",'emirate_redirect_url'=>"checkout.checkout&ec_edit_step=ec_step_two",'enable_card_select'=>true), 0);?>

                <?php } ?>
            </div>
            <a href="<?php echo htmlspecialchars(fn_url("checkout.edit_emirate"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render ec_add_card_btn" data-ca-target-id="ec_emirates_upload_block,ec_prescription__required"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_id");?>
</a>
        <!--ec_emirates_upload_block--></div>
    </div>
<!--ec_patient_tab_content--></div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/views/ec_checkout/components/patient_tab.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/views/ec_checkout/components/patient_tab.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_patient_tab_content" id="ec_patient_tab_content">
    <div class="ec_upload_form">
        <div class="ec_top_div">
            <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
            <div class="ty-control-group ec_upload_rescription_btn">
                <?php $_smarty_tpl->tpl_vars['prescription_required'] = new Smarty_variable(false, null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']&&!$_smarty_tpl->tpl_vars['cart']->value['prescription_details']['files']) {?>
                    <?php $_smarty_tpl->tpl_vars['prescription_required'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                <div id="ec_prescription__required">
                <label for="id_prescribed_attachment" class="ty-control-group__title <?php if ($_smarty_tpl->tpl_vars['prescription_required']->value) {?>cm-required<?php }?>">
                    <?php echo $_smarty_tpl->__("ec_upload_prescription");?>

                </label>
                <!--ec_prescription__required--></div>
                <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['files']) {?>
                <?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['cart']->value['prescription_details']['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value) {
$_smarty_tpl->tpl_vars['f']->_loop = true;
?>
                <p class="" style="display: block;">
                    <i id="" title="Remove this item" class="ty-icon-cancel-circle ty-fileuploader__icon ec-remove-uploaded-file"></i>
                    <span class="ty-fileuploader__filename ty-filename-link" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['f']->value, ENT_QUOTES, 'UTF-8');?>
" data-file-name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['f']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['f']->value, ENT_QUOTES, 'UTF-8');?>
</span>
                </p>
                <?php } ?>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_custom/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"prescriptions[]",'multiupload'=>true,'label_id'=>"id_prescribed_attachment",'upload_file_text'=>$_smarty_tpl->__('ec_upload_prescription'),'upload_another_file_text'=>$_smarty_tpl->__('ec_upload_prescription')), 0);?>

            </div>
            <?php }?>
            <div class="upload_input_div">
                <div class="ty-control-group">
                    <label for="e_prescription_no" class="ty-control-group__title">
                        <?php echo $_smarty_tpl->__("d_prescription.e_prescription_no");?>

                    </label>
                    <input type="text" id="e_prescription_no" name="prescription_details[e_prescription_no]" class="ty-input-text " value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['e_prescription_no'], ENT_QUOTES, 'UTF-8');?>
"/>
                </div>
                <div class="ty-control-group">
                    <label for="doctor_emirates" class="ty-control-group__title">
                        <?php echo $_smarty_tpl->__("d_prescription.doctor_emirates");?>

                    </label>
                    <input type="text" id="doctor_emirates" name="prescription_details[doctor_emirates]" class="ty-input-text " value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['doctor_emirates'], ENT_QUOTES, 'UTF-8');?>
"/>
                </div>
            </div>
        </div>
        <?php echo $_smarty_tpl->getSubTemplate ("addons/ec_dwaae_new/views/ec_checkout/components/authorised_carer_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
                    

        <div id="ec_emirates_upload_block">
            <div class="ec_header"><?php echo $_smarty_tpl->__("emirates_id");?>
</div>
            <div class="ec_upload_form_blocks">
                <?php  $_smarty_tpl->tpl_vars['emirate_card'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['emirate_card']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['emirate_cards']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["emirate_card"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['emirate_card']->key => $_smarty_tpl->tpl_vars['emirate_card']->value) {
$_smarty_tpl->tpl_vars['emirate_card']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["emirate_card"]['iteration']++;
?>
                    <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(false, null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_details']['selected_emirate']==$_smarty_tpl->tpl_vars['emirate_card']->value['emirate_profile_id']||$_smarty_tpl->getVariable('smarty')->value['foreach']['emirate_card']['iteration']==1) {?>
                        <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(true, null, 0);?>
                    <?php }?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('emirate_card_selected'=>$_smarty_tpl->tpl_vars['card_select']->value,'emirate_edit_card'=>"checkout.edit_emirate",'emirate_render_ids'=>"ec_emirates_upload_block,ec_prescription__required",'emirate_redirect_url'=>"checkout.checkout&ec_edit_step=ec_step_two",'enable_card_select'=>true), 0);?>

                <?php } ?>
            </div>
            <a href="<?php echo htmlspecialchars(fn_url("checkout.edit_emirate"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render ec_add_card_btn" data-ca-target-id="ec_emirates_upload_block,ec_prescription__required"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_id");?>
</a>
        <!--ec_emirates_upload_block--></div>
    </div>
<!--ec_patient_tab_content--></div><?php }?><?php }} ?>
