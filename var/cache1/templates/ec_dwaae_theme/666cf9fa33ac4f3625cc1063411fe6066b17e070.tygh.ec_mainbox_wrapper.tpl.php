<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:03
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/wrappers/ec_mainbox_wrapper.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9887354456092b8cfd6e9e2-22685861%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '666cf9fa33ac4f3625cc1063411fe6066b17e070' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/wrappers/ec_mainbox_wrapper.tpl',
      1 => 1606395306,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '9887354456092b8cfd6e9e2-22685861',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'content' => 0,
    'hide_wrapper' => 0,
    'details_page' => 0,
    'block' => 0,
    'content_alignment' => 0,
    'title' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8cfd902a3_35618493',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8cfd902a3_35618493')) {function content_6092b8cfd902a3_35618493($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_see_more','ec_see_more'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (trim($_smarty_tpl->tpl_vars['content']->value)) {?>
    <div class="ec-mainbox-wrapper-container clearfix<?php if (isset($_smarty_tpl->tpl_vars['hide_wrapper']->value)) {?> cm-hidden-wrapper<?php }
if ($_smarty_tpl->tpl_vars['hide_wrapper']->value) {?> hidden<?php }
if ($_smarty_tpl->tpl_vars['details_page']->value) {?> details-page<?php }
if ($_smarty_tpl->tpl_vars['block']->value['user_class']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['user_class'], ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['content_alignment']->value=="RIGHT") {?> ty-float-right<?php } elseif ($_smarty_tpl->tpl_vars['content_alignment']->value=="LEFT") {?> ty-float-left<?php }?>">
        <?php if ($_smarty_tpl->tpl_vars['title']->value||trim(Smarty::$_smarty_vars['capture']['title'])) {?>
            <h1 class="ec-mainbox-wrapper-title">
                <div class="ec_title_header">
                    <?php if (trim(Smarty::$_smarty_vars['capture']['title'])) {?>
                        <?php echo Smarty::$_smarty_vars['capture']['title'];?>

                    <?php } else { ?>
                        <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <?php }?>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['view_more_url']) {?>
                    <div class="view_more_url"><a href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['block']->value['properties']['view_more_url']), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_see_more");?>
</a></div>
                <?php }?>
                <i class="ec_wrapper_icon"></i>
            </h1>
        <?php }?>
        <div class="ec-mainbox-wrapper-body"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</div>
    </div>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/wrappers/ec_mainbox_wrapper.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/wrappers/ec_mainbox_wrapper.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (trim($_smarty_tpl->tpl_vars['content']->value)) {?>
    <div class="ec-mainbox-wrapper-container clearfix<?php if (isset($_smarty_tpl->tpl_vars['hide_wrapper']->value)) {?> cm-hidden-wrapper<?php }
if ($_smarty_tpl->tpl_vars['hide_wrapper']->value) {?> hidden<?php }
if ($_smarty_tpl->tpl_vars['details_page']->value) {?> details-page<?php }
if ($_smarty_tpl->tpl_vars['block']->value['user_class']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['user_class'], ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['content_alignment']->value=="RIGHT") {?> ty-float-right<?php } elseif ($_smarty_tpl->tpl_vars['content_alignment']->value=="LEFT") {?> ty-float-left<?php }?>">
        <?php if ($_smarty_tpl->tpl_vars['title']->value||trim(Smarty::$_smarty_vars['capture']['title'])) {?>
            <h1 class="ec-mainbox-wrapper-title">
                <div class="ec_title_header">
                    <?php if (trim(Smarty::$_smarty_vars['capture']['title'])) {?>
                        <?php echo Smarty::$_smarty_vars['capture']['title'];?>

                    <?php } else { ?>
                        <?php echo $_smarty_tpl->tpl_vars['title']->value;?>

                    <?php }?>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['view_more_url']) {?>
                    <div class="view_more_url"><a href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['block']->value['properties']['view_more_url']), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_see_more");?>
</a></div>
                <?php }?>
                <i class="ec_wrapper_icon"></i>
            </h1>
        <?php }?>
        <div class="ec-mainbox-wrapper-body"><?php echo $_smarty_tpl->tpl_vars['content']->value;?>
</div>
    </div>
<?php }
}?><?php }} ?>
