<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:01
         compiled from "/home/dwaae/public_html/design/backend/templates/components/bottom_panel/icons/bp-close.svg" */ ?>
<?php /*%%SmartyHeaderCode:151553886092b8cd202711-77243432%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '149179cc11f6329ecb96fe193ba97dc175bc8515' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/components/bottom_panel/icons/bp-close.svg',
      1 => 1600492179,
      2 => 'backend',
    ),
  ),
  'nocache_hash' => '151553886092b8cd202711-77243432',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8cd203a76_45883961',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8cd203a76_45883961')) {function content_6092b8cd203a76_45883961($_smarty_tpl) {?><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
    class="bp-close__icon">
    <path
        d="M16.644 8.966l-3.025 3.024 3.025 3.025a1.152 1.152 0 0 1-1.629 1.629l-3.025-3.025-3.024 3.025a1.148 1.148 0 0 1-1.628 0 1.152 1.152 0 0 1 0-1.63l3.024-3.024-3.025-3.024a1.152 1.152 0 0 1 1.629-1.629l3.024 3.025 3.025-3.025a1.152 1.152 0 0 1 1.629 1.629z" />
</svg><?php }} ?>
