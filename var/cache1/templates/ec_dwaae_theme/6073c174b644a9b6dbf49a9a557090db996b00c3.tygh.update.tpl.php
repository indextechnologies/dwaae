<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:36:32
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17754157226092bb80e7f2c1-49678380%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6073c174b644a9b6dbf49a9a557090db996b00c3' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/update.tpl',
      1 => 1614787235,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '17754157226092bb80e7f2c1-49678380',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'product_step_name' => 0,
    'selected_step' => 0,
    'step_sequence' => 0,
    'extra_step_name' => 0,
    'formatCategories' => 0,
    'sequence_no' => 0,
    'delivery_step_name' => 0,
    'buyer_detail_step_name' => 0,
    'product_step' => 0,
    'quotation' => 0,
    'k' => 0,
    'p' => 0,
    'payment_type' => 0,
    'type' => 0,
    'name' => 0,
    'rand' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092bb80f2dd20_13390429',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092bb80f2dd20_13390429')) {function content_6092bb80f2dd20_13390429($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('block_is_not_allowed_to_delete','h_rfq.back','h_rfq.add_product','h_rfq.submitting_application_terms_condition','h_rfq.buyer_aggrement','h_rfq.submit_quote','h_rfq.next','h_rfq.product_details_title','h_rfq.product_details_title_desc','h_rfq.delivery_location_title','h_rfq.delivery_location_title_desc','h_rfq.buyer_details_title','h_rfq.buyer_details_title_desc','h_rfq.add_extra_title','h_rfq.add_exta_data_desc','h_rfq.recommended_payment_method','block_is_not_allowed_to_delete','h_rfq.back','h_rfq.add_product','h_rfq.submitting_application_terms_condition','h_rfq.buyer_aggrement','h_rfq.submit_quote','h_rfq.next','h_rfq.product_details_title','h_rfq.product_details_title_desc','h_rfq.delivery_location_title','h_rfq.delivery_location_title_desc','h_rfq.buyer_details_title','h_rfq.buyer_details_title_desc','h_rfq.add_extra_title','h_rfq.add_exta_data_desc','h_rfq.recommended_payment_method'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div id="h-rfq_update_page">
<?php $_smarty_tpl->tpl_vars['product_step_name'] = new Smarty_variable("product_step", null, 0);?>
<?php $_smarty_tpl->tpl_vars['delivery_step_name'] = new Smarty_variable("delivery_step", null, 0);?>
<?php $_smarty_tpl->tpl_vars['buyer_detail_step_name'] = new Smarty_variable("buyer_details_step", null, 0);?>
<?php $_smarty_tpl->tpl_vars['extra_step_name'] = new Smarty_variable("extra_info_step", null, 0);?>
<?php $_smarty_tpl->tpl_vars['selected_step'] = new Smarty_variable((($tmp = @$_REQUEST['selected_step'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product_step_name']->value : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['sequence_no'] = new Smarty_variable(array_search($_smarty_tpl->tpl_vars['selected_step']->value,$_smarty_tpl->tpl_vars['step_sequence']->value), null, 0);?>
<?php echo smarty_function_script(array('src'=>"js/addons/h_rfq/dropzonejs.js"),$_smarty_tpl);?>


<?php ob_start();
echo $_smarty_tpl->__('h_rfq.create_header');
$_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp1), 0);?>

<form name="rfq_from" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" style="margin-bottom: 50px;">
    <input type="hidden" name="traslation_block_undelete" value="<?php echo $_smarty_tpl->__("block_is_not_allowed_to_delete");?>
" />
    <input type="hidden" name="result_ids" value="h-rfq_update_page" />
    <?php $_smarty_tpl->_capture_stack[0][] = array("footer", null, null); ob_start(); ?>
    <div class="h-rfq_footer">
            <?php if ($_smarty_tpl->tpl_vars['selected_step']->value!=$_smarty_tpl->tpl_vars['product_step_name']->value) {?>
            <span>
                <?php ob_start();
echo $_smarty_tpl->__("h_rfq.back");
$_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[h_rfq.update.back]",'but_text'=>$_tmp2,'but_target_form'=>"rfq_from",'but_meta'=>"ty-btn__ternary h-rfq-footer-button cm-ajax cm-ajax-full-render"), 0);?>

            </span>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['product_step_name']->value) {?>
            <span>
                <a href="javascript:;" class="ty-btn ty-btn__primary " id="h-rfq_add_new_product"><?php echo $_smarty_tpl->__("h_rfq.add_product");?>
</a>
            </span>
            <?php }?>
        <span class="h_rfq-terms_and_condition">
            <?php echo $_smarty_tpl->__("h_rfq.submitting_application_terms_condition");?>
&nbsp;<a target="_blank" href="<?php echo htmlspecialchars(fn_url_for_app("pages.view&page_id=15"), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("h_rfq.buyer_aggrement");?>
</a>
        </span>
        <span>
            <?php if ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['extra_step_name']->value) {?>
                <?php ob_start();
echo $_smarty_tpl->__("h_rfq.submit_quote");
$_tmp3=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[h_rfq.update.submit]",'but_text'=>$_tmp3,'but_target_form'=>"rfq_from",'but_meta'=>"ty-btn__primary h-rfq-footer-button"), 0);?>

            <?php } else { ?>
                <?php ob_start();
echo $_smarty_tpl->__("h_rfq.next");
$_tmp4=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[h_rfq.update.next]",'but_text'=>$_tmp4,'but_target_form'=>"rfq_from",'but_meta'=>"ty-btn__primary h-rfq-footer-button cm-ajax cm-ajax-full-render"), 0);?>

            <?php }?>
        </span>
    </div>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <input type="hidden" class="data-implode-categories" value="<?php echo htmlspecialchars(implode(',',$_smarty_tpl->tpl_vars['formatCategories']->value), ENT_QUOTES, 'UTF-8');?>
" />
    <div id="rfqstepwise" class="h_rfq_stepwise">
        <ul class="h_rfq_steps">
            <li class="h_rfq_step" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_step_name']->value, ENT_QUOTES, 'UTF-8');?>
">
                <span class="h_rfq_step__left <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==0) {?>h_rfq_step__left_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>0) {?>h_rfq_step__left_complete<?php }?>">1</span>
                <span class="h_rfq_step__right <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==0) {?>h_rfq_step__right_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>0) {?>h_rfq_step__left_complete<?php }?>"><span><?php echo $_smarty_tpl->__("h_rfq.product_details_title");?>
<br/><small><?php echo $_smarty_tpl->__("h_rfq.product_details_title_desc");?>
</small></span></span>
            </li>
            <hr class='h_rfq_dashes' />
            <li class="h_rfq_step" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_step_name']->value, ENT_QUOTES, 'UTF-8');?>
">
                <span class="h_rfq_step__left <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==1) {?>h_rfq_step__left_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>1) {?>h_rfq_step__left_complete<?php }?>">2</span>
                <span class="h_rfq_step__right <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==1) {?>h_rfq_step__right_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>1) {?>h_rfq_step__left_complete<?php }?>"><span><?php echo $_smarty_tpl->__("h_rfq.delivery_location_title");?>
<br/><small><?php echo $_smarty_tpl->__("h_rfq.delivery_location_title_desc");?>
</small></span></span>
            </li>
            <hr class='h_rfq_dashes' />
            <li class="h_rfq_step" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['buyer_detail_step_name']->value, ENT_QUOTES, 'UTF-8');?>
">
                <span class="h_rfq_step__left <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==2) {?>h_rfq_step__left_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>2) {?>h_rfq_step__left_complete<?php }?>">3</span>
                <span class="h_rfq_step__right <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==2) {?>h_rfq_step__right_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>2) {?>h_rfq_step__left_complete<?php }?>"><span><?php echo $_smarty_tpl->__("h_rfq.buyer_details_title");?>
<br/><small><?php echo $_smarty_tpl->__("h_rfq.buyer_details_title_desc");?>
</small></span></span>
            </li>
            <hr class='h_rfq_dashes' />
            <li class="h_rfq_step" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['extra_step_name']->value, ENT_QUOTES, 'UTF-8');?>
">
                <span class="h_rfq_step__left <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==3) {?>h_rfq_step__left_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>3) {?>h_rfq_step__left_complete<?php }?>">4</span>
                <span class="h_rfq_step__right <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==3) {?>h_rfq_step__right_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>3) {?>h_rfq_step__left_complete<?php }?>"><span><?php echo $_smarty_tpl->__("h_rfq.add_extra_title");?>
<br/><small><?php echo $_smarty_tpl->__("h_rfq.add_exta_data_desc");?>
</small></span></span>
            </li>
        </ul>

        <span class="h_rfq_caret-top <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==0) {?>ec_one<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value==1) {?>ec_two<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value==2) {?>ec_three<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value==3) {?>ec_four<?php }?>">
            <span class="h_rfq_caret-inner"></span>
        </span>
        <div class="h_rfq_blocks">

        <div id="block_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_step']->value, ENT_QUOTES, 'UTF-8');?>
" class="h_rfq_block">
            <input type="hidden" name="selected_step" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_step']->value, ENT_QUOTES, 'UTF-8');?>
" />
            <?php if ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['product_step_name']->value) {?>
                <div class="h-rfq_products">
                    
                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['product']) {?> 
                        <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['quotation']->value['product']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['p']->key;
 $_smarty_tpl->tpl_vars['p']->iteration++;
?>
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/product_form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('sqid'=>$_smarty_tpl->tpl_vars['k']->value,'name'=>"quotation[product]",'product'=>$_smarty_tpl->tpl_vars['p']->value,'key'=>$_smarty_tpl->tpl_vars['p']->iteration), 0);?>

                        <?php } ?>
                    <?php } else { ?>
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/product_form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('sqid'=>0,'name'=>"quotation[product]"), 0);?>

                    <?php }?>
                    
                    <div class="h-rfq_add_product_before"></div>                    
                </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['delivery_step_name']->value) {?>
                <div class="h-rfq_delivery_location_data">
                    <?php ob_start();
echo $_smarty_tpl->__('h_rfq.create_delivery_location');
$_tmp5=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp5,'class'=>"h-rfq_subheader"), 0);?>

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/delivery_location.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('sqid'=>0,'name'=>"quotation[delivery_location]"), 0);?>

                </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['buyer_detail_step_name']->value) {?>
                <div class="h-rfq_buyer_data">
                    <?php ob_start();
echo $_smarty_tpl->__('h_rfq.buyer_details');
$_tmp6=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp6,'class'=>"h-rfq_subheader"), 0);?>

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/buyer_details.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('name'=>"quotation[buyer_details]"), 0);?>

                </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['extra_step_name']->value) {?>
                <div class="h-rfq_attachments">
                    <div class="ty-control-group">
                        <label for="el_recommended_payment_method" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.recommended_payment_method");?>
</label>
                        <?php $_smarty_tpl->tpl_vars['payment_type'] = new Smarty_variable($_smarty_tpl->tpl_vars['quotation']->value['payment_type'], null, 0);?>
                        <?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable;
 $_from = fn_rfq_payment_types(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['name']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['type']->value = $_smarty_tpl->tpl_vars['name']->key;
 $_smarty_tpl->tpl_vars['name']->index++;
 $_smarty_tpl->tpl_vars['name']->first = $_smarty_tpl->tpl_vars['name']->index === 0;
?>
                            <?php if (!$_smarty_tpl->tpl_vars['payment_type']->value&&$_smarty_tpl->tpl_vars['name']->first) {?>
                                <?php $_smarty_tpl->tpl_vars['payment_type'] = new Smarty_variable($_smarty_tpl->tpl_vars['type']->value, null, 0);?>
                            <?php }?>
                            <input type="radio" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
" name="quotation[payment_type]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['type']->value==$_smarty_tpl->tpl_vars['payment_type']->value) {?>checked="checked"<?php }?>/>
                            <label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
</label><br>
                        <?php } ?>
                    </div>

                    <?php ob_start();
echo $_smarty_tpl->__('h_rfq.attachments');
$_tmp7=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp7,'class'=>"h-rfq_subheader"), 0);?>

                    <input type="hidden" name="quotation[attachments]" value="" />
                    <div id="upload-widget" class="fallback dropzone" >
                        <input type="file" name="file" multiple class="hidden"/>
                    </div>
                </div>
                <?php echo smarty_function_script(array('src'=>"js/addons/h_rfq/dropzone_handle.js?rand=".((string)$_smarty_tpl->tpl_vars['rand']->value)),$_smarty_tpl);?>

            <?php }?>
            <?php echo Smarty::$_smarty_vars['capture']['footer'];?>

        </div>

        

        </div>
    </div>

    

</form>
<!--h-rfq_update_page--></div>

<?php echo smarty_function_script(array('src'=>"js/addons/h_rfq/script.js?rand=".((string)$_smarty_tpl->tpl_vars['rand']->value)),$_smarty_tpl);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/h_rfq/views/h_rfq/update.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/h_rfq/views/h_rfq/update.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div id="h-rfq_update_page">
<?php $_smarty_tpl->tpl_vars['product_step_name'] = new Smarty_variable("product_step", null, 0);?>
<?php $_smarty_tpl->tpl_vars['delivery_step_name'] = new Smarty_variable("delivery_step", null, 0);?>
<?php $_smarty_tpl->tpl_vars['buyer_detail_step_name'] = new Smarty_variable("buyer_details_step", null, 0);?>
<?php $_smarty_tpl->tpl_vars['extra_step_name'] = new Smarty_variable("extra_info_step", null, 0);?>
<?php $_smarty_tpl->tpl_vars['selected_step'] = new Smarty_variable((($tmp = @$_REQUEST['selected_step'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['product_step_name']->value : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['sequence_no'] = new Smarty_variable(array_search($_smarty_tpl->tpl_vars['selected_step']->value,$_smarty_tpl->tpl_vars['step_sequence']->value), null, 0);?>
<?php echo smarty_function_script(array('src'=>"js/addons/h_rfq/dropzonejs.js"),$_smarty_tpl);?>


<?php ob_start();
echo $_smarty_tpl->__('h_rfq.create_header');
$_tmp8=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp8), 0);?>

<form name="rfq_from" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" style="margin-bottom: 50px;">
    <input type="hidden" name="traslation_block_undelete" value="<?php echo $_smarty_tpl->__("block_is_not_allowed_to_delete");?>
" />
    <input type="hidden" name="result_ids" value="h-rfq_update_page" />
    <?php $_smarty_tpl->_capture_stack[0][] = array("footer", null, null); ob_start(); ?>
    <div class="h-rfq_footer">
            <?php if ($_smarty_tpl->tpl_vars['selected_step']->value!=$_smarty_tpl->tpl_vars['product_step_name']->value) {?>
            <span>
                <?php ob_start();
echo $_smarty_tpl->__("h_rfq.back");
$_tmp9=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[h_rfq.update.back]",'but_text'=>$_tmp9,'but_target_form'=>"rfq_from",'but_meta'=>"ty-btn__ternary h-rfq-footer-button cm-ajax cm-ajax-full-render"), 0);?>

            </span>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['product_step_name']->value) {?>
            <span>
                <a href="javascript:;" class="ty-btn ty-btn__primary " id="h-rfq_add_new_product"><?php echo $_smarty_tpl->__("h_rfq.add_product");?>
</a>
            </span>
            <?php }?>
        <span class="h_rfq-terms_and_condition">
            <?php echo $_smarty_tpl->__("h_rfq.submitting_application_terms_condition");?>
&nbsp;<a target="_blank" href="<?php echo htmlspecialchars(fn_url_for_app("pages.view&page_id=15"), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("h_rfq.buyer_aggrement");?>
</a>
        </span>
        <span>
            <?php if ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['extra_step_name']->value) {?>
                <?php ob_start();
echo $_smarty_tpl->__("h_rfq.submit_quote");
$_tmp10=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[h_rfq.update.submit]",'but_text'=>$_tmp10,'but_target_form'=>"rfq_from",'but_meta'=>"ty-btn__primary h-rfq-footer-button"), 0);?>

            <?php } else { ?>
                <?php ob_start();
echo $_smarty_tpl->__("h_rfq.next");
$_tmp11=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[h_rfq.update.next]",'but_text'=>$_tmp11,'but_target_form'=>"rfq_from",'but_meta'=>"ty-btn__primary h-rfq-footer-button cm-ajax cm-ajax-full-render"), 0);?>

            <?php }?>
        </span>
    </div>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <input type="hidden" class="data-implode-categories" value="<?php echo htmlspecialchars(implode(',',$_smarty_tpl->tpl_vars['formatCategories']->value), ENT_QUOTES, 'UTF-8');?>
" />
    <div id="rfqstepwise" class="h_rfq_stepwise">
        <ul class="h_rfq_steps">
            <li class="h_rfq_step" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_step_name']->value, ENT_QUOTES, 'UTF-8');?>
">
                <span class="h_rfq_step__left <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==0) {?>h_rfq_step__left_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>0) {?>h_rfq_step__left_complete<?php }?>">1</span>
                <span class="h_rfq_step__right <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==0) {?>h_rfq_step__right_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>0) {?>h_rfq_step__left_complete<?php }?>"><span><?php echo $_smarty_tpl->__("h_rfq.product_details_title");?>
<br/><small><?php echo $_smarty_tpl->__("h_rfq.product_details_title_desc");?>
</small></span></span>
            </li>
            <hr class='h_rfq_dashes' />
            <li class="h_rfq_step" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_step_name']->value, ENT_QUOTES, 'UTF-8');?>
">
                <span class="h_rfq_step__left <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==1) {?>h_rfq_step__left_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>1) {?>h_rfq_step__left_complete<?php }?>">2</span>
                <span class="h_rfq_step__right <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==1) {?>h_rfq_step__right_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>1) {?>h_rfq_step__left_complete<?php }?>"><span><?php echo $_smarty_tpl->__("h_rfq.delivery_location_title");?>
<br/><small><?php echo $_smarty_tpl->__("h_rfq.delivery_location_title_desc");?>
</small></span></span>
            </li>
            <hr class='h_rfq_dashes' />
            <li class="h_rfq_step" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['buyer_detail_step_name']->value, ENT_QUOTES, 'UTF-8');?>
">
                <span class="h_rfq_step__left <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==2) {?>h_rfq_step__left_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>2) {?>h_rfq_step__left_complete<?php }?>">3</span>
                <span class="h_rfq_step__right <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==2) {?>h_rfq_step__right_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>2) {?>h_rfq_step__left_complete<?php }?>"><span><?php echo $_smarty_tpl->__("h_rfq.buyer_details_title");?>
<br/><small><?php echo $_smarty_tpl->__("h_rfq.buyer_details_title_desc");?>
</small></span></span>
            </li>
            <hr class='h_rfq_dashes' />
            <li class="h_rfq_step" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['extra_step_name']->value, ENT_QUOTES, 'UTF-8');?>
">
                <span class="h_rfq_step__left <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==3) {?>h_rfq_step__left_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>3) {?>h_rfq_step__left_complete<?php }?>">4</span>
                <span class="h_rfq_step__right <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==3) {?>h_rfq_step__right_active<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value>3) {?>h_rfq_step__left_complete<?php }?>"><span><?php echo $_smarty_tpl->__("h_rfq.add_extra_title");?>
<br/><small><?php echo $_smarty_tpl->__("h_rfq.add_exta_data_desc");?>
</small></span></span>
            </li>
        </ul>

        <span class="h_rfq_caret-top <?php if ($_smarty_tpl->tpl_vars['sequence_no']->value==0) {?>ec_one<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value==1) {?>ec_two<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value==2) {?>ec_three<?php } elseif ($_smarty_tpl->tpl_vars['sequence_no']->value==3) {?>ec_four<?php }?>">
            <span class="h_rfq_caret-inner"></span>
        </span>
        <div class="h_rfq_blocks">

        <div id="block_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_step']->value, ENT_QUOTES, 'UTF-8');?>
" class="h_rfq_block">
            <input type="hidden" name="selected_step" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_step']->value, ENT_QUOTES, 'UTF-8');?>
" />
            <?php if ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['product_step_name']->value) {?>
                <div class="h-rfq_products">
                    
                    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['product']) {?> 
                        <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_smarty_tpl->tpl_vars['k'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['quotation']->value['product']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['k']->value = $_smarty_tpl->tpl_vars['p']->key;
 $_smarty_tpl->tpl_vars['p']->iteration++;
?>
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/product_form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('sqid'=>$_smarty_tpl->tpl_vars['k']->value,'name'=>"quotation[product]",'product'=>$_smarty_tpl->tpl_vars['p']->value,'key'=>$_smarty_tpl->tpl_vars['p']->iteration), 0);?>

                        <?php } ?>
                    <?php } else { ?>
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/product_form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('sqid'=>0,'name'=>"quotation[product]"), 0);?>

                    <?php }?>
                    
                    <div class="h-rfq_add_product_before"></div>                    
                </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['delivery_step_name']->value) {?>
                <div class="h-rfq_delivery_location_data">
                    <?php ob_start();
echo $_smarty_tpl->__('h_rfq.create_delivery_location');
$_tmp12=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp12,'class'=>"h-rfq_subheader"), 0);?>

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/delivery_location.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('sqid'=>0,'name'=>"quotation[delivery_location]"), 0);?>

                </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['buyer_detail_step_name']->value) {?>
                <div class="h-rfq_buyer_data">
                    <?php ob_start();
echo $_smarty_tpl->__('h_rfq.buyer_details');
$_tmp13=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp13,'class'=>"h-rfq_subheader"), 0);?>

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/buyer_details.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('name'=>"quotation[buyer_details]"), 0);?>

                </div>
            <?php } elseif ($_smarty_tpl->tpl_vars['selected_step']->value==$_smarty_tpl->tpl_vars['extra_step_name']->value) {?>
                <div class="h-rfq_attachments">
                    <div class="ty-control-group">
                        <label for="el_recommended_payment_method" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.recommended_payment_method");?>
</label>
                        <?php $_smarty_tpl->tpl_vars['payment_type'] = new Smarty_variable($_smarty_tpl->tpl_vars['quotation']->value['payment_type'], null, 0);?>
                        <?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable;
 $_from = fn_rfq_payment_types(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['name']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['type']->value = $_smarty_tpl->tpl_vars['name']->key;
 $_smarty_tpl->tpl_vars['name']->index++;
 $_smarty_tpl->tpl_vars['name']->first = $_smarty_tpl->tpl_vars['name']->index === 0;
?>
                            <?php if (!$_smarty_tpl->tpl_vars['payment_type']->value&&$_smarty_tpl->tpl_vars['name']->first) {?>
                                <?php $_smarty_tpl->tpl_vars['payment_type'] = new Smarty_variable($_smarty_tpl->tpl_vars['type']->value, null, 0);?>
                            <?php }?>
                            <input type="radio" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
" name="quotation[payment_type]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['type']->value==$_smarty_tpl->tpl_vars['payment_type']->value) {?>checked="checked"<?php }?>/>
                            <label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
</label><br>
                        <?php } ?>
                    </div>

                    <?php ob_start();
echo $_smarty_tpl->__('h_rfq.attachments');
$_tmp14=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_tmp14,'class'=>"h-rfq_subheader"), 0);?>

                    <input type="hidden" name="quotation[attachments]" value="" />
                    <div id="upload-widget" class="fallback dropzone" >
                        <input type="file" name="file" multiple class="hidden"/>
                    </div>
                </div>
                <?php echo smarty_function_script(array('src'=>"js/addons/h_rfq/dropzone_handle.js?rand=".((string)$_smarty_tpl->tpl_vars['rand']->value)),$_smarty_tpl);?>

            <?php }?>
            <?php echo Smarty::$_smarty_vars['capture']['footer'];?>

        </div>

        

        </div>
    </div>

    

</form>
<!--h-rfq_update_page--></div>

<?php echo smarty_function_script(array('src'=>"js/addons/h_rfq/script.js?rand=".((string)$_smarty_tpl->tpl_vars['rand']->value)),$_smarty_tpl);?>

<?php }?><?php }} ?>
