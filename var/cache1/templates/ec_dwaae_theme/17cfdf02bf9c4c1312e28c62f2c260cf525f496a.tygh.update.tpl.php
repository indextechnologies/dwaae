<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 06:01:17
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/delivery_address/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:83962509660934ded6667e4-29182652%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '17cfdf02bf9c4c1312e28c62f2c260cf525f496a' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/delivery_address/update.tpl',
      1 => 1619161082,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '83962509660934ded6667e4-29182652',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'address_render_id' => 0,
    'address_additional_render_ids' => 0,
    'make_prescription_required' => 0,
    'address_redirect_url' => 0,
    'form_body' => 0,
    'render_id' => 0,
    'selected_card' => 0,
    'upload_name' => 0,
    'redirect_url' => 0,
    'settings' => 0,
    'profile_id' => 0,
    'auth' => 0,
    'auth_user_info' => 0,
    'full_name' => 0,
    'countries' => 0,
    '_country' => 0,
    'code' => 0,
    'country' => 0,
    'states' => 0,
    '_state' => 0,
    'state' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60934ded74e4c9_79655581',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60934ded74e4c9_79655581')) {function content_60934ded74e4c9_79655581($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('update_address','new_address','profile_name','my_home','my_office','other','full_name','phone_with_no','address_1','address_2','country','state','city','zipcode','address_label','address_residential','address_commercial','is_default','go_back','update_address','new_address','profile_name','my_home','my_office','other','full_name','phone_with_no','address_1','address_2','country','state','city','zipcode','address_label','address_residential','address_commercial','is_default','go_back'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars['render_id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_render_id']->value)===null||$tmp==='' ? "ec_my_account_delivery" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['additional_render_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_additional_render_ids']->value)===null||$tmp==='' ? '' : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['make_prescription_required'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['make_prescription_required']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_redirect_url']->value)===null||$tmp==='' ? "delivery_address.manage" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['form_body'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['form_body']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_id']->value, ENT_QUOTES, 'UTF-8');?>
">
    <h2 class="ec_shipping_addr_header">
        <?php if ($_smarty_tpl->tpl_vars['selected_card']->value) {?>
            <?php echo $_smarty_tpl->__("update_address");?>

        <?php } else { ?>
            <?php echo $_smarty_tpl->__("new_address");?>

        <?php }?>
    </h2>
    <?php $_smarty_tpl->tpl_vars['profile_id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['profile_id'])===null||$tmp==='' ? 0 : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['upload_name'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['upload_name']->value)===null||$tmp==='' ? "delivery_address.update" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=".((string)$_smarty_tpl->tpl_vars['redirect_url']->value), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['_country'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['s_country'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'] : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['_state'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['s_state'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_state'] : $tmp), null, 0);?>
    <div class="ec_shipping_addr_container">
        <?php if ($_smarty_tpl->tpl_vars['form_body']->value) {?>
        <form name="form_deilvery_card" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
        <?php }?>
            <input type="hidden" name="delivery_address[profile_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_id']->value, ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="result_ids" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_id']->value, ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['address_additional_render_ids']->value) {?>,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['address_additional_render_ids']->value, ENT_QUOTES, 'UTF-8');
}?>" />

            <?php if (!@constant('EC_MOBILE_APP')) {?> 
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/map.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php }?>
            <?php if (@constant('EC_MOBILE_APP')&&@constant('EC_APP_VERSION')==2) {?> 
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/map.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php }?>
            <div class="ty-control-group ec_profile_name">
                <label for="profile_name" class="ty-control-group__title cm-required">
                    <?php echo $_smarty_tpl->__("profile_name");?>

                </label>
                
                <select id="profile_name" name="delivery_address[profile_name]" class="ty-profile-field__select-country">
                    <option value="my_home" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['profile_name']=="my_home") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("my_home");?>
</option>
                    <option value="my_office" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['profile_name']=="my_office") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("my_office");?>
</option>
                    <option value="other" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['profile_name']=="other") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("other");?>
</option>
                </select>
            </div>
            <?php $_smarty_tpl->tpl_vars['auth_user_info'] = new Smarty_variable(array(), null, 0);?>
            <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
                <?php $_smarty_tpl->tpl_vars['auth_user_info'] = new Smarty_variable(fn_get_user_info($_smarty_tpl->tpl_vars['auth']->value['user_id']), null, 0);?>
            <?php }?>
            
            <?php $_smarty_tpl->tpl_vars['full_name'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['auth_user_info']->value['firstname'])." ".((string)$_smarty_tpl->tpl_vars['auth_user_info']->value['lastname']), null, 0);?>
            <div class="ty-control-group ec_full_name">
                <label for="s_firstname" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("full_name");?>
:
                </label>
                <input type="text" id="s_firstname"  name="delivery_address[s_firstname]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['s_firstname'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['full_name']->value : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
            </div>
             <div class="ty-control-group ec_phone">
                <label for="s_phone" class="ty-control-group__title cm-required  cm-mask-phone-label ">
                <?php echo $_smarty_tpl->__("phone_with_no");?>
:
                </label>
                <input type="text" id="s_phone"  name="delivery_address[s_phone]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['s_phone'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['auth_user_info']->value['phone'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="cm-mask-phone" />
            </div>


            <div class="ty-control-group ec_address1">
                <label for="s_address" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("address_1");?>
:
                </label>
                <input type="text" id="s_address"  name="delivery_address[s_address]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['s_address'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
            <div class="ty-control-group ec_address2">
                <label for="s_address_2" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("address_2");?>
:
                </label>
                <input type="text" id="s_address_2"  name="delivery_address[s_address_2]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['s_address_2'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>

            <div class="ty-control-group ec_country">
                <label for="s_country" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("country");?>
:
                </label>
                <select id="s_country" class="ty-profile-field__select-country cm-country cm-location-shipping" name="delivery_address[s_country]">
                    <?php  $_smarty_tpl->tpl_vars["country"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["country"]->_loop = false;
 $_smarty_tpl->tpl_vars["code"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["country"]->key => $_smarty_tpl->tpl_vars["country"]->value) {
$_smarty_tpl->tpl_vars["country"]->_loop = true;
 $_smarty_tpl->tpl_vars["code"]->value = $_smarty_tpl->tpl_vars["country"]->key;
?>
                        <option <?php if ($_smarty_tpl->tpl_vars['_country']->value==$_smarty_tpl->tpl_vars['code']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                    <?php } ?>
                </select>
            </div>
            <div class="ty-control-group ec_state">
                <label for="s_state" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("state");?>
:
                </label>
                <select id="s_state" class="ty-profile-field__select-state cm-state cm-location-shipping" name="delivery_address[s_state]">
                    <?php if ($_smarty_tpl->tpl_vars['states']->value&&$_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]) {?>
                        <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                            <option <?php if ($_smarty_tpl->tpl_vars['_state']->value==$_smarty_tpl->tpl_vars['state']->value['code']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                        <?php } ?>
                    <?php }?>
                </select>
                <input  type="text" id="s_state_d" name="delivery_address[s_state]" size="32" maxlength="64" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_state']->value, ENT_QUOTES, 'UTF-8');?>
" disabled="disabled" class="cm-state cm-location-shipping ty-input-text hidden"/>
            </div>
            <div class="ty-control-group ec_city">
                <label for="s_city" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("city");?>
:
                </label>
                <input type="text" id="s_city"  name="delivery_address[s_city]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['s_city'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        
            <div class="ty-control-group ec_zipcode">
                <label for="s_zipcode" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("zipcode");?>
:
                </label>
                <input type="text" id="s_zipcode"  name="delivery_address[s_zipcode]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['s_zipcode'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
           
            <div class="ty-control-group ec_address_label">
                <label for="s_address_type" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("address_label");?>
:
                </label>
                <span>
                    <input type="radio" id="s_address_type_home"  name="delivery_address[s_address_type]" value="residential" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['s_address_type']=="residential"||!$_smarty_tpl->tpl_vars['selected_card']->value['s_address_type']) {?>checked="checked"<?php }?>/><span><?php echo $_smarty_tpl->__("address_residential");?>
</span><br/>
                    <input type="radio" id="s_address_type_office"  name="delivery_address[s_address_type]" value="commercial" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['s_address_type']=="commercial") {?>checked="checked"<?php }?>/><span><?php echo $_smarty_tpl->__("address_commercial");?>
</span>
                </span>
            </div>

            <div class="ty-control-group ec_is_default">
                <label for="is_default" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("is_default");?>

                </label>
                <input type="hidden"  name="delivery_address[is_default]" value="N"/>
                <input type="checkbox" id="is_default" value="Y" name="delivery_address[is_default]" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['is_default']=='Y') {?>checked="checked"<?php }?>/>
            </div>
            <div class="buttons-container">
                <a href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['redirect_url']->value), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__secondary ec_go_back cm-ajax cm-ajax-full-render" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_id']->value, ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['address_additional_render_ids']->value) {?>,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['address_additional_render_ids']->value, ENT_QUOTES, 'UTF-8');
}?>"><?php echo $_smarty_tpl->__("go_back");?>
</a>

                <?php echo $_smarty_tpl->getSubTemplate ("buttons/save.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[".((string)$_smarty_tpl->tpl_vars['upload_name']->value)."]",'but_meta'=>"ty-btn__secondary ec_next_btn cm-ajax cm-ajax-full-render"), 0);?>


            </div>
            <?php if ($_smarty_tpl->tpl_vars['form_body']->value) {?>
        </form>
        <?php }?>
    </div>

    <?php echo '<script'; ?>
 type="text/javascript">
    (function(_, $) {
        $.ceRebuildStates('init', {
            default_country: '<?php echo htmlspecialchars(strtr($_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" )), ENT_QUOTES, 'UTF-8');?>
',
            states: <?php echo json_encode($_smarty_tpl->tpl_vars['states']->value);?>

        });
    }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<!--<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_id']->value, ENT_QUOTES, 'UTF-8');?>
--></div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_customer/views/delivery_address/update.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_customer/views/delivery_address/update.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars['render_id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_render_id']->value)===null||$tmp==='' ? "ec_my_account_delivery" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['additional_render_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_additional_render_ids']->value)===null||$tmp==='' ? '' : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['make_prescription_required'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['make_prescription_required']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_redirect_url']->value)===null||$tmp==='' ? "delivery_address.manage" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['form_body'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['form_body']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
<div id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_id']->value, ENT_QUOTES, 'UTF-8');?>
">
    <h2 class="ec_shipping_addr_header">
        <?php if ($_smarty_tpl->tpl_vars['selected_card']->value) {?>
            <?php echo $_smarty_tpl->__("update_address");?>

        <?php } else { ?>
            <?php echo $_smarty_tpl->__("new_address");?>

        <?php }?>
    </h2>
    <?php $_smarty_tpl->tpl_vars['profile_id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['profile_id'])===null||$tmp==='' ? 0 : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['upload_name'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['upload_name']->value)===null||$tmp==='' ? "delivery_address.update" : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=".((string)$_smarty_tpl->tpl_vars['redirect_url']->value), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['_country'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['s_country'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'] : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['_state'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['s_state'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_state'] : $tmp), null, 0);?>
    <div class="ec_shipping_addr_container">
        <?php if ($_smarty_tpl->tpl_vars['form_body']->value) {?>
        <form name="form_deilvery_card" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
        <?php }?>
            <input type="hidden" name="delivery_address[profile_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_id']->value, ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="result_ids" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_id']->value, ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['address_additional_render_ids']->value) {?>,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['address_additional_render_ids']->value, ENT_QUOTES, 'UTF-8');
}?>" />

            <?php if (!@constant('EC_MOBILE_APP')) {?> 
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/map.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php }?>
            <?php if (@constant('EC_MOBILE_APP')&&@constant('EC_APP_VERSION')==2) {?> 
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/map.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php }?>
            <div class="ty-control-group ec_profile_name">
                <label for="profile_name" class="ty-control-group__title cm-required">
                    <?php echo $_smarty_tpl->__("profile_name");?>

                </label>
                
                <select id="profile_name" name="delivery_address[profile_name]" class="ty-profile-field__select-country">
                    <option value="my_home" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['profile_name']=="my_home") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("my_home");?>
</option>
                    <option value="my_office" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['profile_name']=="my_office") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("my_office");?>
</option>
                    <option value="other" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['profile_name']=="other") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("other");?>
</option>
                </select>
            </div>
            <?php $_smarty_tpl->tpl_vars['auth_user_info'] = new Smarty_variable(array(), null, 0);?>
            <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
                <?php $_smarty_tpl->tpl_vars['auth_user_info'] = new Smarty_variable(fn_get_user_info($_smarty_tpl->tpl_vars['auth']->value['user_id']), null, 0);?>
            <?php }?>
            
            <?php $_smarty_tpl->tpl_vars['full_name'] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['auth_user_info']->value['firstname'])." ".((string)$_smarty_tpl->tpl_vars['auth_user_info']->value['lastname']), null, 0);?>
            <div class="ty-control-group ec_full_name">
                <label for="s_firstname" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("full_name");?>
:
                </label>
                <input type="text" id="s_firstname"  name="delivery_address[s_firstname]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['s_firstname'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['full_name']->value : $tmp), ENT_QUOTES, 'UTF-8');?>
" />
            </div>
             <div class="ty-control-group ec_phone">
                <label for="s_phone" class="ty-control-group__title cm-required  cm-mask-phone-label ">
                <?php echo $_smarty_tpl->__("phone_with_no");?>
:
                </label>
                <input type="text" id="s_phone"  name="delivery_address[s_phone]" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['selected_card']->value['s_phone'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['auth_user_info']->value['phone'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="cm-mask-phone" />
            </div>


            <div class="ty-control-group ec_address1">
                <label for="s_address" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("address_1");?>
:
                </label>
                <input type="text" id="s_address"  name="delivery_address[s_address]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['s_address'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
            <div class="ty-control-group ec_address2">
                <label for="s_address_2" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("address_2");?>
:
                </label>
                <input type="text" id="s_address_2"  name="delivery_address[s_address_2]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['s_address_2'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>

            <div class="ty-control-group ec_country">
                <label for="s_country" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("country");?>
:
                </label>
                <select id="s_country" class="ty-profile-field__select-country cm-country cm-location-shipping" name="delivery_address[s_country]">
                    <?php  $_smarty_tpl->tpl_vars["country"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["country"]->_loop = false;
 $_smarty_tpl->tpl_vars["code"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["country"]->key => $_smarty_tpl->tpl_vars["country"]->value) {
$_smarty_tpl->tpl_vars["country"]->_loop = true;
 $_smarty_tpl->tpl_vars["code"]->value = $_smarty_tpl->tpl_vars["country"]->key;
?>
                        <option <?php if ($_smarty_tpl->tpl_vars['_country']->value==$_smarty_tpl->tpl_vars['code']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                    <?php } ?>
                </select>
            </div>
            <div class="ty-control-group ec_state">
                <label for="s_state" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("state");?>
:
                </label>
                <select id="s_state" class="ty-profile-field__select-state cm-state cm-location-shipping" name="delivery_address[s_state]">
                    <?php if ($_smarty_tpl->tpl_vars['states']->value&&$_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]) {?>
                        <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                            <option <?php if ($_smarty_tpl->tpl_vars['_state']->value==$_smarty_tpl->tpl_vars['state']->value['code']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                        <?php } ?>
                    <?php }?>
                </select>
                <input  type="text" id="s_state_d" name="delivery_address[s_state]" size="32" maxlength="64" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_state']->value, ENT_QUOTES, 'UTF-8');?>
" disabled="disabled" class="cm-state cm-location-shipping ty-input-text hidden"/>
            </div>
            <div class="ty-control-group ec_city">
                <label for="s_city" class="ty-control-group__title cm-required">
                <?php echo $_smarty_tpl->__("city");?>
:
                </label>
                <input type="text" id="s_city"  name="delivery_address[s_city]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['s_city'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
        
            <div class="ty-control-group ec_zipcode">
                <label for="s_zipcode" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("zipcode");?>
:
                </label>
                <input type="text" id="s_zipcode"  name="delivery_address[s_zipcode]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['s_zipcode'], ENT_QUOTES, 'UTF-8');?>
" />
            </div>
           
            <div class="ty-control-group ec_address_label">
                <label for="s_address_type" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("address_label");?>
:
                </label>
                <span>
                    <input type="radio" id="s_address_type_home"  name="delivery_address[s_address_type]" value="residential" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['s_address_type']=="residential"||!$_smarty_tpl->tpl_vars['selected_card']->value['s_address_type']) {?>checked="checked"<?php }?>/><span><?php echo $_smarty_tpl->__("address_residential");?>
</span><br/>
                    <input type="radio" id="s_address_type_office"  name="delivery_address[s_address_type]" value="commercial" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['s_address_type']=="commercial") {?>checked="checked"<?php }?>/><span><?php echo $_smarty_tpl->__("address_commercial");?>
</span>
                </span>
            </div>

            <div class="ty-control-group ec_is_default">
                <label for="is_default" class="ty-control-group__title">
                <?php echo $_smarty_tpl->__("is_default");?>

                </label>
                <input type="hidden"  name="delivery_address[is_default]" value="N"/>
                <input type="checkbox" id="is_default" value="Y" name="delivery_address[is_default]" <?php if ($_smarty_tpl->tpl_vars['selected_card']->value['is_default']=='Y') {?>checked="checked"<?php }?>/>
            </div>
            <div class="buttons-container">
                <a href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['redirect_url']->value), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__secondary ec_go_back cm-ajax cm-ajax-full-render" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_id']->value, ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['address_additional_render_ids']->value) {?>,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['address_additional_render_ids']->value, ENT_QUOTES, 'UTF-8');
}?>"><?php echo $_smarty_tpl->__("go_back");?>
</a>

                <?php echo $_smarty_tpl->getSubTemplate ("buttons/save.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[".((string)$_smarty_tpl->tpl_vars['upload_name']->value)."]",'but_meta'=>"ty-btn__secondary ec_next_btn cm-ajax cm-ajax-full-render"), 0);?>


            </div>
            <?php if ($_smarty_tpl->tpl_vars['form_body']->value) {?>
        </form>
        <?php }?>
    </div>

    <?php echo '<script'; ?>
 type="text/javascript">
    (function(_, $) {
        $.ceRebuildStates('init', {
            default_country: '<?php echo htmlspecialchars(strtr($_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" )), ENT_QUOTES, 'UTF-8');?>
',
            states: <?php echo json_encode($_smarty_tpl->tpl_vars['states']->value);?>

        });
    }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<!--<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['render_id']->value, ENT_QUOTES, 'UTF-8');?>
--></div>

<?php }?><?php }} ?>
