<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 16:19:42
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1059606924609681deb6f263-31905054%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e62b2a46f6fe0394321872aaf129b5e2cb3b70e6' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/manage.tpl',
      1 => 1607415922,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1059606924609681deb6f263-31905054',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'config' => 0,
    'search' => 0,
    'prescriptions' => 0,
    'c_url' => 0,
    'rev' => 0,
    'c_icon' => 0,
    'c_dummy' => 0,
    'p' => 0,
    'settings' => 0,
    'page_title' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_609681debf3fe5_34231570',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_609681debf3fe5_34231570')) {function content_609681debf3fe5_34231570($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_prescription.prescription_list','d_prescription.req_id','d_prescription.create_time','d_prescription.user_id','status','d_prescription.req_id','d_prescription.create_time','d_prescription.user_id','status','tools','view','ec_your_wishlist_empty','d_prescription.prescription_list','d_prescription.req_id','d_prescription.create_time','d_prescription.user_id','status','d_prescription.req_id','d_prescription.create_time','d_prescription.user_id','status','tools','view','ec_your_wishlist_empty'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
<?php if (@constant('EC_MOBILE_APP')) {?>
 <style>
    #tygh_main_container{
        background: #f0f0f0 repeat scroll;
    }
    .tygh-content > div {
        background-color: #f0f0f0;
    }
    </style>
<?php }?>
<div class="ec_order_search_container ec_quotation">
    <div class="span12">
<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" target="_self" name="prescription_form_list" class="">

<?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('save_current_page'=>true,'save_current_url'=>true,'div_id'=>$_REQUEST['content_id']), 0);?>


<?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
<?php $_smarty_tpl->tpl_vars["c_icon"] = new Smarty_variable("<i class=\"icon-".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])."\"></i>", null, 0);?>
<?php $_smarty_tpl->tpl_vars["c_dummy"] = new Smarty_variable("<i class=\"icon-dummy\"></i>", null, 0);?>

<?php $_smarty_tpl->tpl_vars["rev"] = new Smarty_variable((($tmp = @$_REQUEST['content_id'])===null||$tmp==='' ? "pagination_contents" : $tmp), null, 0);?>

<div class="ec_quotation_top hidden">
    <span class="ec_left">
        <span class="ec-icon-quotation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span>
        <span class="header_text"><?php echo $_smarty_tpl->__("d_prescription.prescription_list");?>
</span>
    </span>
</div>
<?php if ($_smarty_tpl->tpl_vars['prescriptions']->value) {?>
<div class="ty-table-responsive-wrapper">
    <table width="100%" class="ty-table ty-table-middle ty-table--relative ty-table-responsive">
    <thead>
    <tr>
        <th width="17%"><a class="cm-ajax" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=req_id&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rev']->value, ENT_QUOTES, 'UTF-8');?>
><?php echo $_smarty_tpl->__("d_prescription.req_id");
if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="req_id") {
echo $_smarty_tpl->tpl_vars['c_icon']->value;
} else {
echo $_smarty_tpl->tpl_vars['c_dummy']->value;
}?></a></th>
        <th width="17%"><a class="cm-ajax" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=create_time&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rev']->value, ENT_QUOTES, 'UTF-8');?>
><?php echo $_smarty_tpl->__("d_prescription.create_time");
if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="create_time") {
echo $_smarty_tpl->tpl_vars['c_icon']->value;
} else {
echo $_smarty_tpl->tpl_vars['c_dummy']->value;
}?></a></th>
        <th width="17%"><a class="cm-ajax" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=user_id&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rev']->value, ENT_QUOTES, 'UTF-8');?>
><?php echo $_smarty_tpl->__("d_prescription.user_id");
if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="create_time") {
echo $_smarty_tpl->tpl_vars['c_icon']->value;
} else {
echo $_smarty_tpl->tpl_vars['c_dummy']->value;
}?></a></th>
        
        <th width="20%"><a class="cm-ajax" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=status&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rev']->value, ENT_QUOTES, 'UTF-8');?>
><?php echo $_smarty_tpl->__("status");
if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="status") {
echo $_smarty_tpl->tpl_vars['c_icon']->value;
}?></a></th>
        <th class="mobile-hide">&nbsp;</th>
    </tr>
    </thead>
    <?php  $_smarty_tpl->tpl_vars["p"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["p"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescriptions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["p"]->key => $_smarty_tpl->tpl_vars["p"]->value) {
$_smarty_tpl->tpl_vars["p"]->_loop = true;
?>
    <tr>
        <td data-th="<?php echo $_smarty_tpl->__("d_prescription.req_id");?>
">
            <a href="<?php echo htmlspecialchars(fn_url("d_prescription.update?req_id=".((string)$_smarty_tpl->tpl_vars['p']->value['req_id'])), ENT_QUOTES, 'UTF-8');?>
" class="underlined"><bdi>#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
</bdi></a>
        </td>
        <td data-th="<?php echo $_smarty_tpl->__("d_prescription.create_time");?>
">
           <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['p']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>

        </td>
         <td class="nowrap" data-th="<?php echo $_smarty_tpl->__("d_prescription.user_id");?>
">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['lastname'], ENT_QUOTES, 'UTF-8');?>

        </td>
        
        <td class="nowrap" data-th="<?php echo $_smarty_tpl->__("status");?>
">
           <?php echo htmlspecialchars(fn_d_prescription_get_statuses($_smarty_tpl->tpl_vars['p']->value['status']), ENT_QUOTES, 'UTF-8');?>

        </td>
        <td width="5%" class="center" data-th="<?php echo $_smarty_tpl->__("tools");?>
">
            <a href="<?php echo htmlspecialchars(fn_url("d_prescription.update?req_id=".((string)$_smarty_tpl->tpl_vars['p']->value['req_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("view");?>
</a>
        </td>
       
    </tr>
    <?php } ?>
    </table>
</div>
<?php } else { ?>
     <div class="ec-container-cart ec_empty_cart ec_empty_wishlist">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/empty_order_list.svg" alt="<?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
" class="ec-empty-cart-image">
    </div>
<?php }?>

<?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('div_id'=>$_REQUEST['content_id']), 0);?>

</form>
</div>
</div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['page_title']->value), 0);?>

<?php echo Smarty::$_smarty_vars['capture']['mainbox'];?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_prescription/views/d_prescription/manage.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_prescription/views/d_prescription/manage.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
<?php if (@constant('EC_MOBILE_APP')) {?>
 <style>
    #tygh_main_container{
        background: #f0f0f0 repeat scroll;
    }
    .tygh-content > div {
        background-color: #f0f0f0;
    }
    </style>
<?php }?>
<div class="ec_order_search_container ec_quotation">
    <div class="span12">
<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" target="_self" name="prescription_form_list" class="">

<?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('save_current_page'=>true,'save_current_url'=>true,'div_id'=>$_REQUEST['content_id']), 0);?>


<?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
<?php $_smarty_tpl->tpl_vars["c_icon"] = new Smarty_variable("<i class=\"icon-".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])."\"></i>", null, 0);?>
<?php $_smarty_tpl->tpl_vars["c_dummy"] = new Smarty_variable("<i class=\"icon-dummy\"></i>", null, 0);?>

<?php $_smarty_tpl->tpl_vars["rev"] = new Smarty_variable((($tmp = @$_REQUEST['content_id'])===null||$tmp==='' ? "pagination_contents" : $tmp), null, 0);?>

<div class="ec_quotation_top hidden">
    <span class="ec_left">
        <span class="ec-icon-quotation"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span>
        <span class="header_text"><?php echo $_smarty_tpl->__("d_prescription.prescription_list");?>
</span>
    </span>
</div>
<?php if ($_smarty_tpl->tpl_vars['prescriptions']->value) {?>
<div class="ty-table-responsive-wrapper">
    <table width="100%" class="ty-table ty-table-middle ty-table--relative ty-table-responsive">
    <thead>
    <tr>
        <th width="17%"><a class="cm-ajax" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=req_id&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rev']->value, ENT_QUOTES, 'UTF-8');?>
><?php echo $_smarty_tpl->__("d_prescription.req_id");
if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="req_id") {
echo $_smarty_tpl->tpl_vars['c_icon']->value;
} else {
echo $_smarty_tpl->tpl_vars['c_dummy']->value;
}?></a></th>
        <th width="17%"><a class="cm-ajax" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=create_time&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rev']->value, ENT_QUOTES, 'UTF-8');?>
><?php echo $_smarty_tpl->__("d_prescription.create_time");
if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="create_time") {
echo $_smarty_tpl->tpl_vars['c_icon']->value;
} else {
echo $_smarty_tpl->tpl_vars['c_dummy']->value;
}?></a></th>
        <th width="17%"><a class="cm-ajax" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=user_id&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rev']->value, ENT_QUOTES, 'UTF-8');?>
><?php echo $_smarty_tpl->__("d_prescription.user_id");
if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="create_time") {
echo $_smarty_tpl->tpl_vars['c_icon']->value;
} else {
echo $_smarty_tpl->tpl_vars['c_dummy']->value;
}?></a></th>
        
        <th width="20%"><a class="cm-ajax" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=status&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rev']->value, ENT_QUOTES, 'UTF-8');?>
><?php echo $_smarty_tpl->__("status");
if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="status") {
echo $_smarty_tpl->tpl_vars['c_icon']->value;
}?></a></th>
        <th class="mobile-hide">&nbsp;</th>
    </tr>
    </thead>
    <?php  $_smarty_tpl->tpl_vars["p"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["p"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescriptions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["p"]->key => $_smarty_tpl->tpl_vars["p"]->value) {
$_smarty_tpl->tpl_vars["p"]->_loop = true;
?>
    <tr>
        <td data-th="<?php echo $_smarty_tpl->__("d_prescription.req_id");?>
">
            <a href="<?php echo htmlspecialchars(fn_url("d_prescription.update?req_id=".((string)$_smarty_tpl->tpl_vars['p']->value['req_id'])), ENT_QUOTES, 'UTF-8');?>
" class="underlined"><bdi>#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['req_id'], ENT_QUOTES, 'UTF-8');?>
</bdi></a>
        </td>
        <td data-th="<?php echo $_smarty_tpl->__("d_prescription.create_time");?>
">
           <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['p']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>

        </td>
         <td class="nowrap" data-th="<?php echo $_smarty_tpl->__("d_prescription.user_id");?>
">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value['lastname'], ENT_QUOTES, 'UTF-8');?>

        </td>
        
        <td class="nowrap" data-th="<?php echo $_smarty_tpl->__("status");?>
">
           <?php echo htmlspecialchars(fn_d_prescription_get_statuses($_smarty_tpl->tpl_vars['p']->value['status']), ENT_QUOTES, 'UTF-8');?>

        </td>
        <td width="5%" class="center" data-th="<?php echo $_smarty_tpl->__("tools");?>
">
            <a href="<?php echo htmlspecialchars(fn_url("d_prescription.update?req_id=".((string)$_smarty_tpl->tpl_vars['p']->value['req_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("view");?>
</a>
        </td>
       
    </tr>
    <?php } ?>
    </table>
</div>
<?php } else { ?>
     <div class="ec-container-cart ec_empty_cart ec_empty_wishlist">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/empty_order_list.svg" alt="<?php echo $_smarty_tpl->__("ec_your_wishlist_empty");?>
" class="ec-empty-cart-image">
    </div>
<?php }?>

<?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('div_id'=>$_REQUEST['content_id']), 0);?>

</form>
</div>
</div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['page_title']->value), 0);?>

<?php echo Smarty::$_smarty_vars['capture']['mainbox'];?>

<?php }?><?php }} ?>
