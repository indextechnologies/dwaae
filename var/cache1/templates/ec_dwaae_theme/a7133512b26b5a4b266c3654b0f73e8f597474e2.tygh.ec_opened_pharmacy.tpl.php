<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:25:03
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_opened_pharmacy.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14087064856092b8cf8f3a99-15631462%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a7133512b26b5a4b266c3654b0f73e8f597474e2' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_opened_pharmacy.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '14087064856092b8cf8f3a99-15631462',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'config' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b8cf9053d6_30477136',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b8cf9053d6_30477136')) {function content_6092b8cf9053d6_30477136($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('pharmacies_in_service','pharmacies_on_map','find_all_the_pharmacies_on_the_map','show_on_map','pharmacies_in_service','pharmacies_on_map','find_all_the_pharmacies_on_the_map','show_on_map'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_opened_pharmacy_div">
    <div class="left_pharmacy_div">
        <div>
            <div class="icon_div"><i class="ec-icon-pharmacy"></i></div>
            <span></span>
            <div class="ec_header"><?php echo $_smarty_tpl->__("pharmacies_in_service");?>
</div>
            <div class="ec_product_count"><?php echo htmlspecialchars(fn_ec_get_opened_pharmacies(), ENT_QUOTES, 'UTF-8');?>
</div>
        </div>
    </div>
    <div class="ec_right_pharmacy_div">
        <div class="ec_icon_div">
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['dir']['root'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/map-dots.png">
        </div>
        <div class="ec_pharmacy_content">
            <div class="ec_header"><?php echo $_smarty_tpl->__("pharmacies_on_map");?>
</div>
            <div class="ec_content_text"><?php echo $_smarty_tpl->__("find_all_the_pharmacies_on_the_map");?>
</div>

            <div class="ec_show_map_links"><a href="<?php echo htmlspecialchars(fn_url("companies.catalog"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__primary"><?php echo $_smarty_tpl->__("show_on_map");?>
</a> <i class="ec-icon-onmap"></i></div>
        </div>
    </div>
    <div class="ec_end_banner_pharmacy">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/opened_pharmacies.jpg">
    </div>
</div>

<div class="ec_end_banner_pharmacy mob">
    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/opened_pharmacies.jpg">
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/static_templates/ec_opened_pharmacy.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/static_templates/ec_opened_pharmacy.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_opened_pharmacy_div">
    <div class="left_pharmacy_div">
        <div>
            <div class="icon_div"><i class="ec-icon-pharmacy"></i></div>
            <span></span>
            <div class="ec_header"><?php echo $_smarty_tpl->__("pharmacies_in_service");?>
</div>
            <div class="ec_product_count"><?php echo htmlspecialchars(fn_ec_get_opened_pharmacies(), ENT_QUOTES, 'UTF-8');?>
</div>
        </div>
    </div>
    <div class="ec_right_pharmacy_div">
        <div class="ec_icon_div">
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['dir']['root'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/map-dots.png">
        </div>
        <div class="ec_pharmacy_content">
            <div class="ec_header"><?php echo $_smarty_tpl->__("pharmacies_on_map");?>
</div>
            <div class="ec_content_text"><?php echo $_smarty_tpl->__("find_all_the_pharmacies_on_the_map");?>
</div>

            <div class="ec_show_map_links"><a href="<?php echo htmlspecialchars(fn_url("companies.catalog"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__primary"><?php echo $_smarty_tpl->__("show_on_map");?>
</a> <i class="ec-icon-onmap"></i></div>
        </div>
    </div>
    <div class="ec_end_banner_pharmacy">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/opened_pharmacies.jpg">
    </div>
</div>

<div class="ec_end_banner_pharmacy mob">
    <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/opened_pharmacies.jpg">
</div><?php }?><?php }} ?>
