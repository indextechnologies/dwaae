<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 22:20:32
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/categories/ec_category_block.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16530975576092e1f0961c07-81625578%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6e94b98807c276d195c2d517f0503d5d3227db1c' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/categories/ec_category_block.tpl',
      1 => 1606063482,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '16530975576092e1f0961c07-81625578',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'items' => 0,
    'block' => 0,
    'category' => 0,
    'language_direction' => 0,
    'prev_selector' => 0,
    'next_selector' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092e1f0982845_99404188',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092e1f0982845_99404188')) {function content_6092e1f0982845_99404188($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('no_image','no_image'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>

<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <div class="ec_category_block" id="scroll_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'UTF-8');?>
" class="owl-carousel">
        <?php  $_smarty_tpl->tpl_vars["category"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["category"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["category"]->key => $_smarty_tpl->tpl_vars["category"]->value) {
$_smarty_tpl->tpl_vars["category"]->_loop = true;
?>
            <div class="ec_cat_scroll_div">
                <a href="<?php echo htmlspecialchars(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['category']->value,$_smarty_tpl->tpl_vars['block']->value['type']), ENT_QUOTES, 'UTF-8');?>
">
                <?php if ($_smarty_tpl->tpl_vars['category']->value['icon_class']) {?>
                    <span class="ec_icon_div"><i class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['icon_class'], ENT_QUOTES, 'UTF-8');?>
"></i></span>
                <?php } else { ?>
                    <span class="ty-no-image"><i class="ty-no-image__icon ty-icon-image" title="<?php echo $_smarty_tpl->__("no_image");?>
"></i></span>
                <?php }?>
                </a>
                <div class="ec_category_header"><a href="<?php echo htmlspecialchars(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['category']->value,$_smarty_tpl->tpl_vars['block']->value['type']), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['category']->value['category'],20), ENT_QUOTES, 'UTF-8');?>
</a></div>
            </div>
        <?php } ?>
    </div>
    
    <?php echo smarty_function_script(array('src'=>"js/lib/owlcarousel/owl.carousel.min.js"),$_smarty_tpl);?>

    <?php echo '<script'; ?>
 type="text/javascript">
    (function(_, $) {
        $.ceEvent('on', 'ce.commoninit', function(context) {
            var elm = context.find('#scroll_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'UTF-8');?>
');

            var item = 12,
                // default setting of carousel
                itemsDesktop = 12,
                itemsDesktopSmall = 6;
                itemsTablet = 6;

            

            var desktop = [1199, itemsDesktop],
                desktopSmall = [979, itemsDesktopSmall],
                tablet = [768, itemsTablet],
                mobile = [479, 5];

            if (elm.length) {
                elm.owlCarousel({
                    direction: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language_direction']->value, ENT_QUOTES, 'UTF-8');?>
',
                    items: item,
                    itemsDesktop: desktop,
                    itemsDesktopSmall: desktopSmall,
                    itemsTablet: tablet,
                    itemsMobile: mobile,
                    autoPlay: false,
                    lazyLoad: true,
                    slideSpeed: 400,
                    stopOnHover: true,
                    navigation: false,                    
                    pagination: false,
                });

                    $('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prev_selector']->value, ENT_QUOTES, 'UTF-8');?>
').click(function(){
                        elm.trigger('owl.prev');
                    });
                    $('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_selector']->value, ENT_QUOTES, 'UTF-8');?>
').click(function(){
                        elm.trigger('owl.next');
                    });
                    
               

            }
        });
    }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>

<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/categories/ec_category_block.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/categories/ec_category_block.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>

<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <div class="ec_category_block" id="scroll_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'UTF-8');?>
" class="owl-carousel">
        <?php  $_smarty_tpl->tpl_vars["category"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["category"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["category"]->key => $_smarty_tpl->tpl_vars["category"]->value) {
$_smarty_tpl->tpl_vars["category"]->_loop = true;
?>
            <div class="ec_cat_scroll_div">
                <a href="<?php echo htmlspecialchars(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['category']->value,$_smarty_tpl->tpl_vars['block']->value['type']), ENT_QUOTES, 'UTF-8');?>
">
                <?php if ($_smarty_tpl->tpl_vars['category']->value['icon_class']) {?>
                    <span class="ec_icon_div"><i class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['icon_class'], ENT_QUOTES, 'UTF-8');?>
"></i></span>
                <?php } else { ?>
                    <span class="ty-no-image"><i class="ty-no-image__icon ty-icon-image" title="<?php echo $_smarty_tpl->__("no_image");?>
"></i></span>
                <?php }?>
                </a>
                <div class="ec_category_header"><a href="<?php echo htmlspecialchars(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['category']->value,$_smarty_tpl->tpl_vars['block']->value['type']), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['category']->value['category'],20), ENT_QUOTES, 'UTF-8');?>
</a></div>
            </div>
        <?php } ?>
    </div>
    
    <?php echo smarty_function_script(array('src'=>"js/lib/owlcarousel/owl.carousel.min.js"),$_smarty_tpl);?>

    <?php echo '<script'; ?>
 type="text/javascript">
    (function(_, $) {
        $.ceEvent('on', 'ce.commoninit', function(context) {
            var elm = context.find('#scroll_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'UTF-8');?>
');

            var item = 12,
                // default setting of carousel
                itemsDesktop = 12,
                itemsDesktopSmall = 6;
                itemsTablet = 6;

            

            var desktop = [1199, itemsDesktop],
                desktopSmall = [979, itemsDesktopSmall],
                tablet = [768, itemsTablet],
                mobile = [479, 5];

            if (elm.length) {
                elm.owlCarousel({
                    direction: '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language_direction']->value, ENT_QUOTES, 'UTF-8');?>
',
                    items: item,
                    itemsDesktop: desktop,
                    itemsDesktopSmall: desktopSmall,
                    itemsTablet: tablet,
                    itemsMobile: mobile,
                    autoPlay: false,
                    lazyLoad: true,
                    slideSpeed: 400,
                    stopOnHover: true,
                    navigation: false,                    
                    pagination: false,
                });

                    $('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prev_selector']->value, ENT_QUOTES, 'UTF-8');?>
').click(function(){
                        elm.trigger('owl.prev');
                    });
                    $('<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['next_selector']->value, ENT_QUOTES, 'UTF-8');?>
').click(function(){
                        elm.trigger('owl.next');
                    });
                    
               

            }
        });
    }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>

<?php }?>
<?php }?><?php }} ?>
