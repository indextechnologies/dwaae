<?php

/* __string_template__0731e4d258490b408ddc4c2b46dd9180bbda7e28e578f328ed6db308624733d2 */
class __TwigTemplate_1bfd5a2284aad59e0a0f184970bb851ccdd6064a2ad2b5e869b6c5b681e3fd3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_p_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__0731e4d258490b408ddc4c2b46dd9180bbda7e28e578f328ed6db308624733d2";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("change_order_status_p_subj", {"[order]": order_info.order_id}) }}*/
