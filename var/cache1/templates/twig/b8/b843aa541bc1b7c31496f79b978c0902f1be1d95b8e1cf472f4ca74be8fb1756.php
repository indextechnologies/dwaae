<?php

/* __string_template__0c4cd4be1e2ef934a27ba53324a96a590fcc7c022008c707c4c2fface9ca5e65 */
class __TwigTemplate_eb36dbb18b59eee989c1c3a4eefcf41b50f83b1638d70420f450a83d3b6f7a38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_d_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())))));
        // line 2
        if ($this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "firstname", array())) {
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello_name", array("[name]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "firstname", array())));
        } else {
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello");
            echo ",";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "customer");
        }
        // line 3
        echo "    <br />";
        // line 4
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_default_text", array("[status]" => $this->getAttribute((isset($context["order_status"]) ? $context["order_status"] : null), "description", array())));
        echo "
    <br />";
        // line 6
        if ((isset($context["reason"]) ? $context["reason"] : null)) {
            // line 7
            echo "<br />";
            // line 8
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "reason");
            echo ":";
            echo (isset($context["reason"]) ? $context["reason"] : null);
            echo "
<br /><br />";
        }
        // line 11
        echo "    <br />";
        echo $this->env->getExtension('tygh.core')->includeDocFunction($this->env, $context, "order.summary", $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array()));
        // line 12
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__0c4cd4be1e2ef934a27ba53324a96a590fcc7c022008c707c4c2fface9ca5e65";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 12,  46 => 11,  39 => 8,  37 => 7,  35 => 6,  31 => 4,  29 => 3,  21 => 2,  19 => 1,);
    }
}
/* {{ snippet("header", {"title": __("change_order_status_d_subj", {"[order]": order_info.order_id}) } ) }}*/
/*     {% if order_info.firstname %}{{__("hello_name", {"[name]" : order_info.firstname})}} {% else %} {{ __("hello") }},  {{ __("customer") }} {% endif %}*/
/*     <br />*/
/*     {{ __("change_order_status_default_text", {"[status]": order_status.description}) }}*/
/*     <br />*/
/* {% if reason %}*/
/* <br />*/
/* {{ __("reason") }}: {{ reason }}*/
/* <br /><br />*/
/* {% endif %}*/
/*     <br /> {{ include_doc("order.summary", order_info.order_id) }}*/
/*   {{ snippet("footer") }}*/
