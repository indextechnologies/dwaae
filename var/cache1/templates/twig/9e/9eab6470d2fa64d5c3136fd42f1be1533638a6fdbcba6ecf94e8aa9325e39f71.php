<?php

/* __string_template__004a65d4ec8009256ae4c24ed97e501085161ef7b180bbef92fb59a53abfdea2 */
class __TwigTemplate_fb020f2605fee121ca84834a18fb5e4ff8d1d2a1c2054c8d6178c8d72b2ae43f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table style=\"font-family: Helvetica, Arial, sans-serif; border-collapse: separate;\" border=\"0\" width=\"600\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"vertical-align: top;\">
<td>
<table style=\"border-collapse: separate; font-family: Helvetica, Arial, sans-serif;\" border=\"0\" width=\"100%;\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"padding: 0px; padding-bottom: 10px; padding-right: 40px; vertical-align: top; font-family: Helvetica, Arial, sans-serif;\" width=\"50%\">";
        // line 8
        if ($this->getAttribute((isset($context["pickup_point"]) ? $context["pickup_point"] : null), "is_selected", array())) {
            echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "pickup_point");
        } else {
            echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "ship_to");
        }
        echo "</td>
<td style=\"padding: 0px; padding-bottom: 10px; padding-left: 40px; -webkit-print-color-adjust: exact; vertical-align: top; font-family: Helvetica, Arial, sans-serif;\" width=\"50%\">
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;\"><span style=\"color: #444444; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;\">";
        // line 10
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "order_date");
        echo ": </span>";
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "timestamp", array());
        echo "</p>
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;\"><span style=\"color: #444444; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;\">";
        // line 11
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "payment");
        echo ": </span>";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "payment", array());
        echo "</p>
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;\"><span style=\"color: #444444; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;\">";
        // line 12
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "shipping");
        echo ": </span>";
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "shippings_method", array());
        echo "</p>";
        // line 13
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "tracking_number", array())) {
            // line 14
            echo "<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;\"><span style=\"color: #444444; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "tracking_number");
            echo ": </span>";
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "tracking_number", array());
            echo "</p>";
        }
        // line 15
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style=\"padding: 0px;\">";
        // line 22
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "products_table");
        echo "</td>
</tr>
<tr>
<td style=\"padding: 0px; border-top: 2px solid #f5f5f5; padding-top: 10px; font-family: Helvetica, Arial, sans-serif;\">
<table style=\"border-collapse: separate; font-family: Helvetica, Arial, sans-serif;\" width=\"100%\">
<tbody>
<tr>
<td style=\"font-size: 14px; font-family: Helvetica, Arial, sans-serif; line-height: 21px; color: #444444; padding-right: 30px; vertical-align: top;\" width=\"66%\">";
        // line 29
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "notes", array())) {
            // line 30
            echo "<h2 style=\"margin: 0px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-transform: uppercase; padding-bottom: 20px; border-bottom: 3px solid #e8e8e8; margin-bottom: 10px;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "customer_notes");
            echo "</h2>";
            // line 31
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "notes", array());
        }
        echo "</td>
<td style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\" width=\"34%\">
<table style=\"font-size: 14px; font-family: Helvetica, Arial, sans-serif; color: #444;\" width=\"100%;\">
<tbody>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 36
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "subtotal");
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 37
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "display_subtotal", array());
        echo "</td>
</tr>
<tr style=\"vertical-align: top;\">
<td style=\"padding-bottom: 20px; text-transform: uppercase; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 40
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "tax_name", array());
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 41
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "tax_total", array());
        echo "</td>
</tr>
<tr style=\"vertical-align: top;\">
<td style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 44
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "shipping");
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 45
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "display_shipping_cost", array());
        echo "</td>
</tr>
<tr style=\"vertical-align: top;\">
<td style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 48
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "payment_surcharge");
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 49
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "payment_surcharge", array());
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 52
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "coupon_code", array())) {
            // line 53
            echo "<div style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "coupon");
            echo "</div>";
        }
        // line 54
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 55
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "coupon_code", array())) {
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "coupon_code", array());
        }
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 58
        if ($this->getAttribute($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "raw", array()), "discount", array())) {
            // line 59
            echo "<div style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "including_discount");
            echo "</div>";
        }
        // line 60
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 61
        if ($this->getAttribute($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "raw", array()), "discount", array())) {
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "discount", array());
        }
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 64
        if ($this->getAttribute($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "raw", array()), "subtotal_discount", array())) {
            // line 65
            echo "<div style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "order_discount");
            echo "</div>";
        }
        // line 66
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 67
        if ($this->getAttribute($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "raw", array()), "subtotal_discount", array())) {
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "subtotal_discount", array());
        }
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-size: 22px; font-weight: 600; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"padding-top: 20px; border-top: 1px solid #e8e8e8; font-size: 22px; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 70
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "total");
        echo "</td>
<td style=\"padding-top: 20px; border-top: 1px solid #e8e8e8; font-size: 22px; font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 71
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "total", array());
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>";
    }

    public function getTemplateName()
    {
        return "__string_template__004a65d4ec8009256ae4c24ed97e501085161ef7b180bbef92fb59a53abfdea2";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  195 => 71,  191 => 70,  183 => 67,  180 => 66,  175 => 65,  173 => 64,  165 => 61,  162 => 60,  157 => 59,  155 => 58,  147 => 55,  144 => 54,  139 => 53,  137 => 52,  131 => 49,  127 => 48,  121 => 45,  117 => 44,  111 => 41,  107 => 40,  101 => 37,  97 => 36,  88 => 31,  84 => 30,  82 => 29,  72 => 22,  63 => 15,  56 => 14,  54 => 13,  49 => 12,  43 => 11,  37 => 10,  28 => 8,  19 => 1,);
    }
}
/* <table style="font-family: Helvetica, Arial, sans-serif; border-collapse: separate;" border="0" width="600" cellspacing="0" cellpadding="0">*/
/* <tbody>*/
/* <tr style="vertical-align: top;">*/
/* <td>*/
/* <table style="border-collapse: separate; font-family: Helvetica, Arial, sans-serif;" border="0" width="100%;" cellspacing="0">*/
/* <tbody>*/
/* <tr>*/
/* <td style="padding: 0px; padding-bottom: 10px; padding-right: 40px; vertical-align: top; font-family: Helvetica, Arial, sans-serif;" width="50%">{% if pickup_point.is_selected %} {{ snippet("pickup_point") }} {% else %} {{ snippet("ship_to") }} {% endif %}</td>*/
/* <td style="padding: 0px; padding-bottom: 10px; padding-left: 40px; -webkit-print-color-adjust: exact; vertical-align: top; font-family: Helvetica, Arial, sans-serif;" width="50%">*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;"><span style="color: #444444; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;">{{__("order_date")}}: </span>{{o.timestamp}}</p>*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;"><span style="color: #444444; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;">{{__("payment")}}: </span>{{p.payment}}</p>*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;"><span style="color: #444444; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;">{{__("shipping")}}: </span>{{o.shippings_method}}</p>*/
/* {% if o.tracking_number %}*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;"><span style="color: #444444; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;">{{__("tracking_number")}}: </span> {{o.tracking_number}}</p>*/
/* {% endif %}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr>*/
/* <td style="padding: 0px;">{{ snippet("products_table") }}</td>*/
/* </tr>*/
/* <tr>*/
/* <td style="padding: 0px; border-top: 2px solid #f5f5f5; padding-top: 10px; font-family: Helvetica, Arial, sans-serif;">*/
/* <table style="border-collapse: separate; font-family: Helvetica, Arial, sans-serif;" width="100%">*/
/* <tbody>*/
/* <tr>*/
/* <td style="font-size: 14px; font-family: Helvetica, Arial, sans-serif; line-height: 21px; color: #444444; padding-right: 30px; vertical-align: top;" width="66%">{% if o.notes %}*/
/* <h2 style="margin: 0px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-transform: uppercase; padding-bottom: 20px; border-bottom: 3px solid #e8e8e8; margin-bottom: 10px;">{{ __("customer_notes") }}</h2>*/
/* {{ o.notes }} {% endif %}</td>*/
/* <td style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;" width="34%">*/
/* <table style="font-size: 14px; font-family: Helvetica, Arial, sans-serif; color: #444;" width="100%;">*/
/* <tbody>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;" align="left">{{ __("subtotal") }}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{{o.display_subtotal}}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top;">*/
/* <td style="padding-bottom: 20px; text-transform: uppercase; font-family: Helvetica, Arial, sans-serif;" align="left">{{o.tax_name}}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{{o.tax_total}}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top;">*/
/* <td style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;" align="left">{{ __("shipping") }}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{{ o.display_shipping_cost }}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top;">*/
/* <td style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;" align="left">{{ __("payment_surcharge") }}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{{ o.payment_surcharge }}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="left">{% if o.coupon_code %}*/
/* <div style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;">{{ __("coupon") }}</div>*/
/* {% endif %}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{% if o.coupon_code %} {{o.coupon_code}} {% endif %}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="left">{% if o.raw.discount %}*/
/* <div style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;">{{ __("including_discount") }}</div>*/
/* {% endif %}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{% if o.raw.discount %} {{o.discount}} {% endif %}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="left">{% if o.raw.subtotal_discount %}*/
/* <div style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;">{{ __("order_discount") }}</div>*/
/* {% endif %}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{% if o.raw.subtotal_discount %} {{o.subtotal_discount}} {% endif %}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-size: 22px; font-weight: 600; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="padding-top: 20px; border-top: 1px solid #e8e8e8; font-size: 22px; font-family: Helvetica, Arial, sans-serif;" align="left">{{ __("total") }}</td>*/
/* <td style="padding-top: 20px; border-top: 1px solid #e8e8e8; font-size: 22px; font-family: Helvetica, Arial, sans-serif;" align="right">{{o.total}}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
