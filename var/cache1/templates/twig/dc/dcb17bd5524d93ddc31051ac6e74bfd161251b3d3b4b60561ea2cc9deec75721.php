<?php

/* __string_template__fe743fdfa3f214adfadd8f1e4e5abf0a51dd67cfafda9f785122233737b29090 */
class __TwigTemplate_986f8e0542eb7f2b40f591af823c0462158ca4ba049d3d86034a13d51303a408 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center; font-family: Helvetica, Arial, sans-serif;\"><strong style=\"font-weight: 600;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "amount", array());
        echo "</strong></p>";
    }

    public function getTemplateName()
    {
        return "__string_template__fe743fdfa3f214adfadd8f1e4e5abf0a51dd67cfafda9f785122233737b29090";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center; font-family: Helvetica, Arial, sans-serif;"><strong style="font-weight: 600;">{{ p.amount }}</strong></p>*/
