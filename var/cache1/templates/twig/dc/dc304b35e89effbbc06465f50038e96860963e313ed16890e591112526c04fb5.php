<?php

/* __string_template__15b624934f0d00f758eba4919af0843ba410272b1d59c45c00a2985631ebdaf7 */
class __TwigTemplate_718de3cc3329d47b4bad74d3f81f06bdd61733a1a472d9fb38952ac0db5f1b4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center; font-family: Helvetica, Arial, sans-serif;\"><strong style=\"font-weight: 600;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "original_price", array());
        echo "</strong></p>";
    }

    public function getTemplateName()
    {
        return "__string_template__15b624934f0d00f758eba4919af0843ba410272b1d59c45c00a2985631ebdaf7";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center; font-family: Helvetica, Arial, sans-serif;"><strong style="font-weight: 600;">{{ p.original_price }}</strong></p>*/
