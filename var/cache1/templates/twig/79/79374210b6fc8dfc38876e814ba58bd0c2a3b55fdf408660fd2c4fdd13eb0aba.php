<?php

/* __string_template__e98559594cfaad158eb0b11232ec74adcb70c694737720e6880e5b9d7180f1ad */
class __TwigTemplate_2e1c862f825826bcff60107be1ac94528efa2b25932ebdc7f05c3b2725513667 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header");
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "vendor_candidate_notification", array("[href]" => (isset($context["company_update_url"]) ? $context["company_update_url"] : null)));
        echo "

<br/><br/>

<table>
    <tr>
        <td class=\"form-field-caption\" nowrap>";
        // line 9
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "company_name");
        echo ":&nbsp;</td>
        <td>";
        // line 10
        echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "company", array());
        echo "</td>
    </tr>";
        // line 13
        if ($this->getAttribute((isset($context["company"]) ? $context["company"] : null), "company_description", array())) {
            // line 14
            echo "    <tr>
        <td class=\"form-field-caption\" nowrap>";
            // line 15
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "description");
            echo ":&nbsp;</td>
        <td>";
            // line 16
            echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "company_description", array());
            echo "</td>
    </tr>";
        }
        // line 20
        if ($this->getAttribute((isset($context["company"]) ? $context["company"] : null), "request_account_name", array())) {
            // line 21
            echo "    <tr>
        <td class=\"form-field-caption\" nowrap>";
            // line 22
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "account_name");
            echo ":&nbsp;</td>
        <td>";
            // line 23
            echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "request_account_name", array());
            echo "</td>
    </tr>";
        }
        // line 27
        if ($this->getAttribute((isset($context["company"]) ? $context["company"] : null), "admin_firstname", array())) {
            // line 28
            echo "    <tr>
        <td class=\"form-field-caption\" nowrap>";
            // line 29
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "first_name");
            echo ":&nbsp;</td>
        <td>";
            // line 30
            echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "admin_firstname", array());
            echo "</td>
    </tr>";
        }
        // line 34
        if ($this->getAttribute((isset($context["company"]) ? $context["company"] : null), "admin_lastname", array())) {
            // line 35
            echo "    <tr>
        <td class=\"form-field-caption\" nowrap>";
            // line 36
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "last_name");
            echo ":&nbsp;</td>
        <td>";
            // line 37
            echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "admin_lastname", array());
            echo "</td>
    </tr>";
        }
        // line 40
        echo "
    <tr>
        <td class=\"form-field-caption\" nowrap>";
        // line 42
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "email");
        echo ":&nbsp;</td>
        <td>";
        // line 43
        echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "email", array());
        echo "</td>
    </tr>
    <tr>
        <td class=\"form-field-caption\" nowrap>";
        // line 46
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "phone");
        echo ":&nbsp;</td>
        <td><bdi>";
        // line 47
        echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "phone", array());
        echo "</bdi></td>
    </tr>
    <tr>";
        // line 51
        if ($this->getAttribute((isset($context["company"]) ? $context["company"] : null), "url", array())) {
            // line 52
            echo "        <td class=\"form-field-caption\" nowrap>";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "url");
            echo ":&nbsp;</td>
        <td >";
            // line 53
            echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "url", array());
            echo "</td>
    </tr>";
        }
        // line 56
        echo "
    <tr>
        <td class=\"form-field-caption\" nowrap>";
        // line 58
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "address");
        echo ":&nbsp;</td>
        <td>";
        // line 59
        echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "address", array());
        echo "</td>
    </tr>
    <tr>
        <td class=\"form-field-caption\" nowrap>";
        // line 62
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "city");
        echo ":&nbsp;</td>
        <td>";
        // line 63
        echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "city", array());
        echo "</td>
    </tr>
    <tr>
        <td class=\"form-field-caption\" nowrap>";
        // line 66
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "country");
        echo ":&nbsp;</td>
        <td>";
        // line 67
        echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "country", array());
        echo "</td>
    </tr>
    <tr>
        <td class=\"form-field-caption\" nowrap>";
        // line 70
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "state");
        echo ":&nbsp;</td>
        <td>";
        // line 71
        echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "state", array());
        echo "</td>
    </tr>
    <tr>
        <td class=\"form-field-caption\" nowrap>";
        // line 74
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "zip_postal_code");
        echo ":&nbsp;</td>
        <td >";
        // line 75
        echo $this->getAttribute((isset($context["company"]) ? $context["company"] : null), "zipcode", array());
        echo "</td>
    </tr>
</table>";
        // line 79
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
        echo "
";
    }

    public function getTemplateName()
    {
        return "__string_template__e98559594cfaad158eb0b11232ec74adcb70c694737720e6880e5b9d7180f1ad";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 79,  177 => 75,  173 => 74,  167 => 71,  163 => 70,  157 => 67,  153 => 66,  147 => 63,  143 => 62,  137 => 59,  133 => 58,  129 => 56,  124 => 53,  119 => 52,  117 => 51,  112 => 47,  108 => 46,  102 => 43,  98 => 42,  94 => 40,  89 => 37,  85 => 36,  82 => 35,  80 => 34,  75 => 30,  71 => 29,  68 => 28,  66 => 27,  61 => 23,  57 => 22,  54 => 21,  52 => 20,  47 => 16,  43 => 15,  40 => 14,  38 => 13,  34 => 10,  30 => 9,  21 => 3,  19 => 1,);
    }
}
/* {{ snippet("header") }}*/
/* */
/* {{ __("vendor_candidate_notification", {"[href]": company_update_url}) }}*/
/* */
/* <br/><br/>*/
/* */
/* <table>*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("company_name") }}:&nbsp;</td>*/
/*         <td>{{ company.company }}</td>*/
/*     </tr>*/
/* */
/*     {% if company.company_description %}*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("description") }}:&nbsp;</td>*/
/*         <td>{{ company.company_description }}</td>*/
/*     </tr>*/
/*     {% endif %}*/
/* */
/*     {% if company.request_account_name %}*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("account_name") }}:&nbsp;</td>*/
/*         <td>{{ company.request_account_name }}</td>*/
/*     </tr>*/
/*     {% endif %}*/
/* */
/*     {% if company.admin_firstname %}*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("first_name") }}:&nbsp;</td>*/
/*         <td>{{ company.admin_firstname }}</td>*/
/*     </tr>*/
/*     {% endif %}*/
/* */
/*     {% if company.admin_lastname %}*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("last_name") }}:&nbsp;</td>*/
/*         <td>{{ company.admin_lastname }}</td>*/
/*     </tr>*/
/*     {% endif %}*/
/* */
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("email") }}:&nbsp;</td>*/
/*         <td>{{ company.email }}</td>*/
/*     </tr>*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("phone") }}:&nbsp;</td>*/
/*         <td><bdi>{{ company.phone }}</bdi></td>*/
/*     </tr>*/
/*     <tr>*/
/* */
/*     {% if company.url %}*/
/*         <td class="form-field-caption" nowrap>{{ __("url") }}:&nbsp;</td>*/
/*         <td >{{ company.url }}</td>*/
/*     </tr>*/
/*     {% endif %}*/
/* */
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("address") }}:&nbsp;</td>*/
/*         <td>{{ company.address }}</td>*/
/*     </tr>*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("city") }}:&nbsp;</td>*/
/*         <td>{{ company.city }}</td>*/
/*     </tr>*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("country") }}:&nbsp;</td>*/
/*         <td>{{ company.country }}</td>*/
/*     </tr>*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("state") }}:&nbsp;</td>*/
/*         <td>{{ company.state }}</td>*/
/*     </tr>*/
/*     <tr>*/
/*         <td class="form-field-caption" nowrap>{{ __("zip_postal_code") }}:&nbsp;</td>*/
/*         <td >{{ company.zipcode }}</td>*/
/*     </tr>*/
/* </table>*/
/* */
/* {{ snippet("footer") }}*/
/* */
