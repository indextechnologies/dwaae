<?php

/* __string_template__9a8e33d4ec9cc6132860ba28d14e067e989ab433b3e95b07dcdb62594125c483 */
class __TwigTemplate_96d53c2c0deb8733908fc20610976d26dd5cab74fa0064580c2a2e231a33eec5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["company_name"]) ? $context["company_name"] : null);
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_d_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__9a8e33d4ec9cc6132860ba28d14e067e989ab433b3e95b07dcdb62594125c483";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ company_name }}: {{ __("change_order_status_d_subj", {"[order]": order_info.order_id}) }}*/
