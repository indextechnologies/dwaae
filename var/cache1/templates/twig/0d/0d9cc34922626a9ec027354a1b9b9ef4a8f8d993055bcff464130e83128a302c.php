<?php

/* __string_template__7a5405312764df82fe40a5fb3b159986e2ea4447d26ee7158092a165044a7a5e */
class __TwigTemplate_e1e2d36b5f96c1c385faff820fed2eaa5a4f54f25da9853dead0eff78ff64d7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_p_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__7a5405312764df82fe40a5fb3b159986e2ea4447d26ee7158092a165044a7a5e";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("change_order_status_p_subj", {"[order]": order_info.order_id}) }}*/
