<?php

/* __string_template__24b9ebc9f6da1919cedb1cfd6b761609ec8a7f984513e4b45e066f0fb6ba4441 */
class __TwigTemplate_b3b1838b113d80d89cf934d7c37c0b67857c9bdd2fb63c4e61bef3d4c09d2007 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.prescription_order_on_the_way");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 5
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => (isset($context["title"]) ? $context["title"] : null)));
        echo "
            <p>";
        // line 7
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello");
        echo "<br/>";
        // line 8
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.prescription_order_on_the_way");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
        echo "<br/>";
        // line 9
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.view_your_request", array("[link]" => (isset($context["link"]) ? $context["link"] : null)));
        echo "<br/>
            </p><br/>";
        // line 11
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__24b9ebc9f6da1919cedb1cfd6b761609ec8a7f984513e4b45e066f0fb6ba4441";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 11,  42 => 9,  35 => 8,  32 => 7,  28 => 5,  21 => 3,  19 => 2,);
    }
}
/* */
/*             {% set title %}*/
/*             {{__("d_prescription.prescription_order_on_the_way")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
/*             {% endset %}*/
/*             {{ snippet("header", {"title": title }) }}*/
/*             <p>*/
/*                 {{__("hello")}}<br/>*/
/*                 {{__("d_prescription.prescription_order_on_the_way")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}<br/>*/
/*                 {{__("d_prescription.view_your_request", {"[link]" : link})}}<br/>*/
/*             </p><br/>*/
/*                   {{ snippet("footer") }}*/
