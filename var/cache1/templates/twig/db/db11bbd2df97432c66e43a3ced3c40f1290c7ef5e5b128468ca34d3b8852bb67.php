<?php

/* __string_template__b0e5db396caa3269d83026a58c47c552fc7b3686cea5a3f4ba8db8c950da1224 */
class __TwigTemplate_6459e0ea7dec0d576a47dd9e06bb1b28ec20d3fe14d54a0928412c07818f8ea4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>";
        if ($this->getAttribute((isset($context["p"]) ? $context["p"] : null), "url", array())) {
            echo "<a href=\"";
            echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "url", array());
            echo "\">";
            echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "name", array());
            echo "</a>";
        } else {
            echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "name", array());
        }
        if ($this->getAttribute((isset($context["p"]) ? $context["p"] : null), "options", array())) {
            echo "<br />";
            echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "options", array());
            echo "<br />";
        }
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "__string_template__b0e5db396caa3269d83026a58c47c552fc7b3686cea5a3f4ba8db8c950da1224";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p>{% if p.url %}<a href="{{ p.url }}">{{ p.name }}</a>{% else %}{{ p.name }}{% endif %} {% if p.options %}<br />{{ p.options }}<br />{% endif %}</p>*/
