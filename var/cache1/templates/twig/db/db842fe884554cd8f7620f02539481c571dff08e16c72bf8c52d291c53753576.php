<?php

/* __string_template__4ef34dfadf7bca42a1d138c8be08d033eebe409113ac68d7ce55239ac924cdc5 */
class __TwigTemplate_7ab92f4ce02cff8ef979c5c63044c275e161e558dd61fa95f07ad0858a4c7c9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["company_name"]) ? $context["company_name"] : null);
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "recover_password_subj");
    }

    public function getTemplateName()
    {
        return "__string_template__4ef34dfadf7bca42a1d138c8be08d033eebe409113ac68d7ce55239ac924cdc5";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ company_name }}: {{ __("recover_password_subj") }}*/
