<?php

/* __string_template__0d915b22eee38eca01c36bc86522844349189c5fd98e1d067a5b2ba039deac62 */
class __TwigTemplate_467965ad0e64048d109a1ff32e6db68e6715221e5a17db2462510e918eb0b2ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "insurance_status", array());
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "__string_template__0d915b22eee38eca01c36bc86522844349189c5fd98e1d067a5b2ba039deac62";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center;">{{p.insurance_status}}</p>*/
