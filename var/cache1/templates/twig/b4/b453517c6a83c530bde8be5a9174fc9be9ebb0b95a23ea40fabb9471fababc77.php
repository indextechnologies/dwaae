<?php

/* __string_template__ac5097d028b9d3291960a7d82f686532fae370557da1824dd790b6a3b8db1fc3 */
class __TwigTemplate_7888c091b9cb7ff1abc58170007c3db90d4e2185c08969e91e70f0801f0104f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["products_table"]) ? $context["products_table"] : null), "rows", array())) {
            // line 2
            echo "\t\t\t\t\t<table width=\"100%\" cellpadding=\"0\" cellspacing=\"1\"
\t\t\t\t\t    style=\"background-color: #dddddd; -webkit-print-color-adjust: exact;\">
\t\t\t\t\t    <thead>
\t\t\t\t\t        <tr style=\"height: 40px;\">";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["products_table"]) ? $context["products_table"] : null), "headers", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                // line 8
                echo "\t\t\t\t\t            <th
\t\t\t\t\t                style=\"background-color: #1A516A; color: white; padding: 5px 1px; white-space: nowrap; font-size: 12px; font-family: Arial;\">";
                // line 10
                echo $context["header"];
                echo "</th>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "\t\t\t\t\t        </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["products_table"]) ? $context["products_table"] : null), "rows", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 16
                if (($this->getAttribute($context["loop"], "index", array()) == 22)) {
                    echo "<tr style=\"height:60px; background: white; border: 0px;\"><td colspan=\"7\"></td></tr>";
                }
                // line 17
                echo "\t\t\t\t\t        <tr
\t\t\t\t\t            style=\"height: 40px;padding: 5px; background-color: #ffffff; font-size: 13px; font-family: Arial; border: 0px solid #eeeeee; text-align: center;\">";
                // line 19
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["row"]);
                foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                    // line 20
                    echo "\t\t\t\t\t            <td style=\"border: 0px solid #eeeeee; padding:5px; padding-top:2px;padding-bottom:2px;text-align:left; \">";
                    // line 21
                    echo $context["column"];
                    echo "
\t\t\t\t\t            </td>";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 24
                echo "\t\t\t\t\t        </tr>";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "\t\t\t\t\t    </tbody>
\t\t\t\t\t</table>";
        }
    }

    public function getTemplateName()
    {
        return "__string_template__ac5097d028b9d3291960a7d82f686532fae370557da1824dd790b6a3b8db1fc3";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 26,  82 => 24,  74 => 21,  72 => 20,  68 => 19,  65 => 17,  61 => 16,  44 => 15,  40 => 12,  33 => 10,  30 => 8,  26 => 7,  21 => 2,  19 => 1,);
    }
}
/* 					{% if products_table.rows %}*/
/* 					<table width="100%" cellpadding="0" cellspacing="1"*/
/* 					    style="background-color: #dddddd; -webkit-print-color-adjust: exact;">*/
/* 					    <thead>*/
/* 					        <tr style="height: 40px;">*/
/* */
/* 					            {% for  header in products_table.headers %}*/
/* 					            <th*/
/* 					                style="background-color: #1A516A; color: white; padding: 5px 1px; white-space: nowrap; font-size: 12px; font-family: Arial;">*/
/* 					                {{ header }}</th>*/
/* 					            {% endfor %}*/
/* 					        </tr>*/
/* 					    </thead>*/
/* 					    <tbody>*/
/* 					        {% for  row in products_table.rows %}*/
/* {% if loop.index == 22 %}<tr style="height:60px; background: white; border: 0px;"><td colspan="7"></td></tr>{% endif %}*/
/* 					        <tr*/
/* 					            style="height: 40px;padding: 5px; background-color: #ffffff; font-size: 13px; font-family: Arial; border: 0px solid #eeeeee; text-align: center;">*/
/* 					            {% for  column in row %}*/
/* 					            <td style="border: 0px solid #eeeeee; padding:5px; padding-top:2px;padding-bottom:2px;text-align:left; ">*/
/* 					                {{ column }}*/
/* 					            </td>*/
/* 					            {% endfor %}*/
/* 					        </tr>*/
/* 					        {% endfor %}*/
/* 					    </tbody>*/
/* 					</table>*/
/* 					{% endif %}*/
