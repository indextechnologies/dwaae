<?php

/* __string_template__f35395ecbc76586b64f54ae8256164e662ecf3ebae17c3ec9bb81b6aa41e0be3 */
class __TwigTemplate_b39e02af10838feed18f255f216104e48c36e97a3098792e4d1d2d540f5c919e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "total", array());
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "__string_template__f35395ecbc76586b64f54ae8256164e662ecf3ebae17c3ec9bb81b6aa41e0be3";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center;">{{ p.total }}</p>*/
