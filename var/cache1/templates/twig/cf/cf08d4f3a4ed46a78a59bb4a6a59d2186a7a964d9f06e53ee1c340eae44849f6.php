<?php

/* __string_template__ee8ac8e65c2773aeb4d2ade41eca89398bc29c5ced2795dacce352675395dbd8 */
class __TwigTemplate_e7dd23cbc2f13dec08e76509601e0990434f6ab675c5003ea1cbe88cff0cdfb7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">

<head>
  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
  <title>";
        // line 7
        echo (isset($context["company_name"]) ? $context["company_name"] : null);
        echo ": Message title</title>
  <style type=\"text/css\">
    #outlook a {
      padding: 0;
    }

    body {
      width: 100% !important;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      margin: 0;
      padding: 0;
    }

    img {
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a {
      outline: none;
    }

    a img {
      border: none;
    }

    .image_fix {
      display: block;
    }

    .message-header > td {
      padding: 10px 30px 20px 30px;
    }

    .message-header__title {
      background-color:";
        // line 44
        if ($this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "links", array())) {
            echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "links", array());
        } else {
            echo "#999";
        }
        // line 45
        echo "    }

    .message-header__title > td {
      padding: 20px 30px;
    }

    .message-header__title h1 {
      font-size: 20px;
      text-transform: uppercase;
      font-weight: 400;
      color: #Fff;
    }

    .message-title > td,
    .message-body > td {
      padding: 30px;
    }

    .message-footer > td {
      

    }

    .message-copyright > td {
      padding: 0px 30px 10px;
    }

    .message-header td,
    .message-title td,
    .message-body th, .message-body td,
    .message-footer th, .message-footer td,
    .message-copyright th, .message-copyright td {
  font-size:";
        // line 77
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "body_font_size", array());
        echo ";
      font-family:";
        // line 78
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "body_font", array());
        echo ",Helvetica,Arial,sans-serif;
      font: Regular 15px/30px Arial;
letter-spacing: 0px;
color: #000000;
    
    }



    .message-footer {
      border-top: 1px solid";
        // line 88
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "base", array());
        echo "
    }

    .message-body table th,
    .message-footer table th {
      text-transform: uppercase;
      border-bottom: 1px solid";
        // line 94
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "base", array());
        echo ";
      text-align: left;
    }

    .message-body table td,
    .message-footer table td {
      padding: 5px;
    }

    .message-footer table th{
        border: none;
    }

    .message-footer td {
      color: #fff;
    }

    .footer-contact__title {
      margin: 0px;
      text-transform: uppercase;
      font-size: 16px !important;
      font-weight: 600;
      color: #fff !important;
    }

    .footer-social__title {
      margin: 0px;
      text-transform: uppercase;
      font-size: 16px !important;
      font-weight: 600;
      color: #fff !important;
    }

    .message-footer table td.footer-social td {
      padding: 0px;
      padding-right: 10px;
    }

    .email-preview{
      display:none;
      font-size:1px;
      color:#333333;
      line-height:1px;
      max-height:0px;
      max-width:0px;
      opacity:0;
      overflow:hidden;
    }

    .with-subline {
      color:";
        // line 144
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "font", array());
        echo ";
      text-transform: uppercase;
      font-weight: 700;
      font-size: 1em;
      padding-bottom: 10px;
      border-bottom: 1px solid #D4D4D4;
    }

    p {
      margin: 1em 0;
    }

    h1,h2,h3,h4,h5,h6 {
      color:";
        // line 157
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "font", array());
        echo ";
    }

    h1 a,h2 a,h3 a,h4 a,h5 a,h6 a {
      color:";
        // line 161
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "links", array());
        echo ";
    }

    h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active {
      color: red;
    }

    h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited {
      color: purple;
    }

    table td,
    table th {
      border-collapse: collapse;
    }

    table {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    address {
      margin: 0px;
    }

    .content-wrapper {
      border: 1px solid";
        // line 188
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "base", array());
        echo ";
      background-color:";
        // line 189
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "general_bg_color", array());
        echo ";
    }

    .copyright td {
      padding: 10px 0 0 0;
      padding-bottom: 0 !important;
    }

    a {
      color:";
        // line 198
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "links", array());
        echo ";
    }";
        // line 201
        if (((isset($context["language_direction"]) ? $context["language_direction"] : null) == "rtl")) {
            // line 202
            echo "
    .ty-email-footer {
      text-align: center !important;
    }

    .ty-email-footer-social-buttons {
      text-align: center !important;
    }

    .ty-email-footer-right-part {
      text-align: center !important;
      float: left !important;
    }

    .ty-email-footer-left-part {
      text-align: center !important;
      float: right !important;
    }";
        }
        // line 222
        echo "
.ec-message-footer td,.ec-message-footer th{
font: Regular 15px/17px Arial;
letter-spacing: 0px;
color: #393939;
}
.ec-header{
background: #0B669105 0% 0% no-repeat padding-box;
opacity: 1;
}
.ec-title{
text-align: left;
font: Bold 15px/17px Arial;
letter-spacing: 0px;
color: #393939;
opacity: 1;
}
.ec-bold{font: Bold 15px/30px Arial;
letter-spacing: 0px;
color: #000000;}
.ec-message-footer{

}
  </style>
</head>

<body>
    <style>

      @media only screen and (max-device-width: 480px){
        .content-wrapper{width: 100% !important;border: 3px solid #ccc !important; }

        table[width]{
          width: 100%! important;
        }

        .message-header > td,
        .message-title > td,
        .message-body > td,
        .message-footer > td,
        .message-copyright > td {
          padding: 10px !important;
        }

        .message-header td,
        .message-title td,
        .message-body th, .message-body td,
        .message-footer th, .message-footer td,
        .message-copyright th, .message-copyright td {
          font-size: 16px !important;
        }
      }

      @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {}

      @media only screen and (-webkit-min-device-pixel-ratio: 2) {}

      @media only screen and (-webkit-device-pixel-ratio:.75) {}

      @media only screen and (-webkit-device-pixel-ratio:1) {}

      @media only screen and (-webkit-device-pixel-ratio:1.5) {}
    </style>
  <!-- Targeting Windows Mobile -->
  <!--[if IEMobile 7]>
  <style type=\"text/css\">

  </style>
  <![endif]-->

  <!--[if gte mso 9]>
    <style>
        /* Target Outlook 2007 and 2010 */
    </style>
  <![endif]-->
  <table class=\"main-wrapper\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" bgcolor=\"";
        // line 297
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "base", array());
        echo "\" style=\"background-color:";
        echo $this->getAttribute((isset($context["styles"]) ? $context["styles"] : null), "base", array());
        echo "\" dir=\"";
        echo (isset($context["language_direction"]) ? $context["language_direction"] : null);
        echo "\">
    <tr>
      <td style=\"padding: 40px 10px 40px 10px;background: #F2F2F2 0% 0% no-repeat padding-box;
opacity: 1;\" >
        <table class=\"content-wrapper\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"600;\" style=\"background: #FFFFFF 0% 0% no-repeat padding-box;box-shadow: 0px 3px 6px #00000029;border: 1px solid #E6E8EB;opacity: 1;\">
          <tr class=\"message-header\">
            <td class=\"ec-header\">
              <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">
                <tr>
                  <td><a href=\"";
        // line 306
        echo $this->getAttribute((isset($context["company_data"]) ? $context["company_data"] : null), "company_website", array());
        echo "\"><img src=\"http://dwaae.com/images/DWAAE_Logo@2x.png\" alt=\"";
        echo $this->getAttribute((isset($context["company_data"]) ? $context["company_data"] : null), "company_name", array());
        echo "\" width=\"97px\" height=\"40px\" style=\"width:97px;height:50px;\"/></a></td>
<td style=\"text-align: right;\">";
        // line 308
        if ((isset($context["title"]) ? $context["title"] : null)) {
            echo "<span class=\"ec-title\">";
            echo (isset($context["title"]) ? $context["title"] : null);
            echo "</span>";
        }
        // line 309
        echo "</td>
                </tr>
              </table>
            </td>
          </tr>
         

          <tr class=\"message-body\">

<td>";
    }

    public function getTemplateName()
    {
        return "__string_template__ee8ac8e65c2773aeb4d2ade41eca89398bc29c5ced2795dacce352675395dbd8";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  386 => 309,  380 => 308,  374 => 306,  358 => 297,  281 => 222,  261 => 202,  259 => 201,  255 => 198,  243 => 189,  239 => 188,  209 => 161,  202 => 157,  186 => 144,  133 => 94,  124 => 88,  111 => 78,  107 => 77,  73 => 45,  67 => 44,  27 => 7,  19 => 1,);
    }
}
/* <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">*/
/* <html xmlns="http://www.w3.org/1999/xhtml">*/
/* */
/* <head>*/
/*   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />*/
/*   <meta name="viewport" content="width=device-width, initial-scale=1.0" />*/
/*   <title>{{ company_name }}: Message title</title>*/
/*   <style type="text/css">*/
/*     #outlook a {*/
/*       padding: 0;*/
/*     }*/
/* */
/*     body {*/
/*       width: 100% !important;*/
/*       -webkit-text-size-adjust: 100%;*/
/*       -ms-text-size-adjust: 100%;*/
/*       margin: 0;*/
/*       padding: 0;*/
/*     }*/
/* */
/*     img {*/
/*       outline: none;*/
/*       text-decoration: none;*/
/*       -ms-interpolation-mode: bicubic;*/
/*     }*/
/* */
/*     a {*/
/*       outline: none;*/
/*     }*/
/* */
/*     a img {*/
/*       border: none;*/
/*     }*/
/* */
/*     .image_fix {*/
/*       display: block;*/
/*     }*/
/* */
/*     .message-header > td {*/
/*       padding: 10px 30px 20px 30px;*/
/*     }*/
/* */
/*     .message-header__title {*/
/*       background-color: {% if styles.links %}{{styles.links}}{% else %}#999{% endif %}*/
/*     }*/
/* */
/*     .message-header__title > td {*/
/*       padding: 20px 30px;*/
/*     }*/
/* */
/*     .message-header__title h1 {*/
/*       font-size: 20px;*/
/*       text-transform: uppercase;*/
/*       font-weight: 400;*/
/*       color: #Fff;*/
/*     }*/
/* */
/*     .message-title > td,*/
/*     .message-body > td {*/
/*       padding: 30px;*/
/*     }*/
/* */
/*     .message-footer > td {*/
/*       */
/* */
/*     }*/
/* */
/*     .message-copyright > td {*/
/*       padding: 0px 30px 10px;*/
/*     }*/
/* */
/*     .message-header td,*/
/*     .message-title td,*/
/*     .message-body th, .message-body td,*/
/*     .message-footer th, .message-footer td,*/
/*     .message-copyright th, .message-copyright td {*/
/*   font-size: {{styles.body_font_size}};*/
/*       font-family: {{styles.body_font}},Helvetica,Arial,sans-serif;*/
/*       font: Regular 15px/30px Arial;*/
/* letter-spacing: 0px;*/
/* color: #000000;*/
/*     */
/*     }*/
/* */
/* */
/* */
/*     .message-footer {*/
/*       border-top: 1px solid {{styles.base}}*/
/*     }*/
/* */
/*     .message-body table th,*/
/*     .message-footer table th {*/
/*       text-transform: uppercase;*/
/*       border-bottom: 1px solid {{styles.base}};*/
/*       text-align: left;*/
/*     }*/
/* */
/*     .message-body table td,*/
/*     .message-footer table td {*/
/*       padding: 5px;*/
/*     }*/
/* */
/*     .message-footer table th{*/
/*         border: none;*/
/*     }*/
/* */
/*     .message-footer td {*/
/*       color: #fff;*/
/*     }*/
/* */
/*     .footer-contact__title {*/
/*       margin: 0px;*/
/*       text-transform: uppercase;*/
/*       font-size: 16px !important;*/
/*       font-weight: 600;*/
/*       color: #fff !important;*/
/*     }*/
/* */
/*     .footer-social__title {*/
/*       margin: 0px;*/
/*       text-transform: uppercase;*/
/*       font-size: 16px !important;*/
/*       font-weight: 600;*/
/*       color: #fff !important;*/
/*     }*/
/* */
/*     .message-footer table td.footer-social td {*/
/*       padding: 0px;*/
/*       padding-right: 10px;*/
/*     }*/
/* */
/*     .email-preview{*/
/*       display:none;*/
/*       font-size:1px;*/
/*       color:#333333;*/
/*       line-height:1px;*/
/*       max-height:0px;*/
/*       max-width:0px;*/
/*       opacity:0;*/
/*       overflow:hidden;*/
/*     }*/
/* */
/*     .with-subline {*/
/*       color: {{styles.font}};*/
/*       text-transform: uppercase;*/
/*       font-weight: 700;*/
/*       font-size: 1em;*/
/*       padding-bottom: 10px;*/
/*       border-bottom: 1px solid #D4D4D4;*/
/*     }*/
/* */
/*     p {*/
/*       margin: 1em 0;*/
/*     }*/
/* */
/*     h1,h2,h3,h4,h5,h6 {*/
/*       color: {{styles.font}};*/
/*     }*/
/* */
/*     h1 a,h2 a,h3 a,h4 a,h5 a,h6 a {*/
/*       color: {{styles.links}};*/
/*     }*/
/* */
/*     h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active {*/
/*       color: red;*/
/*     }*/
/* */
/*     h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited {*/
/*       color: purple;*/
/*     }*/
/* */
/*     table td,*/
/*     table th {*/
/*       border-collapse: collapse;*/
/*     }*/
/* */
/*     table {*/
/*       border-collapse: collapse;*/
/*       mso-table-lspace: 0pt;*/
/*       mso-table-rspace: 0pt;*/
/*     }*/
/* */
/*     address {*/
/*       margin: 0px;*/
/*     }*/
/* */
/*     .content-wrapper {*/
/*       border: 1px solid {{ styles.base }};*/
/*       background-color: {{ styles.general_bg_color }};*/
/*     }*/
/* */
/*     .copyright td {*/
/*       padding: 10px 0 0 0;*/
/*       padding-bottom: 0 !important;*/
/*     }*/
/* */
/*     a {*/
/*       color: {{styles.links}};*/
/*     }*/
/* */
/*     {% if language_direction == "rtl" %}*/
/* */
/*     .ty-email-footer {*/
/*       text-align: center !important;*/
/*     }*/
/* */
/*     .ty-email-footer-social-buttons {*/
/*       text-align: center !important;*/
/*     }*/
/* */
/*     .ty-email-footer-right-part {*/
/*       text-align: center !important;*/
/*       float: left !important;*/
/*     }*/
/* */
/*     .ty-email-footer-left-part {*/
/*       text-align: center !important;*/
/*       float: right !important;*/
/*     }*/
/* */
/*     {% endif %}*/
/* */
/* .ec-message-footer td,.ec-message-footer th{*/
/* font: Regular 15px/17px Arial;*/
/* letter-spacing: 0px;*/
/* color: #393939;*/
/* }*/
/* .ec-header{*/
/* background: #0B669105 0% 0% no-repeat padding-box;*/
/* opacity: 1;*/
/* }*/
/* .ec-title{*/
/* text-align: left;*/
/* font: Bold 15px/17px Arial;*/
/* letter-spacing: 0px;*/
/* color: #393939;*/
/* opacity: 1;*/
/* }*/
/* .ec-bold{font: Bold 15px/30px Arial;*/
/* letter-spacing: 0px;*/
/* color: #000000;}*/
/* .ec-message-footer{*/
/* */
/* }*/
/*   </style>*/
/* </head>*/
/* */
/* <body>*/
/*     <style>*/
/* */
/*       @media only screen and (max-device-width: 480px){*/
/*         .content-wrapper{width: 100% !important;border: 3px solid #ccc !important; }*/
/* */
/*         table[width]{*/
/*           width: 100%! important;*/
/*         }*/
/* */
/*         .message-header > td,*/
/*         .message-title > td,*/
/*         .message-body > td,*/
/*         .message-footer > td,*/
/*         .message-copyright > td {*/
/*           padding: 10px !important;*/
/*         }*/
/* */
/*         .message-header td,*/
/*         .message-title td,*/
/*         .message-body th, .message-body td,*/
/*         .message-footer th, .message-footer td,*/
/*         .message-copyright th, .message-copyright td {*/
/*           font-size: 16px !important;*/
/*         }*/
/*       }*/
/* */
/*       @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {}*/
/* */
/*       @media only screen and (-webkit-min-device-pixel-ratio: 2) {}*/
/* */
/*       @media only screen and (-webkit-device-pixel-ratio:.75) {}*/
/* */
/*       @media only screen and (-webkit-device-pixel-ratio:1) {}*/
/* */
/*       @media only screen and (-webkit-device-pixel-ratio:1.5) {}*/
/*     </style>*/
/*   <!-- Targeting Windows Mobile -->*/
/*   <!--[if IEMobile 7]>*/
/*   <style type="text/css">*/
/* */
/*   </style>*/
/*   <![endif]-->*/
/* */
/*   <!--[if gte mso 9]>*/
/*     <style>*/
/*         /* Target Outlook 2007 and 2010 *//* */
/*     </style>*/
/*   <![endif]-->*/
/*   <table class="main-wrapper" cellpadding="0" cellspacing="0" width="100%" bgcolor="{{ styles.base }}" style="background-color: {{ styles.base }}" dir="{{ language_direction }}">*/
/*     <tr>*/
/*       <td style="padding: 40px 10px 40px 10px;background: #F2F2F2 0% 0% no-repeat padding-box;*/
/* opacity: 1;" >*/
/*         <table class="content-wrapper" cellpadding="0" cellspacing="0" align="center" width="600;" style="background: #FFFFFF 0% 0% no-repeat padding-box;box-shadow: 0px 3px 6px #00000029;border: 1px solid #E6E8EB;opacity: 1;">*/
/*           <tr class="message-header">*/
/*             <td class="ec-header">*/
/*               <table cellpadding="0" cellspacing="0" width="100%">*/
/*                 <tr>*/
/*                   <td><a href="{{ company_data.company_website }}"><img src="http://dwaae.com/images/DWAAE_Logo@2x.png" alt="{{ company_data.company_name }}" width="97px" height="40px" style="width:97px;height:50px;"/></a></td>*/
/* <td style="text-align: right;">*/
/*  {% if title %}<span class="ec-title">{{title}}</span> {% endif %}*/
/* </td>*/
/*                 </tr>*/
/*               </table>*/
/*             </td>*/
/*           </tr>*/
/*          */
/* */
/*           <tr class="message-body">*/
/* */
/* <td>*/
