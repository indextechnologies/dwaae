<?php

/* __string_template__3615128b4bad2d929cb1041a5a7e79074429fe3092edc85275c010ad6c9267e6 */
class __TwigTemplate_81f0b3f0e3039f6873ae9ea29ef0c6425c1a8b6886e1575c17cfe00aa8bd3112 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table style=\"background-color: #ffffff; min-width: 800px; font-family: Helvetica, Arial, sans-serif; border-collapse: separate;\" border=\"0\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"vertical-align: top; height: 73px;\">
<td style=\"height: 73px;\">
<table style=\"border-collapse: separate; float: right;\" border=\"0\" width=\"100%;\" cellspacing=\"0\">
<tbody>
<tr style=\"height: 55px;\">
<td style=\"padding-bottom: 10px; vertical-align: top; height: 55px;\" width=\"60%\">
<p>";
        // line 9
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "is_commercial_quote", array())) {
            echo "<img src=\"https://www.dwaae.com/images/DigiHealthLogo.png?1593670254298\" width=\"175px\" height=\"73px\" />";
        } else {
            echo "<img src=\"https://www.dwaae.com/images/DWAAE_Logo@2x.png\" width=\"173px\" height=\"82px\" />";
        }
        echo "</p>
</td>
<td style=\"height: 55px;\">
<table style=\"float: right; margin-right: 20px;\">
<tbody>
<tr>
<td><span style=\"text-align: right; letter-spacing: 0px; color: #1a516a; font-size: 18px;\">QUOTATION</span></td>
<td>";
        // line 16
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "qr_code_path", array())) {
            echo " <span> <img src=\"";
            echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "qr_code_path", array());
            echo "\" width=\"100px\" height=\"100px\" /> </span>";
        }
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"vertical-align: top;\">
<td>
<table style=\"border-collapse: separate; float: right;\" border=\"0\" width=\"100%;\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"vertical-align: top;\" width=\"60%\">
<p><span style=\"letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;\">From :</span><br /> <span style=\"letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;\">Digital Health - Sole Proprietorship LLC</span><br /> <span style=\"letter-spacing: 0px; color: #262626; font-size: 13px;\">Abu Dhabi, UAE</span><br /><br /> <span style=\"letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;\">Quotation To :</span><br /> <span style=\"font-size: 13px; font-weight: bold; text-transform: uppercase;\">";
        // line 32
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "firstname", array());
        echo " ";
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "lastname", array());
        echo "</span><br />";
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "company", array())) {
            echo "<span style=\"font-size: 13px;\">";
            echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "company", array());
            echo "</span><br />";
        }
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "phone", array())) {
            echo "<span style=\"font-size: 10pt;\">Tel:";
            echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "phone", array());
            echo "</span><br />";
        }
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "email", array())) {
            echo "<span style=\"font-size: 10pt;\">Email:";
            echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "email", array());
            echo "</span>";
        }
        echo "</p>
</td>
<td width=\"40%\">
<table>
<tbody>
<tr>
<td style=\"text-align: right;\"><span style=\"font-size: 14px; color: #262626; font-weight: bold;\">Valid Date From </span></td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\"><span style=\"font-size: 10pt;\">";
        // line 40
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "create_time", array());
        echo "</span></td>
</tr>
<tr>
<td style=\"text-align: right;\"><span style=\"font-size: 14px; color: #262626; font-weight: bold;\">Valid Date To </span></td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\"><span style=\"font-size: 10pt;\">";
        // line 45
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "valid_till", array());
        echo "</span></td>
</tr>
<tr>
<td style=\"text-align: right;\"><span style=\"font-size: 14px; color: #262626; font-weight: bold;\">Quote No </span></td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\"><span style=\"font-size: 10pt;\">";
        // line 50
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "rfq_id", array());
        echo "</span></td>
</tr>
<tr>
<td style=\"text-align: right;\"><span style=\"font-size: 14px; color: #262626; font-weight: bold;\">Enquiry No </span></td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\"><span style=\"font-size: 10pt;\">";
        // line 55
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "enquiry_no", array());
        echo "</span></td>
</tr>
<tr>
<td style=\"text-align: right;\"><span style=\"font-size: 14px; color: #262626; font-weight: bold;\">Payment Terms </span></td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\"><span style=\"font-size: 10pt;\">";
        // line 60
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "payment_terms", array());
        echo "</span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"height: 18px;\">
<td style=\"height: 18px;\">";
        // line 71
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "products_table");
        echo "</td>
</tr>
<tr>
<td style=\"height: 120px;\">
<table style=\"border-collapse: separate;\" width=\"100%\">
<tbody>
<tr>
<td style=\"font-size: 14px; line-height: 21px; color: #444444; padding-right: 30px; vertical-align: top; width: 61.840885142255004%;\"> </td>
<td style=\"vertical-align: top; width: 36.159114857744996%;\">
<table style=\"font-size: 14px; color: #444444;\" width=\"412\">
<tbody>
<tr style=\"background-color: #f7f7f9; height: 2.828125px;\">
<td style=\"padding: 5px; width: 97.34375px; height: 2.828125px; text-align: left; padding-right: 20px;\" align=\"left\"><span>Subtotal</span></td>
<td style=\"padding: 5px; width: 282.671875px; height: 2.828125px; text-align: left;\" align=\"right\">";
        // line 84
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "subtotal_with_shipping", array());
        echo "</td>
</tr>
<tr style=\"background-color: #f7f7f9; height: 20px;\">
<td style=\"padding: 5px; width: 97.34375px; height: 20px; text-align: left; padding-right: 20px;\" align=\"left\"><span>VAT</span></td>
<td style=\"padding: 5px; width: 282.671875px; height: 20px; text-align: left;\" align=\"right\">";
        // line 88
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "total_tax", array());
        echo "</td>
</tr>
<tr style=\"background-color: #eff0f2; font-size: 22px; font-weight: 600; height: 30px;\">
<td style=\"padding: 5px; border-top-color: #e8e8e8; width: 97.34375px; height: 30px; text-align: left; padding-right: 20px;\" align=\"left\">";
        // line 91
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "total");
        echo "</td>
<td style=\"padding: 5px; border-top-color: #e8e8e8; width: 282.671875px; height: 30px; text-align: left;\" align=\"right\">";
        // line 92
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "total", array());
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"height: 200px;\">
<td style=\"padding-top: 5px; height: 200px;\">";
        // line 103
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "quotation_notes", array());
        echo "</td>
</tr>
<tr>
<td style=\"border-top: 3px solid #4777bb;\">
<table style=\"width: 100%; margin-top: 10px;\">
<tbody>
<tr>
<td><span style=\"color: #262626; font-size: 14px; font-weight: bold;\">Digital Health -Sole Proprietorship LLC</span><br /> <span style=\"font-size: 13px; opacity: 0.7;\">Al Falah St, Plot C1, McQueen Building, Mezanine Floor, P.O. Box 26291 – Abu Dhabi, UAE</span><br /> <span style=\"font-size: 13px; opacity: 0.7;\">";
        // line 110
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "is_commercial_quote", array())) {
            echo "Phone: (+971) 2 644 8474 Fax: (+971) 2 666 3824 | Email: info@digitalhealth.ae | www.digitalhealth.ae";
        } else {
            echo "Phone: (+971) 26448474 | Email: care@dwaae.com | www.dwaae.com";
        }
        echo "</span></td>
<td style=\"float: right;\">";
        // line 111
        if (($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "is_commercial_quote", array()) == false)) {
            echo " <img src=\"https://www.dwaae.com/images/DigiHealthLogo.png?1593670254298\" width=\"120px\" height=\"50px\" />";
        }
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>";
    }

    public function getTemplateName()
    {
        return "__string_template__3615128b4bad2d929cb1041a5a7e79074429fe3092edc85275c010ad6c9267e6";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 111,  198 => 110,  188 => 103,  174 => 92,  170 => 91,  164 => 88,  157 => 84,  141 => 71,  127 => 60,  119 => 55,  111 => 50,  103 => 45,  95 => 40,  66 => 32,  43 => 16,  29 => 9,  19 => 1,);
    }
}
/* <table style="background-color: #ffffff; min-width: 800px; font-family: Helvetica, Arial, sans-serif; border-collapse: separate;" border="0" width="100%;" cellspacing="0" cellpadding="0">*/
/* <tbody>*/
/* <tr style="vertical-align: top; height: 73px;">*/
/* <td style="height: 73px;">*/
/* <table style="border-collapse: separate; float: right;" border="0" width="100%;" cellspacing="0">*/
/* <tbody>*/
/* <tr style="height: 55px;">*/
/* <td style="padding-bottom: 10px; vertical-align: top; height: 55px;" width="60%">*/
/* <p>{% if quotation.is_commercial_quote %}<img src="https://www.dwaae.com/images/DigiHealthLogo.png?1593670254298" width="175px" height="73px" />{% else %}<img src="https://www.dwaae.com/images/DWAAE_Logo@2x.png" width="173px" height="82px" />{% endif %}</p>*/
/* </td>*/
/* <td style="height: 55px;">*/
/* <table style="float: right; margin-right: 20px;">*/
/* <tbody>*/
/* <tr>*/
/* <td><span style="text-align: right; letter-spacing: 0px; color: #1a516a; font-size: 18px;">QUOTATION</span></td>*/
/* <td>{% if quotation.qr_code_path %} <span> <img src="{{quotation.qr_code_path}}" width="100px" height="100px" /> </span> {% endif %}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr style="vertical-align: top;">*/
/* <td>*/
/* <table style="border-collapse: separate; float: right;" border="0" width="100%;" cellspacing="0">*/
/* <tbody>*/
/* <tr>*/
/* <td style="vertical-align: top;" width="60%">*/
/* <p><span style="letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;">From :</span><br /> <span style="letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;">Digital Health - Sole Proprietorship LLC</span><br /> <span style="letter-spacing: 0px; color: #262626; font-size: 13px;">Abu Dhabi, UAE</span><br /><br /> <span style="letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;">Quotation To :</span><br /> <span style="font-size: 13px; font-weight: bold; text-transform: uppercase;">{{quotation.firstname }} {{quotation.lastname }}</span><br /> {% if quotation.company %}<span style="font-size: 13px;">{{quotation.company}}</span><br />{% endif %} {% if quotation.phone %}<span style="font-size: 10pt;">Tel: {{quotation.phone}}</span><br />{% endif %} {% if quotation.email%}<span style="font-size: 10pt;">Email: {{quotation.email}}</span>{% endif %}</p>*/
/* </td>*/
/* <td width="40%">*/
/* <table>*/
/* <tbody>*/
/* <tr>*/
/* <td style="text-align: right;"><span style="font-size: 14px; color: #262626; font-weight: bold;">Valid Date From </span></td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;"><span style="font-size: 10pt;">{{quotation.create_time }}</span></td>*/
/* </tr>*/
/* <tr>*/
/* <td style="text-align: right;"><span style="font-size: 14px; color: #262626; font-weight: bold;">Valid Date To </span></td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;"><span style="font-size: 10pt;">{{quotation.valid_till }}</span></td>*/
/* </tr>*/
/* <tr>*/
/* <td style="text-align: right;"><span style="font-size: 14px; color: #262626; font-weight: bold;">Quote No </span></td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;"><span style="font-size: 10pt;">{{quotation.rfq_id }}</span></td>*/
/* </tr>*/
/* <tr>*/
/* <td style="text-align: right;"><span style="font-size: 14px; color: #262626; font-weight: bold;">Enquiry No </span></td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;"><span style="font-size: 10pt;">{{quotation.enquiry_no}}</span></td>*/
/* </tr>*/
/* <tr>*/
/* <td style="text-align: right;"><span style="font-size: 14px; color: #262626; font-weight: bold;">Payment Terms </span></td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;"><span style="font-size: 10pt;">{{quotation.payment_terms }}</span></td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr style="height: 18px;">*/
/* <td style="height: 18px;">{{ snippet("products_table") }}</td>*/
/* </tr>*/
/* <tr>*/
/* <td style="height: 120px;">*/
/* <table style="border-collapse: separate;" width="100%">*/
/* <tbody>*/
/* <tr>*/
/* <td style="font-size: 14px; line-height: 21px; color: #444444; padding-right: 30px; vertical-align: top; width: 61.840885142255004%;"> </td>*/
/* <td style="vertical-align: top; width: 36.159114857744996%;">*/
/* <table style="font-size: 14px; color: #444444;" width="412">*/
/* <tbody>*/
/* <tr style="background-color: #f7f7f9; height: 2.828125px;">*/
/* <td style="padding: 5px; width: 97.34375px; height: 2.828125px; text-align: left; padding-right: 20px;" align="left"><span>Subtotal</span></td>*/
/* <td style="padding: 5px; width: 282.671875px; height: 2.828125px; text-align: left;" align="right">{{quotation.subtotal_with_shipping}}</td>*/
/* </tr>*/
/* <tr style="background-color: #f7f7f9; height: 20px;">*/
/* <td style="padding: 5px; width: 97.34375px; height: 20px; text-align: left; padding-right: 20px;" align="left"><span>VAT</span></td>*/
/* <td style="padding: 5px; width: 282.671875px; height: 20px; text-align: left;" align="right">{{quotation.total_tax}}</td>*/
/* </tr>*/
/* <tr style="background-color: #eff0f2; font-size: 22px; font-weight: 600; height: 30px;">*/
/* <td style="padding: 5px; border-top-color: #e8e8e8; width: 97.34375px; height: 30px; text-align: left; padding-right: 20px;" align="left">{{ __("total") }}</td>*/
/* <td style="padding: 5px; border-top-color: #e8e8e8; width: 282.671875px; height: 30px; text-align: left;" align="right">{{quotation.total}}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr style="height: 200px;">*/
/* <td style="padding-top: 5px; height: 200px;">{{quotation.quotation_notes}}</td>*/
/* </tr>*/
/* <tr>*/
/* <td style="border-top: 3px solid #4777bb;">*/
/* <table style="width: 100%; margin-top: 10px;">*/
/* <tbody>*/
/* <tr>*/
/* <td><span style="color: #262626; font-size: 14px; font-weight: bold;">Digital Health -Sole Proprietorship LLC</span><br /> <span style="font-size: 13px; opacity: 0.7;">Al Falah St, Plot C1, McQueen Building, Mezanine Floor, P.O. Box 26291 – Abu Dhabi, UAE</span><br /> <span style="font-size: 13px; opacity: 0.7;">{% if quotation.is_commercial_quote %}Phone: (+971) 2 644 8474 Fax: (+971) 2 666 3824 | Email: info@digitalhealth.ae | www.digitalhealth.ae{% else %}Phone: (+971) 26448474 | Email: care@dwaae.com | www.dwaae.com{% endif %}</span></td>*/
/* <td style="float: right;">{% if quotation.is_commercial_quote == false %} <img src="https://www.dwaae.com/images/DigiHealthLogo.png?1593670254298" width="120px" height="50px" /> {% endif %}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
