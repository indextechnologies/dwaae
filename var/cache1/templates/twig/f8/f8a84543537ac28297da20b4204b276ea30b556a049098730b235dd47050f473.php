<?php

/* __string_template__71c84ce637ab6ec18c93fffe8d8079262e250e28aebffb0c991823bdd278e3dd */
class __TwigTemplate_b448801f278cac5ee055d58711c717b67d06dfd2d9c9336577b59f8da756e97c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        ob_start();
        // line 2
        echo "                Contact By:";
        echo (isset($context["name"]) ? $context["name"] : null);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 4
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => (isset($context["title"]) ? $context["title"] : null)));
        // line 5
        echo (isset($context["message"]) ? $context["message"] : null);
        // line 6
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__71c84ce637ab6ec18c93fffe8d8079262e250e28aebffb0c991823bdd278e3dd";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 6,  27 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
/*                 {% set title %}*/
/*                 Contact By: {{name}}*/
/*                 {% endset %}*/
/*                 {{ snippet("header", {"title": title }) }}*/
/*                     {{message}}*/
/*                     {{ snippet("footer") }}*/
