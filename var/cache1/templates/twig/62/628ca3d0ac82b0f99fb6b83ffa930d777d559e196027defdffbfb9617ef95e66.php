<?php

/* __string_template__632922cd3b4be9e0881039d630bb5fe50bfb7fa11e1d6c711b625b5adb38a16d */
class __TwigTemplate_fded2ae645a457dd30e4f1ff401c30b51e41bf82b0b4b0e96981e20f2b600e52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"text-align: center;\">
<div class=\"page\" title=\"Page 1\">
<div class=\"section\">
<div class=\"layoutArea\">
<div class=\"column\" style=\"text-align: center;\"> ";
        // line 5
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "total_vat", array());
        echo "</div>
</div>
</div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "__string_template__632922cd3b4be9e0881039d630bb5fe50bfb7fa11e1d6c711b625b5adb38a16d";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 5,  19 => 1,);
    }
}
/* <div style="text-align: center;">*/
/* <div class="page" title="Page 1">*/
/* <div class="section">*/
/* <div class="layoutArea">*/
/* <div class="column" style="text-align: center;"> {{ p.total_vat }}</div>*/
/* </div>*/
/* </div>*/
/* </div>*/
/* </div>*/
