<?php

/* __string_template__4080feab53c0e8fcaf6ae887e207c7e13025eb9a7987b550ee812cda05ed843f */
class __TwigTemplate_8db0a4c43c859352cfe4fc23893ea02f254f8bfa7d9bae2dcd6d8d69e5ee2695 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.prescription_order_assign_to_vendor");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
    }

    public function getTemplateName()
    {
        return "__string_template__4080feab53c0e8fcaf6ae887e207c7e13025eb9a7987b550ee812cda05ed843f";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("d_prescription.prescription_order_assign_to_vendor")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
