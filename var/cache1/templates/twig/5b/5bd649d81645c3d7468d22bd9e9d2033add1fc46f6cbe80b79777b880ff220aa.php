<?php

/* __string_template__7ee6c85a3b521ef8e0c2f30700f48676fefb75aae4bbb4318e41acb11c2d4668 */
class __TwigTemplate_4869483740efad329822315dc3e41957906d050857cc933dc8902fd9505c858f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.prescription_order_accepted_by_vendor");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
    }

    public function getTemplateName()
    {
        return "__string_template__7ee6c85a3b521ef8e0c2f30700f48676fefb75aae4bbb4318e41acb11c2d4668";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("d_prescription.prescription_order_accepted_by_vendor")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
