<?php

/* __string_template__eeace72363d440f6a7ff67450d2b7a2541a225ac3e03490cdcbe26d8b96fe36a */
class __TwigTemplate_3f8cb663d71c67af5fc18c239d781c743c086924ff17cf632ba89703580b63e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center; font-family: Helvetica, Arial, sans-serif;\"><strong style=\"font-weight: 600;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "display_subtotal", array());
        echo "</strong></p>";
    }

    public function getTemplateName()
    {
        return "__string_template__eeace72363d440f6a7ff67450d2b7a2541a225ac3e03490cdcbe26d8b96fe36a";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center; font-family: Helvetica, Arial, sans-serif;"><strong style="font-weight: 600;">{{ p.display_subtotal }}</strong></p>*/
