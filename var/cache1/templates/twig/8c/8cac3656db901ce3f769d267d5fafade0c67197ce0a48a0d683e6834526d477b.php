<?php

/* __string_template__31cdc86011de9f33c85120ae877093a9ffccee6fab7880c1d44ba863122dbbc0 */
class __TwigTemplate_8261d2e8005f25298919322ad7c11ba063459017d250dbdaf06fca9e3895a06b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"text-align: center;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "vat_percentage", array());
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "__string_template__31cdc86011de9f33c85120ae877093a9ffccee6fab7880c1d44ba863122dbbc0";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div style="text-align: center;">{{ p.vat_percentage }}</div>*/
