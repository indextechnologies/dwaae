<?php

/* __string_template__b14dc4c3f84b063c47b3847c12a11bc9a8407878486ad889f523f7c6db59aefd */
class __TwigTemplate_1a88dd397cda47463d4c47cd30dbde114bad017bd68ece72a685fc0fd2ae7bd2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_d_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__b14dc4c3f84b063c47b3847c12a11bc9a8407878486ad889f523f7c6db59aefd";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("change_order_status_d_subj", {"[order]": order_info.order_id}) }}*/
