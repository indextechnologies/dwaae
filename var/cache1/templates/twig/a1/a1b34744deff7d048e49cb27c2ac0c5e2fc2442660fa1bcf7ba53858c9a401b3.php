<?php

/* __string_template__c1fd3ea56744cf5149d6aff463f35a1a3543ff1f4680cde21221770edec1163d */
class __TwigTemplate_16fe7f716fc2daf4b0d210caacae03d878bdae351863b63b209d16a22311cc81 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table style=\"background-color: #ffffff; min-width: 800px; font-family: Helvetica, Arial, sans-serif; border-collapse: separate;\" border=\"0\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"vertical-align: top; height: 73px;\">
<td style=\"height: 73px;\">
<table style=\"border-collapse: separate; float: right;\" border=\"0\" width=\"100%;\" cellspacing=\"0\">
<tbody>
<tr style=\"height: 55px;\">
<td style=\"padding-bottom: 10px; vertical-align: top; height: 55px;\" width=\"60%\">
<p><img src=\"https://www.dwaae.com/images/DWAAE_Logo@2x.png\" width=\"173px\" height=\"82px\" /></p>
</td>
<td style=\"height: 55px;\">
<table style=\"float: right; margin-right: 20px;\">
<tbody>
<tr>
<td><span style=\"text-align: right; letter-spacing: 0px; color: #1a516a; font-size: 18px;\">INVOICE</span></td>
<td>";
        // line 16
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "qr_code_path", array())) {
            echo " <span> <img src=\"";
            echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "qr_code_path", array());
            echo "\" width=\"100px\" height=\"100px\" /> </span>";
        }
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"vertical-align: top;\">
<td>
<table style=\"border-collapse: separate; float: right;\" border=\"0\" width=\"100%;\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"vertical-align: top;\" width=\"60%\">
<p><span style=\"letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;\">From :</span><br /> <span style=\"letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;\">Digital Health - Sole Proprietorship LLC</span><br /> <span style=\"letter-spacing: 0px; color: #262626; font-size: 13px;\">Abu Dhabi, UAE</span><br /><br /> <span style=\"letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;\">Invoice To :</span><br /> <span style=\"font-size: 13px; font-weight: bold; text-transform: uppercase;\">";
        // line 32
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "firstname", array());
        echo " ";
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "lastname", array());
        echo "</span><br />";
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "company", array())) {
            echo "<span style=\"font-size: 13px;\">";
            echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "company", array());
            echo "</span><br />";
        }
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "phone", array())) {
            echo "<span style=\"font-size: 10pt;\">Tel:";
            echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "phone", array());
            echo "</span><br />";
        }
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "email", array())) {
            echo "<span style=\"font-size: 10pt;\">Email:";
            echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "email", array());
            echo "</span>";
        }
        echo "</p>
</td>
<td width=\"40%\">
<table>
<tbody>
<tr>
<td style=\"text-align: right;\">
<div>
<div><strong>Order ID </strong></div>
</div>
</td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\">
<div>
<div><span>#";
        // line 46
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "req_id", array());
        echo "</span></div>
</div>
</td>
</tr>
<tr>
<td style=\"text-align: right;\">
<div>
<div><strong>Invoice Number</strong></div>
</div>
</td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\">
<div>
<div><span>IN-UP-0000";
        // line 59
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "req_id", array());
        echo "</span></div>
</div>
</td>
</tr>
<tr>
<td style=\"text-align: right;\">
<div>
<div><strong>Invoice Date</strong></div>
</div>
</td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\">
<div>
<div><span>";
        // line 72
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "invoice_date", array());
        echo "</span></div>
</div>
</td>
</tr>
<tr>
<td style=\"text-align: right;\">
<div>
<div><strong>Payment Terms</strong></div>
</div>
</td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\">";
        // line 83
        if ($this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "paid_by_card", array())) {
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.paid_by_card");
        } else {
            echo "COD";
        }
        echo "</td>
</tr>
<tr>
<td style=\"text-align: right;\"><span style=\"font-size: 14px; color: #262626; font-weight: bold;\">TRN </span></td>
<td style=\"text-align: right;\">: </td>
<td style=\"text-align: left;\"><span style=\"font-size: 10pt;\">";
        // line 88
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "trn_no", array());
        echo "</span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"height: 18px;\">
<td style=\"height: 18px;\">";
        // line 99
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "products_table");
        echo "</td>
</tr>
<tr>
<td style=\"height: 120px;\">
<table style=\"border-collapse: separate;\" width=\"100%\">
<tbody>
<tr>
<td style=\"font-size: 14px; line-height: 21px; color: #444444; padding-right: 30px; vertical-align: top; width: 61.840885142255004%;\"> </td>
<td style=\"vertical-align: top; width: 36.159114857744996%;\">
<table style=\"font-size: 14px; color: #444444;\" width=\"412\">
<tbody>
<tr style=\"background-color: #f7f7f9; height: 2.828125px;\">
<td style=\"padding: 5px; width: 97.34375px; height: 2.828125px; text-align: left; padding-right: 20px;\" align=\"left\"><span>Subtotal</span></td>
<td style=\"padding: 5px; width: 282.671875px; height: 2.828125px; text-align: left;\" align=\"right\">";
        // line 112
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "subtotal_with_shipping", array());
        echo "</td>
</tr>
<tr style=\"background-color: #f7f7f9; height: 20px;\">
<td style=\"padding: 5px; width: 97.34375px; height: 20px; text-align: left; padding-right: 20px;\" align=\"left\"><span>VAT</span></td>
<td style=\"padding: 5px; width: 282.671875px; height: 20px; text-align: left;\" align=\"right\">";
        // line 116
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "total_tax", array());
        echo "</td>
</tr>
<tr style=\"background-color: #eff0f2; font-size: 22px; font-weight: 600; height: 30px;\">
<td style=\"padding: 5px; border-top-color: #e8e8e8; width: 97.34375px; height: 30px; text-align: left; padding-right: 20px;\" align=\"left\">";
        // line 119
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "total");
        echo "</td>
<td style=\"padding: 5px; border-top-color: #e8e8e8; width: 282.671875px; height: 30px; text-align: left;\" align=\"right\">";
        // line 120
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "total", array());
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"height: 200px;\">
<td style=\"padding-top: 5px; height: 200px;\">";
        // line 131
        echo $this->getAttribute((isset($context["quotation"]) ? $context["quotation"] : null), "quotation_notes", array());
        echo "</td>
</tr>
<tr>
<td style=\"border-top: 3px solid #4777bb;\">
<table style=\"width: 100%; margin-top: 10px;\">
<tbody>
<tr>
<td><span style=\"color: #262626; font-size: 14px; font-weight: bold;\">Digital Health -Sole Proprietorship LLC</span><br /> <span style=\"font-size: 13px; opacity: 0.7;\">Al Falah St, Plot C1, McQueen Building, Mezanine Floor, P.O. Box 26291 – Abu Dhabi, UAE</span><br /> <span style=\"font-size: 13px; opacity: 0.7;\">Phone: (+971) 26448474 | Email: care@dwaae.com | www.dwaae.com</span></td>
<td style=\"float: right;\"><img src=\"https://www.dwaae.com/images/DigiHealthLogo.png?1593670254298\" width=\"120px\" height=\"50px\" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>";
    }

    public function getTemplateName()
    {
        return "__string_template__c1fd3ea56744cf5149d6aff463f35a1a3543ff1f4680cde21221770edec1163d";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 131,  199 => 120,  195 => 119,  189 => 116,  182 => 112,  166 => 99,  152 => 88,  140 => 83,  126 => 72,  110 => 59,  94 => 46,  59 => 32,  36 => 16,  19 => 1,);
    }
}
/* <table style="background-color: #ffffff; min-width: 800px; font-family: Helvetica, Arial, sans-serif; border-collapse: separate;" border="0" width="100%;" cellspacing="0" cellpadding="0">*/
/* <tbody>*/
/* <tr style="vertical-align: top; height: 73px;">*/
/* <td style="height: 73px;">*/
/* <table style="border-collapse: separate; float: right;" border="0" width="100%;" cellspacing="0">*/
/* <tbody>*/
/* <tr style="height: 55px;">*/
/* <td style="padding-bottom: 10px; vertical-align: top; height: 55px;" width="60%">*/
/* <p><img src="https://www.dwaae.com/images/DWAAE_Logo@2x.png" width="173px" height="82px" /></p>*/
/* </td>*/
/* <td style="height: 55px;">*/
/* <table style="float: right; margin-right: 20px;">*/
/* <tbody>*/
/* <tr>*/
/* <td><span style="text-align: right; letter-spacing: 0px; color: #1a516a; font-size: 18px;">INVOICE</span></td>*/
/* <td>{% if quotation.qr_code_path %} <span> <img src="{{quotation.qr_code_path}}" width="100px" height="100px" /> </span> {% endif %}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr style="vertical-align: top;">*/
/* <td>*/
/* <table style="border-collapse: separate; float: right;" border="0" width="100%;" cellspacing="0">*/
/* <tbody>*/
/* <tr>*/
/* <td style="vertical-align: top;" width="60%">*/
/* <p><span style="letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;">From :</span><br /> <span style="letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;">Digital Health - Sole Proprietorship LLC</span><br /> <span style="letter-spacing: 0px; color: #262626; font-size: 13px;">Abu Dhabi, UAE</span><br /><br /> <span style="letter-spacing: 0px; color: #262626; font-size: 14px; font-weight: bold;">Invoice To :</span><br /> <span style="font-size: 13px; font-weight: bold; text-transform: uppercase;">{{quotation.firstname }} {{quotation.lastname }}</span><br /> {% if quotation.company %}<span style="font-size: 13px;">{{quotation.company}}</span><br />{% endif %} {% if quotation.phone %}<span style="font-size: 10pt;">Tel: {{quotation.phone}}</span><br />{% endif %} {% if quotation.email%}<span style="font-size: 10pt;">Email: {{quotation.email}}</span>{% endif %}</p>*/
/* </td>*/
/* <td width="40%">*/
/* <table>*/
/* <tbody>*/
/* <tr>*/
/* <td style="text-align: right;">*/
/* <div>*/
/* <div><strong>Order ID </strong></div>*/
/* </div>*/
/* </td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;">*/
/* <div>*/
/* <div><span>#{{quotation.req_id}}</span></div>*/
/* </div>*/
/* </td>*/
/* </tr>*/
/* <tr>*/
/* <td style="text-align: right;">*/
/* <div>*/
/* <div><strong>Invoice Number</strong></div>*/
/* </div>*/
/* </td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;">*/
/* <div>*/
/* <div><span>IN-UP-0000{{quotation.req_id }}</span></div>*/
/* </div>*/
/* </td>*/
/* </tr>*/
/* <tr>*/
/* <td style="text-align: right;">*/
/* <div>*/
/* <div><strong>Invoice Date</strong></div>*/
/* </div>*/
/* </td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;">*/
/* <div>*/
/* <div><span>{{quotation.invoice_date }}</span></div>*/
/* </div>*/
/* </td>*/
/* </tr>*/
/* <tr>*/
/* <td style="text-align: right;">*/
/* <div>*/
/* <div><strong>Payment Terms</strong></div>*/
/* </div>*/
/* </td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;">{% if quotation.paid_by_card %}{{__("d_prescription.paid_by_card")}}{% else %}COD{% endif %}</td>*/
/* </tr>*/
/* <tr>*/
/* <td style="text-align: right;"><span style="font-size: 14px; color: #262626; font-weight: bold;">TRN </span></td>*/
/* <td style="text-align: right;">: </td>*/
/* <td style="text-align: left;"><span style="font-size: 10pt;">{{quotation.trn_no }}</span></td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr style="height: 18px;">*/
/* <td style="height: 18px;">{{ snippet("products_table") }}</td>*/
/* </tr>*/
/* <tr>*/
/* <td style="height: 120px;">*/
/* <table style="border-collapse: separate;" width="100%">*/
/* <tbody>*/
/* <tr>*/
/* <td style="font-size: 14px; line-height: 21px; color: #444444; padding-right: 30px; vertical-align: top; width: 61.840885142255004%;"> </td>*/
/* <td style="vertical-align: top; width: 36.159114857744996%;">*/
/* <table style="font-size: 14px; color: #444444;" width="412">*/
/* <tbody>*/
/* <tr style="background-color: #f7f7f9; height: 2.828125px;">*/
/* <td style="padding: 5px; width: 97.34375px; height: 2.828125px; text-align: left; padding-right: 20px;" align="left"><span>Subtotal</span></td>*/
/* <td style="padding: 5px; width: 282.671875px; height: 2.828125px; text-align: left;" align="right">{{quotation.subtotal_with_shipping}}</td>*/
/* </tr>*/
/* <tr style="background-color: #f7f7f9; height: 20px;">*/
/* <td style="padding: 5px; width: 97.34375px; height: 20px; text-align: left; padding-right: 20px;" align="left"><span>VAT</span></td>*/
/* <td style="padding: 5px; width: 282.671875px; height: 20px; text-align: left;" align="right">{{quotation.total_tax}}</td>*/
/* </tr>*/
/* <tr style="background-color: #eff0f2; font-size: 22px; font-weight: 600; height: 30px;">*/
/* <td style="padding: 5px; border-top-color: #e8e8e8; width: 97.34375px; height: 30px; text-align: left; padding-right: 20px;" align="left">{{ __("total") }}</td>*/
/* <td style="padding: 5px; border-top-color: #e8e8e8; width: 282.671875px; height: 30px; text-align: left;" align="right">{{quotation.total}}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr style="height: 200px;">*/
/* <td style="padding-top: 5px; height: 200px;">{{quotation.quotation_notes}}</td>*/
/* </tr>*/
/* <tr>*/
/* <td style="border-top: 3px solid #4777bb;">*/
/* <table style="width: 100%; margin-top: 10px;">*/
/* <tbody>*/
/* <tr>*/
/* <td><span style="color: #262626; font-size: 14px; font-weight: bold;">Digital Health -Sole Proprietorship LLC</span><br /> <span style="font-size: 13px; opacity: 0.7;">Al Falah St, Plot C1, McQueen Building, Mezanine Floor, P.O. Box 26291 – Abu Dhabi, UAE</span><br /> <span style="font-size: 13px; opacity: 0.7;">Phone: (+971) 26448474 | Email: care@dwaae.com | www.dwaae.com</span></td>*/
/* <td style="float: right;"><img src="https://www.dwaae.com/images/DigiHealthLogo.png?1593670254298" width="120px" height="50px" /></td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
