<?php

/* __string_template__ea957ee9a6579c2213b50c7cd2e9b9f6411cb65bed5e868355c503e40e94f71f */
class __TwigTemplate_50a05f44750e7362b4d77586e27c3eed3ea0656639d1ad06d6741da3c0b43630 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"text-align: center;\">
<div class=\"page\" title=\"Page 1\">
<div class=\"section\">
<div class=\"layoutArea\">
<div class=\"column\"> ";
        // line 5
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "non_tax_price_total", array());
        echo "</div>
</div>
</div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "__string_template__ea957ee9a6579c2213b50c7cd2e9b9f6411cb65bed5e868355c503e40e94f71f";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 5,  19 => 1,);
    }
}
/* <div style="text-align: center;">*/
/* <div class="page" title="Page 1">*/
/* <div class="section">*/
/* <div class="layoutArea">*/
/* <div class="column"> {{ p.non_tax_price_total }}</div>*/
/* </div>*/
/* </div>*/
/* </div>*/
/* </div>*/
