<?php

/* __string_template__a8eb1398a4a1c3a84971cd6c14001501198254da18af5808ed1e36bb299df003 */
class __TwigTemplate_586bbbd93b1e25608b0cc8bdf6a727a42054f8bf5040463e06d3f24729e7de49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.new_quote_created");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.quote_id");
        echo " #";
        echo $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "rfq_id", array());
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 5
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => (isset($context["title"]) ? $context["title"] : null)));
        echo "
            <p>";
        // line 7
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello");
        echo "<br/>";
        // line 8
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.check_your_quotation_request", array("[link]" => (isset($context["quote_link"]) ? $context["quote_link"] : null)));
        echo "<br/>
            </p>
            <br/>";
        // line 11
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__a8eb1398a4a1c3a84971cd6c14001501198254da18af5808ed1e36bb299df003";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 11,  35 => 8,  32 => 7,  28 => 5,  21 => 3,  19 => 2,);
    }
}
/* */
/*             {% set title %}*/
/*             {{__("h_rfq.new_quote_created")}}: {{__("h_rfq.quote_id")}} #{{quote.rfq_id}}*/
/*             {% endset %}*/
/*             {{ snippet("header", {"title": title }  ) }}*/
/*             <p>*/
/*                 {{__("hello")}}<br/>*/
/*                 {{__("h_rfq.check_your_quotation_request", {"[link]" : quote_link})}}<br/>*/
/*             </p>*/
/*             <br/>*/
/*                   {{ snippet("footer") }}*/
