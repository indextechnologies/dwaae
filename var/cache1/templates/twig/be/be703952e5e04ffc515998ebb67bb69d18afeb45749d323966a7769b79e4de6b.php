<?php

/* __string_template__e77b974819575bdd82daafa60ce6e628bd0155f8105feb4a94543751f90ce83b */
class __TwigTemplate_5a4470975738e92fe4fbf00b8f5c6b79e9b27829a96ae1a33690bf445838c135 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["products_table"]) ? $context["products_table"] : null), "rows", array())) {
            // line 2
            echo "\t\t\t\t\t<table width=\"100%\" cellpadding=\"0\" cellspacing=\"1\"
\t\t\t\t\t    style=\"background-color: #dddddd; -webkit-print-color-adjust: exact;\">
\t\t\t\t\t    <thead>
\t\t\t\t\t        <tr style=\"height: 50px;\">";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["products_table"]) ? $context["products_table"] : null), "headers", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["header"]) {
                // line 8
                echo "\t\t\t\t\t            <th
\t\t\t\t\t                style=\"background-color: #1A516A; color: white; padding: 8px 10px; white-space: nowrap; font-size: 12px; font-family: Arial;\">";
                // line 10
                echo $context["header"];
                echo "</th>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['header'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "\t\t\t\t\t        </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["products_table"]) ? $context["products_table"] : null), "rows", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 16
                echo "\t\t\t\t\t        <tr
\t\t\t\t\t            style=\"height: 50px;padding: 10px; background-color: #ffffff; font-size: 12px; font-family: Arial; border: 0px solid #eeeeee; text-align: center;\">";
                // line 18
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["row"]);
                foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                    // line 19
                    echo "\t\t\t\t\t            <td style=\"border: 0px solid #eeeeee; padding:5px;     padding-top: 2px;     padding-bottom: 2px; text-align:left; \">";
                    // line 20
                    echo $context["column"];
                    echo "
\t\t\t\t\t            </td>";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "\t\t\t\t\t        </tr>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "\t\t\t\t\t    </tbody>
\t\t\t\t\t</table>";
        }
    }

    public function getTemplateName()
    {
        return "__string_template__e77b974819575bdd82daafa60ce6e628bd0155f8105feb4a94543751f90ce83b";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 25,  65 => 23,  57 => 20,  55 => 19,  51 => 18,  48 => 16,  44 => 15,  40 => 12,  33 => 10,  30 => 8,  26 => 7,  21 => 2,  19 => 1,);
    }
}
/* 					{% if products_table.rows %}*/
/* 					<table width="100%" cellpadding="0" cellspacing="1"*/
/* 					    style="background-color: #dddddd; -webkit-print-color-adjust: exact;">*/
/* 					    <thead>*/
/* 					        <tr style="height: 50px;">*/
/* */
/* 					            {% for  header in products_table.headers %}*/
/* 					            <th*/
/* 					                style="background-color: #1A516A; color: white; padding: 8px 10px; white-space: nowrap; font-size: 12px; font-family: Arial;">*/
/* 					                {{ header }}</th>*/
/* 					            {% endfor %}*/
/* 					        </tr>*/
/* 					    </thead>*/
/* 					    <tbody>*/
/* 					        {% for  row in products_table.rows %}*/
/* 					        <tr*/
/* 					            style="height: 50px;padding: 10px; background-color: #ffffff; font-size: 12px; font-family: Arial; border: 0px solid #eeeeee; text-align: center;">*/
/* 					            {% for  column in row %}*/
/* 					            <td style="border: 0px solid #eeeeee; padding:5px;     padding-top: 2px;     padding-bottom: 2px; text-align:left; ">*/
/* 					                {{ column }}*/
/* 					            </td>*/
/* 					            {% endfor %}*/
/* 					        </tr>*/
/* 					        {% endfor %}*/
/* 					    </tbody>*/
/* 					</table>*/
/* 					{% endif %}*/
