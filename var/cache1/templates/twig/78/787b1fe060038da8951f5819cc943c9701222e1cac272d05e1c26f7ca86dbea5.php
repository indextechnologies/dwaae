<?php

/* __string_template__bc40a2f081047b287c5d60d71588f7d37969a9888676e81a602570622dc290fa */
class __TwigTemplate_c84dc3c89995cc7cbabf151a8dd877ea22e8cf0a4e5d83efc595c78290923ec3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.prescription_order_delivered_successfully");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
    }

    public function getTemplateName()
    {
        return "__string_template__bc40a2f081047b287c5d60d71588f7d37969a9888676e81a602570622dc290fa";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("d_prescription.prescription_order_delivered_successfully")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
