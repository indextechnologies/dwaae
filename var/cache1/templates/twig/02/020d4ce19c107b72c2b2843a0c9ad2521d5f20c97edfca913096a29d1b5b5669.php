<?php

/* __string_template__fbe64c844f30e930c776ec7ebf7c7c03cc3ed58ed768b426afd24c7d8c0a7a04 */
class __TwigTemplate_05026dd919437c8c59c879ea19d20ff676a1ec786636065bae04c5f2317020f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.prescription_order_on_the_way");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
    }

    public function getTemplateName()
    {
        return "__string_template__fbe64c844f30e930c776ec7ebf7c7c03cc3ed58ed768b426afd24c7d8c0a7a04";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("d_prescription.prescription_order_on_the_way")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
