<?php

/* __string_template__bc8de63669ca439f5ee903d7924a8246bfa954deb47de9661e0c2581481792ea */
class __TwigTemplate_e042153d6c0f3a288923c6c6de905dc7ca23f09fa8055dbdb1db9901f5c06a7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.request_for_quotation_mail", array("[quote_id]" => $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "rfq_id", array())));
        echo " ";
    }

    public function getTemplateName()
    {
        return "__string_template__bc8de63669ca439f5ee903d7924a8246bfa954deb47de9661e0c2581481792ea";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("h_rfq.request_for_quotation_mail", {"[quote_id]": quote.rfq_id})}} */
