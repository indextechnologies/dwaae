<?php

/* __string_template__42ba7a1fef014240b1decd4c77b9330a172a08d168f153a045c5d3d0019c796c */
class __TwigTemplate_d57b93cf44a7ca7b1c684571a27ee9f35c169f477fe9a47be695df01229ccffb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"text-align: center;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "total_vat", array());
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "__string_template__42ba7a1fef014240b1decd4c77b9330a172a08d168f153a045c5d3d0019c796c";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div style="text-align: center;">{{ p.total_vat }}</div>*/
