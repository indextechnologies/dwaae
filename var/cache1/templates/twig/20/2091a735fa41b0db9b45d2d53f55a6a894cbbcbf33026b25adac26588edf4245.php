<?php

/* __string_template__8305c4441c6a9d8097b21f9d6a438bc58a50ab9152fe6bcc07d296db60fbf83a */
class __TwigTemplate_1717ddac1958f0da3c4d49b0d3a4b6041d77b53c0797b4b8452aa4a001c44989 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table>
<tbody>
<tr>
<td style=\"padding-right: 20px; font-family: Helvetica, Arial, sans-serif;\" rowspan=\"2\">";
        // line 4
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "image", array());
        echo "</td>
<td style=\"vertical-align: middle; text-align: left;\"><span style=\"font-family: Helvetica, Arial, sans-serif; text-transfrom: uppercase;\"><strong style=\"font-weight: 600;\"><a href=\"";
        // line 5
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "product_url", array());
        echo "\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "name", array());
        echo "</a></strong></span></td>
</tr>
<tr>
<td style=\"vertical-align: top; text-align: left; font-family: Helvetica, Arial, sans-serif;\"><span style=\"font-size: 11px; font-weight: 400; font-family: Helvetica, Arial, sans-serif; color: #a8a8a8;\">";
        // line 8
        if ($this->getAttribute((isset($context["p"]) ? $context["p"] : null), "product_code", array())) {
            echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "product_code", array());
            echo "<br />";
        }
        if ($this->getAttribute((isset($context["p"]) ? $context["p"] : null), "options", array())) {
            echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "options", array());
        }
        echo "</span></td>
</tr>
</tbody>
</table>";
    }

    public function getTemplateName()
    {
        return "__string_template__8305c4441c6a9d8097b21f9d6a438bc58a50ab9152fe6bcc07d296db60fbf83a";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 8,  28 => 5,  24 => 4,  19 => 1,);
    }
}
/* <table>*/
/* <tbody>*/
/* <tr>*/
/* <td style="padding-right: 20px; font-family: Helvetica, Arial, sans-serif;" rowspan="2">{{ p.image }}</td>*/
/* <td style="vertical-align: middle; text-align: left;"><span style="font-family: Helvetica, Arial, sans-serif; text-transfrom: uppercase;"><strong style="font-weight: 600;"><a href="{{ p.product_url }}">{{ p.name }}</a></strong></span></td>*/
/* </tr>*/
/* <tr>*/
/* <td style="vertical-align: top; text-align: left; font-family: Helvetica, Arial, sans-serif;"><span style="font-size: 11px; font-weight: 400; font-family: Helvetica, Arial, sans-serif; color: #a8a8a8;">{% if p.product_code %}{{ p.product_code }}<br /> {% endif %}{% if p.options %}{{ p.options }}{% endif %}</span></td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
