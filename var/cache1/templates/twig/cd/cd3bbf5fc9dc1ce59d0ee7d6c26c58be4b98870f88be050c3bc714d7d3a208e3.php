<?php

/* __string_template__b3866265f2d21773dc189cedacb9274c2f41b4053c5aff18aa4c99d92b097678 */
class __TwigTemplate_16171d46aa63ed4d8a56ee85d4d83cf3e2bb6466c80ad79d18f9c678b50ea7d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_i_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__b3866265f2d21773dc189cedacb9274c2f41b4053c5aff18aa4c99d92b097678";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("change_order_status_i_subj", {"[order]": order_info.order_id}) }}*/
