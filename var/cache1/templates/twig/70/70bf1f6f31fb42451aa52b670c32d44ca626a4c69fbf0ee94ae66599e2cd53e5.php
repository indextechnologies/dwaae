<?php

/* __string_template__67ec34869f3adcff4f387bdb2287fea15624672e59570e0052016ca89ff6547d */
class __TwigTemplate_3a68998ed1aa36b821e6ea99dd07465302ac6889f49654798e745b341f288dee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "recover_password_subj")));
        // line 2
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "text_confirm_passwd_recovery");
        echo ":<br /><br />
        <a href=\"";
        // line 3
        echo (isset($context["url"]) ? $context["url"] : null);
        echo "\">";
        echo $this->env->getExtension('tygh.core')->punyDecodeFilter((isset($context["url"]) ? $context["url"] : null));
        echo "</a>";
        // line 4
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
        echo "
";
    }

    public function getTemplateName()
    {
        return "__string_template__67ec34869f3adcff4f387bdb2287fea15624672e59570e0052016ca89ff6547d";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 3,  21 => 2,  19 => 1,);
    }
}
/* {{ snippet("header", { "title" : __("recover_password_subj") }) }}*/
/*         {{ __("text_confirm_passwd_recovery") }}:<br /><br />*/
/*         <a href="{{ url }}">{{ url|puny_decode }}</a>*/
/*     {{ snippet("footer") }}*/
/* */
