<?php

/* __string_template__0066ff718e12306f8ee0f03e3b4e374f7ee3ef3471029e7c3db6bdde6ba107fa */
class __TwigTemplate_95f58d210f49dd2a81d305f54a64fc602ec44fb428c69aa56d2f0f03910f1ff3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_default_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array()), "[status]" => $this->getAttribute((isset($context["order_status"]) ? $context["order_status"] : null), "description", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__0066ff718e12306f8ee0f03e3b4e374f7ee3ef3471029e7c3db6bdde6ba107fa";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("change_order_status_default_subj", {"[order]": order_info.order_id, "[status]": order_status.description}) }}*/
