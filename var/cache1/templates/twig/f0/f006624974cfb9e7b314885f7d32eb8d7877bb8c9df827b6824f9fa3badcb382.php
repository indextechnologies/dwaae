<?php

/* __string_template__f84768c16adffcb1acb46880d7bc849ce3f805925777ca7121cdc963b40104c4 */
class __TwigTemplate_c3d03ab6f4e82a5ee0c2cc3b99adec24345ffcecbcc5b318cb1e9ac7ce65331d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "quantity", array());
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "__string_template__f84768c16adffcb1acb46880d7bc849ce3f805925777ca7121cdc963b40104c4";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center;">{{ p.quantity }}</p>*/
