<?php

/* __string_template__1883043ecb7e1b6d29d580c1f58111932791e1d708ebb598a7edfffa01d53356 */
class __TwigTemplate_e8c0c089d05871ff8a577f5e6eef929490fae83d7dd8d22d9aae42c6b4d14c9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center; font-family: Helvetica, Arial, sans-serif;\"><strong style=\"font-weight: 600;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "display_subtotal", array());
        echo "</strong></p>";
    }

    public function getTemplateName()
    {
        return "__string_template__1883043ecb7e1b6d29d580c1f58111932791e1d708ebb598a7edfffa01d53356";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center; font-family: Helvetica, Arial, sans-serif;"><strong style="font-weight: 600;">{{ p.display_subtotal }}</strong></p>*/
