<?php

/* __string_template__5dfd5f3db839dae04b941ac5535d4ce89bb41b020d64cd50944127de0fb91360 */
class __TwigTemplate_e2df06fe32a34e0f6fff483a706888160a88b62e48172cb187831bba53a1ba9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center; font-family: Helvetica, Arial, sans-serif;\"><strong style=\"font-weight: 600;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "amount", array());
        echo "</strong></p>";
    }

    public function getTemplateName()
    {
        return "__string_template__5dfd5f3db839dae04b941ac5535d4ce89bb41b020d64cd50944127de0fb91360";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center; font-family: Helvetica, Arial, sans-serif;"><strong style="font-weight: 600;">{{ p.amount }}</strong></p>*/
