<?php

/* __string_template__988a27ac65915d807bfbf19fdb36d1dee0bfb187d75d2f5f1058e26c8bc897b0 */
class __TwigTemplate_3c94ac1127f0d6f456810cf1fdbe3114a7b42cccad18d9e9bf87bff6f3ec6c1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.new_order_created");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
    }

    public function getTemplateName()
    {
        return "__string_template__988a27ac65915d807bfbf19fdb36d1dee0bfb187d75d2f5f1058e26c8bc897b0";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("d_prescription.new_order_created")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
