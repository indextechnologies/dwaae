<?php

/* __string_template__661ff0a6ea84b669951c6248778eafa774917b6a05b282acaafecebde75d5435 */
class __TwigTemplate_81858b110dd21b17ddc22a5a47b3255e7ef29f903896c376ee152da18851434c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "non_tax_price", array());
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "__string_template__661ff0a6ea84b669951c6248778eafa774917b6a05b282acaafecebde75d5435";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center;">{{ p.non_tax_price }}</p>*/
