<?php

/* __string_template__7a4c236f58f9fad2fa2afa0a946a4848c6c8c50107a55e7305660676c02f8765 */
class __TwigTemplate_42727251a51d88af74df6e9acaf00dde48f829a21c5341c717983e4c890ae0b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.new_order_created");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 5
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => (isset($context["title"]) ? $context["title"] : null)));
        echo "
            <p>";
        // line 7
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello");
        echo "<br/>";
        // line 8
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.customer_created_new_prescription", array("[req_id]" => $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array())));
        echo "<br/>";
        // line 9
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.view_your_request", array("[link]" => (isset($context["link"]) ? $context["link"] : null)));
        echo "<br/>
            </p><br/>";
        // line 11
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__7a4c236f58f9fad2fa2afa0a946a4848c6c8c50107a55e7305660676c02f8765";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 11,  38 => 9,  35 => 8,  32 => 7,  28 => 5,  21 => 3,  19 => 2,);
    }
}
/* */
/*             {% set title %}*/
/*             {{__("d_prescription.new_order_created")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
/*             {% endset %}*/
/*             {{ snippet("header", {"title": title }) }}*/
/*             <p>*/
/*                 {{__("hello")}}<br/>*/
/*                 {{__("d_prescription.customer_created_new_prescription", {"[req_id]" : prescription.req_id})}}<br/>*/
/*                 {{__("d_prescription.view_your_request", {"[link]" : link})}}<br/>*/
/*             </p><br/>*/
/*                   {{ snippet("footer") }}*/
