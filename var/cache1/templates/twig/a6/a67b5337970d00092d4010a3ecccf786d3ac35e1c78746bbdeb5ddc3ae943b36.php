<?php

/* __string_template__f890a745bef892cd7f064fdada9bd44acf38aed9a778d69b8ed2381608e8de04 */
class __TwigTemplate_cd564aa1a3c47ceacddf9405f8d87d60dfc2025daef2e0c17736eb642dfb2b9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div style=\"text-align: center;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "non_tax_price_total", array());
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "__string_template__f890a745bef892cd7f064fdada9bd44acf38aed9a778d69b8ed2381608e8de04";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div style="text-align: center;">{{ p.non_tax_price_total }}</div>*/
