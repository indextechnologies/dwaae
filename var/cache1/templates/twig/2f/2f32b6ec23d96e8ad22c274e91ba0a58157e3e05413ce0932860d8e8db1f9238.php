<?php

/* __string_template__c41910726e0362cf8e64e5c469fb10119a49ff474493b93cc32c810f32928a62 */
class __TwigTemplate_f2c2d863b5aa21a29c0ac95cd62f6e038232f08296eeadf313dd68568852e863 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "non_copayment_price", array());
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "__string_template__c41910726e0362cf8e64e5c469fb10119a49ff474493b93cc32c810f32928a62";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center;">{{p.non_copayment_price}}</p>*/
