<?php

/* __string_template__c108a3b12fa3f5ba7bbe75c181dd44ec152b9944387456161789a070b0166f94 */
class __TwigTemplate_ea285f304eb5221b6e985a47a799105501e0c9b202f093dff2b3660b044b6407 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_c_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__c108a3b12fa3f5ba7bbe75c181dd44ec152b9944387456161789a070b0166f94";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("change_order_status_c_subj", {"[order]": order_info.order_id}) }}*/
