<?php

/* __string_template__43213af8ea828ddb852c01408cbf017adfbe8ec426edadae68045cdcfd1ae6f8 */
class __TwigTemplate_e3785da0edd9eb98e3a1911a8557231e9a48790de653fbfac472b3ccaaab72e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Contact By:";
        echo (isset($context["name"]) ? $context["name"] : null);
    }

    public function getTemplateName()
    {
        return "__string_template__43213af8ea828ddb852c01408cbf017adfbe8ec426edadae68045cdcfd1ae6f8";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* Contact By: {{name}}*/
