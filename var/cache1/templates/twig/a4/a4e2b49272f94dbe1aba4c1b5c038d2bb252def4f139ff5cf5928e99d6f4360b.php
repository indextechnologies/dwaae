<?php

/* __string_template__5f7e22a31823bd81c3d1a3b25b119f90a94ab8ba996a7b881870d9b5f1f5bf71 */
class __TwigTemplate_834f325afa00572a503e193969e5dc4ca5ea6750677dd69e5ef378e1c5017703 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.request_for_quotation_mail", array("[quote_id]" => $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "rfq_id", array())));
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 5
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => (isset($context["title"]) ? $context["title"] : null)));
        echo "
            <p>";
        // line 7
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello_name", array("[name]" => $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "firstname", array())));
        echo "<br/>";
        // line 8
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.change_quotation_status_default_text", array("[status]" => (isset($context["status_description"]) ? $context["status_description"] : null)));
        echo "<br/>";
        // line 9
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.check_your_quotation_request", array("[link]" => (isset($context["quote_link"]) ? $context["quote_link"] : null)));
        echo "<br/>
            </p>";
        // line 11
        if ((isset($context["send_attachment_quote"]) ? $context["send_attachment_quote"] : null)) {
            // line 12
            echo "            <br />";
            // line 13
            echo $this->env->getExtension('tygh.core')->includeDocFunction($this->env, $context, "quotation.default", $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "rfq_id", array()));
            echo "<br/>";
        }
        // line 15
        if ((isset($context["send_attachment_invoice"]) ? $context["send_attachment_invoice"] : null)) {
            // line 16
            echo "            <br />";
            // line 17
            echo $this->env->getExtension('tygh.core')->includeDocFunction($this->env, $context, "quotation.invoice", $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "rfq_id", array()));
        }
        // line 19
        echo "            <br/>";
        // line 20
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__5f7e22a31823bd81c3d1a3b25b119f90a94ab8ba996a7b881870d9b5f1f5bf71";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 20,  53 => 19,  50 => 17,  48 => 16,  46 => 15,  42 => 13,  40 => 12,  38 => 11,  34 => 9,  31 => 8,  28 => 7,  24 => 5,  21 => 3,  19 => 2,);
    }
}
/* */
/*             {% set title %}*/
/*             {{__("h_rfq.request_for_quotation_mail", {"[quote_id]": quote.rfq_id})}}*/
/*             {% endset %}*/
/*             {{ snippet("header", {"title": title }  ) }}*/
/*             <p>*/
/*                 {{__("hello_name", {"[name]" : quote.firstname })}}<br/>                */
/*                 {{ __("h_rfq.change_quotation_status_default_text", {"[status]": status_description}) }}<br/>*/
/*                 {{__("h_rfq.check_your_quotation_request", {"[link]" : quote_link})}}<br/>*/
/*             </p>*/
/*             {% if send_attachment_quote %}*/
/*             <br /> */
/*             {{ include_doc("quotation.default", quote.rfq_id) }}<br/>*/
/*             {% endif %}*/
/*             {% if send_attachment_invoice %}*/
/*             <br /> */
/*             {{ include_doc("quotation.invoice", quote.rfq_id) }}*/
/*             {% endif %}*/
/*             <br/>*/
/*                   {{ snippet("footer") }}*/
