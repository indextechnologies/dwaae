<?php /* Smarty version Smarty-3.1.21, created on 2021-05-24 15:47:58
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/new_post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:209092518960ab926ebc5127-56682712%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '99faab7d63df31aa54557c39463c75173a0bb1a6' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/new_post.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '209092518960ab926ebc5127-56682712',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'discussion' => 0,
    'quotation' => 0,
    'config' => 0,
    'auth' => 0,
    'user_info' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60ab926ebd2c62_67495029',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60ab926ebd2c62_67495029')) {function content_60ab926ebd2c62_67495029($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('general','name','your_message','add','new_post'));
?>
<?php if (true) {?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("add_new_picker", null, null); ob_start(); ?>
        <div class="tabs cm-j-tabs">
            <ul class="nav nav-tabs">
                <li id="tab_add_post" class="cm-js active"><a><?php echo $_smarty_tpl->__("general");?>
</a></li>
            </ul>
        </div>
        <form id='form' action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="form-horizontal form-edit cm-disable-empty-files" enctype="multipart/form-data">

            <div class="cm-tabs-content cm-no-hide-input" id="content_tab_add_post">
                <input type ="hidden" name="post_data[thread_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['discussion']->value['thread_id'], ENT_QUOTES, 'UTF-8');?>
" />
                <input type ="hidden" name="post_data[rfq_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" />
                <input type ="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
&amp;selected_section=discussion" />

                <div class="control-group">
                    <label for="post_data_name" class="cm-required control-label"><?php echo $_smarty_tpl->__("name");?>
:</label>
                    <div class="controls">
                        <input type="text" name="post_data[name]" id="post_data_name" value="<?php if ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['lastname'], ENT_QUOTES, 'UTF-8');
}?>" disabled="disabled">
                    </div>
                </div>

                <div class="control-group">
                    <label for="message" class="control-label"><?php echo $_smarty_tpl->__("your_message");?>
:</label>
                    <div class="controls">
                        <textarea name="post_data[message]" id="message" class="input-textarea-long" cols="70" rows="8" ></textarea>
                    </div>
                </div>
            </div>

            <div class="buttons-container">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("add"),'but_name'=>"dispatch[h_rfq_discussion.add]",'cancel_action'=>"close",'hide_first_button'=>false), 0);?>

            </div>

        </form>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

    <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"add_new_post",'text'=>$_smarty_tpl->__("new_post"),'content'=>Smarty::$_smarty_vars['capture']['add_new_picker'],'act'=>"fake"), 0);?>

<?php }?>
<?php }} ?>
