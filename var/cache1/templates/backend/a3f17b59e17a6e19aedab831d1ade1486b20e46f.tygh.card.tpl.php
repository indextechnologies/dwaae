<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 15:13:40
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_customer/views/emirate_cards/components/card.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1968060116093cf6467d2c3-54683131%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a3f17b59e17a6e19aedab831d1ade1486b20e46f' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_customer/views/emirate_cards/components/card.tpl',
      1 => 1602500916,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1968060116093cf6467d2c3-54683131',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'card_view_only' => 0,
    'card' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6093cf64685174_07425693',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6093cf64685174_07425693')) {function content_6093cf64685174_07425693($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
<?php $_smarty_tpl->tpl_vars['view_only'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['card_view_only']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>

<div style="border: 1px solid gray; padding-bottom: 10px;">
    <header class="w3-container w3-blue">
        <h4 style="text-align: center;">Emirate Card</h4>
    </header>
    <div class="w3-container ec-download d-emirate_card" data-emirate-profile-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['card']->value['emirate_profile_id'], ENT_QUOTES, 'UTF-8');?>
" style="display: flex; justify-content: space-around;">
        <div class="download">
            <p>
                <a download href="<?php echo htmlspecialchars(fn_url("d_customer.download_file&file=".((string)$_smarty_tpl->tpl_vars['card']->value['front_side_path'])), ENT_QUOTES, 'UTF-8');?>
">Download Front Side</a>
            </p>
             <p class="download__instructions">
                <small>Typically takes 5 seconds to 15 seconds.</small>
            </p>
        </div>
        <div class="download">
            <p>
                <a download href="<?php echo htmlspecialchars(fn_url("d_customer.download_file&file=".((string)$_smarty_tpl->tpl_vars['card']->value['back_side_path'])), ENT_QUOTES, 'UTF-8');?>
">Download Back Side</a>
            </p>
             <p class="download__instructions">
                <small>Typically takes 5 seconds to 15 seconds.</small>
            </p>
        </div>
    </div>
</div><?php }} ?>
