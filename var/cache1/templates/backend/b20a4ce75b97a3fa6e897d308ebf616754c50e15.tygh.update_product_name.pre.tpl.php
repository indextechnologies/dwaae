<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 20:41:04
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/product_variations/hooks/products/update_product_name.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2991430396092caa00db738-02361993%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b20a4ce75b97a3fa6e897d308ebf616754c50e15' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/product_variations/hooks/products/update_product_name.pre.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '2991430396092caa00db738-02361993',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'parent_product_data' => 0,
    'product_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092caa00df366_97522146',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092caa00df366_97522146')) {function content_6092caa00df366_97522146($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('product_variations.variation_of_product'));
?>
<?php if ($_smarty_tpl->tpl_vars['parent_product_data']->value) {?>
    <div class="control-group">
        <div class="controls">
            <p>
                <?php echo $_smarty_tpl->__("product_variations.variation_of_product",array("[url]"=>fn_url("products.update?product_id=".((string)$_smarty_tpl->tpl_vars['product_data']->value['parent_product_id'])),"[product]"=>$_smarty_tpl->tpl_vars['parent_product_data']->value['variation_name']));?>

            </p>
        </div>
    </div>
<?php }?><?php }} ?>
