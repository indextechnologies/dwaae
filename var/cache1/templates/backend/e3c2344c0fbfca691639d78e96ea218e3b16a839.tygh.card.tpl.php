<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 15:46:53
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_customer/views/insurance_cards/components/card.tpl" */ ?>
<?php /*%%SmartyHeaderCode:140286610560967a2d695e98-64903738%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3c2344c0fbfca691639d78e96ea218e3b16a839' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_customer/views/insurance_cards/components/card.tpl',
      1 => 1602501043,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '140286610560967a2d695e98-64903738',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'card_view_only' => 0,
    'insurance_type' => 0,
    'card' => 0,
    'prescription' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60967a2d6e66d9_62629764',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60967a2d6e66d9_62629764')) {function content_60967a2d6e66d9_62629764($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_prescription.emirate_id_is_insurance','d_prescription.no_insurance'));
?>
<?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
<?php $_smarty_tpl->tpl_vars['view_only'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['card_view_only']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['insurance_type'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['insurance_type']->value)===null||$tmp==='' ? "S" : $tmp), null, 0);?>
<div  style="border: 1px solid gray; padding-bottom: 10px;">
    <header class="w3-container w3-blue">
        <h4 style="text-align: center;">Insurance Card</h4>
    </header>
    <div class="w3-container ec-download d-insurance_card " data-insurance-profile-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['card']->value['insurance_profile_id'], ENT_QUOTES, 'UTF-8');?>
" style="display: flex; justify-content: space-around;">
        <?php if ($_smarty_tpl->tpl_vars['insurance_type']->value=="S") {?>
             <div class="download">
                <p>
                    <a download href="<?php echo htmlspecialchars(fn_url("d_customer.download_file&file=".((string)$_smarty_tpl->tpl_vars['card']->value['front_side_path'])), ENT_QUOTES, 'UTF-8');?>
">Download Front Side</a>
                </p>
                <p class="download__instructions">
                    <small>Typically takes 5 seconds to 15 seconds.</small>
                </p>
            </div>
            <div class="download">
                <p>
                    <a download href="<?php echo htmlspecialchars(fn_url("d_customer.download_file&file=".((string)$_smarty_tpl->tpl_vars['card']->value['back_side_path'])), ENT_QUOTES, 'UTF-8');?>
">Download Back Side</a>
                </p>
                <p class="download__instructions">
                    <small>Typically takes 5 seconds to 15 seconds.</small>
                </p>
            </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['insurance_type']->value=="E") {?>
            <?php echo $_smarty_tpl->__("d_prescription.emirate_id_is_insurance");?>

        <?php } elseif ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='N') {?>
            <?php echo $_smarty_tpl->__("d_prescription.no_insurance");?>

        <?php }?>
    </div>
</div>
<?php }} ?>
