<?php /* Smarty version Smarty-3.1.21, created on 2021-05-24 15:47:58
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:96166089360ab926eaaae66-02288879%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a88d83403134bdbcf9552774b18b5f7fef704f8e' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/update.tpl',
      1 => 1602600734,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '96166089360ab926eaaae66-02288879',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'quotation' => 0,
    'settings' => 0,
    'is_form_readonly' => 0,
    'id' => 0,
    'config' => 0,
    'sidebar_icon' => 0,
    'rfq_id' => 0,
    'notify_customer_status' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60ab926eb1c511_01741704',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60ab926eb1c511_01741704')) {function content_60ab926eb1c511_01741704($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_block_inline_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('general','h_rfq.rfq_id','h_rfq.jfn','h_rfq.enquiry_no','h_rfq.invoice_no','h_rfq.valid_till','h_rfq.invoice_date','h_rfq.lpo_date','h_rfq.firstname','h_rfq.lastname','h_rfq.email','h_rfq.phone','h_rfq.fax','h_rfq.company','h_rfq.url','h_rfq.quotation_detail_header','h_rfq.new_quote','h_rfq.print_quote','h_rfq.print_quote_pdf','h_rfq.print_invoice','h_rfq.print_invoice_pdf','notify_customer'));
?>
<?php $_smarty_tpl->tpl_vars['is_form_readonly'] = new Smarty_variable(false, null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['quotation']->value['rfq_id']) {?>
    <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable(0, null, 0);?>
<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php $_smarty_tpl->_capture_stack[0][] = array("sidebar", null, null); ob_start(); ?>

    <div class="sidebar-row">
        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("general"),'target'=>"#acc_general",'meta'=>"h-rfq_sidebar_header"), 0);?>

        <div id="acc_general" class="h_rfq-sidebar collapse in" >
            <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.rfq_id");?>
:</span><input type="text"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input input-small" disabled/></p>
            <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.jfn");?>
:</span> <input type="text" name="quotation[jfn]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['jfn'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input input-small" /></p>
            <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.enquiry_no");?>
:</span> <input type="text" name="quotation[enquiry_no]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['enquiry_no'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input input-small" /></p>
            <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.invoice_no");?>
:</span> <input type="text" name="quotation[invoice_no]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['invoice_no'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input input-small" /></p>
            <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.valid_till");?>
:</span> 
                <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_valid_till",'date_name'=>"quotation[valid_till]",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['valid_till'])===null||$tmp==='' ? @constant('TIME') : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year'],'show_time'=>false,'date_meta'=>"h-rfq_sidebar_calendar",'disabled'=>true), 0);?>

            </p>
            <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.invoice_date");?>
:</span> 
                <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_invoice_date",'date_name'=>"quotation[invoice_date]",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['invoice_date'])===null||$tmp==='' ? @constant('TIME') : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year'],'show_time'=>false,'date_meta'=>"h-rfq_sidebar_calendar",'disabled'=>true), 0);?>

            </p>
            <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.lpo_date");?>
:</span> 
                <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_lpo_date",'date_name'=>"quotation[lpo_date]",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['lpo_date'])===null||$tmp==='' ? @constant('TIME') : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year'],'show_time'=>false,'date_meta'=>"h-rfq_sidebar_calendar",'disabled'=>true), 0);?>

            </p>
        </div>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/profile_info.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('quotation'=>$_smarty_tpl->tpl_vars['quotation']->value,'form_id'=>"quotation_form"), 0);?>


<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("customer_information", null, null); ob_start(); ?>
    <div class="control-group"><label class="control-label" for="elm_buyer_firstname"><?php echo $_smarty_tpl->__("h_rfq.firstname");?>
:</label>
        <div class="controls">
            <input type="text" name="quotation[buyer_details][firstname]" id="elm_buyer_firstname" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input"  />
        </div>
    </div>
    <div class="control-group"><label class="control-label" for="elm_buyer_lastname"><?php echo $_smarty_tpl->__("h_rfq.lastname");?>
:</label>
        <div class="controls">
            <input type="text" name="quotation[buyer_details][lastname]" id="elm_buyer_lastname" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input"  />
        </div>
    </div>
    <div class="control-group"><label class="control-label" for="elm_buyer_email"><?php echo $_smarty_tpl->__("h_rfq.email");?>
:</label>
        <div class="controls">
            <input type="text" name="quotation[buyer_details][email]" id="elm_buyer_email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['email'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input"  />
        </div>
    </div>
    <div class="control-group"><label class="control-label" for="elm_buyer_phone"><?php echo $_smarty_tpl->__("h_rfq.phone");?>
:</label>
        <div class="controls">
            <input type="text" name="quotation[buyer_details][phone]" id="elm_buyer_phone" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['phone'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input"  />
        </div>
    </div>
    <div class="control-group"><label class="control-label" for="elm_buyer_fax"><?php echo $_smarty_tpl->__("h_rfq.fax");?>
:</label>
        <div class="controls">
            <input type="text" name="quotation[buyer_details][fax]" id="elm_buyer_fax" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['fax'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input"  />
        </div>
    </div>
    <div class="control-group"><label class="control-label" for="elm_buyer_company"><?php echo $_smarty_tpl->__("h_rfq.company");?>
:</label>
        <div class="controls">
            <input type="text" name="quotation[buyer_details][company]" id="elm_buyer_company" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['company'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input"  />
        </div>
    </div>
    <div class="control-group"><label class="control-label" for="elm_buyer_url"><?php echo $_smarty_tpl->__("h_rfq.url");?>
:</label>
        <div class="controls">
            <input type="text" name="quotation[buyer_details][url]" id="elm_buyer_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['url'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_sidebar_input"  />
        </div>
    </div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("tabsbox", null, null); ob_start(); ?>
        <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="quotation_form" id="quotation_form" class="form-horizontal form-edit order-info-form  <?php if ($_smarty_tpl->tpl_vars['is_form_readonly']->value) {?>cm-hide-inputs<?php }?>" >
            <input type="hidden" name="rfq_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="status" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['status'], ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="result_ids" value="content_general" />
            <input type="hidden" name="selected_section" value="<?php echo htmlspecialchars($_REQUEST['selected_section'], ENT_QUOTES, 'UTF-8');?>
" />

            <div class="h-rfq_container">
                <div class="sidebar sidebar-left" id="elm_sidebar">
                    <div class="sidebar-toggle"><i class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sidebar_icon']->value, ENT_QUOTES, 'UTF-8');?>
 sidebar-icon"></i></div>
                    <div class="sidebar-wrapper">
                        <?php echo Smarty::$_smarty_vars['capture']['sidebar'];?>

                    </div>
                <!--elm_sidebar--></div>

                <div class="h-rfq_content_block">
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/general.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                </div>
                
            </div>

        </form>

        <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
            <div class="h-rfq_content_block">
            <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/discussion.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('user_id'=>$_smarty_tpl->tpl_vars['quotation']->value['user_id'],'object_company_id'=>0), 0);?>

            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
            <div class="h-rfq_content_block">
                <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/attachment.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
            <div class="h-rfq_content_block">
                <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/payment_details.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
            <div class="h-rfq_content_block">
                <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/logs.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            </div>
        <?php }?>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/tabsbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['tabsbox'],'active_tab'=>$_REQUEST['selected_section'],'track'=>true), 0);?>


<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
    <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
        <?php echo $_smarty_tpl->__("h_rfq.quotation_detail_header",array("[id]"=>((string)$_smarty_tpl->tpl_vars['rfq_id']->value)));?>
 
        <span class="f-small">
        <?php $_smarty_tpl->tpl_vars["timestamp"] = new Smarty_variable(rawurlencode(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']))), null, 0);?>
        / <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>
,<?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['quotation']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>

        </span>
    <?php } else { ?>
        <?php echo $_smarty_tpl->__("h_rfq.new_quote");?>

    <?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>



<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/view_tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('url'=>"h_rfq.details&rfq_id="), 0);?>


    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("h_rfq.print_quote"),'href'=>"h_rfq_invoice.print_invoice?rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id']),'class'=>"cm-new-window"));?>
</li>
        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("h_rfq.print_quote_pdf"),'href'=>"h_rfq_invoice.print_invoice?rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&format=pdf",'class'=>''));?>
</li>
        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("h_rfq.print_invoice"),'href'=>"h_rfq_invoice.print_final_invoice?rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id']),'class'=>"cm-new-window"));?>
</li>
        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("h_rfq.print_invoice_pdf"),'href'=>"h_rfq_invoice.print_final_invoice?rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&format=pdf",'class'=>''));?>
</li>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>


    <div class="btn-group btn-hover dropleft">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_changes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_meta'=>"cm-no-ajax dropdown-toggle",'but_role'=>"submit-link",'but_target_form'=>"quotation_form",'but_name'=>"dispatch[h_rfq.update_details]",'save'=>true), 0);?>

        <ul class="dropdown-menu">
            <?php $_smarty_tpl->tpl_vars['notify_customer_status'] = new Smarty_variable(false, null, 0);?>
            <?php $_smarty_tpl->tpl_vars['notify_department_status'] = new Smarty_variable(false, null, 0);?>
            <?php $_smarty_tpl->tpl_vars['notify_vendor_status'] = new Smarty_variable(false, null, 0);?>

        <li><a><input type="checkbox" name="notify_user" <?php if ($_smarty_tpl->tpl_vars['notify_customer_status']->value==true) {?> checked="checked" <?php }?> id="notify_user" value="Y" form="quotation_form" />
                <?php echo $_smarty_tpl->__("notify_customer");?>
</a></li>
        </ul>
    </div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/rfq_mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>Smarty::$_smarty_vars['capture']['mainbox_title'],'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'sidebar_position'=>"left",'sidebar_icon'=>"icon-user"), 0);?>


<?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
>
    $('.cm-expand-view').click();
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>
