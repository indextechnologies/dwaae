<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 11:57:04
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/vendor_locations/hooks/companies/shipping_address.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:102568942960964450b407d5-31700103%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c1108059ee9f6d95fc639268d209ea2bb77a4a7c' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/vendor_locations/hooks/companies/shipping_address.post.tpl',
      1 => 1604834134,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '102568942960964450b407d5-31700103',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'company_data' => 0,
    'place_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60964450b634a9_38129244',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60964450b634a9_38129244')) {function content_60964450b634a9_38129244($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('vendor_locations.location','tt_addons_vendor_locations_hooks_companies_shipping_address_post_vendor_locations.location','coordinates','latitude_short','longitude_short','latitude','longitude','select'));
?>
<div class="control-group">
    <label for="elm_company_location" class="control-label"><?php echo $_smarty_tpl->__("vendor_locations.location");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->__("tt_addons_vendor_locations_hooks_companies_shipping_address_post_vendor_locations.location")), 0);?>
:</label>
    <div class="controls">
        <?php $_smarty_tpl->tpl_vars['place_id'] = new Smarty_variable(null, null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['company_data']->value['vendor_location']) {?>
            <?php $_smarty_tpl->tpl_vars['place_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['company_data']->value['vendor_location']->getPlaceId(), null, 0);?>
        <?php }?>
        <input type="text" class="cm-geocomplete input-large" data-ca-geocomplete-type="address" data-ca-geocomplete-place-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['place_id']->value, ENT_QUOTES, 'UTF-8');?>
" data-ca-geocomplete-value-elem-id="elm_company_vendor_location_value" id="elm_company_location" />

        <input type="hidden" name="company_data[vendor_location]" id="elm_company_vendor_location_value" />
    </div>
</div>

<div class="control-group">
    <label class="control-label cm-required"><?php echo $_smarty_tpl->__("coordinates");?>
 (<?php echo $_smarty_tpl->__("latitude_short");?>
 &times; <?php echo $_smarty_tpl->__("longitude_short");?>
):</label>
    <label class="control-label cm-required hidden" for="elm_latitude"><?php echo $_smarty_tpl->__("latitude");?>
</label>
    <label class="control-label cm-required hidden" for="elm_longitude"><?php echo $_smarty_tpl->__("longitude");?>
</label>
    <div class="controls">
        <input type="text" name="company_data[latitude]" id="elm_latitude" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['latitude'], ENT_QUOTES, 'UTF-8');?>
" data-ca-latest-latitude="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['latitude'], ENT_QUOTES, 'UTF-8');?>
" class="input-small">
        &times;
        <input type="text" name="company_data[longitude]" id="elm_longitude" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['longitude'], ENT_QUOTES, 'UTF-8');?>
" data-ca-latest-longitude="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['longitude'], ENT_QUOTES, 'UTF-8');?>
" class="input-small">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("select"),'but_target_id'=>"map_picker",'but_role'=>"action",'but_meta'=>"btn-primary cm-dialog-opener cm-hide-with-inputs",'but_id'=>"store_locator_picker_opener"), 0);?>

    </div>
</div><?php }} ?>
