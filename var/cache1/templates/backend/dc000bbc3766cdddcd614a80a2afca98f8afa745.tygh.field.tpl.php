<?php /* Smarty version Smarty-3.1.21, created on 2021-05-10 09:54:21
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/advanced_import/views/import_presets/components/field.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10189017026098ca8d7ec267-57853690%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dc000bbc3766cdddcd614a80a2afca98f8afa745' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/advanced_import/views/import_presets/components/field.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '10189017026098ca8d7ec267-57853690',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id' => 0,
    'name' => 0,
    'preview' => 0,
    'preset' => 0,
    'preview_item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6098ca8d80cb12_44305745',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6098ca8d80cb12_44305745')) {function content_6098ca8d80cb12_44305745($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('advanced_import.column_header','advanced_import.product_property','none','none','advanced_import.first_line_import_value','advanced_import.modifier_title','advanced_import.example_imported_title','advanced_import.example_modified_title','advanced_import.show_more','advanced_import.show_less','advanced_import.modifier','advanced_import.modifier'));
?>
<tr class="import-field" id="field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">
    <td class="import-field__name" data-th="<?php echo $_smarty_tpl->__("advanced_import.column_header");?>
">
        <input type="hidden"
               name="fields[<?php echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['name']->value), ENT_QUOTES, 'UTF-8');?>
][name]"
               value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
"
        />
        <span data-ca-advanced-import-element="field"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
</span>
    </td>
    <td class="import-field__related_object" data-th="<?php echo $_smarty_tpl->__("advanced_import.product_property",array("[product]"=>@constant('PRODUCT_NAME')));?>
">
        <input type="hidden"
               name="fields[<?php echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['name']->value), ENT_QUOTES, 'UTF-8');?>
][related_object_type]"
               id="elm_field_related_object_type_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"
        />
        <label class="cm-adv-import-placeholder cm-adv-import-placeholder--empty" 
            data-ca-advanced-import-field-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"
            data-ca-advanced-import-select-name="fields[<?php echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['name']->value), ENT_QUOTES, 'UTF-8');?>
][related_object]"
            data-ca-advanced-import-field-name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
"
            data-ca-placeholder="-<?php echo $_smarty_tpl->__("none");?>
-"
        >-<?php echo $_smarty_tpl->__("none");?>
-</label>
        <input type="hidden" id="elm_field_related_object_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="fields[<?php echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['name']->value), ENT_QUOTES, 'UTF-8');?>
][related_object]" value=""/>
    </td>
    <td class="import-field__preview" data-th="<?php echo $_smarty_tpl->__("advanced_import.first_line_import_value");?>
">
        <?php if ($_smarty_tpl->tpl_vars['preview']->value) {?>
            <?php  $_smarty_tpl->tpl_vars['preview_item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['preview_item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['preview']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['preview_item']->key => $_smarty_tpl->tpl_vars['preview_item']->value) {
$_smarty_tpl->tpl_vars['preview_item']->_loop = true;
?>
                <div class="import-field__preview-wrapper cm-show-more__wrapper">
                    <div class="import-field__preview-value cm-show-more__block">
                        <?php if ($_smarty_tpl->tpl_vars['preset']->value['fields'][$_smarty_tpl->tpl_vars['name']->value]['modifier']) {?>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preview_item']->value[$_smarty_tpl->tpl_vars['name']->value]['modified'], ENT_QUOTES, 'UTF-8');?>

                            <div class="import-field__preview-info">
                                <a class="import-field__preview-button"><i class="icon-question-sign"></i></a>
                                <div class="popover fade bottom in">
                                    <div class="arrow"></div>
                                    <h3 class="popover-title"><?php echo $_smarty_tpl->__("advanced_import.modifier_title");?>
</h3>
                                    <div class="popover-content">
                                        <div class="import-field__preview--original">
                                            <strong><?php echo $_smarty_tpl->__("advanced_import.example_imported_title");?>
</strong>
                                            <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preview_item']->value[$_smarty_tpl->tpl_vars['name']->value]['original'], ENT_QUOTES, 'UTF-8');?>
</p>
                                        </div>
                                        <div class="import-field__preview--modified">
                                            <strong><?php echo $_smarty_tpl->__("advanced_import.example_modified_title");?>
</strong>
                                            <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preview_item']->value[$_smarty_tpl->tpl_vars['name']->value]['modified'], ENT_QUOTES, 'UTF-8');?>
</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preview_item']->value[$_smarty_tpl->tpl_vars['name']->value]['original'], ENT_QUOTES, 'UTF-8');?>

                        <?php }?>
                    </div>
                </div>
            <?php } ?>
            <div class="cm-show-more__btn">
                <a href="#" class="cm-show-more__btn-more"><?php echo $_smarty_tpl->__("advanced_import.show_more");?>
</a>
                <a href="#" class="cm-show-more__btn-less"><?php echo $_smarty_tpl->__("advanced_import.show_less");?>
</a>
            </div>
        <?php }?>
    </td>
    <td class="import-field__modifier" data-th="<?php echo $_smarty_tpl->__("advanced_import.modifier");?>
">
        <div class="control-group import-field__modifier-input-group">
            <input type="text"
                   name="fields[<?php echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['name']->value), ENT_QUOTES, 'UTF-8');?>
][modifier]"
                   class="input-text input-hidden import-field__modifier-input"
                   placeholder="<?php echo $_smarty_tpl->__("advanced_import.modifier");?>
"
                   value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preset']->value['fields'][$_smarty_tpl->tpl_vars['name']->value]['modifier'], ENT_QUOTES, 'UTF-8');?>
"
                   data-ca-advanced-import-original-value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['preview_item']->value[$_smarty_tpl->tpl_vars['name']->value]['original'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'UTF-8');?>
"
                   data-ca-advanced-import-element="modifier"
            />
        </div>
    </td>
<!--field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
--></tr>
<?php }} ?>
