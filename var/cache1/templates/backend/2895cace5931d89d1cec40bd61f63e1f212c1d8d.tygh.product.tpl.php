<?php /* Smarty version Smarty-3.1.21, created on 2021-05-06 15:13:40
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15161293096093cf64690114-36365391%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2895cace5931d89d1cec40bd61f63e1f212c1d8d' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/product.tpl',
      1 => 1618382112,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '15161293096093cf64690114-36365391',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id' => 0,
    'config' => 0,
    'name_var' => 0,
    'form_for' => 0,
    'required_values' => 0,
    'product' => 0,
    'curl' => 0,
    'primary_currency' => 0,
    'currencies' => 0,
    'taxes' => 0,
    'tax_id' => 0,
    'tax' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6093cf646e4e25_99857746',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6093cf646e4e25_99857746')) {function content_6093cf646e4e25_99857746($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('remove','h_rfq.product_name','h_rfq.product_details','h_rfq.description','h_rfq.quantity','h_rfq.quantity_unit','price','price','h_rfq.tax_for_product','h_rfq.covered_in_insurance'));
?>
<?php if (!$_smarty_tpl->tpl_vars['id']->value) {?>
    <?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable(uniqid(), null, 0);?>
<?php }?>
<?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
<?php $_smarty_tpl->tpl_vars['name_var'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['name_var']->value)===null||$tmp==='' ? "quotation[product]" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['form_for'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['form_for']->value)===null||$tmp==='' ? "rfq" : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['required_values'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['required_values']->value)===null||$tmp==='' ? true : $tmp), null, 0);?>
<div class="rfq-product">
    
    <?php if ($_smarty_tpl->tpl_vars['product']->value['rfq_product_id']) {?>
        <a href="<?php echo htmlspecialchars(fn_url("h_rfq.delete_product&rfq_product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['rfq_product_id'])."?redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post cm-confirm" style="float: right; text-decoration: solid; font-weight: bold; margin-right: 10px; margin-bottom: 15px;">X</a>
    <?php } else { ?>
        <div class="right h-rfq_remove_btn"><a href="javascript:;" class="cm-rfq-remove-new-product btn btn-secondary"><?php echo $_smarty_tpl->__("remove");?>
</a></div>
    <?php }?>
    <div class="control-group">
        <label class="<?php if ($_smarty_tpl->tpl_vars['required_values']->value) {?>cm-required<?php }?>" for="elm_product_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("h_rfq.product_name");?>
:</label>
        <div class="">
            <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][product_id]" value="" class="cm-rfq-product-id" />
            <?php if ($_smarty_tpl->tpl_vars['product']->value['rfq_product_id']) {?>
                <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][rfq_product_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['rfq_product_id'], ENT_QUOTES, 'UTF-8');?>
" class="cm-rfq-product-id" />
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['product']->value['prescription_pid']) {?>
                <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][prescription_pid]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['prescription_pid'], ENT_QUOTES, 'UTF-8');?>
" class="cm-rfq-product-id" />
            <?php }?>
            <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][product_name]" id="elm_product_name_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_name'], ENT_QUOTES, 'UTF-8');?>
" class="h-rfq_input_large cm-admin-product-list-search" />
        </div>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("h_rfq.product_details"),'target'=>"#acc_poduct_extra_".((string)$_smarty_tpl->tpl_vars['id']->value),'meta'=>"h-rfq_product_extra_details",'compressed'=>false), 0);?>

    <div id="acc_poduct_extra_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="collapse in">
        
        <div class="control-group">
            <label class="control-label" for="elm_description_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("h_rfq.description");?>
:</label>
            <div class="controls">
            <textarea id="elm_description_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][description]" class="h-rfq_textarea cm-rfq_auto_description" ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['description'], ENT_QUOTES, 'UTF-8');?>
</textarea>
            </div>
        </div>
        <div class="h-rfq_product_data">
            <div class="control-group">
                <label class="control-label" for="elm_quantity_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("h_rfq.quantity");?>
:</label>
                <div class="controls">
                    <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][quantity]" id="elm_quantity_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" size="55" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['product']->value['quantity'])===null||$tmp==='' ? 1 : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="input-small"  />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_quantity_unit_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("h_rfq.quantity_unit");?>
(Like Kg, Bags):</label>
                <div class="controls">
                    <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][quantity_unit]" id="elm_quantity_unit_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity_unit'], ENT_QUOTES, 'UTF-8');?>
" class="input-long cm-quantity-units"  />
                </div>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['form_for']->value=="prescription_order") {?>
            <div class="control-group">
                <label class="control-label" for="elm_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("price");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
):</label>
                <div class="controls">
                    <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][non_copayment_price]" id="elm_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" size="10" value="<?php echo htmlspecialchars(fn_format_price((($tmp = @$_smarty_tpl->tpl_vars['product']->value['non_copayment_price'])===null||$tmp==='' ? "0.00" : $tmp),$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" class="input-small cm-rfq_auto_price" />
                </div>
            </div>
            <?php } else { ?>
            <div class="control-group">
                <label class="control-label" for="elm_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("price");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
):</label>
                <div class="controls">
                    <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][price]" id="elm_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" size="10" value="<?php echo htmlspecialchars(fn_format_price((($tmp = @$_smarty_tpl->tpl_vars['product']->value['price'])===null||$tmp==='' ? "0.00" : $tmp),$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" class="input-small cm-rfq_auto_price" />
                </div>
            </div>
            <?php }?>
            <?php $_smarty_tpl->tpl_vars['taxes'] = new Smarty_variable(fn_get_taxes(), null, 0);?>
            <div class="control-group">
                <label class="control-label" for="elm_product_tax_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("h_rfq.tax_for_product");?>
:</label>
                <div class="controls">
                    <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][tax_id]" id="elm_product_tax_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">
                        <?php  $_smarty_tpl->tpl_vars['tax'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tax']->_loop = false;
 $_smarty_tpl->tpl_vars['tax_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['taxes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tax']->key => $_smarty_tpl->tpl_vars['tax']->value) {
$_smarty_tpl->tpl_vars['tax']->_loop = true;
 $_smarty_tpl->tpl_vars['tax_id']->value = $_smarty_tpl->tpl_vars['tax']->key;
?>
                            <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_id']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['product']->value['tax_id']==$_smarty_tpl->tpl_vars['tax_id']->value) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax']->value['tax'], ENT_QUOTES, 'UTF-8');?>
</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['form_for']->value=="prescription_order") {?>
            <div class="control-group">
                <label class="control-label" for="elm_covered_in_insurance_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("h_rfq.covered_in_insurance");?>
:</label>
                <div class="controls">
                    <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][covered_in_insurance]" value="N" />
                    <input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name_var']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
][covered_in_insurance]" id="elm_covered_in_insurance_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" value="Y" <?php if ($_smarty_tpl->tpl_vars['product']->value['covered_in_insurance']=="Y") {?>checked="checked"<?php }?>/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_discounted_price<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">Price After Pharmacy Discount:</label>
                <div class="controls">
                    <input type="text"  id="elm_discounted_price<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discounted_price'], ENT_QUOTES, 'UTF-8');?>
" disabled/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_final_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">Final Price (After Copayment):</label>
                <div class="controls">
                    <input type="text"  id="elm_final_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['price'], ENT_QUOTES, 'UTF-8');?>
" disabled/>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div><?php }} ?>
