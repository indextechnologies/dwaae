<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 20:41:04
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_master_products/hooks/products/update_product_name.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13543079576092caa00e1e37-00921916%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '89ab135d85b0cd73d873cb9b9b601ea1d0628a3d' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_master_products/hooks/products/update_product_name.pre.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '13543079576092caa00e1e37-00921916',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product_data' => 0,
    'id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092caa00eb457_32663532',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092caa00eb457_32663532')) {function content_6092caa00eb457_32663532($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('h_master_products.master_product'));
?>
<?php if ($_smarty_tpl->tpl_vars['product_data']->value['master_product_id']) {?>
    <div class="control-group">
        <label for="elm_parent_product_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"
               class="control-label"
        ><?php echo $_smarty_tpl->__("h_master_products.master_product");?>
:</label>
        <div class="controls" id="elm_parent_product_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
">
            <a class="shift-input"
               href="<?php echo htmlspecialchars(fn_url("products.update?product_id=".((string)$_smarty_tpl->tpl_vars['product_data']->value['master_product_id'])), ENT_QUOTES, 'UTF-8');?>
"
            ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['product'], ENT_QUOTES, 'UTF-8');?>
</a>
        </div>
    </div>
<?php }?><?php }} ?>
