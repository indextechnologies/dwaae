<?php /* Smarty version Smarty-3.1.21, created on 2021-05-24 15:47:58
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/attachment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:87934573660ab926ebdb573-53084776%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '803562e47ffc951891d058485b77119e6ecf1f84' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/attachment.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '87934573660ab926ebdb573-53084776',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'not_main_admin' => 0,
    'quotation' => 0,
    'a' => 0,
    'config' => 0,
    'current_redirect_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60ab926ebec5d7_35860569',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60ab926ebec5d7_35860569')) {function content_60ab926ebec5d7_35860569($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('new_attachment','add_attachment','h_rfq.file_name','download','delete','h_rfq.no_attachments'));
?>
<div id="content_attachments" class="">
    <?php if (!$_smarty_tpl->tpl_vars['not_main_admin']->value) {?>
    <div class="btn-toolbar clearfix">
        <div class="pull-right">
            <?php $_smarty_tpl->_capture_stack[0][] = array("add_new_picker", null, null); ob_start(); ?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/attachments/update.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"add_new_attachments_files",'text'=>$_smarty_tpl->__("new_attachment"),'link_text'=>$_smarty_tpl->__("add_attachment"),'content'=>Smarty::$_smarty_vars['capture']['add_new_picker'],'act'=>"general",'icon'=>"icon-plus"), 0);?>

        </div>
    </div>
    <?php }?>
    <div class="row-fluid">
        <?php if ($_smarty_tpl->tpl_vars['quotation']->value['attachments']) {?>
            <table width="100%" class="table table-middle table--relative table-responsive ">
                <thead>
                    <tr>
                        <th width="50%"><?php echo $_smarty_tpl->__("h_rfq.file_name");?>
</th>
                        <th width="25%">&nbsp;</th>
                        <th width="25%">&nbsp;</th>
                    </tr>
                </thead>
                <?php  $_smarty_tpl->tpl_vars['a'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['a']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['quotation']->value['attachments']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['a']->key => $_smarty_tpl->tpl_vars['a']->value) {
$_smarty_tpl->tpl_vars['a']->_loop = true;
?>
                    <tr>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['a']->value['name'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'href'=>"h_rfq_attachments.download?file=".((string)$_smarty_tpl->tpl_vars['a']->value['file']),'text'=>$_smarty_tpl->__("download"),'class'=>"btn"));?>
</td>
                        <td class="right">
                            <?php if (!$_smarty_tpl->tpl_vars['not_main_admin']->value) {?>
                            <?php $_smarty_tpl->_capture_stack[0][] = array("attachment_tools", null, null); ob_start(); ?>
                                <?php $_smarty_tpl->tpl_vars["current_redirect_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
                                <li><?php ob_start();?><?php echo $_smarty_tpl->__("delete");?>
<?php $_tmp1=ob_get_clean();?><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'href'=>"h_rfq_attachments.delete?file=".((string)$_smarty_tpl->tpl_vars['a']->value['file'])."&rfq_id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['current_redirect_url']->value),'text'=>$_tmp1,'class'=>"cm-post cm-confirm"));?>
</li>
                            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                            <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['attachment_tools']));?>

                            <?php }?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        <?php } else { ?>
            <p class="no-items"><?php echo $_smarty_tpl->__("h_rfq.no_attachments");?>
</p>
        <?php }?>
    </div>
</div><?php }} ?>
