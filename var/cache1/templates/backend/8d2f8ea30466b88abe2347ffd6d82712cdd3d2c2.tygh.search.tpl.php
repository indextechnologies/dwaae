<?php /* Smarty version Smarty-3.1.21, created on 2021-05-24 15:47:51
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:124560810660ab9267ebc4b1-58383120%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d2f8ea30466b88abe2347ffd6d82712cdd3d2c2' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/search.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '124560810660ab9267ebc4b1-58383120',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'in_popup' => 0,
    'form_meta' => 0,
    'selected_section' => 0,
    'extra' => 0,
    'search' => 0,
    'dispatch' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60ab9267ed57c4_80147617',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60ab9267ed57c4_80147617')) {function content_60ab9267ed57c4_80147617($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('search','h_rfq.rfq_id','email','h_rfq.jfn','h_rfq.enquiry_no','order_status'));
?>
<?php if ($_smarty_tpl->tpl_vars['in_popup']->value) {?>
    <div class="adv-search">
    <div class="group">
<?php } else { ?>
    <div class="sidebar-row">
    <h6><?php echo $_smarty_tpl->__("search");?>
</h6>
<?php }?>

<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" name="rfq_search_form" method="get" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_meta']->value, ENT_QUOTES, 'UTF-8');?>
">
<?php $_smarty_tpl->_capture_stack[0][] = array("simple_search", null, null); ob_start(); ?>

<?php if ($_REQUEST['redirect_url']) {?>
<input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_REQUEST['redirect_url'], ENT_QUOTES, 'UTF-8');?>
" />
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['selected_section']->value!='') {?>
<input type="hidden" id="selected_section" name="selected_section" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_section']->value, ENT_QUOTES, 'UTF-8');?>
" />
<?php }?>

<?php echo $_smarty_tpl->tpl_vars['extra']->value;?>


<div class="sidebar-field">
    <label for="rfq_id"><?php echo $_smarty_tpl->__("h_rfq.rfq_id");?>
</label>
    <input type="text" name="rfq_id" id="rfq_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" size="30" />
</div>

<div class="sidebar-field">
    <label for="email"><?php echo $_smarty_tpl->__("email");?>
</label>
    <input type="text" name="email" id="email" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['email'], ENT_QUOTES, 'UTF-8');?>
" size="30"/>
</div>

<div class="sidebar-field">
    <label for="jfn"><?php echo $_smarty_tpl->__("h_rfq.jfn");?>
</label>
    <input type="text" name="jfn" id="jfn" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['jfn'], ENT_QUOTES, 'UTF-8');?>
" size="30"/>
</div>

<div class="sidebar-field">
    <label for="enquiry_no"><?php echo $_smarty_tpl->__("h_rfq.enquiry_no");?>
</label>
    <input type="text" name="enquiry_no" id="enquiry_no" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['enquiry_no'], ENT_QUOTES, 'UTF-8');?>
" size="30"/>
</div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("advanced_search", null, null); ob_start(); ?>

<div class="group">
<div class="control-group">
    <label class="control-label"><?php echo $_smarty_tpl->__("order_status");?>
</label>
    <div class="controls checkbox-list">
        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['search']->value['status'],'display'=>"checkboxes",'name'=>"statuses",'columns'=>5), 0);?>

    </div>
</div>
</div>

<div class="group">
    <div class="control-group">
    </div>
</div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/advanced_search.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('simple_search'=>Smarty::$_smarty_vars['capture']['simple_search'],'advanced_search'=>Smarty::$_smarty_vars['capture']['advanced_search'],'dispatch'=>$_smarty_tpl->tpl_vars['dispatch']->value,'view_type'=>"orders",'in_popup'=>$_smarty_tpl->tpl_vars['in_popup']->value), 0);?>


</form>

<?php if ($_smarty_tpl->tpl_vars['in_popup']->value) {?>
    </div></div>
<?php } else { ?>
    </div><hr>
<?php }?>
<?php }} ?>
