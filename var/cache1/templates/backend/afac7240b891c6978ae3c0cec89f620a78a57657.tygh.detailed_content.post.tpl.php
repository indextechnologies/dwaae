<?php /* Smarty version Smarty-3.1.21, created on 2021-05-09 23:09:20
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_dwaae_new/hooks/categories/detailed_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:68179941460983360ccf071-45076318%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'afac7240b891c6978ae3c0cec89f620a78a57657' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_dwaae_new/hooks/categories/detailed_content.post.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '68179941460983360ccf071-45076318',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'category_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60983360cd9654_24996537',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60983360cd9654_24996537')) {function content_60983360cd9654_24996537($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('category_icon_class'));
?>
<?php if (($_smarty_tpl->tpl_vars['runtime']->value['company_id']&&fn_allowed_for("ULTIMATE"))||fn_allowed_for("MULTIVENDOR")) {?>
<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("category_icon_class"),'target'=>"#ec_cat_icon"), 0);?>

<fieldset>
	<div id="ec_cat_icon" class="in collapse">
		<div class="control-group">
            <label class="control-label" for="elm_category_icon_class"><?php echo $_smarty_tpl->__('icon_class');?>
:</label>
            <div class="controls">
                <input type="text" name="category_data[icon_class]" id="elm_category_icon_class" size="10" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['icon_class'], ENT_QUOTES, 'UTF-8');?>
" class="input-text-short user-success">
            </div>
        </div>
	</div>
</fieldset>
<?php }?>
<?php }} ?>
