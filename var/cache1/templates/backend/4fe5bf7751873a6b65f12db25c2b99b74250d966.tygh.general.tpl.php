<?php /* Smarty version Smarty-3.1.21, created on 2021-05-24 15:47:58
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/general.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16013303760ab926eb5aad9-33276757%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4fe5bf7751873a6b65f12db25c2b99b74250d966' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/general.tpl',
      1 => 1602066861,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '16013303760ab926eb5aad9-33276757',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'quotation' => 0,
    'display' => 0,
    'extra_url' => 0,
    'type' => 0,
    'name' => 0,
    'default_notes' => 0,
    'allow_payment_detail_status' => 0,
    'id' => 0,
    'product' => 0,
    'primary_currency' => 0,
    'currencies' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60ab926eb8cc05_94976874',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60ab926eb8cc05_94976874')) {function content_60ab926eb8cc05_94976874($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('actions','h_rfq.status','h_rfq.delivey_period','h_rfq.user','h_rfq.recommended_payment_method','h_rfq.quotation_notes','h_rfq.buyer_details','products','h_rfq.information_for_admin_to_complete_allow_for_payment','h_rfq.total_vat','h_rfq.non_tax_price_total','h_rfq.tax_price_total','h_rfq.total_vat','h_rfq.non_tax_price_total','h_rfq.tax_price_total','h_rfq.add_product','totals','subtotal','shipping_cost','h_rfq.vat','total','h_rfq.add_product'));
?>
<div id="content_general" class="form-horizontal form-edit">
    <div class="btn-toolbar clearfix">
        <div class="pull-right">
            <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/expand_compress.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        </div>
    </div>
    <div class="row-fluid">
        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("actions"),'target'=>"#rfq_actions",'meta'=>'','compressed'=>false), 0);?>

        <div id="rfq_actions" class="collapse in">

            <div class="control-group">
                <label class="control-label" for="elm_commercial_quote">Commercial Quote:</label>
                <div class="controls">
                    <input type="hidden" name="quotation[is_commercial_quote]" value="N" />
                    <input type="checkbox" name="quotation[is_commercial_quote]" id="elm_commercial_quote" value="Y" <?php if ($_smarty_tpl->tpl_vars['quotation']->value['is_commercial_quote']=="Y") {?>checked="checked"<?php }?>  />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"><?php echo $_smarty_tpl->__("h_rfq.status");?>
</label>
                <div class="controls">
                    <?php $_smarty_tpl->tpl_vars['display'] = new Smarty_variable("select", null, 0);?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['quotation']->value['status'],'display'=>$_smarty_tpl->tpl_vars['display']->value,'name'=>"quotation[status]"), 0);?>

                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_delivey_period"><?php echo $_smarty_tpl->__("h_rfq.delivey_period");?>
 (Days):</label>
                <div class="controls">
                    <input type="text" name="quotation[delivery_period]" id="elm_delivey_period" size="10" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['delivery_period'], ENT_QUOTES, 'UTF-8');?>
" class="input-long"  />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label"><?php echo $_smarty_tpl->__("h_rfq.user");?>
</label>
                <div class="controls">
                    <?php $_smarty_tpl->tpl_vars['extra_url'] = new Smarty_variable("&rfq_user_select=true", null, 0);?>
                    <?php echo $_smarty_tpl->getSubTemplate ("pickers/users/picker.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('display'=>"radio",'but_meta'=>"btn",'extra_url'=>$_smarty_tpl->tpl_vars['extra_url']->value,'view_mode'=>"single_button",'user_info'=>fn_get_user_info($_smarty_tpl->tpl_vars['quotation']->value['user_id']),'data_id'=>"issuer_info",'input_name'=>"quotation[user_id]"), 0);?>

                </div>
            </div>

            <div class="control-group">
                <label for="el_recommended_payment_method" class="control-label cm-required"><?php echo $_smarty_tpl->__("h_rfq.recommended_payment_method");?>
</label>
                <div class="controls">
                    <select id="el_recommended_payment_method"  name="quotation[payment_type]">
                        <?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable;
 $_from = fn_rfq_payment_types(); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['type']->value = $_smarty_tpl->tpl_vars['name']->key;
?>
                            <option <?php if ($_smarty_tpl->tpl_vars['type']->value==$_smarty_tpl->tpl_vars['quotation']->value['payment_type']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label for="el_some_notes_of_quotation" class="control-label"><?php echo $_smarty_tpl->__("h_rfq.quotation_notes");?>
</label>
                <div class="controls">
                    <?php $_smarty_tpl->tpl_vars['default_notes'] = new Smarty_variable('<table style="width: 100%; height: 50px; margin-left: auto; margin-right: auto;"> <tbody> <tr style="border-color: #000000;"> <td style="width: 20%; background-color: #f4f4f7; text-align: left;"><strong>Delivery:</strong> </td> <td style="width: 80%; background-color: #f4f4f7; text-align: left;"> </td> </tr> <tr> <td style="width: 20%; background-color: #f4f4f7; text-align: left;"><strong>Payment Terms:</strong> </td> <td style="width: 80%; background-color: #f4f4f7; text-align: left;"> </td> </tr> <tr> <td style="width: 20%; background-color: #f4f4f7; text-align: left;"><strong>NOTES:</strong></td> <td style="width: 80%; background-color: #f4f4f7; text-align: left;"> </td> </tr> </tbody> </table>
                    <table style="width: 931px;"><tbody><tr><td style="text-align: justify;"><p style="color: #040505; font-weight: bold; text-decoration: underline;">Terms & Conditions :</p><ul><li><span style="font-size: 10pt;">Upon receipt by the customer of the goods described above, discharged of any obligations related to its integrity from defects/damage.</span></li><li><span style="font-size: 10pt;">Given the prevailing conditions of Corona, we do not commit to delivering the goods on time if the reason is due to the customer or because of his lack of commitment to pay the price expedited, and we reserve the right to claim compensation for the damage. The Purchase Order is irrevocable.</span></li><li><span style="font-size: 10pt;">The Purchase Order is irrevocable & the stock and validity of the product is limited.</span></li><li><span style="font-size: 10pt;">The delivery of the product will be subjected to stock availability.</span></li><li><span style="font-size: 10pt;">Any changes on the design / quantity client need to inform in prior notice.</span></li><li><span style="font-size: 10pt;">Fulfillment of order commitment is subjected to prior sales.</span></li></ul></td></tr></tbody></table> ', null, 0);?>
                    <textarea class="cm-wysiwyg" name="quotation[quotation_notes]"><?php if ($_smarty_tpl->tpl_vars['quotation']->value['quotation_notes']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['quotation_notes'], ENT_QUOTES, 'UTF-8');
} elseif (!$_REQUEST['rfq_id']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['default_notes']->value, ENT_QUOTES, 'UTF-8');
}?></textarea>
                </div>
            </div>

        </div>
    </div>
    <div class="row-fluid">
        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("h_rfq.buyer_details"),'target'=>"#acc_buyer_details",'meta'=>'','compressed'=>false), 0);?>

        <div id="acc_buyer_details" class="collapse in">
            <div class="h-rfq_buyer_details_container">
                <?php echo Smarty::$_smarty_vars['capture']['customer_information'];?>

            </div>
        </div>
    </div>
    <div class="row-fluid">
        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("products"),'target'=>"#rfq_products",'meta'=>'','compressed'=>false), 0);?>

        <div id="rfq_products" class="collapse in">
            <h5><?php echo $_smarty_tpl->__("h_rfq.information_for_admin_to_complete_allow_for_payment",array('[statuses]'=>implode(',',$_smarty_tpl->tpl_vars['allow_payment_detail_status']->value)));?>
</h5>
            <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
                
                <div class="table-responsive-wrapper">
                    <table width="90%" class="table table-middle table--relative table-responsive h-rfq_table">
                    <thead>
                        <tr>
                            <th width="60%">&nbsp;</th>
                            <th class="center" width="10%" >&nbsp;<?php echo $_smarty_tpl->__("h_rfq.total_vat");?>
</th>
                            <th class="center" width="10%" >&nbsp;<?php echo $_smarty_tpl->__("h_rfq.non_tax_price_total");?>
</th>
                            <th class="center" width="10%" >&nbsp;<?php echo $_smarty_tpl->__("h_rfq.tax_price_total");?>
</th>
                        </tr>
                    </thead>
                    <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['quotation']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                    <tr>
                        <td width="60%"> 
                            <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>((string)$_smarty_tpl->tpl_vars['product']->value['prescription_pid']),'product'=>$_smarty_tpl->tpl_vars['product']->value,'name_var'=>"quotation[product]",'form_for'=>"rfq"), 0);?>

                        </td>
                        <td  width="10%" class="center" data-th="<?php echo $_smarty_tpl->__("h_rfq.total_vat");?>
"><span><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['total_vat']), 0);?>
</span></td>
                        <td  width="10%" class="center" data-th="<?php echo $_smarty_tpl->__("h_rfq.non_tax_price_total");?>
"><span><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['non_tax_price_total']), 0);?>
</span></td>
                        <td  width="10%" class="center" data-th="<?php echo $_smarty_tpl->__("h_rfq.tax_price_total");?>
"><span><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_price_total']), 0);?>
</span></td>
                    </tr>
                    <?php } ?>
                    </table>
                </div>
                <div class="h-rfq_add_product_before"></div>
                <div class="h-rfq_add_product">
                    <a href="javascript:;" class="btn btn__primary" id="h-rfq_add_new_product" data-form-for="rfq" data-name-var="quotation[product]"><?php echo $_smarty_tpl->__("h_rfq.add_product");?>
</a>
                </div>
                <div class="order-notes statistic">
                    <div class="clearfix">
                        <div class="table-wrapper">
                            <table class="pull-right">
                                <tr class="totals">
                                    <td class="totals-label">&nbsp;</td>
                                    <td class="totals" width="100px"><h4><?php echo $_smarty_tpl->__("totals");?>
</h4></td>
                                </tr>

                                <tr>
                                    <td class="statistic-label"><?php echo $_smarty_tpl->__("subtotal");?>
:</td>
                                    <td class="right" data-ct-totals="subtotal"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['subtotal']), 0);?>
</td>
                                </tr>

                                <tr>
                                    <td class="statistic-label"><?php echo $_smarty_tpl->__("shipping_cost");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
):</td>
                                    <td class="right" data-ct-totals="shipping_cost">
                                    <?php if (true) {?>
                                        <input type="text" name="quotation[shipping_price]" id="elm_shipping_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" size="10" value="<?php echo htmlspecialchars(fn_format_price((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['shipping_price'])===null||$tmp==='' ? "0.00" : $tmp),$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" class="input-small" />
                                    <?php } else { ?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['shipping_non_tax_price']), 0);?>

                                    <?php }?>
                                    
                                    </td>
                                </tr>
                              
                                <tr>
                                    <td class="statistic-label">(Products+Shipping+Surcharge VAT's) <?php echo $_smarty_tpl->__("h_rfq.vat");?>
:</td>
                                    <td class="right"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['total_tax']), 0);?>
</td>
                                </tr>

                                <tr>
                                    <td class="statistic-label"><h4><?php echo $_smarty_tpl->__("total");?>
:</h4></td>
                                    <td class="price right" data-ct-totals="total"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['quotation']->value['total']), 0);?>
</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                <div class="h-rfq_add_product_before"></div>
                <div class="h-rfq_add_product">
                    <a href="javascript:;" class="btn btn__primary" id="h-rfq_add_new_product" data-form-for="rfq" data-name-var="quotation[product]"><?php echo $_smarty_tpl->__("h_rfq.add_product");?>
</a>
                </div>
            <?php }?>
        </div>
    </div>
<!--content_general--></div><?php }} ?>
