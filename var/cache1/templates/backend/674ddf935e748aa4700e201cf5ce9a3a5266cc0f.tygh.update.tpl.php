<?php /* Smarty version Smarty-3.1.21, created on 2021-05-24 15:47:58
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/attachments/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:139839488060ab926ebef038-65318871%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '674ddf935e748aa4700e201cf5ce9a3a5266cc0f' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/attachments/update.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '139839488060ab926ebef038-65318871',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'attachment' => 0,
    'hide_inputs' => 0,
    'id' => 0,
    'quotation' => 0,
    'config' => 0,
    'hide_first_button' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_60ab926ebfe8a6_88286586',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_60ab926ebfe8a6_88286586')) {function content_60ab926ebfe8a6_88286586($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('general','file'));
?>
<?php if ($_smarty_tpl->tpl_vars['attachment']->value['attachment_id']) {?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable($_smarty_tpl->tpl_vars['attachment']->value['attachment_id'], null, 0);?>    
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["id"] = new Smarty_variable("0", null, 0);?>
<?php }?>

<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="form-horizontal form-edit  <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['hide_inputs']->value, ENT_QUOTES, 'UTF-8');?>
" name="attachments_form_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />
<input type="hidden" name="rfq_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" />
<input type="hidden" name="selected_section" value="attachments" />
<input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />

<div class="tabs cm-j-tabs clear">
    <ul class="nav nav-tabs">
        <li id="tab_details_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-js active"><a><?php echo $_smarty_tpl->__("general");?>
</a></li>
    </ul>
</div>

<div class="cm-tabs-content">
    <div id="content_tab_details_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"> 
        
        <div class="control-group">
            <label for="type_<?php echo htmlspecialchars(md5("file"), ENT_QUOTES, 'UTF-8');?>
" class="control-label <?php if (!$_smarty_tpl->tpl_vars['attachment']->value) {?>cm-required<?php }?>"><?php echo $_smarty_tpl->__("file");?>
</label>
            <div class="controls">
                <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"attachment",'hide_server'=>true,'hide_url'=>true), 0);?>

            </div>
        </div>
    </div>
</div>

<div class="buttons-container">
    <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
        <?php $_smarty_tpl->tpl_vars["hide_first_button"] = new Smarty_variable($_smarty_tpl->tpl_vars['hide_inputs']->value, null, 0);?>
    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[h_rfq_attachments.update]",'cancel_action'=>"close",'hide_first_button'=>$_smarty_tpl->tpl_vars['hide_first_button']->value,'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

</div>

</form>
<?php }} ?>
