<?php /* Smarty version Smarty-3.1.21, created on 2021-05-05 19:28:17
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/orders/tabs_extra.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12992208696092b9910ce8e0-53436495%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39f3ca09b78f4f607c474cf3be77dd9b54a91eca' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/orders/tabs_extra.pre.tpl',
      1 => 1605416701,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '12992208696092b9910ce8e0-53436495',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'order_info' => 0,
    'config' => 0,
    'prescriptions' => 0,
    'p' => 0,
    'delivery_details' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6092b9910f43f1_89180011',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6092b9910f43f1_89180011')) {function content_6092b9910f43f1_89180011($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_custom.upload_prescription','d_custom.upload_prescription','d_custom.prescriptions'));
?>
<div id="content_prescriptions">
    <div class="items-container">
    <form name="add_more_prescriptions" action="<?php echo htmlspecialchars(fn_url("d_prescriptions.upload_prescription"), ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data" method="post" class="form-edit form-horizontal">
        <div class="btn-toolbar clearfix">
            <div class="pull-right">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('method'=>"post",'but_role'=>"submit",'but_text'=>$_smarty_tpl->__("d_custom.upload_prescription")), 0);?>

            </div>
        </div>
        <input type="hidden" name="order_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['order_id'], ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
        <div class="control-group">
            <label for="id_prescribed_product_attachment" class="control-label cm-required">
                <?php echo $_smarty_tpl->__("d_custom.upload_prescription");?>

            </label>
            <div class="controls">
                <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"prescriptions[]",'multiupload'=>true,'label_id'=>"id_prescribed_product_attachment"), 0);?>

            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['prescriptions']->value) {?>
            <div class="control-group">
                <div class="control-label"><?php echo $_smarty_tpl->__("d_custom.prescriptions");?>
:</div>
                <div class="controls">
                    <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescriptions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                        <a href="<?php echo htmlspecialchars(fn_url("d_prescriptions.download&order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])."&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                        <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                            <br/>
                        <?php }?>
                    <?php } ?>
                </div>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['prescription_data']) {?>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['prescription_data']['emirate_card']) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card'=>$_smarty_tpl->tpl_vars['order_info']->value['prescription_data']['emirate_card']), 0);?>

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['prescription_data']['insurance_card']&&is_array($_smarty_tpl->tpl_vars['order_info']->value['prescription_data']['insurance_card'])&&$_smarty_tpl->tpl_vars['order_info']->value['prescription_data']['insurance_type']) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/insurance_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card'=>$_smarty_tpl->tpl_vars['order_info']->value['prescription_data']['insurance_card'],'insurance_type'=>$_smarty_tpl->tpl_vars['order_info']->value['prescription_data']['insurance_type']), 0);?>

            <?php }?>
        <?php }?>
    </form>
    </div>
</div>

<div id="content_delivery" class="hidden">
        <form name="add_delivery_details" action="<?php echo htmlspecialchars(fn_url("orders.delivery_details"), ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data" method="post" class="form-edit form-horizontal">
            <div class="btn-toolbar clearfix">
                <div class="pull-right">
                    <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('method'=>"post",'but_role'=>"submit",'but_text'=>"Save Delivery Details"), 0);?>

                </div>
            </div>
            <input type="hidden" name="order_id" value="<?php echo htmlspecialchars($_REQUEST['order_id'], ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="selected_section" value="delivery" />
            <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />

            <?php if ($_smarty_tpl->tpl_vars['delivery_details']->value['signature']) {?>
            <div>
                <h4>Customer Signature</h4>
                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_details']->value['signature'], ENT_QUOTES, 'UTF-8');?>
" style="width:250px;height:250px;" />
            </div>
            <?php }?>
            <div class="control-group ">
                <label for="delivery_boy_name" class="control-label">Delivery Boy Name</label>
                <div class="controls">
                    <input class="input-large" type="text" name="delivery_boy_name" id="delivery_boy_name" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_details']->value['delivery_boy_name'], ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>

            <div class="control-group">
                <label for="id_photos_delivered_order" class="control-label ">
                    Photos of delivered order
                </label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"delivery_photos[]",'multiupload'=>true,'label_id'=>"id_photos_delivered_order"), 0);?>

                        <?php if ($_smarty_tpl->tpl_vars['delivery_details']->value['photos']) {?>
                            <div class="">
                                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['delivery_details']->value['photos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.download&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                                    <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                                        <br/>
                                    <?php }?>
                                <?php } ?>
                            </div>
                        <?php }?>
                </div>
                 
            </div>
           

            <div class="control-group">
                <label for="id_acknowledge_reciept" class="control-label ">
                    Acknowledge/Signature
                </label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"reciepts[]",'multiupload'=>true,'label_id'=>"id_acknowledge_reciept"), 0);?>

                        <?php if ($_smarty_tpl->tpl_vars['delivery_details']->value['reciepts']) {?>
                            <div class="">
                                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['delivery_details']->value['reciepts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.download&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                                    <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                                        <br/>
                                    <?php }?>
                                <?php } ?>
                            </div>
                        <?php }?>
                </div>
            </div>

            <div class="control-group ">
                <label for="person_name" class="control-label">Person Name Who Collected the Order</label>
                <div class="controls">
                    <input class="input-large" type="text" name="person_name" id="person_name" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_details']->value['person_name'], ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>

            <div class="control-group ">
                 <label for="id_person_emirate_id" class="control-label ">
                    Person EMIRATE ID Who Collected the Order
                </label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"emirate_ids[]",'multiupload'=>true,'label_id'=>"id_person_emirate_id"), 0);?>

                        <?php if ($_smarty_tpl->tpl_vars['delivery_details']->value['emirate_ids']) {?>
                            <div class="">
                                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['delivery_details']->value['emirate_ids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.download&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                                    <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                                        <br/>
                                    <?php }?>
                                <?php } ?>
                            </div>
                        <?php }?>
                </div>
            </div>
        </form>
    </div><?php }} ?>
