<?php /* Smarty version Smarty-3.1.21, created on 2021-05-08 12:04:03
         compiled from "/home/dwaae/public_html/design/backend/templates/views/products/components/picker/item.tpl" */ ?>
<?php /*%%SmartyHeaderCode:679519550609645f392d771-89217285%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '158bdecda4cc19b0db8ff37c7a5efee3ab0e1dc3' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/views/products/components/picker/item.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '679519550609645f392d771-89217285',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title_pre' => 0,
    'title_post' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_609645f3930200_44817241',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_609645f3930200_44817241')) {function content_609645f3930200_44817241($_smarty_tpl) {?>
    ${data.image && data.image.image_path
    ? `<img src="${data.image.image_path}" width="30" height="30" alt="${data.image.alt}" class="object-picker__products-image"/>`
        : `<div class="no-image object-picker__products-image object-picker__products-image--no-image" style="width: 30px; height: 30px;"> <i class="glyph-image"></i></div>`
    }

<div class="object-picker__products-main">
    <div class="object-picker__products-name">
        <div class="object-picker__products-name-content"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title_pre']->value, ENT_QUOTES, 'UTF-8');?>
 ${data.product} <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title_post']->value, ENT_QUOTES, 'UTF-8');?>
</div>
    </div>
</div><?php }} ?>
