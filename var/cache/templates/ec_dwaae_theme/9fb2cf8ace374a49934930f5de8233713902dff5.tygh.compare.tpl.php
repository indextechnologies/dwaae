<?php /* Smarty version Smarty-3.1.21, created on 2022-02-23 05:42:11
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/product_features/compare.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1788632113621590f3ea0773-09431482%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fb2cf8ace374a49934930f5de8233713902dff5' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/product_features/compare.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1788632113621590f3ea0773-09431482',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'comparison_data' => 0,
    'continue_url' => 0,
    'config' => 0,
    'action' => 0,
    'product' => 0,
    'compare_product_id' => 0,
    'return_current_url' => 0,
    'addons' => 0,
    'average_rating' => 0,
    'obj_id' => 0,
    'old_price' => 0,
    'clean_price' => 0,
    'list_discount' => 0,
    'price' => 0,
    'capture_options_vs_qty' => 0,
    'product_options' => 0,
    'advanced_options' => 0,
    'product_amount' => 0,
    'min_qty' => 0,
    'product_edp' => 0,
    'show_descr' => 0,
    'prod_descr' => 0,
    'add_to_cart' => 0,
    'group_features' => 0,
    '_feature' => 0,
    'id' => 0,
    'group_id' => 0,
    'feature' => 0,
    'settings' => 0,
    'var' => 0,
    'r_url' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621590f40adda2_33985685',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621590f40adda2_33985685')) {function content_621590f40adda2_33985685($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_block_hook')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_modifier_enum')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.enum.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_function_html_checkboxes')) include '/home/dwaae/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.html_checkboxes.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('no_products_selected','ec_compare_list','ec_compare_sub_header','ec_sub_category_compare','all_features','all_features','similar_only','similar_only','different_only','different_only','remove','remove','by','reviews','add_to_cart','ec_qty','description','remove','clear_list','clear_list','add_feature','add','compare','no_products_selected','ec_compare_list','ec_compare_sub_header','ec_sub_category_compare','all_features','all_features','similar_only','similar_only','different_only','different_only','remove','remove','by','reviews','add_to_cart','ec_qty','description','remove','clear_list','clear_list','add_feature','add','compare'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (!$_smarty_tpl->tpl_vars['comparison_data']->value) {?>
    <p class="ty-no-items ty-compare__no-items"><?php echo $_smarty_tpl->__("no_products_selected");?>
</p>
    <div class="buttons-container ty-compare__button-empty">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/continue_shopping.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>fn_url($_smarty_tpl->tpl_vars['continue_url']->value),'but_role'=>"text"), 0);?>

    </div>
<?php } else { ?>
    <?php echo smarty_function_script(array('src'=>"js/tygh/exceptions.js"),$_smarty_tpl);?>

    <?php $_smarty_tpl->tpl_vars["return_current_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
    <div class="ec_compare_container">
        <div class="ty-compare">
            <div class="ty-compare__wrapper">
                <table class="ty-compare-products">
                    <tr>
                        <td class="ty-compare-products__menu" style="width: 290px;">
                            <div class="ec_cat_vendor_info ec_compare">
                                <div class="ec_img_div"><span class="ec-icon-comparision"><span class="path1"></span><span class="path2"></span></span></div>
                                <div class="ec_text"><?php echo $_smarty_tpl->__("ec_compare_list");?>
</div>
                                <div class="ec_cat_desc">
                                    <?php echo $_smarty_tpl->__("ec_compare_sub_header");?>

                                </div>
                            </div>

                            <div class="ec_filter_accordian_div">
                                <div id="sw_content_order_status" class="ty-order_filter__switch cm-combination-filter_order_status open cm-save-state">
                                    <div class="order_filter_header">
                                        <?php echo $_smarty_tpl->__("ec_sub_category_compare");?>

                                    </div>
                                    
                                </div>

                                <div class="ty-product-filters" id="content_order_status">
                                    <li class="ty-compare-menu__item"><?php if ($_smarty_tpl->tpl_vars['action']->value!="show_all") {?><a class="ty-compare-menu__a" href="<?php echo htmlspecialchars(fn_url("product_features.compare.show_all"), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("all_features");?>
</a><?php } else { ?><span class="ty-compare-menu__elem"><?php echo $_smarty_tpl->__("all_features");?>
</span><?php }?></li>
                                    <li class="ty-compare-menu__item"><?php if ($_smarty_tpl->tpl_vars['action']->value!="similar_only") {?><a class="ty-compare-menu__a" href="<?php echo htmlspecialchars(fn_url("product_features.compare.similar_only"), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("similar_only");?>
</a><?php } else { ?><span class="ty-compare-menu__elem"><?php echo $_smarty_tpl->__("similar_only");?>
</span><?php }?></li>
                                    <li class="ty-compare-menu__item"><?php if ($_smarty_tpl->tpl_vars['action']->value!="different_only") {?><a class="ty-compare-menu__a" href="<?php echo htmlspecialchars(fn_url("product_features.compare.different_only"), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("different_only");?>
</a><?php } else { ?><span class="ty-compare-menu__elem"><?php echo $_smarty_tpl->__("different_only");?>
</span><?php }?></li>
                                </div>
                            </div>
                        </td>
                        <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['comparison_data']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                        <td class="ty-compare-products__product" style="width: 450px;">
                            <?php $_smarty_tpl->tpl_vars['compare_product_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                            <div class="ec_compare_container">
                                <div class="ty-compare-products__delete"><a href="<?php echo htmlspecialchars(fn_url("product_features.delete_product?product_id=".((string)$_smarty_tpl->tpl_vars['compare_product_id']->value)."&redirect_url=".((string)$_smarty_tpl->tpl_vars['return_current_url']->value)), ENT_QUOTES, 'UTF-8');?>
" class="ty-remove"  title="<?php echo $_smarty_tpl->__("remove");?>
"><i class="ty-remove__icon ty-icon-cancel-circle"></i><span class="ty-remove__txt"><?php echo $_smarty_tpl->__("remove");?>
</span></a></div>
                                <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"product_features:product_compare")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"product_features:product_compare"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                                <div class="ty-compare-products__item top">
                                    
                                    <div class="ty-compare-products__img">
                                        <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['compare_product_id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>50,'image_height'=>50,'obj_id'=>$_smarty_tpl->tpl_vars['product']->value['product_id'],'images'=>$_smarty_tpl->tpl_vars['product']->value['main_pair'],'no_ids'=>true), 0);?>
</a>
                                    </div>
                                    <div class="ec_compare_product_side_div">
                                        <a class="ec_product_name" href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['compare_product_id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],30), ENT_QUOTES, 'UTF-8');?>
</a>
                                        <div class="ec_company">
                                        <?php echo $_smarty_tpl->__("by");?>
 
                                        <a href="<?php echo htmlspecialchars(fn_url("companies.products?company_id=".((string)$_smarty_tpl->tpl_vars['product']->value['company_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['product']->value['company_name']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['company_name'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');
}?></a>
                                        </div>
                                        <?php if ($_smarty_tpl->tpl_vars['addons']->value['discussion']['status']=='A') {?>
                                            <?php $_smarty_tpl->createLocalArrayVariable('product', null, 0);
$_smarty_tpl->tpl_vars['product']->value['discussion'] = fn_get_discussion($_smarty_tpl->tpl_vars['product']->value['product_id'],"P",true,$_REQUEST);?>

                                            <?php if ($_smarty_tpl->tpl_vars['product']->value['discussion_type']&&$_smarty_tpl->tpl_vars['product']->value['discussion_type']!='D') {?>
                                                <div class="ty-discussion__rating-wrapper" id="average_rating_product">
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['discussion_type']&&$_smarty_tpl->tpl_vars['product']->value['discussion_type']=="R"||$_smarty_tpl->tpl_vars['product']->value['discussion_type']=="B") {?>
                                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['average_rating']) {?>
                                                            <?php $_smarty_tpl->tpl_vars['average_rating'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['average_rating'], null, 0);?>
                                                        <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['discussion']['average_rating']) {?>
                                                            <?php $_smarty_tpl->tpl_vars['average_rating'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['discussion']['average_rating'], null, 0);?>
                                                        <?php }?>

                                                        <?php if ($_smarty_tpl->tpl_vars['average_rating']->value>0) {?>
                                                            <?php echo $_smarty_tpl->getSubTemplate ("addons/discussion/views/discussion/components/stars.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('stars'=>fn_get_discussion_rating($_smarty_tpl->tpl_vars['average_rating']->value),'link'=>"products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."&selected_section=discussion#discussion"), 0);?>

                                                        <?php }?>

                                                    <?php }?>

                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['discussion']['posts']) {?>
                                                    <a class="ty-discussion__review-a cm-external-click" data-ca-scroll="content_discussion" data-ca-external-click-id="discussion"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discussion']['search']['total_items'], ENT_QUOTES, 'UTF-8');?>
 <?php echo $_smarty_tpl->__("reviews",array($_smarty_tpl->tpl_vars['product']->value['discussion']['search']['total_items']));?>
</a>
                                                    <?php }?>
                                                <!--average_rating_product--></div>
                                            <?php }?>
                                        <?php }?>                                    
                                    </div>
                                </div>
                                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"product_features:product_compare"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                                <div class="ty-product-block">
                                    <div class="ty-product-block__wrapper">
                                        <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/product_data.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'show_old_price'=>true,'show_price_values'=>true,'show_price'=>true,'show_clean_price'=>true,'show_product_amount'=>true,'but_role'=>"big",'but_text'=>$_smarty_tpl->__("add_to_cart"),'quantity_text'=>$_smarty_tpl->__("ec_qty"),'show_add_to_cart'=>true), 0);?>

                                        
                                        <?php $_smarty_tpl->tpl_vars["old_price"] = new Smarty_variable("old_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars["price"] = new Smarty_variable("price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars["clean_price"] = new Smarty_variable("clean_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars["list_discount"] = new Smarty_variable("list_discount_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars["discount_label"] = new Smarty_variable("discount_label_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>

                                        <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"products:promo_text")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"products:promo_text"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['promo_text']) {?>
                                        <div class="ty-product-block__note-wrapper">
                                            <div class="ty-product-block__note ty-product-block__note-inner">
                                                <?php echo $_smarty_tpl->tpl_vars['product']->value['promo_text'];?>

                                            </div>
                                        </div>
                                        <?php }?>
                                        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"products:promo_text"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


                                        <div class="<?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value])) {?>prices-container <?php }?>price-wrap">
                                            <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value])) {?>
                                                <div class="ty-product-prices">
                                                    <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])) {
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value];
}?>
                                            <?php }?>

                                            <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['price']->value])) {?>
                                                <div class="ty-product-block__price-actual">
                                                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['price']->value];?>

                                                </div>
                                            <?php }?>

                                            <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value])) {?>
                                                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value];?>

                                                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value];?>

                                                </div>
                                            <?php }?>
                                        </div>

                                        <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
$_smarty_tpl->_capture_stack[0][] = array("product_options", null, null); ob_start();
echo Smarty::$_smarty_vars['capture']['product_options'];
}?>
                                        <div class="ty-product-block__option">
                                            <?php $_smarty_tpl->tpl_vars["product_options"] = new Smarty_variable("product_options_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_options']->value];?>

                                        </div>
                                        <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?>

                                        <div class="ty-product-block__advanced-option clearfix">
                                            <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
$_smarty_tpl->_capture_stack[0][] = array("product_options", null, null); ob_start();
echo Smarty::$_smarty_vars['capture']['product_options'];
}?>
                                            <?php $_smarty_tpl->tpl_vars["advanced_options"] = new Smarty_variable("advanced_options_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['advanced_options']->value];?>

                                            <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?>
                                        </div>

                                        <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
$_smarty_tpl->_capture_stack[0][] = array("product_options", null, null); ob_start();
echo Smarty::$_smarty_vars['capture']['product_options'];
}?>
                                        <div class="ty-product-block__field-group">
                                            <?php $_smarty_tpl->tpl_vars["product_amount"] = new Smarty_variable("product_amount_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_amount']->value];?>


                                            
                                            <?php if ($_smarty_tpl->tpl_vars['product']->value['prices']) {?>
                                                <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/products_qty_discounts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                                            <?php }?>

                                            <?php $_smarty_tpl->tpl_vars["min_qty"] = new Smarty_variable("min_qty_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['min_qty']->value];?>

                                        </div>
                                        <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?>

                                        <?php $_smarty_tpl->tpl_vars["product_edp"] = new Smarty_variable("product_edp_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_edp']->value];?>


                                        <?php if ($_smarty_tpl->tpl_vars['show_descr']->value) {?>
                                        <?php $_smarty_tpl->tpl_vars["prod_descr"] = new Smarty_variable("prod_descr_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <h3 class="ty-product-block__description-title"><?php echo $_smarty_tpl->__("description");?>
</h3>
                                            <div class="ty-product-block__description"><?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['prod_descr']->value];?>
</div>
                                        <?php }?>
                                        
                                        <div class="ec_product_features">
                                            <?php $_smarty_tpl->createLocalArrayVariable('product', null, 0);
$_smarty_tpl->tpl_vars['product']->value['header_features'] = fn_get_product_features_list($_smarty_tpl->tpl_vars['product']->value,'H');?>
                                            <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/product_features_short_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('features'=>$_smarty_tpl->tpl_vars['product']->value['header_features']), 0);?>

                                        </div>
                                    </div>
                                </div>
                                <div class="ec_product_right_info">
                                    <div class="ec_inner_product_btn">
                                        <div class="ty-product-block__button">
                                            
                                            <?php $_smarty_tpl->tpl_vars["add_to_cart"] = new Smarty_variable("details_add_to_cart_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['add_to_cart']->value];?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <?php } ?>
                    </tr>
                </table>
        
                <div class="ty-compare-feature">
                    <table class="ty-compare-feature__table">
                    <?php  $_smarty_tpl->tpl_vars["group_features"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["group_features"]->_loop = false;
 $_smarty_tpl->tpl_vars["group_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['comparison_data']->value['product_features']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["group_features"]->key => $_smarty_tpl->tpl_vars["group_features"]->value) {
$_smarty_tpl->tpl_vars["group_features"]->_loop = true;
 $_smarty_tpl->tpl_vars["group_id"]->value = $_smarty_tpl->tpl_vars["group_features"]->key;
?>
                        <?php  $_smarty_tpl->tpl_vars["_feature"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_feature"]->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['group_features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_feature"]->key => $_smarty_tpl->tpl_vars["_feature"]->value) {
$_smarty_tpl->tpl_vars["_feature"]->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars["_feature"]->key;
?>
                            <tr class="ty-compare-feature__row">
                                <td class="ty-compare-feature__item ty-compare-sort" style="width: 290px;">
                                    <strong class="ty-compare-sort__title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_feature']->value, ENT_QUOTES, 'UTF-8');?>
:</strong>
                                        <a href="<?php echo htmlspecialchars(fn_url("product_features.delete_feature?feature_id=".((string)$_smarty_tpl->tpl_vars['id']->value)."&redirect_url=".((string)$_smarty_tpl->tpl_vars['return_current_url']->value)), ENT_QUOTES, 'UTF-8');?>
" class="ty-compare-sort__a ty-icon-cancel-circle" title="<?php echo $_smarty_tpl->__("remove");?>
"></a>
                                </td>
                                <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['comparison_data']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                                    <td class="ty-compare-feature__item ty-compare-feature_item_size" style="width: 450px;">

                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['product_features'][$_smarty_tpl->tpl_vars['id']->value]) {?>
                                        <?php $_smarty_tpl->tpl_vars["feature"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_features'][$_smarty_tpl->tpl_vars['id']->value], null, 0);?>
                                    <?php } else { ?>
                                        <?php $_smarty_tpl->tpl_vars["feature"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_features'][$_smarty_tpl->tpl_vars['group_id']->value]['subfeatures'][$_smarty_tpl->tpl_vars['id']->value], null, 0);?>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['feature']->value['prefix']&&$_smarty_tpl->tpl_vars['feature']->value['feature_type']!=smarty_modifier_enum("ProductFeatures::MULTIPLE_CHECKBOX")) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['prefix'], ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::SINGLE_CHECKBOX")) {?><span class="ty-compare-checkbox" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['value'], ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['feature']->value['value']=="Y") {?><i class="ty-compare-checkbox__icon ty-icon-ok"></i><?php }?></span><?php } elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::DATE")) {
echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['feature']->value['value_int'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');
} elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::MULTIPLE_CHECKBOX")&&$_smarty_tpl->tpl_vars['feature']->value['variants']) {?><ul class="ty-compare-list"><?php  $_smarty_tpl->tpl_vars["var"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["var"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['variants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["var"]->key => $_smarty_tpl->tpl_vars["var"]->value) {
$_smarty_tpl->tpl_vars["var"]->_loop = true;
if ($_smarty_tpl->tpl_vars['var']->value['selected']) {?><li class="ty-compare-list__item"><span class="ty-compare-checkbox" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant'], ENT_QUOTES, 'UTF-8');?>
"><i class="ty-compare-checkbox__icon ty-icon-ok"></i></span>&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['prefix'], ENT_QUOTES, 'UTF-8');?>
&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant'], ENT_QUOTES, 'UTF-8');?>
&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['suffix'], ENT_QUOTES, 'UTF-8');?>
</li><?php }
} ?></ul><?php } elseif (in_array($_smarty_tpl->tpl_vars['feature']->value['feature_type'],array(smarty_modifier_enum("ProductFeatures::TEXT_SELECTBOX"),smarty_modifier_enum("ProductFeatures::EXTENDED"),smarty_modifier_enum("ProductFeatures::NUMBER_SELECTBOX")))) {
$_smarty_tpl->tpl_vars["var"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["var"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['variants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["var"]->key => $_smarty_tpl->tpl_vars["var"]->value) {
$_smarty_tpl->tpl_vars["var"]->_loop = true;
if ($_smarty_tpl->tpl_vars['var']->value['selected']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant'], ENT_QUOTES, 'UTF-8');
}
}
} elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::NUMBER_FIELD")) {
echo htmlspecialchars((($tmp = @floatval($_smarty_tpl->tpl_vars['feature']->value['value_int']))===null||$tmp==='' ? "-" : $tmp), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['feature']->value['value'])===null||$tmp==='' ? "-" : $tmp), ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['feature']->value['suffix']&&$_smarty_tpl->tpl_vars['feature']->value['feature_type']!=smarty_modifier_enum("ProductFeatures::MULTIPLE_CHECKBOX")) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['suffix'], ENT_QUOTES, 'UTF-8');
}?>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="buttons-container ty-compare__buttons">
        <?php $_smarty_tpl->tpl_vars["r_url"] = new Smarty_variable(fn_url(''), null, 0);?>
        
        <a href="<?php echo htmlspecialchars(fn_url("product_features.clear_list?redirect_url=".((string)$_smarty_tpl->tpl_vars['r_url']->value)), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("clear_list");?>
" class="ty-btn__primary ty-btn ec_btn_icon_blue"><i class="ec-icon-delete"></i><?php echo $_smarty_tpl->__("clear_list");?>
</a>

        <?php echo $_smarty_tpl->getSubTemplate ("buttons/continue_shopping.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>fn_url($_smarty_tpl->tpl_vars['continue_url']->value),'but_role'=>"text",'cart_checkout'=>true), 0);?>

    </div>

    <?php if ($_smarty_tpl->tpl_vars['comparison_data']->value['hidden_features']) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("add_feature")), 0);?>

    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="add_feature_form">
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
        <?php echo smarty_function_html_checkboxes(array('name'=>"add_features",'options'=>$_smarty_tpl->tpl_vars['comparison_data']->value['hidden_features'],'columns'=>"4"),$_smarty_tpl);?>

        <div class="buttons-container ty-mt-s">
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("add"),'but_name'=>"dispatch[product_features.add_feature]"), 0);?>

        </div>
    </form>
    <?php }?>
<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("compare");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/product_features/compare.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/product_features/compare.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (!$_smarty_tpl->tpl_vars['comparison_data']->value) {?>
    <p class="ty-no-items ty-compare__no-items"><?php echo $_smarty_tpl->__("no_products_selected");?>
</p>
    <div class="buttons-container ty-compare__button-empty">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/continue_shopping.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>fn_url($_smarty_tpl->tpl_vars['continue_url']->value),'but_role'=>"text"), 0);?>

    </div>
<?php } else { ?>
    <?php echo smarty_function_script(array('src'=>"js/tygh/exceptions.js"),$_smarty_tpl);?>

    <?php $_smarty_tpl->tpl_vars["return_current_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
    <div class="ec_compare_container">
        <div class="ty-compare">
            <div class="ty-compare__wrapper">
                <table class="ty-compare-products">
                    <tr>
                        <td class="ty-compare-products__menu" style="width: 290px;">
                            <div class="ec_cat_vendor_info ec_compare">
                                <div class="ec_img_div"><span class="ec-icon-comparision"><span class="path1"></span><span class="path2"></span></span></div>
                                <div class="ec_text"><?php echo $_smarty_tpl->__("ec_compare_list");?>
</div>
                                <div class="ec_cat_desc">
                                    <?php echo $_smarty_tpl->__("ec_compare_sub_header");?>

                                </div>
                            </div>

                            <div class="ec_filter_accordian_div">
                                <div id="sw_content_order_status" class="ty-order_filter__switch cm-combination-filter_order_status open cm-save-state">
                                    <div class="order_filter_header">
                                        <?php echo $_smarty_tpl->__("ec_sub_category_compare");?>

                                    </div>
                                    
                                </div>

                                <div class="ty-product-filters" id="content_order_status">
                                    <li class="ty-compare-menu__item"><?php if ($_smarty_tpl->tpl_vars['action']->value!="show_all") {?><a class="ty-compare-menu__a" href="<?php echo htmlspecialchars(fn_url("product_features.compare.show_all"), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("all_features");?>
</a><?php } else { ?><span class="ty-compare-menu__elem"><?php echo $_smarty_tpl->__("all_features");?>
</span><?php }?></li>
                                    <li class="ty-compare-menu__item"><?php if ($_smarty_tpl->tpl_vars['action']->value!="similar_only") {?><a class="ty-compare-menu__a" href="<?php echo htmlspecialchars(fn_url("product_features.compare.similar_only"), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("similar_only");?>
</a><?php } else { ?><span class="ty-compare-menu__elem"><?php echo $_smarty_tpl->__("similar_only");?>
</span><?php }?></li>
                                    <li class="ty-compare-menu__item"><?php if ($_smarty_tpl->tpl_vars['action']->value!="different_only") {?><a class="ty-compare-menu__a" href="<?php echo htmlspecialchars(fn_url("product_features.compare.different_only"), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("different_only");?>
</a><?php } else { ?><span class="ty-compare-menu__elem"><?php echo $_smarty_tpl->__("different_only");?>
</span><?php }?></li>
                                </div>
                            </div>
                        </td>
                        <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['comparison_data']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                        <td class="ty-compare-products__product" style="width: 450px;">
                            <?php $_smarty_tpl->tpl_vars['compare_product_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                            <div class="ec_compare_container">
                                <div class="ty-compare-products__delete"><a href="<?php echo htmlspecialchars(fn_url("product_features.delete_product?product_id=".((string)$_smarty_tpl->tpl_vars['compare_product_id']->value)."&redirect_url=".((string)$_smarty_tpl->tpl_vars['return_current_url']->value)), ENT_QUOTES, 'UTF-8');?>
" class="ty-remove"  title="<?php echo $_smarty_tpl->__("remove");?>
"><i class="ty-remove__icon ty-icon-cancel-circle"></i><span class="ty-remove__txt"><?php echo $_smarty_tpl->__("remove");?>
</span></a></div>
                                <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"product_features:product_compare")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"product_features:product_compare"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                                <div class="ty-compare-products__item top">
                                    
                                    <div class="ty-compare-products__img">
                                        <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['compare_product_id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>50,'image_height'=>50,'obj_id'=>$_smarty_tpl->tpl_vars['product']->value['product_id'],'images'=>$_smarty_tpl->tpl_vars['product']->value['main_pair'],'no_ids'=>true), 0);?>
</a>
                                    </div>
                                    <div class="ec_compare_product_side_div">
                                        <a class="ec_product_name" href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['compare_product_id']->value)), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],30), ENT_QUOTES, 'UTF-8');?>
</a>
                                        <div class="ec_company">
                                        <?php echo $_smarty_tpl->__("by");?>
 
                                        <a href="<?php echo htmlspecialchars(fn_url("companies.products?company_id=".((string)$_smarty_tpl->tpl_vars['product']->value['company_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['product']->value['company_name']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['company_name'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');
}?></a>
                                        </div>
                                        <?php if ($_smarty_tpl->tpl_vars['addons']->value['discussion']['status']=='A') {?>
                                            <?php $_smarty_tpl->createLocalArrayVariable('product', null, 0);
$_smarty_tpl->tpl_vars['product']->value['discussion'] = fn_get_discussion($_smarty_tpl->tpl_vars['product']->value['product_id'],"P",true,$_REQUEST);?>

                                            <?php if ($_smarty_tpl->tpl_vars['product']->value['discussion_type']&&$_smarty_tpl->tpl_vars['product']->value['discussion_type']!='D') {?>
                                                <div class="ty-discussion__rating-wrapper" id="average_rating_product">
                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['discussion_type']&&$_smarty_tpl->tpl_vars['product']->value['discussion_type']=="R"||$_smarty_tpl->tpl_vars['product']->value['discussion_type']=="B") {?>
                                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['average_rating']) {?>
                                                            <?php $_smarty_tpl->tpl_vars['average_rating'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['average_rating'], null, 0);?>
                                                        <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['discussion']['average_rating']) {?>
                                                            <?php $_smarty_tpl->tpl_vars['average_rating'] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['discussion']['average_rating'], null, 0);?>
                                                        <?php }?>

                                                        <?php if ($_smarty_tpl->tpl_vars['average_rating']->value>0) {?>
                                                            <?php echo $_smarty_tpl->getSubTemplate ("addons/discussion/views/discussion/components/stars.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('stars'=>fn_get_discussion_rating($_smarty_tpl->tpl_vars['average_rating']->value),'link'=>"products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."&selected_section=discussion#discussion"), 0);?>

                                                        <?php }?>

                                                    <?php }?>

                                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['discussion']['posts']) {?>
                                                    <a class="ty-discussion__review-a cm-external-click" data-ca-scroll="content_discussion" data-ca-external-click-id="discussion"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['discussion']['search']['total_items'], ENT_QUOTES, 'UTF-8');?>
 <?php echo $_smarty_tpl->__("reviews",array($_smarty_tpl->tpl_vars['product']->value['discussion']['search']['total_items']));?>
</a>
                                                    <?php }?>
                                                <!--average_rating_product--></div>
                                            <?php }?>
                                        <?php }?>                                    
                                    </div>
                                </div>
                                <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"product_features:product_compare"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                                <div class="ty-product-block">
                                    <div class="ty-product-block__wrapper">
                                        <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/product_data.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'show_old_price'=>true,'show_price_values'=>true,'show_price'=>true,'show_clean_price'=>true,'show_product_amount'=>true,'but_role'=>"big",'but_text'=>$_smarty_tpl->__("add_to_cart"),'quantity_text'=>$_smarty_tpl->__("ec_qty"),'show_add_to_cart'=>true), 0);?>

                                        
                                        <?php $_smarty_tpl->tpl_vars["old_price"] = new Smarty_variable("old_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars["price"] = new Smarty_variable("price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars["clean_price"] = new Smarty_variable("clean_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars["list_discount"] = new Smarty_variable("list_discount_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars["discount_label"] = new Smarty_variable("discount_label_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>

                                        <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"products:promo_text")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"products:promo_text"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

                                        <?php if ($_smarty_tpl->tpl_vars['product']->value['promo_text']) {?>
                                        <div class="ty-product-block__note-wrapper">
                                            <div class="ty-product-block__note ty-product-block__note-inner">
                                                <?php echo $_smarty_tpl->tpl_vars['product']->value['promo_text'];?>

                                            </div>
                                        </div>
                                        <?php }?>
                                        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"products:promo_text"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


                                        <div class="<?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value])) {?>prices-container <?php }?>price-wrap">
                                            <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value])) {?>
                                                <div class="ty-product-prices">
                                                    <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])) {
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value];
}?>
                                            <?php }?>

                                            <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['price']->value])) {?>
                                                <div class="ty-product-block__price-actual">
                                                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['price']->value];?>

                                                </div>
                                            <?php }?>

                                            <?php if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value])||trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value])) {?>
                                                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value];?>

                                                    <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value];?>

                                                </div>
                                            <?php }?>
                                        </div>

                                        <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
$_smarty_tpl->_capture_stack[0][] = array("product_options", null, null); ob_start();
echo Smarty::$_smarty_vars['capture']['product_options'];
}?>
                                        <div class="ty-product-block__option">
                                            <?php $_smarty_tpl->tpl_vars["product_options"] = new Smarty_variable("product_options_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_options']->value];?>

                                        </div>
                                        <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?>

                                        <div class="ty-product-block__advanced-option clearfix">
                                            <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
$_smarty_tpl->_capture_stack[0][] = array("product_options", null, null); ob_start();
echo Smarty::$_smarty_vars['capture']['product_options'];
}?>
                                            <?php $_smarty_tpl->tpl_vars["advanced_options"] = new Smarty_variable("advanced_options_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['advanced_options']->value];?>

                                            <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?>
                                        </div>

                                        <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
$_smarty_tpl->_capture_stack[0][] = array("product_options", null, null); ob_start();
echo Smarty::$_smarty_vars['capture']['product_options'];
}?>
                                        <div class="ty-product-block__field-group">
                                            <?php $_smarty_tpl->tpl_vars["product_amount"] = new Smarty_variable("product_amount_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_amount']->value];?>


                                            
                                            <?php if ($_smarty_tpl->tpl_vars['product']->value['prices']) {?>
                                                <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/products_qty_discounts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

                                            <?php }?>

                                            <?php $_smarty_tpl->tpl_vars["min_qty"] = new Smarty_variable("min_qty_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['min_qty']->value];?>

                                        </div>
                                        <?php if ($_smarty_tpl->tpl_vars['capture_options_vs_qty']->value) {
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?>

                                        <?php $_smarty_tpl->tpl_vars["product_edp"] = new Smarty_variable("product_edp_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                        <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_edp']->value];?>


                                        <?php if ($_smarty_tpl->tpl_vars['show_descr']->value) {?>
                                        <?php $_smarty_tpl->tpl_vars["prod_descr"] = new Smarty_variable("prod_descr_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <h3 class="ty-product-block__description-title"><?php echo $_smarty_tpl->__("description");?>
</h3>
                                            <div class="ty-product-block__description"><?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['prod_descr']->value];?>
</div>
                                        <?php }?>
                                        
                                        <div class="ec_product_features">
                                            <?php $_smarty_tpl->createLocalArrayVariable('product', null, 0);
$_smarty_tpl->tpl_vars['product']->value['header_features'] = fn_get_product_features_list($_smarty_tpl->tpl_vars['product']->value,'H');?>
                                            <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/product_features_short_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('features'=>$_smarty_tpl->tpl_vars['product']->value['header_features']), 0);?>

                                        </div>
                                    </div>
                                </div>
                                <div class="ec_product_right_info">
                                    <div class="ec_inner_product_btn">
                                        <div class="ty-product-block__button">
                                            
                                            <?php $_smarty_tpl->tpl_vars["add_to_cart"] = new Smarty_variable("details_add_to_cart_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);?>
                                            <?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['add_to_cart']->value];?>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <?php } ?>
                    </tr>
                </table>
        
                <div class="ty-compare-feature">
                    <table class="ty-compare-feature__table">
                    <?php  $_smarty_tpl->tpl_vars["group_features"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["group_features"]->_loop = false;
 $_smarty_tpl->tpl_vars["group_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['comparison_data']->value['product_features']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["group_features"]->key => $_smarty_tpl->tpl_vars["group_features"]->value) {
$_smarty_tpl->tpl_vars["group_features"]->_loop = true;
 $_smarty_tpl->tpl_vars["group_id"]->value = $_smarty_tpl->tpl_vars["group_features"]->key;
?>
                        <?php  $_smarty_tpl->tpl_vars["_feature"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_feature"]->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['group_features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_feature"]->key => $_smarty_tpl->tpl_vars["_feature"]->value) {
$_smarty_tpl->tpl_vars["_feature"]->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars["_feature"]->key;
?>
                            <tr class="ty-compare-feature__row">
                                <td class="ty-compare-feature__item ty-compare-sort" style="width: 290px;">
                                    <strong class="ty-compare-sort__title"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_feature']->value, ENT_QUOTES, 'UTF-8');?>
:</strong>
                                        <a href="<?php echo htmlspecialchars(fn_url("product_features.delete_feature?feature_id=".((string)$_smarty_tpl->tpl_vars['id']->value)."&redirect_url=".((string)$_smarty_tpl->tpl_vars['return_current_url']->value)), ENT_QUOTES, 'UTF-8');?>
" class="ty-compare-sort__a ty-icon-cancel-circle" title="<?php echo $_smarty_tpl->__("remove");?>
"></a>
                                </td>
                                <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['product']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['comparison_data']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars['product']->_loop = true;
?>
                                    <td class="ty-compare-feature__item ty-compare-feature_item_size" style="width: 450px;">

                                    <?php if ($_smarty_tpl->tpl_vars['product']->value['product_features'][$_smarty_tpl->tpl_vars['id']->value]) {?>
                                        <?php $_smarty_tpl->tpl_vars["feature"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_features'][$_smarty_tpl->tpl_vars['id']->value], null, 0);?>
                                    <?php } else { ?>
                                        <?php $_smarty_tpl->tpl_vars["feature"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_features'][$_smarty_tpl->tpl_vars['group_id']->value]['subfeatures'][$_smarty_tpl->tpl_vars['id']->value], null, 0);?>
                                    <?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['feature']->value['prefix']&&$_smarty_tpl->tpl_vars['feature']->value['feature_type']!=smarty_modifier_enum("ProductFeatures::MULTIPLE_CHECKBOX")) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['prefix'], ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::SINGLE_CHECKBOX")) {?><span class="ty-compare-checkbox" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['value'], ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['feature']->value['value']=="Y") {?><i class="ty-compare-checkbox__icon ty-icon-ok"></i><?php }?></span><?php } elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::DATE")) {
echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['feature']->value['value_int'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');
} elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::MULTIPLE_CHECKBOX")&&$_smarty_tpl->tpl_vars['feature']->value['variants']) {?><ul class="ty-compare-list"><?php  $_smarty_tpl->tpl_vars["var"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["var"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['variants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["var"]->key => $_smarty_tpl->tpl_vars["var"]->value) {
$_smarty_tpl->tpl_vars["var"]->_loop = true;
if ($_smarty_tpl->tpl_vars['var']->value['selected']) {?><li class="ty-compare-list__item"><span class="ty-compare-checkbox" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant'], ENT_QUOTES, 'UTF-8');?>
"><i class="ty-compare-checkbox__icon ty-icon-ok"></i></span>&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['prefix'], ENT_QUOTES, 'UTF-8');?>
&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant'], ENT_QUOTES, 'UTF-8');?>
&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['suffix'], ENT_QUOTES, 'UTF-8');?>
</li><?php }
} ?></ul><?php } elseif (in_array($_smarty_tpl->tpl_vars['feature']->value['feature_type'],array(smarty_modifier_enum("ProductFeatures::TEXT_SELECTBOX"),smarty_modifier_enum("ProductFeatures::EXTENDED"),smarty_modifier_enum("ProductFeatures::NUMBER_SELECTBOX")))) {
$_smarty_tpl->tpl_vars["var"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["var"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['variants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["var"]->key => $_smarty_tpl->tpl_vars["var"]->value) {
$_smarty_tpl->tpl_vars["var"]->_loop = true;
if ($_smarty_tpl->tpl_vars['var']->value['selected']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant'], ENT_QUOTES, 'UTF-8');
}
}
} elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::NUMBER_FIELD")) {
echo htmlspecialchars((($tmp = @floatval($_smarty_tpl->tpl_vars['feature']->value['value_int']))===null||$tmp==='' ? "-" : $tmp), ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['feature']->value['value'])===null||$tmp==='' ? "-" : $tmp), ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['feature']->value['suffix']&&$_smarty_tpl->tpl_vars['feature']->value['feature_type']!=smarty_modifier_enum("ProductFeatures::MULTIPLE_CHECKBOX")) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['suffix'], ENT_QUOTES, 'UTF-8');
}?>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="buttons-container ty-compare__buttons">
        <?php $_smarty_tpl->tpl_vars["r_url"] = new Smarty_variable(fn_url(''), null, 0);?>
        
        <a href="<?php echo htmlspecialchars(fn_url("product_features.clear_list?redirect_url=".((string)$_smarty_tpl->tpl_vars['r_url']->value)), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("clear_list");?>
" class="ty-btn__primary ty-btn ec_btn_icon_blue"><i class="ec-icon-delete"></i><?php echo $_smarty_tpl->__("clear_list");?>
</a>

        <?php echo $_smarty_tpl->getSubTemplate ("buttons/continue_shopping.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_href'=>fn_url($_smarty_tpl->tpl_vars['continue_url']->value),'but_role'=>"text",'cart_checkout'=>true), 0);?>

    </div>

    <?php if ($_smarty_tpl->tpl_vars['comparison_data']->value['hidden_features']) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("add_feature")), 0);?>

    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="add_feature_form">
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />
        <?php echo smarty_function_html_checkboxes(array('name'=>"add_features",'options'=>$_smarty_tpl->tpl_vars['comparison_data']->value['hidden_features'],'columns'=>"4"),$_smarty_tpl);?>

        <div class="buttons-container ty-mt-s">
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>$_smarty_tpl->__("add"),'but_name'=>"dispatch[product_features.add_feature]"), 0);?>

        </div>
    </form>
    <?php }?>
<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("compare");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
