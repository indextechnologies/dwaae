<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 19:21:11
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/sitemap/components/categories_tree.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7358293266214ff67627232-75226448%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8067e6ba09a3fbd150fa047aeb9702cb56544078' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/sitemap/components/categories_tree.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '7358293266214ff67627232-75226448',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'all_categories_tree' => 0,
    'category' => 0,
    'sub_category_1' => 0,
    'sub_category_2' => 0,
    'sub_category_3' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214ff67656ac4_18164344',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214ff67656ac4_18164344')) {function content_6214ff67656ac4_18164344($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>
<ul id="sitemap_1">
<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['cat_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['all_categories_tree']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['cat_key']->value = $_smarty_tpl->tpl_vars['category']->key;
?>
     <li><a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a>
     <?php if ($_smarty_tpl->tpl_vars['category']->value['subcategories']) {?>
          <ul>
               <?php  $_smarty_tpl->tpl_vars['sub_category_1'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_category_1']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['category']->value['subcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_category_1']->key => $_smarty_tpl->tpl_vars['sub_category_1']->value) {
$_smarty_tpl->tpl_vars['sub_category_1']->_loop = true;
?>
                    <li><a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['sub_category_1']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sub_category_1']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a>
                    <?php if ($_smarty_tpl->tpl_vars['sub_category_1']->value['subcategories']) {?>
                         <ul>
                              <?php  $_smarty_tpl->tpl_vars['sub_category_2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_category_2']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sub_category_1']->value['subcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_category_2']->key => $_smarty_tpl->tpl_vars['sub_category_2']->value) {
$_smarty_tpl->tpl_vars['sub_category_2']->_loop = true;
?>
                                   <li><a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['sub_category_2']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sub_category_2']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a>
                                   <?php if ($_smarty_tpl->tpl_vars['sub_category_2']->value['subcategories']) {?>
                                        <ul>
                                             <?php  $_smarty_tpl->tpl_vars['sub_category_3'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_category_3']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sub_category_2']->value['subcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_category_3']->key => $_smarty_tpl->tpl_vars['sub_category_3']->value) {
$_smarty_tpl->tpl_vars['sub_category_3']->_loop = true;
?>
                                                  <li><a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['sub_category_3']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sub_category_3']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a></li>
                                             <?php } ?>
                                        </ul>
                                   <?php }?>
                                   </li>
                              <?php } ?>
                         </ul>
                    <?php }?>
                    </li>
               <?php } ?>
          </ul>
     <?php }?>
     </li>
<?php } ?>
</ul>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/sitemap/components/categories_tree.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/sitemap/components/categories_tree.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>
<ul id="sitemap_1">
<?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['cat_key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['all_categories_tree']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['cat_key']->value = $_smarty_tpl->tpl_vars['category']->key;
?>
     <li><a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a>
     <?php if ($_smarty_tpl->tpl_vars['category']->value['subcategories']) {?>
          <ul>
               <?php  $_smarty_tpl->tpl_vars['sub_category_1'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_category_1']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['category']->value['subcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_category_1']->key => $_smarty_tpl->tpl_vars['sub_category_1']->value) {
$_smarty_tpl->tpl_vars['sub_category_1']->_loop = true;
?>
                    <li><a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['sub_category_1']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sub_category_1']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a>
                    <?php if ($_smarty_tpl->tpl_vars['sub_category_1']->value['subcategories']) {?>
                         <ul>
                              <?php  $_smarty_tpl->tpl_vars['sub_category_2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_category_2']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sub_category_1']->value['subcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_category_2']->key => $_smarty_tpl->tpl_vars['sub_category_2']->value) {
$_smarty_tpl->tpl_vars['sub_category_2']->_loop = true;
?>
                                   <li><a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['sub_category_2']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sub_category_2']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a>
                                   <?php if ($_smarty_tpl->tpl_vars['sub_category_2']->value['subcategories']) {?>
                                        <ul>
                                             <?php  $_smarty_tpl->tpl_vars['sub_category_3'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub_category_3']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sub_category_2']->value['subcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub_category_3']->key => $_smarty_tpl->tpl_vars['sub_category_3']->value) {
$_smarty_tpl->tpl_vars['sub_category_3']->_loop = true;
?>
                                                  <li><a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['sub_category_3']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sub_category_3']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a></li>
                                             <?php } ?>
                                        </ul>
                                   <?php }?>
                                   </li>
                              <?php } ?>
                         </ul>
                    <?php }?>
                    </li>
               <?php } ?>
          </ul>
     <?php }?>
     </li>
<?php } ?>
</ul>
<?php }?><?php }} ?>
