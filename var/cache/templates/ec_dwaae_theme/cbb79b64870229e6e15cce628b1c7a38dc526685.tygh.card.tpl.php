<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 13:21:50
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/delivery_address/components/card.tpl" */ ?>
<?php /*%%SmartyHeaderCode:21238931836214ab2e4d75a9-42508786%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cbb79b64870229e6e15cce628b1c7a38dc526685' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/delivery_address/components/card.tpl',
      1 => 1619161170,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '21238931836214ab2e4d75a9-42508786',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'address_redirect_url' => 0,
    'address_result_ids' => 0,
    'address_edit_page' => 0,
    'enable_address_select' => 0,
    'address_card_selected' => 0,
    'card_view_only' => 0,
    'user_profile' => 0,
    'profile_id' => 0,
    'details_are_complete' => 0,
    'result_ids' => 0,
    'redirect_url' => 0,
    'curl' => 0,
    'states' => 0,
    'state_info' => 0,
    'state' => 0,
    'countries' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214ab2e53fe91_54595407',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214ab2e53fe91_54595407')) {function content_6214ab2e53fe91_54595407($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('my_home','my_office','other','edit_address','name','address','lattiude_and_longitude','lattiude_and_longitude','phone_number','my_home','my_office','other','edit_address','name','address','lattiude_and_longitude','lattiude_and_longitude','phone_number'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?> <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_redirect_url']->value)===null||$tmp==='' ? "delivery_address.manage" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['result_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_result_ids']->value)===null||$tmp==='' ? "ec_my_account_delivery" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['address_edit_page'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_edit_page']->value)===null||$tmp==='' ? "delivery_address.update" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['enable_address_select'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['enable_address_select']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['address_card_selected'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_card_selected']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['card_view_only'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['card_view_only']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 
<div class="ec_card_profile">
    <label class="labl">
        <?php if ($_smarty_tpl->tpl_vars['enable_address_select']->value) {?>
        <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(true, null, 0);?>
        <?php if (!$_smarty_tpl->tpl_vars['user_profile']->value['s_firstname']&&!$_smarty_tpl->tpl_vars['user_profile']->value['s_lastname']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['s_country']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['s_state']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['s_address']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['s_phone']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['ec_lat']&&!@constant('EC_MOBILE_APP')) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['ec_lng']&&!@constant('EC_MOBILE_APP')) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php }?>
        <input type="radio" name="selected_profile" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_id']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['address_card_selected']->value) {?>checked="checked"<?php }?> data-details-completed="<?php if ($_smarty_tpl->tpl_vars['details_are_complete']->value) {?>Y<?php } else { ?>N<?php }?>" />
        <?php }?>
        <div>
            <div class="ec_header_row">
                <div class="ec_left_content">
                    <i class="ec-icon-union"></i>
                    <span>
                    
                    <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['profile_name']=="my_home") {
echo $_smarty_tpl->__("my_home");?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['user_profile']->value['profile_name']=="my_office") {
echo $_smarty_tpl->__("my_office");?>

                    <?php } else {
echo $_smarty_tpl->__("other");
}?>
                    
                    </span>
                </div>
                <?php if (!$_smarty_tpl->tpl_vars['card_view_only']->value) {?>
                <div class="ec_right_content">
                    <div class="">
                        <a href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['address_edit_page']->value)."&profile_id=".((string)$_smarty_tpl->tpl_vars['user_profile']->value['profile_id'])), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax cm-ajax-full-render edit_address stop-cm-required" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
">
                            <span><?php echo $_smarty_tpl->__("edit_address");?>
</span>
                            <i class="ec-icon-edit"></i>
                        </a>
                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['profile_type']!='P') {?>
                    <div>
                        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['redirect_url']->value), null, 0);?>
                        <a href="<?php echo htmlspecialchars(fn_url("delivery_address.delete&profile_id=".((string)$_smarty_tpl->tpl_vars['user_profile']->value['profile_id'])."?redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value)), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-post cm-ajax cm-ajax-full-render ec_delete_div" <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['profile_type']!='S') {?>style="pointer-events: none;"<?php }?>>
                            <i class="ec-icon-delete"></i>
                        </a>
                    </div>
                    <?php }?>
                </div>
                <?php }?>
            </div>
            <div class="ec_profile_content">
                <table border="1" bordercolor="#c6c6c5">
                    <tr>
                        <td><?php echo $_smarty_tpl->__("name");?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_firstname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_lastname'], ENT_QUOTES, 'UTF-8');?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->__("address");?>
</td>
                        <td>
                            <?php  $_smarty_tpl->tpl_vars['state_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['user_profile']->value['s_country']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state_info']->key => $_smarty_tpl->tpl_vars['state_info']->value) {
$_smarty_tpl->tpl_vars['state_info']->_loop = true;
?>
                                <?php if ($_smarty_tpl->tpl_vars['state_info']->value['code']==$_smarty_tpl->tpl_vars['user_profile']->value['s_state']) {?>
                                    <?php $_smarty_tpl->tpl_vars['state'] = new Smarty_variable($_smarty_tpl->tpl_vars['state_info']->value['state'], null, 0);?>
                                <?php }?>
                            <?php } ?>
                            <div style="min-height: 85px; box-sizing: border-box;">
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_address'], ENT_QUOTES, 'UTF-8');?>

                                <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['s_address_2']) {?><br><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_address_2'], ENT_QUOTES, 'UTF-8');
}?><br>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_city'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['state']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_profile']->value['s_state'] : $tmp), ENT_QUOTES, 'UTF-8');?>

                                <br><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countries']->value[$_smarty_tpl->tpl_vars['user_profile']->value['s_country']], ENT_QUOTES, 'UTF-8');?>

                            </div>
                        </td>
                    </tr>
                    <?php if (!@constant('EC_MOBILE_APP')) {?> 
                    <tr>
                        <td><?php echo $_smarty_tpl->__("lattiude_and_longitude");?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['ec_lat'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['ec_lng'], ENT_QUOTES, 'UTF-8');?>
</td>
                    </tr>
                    <?php }?>
                    <?php if (@constant('EC_MOBILE_APP')&&@constant('EC_APP_VERSION')==2) {?> 
                    <tr>
                        <td><?php echo $_smarty_tpl->__("lattiude_and_longitude");?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['ec_lat'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['ec_lng'], ENT_QUOTES, 'UTF-8');?>
</td>
                    </tr>
                    <?php }?>
                    <tr>
                        <td><?php echo $_smarty_tpl->__("phone_number");?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_phone'], ENT_QUOTES, 'UTF-8');?>
</td>
                    </tr>
                    
                </table>
            </div>
        </div>
    <label>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_customer/views/delivery_address/components/card.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_customer/views/delivery_address/components/card.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?> <?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_redirect_url']->value)===null||$tmp==='' ? "delivery_address.manage" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['result_ids'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_result_ids']->value)===null||$tmp==='' ? "ec_my_account_delivery" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['address_edit_page'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_edit_page']->value)===null||$tmp==='' ? "delivery_address.update" : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['enable_address_select'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['enable_address_select']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['address_card_selected'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['address_card_selected']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 <?php $_smarty_tpl->tpl_vars['card_view_only'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['card_view_only']->value)===null||$tmp==='' ? false : $tmp), null, 0);?>
 
<div class="ec_card_profile">
    <label class="labl">
        <?php if ($_smarty_tpl->tpl_vars['enable_address_select']->value) {?>
        <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(true, null, 0);?>
        <?php if (!$_smarty_tpl->tpl_vars['user_profile']->value['s_firstname']&&!$_smarty_tpl->tpl_vars['user_profile']->value['s_lastname']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['s_country']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['s_state']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['s_address']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['s_phone']) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['ec_lat']&&!@constant('EC_MOBILE_APP')) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php } elseif (!$_smarty_tpl->tpl_vars['user_profile']->value['ec_lng']&&!@constant('EC_MOBILE_APP')) {?>
            <?php $_smarty_tpl->tpl_vars['details_are_complete'] = new Smarty_variable(false, null, 0);?>
        <?php }?>
        <input type="radio" name="selected_profile" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['profile_id']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['address_card_selected']->value) {?>checked="checked"<?php }?> data-details-completed="<?php if ($_smarty_tpl->tpl_vars['details_are_complete']->value) {?>Y<?php } else { ?>N<?php }?>" />
        <?php }?>
        <div>
            <div class="ec_header_row">
                <div class="ec_left_content">
                    <i class="ec-icon-union"></i>
                    <span>
                    
                    <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['profile_name']=="my_home") {
echo $_smarty_tpl->__("my_home");?>

                    <?php } elseif ($_smarty_tpl->tpl_vars['user_profile']->value['profile_name']=="my_office") {
echo $_smarty_tpl->__("my_office");?>

                    <?php } else {
echo $_smarty_tpl->__("other");
}?>
                    
                    </span>
                </div>
                <?php if (!$_smarty_tpl->tpl_vars['card_view_only']->value) {?>
                <div class="ec_right_content">
                    <div class="">
                        <a href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['address_edit_page']->value)."&profile_id=".((string)$_smarty_tpl->tpl_vars['user_profile']->value['profile_id'])), ENT_QUOTES, 'UTF-8');?>
" class="cm-ajax cm-ajax-full-render edit_address stop-cm-required" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
">
                            <span><?php echo $_smarty_tpl->__("edit_address");?>
</span>
                            <i class="ec-icon-edit"></i>
                        </a>
                    </div>
                    <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['profile_type']!='P') {?>
                    <div>
                        <?php $_smarty_tpl->tpl_vars['curl'] = new Smarty_variable(urlencode($_smarty_tpl->tpl_vars['redirect_url']->value), null, 0);?>
                        <a href="<?php echo htmlspecialchars(fn_url("delivery_address.delete&profile_id=".((string)$_smarty_tpl->tpl_vars['user_profile']->value['profile_id'])."?redirect_url=".((string)$_smarty_tpl->tpl_vars['curl']->value)), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['result_ids']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-post cm-ajax cm-ajax-full-render ec_delete_div" <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['profile_type']!='S') {?>style="pointer-events: none;"<?php }?>>
                            <i class="ec-icon-delete"></i>
                        </a>
                    </div>
                    <?php }?>
                </div>
                <?php }?>
            </div>
            <div class="ec_profile_content">
                <table border="1" bordercolor="#c6c6c5">
                    <tr>
                        <td><?php echo $_smarty_tpl->__("name");?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_firstname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_lastname'], ENT_QUOTES, 'UTF-8');?>
</td>
                    </tr>
                    <tr>
                        <td><?php echo $_smarty_tpl->__("address");?>
</td>
                        <td>
                            <?php  $_smarty_tpl->tpl_vars['state_info'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state_info']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['user_profile']->value['s_country']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state_info']->key => $_smarty_tpl->tpl_vars['state_info']->value) {
$_smarty_tpl->tpl_vars['state_info']->_loop = true;
?>
                                <?php if ($_smarty_tpl->tpl_vars['state_info']->value['code']==$_smarty_tpl->tpl_vars['user_profile']->value['s_state']) {?>
                                    <?php $_smarty_tpl->tpl_vars['state'] = new Smarty_variable($_smarty_tpl->tpl_vars['state_info']->value['state'], null, 0);?>
                                <?php }?>
                            <?php } ?>
                            <div style="min-height: 85px; box-sizing: border-box;">
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_address'], ENT_QUOTES, 'UTF-8');?>

                                <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['s_address_2']) {?><br><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_address_2'], ENT_QUOTES, 'UTF-8');
}?><br>
                                <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_city'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['state']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_profile']->value['s_state'] : $tmp), ENT_QUOTES, 'UTF-8');?>

                                <br><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countries']->value[$_smarty_tpl->tpl_vars['user_profile']->value['s_country']], ENT_QUOTES, 'UTF-8');?>

                            </div>
                        </td>
                    </tr>
                    <?php if (!@constant('EC_MOBILE_APP')) {?> 
                    <tr>
                        <td><?php echo $_smarty_tpl->__("lattiude_and_longitude");?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['ec_lat'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['ec_lng'], ENT_QUOTES, 'UTF-8');?>
</td>
                    </tr>
                    <?php }?>
                    <?php if (@constant('EC_MOBILE_APP')&&@constant('EC_APP_VERSION')==2) {?> 
                    <tr>
                        <td><?php echo $_smarty_tpl->__("lattiude_and_longitude");?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['ec_lat'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['ec_lng'], ENT_QUOTES, 'UTF-8');?>
</td>
                    </tr>
                    <?php }?>
                    <tr>
                        <td><?php echo $_smarty_tpl->__("phone_number");?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_profile']->value['s_phone'], ENT_QUOTES, 'UTF-8');?>
</td>
                    </tr>
                    
                </table>
            </div>
        </div>
    <label>
</div><?php }?><?php }} ?>
