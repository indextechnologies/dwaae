<?php /* Smarty version Smarty-3.1.21, created on 2022-02-25 20:01:52
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/wishlist/views/wishlist/components/wishlist_grid.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3780745216218fd701431a3-10021957%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fce414f4f420c5f2519af945499508c2e6fc2e9d' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/wishlist/views/wishlist/components/wishlist_grid.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3780745216218fd701431a3-10021957',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'products' => 0,
    'no_pagination' => 0,
    'no_sorting' => 0,
    'selected_search' => 0,
    'show_empty' => 0,
    'columns' => 0,
    'item_number' => 0,
    'settings' => 0,
    'splitted_products' => 0,
    'sproducts' => 0,
    'product' => 0,
    'obj_prefix' => 0,
    'obj_id' => 0,
    'form_open' => 0,
    'product_labels' => 0,
    'rating' => 0,
    'old_price' => 0,
    'price' => 0,
    'clean_price' => 0,
    'list_discount' => 0,
    'show_add_to_cart' => 0,
    'add_to_cart' => 0,
    'form_close' => 0,
    'title' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6218fd701ed7f2_89194669',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6218fd701ed7f2_89194669')) {function content_6218fd701ed7f2_89194669($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_split')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.split.php';
if (!is_callable('smarty_function_math')) include '/home/dwaae/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_block_hook')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('filter','ec_wishlist_count','sort','sort','sort_by','ec_sort_atoz','ec_sort_ztoa','ec_sort_price_low_to_high','ec_sort_price_high_to_low','filter','ec_wishlist_count','sort','sort','sort_by','ec_sort_atoz','ec_sort_ztoa','ec_sort_price_low_to_high','ec_sort_price_high_to_low'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['products']->value||$_REQUEST['wishlist_filter']) {?>

    <?php echo smarty_function_script(array('src'=>"js/tygh/exceptions.js"),$_smarty_tpl);?>

    

    <?php if (!$_smarty_tpl->tpl_vars['no_pagination']->value) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php if (!$_smarty_tpl->tpl_vars['no_sorting']->value) {?>
        
        <div class="ty-sort-container ec_wishlist_sort">
            <div class="ec_mob_filter">
                <span><?php echo $_smarty_tpl->__("filter");?>
</span>
                <i class="ec-icon-filter"></i>
            </div>
            <div class="ec_wishlist_count"><?php echo $_smarty_tpl->__("ec_wishlist_count",array("[count]"=>count($_smarty_tpl->tpl_vars['products']->value)));?>
</div>

            <div class="ec_mob_sort">
                <span><?php echo $_smarty_tpl->__("sort");?>
</span>
                <i class="ec-icon-sort"></i>
            </div>
            <div class="ec_sort">
                <div class="ec_sort_mob_heading">
                    <div><span><?php echo $_smarty_tpl->__("sort");?>
</span><i class="ec-icon-sort"></i></div>
                    <i class="ec-icon-close"></i>
                </div>
                <div class="ec_sort_mob_title">
                    <span><?php echo $_smarty_tpl->__("sort_by");?>
</span>
                </div>

                <div class="ty-sort-dropdown">
                    <a id="sw_elm_sort_fields" class="ty-sort-dropdown__wrapper cm-combination">
                        <?php echo $_smarty_tpl->__($_smarty_tpl->tpl_vars['selected_search']->value);?>
<i class="ty-sort-dropdown__icon ty-icon-down-micro"></i>
                    </a>
                    <ul id="elm_sort_fields" class="ty-sort-dropdown__content cm-popup-box hidden">
                        <li class="sort-by-product-asc ty-sort-dropdown__content-item">
                            <a class="cm-ajax cm-ajax-full-render ty-sort-dropdown__content-item-a <?php if ($_smarty_tpl->tpl_vars['selected_search']->value=='ec_sort_atoz') {?>ec_active<?php }?>" data-ca-target-id="pagination_contents,wishlist_filter" href="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=product&sort_order=asc"), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow">
                                <span class="ec_selected_info_div">
                                    <span class="fa fa-check" aria-hidden="true"></span>
                                </span>
                                <?php echo $_smarty_tpl->__("ec_sort_atoz");?>

                            </a>
                        </li>
                        <li class="sort-by-product-desc ty-sort-dropdown__content-item">
                            <a class="cm-ajax cm-ajax-full-render ty-sort-dropdown__content-item-a <?php if ($_smarty_tpl->tpl_vars['selected_search']->value=='ec_sort_ztoa') {?>ec_active<?php }?>" data-ca-target-id="pagination_contents,wishlist_filter" href="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=product&sort_order=desc"), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow">
                                <span class="ec_selected_info_div">
                                    <span class="fa fa-check" aria-hidden="true"></span>
                                </span>
                                <?php echo $_smarty_tpl->__("ec_sort_ztoa");?>

                            </a>
                        </li>
                        <li class="sort-by-price-asc ty-sort-dropdown__content-item">
                            <a class="cm-ajax cm-ajax-full-render ty-sort-dropdown__content-item-a <?php if ($_smarty_tpl->tpl_vars['selected_search']->value=='ec_sort_price_low_to_high') {?>ec_active<?php }?>" data-ca-target-id="pagination_contents,wishlist_filter" href="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=price&sort_order=asc"), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow">
                                <span class="ec_selected_info_div">
                                    <span class="fa fa-check" aria-hidden="true"></span>
                                </span>
                                <?php echo $_smarty_tpl->__("ec_sort_price_low_to_high");?>

                            </a>
                        </li>
                        <li class="sort-by-price-desc ty-sort-dropdown__content-item">
                            <a class="cm-ajax cm-ajax-full-render ty-sort-dropdown__content-item-a <?php if ($_smarty_tpl->tpl_vars['selected_search']->value=='ec_sort_price_high_to_low') {?>ec_active<?php }?>" data-ca-target-id="pagination_contents,wishlist_filter" href="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=price&sort_order=desc"), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow">
                                <span class="ec_selected_info_div">
                                    <span class="fa fa-check" aria-hidden="true"></span>
                                </span>
                                <?php echo $_smarty_tpl->__("ec_sort_price_high_to_low");?>

                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php }?>

    <?php if (!$_smarty_tpl->tpl_vars['show_empty']->value) {?>
        <?php echo smarty_function_split(array('data'=>$_smarty_tpl->tpl_vars['products']->value,'size'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp),'assign'=>"splitted_products"),$_smarty_tpl);?>

    <?php } else { ?>
        <?php echo smarty_function_split(array('data'=>$_smarty_tpl->tpl_vars['products']->value,'size'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp),'assign'=>"splitted_products",'skip_complete'=>true),$_smarty_tpl);?>

    <?php }?>

    <?php echo smarty_function_math(array('equation'=>"100 / x",'x'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp),'assign'=>"cell_width"),$_smarty_tpl);?>

    <?php if ($_smarty_tpl->tpl_vars['item_number']->value=="Y") {?>
        <?php $_smarty_tpl->tpl_vars["cur_number"] = new Smarty_variable(1, null, 0);?>
    <?php }?>

    
    <?php echo smarty_function_script(array('src'=>"js/tygh/product_image_gallery.js"),$_smarty_tpl);?>


    <?php if ($_smarty_tpl->tpl_vars['settings']->value['Appearance']['enable_quick_view']=='Y') {?>
        <?php $_smarty_tpl->tpl_vars['quick_nav_ids'] = new Smarty_variable(fn_fields_from_multi_level($_smarty_tpl->tpl_vars['products']->value,"product_id","product_id"), null, 0);?>
    <?php }?>
    <div class="ec_grid_list">
        <?php  $_smarty_tpl->tpl_vars["sproducts"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["sproducts"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['splitted_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["sproducts"]->key => $_smarty_tpl->tpl_vars["sproducts"]->value) {
$_smarty_tpl->tpl_vars["sproducts"]->_loop = true;
$_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sproducts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?><div class="ty-column<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['columns']->value, ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);
$_smarty_tpl->tpl_vars["obj_id_prefix"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);
echo $_smarty_tpl->getSubTemplate ("common/product_data.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'show_list_buttons'=>true), 0);?>
<div class="ec-grid-list__item"><?php $_smarty_tpl->tpl_vars["form_open"] = new Smarty_variable("form_open_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['form_open']->value];
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"products:product_multicolumns_list")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"products:product_multicolumns_list"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<div class="ec-grid-list__image"><?php echo $_smarty_tpl->getSubTemplate ("views/products/components/product_icon.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'show_gallery'=>true), 0);
$_smarty_tpl->tpl_vars["product_labels"] = new Smarty_variable("product_labels_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_labels']->value];?>
</div><div class="ec_grid_list_bottom"><?php $_smarty_tpl->tpl_vars["rating"] = new Smarty_variable("rating_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
if (Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['rating']->value]) {?><div class="ec-grid-list__rating"><?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['rating']->value];?>
</div><?php }?><div class="ec_seller_info"><div class="ec_seller_name"><?php echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');?>
</div></div><div class="ec-grid-list__item-name"><a href="<?php echo htmlspecialchars(fn_url("products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
" class="product-title"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],24,"...",true);?>
</a></div><div class="ec-grid-list__price <?php if ($_smarty_tpl->tpl_vars['product']->value['price']==0) {?>ty-grid-list__no-price<?php }?>"><?php $_smarty_tpl->tpl_vars["old_price"] = new Smarty_variable("old_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])) {
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value];
}
$_smarty_tpl->tpl_vars["price"] = new Smarty_variable("price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['price']->value];
$_smarty_tpl->tpl_vars["clean_price"] = new Smarty_variable("clean_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value];
$_smarty_tpl->tpl_vars["list_discount"] = new Smarty_variable("list_discount_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value];?>
</div></div><div class="ec-grid-list__control"><?php $_smarty_tpl->_capture_stack[0][] = array("product_multicolumns_list_control_data", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['show_add_to_cart']->value) {
$_smarty_tpl->tpl_vars['add_to_cart'] = new Smarty_variable("grid_add_to_cart_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['add_to_cart']->value];
}
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
echo Smarty::$_smarty_vars['capture']['product_multicolumns_list_control_data'];?>
</div><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"products:product_multicolumns_list"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
$_smarty_tpl->tpl_vars["form_close"] = new Smarty_variable("form_close_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['form_close']->value];?>
</div><?php }?></div><?php }
} ?>
    </div>

    <?php if (!$_smarty_tpl->tpl_vars['no_pagination']->value) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/wishlist/views/wishlist/components/wishlist_grid.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/wishlist/views/wishlist/components/wishlist_grid.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['products']->value||$_REQUEST['wishlist_filter']) {?>

    <?php echo smarty_function_script(array('src'=>"js/tygh/exceptions.js"),$_smarty_tpl);?>

    

    <?php if (!$_smarty_tpl->tpl_vars['no_pagination']->value) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php if (!$_smarty_tpl->tpl_vars['no_sorting']->value) {?>
        
        <div class="ty-sort-container ec_wishlist_sort">
            <div class="ec_mob_filter">
                <span><?php echo $_smarty_tpl->__("filter");?>
</span>
                <i class="ec-icon-filter"></i>
            </div>
            <div class="ec_wishlist_count"><?php echo $_smarty_tpl->__("ec_wishlist_count",array("[count]"=>count($_smarty_tpl->tpl_vars['products']->value)));?>
</div>

            <div class="ec_mob_sort">
                <span><?php echo $_smarty_tpl->__("sort");?>
</span>
                <i class="ec-icon-sort"></i>
            </div>
            <div class="ec_sort">
                <div class="ec_sort_mob_heading">
                    <div><span><?php echo $_smarty_tpl->__("sort");?>
</span><i class="ec-icon-sort"></i></div>
                    <i class="ec-icon-close"></i>
                </div>
                <div class="ec_sort_mob_title">
                    <span><?php echo $_smarty_tpl->__("sort_by");?>
</span>
                </div>

                <div class="ty-sort-dropdown">
                    <a id="sw_elm_sort_fields" class="ty-sort-dropdown__wrapper cm-combination">
                        <?php echo $_smarty_tpl->__($_smarty_tpl->tpl_vars['selected_search']->value);?>
<i class="ty-sort-dropdown__icon ty-icon-down-micro"></i>
                    </a>
                    <ul id="elm_sort_fields" class="ty-sort-dropdown__content cm-popup-box hidden">
                        <li class="sort-by-product-asc ty-sort-dropdown__content-item">
                            <a class="cm-ajax cm-ajax-full-render ty-sort-dropdown__content-item-a <?php if ($_smarty_tpl->tpl_vars['selected_search']->value=='ec_sort_atoz') {?>ec_active<?php }?>" data-ca-target-id="pagination_contents,wishlist_filter" href="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=product&sort_order=asc"), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow">
                                <span class="ec_selected_info_div">
                                    <span class="fa fa-check" aria-hidden="true"></span>
                                </span>
                                <?php echo $_smarty_tpl->__("ec_sort_atoz");?>

                            </a>
                        </li>
                        <li class="sort-by-product-desc ty-sort-dropdown__content-item">
                            <a class="cm-ajax cm-ajax-full-render ty-sort-dropdown__content-item-a <?php if ($_smarty_tpl->tpl_vars['selected_search']->value=='ec_sort_ztoa') {?>ec_active<?php }?>" data-ca-target-id="pagination_contents,wishlist_filter" href="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=product&sort_order=desc"), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow">
                                <span class="ec_selected_info_div">
                                    <span class="fa fa-check" aria-hidden="true"></span>
                                </span>
                                <?php echo $_smarty_tpl->__("ec_sort_ztoa");?>

                            </a>
                        </li>
                        <li class="sort-by-price-asc ty-sort-dropdown__content-item">
                            <a class="cm-ajax cm-ajax-full-render ty-sort-dropdown__content-item-a <?php if ($_smarty_tpl->tpl_vars['selected_search']->value=='ec_sort_price_low_to_high') {?>ec_active<?php }?>" data-ca-target-id="pagination_contents,wishlist_filter" href="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=price&sort_order=asc"), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow">
                                <span class="ec_selected_info_div">
                                    <span class="fa fa-check" aria-hidden="true"></span>
                                </span>
                                <?php echo $_smarty_tpl->__("ec_sort_price_low_to_high");?>

                            </a>
                        </li>
                        <li class="sort-by-price-desc ty-sort-dropdown__content-item">
                            <a class="cm-ajax cm-ajax-full-render ty-sort-dropdown__content-item-a <?php if ($_smarty_tpl->tpl_vars['selected_search']->value=='ec_sort_price_high_to_low') {?>ec_active<?php }?>" data-ca-target-id="pagination_contents,wishlist_filter" href="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=price&sort_order=desc"), ENT_QUOTES, 'UTF-8');?>
" rel="nofollow">
                                <span class="ec_selected_info_div">
                                    <span class="fa fa-check" aria-hidden="true"></span>
                                </span>
                                <?php echo $_smarty_tpl->__("ec_sort_price_high_to_low");?>

                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    <?php }?>

    <?php if (!$_smarty_tpl->tpl_vars['show_empty']->value) {?>
        <?php echo smarty_function_split(array('data'=>$_smarty_tpl->tpl_vars['products']->value,'size'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp),'assign'=>"splitted_products"),$_smarty_tpl);?>

    <?php } else { ?>
        <?php echo smarty_function_split(array('data'=>$_smarty_tpl->tpl_vars['products']->value,'size'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp),'assign'=>"splitted_products",'skip_complete'=>true),$_smarty_tpl);?>

    <?php }?>

    <?php echo smarty_function_math(array('equation'=>"100 / x",'x'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp),'assign'=>"cell_width"),$_smarty_tpl);?>

    <?php if ($_smarty_tpl->tpl_vars['item_number']->value=="Y") {?>
        <?php $_smarty_tpl->tpl_vars["cur_number"] = new Smarty_variable(1, null, 0);?>
    <?php }?>

    
    <?php echo smarty_function_script(array('src'=>"js/tygh/product_image_gallery.js"),$_smarty_tpl);?>


    <?php if ($_smarty_tpl->tpl_vars['settings']->value['Appearance']['enable_quick_view']=='Y') {?>
        <?php $_smarty_tpl->tpl_vars['quick_nav_ids'] = new Smarty_variable(fn_fields_from_multi_level($_smarty_tpl->tpl_vars['products']->value,"product_id","product_id"), null, 0);?>
    <?php }?>
    <div class="ec_grid_list">
        <?php  $_smarty_tpl->tpl_vars["sproducts"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["sproducts"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['splitted_products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["sproducts"]->key => $_smarty_tpl->tpl_vars["sproducts"]->value) {
$_smarty_tpl->tpl_vars["sproducts"]->_loop = true;
$_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sproducts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
?><div class="ty-column<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['columns']->value, ENT_QUOTES, 'UTF-8');?>
"><?php if ($_smarty_tpl->tpl_vars['product']->value) {
$_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);
$_smarty_tpl->tpl_vars["obj_id_prefix"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['product']->value['product_id']), null, 0);
echo $_smarty_tpl->getSubTemplate ("common/product_data.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'show_list_buttons'=>true), 0);?>
<div class="ec-grid-list__item"><?php $_smarty_tpl->tpl_vars["form_open"] = new Smarty_variable("form_open_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['form_open']->value];
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"products:product_multicolumns_list")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"products:product_multicolumns_list"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<div class="ec-grid-list__image"><?php echo $_smarty_tpl->getSubTemplate ("views/products/components/product_icon.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('product'=>$_smarty_tpl->tpl_vars['product']->value,'show_gallery'=>true), 0);
$_smarty_tpl->tpl_vars["product_labels"] = new Smarty_variable("product_labels_".((string)$_smarty_tpl->tpl_vars['obj_prefix']->value).((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['product_labels']->value];?>
</div><div class="ec_grid_list_bottom"><?php $_smarty_tpl->tpl_vars["rating"] = new Smarty_variable("rating_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
if (Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['rating']->value]) {?><div class="ec-grid-list__rating"><?php echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['rating']->value];?>
</div><?php }?><div class="ec_seller_info"><div class="ec_seller_name"><?php echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');?>
</div></div><div class="ec-grid-list__item-name"><a href="<?php echo htmlspecialchars(fn_url("products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
" class="product-title"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['product']->value['product'],24,"...",true);?>
</a></div><div class="ec-grid-list__price <?php if ($_smarty_tpl->tpl_vars['product']->value['price']==0) {?>ty-grid-list__no-price<?php }?>"><?php $_smarty_tpl->tpl_vars["old_price"] = new Smarty_variable("old_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
if (trim(Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value])) {
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['old_price']->value];
}
$_smarty_tpl->tpl_vars["price"] = new Smarty_variable("price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['price']->value];
$_smarty_tpl->tpl_vars["clean_price"] = new Smarty_variable("clean_price_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['clean_price']->value];
$_smarty_tpl->tpl_vars["list_discount"] = new Smarty_variable("list_discount_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['list_discount']->value];?>
</div></div><div class="ec-grid-list__control"><?php $_smarty_tpl->_capture_stack[0][] = array("product_multicolumns_list_control_data", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['show_add_to_cart']->value) {
$_smarty_tpl->tpl_vars['add_to_cart'] = new Smarty_variable("grid_add_to_cart_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['add_to_cart']->value];
}
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
echo Smarty::$_smarty_vars['capture']['product_multicolumns_list_control_data'];?>
</div><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"products:product_multicolumns_list"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
$_smarty_tpl->tpl_vars["form_close"] = new Smarty_variable("form_close_".((string)$_smarty_tpl->tpl_vars['obj_id']->value), null, 0);
echo Smarty::$_smarty_vars['capture'][$_smarty_tpl->tpl_vars['form_close']->value];?>
</div><?php }?></div><?php }
} ?>
    </div>

    <?php if (!$_smarty_tpl->tpl_vars['no_pagination']->value) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
