<?php /* Smarty version Smarty-3.1.21, created on 2022-02-25 19:55:47
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/components/buyer_details.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15142451596218fc038152e9-86775060%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '982b02d409b57c417be75bcbe98a024cb7a05e3b' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/components/buyer_details.tpl',
      1 => 1606115529,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '15142451596218fc038152e9-86775060',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'sqid' => 0,
    'name' => 0,
    'quotation' => 0,
    'user_data' => 0,
    'email' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6218fc038b6911_92292865',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6218fc038b6911_92292865')) {function content_6218fc038b6911_92292865($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('h_rfq.firstname','h_rfq.lastname','h_rfq.email','h_rfq.phone','h_rfq.fax','h_rfq.company','h_rfq.url','h_rfq.firstname','h_rfq.lastname','h_rfq.email','h_rfq.phone','h_rfq.fax','h_rfq.company','h_rfq.url'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (!$_smarty_tpl->tpl_vars['sqid']->value) {?>
    <?php $_smarty_tpl->tpl_vars['sqid'] = new Smarty_variable(rand(100000,999999), null, 0);?>
<?php }?>

<div class="h_rfq-buyer_information">
<span>
    <div class="ty-control-group">
        <label for="el_firstname_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.firstname");?>
</label>
        <input type="text" id="el_firstname_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[firstname]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['firstname'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['firstname'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
      <div class="ty-control-group">
        <label for="el_lastname_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.lastname");?>
</label>
        <input type="text" id="el_lastname_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[lastname]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['lastname'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['lastname'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
</span>
<span>

     <div class="ty-control-group">
        <label for="el_email_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-email cm-required"><?php echo $_smarty_tpl->__("h_rfq.email");?>
</label>
        <?php $_smarty_tpl->tpl_vars['email'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['email'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['email'] : $tmp), null, 0);?>
        <input type="text" id="el_email_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[email]" size="32" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>

    <div class="ty-control-group">
        <label for="el_phone_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-mask-phone-label cm-required"><?php echo $_smarty_tpl->__("h_rfq.phone");?>
</label>
        <input type="text" id="el_phone_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[phone]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['phone'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['phone'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text cm-mask-phone" />
    </div>

     <div class="ty-control-group">
        <label for="el_fax_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.fax");?>
</label>
        <input type="text" id="el_fax_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[fax]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['fax'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['fax'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
</span>
<span>
    <div class="ty-control-group">
        <label for="el_company_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.company");?>
</label>
        <input type="text" id="el_company_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[company]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['company'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['company'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
    <div class="ty-control-group">
        <label for="el_url_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.url");?>
</label>
        <input type="text" id="el_url_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[url]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
</span>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/h_rfq/views/h_rfq/components/buyer_details.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/h_rfq/views/h_rfq/components/buyer_details.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (!$_smarty_tpl->tpl_vars['sqid']->value) {?>
    <?php $_smarty_tpl->tpl_vars['sqid'] = new Smarty_variable(rand(100000,999999), null, 0);?>
<?php }?>

<div class="h_rfq-buyer_information">
<span>
    <div class="ty-control-group">
        <label for="el_firstname_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.firstname");?>
</label>
        <input type="text" id="el_firstname_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[firstname]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['firstname'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['firstname'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
      <div class="ty-control-group">
        <label for="el_lastname_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.lastname");?>
</label>
        <input type="text" id="el_lastname_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[lastname]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['lastname'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['lastname'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
</span>
<span>

     <div class="ty-control-group">
        <label for="el_email_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-email cm-required"><?php echo $_smarty_tpl->__("h_rfq.email");?>
</label>
        <?php $_smarty_tpl->tpl_vars['email'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['email'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['email'] : $tmp), null, 0);?>
        <input type="text" id="el_email_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[email]" size="32" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>

    <div class="ty-control-group">
        <label for="el_phone_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-mask-phone-label cm-required"><?php echo $_smarty_tpl->__("h_rfq.phone");?>
</label>
        <input type="text" id="el_phone_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[phone]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['phone'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['phone'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text cm-mask-phone" />
    </div>

     <div class="ty-control-group">
        <label for="el_fax_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.fax");?>
</label>
        <input type="text" id="el_fax_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[fax]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['fax'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['fax'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
</span>
<span>
    <div class="ty-control-group">
        <label for="el_company_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.company");?>
</label>
        <input type="text" id="el_company_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[company]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['company'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['company'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
    <div class="ty-control-group">
        <label for="el_url_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title"><?php echo $_smarty_tpl->__("h_rfq.url");?>
</label>
        <input type="text" id="el_url_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[url]" size="32" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['quotation']->value['buyer_details']['url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['user_data']->value['url'] : $tmp), ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
    </div>
</span>
</div><?php }?><?php }} ?>
