<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 13:21:50
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/step_header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19519834996214ab2e449775-92929746%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c03eb4c5c39803eb9bc9abb3ec0cda8ce3a40860' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/views/ec_checkout/components/step_header.tpl',
      1 => 1615614229,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '19519834996214ab2e449775-92929746',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'cart' => 0,
    'completed_steps' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214ab2e48cc42_63021525',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214ab2e48cc42_63021525')) {function content_6214ab2e48cc42_63021525($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('checkout','ec_shipping_address','ec_shipping_step_subheader','ec_shipping_address','ec_shipping_step_subheader','ec_prescription_step','ec_prescription_step','ec_payment_step','ec_payment_step_subheader','ec_payment_step','ec_payment_step_subheader','checkout','ec_shipping_address','ec_shipping_step_subheader','ec_shipping_address','ec_shipping_step_subheader','ec_prescription_step','ec_prescription_step','ec_payment_step','ec_payment_step_subheader','ec_payment_step','ec_payment_step_subheader'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (!$_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
<style>
.ec_checkout_page_container .ec_checkout_steps_header_container .checkout-steps__steps:after {
    width: 304%;
}
</style>
<?php }?>

<div class="ec_checkout_steps_header_container">
    <div class="ec_mobile_header" style="display: none;"><?php echo $_smarty_tpl->__("checkout");?>
</div>
    <div class="checkout-steps" <?php if (!$_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>style="overflow: hidden;"<?php }?>>
        <div class="checkout-steps__steps <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']=='ec_step_one') {?>current<?php }?> <?php if ($_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_one']) {?>completed<?php }?>">
            <span class="checkout-steps__steps--text">
                <div class="ec_step_count_text">
                    1 
                    <i class="ec-icon-union hidden"></i>
                </div>
                <div class="ec_step_right_container">
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']!='ec_step_one'&&$_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_one']) {?>
                        <a class="cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_one&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <i class="ec-icon-delivery-location"></i>
                        </a>

                        <a class="ec_step_text cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_one&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <?php echo $_smarty_tpl->__("ec_shipping_address");?>

                            <div class="ec_bottom_sub_header"><?php echo $_smarty_tpl->__("ec_shipping_step_subheader");?>
</div>
                        </a>
                    <?php } else { ?>
                        <i class="ec-icon-delivery-location"></i>

                        <span class="ec_step_text">
                            <?php echo $_smarty_tpl->__("ec_shipping_address");?>

                            <div class="ec_bottom_sub_header"><?php echo $_smarty_tpl->__("ec_shipping_step_subheader");?>
</div>
                        </span>
                    <?php }?>
                </div>
                
            </span>
        </div>
        
        <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
        <div class="checkout-steps__steps <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']=='ec_step_two') {?>current<?php }?> <?php if ($_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_two']) {?>completed<?php }?>">
            <span class="checkout-steps__steps--text">
                <div class="ec_step_count_text">
                    2
                    <i class="ec-icon-union hidden"></i>
                </div>
                <div class="ec_step_right_container">
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']!='ec_step_two'&&$_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_two']) {?>
                        <a class="cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_two&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <i class="ec-icon-upload-prescription"></i>
                        </a>

                        <a class="ec_step_text cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_two&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <?php echo $_smarty_tpl->__("ec_prescription_step");?>

                            
                        </a>
                    <?php } else { ?>
                        <i class="ec-icon-upload-prescription"></i>

                        <span class="ec_step_text">
                            <?php echo $_smarty_tpl->__("ec_prescription_step");?>

                            
                        </span>
                    <?php }?>
                </div>
            </span>
        </div>
        <?php }?>
        <div class="checkout-steps__steps <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']=='ec_step_three') {?>current<?php }?> <?php if ($_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_three']) {?>completed<?php }?>">
            <span class="checkout-steps__steps--text">
                <div class="ec_step_count_text">
                    3
                    <i class="ec-icon-union hidden"></i>
                </div>
                <div class="ec_step_right_container">
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']!='ec_step_three'&&$_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_three']) {?>
                        <a class="cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_three&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <i class="ec-icon-delivery-location"></i>
                        </a>

                        <a class="ec_step_text cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_three&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <?php echo $_smarty_tpl->__("ec_payment_step");?>

                            <div class="ec_bottom_sub_header"><?php echo $_smarty_tpl->__("ec_payment_step_subheader");?>
</div>
                        </a>
                    <?php } else { ?>
                        <i class="ec-icon-delivery-location"></i>

                        <span class="ec_step_text">
                            <?php echo $_smarty_tpl->__("ec_payment_step");?>

                            <div class="ec_bottom_sub_header"><?php echo $_smarty_tpl->__("ec_payment_step_subheader");?>
</div>
                        </span>
                    <?php }?>
                </div>
            </span>
        </div>
        
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/views/ec_checkout/components/step_header.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/views/ec_checkout/components/step_header.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (!$_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
<style>
.ec_checkout_page_container .ec_checkout_steps_header_container .checkout-steps__steps:after {
    width: 304%;
}
</style>
<?php }?>

<div class="ec_checkout_steps_header_container">
    <div class="ec_mobile_header" style="display: none;"><?php echo $_smarty_tpl->__("checkout");?>
</div>
    <div class="checkout-steps" <?php if (!$_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>style="overflow: hidden;"<?php }?>>
        <div class="checkout-steps__steps <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']=='ec_step_one') {?>current<?php }?> <?php if ($_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_one']) {?>completed<?php }?>">
            <span class="checkout-steps__steps--text">
                <div class="ec_step_count_text">
                    1 
                    <i class="ec-icon-union hidden"></i>
                </div>
                <div class="ec_step_right_container">
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']!='ec_step_one'&&$_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_one']) {?>
                        <a class="cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_one&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <i class="ec-icon-delivery-location"></i>
                        </a>

                        <a class="ec_step_text cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_one&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <?php echo $_smarty_tpl->__("ec_shipping_address");?>

                            <div class="ec_bottom_sub_header"><?php echo $_smarty_tpl->__("ec_shipping_step_subheader");?>
</div>
                        </a>
                    <?php } else { ?>
                        <i class="ec-icon-delivery-location"></i>

                        <span class="ec_step_text">
                            <?php echo $_smarty_tpl->__("ec_shipping_address");?>

                            <div class="ec_bottom_sub_header"><?php echo $_smarty_tpl->__("ec_shipping_step_subheader");?>
</div>
                        </span>
                    <?php }?>
                </div>
                
            </span>
        </div>
        
        <?php if ($_smarty_tpl->tpl_vars['cart']->value['prescription_required']) {?>
        <div class="checkout-steps__steps <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']=='ec_step_two') {?>current<?php }?> <?php if ($_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_two']) {?>completed<?php }?>">
            <span class="checkout-steps__steps--text">
                <div class="ec_step_count_text">
                    2
                    <i class="ec-icon-union hidden"></i>
                </div>
                <div class="ec_step_right_container">
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']!='ec_step_two'&&$_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_two']) {?>
                        <a class="cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_two&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <i class="ec-icon-upload-prescription"></i>
                        </a>

                        <a class="ec_step_text cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_two&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <?php echo $_smarty_tpl->__("ec_prescription_step");?>

                            
                        </a>
                    <?php } else { ?>
                        <i class="ec-icon-upload-prescription"></i>

                        <span class="ec_step_text">
                            <?php echo $_smarty_tpl->__("ec_prescription_step");?>

                            
                        </span>
                    <?php }?>
                </div>
            </span>
        </div>
        <?php }?>
        <div class="checkout-steps__steps <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']=='ec_step_three') {?>current<?php }?> <?php if ($_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_three']) {?>completed<?php }?>">
            <span class="checkout-steps__steps--text">
                <div class="ec_step_count_text">
                    3
                    <i class="ec-icon-union hidden"></i>
                </div>
                <div class="ec_step_right_container">
                    <?php if ($_smarty_tpl->tpl_vars['cart']->value['ec_edit_step']!='ec_step_three'&&$_smarty_tpl->tpl_vars['completed_steps']->value['ec_step_three']) {?>
                        <a class="cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_three&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <i class="ec-icon-delivery-location"></i>
                        </a>

                        <a class="ec_step_text cm-ajax cm-ajax-full-render" href="<?php echo htmlspecialchars(fn_url("checkout.checkout?ec_edit_step=ec_step_three&from_step=".((string)$_smarty_tpl->tpl_vars['cart']->value['ec_edit_step'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="checkout_*">
                            <?php echo $_smarty_tpl->__("ec_payment_step");?>

                            <div class="ec_bottom_sub_header"><?php echo $_smarty_tpl->__("ec_payment_step_subheader");?>
</div>
                        </a>
                    <?php } else { ?>
                        <i class="ec-icon-delivery-location"></i>

                        <span class="ec_step_text">
                            <?php echo $_smarty_tpl->__("ec_payment_step");?>

                            <div class="ec_bottom_sub_header"><?php echo $_smarty_tpl->__("ec_payment_step_subheader");?>
</div>
                        </span>
                    <?php }?>
                </div>
            </span>
        </div>
        
    </div>
</div><?php }?><?php }} ?>
