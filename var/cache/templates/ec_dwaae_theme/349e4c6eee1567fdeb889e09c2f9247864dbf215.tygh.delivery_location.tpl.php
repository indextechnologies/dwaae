<?php /* Smarty version Smarty-3.1.21, created on 2022-02-25 19:54:34
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/components/delivery_location.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14172850446218fbbaf22255-70363422%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '349e4c6eee1567fdeb889e09c2f9247864dbf215' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/addons/h_rfq/views/h_rfq/components/delivery_location.tpl',
      1 => 1606115529,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '14172850446218fbbaf22255-70363422',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'sqid' => 0,
    'quotation' => 0,
    'delivery_location' => 0,
    'settings' => 0,
    'name' => 0,
    'countries' => 0,
    '_country' => 0,
    'code' => 0,
    'country' => 0,
    'states' => 0,
    '_state' => 0,
    'state' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6218fbbb049d27_73626825',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6218fbbb049d27_73626825')) {function content_6218fbbb049d27_73626825($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('h_rfq.address','h_rfq.address_2','h_rfq.country','h_rfq.state','h_rfq.city','h_rfq.zipcode','h_rfq.address','h_rfq.address_2','h_rfq.country','h_rfq.state','h_rfq.city','h_rfq.zipcode'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (!$_smarty_tpl->tpl_vars['sqid']->value) {?>
    <?php $_smarty_tpl->tpl_vars['sqid'] = new Smarty_variable(rand(100000,999999), null, 0);?>
<?php }?>
<?php $_smarty_tpl->tpl_vars['countries'] = new Smarty_variable(fn_get_simple_countries(true,@constant('CART_LANGUAGE')), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['quotation']->value['delivery_location']) {?>
    <?php $_smarty_tpl->tpl_vars['delivery_location'] = new Smarty_variable(reset($_smarty_tpl->tpl_vars['quotation']->value['delivery_location']), null, 0);?>
<?php }?>
<?php $_smarty_tpl->tpl_vars['_country'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['delivery_location']->value['country'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'] : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['_state'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['delivery_location']->value['state'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_state'] : $tmp), null, 0);?>
<div class="h-rfq_delivery_location">
    <span>
        <div class="ty-control-group">
            <label for="el_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.address");?>
</label>
            <input type="text" id="el_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][address]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['address'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="el_address_2_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.address_2");?>
</label>
            <input type="text" id="el_address_2_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][address_2]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['address_2'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
        </div>
    </span>
    <span>
        <div class="ty-control-group">
            <label for="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.country");?>
</label>
            <select id="el_country_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-profile-field__select-country cm-country cm-location-shipping" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][country]">
                <?php  $_smarty_tpl->tpl_vars["country"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["country"]->_loop = false;
 $_smarty_tpl->tpl_vars["code"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["country"]->key => $_smarty_tpl->tpl_vars["country"]->value) {
$_smarty_tpl->tpl_vars["country"]->_loop = true;
 $_smarty_tpl->tpl_vars["code"]->value = $_smarty_tpl->tpl_vars["country"]->key;
?>
                <option <?php if ($_smarty_tpl->tpl_vars['_country']->value==$_smarty_tpl->tpl_vars['code']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
        <div class="ty-control-group">
            <label for="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.state");?>
</label>
            <select id="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-profile-field__select-state cm-state cm-location-shipping" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][state]">
                <?php if ($_smarty_tpl->tpl_vars['states']->value&&$_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]) {?>
                    <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                        <option <?php if ($_smarty_tpl->tpl_vars['_state']->value==$_smarty_tpl->tpl_vars['state']->value['code']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                    <?php } ?>
                <?php }?>
            </select>
            <input  type="text" id="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
_d" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][state]" size="32" maxlength="64" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_state']->value, ENT_QUOTES, 'UTF-8');?>
" disabled="disabled" class="cm-state cm-location-shipping ty-input-text hidden"/>
        </div>
        <div class="ty-control-group">
            <label for="el_city_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.city");?>
</label>
            <input type="text" id="el_city_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][city]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['city'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="el_zipcode_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.zipcode");?>
</label>
            <input type="text" id="el_zipcode_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][zipcode]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['zipcode'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
        </div>
    </span>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
(function(_, $) {
    $.ceRebuildStates('init', {
        default_country: '<?php echo htmlspecialchars(strtr($_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" )), ENT_QUOTES, 'UTF-8');?>
',
        states: <?php echo json_encode($_smarty_tpl->tpl_vars['states']->value);?>

    });
}(Tygh, Tygh.$));
<?php echo '</script'; ?>
><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/h_rfq/views/h_rfq/components/delivery_location.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/h_rfq/views/h_rfq/components/delivery_location.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (!$_smarty_tpl->tpl_vars['sqid']->value) {?>
    <?php $_smarty_tpl->tpl_vars['sqid'] = new Smarty_variable(rand(100000,999999), null, 0);?>
<?php }?>
<?php $_smarty_tpl->tpl_vars['countries'] = new Smarty_variable(fn_get_simple_countries(true,@constant('CART_LANGUAGE')), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['quotation']->value['delivery_location']) {?>
    <?php $_smarty_tpl->tpl_vars['delivery_location'] = new Smarty_variable(reset($_smarty_tpl->tpl_vars['quotation']->value['delivery_location']), null, 0);?>
<?php }?>
<?php $_smarty_tpl->tpl_vars['_country'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['delivery_location']->value['country'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'] : $tmp), null, 0);?>
<?php $_smarty_tpl->tpl_vars['_state'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['delivery_location']->value['state'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_state'] : $tmp), null, 0);?>
<div class="h-rfq_delivery_location">
    <span>
        <div class="ty-control-group">
            <label for="el_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.address");?>
</label>
            <input type="text" id="el_address_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][address]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['address'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="el_address_2_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.address_2");?>
</label>
            <input type="text" id="el_address_2_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][address_2]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['address_2'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
        </div>
    </span>
    <span>
        <div class="ty-control-group">
            <label for="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.country");?>
</label>
            <select id="el_country_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-profile-field__select-country cm-country cm-location-shipping" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][country]">
                <?php  $_smarty_tpl->tpl_vars["country"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["country"]->_loop = false;
 $_smarty_tpl->tpl_vars["code"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["country"]->key => $_smarty_tpl->tpl_vars["country"]->value) {
$_smarty_tpl->tpl_vars["country"]->_loop = true;
 $_smarty_tpl->tpl_vars["code"]->value = $_smarty_tpl->tpl_vars["country"]->key;
?>
                <option <?php if ($_smarty_tpl->tpl_vars['_country']->value==$_smarty_tpl->tpl_vars['code']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            </select>
        </div>
        <div class="ty-control-group">
            <label for="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.state");?>
</label>
            <select id="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-profile-field__select-state cm-state cm-location-shipping" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][state]">
                <?php if ($_smarty_tpl->tpl_vars['states']->value&&$_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]) {?>
                    <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                        <option <?php if ($_smarty_tpl->tpl_vars['_state']->value==$_smarty_tpl->tpl_vars['state']->value['code']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                    <?php } ?>
                <?php }?>
            </select>
            <input  type="text" id="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
_d" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][state]" size="32" maxlength="64" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_state']->value, ENT_QUOTES, 'UTF-8');?>
" disabled="disabled" class="cm-state cm-location-shipping ty-input-text hidden"/>
        </div>
        <div class="ty-control-group">
            <label for="el_city_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.city");?>
</label>
            <input type="text" id="el_city_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][city]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['city'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
        </div>
        <div class="ty-control-group">
            <label for="el_zipcode_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("h_rfq.zipcode");?>
</label>
            <input type="text" id="el_zipcode_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sqid']->value, ENT_QUOTES, 'UTF-8');?>
][zipcode]" size="100" maxlength="200" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_location']->value['zipcode'], ENT_QUOTES, 'UTF-8');?>
" class="ty-input-text" />
        </div>
    </span>
</div>

<?php echo '<script'; ?>
 type="text/javascript">
(function(_, $) {
    $.ceRebuildStates('init', {
        default_country: '<?php echo htmlspecialchars(strtr($_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'], array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" )), ENT_QUOTES, 'UTF-8');?>
',
        states: <?php echo json_encode($_smarty_tpl->tpl_vars['states']->value);?>

    });
}(Tygh, Tygh.$));
<?php echo '</script'; ?>
><?php }?><?php }} ?>
