<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 14:06:28
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/upload_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:303463256214b5a4d685f9-91469854%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e34e7bf83c8778186d37500f42e33cb0863fb32f' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_prescription/views/d_prescription/upload_form.tpl',
      1 => 1616240678,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '303463256214b5a4d685f9-91469854',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'redirect_url' => 0,
    'tabs_section' => 0,
    'delivery_address' => 0,
    'prescription_details' => 0,
    'profile_id' => 0,
    'card_select' => 0,
    'dispatch' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214b5a4dbf827_35417242',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214b5a4dbf827_35417242')) {function content_6214b5a4dbf827_35417242($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_patient_info_prescription','ec_i_am_patient','ec_authorised_carer','ec_patient_insurance_cards','do_not_use_insurance','close_dont_use_insurance_to_use_the_insurance_in_order','ec_insurance_linked_to_emirates','ec_insurance_linked_to_emirates_desc','ec_my_addresses','add_new_address','submit_the_order','error','add_or_select_address_to_continue','error','emirate_id_is_mandatory_to_upload','error','its_mandatory_to_upload_the_insurance_card','ec_patient_info_prescription','ec_i_am_patient','ec_authorised_carer','ec_patient_insurance_cards','do_not_use_insurance','close_dont_use_insurance_to_use_the_insurance_in_order','ec_insurance_linked_to_emirates','ec_insurance_linked_to_emirates_desc','ec_my_addresses','add_new_address','submit_the_order','error','add_or_select_address_to_continue','error','emirate_id_is_mandatory_to_upload','error','its_mandatory_to_upload_the_insurance_card'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_checkout_step_content" id="upload_prescription" style="margin-top: 15px;">

<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=checkout.checkout&ec_edit_step=ec_step_three", null, 0);?>



<form  name="form_upload_prescription" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">

    <input type="hidden" name="result_ids" value="checkout_steps" />

    <input type="hidden" name="ec_edit_step" value="ec_step_three" />

    <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />



    <div class="ec_prescription_top_div">

        <div class="ec_header"><i class="ec-icon-id"></i><span><?php echo $_smarty_tpl->__("ec_patient_info_prescription");?>
</span></div>

        <div class="ec_content">

            <?php echo smarty_function_script(array('src'=>"js/tygh/tabs.js"),$_smarty_tpl);?>


            <div class="ty-tabs cm-j-tabs cm-j-tabs-disable-convertation clearfix">

                <ul class="ty-tabs__list" <?php if ($_smarty_tpl->tpl_vars['tabs_section']->value) {?>id="tabs_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tabs_section']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>>

                    <li id="ec_tab_patient" class="ty-tabs__item cm-js"><a class="ty-tabs__a"><i class="ec-icon-union"></i><?php echo $_smarty_tpl->__("ec_i_am_patient");?>
</a></li>

                    <li id="ec_tab_authorised_carer" class="ty-tabs__item cm-js"><a class="ty-tabs__a"><i class="ec-icon-union"></i><?php echo $_smarty_tpl->__("ec_authorised_carer");?>
</a></li>

                </ul>

            </div>

            <div class="cm-tabs-content ty-tabs__content clearfix" id="tabs_content">

                <div id="content_ec_tab_patient11" class="">

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_prescription/views/d_prescription/components/patient_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                </div>

            </div>

        </div>

    </div>



    <div class="ec_prescription_bottom_div">

        <div class="ec_header">

            <div class="ec_left">

                <i class="ec-icon-id"></i><span><?php echo $_smarty_tpl->__("ec_patient_insurance_cards");?>
</span>

            </div>

            <div class="ec_right">

                <span><?php echo $_smarty_tpl->__("do_not_use_insurance");?>
</span>

                <label class="switch">

                    <input type="hidden" name="prescription_details[insurance_card]" value="S">

                    <input type="checkbox" class="do_not_use_insurance" name="prescription_details[dont_use_insurance]" value="Y">

                    <span class="slider round"></span>

                </label>

            </div>

        </div>

        <div class="ec_content">

            <div class="ec_top_check dont_use_insurance_text ec-imp-hidden">

                <span><?php echo $_smarty_tpl->__("close_dont_use_insurance_to_use_the_insurance_in_order");?>
</span>

            </div>

            <div id="emirate_is_insurance_block">

                <div class="ec_top_check">

                    <input type="checkbox" class="ec_insurance_linked_to_emirates" name="prescription_details[linked_to_emirates]" value="Y"> 

                    <span><?php echo $_smarty_tpl->__("ec_insurance_linked_to_emirates");?>
</span>

                </div>

                <div class="ec_checked_info_text hidden"><?php echo $_smarty_tpl->__("ec_insurance_linked_to_emirates_desc");?>
</div>

            </div> 

            <?php echo $_smarty_tpl->getSubTemplate ("addons/d_prescription/views/d_prescription/components/insurance_cards.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('show_next_button'=>false), 0);?>


            

        </div>

    </div>



    <div class="ec_my_account_delivery" id="ec_my_account_delivery">

        <div class="ec_header"><?php echo $_smarty_tpl->__("ec_my_addresses");?>
</div>

            <div class="ec_profile_container">

                <?php  $_smarty_tpl->tpl_vars['user_profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user_profile']->_loop = false;
 $_smarty_tpl->tpl_vars['profile_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['delivery_address']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["shipping_addesss"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['user_profile']->key => $_smarty_tpl->tpl_vars['user_profile']->value) {
$_smarty_tpl->tpl_vars['user_profile']->_loop = true;
 $_smarty_tpl->tpl_vars['profile_id']->value = $_smarty_tpl->tpl_vars['user_profile']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["shipping_addesss"]['iteration']++;
?>

                    <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(false, null, 0);?>

                    <?php if ($_smarty_tpl->tpl_vars['prescription_details']->value['selected_profile']==$_smarty_tpl->tpl_vars['profile_id']->value||$_smarty_tpl->getVariable('smarty')->value['foreach']['shipping_addesss']['iteration']==1) {?>

                        <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(true, null, 0);?>

                    <?php }?>

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('enable_address_select'=>true,'address_card_selected'=>$_smarty_tpl->tpl_vars['card_select']->value,'address_edit_page'=>"d_prescription.edit_delivery",'address_redirect_url'=>"d_prescription.upload_form",'address_result_ids'=>"ec_my_account_delivery,ec_prescription__required"), 0);?>


                <?php } ?>

            </div>

        <div class="ec_bottom_buttons">

            <a href="<?php echo htmlspecialchars(fn_url("d_prescription.edit_delivery"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render stop-cm-required" data-ca-target-id="ec_my_account_delivery,ec_prescription__required"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_address");?>
</a>

        </div>

    <!--ec_my_account_delivery--></div>



    <div class="ec_bottom_btn ty-float-right" style="margin-top:15px;">

        <?php $_smarty_tpl->tpl_vars['dispatch'] = new Smarty_variable("d_prescription.upload_form", null, 0);?>

        <a data-ca-dispatch="dispatch[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dispatch']->value, ENT_QUOTES, 'UTF-8');?>
]" class="cm-submit cm-no-ajax ty-btn ec_next_btn">

            <?php echo $_smarty_tpl->__("submit_the_order");?>


        </a>

    </div>

    <?php echo '<script'; ?>
>

        (function(_, $) {

            $.ceEvent('on', 'ce.commoninit', function(context){

                $(document).on('click', '.ec_prescription_top_div .ty-tabs__item', function(){

                    if($(this).attr("id") == 'ec_tab_authorised_carer'){

                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').show();

                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').find('.ty-control-group__title').each(function(){

                            $(this).addClass('cm-required');

                        });

                    } else {

                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').hide();

                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').find('.ty-control-group__title').each(function(){

                            $(this).removeClass('cm-required');

                        });

                    }

                });



                $(".do_not_use_insurance").change(function() {

                    if(this.checked) {

                        $('#ec_insurance_upload_block .ec_upload_form_blocks').hide();

                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'hidden');

                        $('.dont_use_insurance_text').removeClass('ec-imp-hidden');

                        $('.ec_insurance_linked_to_emirates').prop('checked', false);

                        $('#emirate_is_insurance_block').hide();

                    } else {

                        $('#ec_insurance_upload_block .ec_upload_form_blocks').show();

                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'visible');

                        $('.dont_use_insurance_text').addClass('ec-imp-hidden');

                        $('#emirate_is_insurance_block').show();

                    }

                });

                $(".ec_insurance_linked_to_emirates").change(function() {

                    if(this.checked) {

                        $('.ec_checked_info_text').show();

                        $('#ec_insurance_upload_block .ec_upload_form_blocks').hide();

                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'hidden');      

                    } else {

                        $('.ec_checked_info_text').hide();

                        $('#ec_insurance_upload_block .ec_upload_form_blocks').show();

                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'visible');      

                    }

                });

                

                $.ceEvent('on', 'ce.formpre_form_upload_prescription', function (form, clicked_elm) {

                    if(clicked_elm.attr('name') == "dispatch[d_prescription.upload_form]"){

                        var selected_shipping = $('[name="selected_profile"]:checked').val();

                        if(!selected_shipping){

                            $.ceNotification('show', {

                                type: 'E',

                                title: "<?php echo $_smarty_tpl->__("error");?>
",

                                message: "<?php echo $_smarty_tpl->__("add_or_select_address_to_continue");?>
"

                            });

                            return false;

                        }

                        var selected_emirate = $('[name="selected_emirate"]:checked').val();
                        if(!selected_emirate){

                            $.ceNotification('show', {
                                type: 'E',
                                title: "<?php echo $_smarty_tpl->__("error");?>
",
                                message: "<?php echo $_smarty_tpl->__("emirate_id_is_mandatory_to_upload");?>
"
                            });

                            return false;

                        }

                        var do_not_use_insurance = $('.do_not_use_insurance:checked').val();
                        if(do_not_use_insurance == 'undefined' || do_not_use_insurance == undefined){
                            console.log('do_not_use_insurance');
                            var linked_to_emirates = $('[name="prescription_details[linked_to_emirates]"]:checked').val();
                            if(linked_to_emirates  == 'undefined' || linked_to_emirates == undefined){
                                console.log('linked_to_emirates');
                                var selected_insurance = $('[name="selected_insurance"]').val();
                                if(!selected_insurance){
                                    $.ceNotification('show', {
                                        type: 'E',
                                        title: "<?php echo $_smarty_tpl->__("error");?>
",
                                        message: "<?php echo $_smarty_tpl->__("its_mandatory_to_upload_the_insurance_card");?>
"
                                    });

                                    return false;
                                }
                            }
                        }
                    }

                });
            });
            

            <?php if (!$_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
            $(document).ready(function(){
                $("#ec_tab_patient").hide();
                $("#ec_tab_authorised_carer").click();
            });
            <?php }?>
            $(document).ready(function(){
                $('[name="prescription_details[dont_use_insurance]"]').click();
            });
        }(Tygh, Tygh.$));

    <?php echo '</script'; ?>
>

</form>

</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_prescription/views/d_prescription/upload_form.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_prescription/views/d_prescription/upload_form.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_checkout_step_content" id="upload_prescription" style="margin-top: 15px;">

<?php $_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=checkout.checkout&ec_edit_step=ec_step_three", null, 0);?>



<form  name="form_upload_prescription" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">

    <input type="hidden" name="result_ids" value="checkout_steps" />

    <input type="hidden" name="ec_edit_step" value="ec_step_three" />

    <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />



    <div class="ec_prescription_top_div">

        <div class="ec_header"><i class="ec-icon-id"></i><span><?php echo $_smarty_tpl->__("ec_patient_info_prescription");?>
</span></div>

        <div class="ec_content">

            <?php echo smarty_function_script(array('src'=>"js/tygh/tabs.js"),$_smarty_tpl);?>


            <div class="ty-tabs cm-j-tabs cm-j-tabs-disable-convertation clearfix">

                <ul class="ty-tabs__list" <?php if ($_smarty_tpl->tpl_vars['tabs_section']->value) {?>id="tabs_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tabs_section']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>>

                    <li id="ec_tab_patient" class="ty-tabs__item cm-js"><a class="ty-tabs__a"><i class="ec-icon-union"></i><?php echo $_smarty_tpl->__("ec_i_am_patient");?>
</a></li>

                    <li id="ec_tab_authorised_carer" class="ty-tabs__item cm-js"><a class="ty-tabs__a"><i class="ec-icon-union"></i><?php echo $_smarty_tpl->__("ec_authorised_carer");?>
</a></li>

                </ul>

            </div>

            <div class="cm-tabs-content ty-tabs__content clearfix" id="tabs_content">

                <div id="content_ec_tab_patient11" class="">

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_prescription/views/d_prescription/components/patient_tab.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


                </div>

            </div>

        </div>

    </div>



    <div class="ec_prescription_bottom_div">

        <div class="ec_header">

            <div class="ec_left">

                <i class="ec-icon-id"></i><span><?php echo $_smarty_tpl->__("ec_patient_insurance_cards");?>
</span>

            </div>

            <div class="ec_right">

                <span><?php echo $_smarty_tpl->__("do_not_use_insurance");?>
</span>

                <label class="switch">

                    <input type="hidden" name="prescription_details[insurance_card]" value="S">

                    <input type="checkbox" class="do_not_use_insurance" name="prescription_details[dont_use_insurance]" value="Y">

                    <span class="slider round"></span>

                </label>

            </div>

        </div>

        <div class="ec_content">

            <div class="ec_top_check dont_use_insurance_text ec-imp-hidden">

                <span><?php echo $_smarty_tpl->__("close_dont_use_insurance_to_use_the_insurance_in_order");?>
</span>

            </div>

            <div id="emirate_is_insurance_block">

                <div class="ec_top_check">

                    <input type="checkbox" class="ec_insurance_linked_to_emirates" name="prescription_details[linked_to_emirates]" value="Y"> 

                    <span><?php echo $_smarty_tpl->__("ec_insurance_linked_to_emirates");?>
</span>

                </div>

                <div class="ec_checked_info_text hidden"><?php echo $_smarty_tpl->__("ec_insurance_linked_to_emirates_desc");?>
</div>

            </div> 

            <?php echo $_smarty_tpl->getSubTemplate ("addons/d_prescription/views/d_prescription/components/insurance_cards.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('show_next_button'=>false), 0);?>


            

        </div>

    </div>



    <div class="ec_my_account_delivery" id="ec_my_account_delivery">

        <div class="ec_header"><?php echo $_smarty_tpl->__("ec_my_addresses");?>
</div>

            <div class="ec_profile_container">

                <?php  $_smarty_tpl->tpl_vars['user_profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user_profile']->_loop = false;
 $_smarty_tpl->tpl_vars['profile_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['delivery_address']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["shipping_addesss"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['user_profile']->key => $_smarty_tpl->tpl_vars['user_profile']->value) {
$_smarty_tpl->tpl_vars['user_profile']->_loop = true;
 $_smarty_tpl->tpl_vars['profile_id']->value = $_smarty_tpl->tpl_vars['user_profile']->key;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["shipping_addesss"]['iteration']++;
?>

                    <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(false, null, 0);?>

                    <?php if ($_smarty_tpl->tpl_vars['prescription_details']->value['selected_profile']==$_smarty_tpl->tpl_vars['profile_id']->value||$_smarty_tpl->getVariable('smarty')->value['foreach']['shipping_addesss']['iteration']==1) {?>

                        <?php $_smarty_tpl->tpl_vars['card_select'] = new Smarty_variable(true, null, 0);?>

                    <?php }?>

                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('enable_address_select'=>true,'address_card_selected'=>$_smarty_tpl->tpl_vars['card_select']->value,'address_edit_page'=>"d_prescription.edit_delivery",'address_redirect_url'=>"d_prescription.upload_form",'address_result_ids'=>"ec_my_account_delivery,ec_prescription__required"), 0);?>


                <?php } ?>

            </div>

        <div class="ec_bottom_buttons">

            <a href="<?php echo htmlspecialchars(fn_url("d_prescription.edit_delivery"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render stop-cm-required" data-ca-target-id="ec_my_account_delivery,ec_prescription__required"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_address");?>
</a>

        </div>

    <!--ec_my_account_delivery--></div>



    <div class="ec_bottom_btn ty-float-right" style="margin-top:15px;">

        <?php $_smarty_tpl->tpl_vars['dispatch'] = new Smarty_variable("d_prescription.upload_form", null, 0);?>

        <a data-ca-dispatch="dispatch[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dispatch']->value, ENT_QUOTES, 'UTF-8');?>
]" class="cm-submit cm-no-ajax ty-btn ec_next_btn">

            <?php echo $_smarty_tpl->__("submit_the_order");?>


        </a>

    </div>

    <?php echo '<script'; ?>
>

        (function(_, $) {

            $.ceEvent('on', 'ce.commoninit', function(context){

                $(document).on('click', '.ec_prescription_top_div .ty-tabs__item', function(){

                    if($(this).attr("id") == 'ec_tab_authorised_carer'){

                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').show();

                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').find('.ty-control-group__title').each(function(){

                            $(this).addClass('cm-required');

                        });

                    } else {

                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').hide();

                        $(this).parent().parent().next('.ty-tabs__content').find('.ec_authorised_carer_tab_content').find('.ty-control-group__title').each(function(){

                            $(this).removeClass('cm-required');

                        });

                    }

                });



                $(".do_not_use_insurance").change(function() {

                    if(this.checked) {

                        $('#ec_insurance_upload_block .ec_upload_form_blocks').hide();

                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'hidden');

                        $('.dont_use_insurance_text').removeClass('ec-imp-hidden');

                        $('.ec_insurance_linked_to_emirates').prop('checked', false);

                        $('#emirate_is_insurance_block').hide();

                    } else {

                        $('#ec_insurance_upload_block .ec_upload_form_blocks').show();

                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'visible');

                        $('.dont_use_insurance_text').addClass('ec-imp-hidden');

                        $('#emirate_is_insurance_block').show();

                    }

                });

                $(".ec_insurance_linked_to_emirates").change(function() {

                    if(this.checked) {

                        $('.ec_checked_info_text').show();

                        $('#ec_insurance_upload_block .ec_upload_form_blocks').hide();

                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'hidden');      

                    } else {

                        $('.ec_checked_info_text').hide();

                        $('#ec_insurance_upload_block .ec_upload_form_blocks').show();

                        $('#ec_insurance_upload_block .ec_add_card_btn').css('visibility', 'visible');      

                    }

                });

                

                $.ceEvent('on', 'ce.formpre_form_upload_prescription', function (form, clicked_elm) {

                    if(clicked_elm.attr('name') == "dispatch[d_prescription.upload_form]"){

                        var selected_shipping = $('[name="selected_profile"]:checked').val();

                        if(!selected_shipping){

                            $.ceNotification('show', {

                                type: 'E',

                                title: "<?php echo $_smarty_tpl->__("error");?>
",

                                message: "<?php echo $_smarty_tpl->__("add_or_select_address_to_continue");?>
"

                            });

                            return false;

                        }

                        var selected_emirate = $('[name="selected_emirate"]:checked').val();
                        if(!selected_emirate){

                            $.ceNotification('show', {
                                type: 'E',
                                title: "<?php echo $_smarty_tpl->__("error");?>
",
                                message: "<?php echo $_smarty_tpl->__("emirate_id_is_mandatory_to_upload");?>
"
                            });

                            return false;

                        }

                        var do_not_use_insurance = $('.do_not_use_insurance:checked').val();
                        if(do_not_use_insurance == 'undefined' || do_not_use_insurance == undefined){
                            console.log('do_not_use_insurance');
                            var linked_to_emirates = $('[name="prescription_details[linked_to_emirates]"]:checked').val();
                            if(linked_to_emirates  == 'undefined' || linked_to_emirates == undefined){
                                console.log('linked_to_emirates');
                                var selected_insurance = $('[name="selected_insurance"]').val();
                                if(!selected_insurance){
                                    $.ceNotification('show', {
                                        type: 'E',
                                        title: "<?php echo $_smarty_tpl->__("error");?>
",
                                        message: "<?php echo $_smarty_tpl->__("its_mandatory_to_upload_the_insurance_card");?>
"
                                    });

                                    return false;
                                }
                            }
                        }
                    }

                });
            });
            

            <?php if (!$_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
            $(document).ready(function(){
                $("#ec_tab_patient").hide();
                $("#ec_tab_authorised_carer").click();
            });
            <?php }?>
            $(document).ready(function(){
                $('[name="prescription_details[dont_use_insurance]"]').click();
            });
        }(Tygh, Tygh.$));

    <?php echo '</script'; ?>
>

</form>

</div><?php }?><?php }} ?>
