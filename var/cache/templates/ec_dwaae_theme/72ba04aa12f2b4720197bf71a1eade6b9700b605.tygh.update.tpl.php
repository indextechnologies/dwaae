<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 14:50:31
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/profiles/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7223693006214bff7cf9726-59076720%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '72ba04aa12f2b4720197bf71a1eade6b9700b605' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/profiles/update.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '7223693006214bff7cf9726-59076720',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'settings' => 0,
    'config' => 0,
    'image_verification' => 0,
    'dispatch' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214bff7d45183_92664047',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214bff7d45183_92664047')) {function content_6214bff7d45183_92664047($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('register_new_account','register_new_account'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php $_smarty_tpl->tpl_vars['dispatch'] = new Smarty_variable("profiles.update", null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['runtime']->value['action']) {?>
    <?php $_smarty_tpl->tpl_vars['dispatch'] = new Smarty_variable("profiles.update.".((string)$_smarty_tpl->tpl_vars['runtime']->value['action']), null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['runtime']->value['mode']=="add"&&$_smarty_tpl->tpl_vars['settings']->value['General']['quick_registration']=="Y") {?>
    <div class="ec_top_form_logo">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/new_logo.png">
    </div>
    <div class="ty-account">

        <form name="profiles_register_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
            <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profile_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"C",'nothing_extra'=>"Y"), 0);?>

            <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_account.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('nothing_extra'=>"Y",'location'=>"checkout"), 0);?>


            <?php if ($_REQUEST['return_url']) {?>
                <input type="hidden" name="return_url" value="<?php echo htmlspecialchars($_REQUEST['return_url'], ENT_QUOTES, 'UTF-8');?>
" />
            <?php }?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"profiles:account_update")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"profiles:account_update"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"profiles:account_update"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            <?php $_smarty_tpl->tpl_vars["image_verification"] = new Smarty_variable($_smarty_tpl->getSubTemplate ("common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"register",'align'=>"left"), 0));?>

            <?php if ($_smarty_tpl->tpl_vars['image_verification']->value) {?>
            <div class="ty-control-group">
                <?php echo $_smarty_tpl->tpl_vars['image_verification']->value;?>

            </div>
            <?php }?>

            <div class="ty-profile-field__buttons buttons-container">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/register_profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[".((string)$_smarty_tpl->tpl_vars['dispatch']->value)."]"), 0);?>

            </div>
        </form>
    </div>
    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("register_new_account");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php } else { ?>
    <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/ec_update_profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    

    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/profiles/update.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/profiles/update.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php $_smarty_tpl->tpl_vars['dispatch'] = new Smarty_variable("profiles.update", null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['runtime']->value['action']) {?>
    <?php $_smarty_tpl->tpl_vars['dispatch'] = new Smarty_variable("profiles.update.".((string)$_smarty_tpl->tpl_vars['runtime']->value['action']), null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['runtime']->value['mode']=="add"&&$_smarty_tpl->tpl_vars['settings']->value['General']['quick_registration']=="Y") {?>
    <div class="ec_top_form_logo">
        <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_location'], ENT_QUOTES, 'UTF-8');?>
/images/ec_dwaae_new/new_logo.png">
    </div>
    <div class="ty-account">

        <form name="profiles_register_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
            <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profile_fields.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('section'=>"C",'nothing_extra'=>"Y"), 0);?>

            <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/components/profiles_account.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('nothing_extra'=>"Y",'location'=>"checkout"), 0);?>


            <?php if ($_REQUEST['return_url']) {?>
                <input type="hidden" name="return_url" value="<?php echo htmlspecialchars($_REQUEST['return_url'], ENT_QUOTES, 'UTF-8');?>
" />
            <?php }?>

            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"profiles:account_update")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"profiles:account_update"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"profiles:account_update"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


            <?php $_smarty_tpl->tpl_vars["image_verification"] = new Smarty_variable($_smarty_tpl->getSubTemplate ("common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"register",'align'=>"left"), 0));?>

            <?php if ($_smarty_tpl->tpl_vars['image_verification']->value) {?>
            <div class="ty-control-group">
                <?php echo $_smarty_tpl->tpl_vars['image_verification']->value;?>

            </div>
            <?php }?>

            <div class="ty-profile-field__buttons buttons-container">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/register_profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[".((string)$_smarty_tpl->tpl_vars['dispatch']->value)."]"), 0);?>

            </div>
        </form>
    </div>
    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("register_new_account");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php } else { ?>
    <?php echo $_smarty_tpl->getSubTemplate ("views/profiles/ec_update_profile.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    

    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?>
<?php }?><?php }} ?>
