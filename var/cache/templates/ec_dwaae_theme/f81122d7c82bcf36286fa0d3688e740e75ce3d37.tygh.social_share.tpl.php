<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:56
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/social_share.tpl" */ ?>
<?php /*%%SmartyHeaderCode:40306564262149cac24dc42-22986114%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f81122d7c82bcf36286fa0d3688e740e75ce3d37' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/social_share.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '40306564262149cac24dc42-22986114',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'config' => 0,
    'url' => 0,
    'product' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149cac25f850_97521407',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149cac25f850_97521407')) {function content_62149cac25f850_97521407($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_share_product','ec_share_product'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_social_share_container">
    <?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable(fn_url(((string)$_smarty_tpl->tpl_vars['config']->value['current_url'])), null, 0);?>
    <div class="ec_header"><?php echo $_smarty_tpl->__("ec_share_product");?>
</div>
    <div class="ec_icon_container">
        <a href="https://plus.google.com/share?url=<?php echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['url']->value), ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="Google+">
            <span class="ec-icon-mail"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
        </a>

        <a href="http://www.facebook.com/sharer.php?u=<?php echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['url']->value), ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="Facebook">
            <span class="ec-icon-facebook-c"><span class="path1"></span><span class="path2"></span></span>
        </a>

        <a href="http://twitter.com/share?text=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
&url=<?php echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['url']->value), ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="Twitter">
            <span class="ec-icon-twitter-1"></span>
        </a>
        <a href="http://pinterest.com/pin/create/button/?url=<?php echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['url']->value), ENT_QUOTES, 'UTF-8');?>
&description=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="Pinterest">
            <span class="ec-icon-pinterest"></span>
        </a>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/social_share.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/social_share.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_social_share_container">
    <?php $_smarty_tpl->tpl_vars['url'] = new Smarty_variable(fn_url(((string)$_smarty_tpl->tpl_vars['config']->value['current_url'])), null, 0);?>
    <div class="ec_header"><?php echo $_smarty_tpl->__("ec_share_product");?>
</div>
    <div class="ec_icon_container">
        <a href="https://plus.google.com/share?url=<?php echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['url']->value), ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="Google+">
            <span class="ec-icon-mail"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></span>
        </a>

        <a href="http://www.facebook.com/sharer.php?u=<?php echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['url']->value), ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="Facebook">
            <span class="ec-icon-facebook-c"><span class="path1"></span><span class="path2"></span></span>
        </a>

        <a href="http://twitter.com/share?text=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
&url=<?php echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['url']->value), ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="Twitter">
            <span class="ec-icon-twitter-1"></span>
        </a>
        <a href="http://pinterest.com/pin/create/button/?url=<?php echo htmlspecialchars(urlencode($_smarty_tpl->tpl_vars['url']->value), ENT_QUOTES, 'UTF-8');?>
&description=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="Pinterest">
            <span class="ec-icon-pinterest"></span>
        </a>
    </div>
</div><?php }?><?php }} ?>
