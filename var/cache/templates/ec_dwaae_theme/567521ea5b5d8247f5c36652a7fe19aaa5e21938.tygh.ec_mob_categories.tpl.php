<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:19
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/ec_dwaae_categories/ec_mob_categories.tpl" */ ?>
<?php /*%%SmartyHeaderCode:46041272162149c87022110-78209414%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '567521ea5b5d8247f5c36652a7fe19aaa5e21938' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/ec_dwaae_categories/ec_mob_categories.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '46041272162149c87022110-78209414',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'block' => 0,
    'auth' => 0,
    'user_info' => 0,
    'items' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149c8704f4e0_57679691',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149c8704f4e0_57679691')) {function content_62149c8704f4e0_57679691($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_hello','guest','categories','location','account','ec_pharmacies','ec_register_your_pharmacy','ec_services_and_benifits','ec_required_documents','ec_regulations_conditions','ec_register_your_pharmacy','ec_hello','guest','categories','location','account','ec_pharmacies','ec_register_your_pharmacy','ec_services_and_benifits','ec_required_documents','ec_regulations_conditions','ec_register_your_pharmacy'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>

<?php $_smarty_tpl->tpl_vars["dropdown_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
<?php $_smarty_tpl->_capture_stack[0][] = array("title", null, null); ob_start(); ?>
    <span class="ec_mob_cat_title">
        <i class="ec_all_cat_icon"></i>
    </span>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<div class="ec_mob_side_content">
    <div class="ec_top_info">
        <div class="ec_top_div">
            <?php echo $_smarty_tpl->getSubTemplate ("blocks/static_templates/ec_mob_logo.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <span class="sideviewer-close"><i class="ec-icon-close"></i></span>
        </div>
        <span class="ec_user_welcome">
            <?php echo $_smarty_tpl->__("ec_hello");?>
,
            <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
                <?php if ($_smarty_tpl->tpl_vars['user_info']->value['firstname']) {?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['firstname'], ENT_QUOTES, 'UTF-8');?>

                <?php } elseif ($_smarty_tpl->tpl_vars['user_info']->value['lastname']) {?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['lastname'], ENT_QUOTES, 'UTF-8');?>

                <?php }?>
            <?php } else { ?>
                <?php echo $_smarty_tpl->__("guest");?>

            <?php }?>
        </span>

        <?php echo smarty_function_script(array('src'=>"js/tygh/tabs.js"),$_smarty_tpl);?>

        <div class="ty-tabs cm-j-tabs cm-j-tabs-disable-convertation clearfix">
            <ul class="ty-tabs__list">
                <li id="ec_tab_cat" class="ty-tabs__item cm-js">
                    <a class="ty-tabs__a">
                        <span class="ec-icon-cat_all"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>                    
                    </a>
                    <div class="ec_sub_text"><?php echo $_smarty_tpl->__("categories");?>
</div>
                </li>
                <li id="ec_tab_loc" class="ty-tabs__item cm-js">
                    <a class="ty-tabs__a">
                        <i class="ec-icon-location"></i>
                    </a>
                    <div class="ec_sub_text"><?php echo $_smarty_tpl->__("location");?>
</div>
                </li>
                <li id="ec_tab_acc" class="ty-tabs__item cm-js">
                    <a class="ty-tabs__a">
                        <span class="ec-icon-profile_details"><span class="path1"></span><span class="path2"></span><span class="path3"></span>
                    </a>
                    <div class="ec_sub_text"><?php echo $_smarty_tpl->__("account");?>
</div>
                </li>
                <li id="ec_tab_phar" class="ty-tabs__item cm-js">
                    <a class="ty-tabs__a">
                        <i class="ec-icon-pharmacy"></i>
                    </a>
                    <div class="ec_sub_text"><?php echo $_smarty_tpl->__("ec_pharmacies");?>
</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="cm-tabs-content ty-tabs__content clearfix" id="tabs_content">
        <div id="content_ec_tab_cat">
            <?php echo $_smarty_tpl->getSubTemplate ("blocks/ec_dwaae_categories/components/ec_category_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('items'=>$_smarty_tpl->tpl_vars['items']->value,'item1_url'=>true,'name'=>"category",'item_id'=>"param_id",'childs'=>"subcategories"), 0);?>
 
        </div>
        <div id="content_ec_tab_loc">
            <?php echo $_smarty_tpl->getSubTemplate ("blocks/languages.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        </div>
        <div id="content_ec_tab_acc">
            <?php echo $_smarty_tpl->getSubTemplate ("blocks/my_account.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('no_capture'=>true), 0);?>

        </div>
        <div id="content_ec_tab_phar">
            <ul>
                <li class="ec_list_item"><div><?php echo $_smarty_tpl->__("ec_register_your_pharmacy");?>
</div></li>
                <li class="ec_list_item"><a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_services_and_benifits");?>
</a></li>
                <li class="ec_list_item"><a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_required_documents");?>
</a></li>
                <li class="ec_list_item"><a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_regulations_conditions");?>
</a></li>
            </ul>
            <div class="ty-account-info__buttons buttons-container">
                <a href="<?php echo htmlspecialchars(fn_url("companies.apply_for_vendor"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__primary"><?php echo $_smarty_tpl->__("ec_register_your_pharmacy");?>
</a>
            </div>
        </div>
    </div>


</div>
    
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/ec_dwaae_categories/ec_mob_categories.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/ec_dwaae_categories/ec_mob_categories.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>

<?php $_smarty_tpl->tpl_vars["dropdown_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
<?php $_smarty_tpl->_capture_stack[0][] = array("title", null, null); ob_start(); ?>
    <span class="ec_mob_cat_title">
        <i class="ec_all_cat_icon"></i>
    </span>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<div class="ec_mob_side_content">
    <div class="ec_top_info">
        <div class="ec_top_div">
            <?php echo $_smarty_tpl->getSubTemplate ("blocks/static_templates/ec_mob_logo.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <span class="sideviewer-close"><i class="ec-icon-close"></i></span>
        </div>
        <span class="ec_user_welcome">
            <?php echo $_smarty_tpl->__("ec_hello");?>
,
            <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
                <?php if ($_smarty_tpl->tpl_vars['user_info']->value['firstname']) {?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['firstname'], ENT_QUOTES, 'UTF-8');?>

                <?php } elseif ($_smarty_tpl->tpl_vars['user_info']->value['lastname']) {?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['lastname'], ENT_QUOTES, 'UTF-8');?>

                <?php }?>
            <?php } else { ?>
                <?php echo $_smarty_tpl->__("guest");?>

            <?php }?>
        </span>

        <?php echo smarty_function_script(array('src'=>"js/tygh/tabs.js"),$_smarty_tpl);?>

        <div class="ty-tabs cm-j-tabs cm-j-tabs-disable-convertation clearfix">
            <ul class="ty-tabs__list">
                <li id="ec_tab_cat" class="ty-tabs__item cm-js">
                    <a class="ty-tabs__a">
                        <span class="ec-icon-cat_all"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span></span>                    
                    </a>
                    <div class="ec_sub_text"><?php echo $_smarty_tpl->__("categories");?>
</div>
                </li>
                <li id="ec_tab_loc" class="ty-tabs__item cm-js">
                    <a class="ty-tabs__a">
                        <i class="ec-icon-location"></i>
                    </a>
                    <div class="ec_sub_text"><?php echo $_smarty_tpl->__("location");?>
</div>
                </li>
                <li id="ec_tab_acc" class="ty-tabs__item cm-js">
                    <a class="ty-tabs__a">
                        <span class="ec-icon-profile_details"><span class="path1"></span><span class="path2"></span><span class="path3"></span>
                    </a>
                    <div class="ec_sub_text"><?php echo $_smarty_tpl->__("account");?>
</div>
                </li>
                <li id="ec_tab_phar" class="ty-tabs__item cm-js">
                    <a class="ty-tabs__a">
                        <i class="ec-icon-pharmacy"></i>
                    </a>
                    <div class="ec_sub_text"><?php echo $_smarty_tpl->__("ec_pharmacies");?>
</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="cm-tabs-content ty-tabs__content clearfix" id="tabs_content">
        <div id="content_ec_tab_cat">
            <?php echo $_smarty_tpl->getSubTemplate ("blocks/ec_dwaae_categories/components/ec_category_menu.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('items'=>$_smarty_tpl->tpl_vars['items']->value,'item1_url'=>true,'name'=>"category",'item_id'=>"param_id",'childs'=>"subcategories"), 0);?>
 
        </div>
        <div id="content_ec_tab_loc">
            <?php echo $_smarty_tpl->getSubTemplate ("blocks/languages.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        </div>
        <div id="content_ec_tab_acc">
            <?php echo $_smarty_tpl->getSubTemplate ("blocks/my_account.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('no_capture'=>true), 0);?>

        </div>
        <div id="content_ec_tab_phar">
            <ul>
                <li class="ec_list_item"><div><?php echo $_smarty_tpl->__("ec_register_your_pharmacy");?>
</div></li>
                <li class="ec_list_item"><a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_services_and_benifits");?>
</a></li>
                <li class="ec_list_item"><a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_required_documents");?>
</a></li>
                <li class="ec_list_item"><a href="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_regulations_conditions");?>
</a></li>
            </ul>
            <div class="ty-account-info__buttons buttons-container">
                <a href="<?php echo htmlspecialchars(fn_url("companies.apply_for_vendor"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ty-btn__primary"><?php echo $_smarty_tpl->__("ec_register_your_pharmacy");?>
</a>
            </div>
        </div>
    </div>


</div>
    
<?php }?><?php }} ?>
