<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:20
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/hooks/index/scripts.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:192488783362149c8806d3e3-95000851%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35db246117f2e49df8795f2408140e76c278ace5' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_dwaae_new/hooks/index/scripts.post.tpl',
      1 => 1615798427,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '192488783362149c8806d3e3-95000851',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149c880974d9_01025984',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149c880974d9_01025984')) {function content_62149c880974d9_01025984($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('filter','filter'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo smarty_function_script(array('src'=>"js/addons/ec_dwaae_new/sitemapstyler.js"),$_smarty_tpl);?>

<?php ob_start();
echo htmlspecialchars(uniqid(), ENT_QUOTES, 'UTF-8');
$_tmp2=ob_get_clean();?><?php echo smarty_function_script(array('src'=>"js/addons/ec_google_pin_address/func.js?".$_tmp2),$_smarty_tpl);?>


<?php echo '<script'; ?>
>
    (function(_, $) {
        <?php if ($_REQUEST['from_app']=='Y') {?>
            $('.header-grid').remove();
            $('.tygh-top-panel').remove();
            $('.ec_app_bottom').remove();
            $('#tygh_footer').remove();
            $('.ty-footer-grid').remove();
            $(document).on('click', 'a', function(e){
                e.preventDefault();
            });
            $('iframe').remove();
        <?php }?>

        $.ceEvent('on', 'ce.commoninit', function(context) {
            setTimeout(function(){
                grid_max = 0;
                img_max = 0;
                $(".ec_grid_list .ty-column4").each(function(){
                    c_height = parseInt($(this).find('.ec-grid-list__item').outerHeight());
                    if (c_height > grid_max) {
                        grid_max = c_height;
                    }
                    img_height = parseInt($(this).find('.ec-grid-list__image img').outerHeight());
                    if (img_height > img_max) {
                        img_max = img_height;
                    }
                });
                if(grid_max != 0){
                    if($(window).width() > 600){
                        $(".ec_grid_list .ty-column4").find('.ec-grid-list__item').outerHeight(grid_max);
                    }
                }
                if(img_max != 0){
                    $(".ec_grid_list .ty-column4").find('.ty-no-image').outerHeight(img_max);
                }
            }, 1000);

             setTimeout(function(){
                grid_max = 0;
                img_max = 0;
                $(".ec_grid_list .ec-column").each(function(){
                    c_height = parseInt($(this).find('.ec-grid-list__item').outerHeight());
                    if (c_height > grid_max) {
                        grid_max = c_height;
                    }
                    img_height = parseInt($(this).find('.ec-grid-list__image img').outerHeight());
                    if (img_height > img_max) {
                        img_max = img_height;
                    }
                });
                if(grid_max != 0){
                    if($(window).width() > 600){
                        $(".ec_grid_list .ec-column").find('.ec-grid-list__item').outerHeight(grid_max);
                    }
                }
                if(img_max != 0){
                    $(".ec_grid_list .ec-column").find('.ty-no-image').outerHeight(img_max);
                }
            }, 2000);
            //$(document).find('.ec_services_div .ty-dropdown-box__content').removeClass('hidden');
            //$(document).find('.ec_services_div .ty-dropdown-box__title').removeClass('cm-combination');


            $(document).on('click', '.ec_announcement', function(){
                $('.ec_close_service').remove();
                $('.ec_hidden_video .banners').append('<div class="ec_close_service"></div>');
                $('.ec_announcement').toggleClass('active');
                $('.ec_services_div').toggleClass('ec_opened');
                $('.ec_hidden_video').toggleClass("ec_display");
                $('.container-fluid.content-grid').toggleClass("ec_display");                
            });
            $(document).on('click', '.ec_close_service', function(){
                $('.ec_announcement').click();
            });

            $(document).on('change', '.cm-value-changer select', function() {
                if($(this).val() == '9'){
                    $(this).prev().show();
                    $(this).prop('disabled', 'disabled');
                    $(this).hide();
                } else {
                    $('.ty-btn--recalculate-cart').click();
                }
            });
            
            $('.ec_lang_location_grid .ty-filter-products-by-geolocation-popup__item').removeClass('cm-dialog-opener');
            $(document).on('click', '.ec_lang_location_grid .ty-filter-products-by-geolocation-popup__item', function(e){
                e.preventDefault();
                $('.top-languages .ty-select-block__a').click();
            });
        });

         // slider menu //

        $('.fn-sideviewer').on('click', '.fn-sideviewer__toggle', function(e) {
            $(this).siblings(".fn-sideviewer-wrapper").addClass('sideviewer-open');
            $('body, html').addClass('sv-open');
            e.preventDefault();
        });

        $('.fn-sideviewer').on('click', '.sideviewer-overlay', function(e) {
            $(this).prev(".sideviewer-open").removeClass('sideviewer-open');
            $('body, html').removeClass('sv-open');
        });

        $('.fn-sideviewer').on('click', ".sideviewer-close", function(e) {
            $(this).parents(".sideviewer-open").removeClass('sideviewer-open');
            $('body, html').removeClass('sv-open');
        });

        // lang location mobile sidebar//
        if($(window).width() < 980){
            $('.ec_card_profile .labl>input:checked+div .ec_header_row').next('.ec_profile_content').slideToggle(100);
            $('.ec_card_profile .labl>input:checked+div .ec_header_row').toggleClass('active');
            $(document).on('click', '.ec_card_profile .ec_header_row', function(e){
                if (!($(e.target).hasClass('ec_right_content') || $(e.target).hasClass('edit_address') || $(e.target).hasClass('ec_delete_div'))) {
                    $(this).next('.ec_profile_content').slideToggle(100);
                    $(this).toggleClass('active');
                }
            });
            $(document).on('click', '.ec_mob_filter', function(){
                $('.ec_filters .ty-sidebox-important__title').append('<i class="ec-icon-close ec_filter_close"></i>');
                $('.ec_filters .ty-sidebox__title-wrapper').html('<div class="ec_mob_filter"><span><?php echo $_smarty_tpl->__("filter");?>
</span><i class="ec-icon-filter"></i></div>');
                $('.side-grid .ec_filters').show();
            });
            $(document).on('click', '.ec_mob_sort', function(){
                $('.ec_sort').show();
                $('.ec_sort_mob_title').show();
                $('.ec_sort_mob_heading').addClass('active');
            });
            $(document).on('click', '.ec_filter_close', function(){
                $('.ec_filters .ec_filter_close').remove();
                $('.side-grid .ec_filters').hide();
            });

            $(document).on('click', '.ec_sort_mob_heading .ec-icon-close', function(){
                $('.ec_sort').hide();
                $('.ec_sort_mob_title').hide();
                $('.ec_sort_mob_heading').removeClass('active');
            });

            $(document).on('click', '.ec_app_seach_btn', function(){
                $('.ec_app_search').addClass('show');
                $('body').addClass("search_open");
            });
            $(document).on('click', '.ec_search_close_btn', function(){
                $('.ec_app_search').removeClass('show');
                $('body').removeClass("search_open");
            });

            $(document).on('click', '.ec_category_bottom_btn', function(e){
                e.preventDefault();
                $('.ec_app_category_opener').click();
            });

            $(document).on('click', '.ec_services_bottom_btn', function(e){
                e.preventDefault();
                $('.ec_app_services_opener').click();
            });

            $(document).on('click', '.ec_account_bottom_btn', function(e){
                e.preventDefault();
                $('.ec_app_account_opener').click();
            });

            $(document).on('click', '.ec_track_app_btn', function(e){
                e.preventDefault();
                $('.ec_track_order_container').show();
            });
            $(document).on('click', '.ec_track_close', function(e){
                $('.ec_track_order_container').hide();
            });
        
        }
    }(Tygh, Tygh.$));

<?php echo '</script'; ?>
>

<?php if (@constant('EC_MOBILE_APP')) {?>
    <?php echo '<script'; ?>
>
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 50) {
                $('body').addClass('fixed-header');
            } else {
                $('body').removeClass('fixed-header');
            }
        });
        function removeParam(parameter)
        {
            var url=document.location.href;
            var urlparts= url.split('?');

            if (urlparts.length>=2)
            {
            var urlBase=urlparts.shift(); 
            var queryString=urlparts.join("?"); 

            var prefix = encodeURIComponent(parameter)+'=';
            var pars = queryString.split(/[&;]/g);
            for (var i= pars.length; i-->0;)               
                if (pars[i].lastIndexOf(prefix, 0)!==-1)   
                    pars.splice(i, 1);
            url = urlBase+'?'+pars.join('&');
            window.history.pushState('',document.title,url); // added this line to push the new url directly to url bar .

            }
            return url;
        }
        <?php if ($_smarty_tpl->tpl_vars['runtime']->value['controller']=='pages'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view'&&$_REQUEST['page_id']) {?>
            $('.header-grid').remove();
            $('.ec_app_bottom').remove();
            $(document).on('click', 'a', function(e){
                e.preventDefault();
            });
            removeParam('s_layout');
        <?php }?>
        $(document).on('click', 'a', function(e){
            var href = $(this).attr('href');
            if(typeof(href) !== 'undefined' && href.includes('mailto:')){
                e.preventDefault();
            }
        });
        function showSectionLoader() {
            $('body').prepend('<div id="load-overlay_pre"><span></span></div>');
        }
        $(document).on('click', '[name="dispatch[h_rfq.update.back]"]', function(){
            $(".cm-required").removeClass('cm-required');
        });
        $(document.body).on('click', "a[href]:not(.cm-dialog-opener, .cm-previewer, .fn-noloader, .fn-noloader > a, .cm-form-dialog-closer, .cm-ajax, .cm-combination, .cm-combination > a, .cm-new-window, .cm-no-ajax, .ty-product-options__image--wrapper, .ec_links):not([href^='mailto']):not([href^='#']):not([target='_blank'])", function(e) {
            e.preventDefault();
            showSectionLoader();
            var attr = $(this).attr('href');
            setTimeout(function() {
                if (typeof attr !== typeof undefined && attr !== false) {
                    location = attr;
                }
            }, 50);

            setTimeout(function() {
                if ($('#load-overlay_pre').length > 0) {
                    $('#load-overlay_pre').remove();
                }
            }, 12000);
        });

        $(window).load(function(){
            $('#load-overlay').fadeOut();
            $('#load-overlay_pre').remove();
        });
        (function(_, $) {
            $.ceEvent('on', 'ce.notificationshow', function(notification){
                container = $('.cm-notification-container');
                if(container.hasClass('ec_notification_pop')){
                    container.find(".cm-notification-content").each(function(i, el){
                        if ( i === 0) {
                            if(!$(this).hasClass('cm-notification-content-extended')){
                                $(this).addClass('ec-modal-container');
                                var alert_class = '';
                                if($(this).hasClass('alert-warning')){
                                    $(this).removeClass('alert-warning');
                                    alert_class="alert-warning";
                                }
                                if($(this).hasClass('alert-success')){
                                    $(this).removeClass('alert-success');
                                    alert_class="alert-success";
                                }
                                if($(this).hasClass('alert-error')){
                                    $(this).removeClass('alert-error');
                                    alert_class="alert-error";
                                }
                    
                                var title = $(this).find('strong').html();
                                $(this).find('strong').remove();
                                if($(this).find('.cm-notification-close').hasClass('cm-notification-close-ajax')){
                                    var ec_class = 'cm-notification-close-ajax';
                                    var dismiss = '';
                                } else {
                                    var ec_class = '';
                                    var dismiss = 'data-dismiss="alert"';
                                }
                                $(this).find('.cm-notification-close').remove();
                                var message = $(this).html();

                                var notice = '<div class="modal-background"><div class="modal"><h2 class="'+alert_class+'">'+title+'</h2><div>'+message+'</div> <button type="button" class="close cm-notification-close'+ec_class+'"'+dismiss+'>&times;</button>   </div></div>';

                                $(this).html(notice);
                            }
                        }
                    });
                }
            });

            $(document).on('click', '.ec_go_back', function(e){
                window.history.back();
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php }?>

<?php echo smarty_function_script(array('src'=>"js/lib/maskedinput/jquery.maskedinput.min.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/lib/inputmask/jquery.inputmask.min.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/lib/jquery-bind-first/jquery.bind-first-0.2.3.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/lib/inputmask-multi/jquery.inputmask-multi.js"),$_smarty_tpl);?>

<?php echo '<script'; ?>
 type="text/javascript">
    (function(_, $) {
        _.otp_phone_mask = '+\\971-99-999-9999'
    }(Tygh, Tygh.$));
    (function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        // var mask_elements = $('.cm-otp-mask-phone', context);
        var mask_elements = context.find('.s_phone, .b_phone, .phone, input[name="company_data[phone]"], input[name="user_data[phone]"], .cm-mask-phone,.cm-otp-mask-phone');
        if (mask_elements.length) {
            mask_elements.each(function() {
                console.log(_.otp_phone_mask);
                if (_.otp_phone_mask) {
                    $(this).inputmask({
                        mask: _.otp_phone_mask,
                        showMaskOnHover: true,
                        autoUnmask: true
                    });
                }
            });
        }
    });
})(Tygh, Tygh.$);
<?php echo '</script'; ?>
><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_dwaae_new/hooks/index/scripts.post.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_dwaae_new/hooks/index/scripts.post.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo smarty_function_script(array('src'=>"js/addons/ec_dwaae_new/sitemapstyler.js"),$_smarty_tpl);?>

<?php ob_start();
echo htmlspecialchars(uniqid(), ENT_QUOTES, 'UTF-8');
$_tmp3=ob_get_clean();?><?php echo smarty_function_script(array('src'=>"js/addons/ec_google_pin_address/func.js?".$_tmp3),$_smarty_tpl);?>


<?php echo '<script'; ?>
>
    (function(_, $) {
        <?php if ($_REQUEST['from_app']=='Y') {?>
            $('.header-grid').remove();
            $('.tygh-top-panel').remove();
            $('.ec_app_bottom').remove();
            $('#tygh_footer').remove();
            $('.ty-footer-grid').remove();
            $(document).on('click', 'a', function(e){
                e.preventDefault();
            });
            $('iframe').remove();
        <?php }?>

        $.ceEvent('on', 'ce.commoninit', function(context) {
            setTimeout(function(){
                grid_max = 0;
                img_max = 0;
                $(".ec_grid_list .ty-column4").each(function(){
                    c_height = parseInt($(this).find('.ec-grid-list__item').outerHeight());
                    if (c_height > grid_max) {
                        grid_max = c_height;
                    }
                    img_height = parseInt($(this).find('.ec-grid-list__image img').outerHeight());
                    if (img_height > img_max) {
                        img_max = img_height;
                    }
                });
                if(grid_max != 0){
                    if($(window).width() > 600){
                        $(".ec_grid_list .ty-column4").find('.ec-grid-list__item').outerHeight(grid_max);
                    }
                }
                if(img_max != 0){
                    $(".ec_grid_list .ty-column4").find('.ty-no-image').outerHeight(img_max);
                }
            }, 1000);

             setTimeout(function(){
                grid_max = 0;
                img_max = 0;
                $(".ec_grid_list .ec-column").each(function(){
                    c_height = parseInt($(this).find('.ec-grid-list__item').outerHeight());
                    if (c_height > grid_max) {
                        grid_max = c_height;
                    }
                    img_height = parseInt($(this).find('.ec-grid-list__image img').outerHeight());
                    if (img_height > img_max) {
                        img_max = img_height;
                    }
                });
                if(grid_max != 0){
                    if($(window).width() > 600){
                        $(".ec_grid_list .ec-column").find('.ec-grid-list__item').outerHeight(grid_max);
                    }
                }
                if(img_max != 0){
                    $(".ec_grid_list .ec-column").find('.ty-no-image').outerHeight(img_max);
                }
            }, 2000);
            //$(document).find('.ec_services_div .ty-dropdown-box__content').removeClass('hidden');
            //$(document).find('.ec_services_div .ty-dropdown-box__title').removeClass('cm-combination');


            $(document).on('click', '.ec_announcement', function(){
                $('.ec_close_service').remove();
                $('.ec_hidden_video .banners').append('<div class="ec_close_service"></div>');
                $('.ec_announcement').toggleClass('active');
                $('.ec_services_div').toggleClass('ec_opened');
                $('.ec_hidden_video').toggleClass("ec_display");
                $('.container-fluid.content-grid').toggleClass("ec_display");                
            });
            $(document).on('click', '.ec_close_service', function(){
                $('.ec_announcement').click();
            });

            $(document).on('change', '.cm-value-changer select', function() {
                if($(this).val() == '9'){
                    $(this).prev().show();
                    $(this).prop('disabled', 'disabled');
                    $(this).hide();
                } else {
                    $('.ty-btn--recalculate-cart').click();
                }
            });
            
            $('.ec_lang_location_grid .ty-filter-products-by-geolocation-popup__item').removeClass('cm-dialog-opener');
            $(document).on('click', '.ec_lang_location_grid .ty-filter-products-by-geolocation-popup__item', function(e){
                e.preventDefault();
                $('.top-languages .ty-select-block__a').click();
            });
        });

         // slider menu //

        $('.fn-sideviewer').on('click', '.fn-sideviewer__toggle', function(e) {
            $(this).siblings(".fn-sideviewer-wrapper").addClass('sideviewer-open');
            $('body, html').addClass('sv-open');
            e.preventDefault();
        });

        $('.fn-sideviewer').on('click', '.sideviewer-overlay', function(e) {
            $(this).prev(".sideviewer-open").removeClass('sideviewer-open');
            $('body, html').removeClass('sv-open');
        });

        $('.fn-sideviewer').on('click', ".sideviewer-close", function(e) {
            $(this).parents(".sideviewer-open").removeClass('sideviewer-open');
            $('body, html').removeClass('sv-open');
        });

        // lang location mobile sidebar//
        if($(window).width() < 980){
            $('.ec_card_profile .labl>input:checked+div .ec_header_row').next('.ec_profile_content').slideToggle(100);
            $('.ec_card_profile .labl>input:checked+div .ec_header_row').toggleClass('active');
            $(document).on('click', '.ec_card_profile .ec_header_row', function(e){
                if (!($(e.target).hasClass('ec_right_content') || $(e.target).hasClass('edit_address') || $(e.target).hasClass('ec_delete_div'))) {
                    $(this).next('.ec_profile_content').slideToggle(100);
                    $(this).toggleClass('active');
                }
            });
            $(document).on('click', '.ec_mob_filter', function(){
                $('.ec_filters .ty-sidebox-important__title').append('<i class="ec-icon-close ec_filter_close"></i>');
                $('.ec_filters .ty-sidebox__title-wrapper').html('<div class="ec_mob_filter"><span><?php echo $_smarty_tpl->__("filter");?>
</span><i class="ec-icon-filter"></i></div>');
                $('.side-grid .ec_filters').show();
            });
            $(document).on('click', '.ec_mob_sort', function(){
                $('.ec_sort').show();
                $('.ec_sort_mob_title').show();
                $('.ec_sort_mob_heading').addClass('active');
            });
            $(document).on('click', '.ec_filter_close', function(){
                $('.ec_filters .ec_filter_close').remove();
                $('.side-grid .ec_filters').hide();
            });

            $(document).on('click', '.ec_sort_mob_heading .ec-icon-close', function(){
                $('.ec_sort').hide();
                $('.ec_sort_mob_title').hide();
                $('.ec_sort_mob_heading').removeClass('active');
            });

            $(document).on('click', '.ec_app_seach_btn', function(){
                $('.ec_app_search').addClass('show');
                $('body').addClass("search_open");
            });
            $(document).on('click', '.ec_search_close_btn', function(){
                $('.ec_app_search').removeClass('show');
                $('body').removeClass("search_open");
            });

            $(document).on('click', '.ec_category_bottom_btn', function(e){
                e.preventDefault();
                $('.ec_app_category_opener').click();
            });

            $(document).on('click', '.ec_services_bottom_btn', function(e){
                e.preventDefault();
                $('.ec_app_services_opener').click();
            });

            $(document).on('click', '.ec_account_bottom_btn', function(e){
                e.preventDefault();
                $('.ec_app_account_opener').click();
            });

            $(document).on('click', '.ec_track_app_btn', function(e){
                e.preventDefault();
                $('.ec_track_order_container').show();
            });
            $(document).on('click', '.ec_track_close', function(e){
                $('.ec_track_order_container').hide();
            });
        
        }
    }(Tygh, Tygh.$));

<?php echo '</script'; ?>
>

<?php if (@constant('EC_MOBILE_APP')) {?>
    <?php echo '<script'; ?>
>
        $(window).scroll(function() {
            if ($(window).scrollTop() >= 50) {
                $('body').addClass('fixed-header');
            } else {
                $('body').removeClass('fixed-header');
            }
        });
        function removeParam(parameter)
        {
            var url=document.location.href;
            var urlparts= url.split('?');

            if (urlparts.length>=2)
            {
            var urlBase=urlparts.shift(); 
            var queryString=urlparts.join("?"); 

            var prefix = encodeURIComponent(parameter)+'=';
            var pars = queryString.split(/[&;]/g);
            for (var i= pars.length; i-->0;)               
                if (pars[i].lastIndexOf(prefix, 0)!==-1)   
                    pars.splice(i, 1);
            url = urlBase+'?'+pars.join('&');
            window.history.pushState('',document.title,url); // added this line to push the new url directly to url bar .

            }
            return url;
        }
        <?php if ($_smarty_tpl->tpl_vars['runtime']->value['controller']=='pages'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view'&&$_REQUEST['page_id']) {?>
            $('.header-grid').remove();
            $('.ec_app_bottom').remove();
            $(document).on('click', 'a', function(e){
                e.preventDefault();
            });
            removeParam('s_layout');
        <?php }?>
        $(document).on('click', 'a', function(e){
            var href = $(this).attr('href');
            if(typeof(href) !== 'undefined' && href.includes('mailto:')){
                e.preventDefault();
            }
        });
        function showSectionLoader() {
            $('body').prepend('<div id="load-overlay_pre"><span></span></div>');
        }
        $(document).on('click', '[name="dispatch[h_rfq.update.back]"]', function(){
            $(".cm-required").removeClass('cm-required');
        });
        $(document.body).on('click', "a[href]:not(.cm-dialog-opener, .cm-previewer, .fn-noloader, .fn-noloader > a, .cm-form-dialog-closer, .cm-ajax, .cm-combination, .cm-combination > a, .cm-new-window, .cm-no-ajax, .ty-product-options__image--wrapper, .ec_links):not([href^='mailto']):not([href^='#']):not([target='_blank'])", function(e) {
            e.preventDefault();
            showSectionLoader();
            var attr = $(this).attr('href');
            setTimeout(function() {
                if (typeof attr !== typeof undefined && attr !== false) {
                    location = attr;
                }
            }, 50);

            setTimeout(function() {
                if ($('#load-overlay_pre').length > 0) {
                    $('#load-overlay_pre').remove();
                }
            }, 12000);
        });

        $(window).load(function(){
            $('#load-overlay').fadeOut();
            $('#load-overlay_pre').remove();
        });
        (function(_, $) {
            $.ceEvent('on', 'ce.notificationshow', function(notification){
                container = $('.cm-notification-container');
                if(container.hasClass('ec_notification_pop')){
                    container.find(".cm-notification-content").each(function(i, el){
                        if ( i === 0) {
                            if(!$(this).hasClass('cm-notification-content-extended')){
                                $(this).addClass('ec-modal-container');
                                var alert_class = '';
                                if($(this).hasClass('alert-warning')){
                                    $(this).removeClass('alert-warning');
                                    alert_class="alert-warning";
                                }
                                if($(this).hasClass('alert-success')){
                                    $(this).removeClass('alert-success');
                                    alert_class="alert-success";
                                }
                                if($(this).hasClass('alert-error')){
                                    $(this).removeClass('alert-error');
                                    alert_class="alert-error";
                                }
                    
                                var title = $(this).find('strong').html();
                                $(this).find('strong').remove();
                                if($(this).find('.cm-notification-close').hasClass('cm-notification-close-ajax')){
                                    var ec_class = 'cm-notification-close-ajax';
                                    var dismiss = '';
                                } else {
                                    var ec_class = '';
                                    var dismiss = 'data-dismiss="alert"';
                                }
                                $(this).find('.cm-notification-close').remove();
                                var message = $(this).html();

                                var notice = '<div class="modal-background"><div class="modal"><h2 class="'+alert_class+'">'+title+'</h2><div>'+message+'</div> <button type="button" class="close cm-notification-close'+ec_class+'"'+dismiss+'>&times;</button>   </div></div>';

                                $(this).html(notice);
                            }
                        }
                    });
                }
            });

            $(document).on('click', '.ec_go_back', function(e){
                window.history.back();
            });
        }(Tygh, Tygh.$));
    <?php echo '</script'; ?>
>
<?php }?>

<?php echo smarty_function_script(array('src'=>"js/lib/maskedinput/jquery.maskedinput.min.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/lib/inputmask/jquery.inputmask.min.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/lib/jquery-bind-first/jquery.bind-first-0.2.3.js"),$_smarty_tpl);?>

<?php echo smarty_function_script(array('src'=>"js/lib/inputmask-multi/jquery.inputmask-multi.js"),$_smarty_tpl);?>

<?php echo '<script'; ?>
 type="text/javascript">
    (function(_, $) {
        _.otp_phone_mask = '+\\971-99-999-9999'
    }(Tygh, Tygh.$));
    (function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        // var mask_elements = $('.cm-otp-mask-phone', context);
        var mask_elements = context.find('.s_phone, .b_phone, .phone, input[name="company_data[phone]"], input[name="user_data[phone]"], .cm-mask-phone,.cm-otp-mask-phone');
        if (mask_elements.length) {
            mask_elements.each(function() {
                console.log(_.otp_phone_mask);
                if (_.otp_phone_mask) {
                    $(this).inputmask({
                        mask: _.otp_phone_mask,
                        showMaskOnHover: true,
                        autoUnmask: true
                    });
                }
            });
        }
    });
})(Tygh, Tygh.$);
<?php echo '</script'; ?>
><?php }?><?php }} ?>
