<?php /* Smarty version Smarty-3.1.21, created on 2022-03-02 17:54:28
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/delivery_address/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1065631272621f771450ca63-01846527%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '715507a275e7446985c9891f5ce2580f3c6ee073' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/delivery_address/manage.tpl',
      1 => 1609308449,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1065631272621f771450ca63-01846527',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'redirect_url' => 0,
    'delivery_address' => 0,
    'user_profile' => 0,
    'enable_address_select' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621f7714552011_90777359',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621f7714552011_90777359')) {function content_621f7714552011_90777359($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_my_addresses','add_new_address','done','ec_my_addresses','add_new_address','done'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=delivery_address.manage", null, 0);?>
<div class="ec_my_account_delivery" id="ec_my_account_delivery">
    <div class="ec_header"><?php echo $_smarty_tpl->__("ec_my_addresses");?>
</div>
    <form name="ec_deliver_address_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
        <input type="hidden" name="result_ids" value="ec_my_account_delivery" />
        <input type="hidden" name="ec_edit_step" value="ec_step_two" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <div class="ec_profile_container">
            <?php  $_smarty_tpl->tpl_vars['user_profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user_profile']->_loop = false;
 $_smarty_tpl->tpl_vars['profile_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['delivery_address']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user_profile']->key => $_smarty_tpl->tpl_vars['user_profile']->value) {
$_smarty_tpl->tpl_vars['user_profile']->_loop = true;
 $_smarty_tpl->tpl_vars['profile_id']->value = $_smarty_tpl->tpl_vars['user_profile']->key;
?>
                <?php $_smarty_tpl->tpl_vars['enable_address_select'] = new Smarty_variable(false, null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['is_default']=="Y") {?>
                    <?php $_smarty_tpl->tpl_vars['enable_address_select'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('enable_address_select'=>$_smarty_tpl->tpl_vars['enable_address_select']->value,'address_card_selected'=>$_smarty_tpl->tpl_vars['enable_address_select']->value,'address_edit_page'=>"delivery_address.update",'address_redirect_url'=>"delivery_address.manage",'address_result_ids'=>"ec_my_account_delivery"), 0);?>

            <?php } ?>
        </div>
        <div class="ec_bottom_buttons">
            <a href="<?php echo htmlspecialchars(fn_url("delivery_address.update"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render stop-cm-required" data-ca-target-id="ec_my_account_delivery"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_address");?>
</a>

            <a href="<?php echo htmlspecialchars(fn_url("my_account.manage"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_next_btn"><?php echo $_smarty_tpl->__("done");?>
</a>
        </div>
    </form>
<!--ec_my_account_delivery--></div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_customer/views/delivery_address/manage.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_customer/views/delivery_address/manage.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars['redirect_url'] = new Smarty_variable("index.php?dispatch=delivery_address.manage", null, 0);?>
<div class="ec_my_account_delivery" id="ec_my_account_delivery">
    <div class="ec_header"><?php echo $_smarty_tpl->__("ec_my_addresses");?>
</div>
    <form name="ec_deliver_address_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
        <input type="hidden" name="result_ids" value="ec_my_account_delivery" />
        <input type="hidden" name="ec_edit_step" value="ec_step_two" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['redirect_url']->value, ENT_QUOTES, 'UTF-8');?>
" />
        <div class="ec_profile_container">
            <?php  $_smarty_tpl->tpl_vars['user_profile'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user_profile']->_loop = false;
 $_smarty_tpl->tpl_vars['profile_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['delivery_address']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user_profile']->key => $_smarty_tpl->tpl_vars['user_profile']->value) {
$_smarty_tpl->tpl_vars['user_profile']->_loop = true;
 $_smarty_tpl->tpl_vars['profile_id']->value = $_smarty_tpl->tpl_vars['user_profile']->key;
?>
                <?php $_smarty_tpl->tpl_vars['enable_address_select'] = new Smarty_variable(false, null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['user_profile']->value['is_default']=="Y") {?>
                    <?php $_smarty_tpl->tpl_vars['enable_address_select'] = new Smarty_variable(true, null, 0);?>
                <?php }?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/delivery_address/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('enable_address_select'=>$_smarty_tpl->tpl_vars['enable_address_select']->value,'address_card_selected'=>$_smarty_tpl->tpl_vars['enable_address_select']->value,'address_edit_page'=>"delivery_address.update",'address_redirect_url'=>"delivery_address.manage",'address_result_ids'=>"ec_my_account_delivery"), 0);?>

            <?php } ?>
        </div>
        <div class="ec_bottom_buttons">
            <a href="<?php echo htmlspecialchars(fn_url("delivery_address.update"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_btn_icon_green cm-ajax cm-ajax-full-render stop-cm-required" data-ca-target-id="ec_my_account_delivery"><i class="ec-icon-add"></i><?php echo $_smarty_tpl->__("add_new_address");?>
</a>

            <a href="<?php echo htmlspecialchars(fn_url("my_account.manage"), ENT_QUOTES, 'UTF-8');?>
" class="ty-btn ec_next_btn"><?php echo $_smarty_tpl->__("done");?>
</a>
        </div>
    </form>
<!--ec_my_account_delivery--></div><?php }?><?php }} ?>
