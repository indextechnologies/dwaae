<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 19:21:11
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/sitemap/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13630815646214ff675660f1-25670364%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '58dd0823fba3a687b0d763b879fb70b01dbbc1b6' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/sitemap/view.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '13630815646214ff675660f1-25670364',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'language_direction' => 0,
    'sitemap_settings' => 0,
    'sitemap' => 0,
    'direction' => 0,
    'name' => 0,
    'section' => 0,
    'link' => 0,
    'category' => 0,
    'cat_tree' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214ff675df5f0_26259640',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214ff675df5f0_26259640')) {function content_6214ff675df5f0_26259640($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('cart_info','information','catalog','sitemap','cart_info','information','catalog','sitemap'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['language_direction']->value=="rtl") {?>
    <?php $_smarty_tpl->tpl_vars['direction'] = new Smarty_variable("right", null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars['direction'] = new Smarty_variable("left", null, 0);?>
<?php }?>

<div class="ty-sitemap">
    <div class="ty-sitemap__section">
        <h2 class="ty-sitemap__section-title"><?php echo $_smarty_tpl->__("cart_info");?>
</h2>
        <div class="ty-sitemap__section-wrapper">
            <?php if ($_smarty_tpl->tpl_vars['sitemap_settings']->value['show_site_info']=="Y") {?>
                <ul id="sitemap">
                    <li><a href="#"><?php echo $_smarty_tpl->__("information");?>
</a>
                        <ul>
                            <?php echo $_smarty_tpl->getSubTemplate ("views/sitemap/components/page_tree.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tree'=>$_smarty_tpl->tpl_vars['sitemap']->value['pages_tree'],'root'=>true,'no_delim'=>true,'direction'=>$_smarty_tpl->tpl_vars['direction']->value), 0);?>

                        </ul>
                    </li>
                    <?php  $_smarty_tpl->tpl_vars['section'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['section']->_loop = false;
 $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['sitemap']->value['custom']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['section']->key => $_smarty_tpl->tpl_vars['section']->value) {
$_smarty_tpl->tpl_vars['section']->_loop = true;
 $_smarty_tpl->tpl_vars['name']->value = $_smarty_tpl->tpl_vars['section']->key;
?>
                    <li><a href="#"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                        <ul>
                            <?php  $_smarty_tpl->tpl_vars['link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['link']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['section']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['link']->key => $_smarty_tpl->tpl_vars['link']->value) {
$_smarty_tpl->tpl_vars['link']->_loop = true;
?>
                                <li><a href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['link']->value['link_href']), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value['link'], ENT_QUOTES, 'UTF-8');?>
</a></li>
                            <?php } ?>
                        </ul>					
                    </li>
                    <?php } ?>
                </ul>
            <?php }?>
            <i class="ec-icon-sitemap_1"></i>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="ty-sitemap__section">
        <h2 class="ty-sitemap__section-title"><?php echo $_smarty_tpl->__("catalog");?>
</h2>
        <div class="ty-sitemap__tree">
            <?php if ($_smarty_tpl->tpl_vars['sitemap']->value['categories']||$_smarty_tpl->tpl_vars['sitemap']->value['categories_tree']) {?>
                <?php if ($_smarty_tpl->tpl_vars['sitemap']->value['categories']) {?>
                    <ul id="sitemap_2">
                        <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sitemap']->value['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
                            <li><a class="ty-sitemap__tree-list-a ty-strong" href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a></li>
                        <?php } ?>
                    </ul>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['sitemap']->value['categories_tree']) {?>
                    <?php $_smarty_tpl->tpl_vars['cat_tree'] = new Smarty_variable(fn_ec_create_category_tree($_smarty_tpl->tpl_vars['sitemap']->value['categories_tree']), null, 0);?>

                    <?php echo $_smarty_tpl->getSubTemplate ("views/sitemap/components/categories_tree.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('all_categories_tree'=>$_smarty_tpl->tpl_vars['cat_tree']->value), 0);?>

                <?php }?>
            <?php }?>
            <i class="ec-icon-sitemap_2"></i>

        </div>
    </div>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("sitemap");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/sitemap/view.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/sitemap/view.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['language_direction']->value=="rtl") {?>
    <?php $_smarty_tpl->tpl_vars['direction'] = new Smarty_variable("right", null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars['direction'] = new Smarty_variable("left", null, 0);?>
<?php }?>

<div class="ty-sitemap">
    <div class="ty-sitemap__section">
        <h2 class="ty-sitemap__section-title"><?php echo $_smarty_tpl->__("cart_info");?>
</h2>
        <div class="ty-sitemap__section-wrapper">
            <?php if ($_smarty_tpl->tpl_vars['sitemap_settings']->value['show_site_info']=="Y") {?>
                <ul id="sitemap">
                    <li><a href="#"><?php echo $_smarty_tpl->__("information");?>
</a>
                        <ul>
                            <?php echo $_smarty_tpl->getSubTemplate ("views/sitemap/components/page_tree.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tree'=>$_smarty_tpl->tpl_vars['sitemap']->value['pages_tree'],'root'=>true,'no_delim'=>true,'direction'=>$_smarty_tpl->tpl_vars['direction']->value), 0);?>

                        </ul>
                    </li>
                    <?php  $_smarty_tpl->tpl_vars['section'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['section']->_loop = false;
 $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['sitemap']->value['custom']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['section']->key => $_smarty_tpl->tpl_vars['section']->value) {
$_smarty_tpl->tpl_vars['section']->_loop = true;
 $_smarty_tpl->tpl_vars['name']->value = $_smarty_tpl->tpl_vars['section']->key;
?>
                    <li><a href="#"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                        <ul>
                            <?php  $_smarty_tpl->tpl_vars['link'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['link']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['section']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['link']->key => $_smarty_tpl->tpl_vars['link']->value) {
$_smarty_tpl->tpl_vars['link']->_loop = true;
?>
                                <li><a href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['link']->value['link_href']), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value['link'], ENT_QUOTES, 'UTF-8');?>
</a></li>
                            <?php } ?>
                        </ul>					
                    </li>
                    <?php } ?>
                </ul>
            <?php }?>
            <i class="ec-icon-sitemap_1"></i>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="ty-sitemap__section">
        <h2 class="ty-sitemap__section-title"><?php echo $_smarty_tpl->__("catalog");?>
</h2>
        <div class="ty-sitemap__tree">
            <?php if ($_smarty_tpl->tpl_vars['sitemap']->value['categories']||$_smarty_tpl->tpl_vars['sitemap']->value['categories_tree']) {?>
                <?php if ($_smarty_tpl->tpl_vars['sitemap']->value['categories']) {?>
                    <ul id="sitemap_2">
                        <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['sitemap']->value['categories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
                            <li><a class="ty-sitemap__tree-list-a ty-strong" href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a></li>
                        <?php } ?>
                    </ul>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['sitemap']->value['categories_tree']) {?>
                    <?php $_smarty_tpl->tpl_vars['cat_tree'] = new Smarty_variable(fn_ec_create_category_tree($_smarty_tpl->tpl_vars['sitemap']->value['categories_tree']), null, 0);?>

                    <?php echo $_smarty_tpl->getSubTemplate ("views/sitemap/components/categories_tree.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('all_categories_tree'=>$_smarty_tpl->tpl_vars['cat_tree']->value), 0);?>

                <?php }?>
            <?php }?>
            <i class="ec-icon-sitemap_2"></i>

        </div>
    </div>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("sitemap");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
