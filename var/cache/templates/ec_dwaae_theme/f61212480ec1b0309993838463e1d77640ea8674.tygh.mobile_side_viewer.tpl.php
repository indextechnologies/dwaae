<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:19
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/wrappers/mobile_side_viewer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:138982175862149c87452773-95778379%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f61212480ec1b0309993838463e1d77640ea8674' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/wrappers/mobile_side_viewer.tpl',
      1 => 1605863527,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '138982175862149c87452773-95778379',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'content' => 0,
    'block' => 0,
    'content_alignment' => 0,
    'header_class' => 0,
    'title' => 0,
    'sideviewer_id' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149c8746d605_40110783',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149c8746d605_40110783')) {function content_62149c8746d605_40110783($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if (trim($_smarty_tpl->tpl_vars['content']->value)) {?>
    <?php $_smarty_tpl->tpl_vars["sideviewer_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
<div class="<?php if ($_smarty_tpl->tpl_vars['block']->value['user_class']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['user_class'], ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['content_alignment']->value=="RIGHT") {?> ty-float-right<?php } elseif ($_smarty_tpl->tpl_vars['content_alignment']->value=="LEFT") {?> ty-float-left<?php }?>">
    <div class="fn-sideviewer">
    	<div class="fn-sideviewer__toggle <?php if ($_smarty_tpl->tpl_vars['header_class']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['header_class']->value, ENT_QUOTES, 'UTF-8');?>
 fn-noloader<?php }?>">
			<?php if (trim(Smarty::$_smarty_vars['capture']['title'])) {?>
				<?php echo Smarty::$_smarty_vars['capture']['title'];?>

			<?php } else { ?>
				<a class="fn-noloader"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</a>
			<?php }?>
		</div>
		<div id="sideviewer_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sideviewer_id']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" class="fn-sideviewer-wrapper">
			<div class="side-viewer-content">
				<div class="sideviewer-content-wrapper"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['content']->value)===null||$tmp==='' ? "&nbsp;" : $tmp);?>
</div>
			</div>
		</div>	
		<div class="sideviewer-overlay"></div>
	</div>
</div>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/wrappers/mobile_side_viewer.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/wrappers/mobile_side_viewer.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if (trim($_smarty_tpl->tpl_vars['content']->value)) {?>
    <?php $_smarty_tpl->tpl_vars["sideviewer_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
<div class="<?php if ($_smarty_tpl->tpl_vars['block']->value['user_class']) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['user_class'], ENT_QUOTES, 'UTF-8');
}
if ($_smarty_tpl->tpl_vars['content_alignment']->value=="RIGHT") {?> ty-float-right<?php } elseif ($_smarty_tpl->tpl_vars['content_alignment']->value=="LEFT") {?> ty-float-left<?php }?>">
    <div class="fn-sideviewer">
    	<div class="fn-sideviewer__toggle <?php if ($_smarty_tpl->tpl_vars['header_class']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['header_class']->value, ENT_QUOTES, 'UTF-8');?>
 fn-noloader<?php }?>">
			<?php if (trim(Smarty::$_smarty_vars['capture']['title'])) {?>
				<?php echo Smarty::$_smarty_vars['capture']['title'];?>

			<?php } else { ?>
				<a class="fn-noloader"><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</a>
			<?php }?>
		</div>
		<div id="sideviewer_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sideviewer_id']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: none;" class="fn-sideviewer-wrapper">
			<div class="side-viewer-content">
				<div class="sideviewer-content-wrapper"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['content']->value)===null||$tmp==='' ? "&nbsp;" : $tmp);?>
</div>
			</div>
		</div>	
		<div class="sideviewer-overlay"></div>
	</div>
</div>
<?php }
}?><?php }} ?>
