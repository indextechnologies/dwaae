<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 14:50:32
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_google_pin_address/hooks/profiles/profile_fields.override.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11818059406214bff80765a7-96656035%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '74210e40cb40f20b498988bc8bc6b1ddecaa8b49' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/ec_google_pin_address/hooks/profiles/profile_fields.override.tpl',
      1 => 1611834271,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '11818059406214bff80765a7-96656035',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'field' => 0,
    'id_prefix' => 0,
    'addons' => 0,
    'data_name' => 0,
    'data_id' => 0,
    'value' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214bff8098ba2_57055900',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214bff8098ba2_57055900')) {function content_6214bff8098ba2_57055900($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_google_pin_address.get_address','ec_google_pin_address.use_google_map','ec_google_pin_address.use_map','save','ec_google_pin_address.get_address','ec_google_pin_address.use_google_map','ec_google_pin_address.use_map','save'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="L"&&$_smarty_tpl->tpl_vars['field']->value['field_name']=="ec_lat") {?>
    <div class="ty-control-group">
        <label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
" class="cm-required hidden"><?php echo $_smarty_tpl->__("ec_google_pin_address.get_address");?>
</label>
        <div class="ec-google-pin-button">
            <a href="" id="google_map_btn_follow" class="cm-dialog-opener cm-dialog-auto-size ty-btn ty-btn__secondary" rel="nofollow" data-ca-target-id="ec_google_map_btn_follow" ><?php echo $_smarty_tpl->__("ec_google_pin_address.use_google_map");?>

                <?php echo '<?xml';?> version="1.0" encoding="UTF-8"<?php echo '?>';?>

                <svg style="vertical-align:bottom" width="14px" height="20px" viewBox="0 0 14 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <path d="M7,0 C3.13,0 0,3.13 0,7 C0,12.25 7,20 7,20 C7,20 14,12.25 14,7 C14,3.13 10.87,0 7,0 Z M7,9.5 C5.62,9.5 4.5,8.38 4.5,7 C4.5,5.62 5.62,4.5 7,4.5 C8.38,4.5 9.5,5.62 9.5,7 C9.5,8.38 8.38,9.5 7,9.5 Z" id="path-1"></path>
                    </defs>
                    <g id="icon/maps/place_24px" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <mask id="mask-2" fill="white">
                            <use xlink:href="#path-1"></use>
                        </mask>
                        <use fill="red" fill-rule="nonzero" xlink:href="#path-1"></use>
                    </g>
                </svg>
            </a>
        </div>
        <div id="ec_google_map_btn_follow" class="hidden" title="<?php echo $_smarty_tpl->__("ec_google_pin_address.use_map");?>
">
            <div class="ec_map_container">
                <div style="padding: 10px 0px;"><input id="pac-input" class="ty-input-text" type="text" placeholder="<?php echo $_smarty_tpl->__('ec_google_pin_address.search_placeholder');?>
" style="width: 100%;"></div>
                <div id="ec-google-map" style="height: 350px;" data-map-key="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['ec_google_pin_address']['ec_google_api_key'], ENT_QUOTES, 'UTF-8');?>
" class="cm-ec-geo-map-container" data-ca-lat="" data-ca-lng=""></div>
            </div>      
            <div class="buttons-container">
                <div class="ty-float-right">
                    <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_id'=>"ec_button_map",'but_text'=>$_smarty_tpl->__("save"),'but_role'=>"text",'but_meta'=>"cm-dialog-closer ty-btn ty-btn__secondary"), 0);?>

                </div>
            </div>
        </div> 
    </div>
    <input
        type="hidden"
        id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
"
        name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_id']->value, ENT_QUOTES, 'UTF-8');?>
]"
        size="32"
        value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"
        class="ty-input-text ec_lat_val" 
    />   
    
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="L"&&$_smarty_tpl->tpl_vars['field']->value['field_name']=="ec_lng") {?>
    <input
        type="hidden"
        id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
"
        name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_id']->value, ENT_QUOTES, 'UTF-8');?>
]"
        size="32"
        value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"
        class="ty-input-text ec_lng_val"
    />
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/ec_google_pin_address/hooks/profiles/profile_fields.override.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/ec_google_pin_address/hooks/profiles/profile_fields.override.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="L"&&$_smarty_tpl->tpl_vars['field']->value['field_name']=="ec_lat") {?>
    <div class="ty-control-group">
        <label for="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
" class="cm-required hidden"><?php echo $_smarty_tpl->__("ec_google_pin_address.get_address");?>
</label>
        <div class="ec-google-pin-button">
            <a href="" id="google_map_btn_follow" class="cm-dialog-opener cm-dialog-auto-size ty-btn ty-btn__secondary" rel="nofollow" data-ca-target-id="ec_google_map_btn_follow" ><?php echo $_smarty_tpl->__("ec_google_pin_address.use_google_map");?>

                <?php echo '<?xml';?> version="1.0" encoding="UTF-8"<?php echo '?>';?>

                <svg style="vertical-align:bottom" width="14px" height="20px" viewBox="0 0 14 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <path d="M7,0 C3.13,0 0,3.13 0,7 C0,12.25 7,20 7,20 C7,20 14,12.25 14,7 C14,3.13 10.87,0 7,0 Z M7,9.5 C5.62,9.5 4.5,8.38 4.5,7 C4.5,5.62 5.62,4.5 7,4.5 C8.38,4.5 9.5,5.62 9.5,7 C9.5,8.38 8.38,9.5 7,9.5 Z" id="path-1"></path>
                    </defs>
                    <g id="icon/maps/place_24px" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <mask id="mask-2" fill="white">
                            <use xlink:href="#path-1"></use>
                        </mask>
                        <use fill="red" fill-rule="nonzero" xlink:href="#path-1"></use>
                    </g>
                </svg>
            </a>
        </div>
        <div id="ec_google_map_btn_follow" class="hidden" title="<?php echo $_smarty_tpl->__("ec_google_pin_address.use_map");?>
">
            <div class="ec_map_container">
                <div style="padding: 10px 0px;"><input id="pac-input" class="ty-input-text" type="text" placeholder="<?php echo $_smarty_tpl->__('ec_google_pin_address.search_placeholder');?>
" style="width: 100%;"></div>
                <div id="ec-google-map" style="height: 350px;" data-map-key="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['ec_google_pin_address']['ec_google_api_key'], ENT_QUOTES, 'UTF-8');?>
" class="cm-ec-geo-map-container" data-ca-lat="" data-ca-lng=""></div>
            </div>      
            <div class="buttons-container">
                <div class="ty-float-right">
                    <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_id'=>"ec_button_map",'but_text'=>$_smarty_tpl->__("save"),'but_role'=>"text",'but_meta'=>"cm-dialog-closer ty-btn ty-btn__secondary"), 0);?>

                </div>
            </div>
        </div> 
    </div>
    <input
        type="hidden"
        id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
"
        name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_id']->value, ENT_QUOTES, 'UTF-8');?>
]"
        size="32"
        value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"
        class="ty-input-text ec_lat_val" 
    />   
    
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['field']->value['field_type']=="L"&&$_smarty_tpl->tpl_vars['field']->value['field_name']=="ec_lng") {?>
    <input
        type="hidden"
        id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
"
        name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'UTF-8');?>
[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_id']->value, ENT_QUOTES, 'UTF-8');?>
]"
        size="32"
        value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
"
        class="ty-input-text ec_lng_val"
    />
<?php }
}?><?php }} ?>
