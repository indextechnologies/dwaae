<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 15:19:58
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/delivery_address/components/map.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18868184916214c6de388a85-44430930%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6647101e0c565101bd91765398c9c06c0cd8aa01' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_customer/views/delivery_address/components/map.tpl',
      1 => 1612874554,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '18868184916214c6de388a85-44430930',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'addons' => 0,
    'id_prefix' => 0,
    'field' => 0,
    'selected_card' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214c6de3a1380_01600917',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214c6de3a1380_01600917')) {function content_6214c6de3a1380_01600917($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('update_location_from_map','error','it_mandatory_to_update_the_location_by_using_the_map','update_location_from_map','error','it_mandatory_to_update_the_location_by_using_the_map'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars['id_prefix'] = new Smarty_variable(uniqid(), null, 0);?>

    
        <div class="ec_map_container">
            <div style="padding: 10px 0px;"><input id="pac-input" class="ty-input-text" type="text" placeholder="<?php echo $_smarty_tpl->__('ec_google_pin_address.search_placeholder');?>
" style="width: 100%;"></div>
            <div id="ec-google-map" style="height: 350px;width:100%;" data-map-key="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['ec_google_pin_address']['ec_google_api_key'], ENT_QUOTES, 'UTF-8');?>
" class="cm-ec-geo-map-container" data-ca-lat="" data-ca-lng=""></div>
        </div>      
        <div class="buttons-container">
            
                
                <a class="ty-btn ty-btn ty-btn__secondary text-button " id="ec_button_map" style="width:100%"><?php echo $_smarty_tpl->__("update_location_from_map");?>
</a>
            
        </div>
    
<input
    type="hidden"
    id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
"
    name="delivery_address[ec_lat]"
    size="32"
    value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['ec_lat'], ENT_QUOTES, 'UTF-8');?>
"
    class="ty-input-text ec_lat_val" 
/>
<input
        type="hidden"
        id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_lng_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
"
        name="delivery_address[ec_lng]"
        size="32"
        value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['ec_lat'], ENT_QUOTES, 'UTF-8');?>
"
        class="ty-input-text ec_lng_val"
    />

        <?php echo '<script'; ?>
>

        (function(_, $) {

            $.ceEvent('on', 'ce.commoninit', function(context){
                $.ceEvent('on', 'ce.formpre_form_deilvery_card', function (form, clicked_elm) {
                    if(clicked_elm.attr('name') == "dispatch[delivery_address.update]"){
                        var ec_lat = $('[name="delivery_address[ec_lat]"]').val();
                        var ec_lng = $('[name="delivery_address[ec_lng]"]').val();
                        if(!(ec_lat && ec_lng)){
                            $.ceNotification('show', {
                                type: 'E',
                                title: "<?php echo $_smarty_tpl->__("error");?>
",
                                message: "<?php echo $_smarty_tpl->__("it_mandatory_to_update_the_location_by_using_the_map");?>
"
                            });

                            return false;
                        }
                    }
                });
            });
            }(Tygh, Tygh.$));
        <?php echo '</script'; ?>
><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_customer/views/delivery_address/components/map.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_customer/views/delivery_address/components/map.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars['id_prefix'] = new Smarty_variable(uniqid(), null, 0);?>

    
        <div class="ec_map_container">
            <div style="padding: 10px 0px;"><input id="pac-input" class="ty-input-text" type="text" placeholder="<?php echo $_smarty_tpl->__('ec_google_pin_address.search_placeholder');?>
" style="width: 100%;"></div>
            <div id="ec-google-map" style="height: 350px;width:100%;" data-map-key="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['addons']->value['ec_google_pin_address']['ec_google_api_key'], ENT_QUOTES, 'UTF-8');?>
" class="cm-ec-geo-map-container" data-ca-lat="" data-ca-lng=""></div>
        </div>      
        <div class="buttons-container">
            
                
                <a class="ty-btn ty-btn ty-btn__secondary text-button " id="ec_button_map" style="width:100%"><?php echo $_smarty_tpl->__("update_location_from_map");?>
</a>
            
        </div>
    
<input
    type="hidden"
    id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
"
    name="delivery_address[ec_lat]"
    size="32"
    value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['ec_lat'], ENT_QUOTES, 'UTF-8');?>
"
    class="ty-input-text ec_lat_val" 
/>
<input
        type="hidden"
        id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id_prefix']->value, ENT_QUOTES, 'UTF-8');?>
elm_lng_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value['field_id'], ENT_QUOTES, 'UTF-8');?>
"
        name="delivery_address[ec_lng]"
        size="32"
        value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected_card']->value['ec_lat'], ENT_QUOTES, 'UTF-8');?>
"
        class="ty-input-text ec_lng_val"
    />

        <?php echo '<script'; ?>
>

        (function(_, $) {

            $.ceEvent('on', 'ce.commoninit', function(context){
                $.ceEvent('on', 'ce.formpre_form_deilvery_card', function (form, clicked_elm) {
                    if(clicked_elm.attr('name') == "dispatch[delivery_address.update]"){
                        var ec_lat = $('[name="delivery_address[ec_lat]"]').val();
                        var ec_lng = $('[name="delivery_address[ec_lng]"]').val();
                        if(!(ec_lat && ec_lng)){
                            $.ceNotification('show', {
                                type: 'E',
                                title: "<?php echo $_smarty_tpl->__("error");?>
",
                                message: "<?php echo $_smarty_tpl->__("it_mandatory_to_update_the_location_by_using_the_map");?>
"
                            });

                            return false;
                        }
                    }
                });
            });
            }(Tygh, Tygh.$));
        <?php echo '</script'; ?>
><?php }?><?php }} ?>
