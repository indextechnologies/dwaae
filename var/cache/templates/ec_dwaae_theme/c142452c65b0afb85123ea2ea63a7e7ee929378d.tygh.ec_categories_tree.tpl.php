<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:18
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/ec_dwaae_categories/ec_categories_tree.tpl" */ ?>
<?php /*%%SmartyHeaderCode:147482615862149c86edaeb8-65540657%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c142452c65b0afb85123ea2ea63a7e7ee929378d' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/ec_dwaae_categories/ec_categories_tree.tpl',
      1 => 1609318062,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '147482615862149c86edaeb8-65540657',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'items' => 0,
    'block' => 0,
    'dropdown_id' => 0,
    'item1' => 0,
    'item1_url' => 0,
    'item2' => 0,
    'item_url2' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149c86f0e205_28304132',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149c86f0e205_28304132')) {function content_62149c86f0e205_28304132($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_count')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.count.php';
if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('back_to_main_menu','text_topmenu_view_more','back_to_main_menu','text_topmenu_view_more'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>

<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <?php $_smarty_tpl->tpl_vars["dropdown_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
    <div class="ec_category_conatiner">
        <a id="sw_dropdown_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ec-categories-header cm-combination open">
            <span class="ec_categories_title">
                <i class="ec_all_cat_icon"></i>
            </span>
        </a>
        <div id="dropdown_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ec_categories_tree_content">
            <div class="ec-categories js-categories">
                <div class="ec-categories__navigation js-selectbox__left-column">
                    <ul class="ec-categories__list ec-categories__list--lvl1 ec-selectbox__list">
                    
                        <?php  $_smarty_tpl->tpl_vars['item1'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item1']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item1']->key => $_smarty_tpl->tpl_vars['item1']->value) {
$_smarty_tpl->tpl_vars['item1']->_loop = true;
?>
                            <?php $_smarty_tpl->tpl_vars['item1_url'] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item1']->value,"categories"), null, 0);?>

                            <li
                                class="
                                    ec-categories__item
                                    ec-categories__item--lvl1
                                    ec-selectbox__item
                                    js-categories__item--lvl1
                                "
                            >
                                <span
                                    class="
                                        ec-categories__name
                                        ec-categories__name--lvl1
                                        ec-selectbox__option
                                        js-categories__name--lvl1
                                        js-selectbox__option
                                        js-selectbox__option--left
                                    "
                                    <?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>
                                    title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'UTF-8');?>
"
                                    tabindex="0"
                                >
                                    <i class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['icon_class'], ENT_QUOTES, 'UTF-8');?>
"></i>
                                    <span class="ec-selectbox__option-text">
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'UTF-8');?>
 <i class="ec-icon-right-arrow"></i>
                                    </span>
                                </span>

                                <div class="ec-categories__submenu js-categories__submenu">
                                    <div class="ec_back_to_main_menu"><i class="ty-icon-left-open-thin"></i><span><?php echo $_smarty_tpl->__("back_to_main_menu");?>
</span></div>
                                    <div class="catHeader">
                                        <p class="categoryTitle"><i class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['icon_class'], ENT_QUOTES, 'UTF-8');?>
"></i><a <?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>
                                    title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a></p>
                                        
                                        <i class="ec-icon-close ec_cat_close"></i>
                                    </div>
                                    <?php if ($_smarty_tpl->tpl_vars['item1']->value['subcategories']) {?>
                                    <ul
                                        class="
                                            ec-categories__list
                                            ec-categories__list--lvl2
                                            ec-categories__list--columns<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['properties']['number_of_columns'], ENT_QUOTES, 'UTF-8');?>

                                        "
                                    >
                                        <?php  $_smarty_tpl->tpl_vars['item2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item2']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item1']->value['subcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_sub_cat"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item2']->key => $_smarty_tpl->tpl_vars['item2']->value) {
$_smarty_tpl->tpl_vars['item2']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_sub_cat"]['iteration']++;
?>
                                            <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['ec_sub_cat']['iteration']<13) {?>
                                                <?php $_smarty_tpl->tpl_vars['item_url2'] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item2']->value,"categories"), null, 0);?>

                                                <li
                                                    class="
                                                        ec-categories__item
                                                        ec-categories__item--lvl2
                                                        ec-selectbox__item
                                                    "
                                                >
                                                    <a
                                                        class="
                                                            ec-categories__name
                                                            ec-categories__name--lvl2
                                                            ec-selectbox__option
                                                            js-selectbox__option
                                                            js-selectbox__option--right
                                                        "
                                                        <?php if ($_smarty_tpl->tpl_vars['item_url2']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item_url2']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>
                                                        tabindex="0" 
                                                    >
                                                        <span class="ec-selectbox__option-text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2']->value['category'], ENT_QUOTES, 'UTF-8');?>
</span>
                                                    </a>
                                                </li>
                                            <?php }?>
                                        <?php } ?>
                                        <?php if (smarty_modifier_count($_smarty_tpl->tpl_vars['item1']->value['subcategories'])>12) {?>
                                            <?php $_smarty_tpl->tpl_vars['item_url2'] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item2']->value,"categories"), null, 0);?>

                                            <li
                                                class="
                                                    ec-categories__item
                                                    ec-categories__item--lvl2
                                                    ec-selectbox__item
                                                "
                                            >
                                                <a
                                                    class="
                                                        ec-categories__name
                                                        ec-categories__name--lvl2
                                                        ec-selectbox__option
                                                        js-selectbox__option
                                                        js-selectbox__option--right
                                                    "
                                                    href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'UTF-8');?>
"
                                                    tabindex="0" 
                                                >
                                                    <span class="ec-selectbox__option-text"><?php echo $_smarty_tpl->__("text_topmenu_view_more");?>
</span>
                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                    <?php }?>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div
                    class="
                        ec-categories__submenu-container
                        js-selectbox__right-column
                        js-categories__submenu-container
                    "
                >
                </div>
                <div class="ec_overlay_div"></div>
            </div>
        </div>
    </div>
<?php }?>

<?php echo smarty_function_script(array('src'=>"js/addons/ec_dwaae_new/ec_categories.js"),$_smarty_tpl);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/ec_dwaae_categories/ec_categories_tree.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/ec_dwaae_categories/ec_categories_tree.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>

<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>
    <?php $_smarty_tpl->tpl_vars["dropdown_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
    <div class="ec_category_conatiner">
        <a id="sw_dropdown_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ec-categories-header cm-combination open">
            <span class="ec_categories_title">
                <i class="ec_all_cat_icon"></i>
            </span>
        </a>
        <div id="dropdown_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ec_categories_tree_content">
            <div class="ec-categories js-categories">
                <div class="ec-categories__navigation js-selectbox__left-column">
                    <ul class="ec-categories__list ec-categories__list--lvl1 ec-selectbox__list">
                    
                        <?php  $_smarty_tpl->tpl_vars['item1'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item1']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item1']->key => $_smarty_tpl->tpl_vars['item1']->value) {
$_smarty_tpl->tpl_vars['item1']->_loop = true;
?>
                            <?php $_smarty_tpl->tpl_vars['item1_url'] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item1']->value,"categories"), null, 0);?>

                            <li
                                class="
                                    ec-categories__item
                                    ec-categories__item--lvl1
                                    ec-selectbox__item
                                    js-categories__item--lvl1
                                "
                            >
                                <span
                                    class="
                                        ec-categories__name
                                        ec-categories__name--lvl1
                                        ec-selectbox__option
                                        js-categories__name--lvl1
                                        js-selectbox__option
                                        js-selectbox__option--left
                                    "
                                    <?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>
                                    title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'UTF-8');?>
"
                                    tabindex="0"
                                >
                                    <i class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['icon_class'], ENT_QUOTES, 'UTF-8');?>
"></i>
                                    <span class="ec-selectbox__option-text">
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'UTF-8');?>
 <i class="ec-icon-right-arrow"></i>
                                    </span>
                                </span>

                                <div class="ec-categories__submenu js-categories__submenu">
                                    <div class="ec_back_to_main_menu"><i class="ty-icon-left-open-thin"></i><span><?php echo $_smarty_tpl->__("back_to_main_menu");?>
</span></div>
                                    <div class="catHeader">
                                        <p class="categoryTitle"><i class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['icon_class'], ENT_QUOTES, 'UTF-8');?>
"></i><a <?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>
                                    title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'UTF-8');?>
</a></p>
                                        
                                        <i class="ec-icon-close ec_cat_close"></i>
                                    </div>
                                    <?php if ($_smarty_tpl->tpl_vars['item1']->value['subcategories']) {?>
                                    <ul
                                        class="
                                            ec-categories__list
                                            ec-categories__list--lvl2
                                            ec-categories__list--columns<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['properties']['number_of_columns'], ENT_QUOTES, 'UTF-8');?>

                                        "
                                    >
                                        <?php  $_smarty_tpl->tpl_vars['item2'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item2']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item1']->value['subcategories']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_sub_cat"]['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item2']->key => $_smarty_tpl->tpl_vars['item2']->value) {
$_smarty_tpl->tpl_vars['item2']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["ec_sub_cat"]['iteration']++;
?>
                                            <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['ec_sub_cat']['iteration']<13) {?>
                                                <?php $_smarty_tpl->tpl_vars['item_url2'] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item2']->value,"categories"), null, 0);?>

                                                <li
                                                    class="
                                                        ec-categories__item
                                                        ec-categories__item--lvl2
                                                        ec-selectbox__item
                                                    "
                                                >
                                                    <a
                                                        class="
                                                            ec-categories__name
                                                            ec-categories__name--lvl2
                                                            ec-selectbox__option
                                                            js-selectbox__option
                                                            js-selectbox__option--right
                                                        "
                                                        <?php if ($_smarty_tpl->tpl_vars['item_url2']->value) {?>href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item_url2']->value, ENT_QUOTES, 'UTF-8');?>
"<?php }?>
                                                        tabindex="0" 
                                                    >
                                                        <span class="ec-selectbox__option-text"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2']->value['category'], ENT_QUOTES, 'UTF-8');?>
</span>
                                                    </a>
                                                </li>
                                            <?php }?>
                                        <?php } ?>
                                        <?php if (smarty_modifier_count($_smarty_tpl->tpl_vars['item1']->value['subcategories'])>12) {?>
                                            <?php $_smarty_tpl->tpl_vars['item_url2'] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item2']->value,"categories"), null, 0);?>

                                            <li
                                                class="
                                                    ec-categories__item
                                                    ec-categories__item--lvl2
                                                    ec-selectbox__item
                                                "
                                            >
                                                <a
                                                    class="
                                                        ec-categories__name
                                                        ec-categories__name--lvl2
                                                        ec-selectbox__option
                                                        js-selectbox__option
                                                        js-selectbox__option--right
                                                    "
                                                    href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'UTF-8');?>
"
                                                    tabindex="0" 
                                                >
                                                    <span class="ec-selectbox__option-text"><?php echo $_smarty_tpl->__("text_topmenu_view_more");?>
</span>
                                                </a>
                                            </li>
                                        <?php }?>
                                    </ul>
                                    <?php }?>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div
                    class="
                        ec-categories__submenu-container
                        js-selectbox__right-column
                        js-categories__submenu-container
                    "
                >
                </div>
                <div class="ec_overlay_div"></div>
            </div>
        </div>
    </div>
<?php }?>

<?php echo smarty_function_script(array('src'=>"js/addons/ec_dwaae_new/ec_categories.js"),$_smarty_tpl);?>

<?php }?><?php }} ?>
