<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:19
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_services.tpl" */ ?>
<?php /*%%SmartyHeaderCode:168323926762149c87ac4bb5-23502027%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e3a736c8b56092fcf9cf302eb510b88c7f21bc9a' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_services.tpl',
      1 => 1612438082,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '168323926762149c87ac4bb5-23502027',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'block' => 0,
    'dropdown_id' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149c87adaf42_93926904',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149c87adaf42_93926904')) {function content_62149c87adaf42_93926904($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_services','gov_announcements','upload_prescription','247_pharmacies','quotation_request','ec_services','gov_announcements','upload_prescription','247_pharmacies','quotation_request'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_services_div">
    <?php $_smarty_tpl->tpl_vars["dropdown_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
    <div class="ty-dropdown-box" id="services_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
">
        <div id="sw_dropdown_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-dropdown-box__title cm-combination open">
            <span class="ec_services_header">
                <span><?php echo $_smarty_tpl->__("ec_services");?>
</span><i class="ec-icon-down-arrow"></i>
            </span>
        </div>
        <div id="dropdown_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-popup-box ty-dropdown-box__content">
            <div class="ec_servces_content">
                <div class="ec_service_block ec_announcement">
                    <div class="ec_notice_icon">
                        <i class="ec-icon-announcement"></i><span class="ec_count_announcement">0</span>
                    </div>
                    <div class="ec_notice_text"><?php echo $_smarty_tpl->__("gov_announcements");?>
</div>
                </div>

                
                
                <div class="ec_service_block">
                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.upload_form"), ENT_QUOTES, 'UTF-8');?>
">
                    <div class="ec_notice_icon">
                        <i class="ec-icon-upload-prescription"></i>
                    </div>
                    <div class="ec_notice_text"><?php echo $_smarty_tpl->__("upload_prescription");?>
</div>
                    </a>
                </div>

                

                <div class="ec_service_block">
                    <a href="<?php echo htmlspecialchars(fn_url("companies.catalog&24_7_pharmacies=Y"), ENT_QUOTES, 'UTF-8');?>
">
                    <div class="ec_notice_icon">
                        <i class="ec-icon-pharmacy"></i>
                    </div>
                    <div class="ec_notice_text"><?php echo $_smarty_tpl->__("247_pharmacies");?>
</div>
                    </a>
                </div>

                <div class="ec_service_block">
                    <a href="<?php echo htmlspecialchars(fn_url("h_rfq.update"), ENT_QUOTES, 'UTF-8');?>
">
                    <div class="ec_notice_icon">
                        <i class="ec-icon-indications"></i>
                    </div>
                    <div class="ec_notice_text"><?php echo $_smarty_tpl->__("quotation_request");?>
</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo '<script'; ?>
>
    (function(_, $) {
        $.ceEvent('on', 'ce.commoninit', function(context) {
            setTimeout(function(){
                count = $('.ec_hidden_video').find('.owl-wrapper .owl-item').length;
                $('.ec_count_announcement').text(count);
            }, 1000);
        });
    }(Tygh, Tygh.$));

<?php echo '</script'; ?>
><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/static_templates/ec_services.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/static_templates/ec_services.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_services_div">
    <?php $_smarty_tpl->tpl_vars["dropdown_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['snapping_id'], null, 0);?>
    <div class="ty-dropdown-box" id="services_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
">
        <div id="sw_dropdown_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-dropdown-box__title cm-combination open">
            <span class="ec_services_header">
                <span><?php echo $_smarty_tpl->__("ec_services");?>
</span><i class="ec-icon-down-arrow"></i>
            </span>
        </div>
        <div id="dropdown_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="cm-popup-box ty-dropdown-box__content">
            <div class="ec_servces_content">
                <div class="ec_service_block ec_announcement">
                    <div class="ec_notice_icon">
                        <i class="ec-icon-announcement"></i><span class="ec_count_announcement">0</span>
                    </div>
                    <div class="ec_notice_text"><?php echo $_smarty_tpl->__("gov_announcements");?>
</div>
                </div>

                
                
                <div class="ec_service_block">
                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.upload_form"), ENT_QUOTES, 'UTF-8');?>
">
                    <div class="ec_notice_icon">
                        <i class="ec-icon-upload-prescription"></i>
                    </div>
                    <div class="ec_notice_text"><?php echo $_smarty_tpl->__("upload_prescription");?>
</div>
                    </a>
                </div>

                

                <div class="ec_service_block">
                    <a href="<?php echo htmlspecialchars(fn_url("companies.catalog&24_7_pharmacies=Y"), ENT_QUOTES, 'UTF-8');?>
">
                    <div class="ec_notice_icon">
                        <i class="ec-icon-pharmacy"></i>
                    </div>
                    <div class="ec_notice_text"><?php echo $_smarty_tpl->__("247_pharmacies");?>
</div>
                    </a>
                </div>

                <div class="ec_service_block">
                    <a href="<?php echo htmlspecialchars(fn_url("h_rfq.update"), ENT_QUOTES, 'UTF-8');?>
">
                    <div class="ec_notice_icon">
                        <i class="ec-icon-indications"></i>
                    </div>
                    <div class="ec_notice_text"><?php echo $_smarty_tpl->__("quotation_request");?>
</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo '<script'; ?>
>
    (function(_, $) {
        $.ceEvent('on', 'ce.commoninit', function(context) {
            setTimeout(function(){
                count = $('.ec_hidden_video').find('.owl-wrapper .owl-item').length;
                $('.ec_count_announcement').text(count);
            }, 1000);
        });
    }(Tygh, Tygh.$));

<?php echo '</script'; ?>
><?php }?><?php }} ?>
