<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 13:24:18
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/checkout/complete.tpl" */ ?>
<?php /*%%SmartyHeaderCode:125617026214abc255fe62-80153285%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ab38d39f892d9dd7e01a9d0f90559c12dd7cd620' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/views/checkout/complete.tpl',
      1 => 1607416221,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '125617026214abc255fe62-80153285',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'order_info' => 0,
    'k' => 0,
    'order_ids' => 0,
    'o_id' => 0,
    'settings' => 0,
    'cart_products' => 0,
    'product' => 0,
    'total_order_count' => 0,
    'obj_id' => 0,
    'key' => 0,
    'o_info' => 0,
    'addons' => 0,
    'ec_wishlist' => 0,
    'selected' => 0,
    'tax_data' => 0,
    'take_surcharge_from_vendor' => 0,
    'auth' => 0,
    'order_info_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214abc26d4598_70582456',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214abc26d4598_70582456')) {function content_6214abc26d4598_70582456($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_block_hook')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_congratulations','ec_congratulations_sub_header','unit_price','quantity','total_price','re_order','by','status','ec_add_to_wishlist','ec_search_product','ec_qty','print_invoice','item_in_order','shipping_cost','subtotal','including_discount','order_discount','coupon','taxes','included','tax_exempt','payment_surcharge','total','create_account','password','confirm_password','create','payment_instructions','ec_congratulations','ec_congratulations_sub_header','unit_price','quantity','total_price','re_order','by','status','ec_add_to_wishlist','ec_search_product','ec_qty','print_invoice','item_in_order','shipping_cost','subtotal','including_discount','order_discount','coupon','taxes','included','tax_exempt','payment_surcharge','total','create_account','password','confirm_password','create','payment_instructions'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>

<div class="ec-container-cart ec_empty_cart ec_order_complete">
    <i class="ec-icon-congrats"></i>
    <p class="ty-no-items_text"><?php echo $_smarty_tpl->__("ec_congratulations");?>
</p>
    <span class="ec_sub_text"><?php echo $_smarty_tpl->__("ec_congratulations_sub_header");?>
</span>
</div>


<?php if ($_smarty_tpl->tpl_vars['order_info']->value['child_ids']) {?>
    <?php $_smarty_tpl->tpl_vars['order_ids'] = new Smarty_variable(explode(",",$_smarty_tpl->tpl_vars['order_info']->value['child_ids']), null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->createLocalArrayVariable('order_ids', null, 0);
$_smarty_tpl->tpl_vars['order_ids']->value[0] = $_smarty_tpl->tpl_vars['order_info']->value['order_id'];?>

<?php }?>
<?php $_smarty_tpl->tpl_vars['total_order_count'] = new Smarty_variable(0, null, 0);?>
<div class="complete_order ec_order_accordian_div">
    <div class="ec_order_content_wrapper">
        <div class="span12">
            <div id="cart_items">
                <?php  $_smarty_tpl->tpl_vars["cart_products"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["cart_products"]->_loop = false;
 $_smarty_tpl->tpl_vars["k"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['product_groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["cart_products"]->key => $_smarty_tpl->tpl_vars["cart_products"]->value) {
$_smarty_tpl->tpl_vars["cart_products"]->_loop = true;
 $_smarty_tpl->tpl_vars["k"]->value = $_smarty_tpl->tpl_vars["cart_products"]->key;
?>
                    <?php $_smarty_tpl->tpl_vars['o_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['order_ids']->value[$_smarty_tpl->tpl_vars['k']->value], null, 0);?>
                    <div class="ec_order_info_header">
                        <div><a href="<?php echo htmlspecialchars(fn_url("orders.details?order_id=".((string)$_smarty_tpl->tpl_vars['o_id']->value)), ENT_QUOTES, 'UTF-8');?>
">#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_ids']->value[$_smarty_tpl->tpl_vars['k']->value], ENT_QUOTES, 'UTF-8');?>
</a></div>
                        <div class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['order_info']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</div>
                    </div>
                    <div class="ec_order_table_container">
                        <table class="ty-cart-content ty-table">

                            <thead class="hidden">
                                <tr>
                                    <th class="ty-cart-content__title ty-left"></th>
                                    <th class="ty-cart-content__title ty-left"></th>
                                    <th class="ty-cart-content__title ty-left">&nbsp;</th>
                                    <th class="ty-cart-content__title ty-right"><?php echo $_smarty_tpl->__("unit_price");?>
</th>
                                    <th class="ty-cart-content__title quantity-cell"><?php echo $_smarty_tpl->__("quantity");?>
</th>
                                    <th class="ty-cart-content__title ty-right"><?php echo $_smarty_tpl->__("total_price");?>
</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php if ($_smarty_tpl->tpl_vars['cart_products']->value['products']) {?>
                                    <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                                        <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars['total_order_count'] = new Smarty_variable($_smarty_tpl->tpl_vars['total_order_count']->value+$_smarty_tpl->tpl_vars['product']->value['amount'], null, 0);?>

                                        <tr>
                                            <td class="ec_reorder_div ec_middle" width="1%">
                                                <a href="<?php echo htmlspecialchars(fn_url("orders.reorder?order_id=".((string)$_smarty_tpl->tpl_vars['o_id']->value)), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("re_order");?>
"><i class="ec-icon-reorder-1"></i></a>
                                            </td>
                                            <td class="ty-cart-content__image-block" width="15%">
                                                <div class="ty-cart-content__image cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="product_image_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                                                    <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                                        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('obj_id'=>$_smarty_tpl->tpl_vars['key']->value,'images'=>$_smarty_tpl->tpl_vars['product']->value['main_pair'],'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_height']), 0);?>

                                                    </a>
                                                </div>
                                            </td>

                                            <td class="ty-cart-content__description" style="width: 30%;">
                                                <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" class="ty-cart-content__product-title"><?php echo $_smarty_tpl->tpl_vars['product']->value['product'];?>
</a>
                                                <div class="ec_cart_seller"><?php echo $_smarty_tpl->__("by");?>
 <a href="<?php echo htmlspecialchars(fn_url("companies.view?company_id=".((string)$_smarty_tpl->tpl_vars['product']->value['company_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');?>
</a></div>
                                                <?php $_smarty_tpl->tpl_vars['o_info'] = new Smarty_variable(fn_get_order_short_info($_smarty_tpl->tpl_vars['o_id']->value), null, 0);?>
                                                <div class="ec_order_status ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['o_info']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                                                    <span><?php echo $_smarty_tpl->__("status");?>
:</span><span><?php echo $_smarty_tpl->getSubTemplate ("common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['o_info']->value['status'],'display'=>"view"), 0);?>
</span>
                                                </div>
                                                
                                                <?php if ($_smarty_tpl->tpl_vars['addons']->value['wishlist']['status']=='A') {?>
                                                    <?php $_smarty_tpl->tpl_vars['ec_wishlist'] = new Smarty_variable(array(), null, 0);?>
                                                    <?php if (!empty($_SESSION['wishlist']['products'])) {?>
                                                        <?php $_smarty_tpl->tpl_vars['ec_wishlist'] = new Smarty_variable(fn_array_value_to_key($_SESSION['wishlist']['products'],'product_id'), null, 0);?>
                                                    <?php }?>
                                                    <?php if (array_key_exists($_smarty_tpl->tpl_vars['product']->value['product_id'],$_smarty_tpl->tpl_vars['ec_wishlist']->value)) {?>
                                                        <?php $_smarty_tpl->tpl_vars['selected'] = new Smarty_variable('added', null, 0);?>
                                                    <?php } else { ?>
                                                        <?php $_smarty_tpl->tpl_vars['selected'] = new Smarty_variable('', null, 0);?>
                                                    <?php }?>
                                                    <div class="ec_save_later">
                                                        <a class="cm-post cm-ajax1 cm-ajax-full-render1 ec_save_for_latter <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected']->value, ENT_QUOTES, 'UTF-8');?>
"  href="<?php echo htmlspecialchars(fn_url("wishlist.add..".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."&product_data[".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."][product_id]=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><i class="ec-icon-wishlist"></i><span><?php echo $_smarty_tpl->__("ec_add_to_wishlist");?>
</span></a>
                                                    </div>
                                                <?php }?>
                                            </td>

                                            <td class="ec_order_search_div ec_middle hidden-phone" width="25%">
                                                <a href="<?php echo htmlspecialchars(fn_url("products.search?search_performed=Y&order_ids=".((string)$_smarty_tpl->tpl_vars['o_id']->value)), ENT_QUOTES, 'UTF-8');?>
">
                                                    <span><?php echo $_smarty_tpl->__("ec_search_product");?>
</span>
                                                    <span class="ec-icon-find-item-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                                                </a>
                                            </td>

                                            <td class="ec_cart_amount_div ec_middle">
                                                <span><?php echo $_smarty_tpl->__("ec_qty");?>
</span> <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</span>
                                            </td>

                                            <td class="ec_order_product_price ec_middle">
                                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['price'],'span_id'=>"product_subtotal_".((string)$_smarty_tpl->tpl_vars['key']->value),'class'=>"price"), 0);?>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            <!--cart_items--></div>

            <?php echo $_smarty_tpl->getSubTemplate ("buttons/continue_shopping.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_meta'=>"ty-checkout-complete__button-vmid",'but_href'=>fn_url("index.index")), 0);?>

        </div>
        <div class="span4 ec_order_total">
            <?php if (!@constant('EC_MOBILE_APP')) {?>
            <div class="ec_print_order">
                <a href="<?php echo htmlspecialchars(fn_url("orders.print_invoice&order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
" class="cm-new-window">
                    <span><?php echo $_smarty_tpl->__("print_invoice");?>
</span>
                    <span class="ec-icon-print-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                </a>
            </div>
            <?php }?>
            <div class="ec_order_total_container">
                <div class="ty-orders-summary__wrapper">
                    <div class="ec_order_item_count"><?php echo $_smarty_tpl->__("item_in_order",array("[count]"=>$_smarty_tpl->tpl_vars['total_order_count']->value));?>
</div>
                    <table class="ty-orders-summary__table">

                        <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['display_shipping_cost'])) {?>
                            <tr class="ty-orders-summary__row">
                                <td><?php echo $_smarty_tpl->__("shipping_cost");?>
:&nbsp;</td>
                                <td data-ct-orders-summary="summary-shipcost"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['display_shipping_cost']), 0);?>
</td>
                            </tr>
                        <?php }?>

                        <tr class="ty-orders-summary__row">
                            <td><?php echo $_smarty_tpl->__("subtotal");?>
:&nbsp;</td>
                            <td data-ct-orders-summary="summary-subtotal"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['display_subtotal']), 0);?>
</td>
                        </tr>

                        <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['discount'])) {?>
                        <tr class="ty-orders-summary__row">
                            <td class="ty-strong"><?php echo $_smarty_tpl->__("including_discount");?>
:</td>
                            <td class="ty-nowrap" data-ct-orders-summary="summary-discount">
                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['discount']), 0);?>

                            </td>
                        </tr>
                        <?php }?>

                        <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['subtotal_discount'])) {?>
                            <tr class="ty-orders-summary__row">
                                <td class="ty-strong"><?php echo $_smarty_tpl->__("order_discount");?>
:</td>
                                <td class="ty-nowrap" data-ct-orders-summary="summary-sub-discount">
                                    <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['subtotal_discount']), 0);?>

                                </td>
                            </tr>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['coupons']) {?>
                            <?php  $_smarty_tpl->tpl_vars["coupon"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["coupon"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['coupons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["coupon"]->key => $_smarty_tpl->tpl_vars["coupon"]->value) {
$_smarty_tpl->tpl_vars["coupon"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["coupon"]->key;
?>
                                <tr class="ty-orders-summary__row">
                                    <td class="ty-nowrap"><?php echo $_smarty_tpl->__("coupon");?>
:</td>
                                    <td data-ct-orders-summary="summary-coupons"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
</td>
                                </tr>
                            <?php } ?>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']) {?>
                            <tr class="taxes">
                                <td><strong><?php echo $_smarty_tpl->__("taxes");?>
:</strong></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php  $_smarty_tpl->tpl_vars['tax_data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tax_data']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['taxes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tax_data']->key => $_smarty_tpl->tpl_vars['tax_data']->value) {
$_smarty_tpl->tpl_vars['tax_data']->_loop = true;
?>
                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__taxes-description">
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_data']->value['description'], ENT_QUOTES, 'UTF-8');?>

                                        <?php echo $_smarty_tpl->getSubTemplate ("common/modifier.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('mod_value'=>$_smarty_tpl->tpl_vars['tax_data']->value['rate_value'],'mod_type'=>$_smarty_tpl->tpl_vars['tax_data']->value['rate_type']), 0);?>

                                        <?php if ($_smarty_tpl->tpl_vars['tax_data']->value['price_includes_tax']=="Y"&&($_smarty_tpl->tpl_vars['settings']->value['Appearance']['cart_prices_w_taxes']!="Y"||$_smarty_tpl->tpl_vars['settings']->value['Checkout']['tax_calculation']=="subtotal")) {?>
                                            <?php echo $_smarty_tpl->__("included");?>

                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['tax_data']->value['regnumber']) {?>
                                            (<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_data']->value['regnumber'], ENT_QUOTES, 'UTF-8');?>
)
                                        <?php }?>
                                    </td>
                                    <td class="ty-orders-summary__taxes-description" data-ct-orders-summary="summary-tax-sub"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['tax_data']->value['tax_subtotal']), 0);?>
</td>
                                </tr>
                            <?php } ?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['tax_exempt']=="Y") {?>
                            <tr class="ty-orders-summary__row">
                                <td><?php echo $_smarty_tpl->__("tax_exempt");?>
</td>
                                <td>&nbsp;</td>
                            <tr>
                        <?php }?>

                        <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['payment_surcharge'])&&!$_smarty_tpl->tpl_vars['take_surcharge_from_vendor']->value) {?>
                            <tr class="ty-orders-summary__row">
                                <td><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['order_info']->value['payment_method']['surcharge_title'])===null||$tmp==='' ? $_smarty_tpl->__("payment_surcharge") : $tmp), ENT_QUOTES, 'UTF-8');?>
:&nbsp;</td>
                                <td data-ct-orders-summary="summary-surchange"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['payment_surcharge']), 0);?>
</td>
                            </tr>
                        <?php }?>
                        <tr class="ec_devide">
                            <td colspan="2">
                            <div></div>
                            </td>
                        </tr>
                        <tr class="ty-orders-summary__row">
                            <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("total");?>
:&nbsp;</td>
                            <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['total']), 0);?>
</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['order_info']->value&&$_smarty_tpl->tpl_vars['settings']->value['Checkout']['allow_create_account_after_order']=="Y"&&!$_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
<div class="ty-checkout-complete__create-account">
    <h3 class="ty-subheader"><?php echo $_smarty_tpl->__("create_account");?>
</h3>
    <div class="ty-login">
        <form name="order_register_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
            <input type="hidden" name="order_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info_id']->value, ENT_QUOTES, 'UTF-8');?>
" />

            <div class="ty-control-group">
                <label for="password1" class="ty-control-group__label ty-login__filed-label cm-required cm-password"><?php echo $_smarty_tpl->__("password");?>
</label>
                <input type="password" id="password1" name="user_data[password1]" size="32" maxlength="32" value="" class="cm-autocomplete-off ty-login__input cm-focus" />
            </div>

            <div class="ty-control-group">
                <label for="password2" class="ty-control-group__label ty-login__filed-label cm-required cm-password"><?php echo $_smarty_tpl->__("confirm_password");?>
</label>
                <input type="password" id="password2" name="user_data[password2]" size="32" maxlength="32" value="" class="cm-autocomplete-off ty-login__input" />
            </div>

            <div class="buttons-container clearfix">
                <p><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[checkout.create_profile]",'but_text'=>$_smarty_tpl->__("create")), 0);?>
</p>
            </div>
        </form>
        </div>
    </div>
    <div class="ty-checkout-complete__login-info">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"checkout:payment_instruction")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"checkout:payment_instruction"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['instructions']) {?>
                <div class="ty-login-info">
                    <h4 class="ty-subheader"><?php echo $_smarty_tpl->__("payment_instructions");?>
</h4>
                    <div class="ty-wysiwyg-content">
                        <?php echo $_smarty_tpl->tpl_vars['order_info']->value['payment_method']['instructions'];?>

                    </div>
                </div>
            <?php }?>
        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"checkout:payment_instruction"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
<?php } else { ?>
    <div class="ty-checkout-complete__login-info ty-checkout-complete_width_full">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"checkout:payment_instruction")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"checkout:payment_instruction"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            
        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"checkout:payment_instruction"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
<?php }?>

    
    

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/checkout/complete.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/checkout/complete.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>

<div class="ec-container-cart ec_empty_cart ec_order_complete">
    <i class="ec-icon-congrats"></i>
    <p class="ty-no-items_text"><?php echo $_smarty_tpl->__("ec_congratulations");?>
</p>
    <span class="ec_sub_text"><?php echo $_smarty_tpl->__("ec_congratulations_sub_header");?>
</span>
</div>


<?php if ($_smarty_tpl->tpl_vars['order_info']->value['child_ids']) {?>
    <?php $_smarty_tpl->tpl_vars['order_ids'] = new Smarty_variable(explode(",",$_smarty_tpl->tpl_vars['order_info']->value['child_ids']), null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->createLocalArrayVariable('order_ids', null, 0);
$_smarty_tpl->tpl_vars['order_ids']->value[0] = $_smarty_tpl->tpl_vars['order_info']->value['order_id'];?>

<?php }?>
<?php $_smarty_tpl->tpl_vars['total_order_count'] = new Smarty_variable(0, null, 0);?>
<div class="complete_order ec_order_accordian_div">
    <div class="ec_order_content_wrapper">
        <div class="span12">
            <div id="cart_items">
                <?php  $_smarty_tpl->tpl_vars["cart_products"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["cart_products"]->_loop = false;
 $_smarty_tpl->tpl_vars["k"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['product_groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["cart_products"]->key => $_smarty_tpl->tpl_vars["cart_products"]->value) {
$_smarty_tpl->tpl_vars["cart_products"]->_loop = true;
 $_smarty_tpl->tpl_vars["k"]->value = $_smarty_tpl->tpl_vars["cart_products"]->key;
?>
                    <?php $_smarty_tpl->tpl_vars['o_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['order_ids']->value[$_smarty_tpl->tpl_vars['k']->value], null, 0);?>
                    <div class="ec_order_info_header">
                        <div><a href="<?php echo htmlspecialchars(fn_url("orders.details?order_id=".((string)$_smarty_tpl->tpl_vars['o_id']->value)), ENT_QUOTES, 'UTF-8');?>
">#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_ids']->value[$_smarty_tpl->tpl_vars['k']->value], ENT_QUOTES, 'UTF-8');?>
</a></div>
                        <div class="ec_order_time"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['order_info']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</div>
                    </div>
                    <div class="ec_order_table_container">
                        <table class="ty-cart-content ty-table">

                            <thead class="hidden">
                                <tr>
                                    <th class="ty-cart-content__title ty-left"></th>
                                    <th class="ty-cart-content__title ty-left"></th>
                                    <th class="ty-cart-content__title ty-left">&nbsp;</th>
                                    <th class="ty-cart-content__title ty-right"><?php echo $_smarty_tpl->__("unit_price");?>
</th>
                                    <th class="ty-cart-content__title quantity-cell"><?php echo $_smarty_tpl->__("quantity");?>
</th>
                                    <th class="ty-cart-content__title ty-right"><?php echo $_smarty_tpl->__("total_price");?>
</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php if ($_smarty_tpl->tpl_vars['cart_products']->value['products']) {?>
                                    <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cart_products']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                                        <?php $_smarty_tpl->tpl_vars["obj_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['product']->value['product_id'], null, 0);?>
                                        <?php $_smarty_tpl->tpl_vars['total_order_count'] = new Smarty_variable($_smarty_tpl->tpl_vars['total_order_count']->value+$_smarty_tpl->tpl_vars['product']->value['amount'], null, 0);?>

                                        <tr>
                                            <td class="ec_reorder_div ec_middle" width="1%">
                                                <a href="<?php echo htmlspecialchars(fn_url("orders.reorder?order_id=".((string)$_smarty_tpl->tpl_vars['o_id']->value)), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("re_order");?>
"><i class="ec-icon-reorder-1"></i></a>
                                            </td>
                                            <td class="ty-cart-content__image-block" width="15%">
                                                <div class="ty-cart-content__image cm-reload-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
" id="product_image_update_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['obj_id']->value, ENT_QUOTES, 'UTF-8');?>
">
                                                    <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
">
                                                        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('obj_id'=>$_smarty_tpl->tpl_vars['key']->value,'images'=>$_smarty_tpl->tpl_vars['product']->value['main_pair'],'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['product_cart_thumbnail_height']), 0);?>

                                                    </a>
                                                </div>
                                            </td>

                                            <td class="ty-cart-content__description" style="width: 30%;">
                                                <a href="<?php echo htmlspecialchars(fn_url("products.view?product_id=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
" class="ty-cart-content__product-title"><?php echo $_smarty_tpl->tpl_vars['product']->value['product'];?>
</a>
                                                <div class="ec_cart_seller"><?php echo $_smarty_tpl->__("by");?>
 <a href="<?php echo htmlspecialchars(fn_url("companies.view?company_id=".((string)$_smarty_tpl->tpl_vars['product']->value['company_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(fn_get_company_name($_smarty_tpl->tpl_vars['product']->value['company_id']), ENT_QUOTES, 'UTF-8');?>
</a></div>
                                                <?php $_smarty_tpl->tpl_vars['o_info'] = new Smarty_variable(fn_get_order_short_info($_smarty_tpl->tpl_vars['o_id']->value), null, 0);?>
                                                <div class="ec_order_status ec_order_status_<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['o_info']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                                                    <span><?php echo $_smarty_tpl->__("status");?>
:</span><span><?php echo $_smarty_tpl->getSubTemplate ("common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['o_info']->value['status'],'display'=>"view"), 0);?>
</span>
                                                </div>
                                                
                                                <?php if ($_smarty_tpl->tpl_vars['addons']->value['wishlist']['status']=='A') {?>
                                                    <?php $_smarty_tpl->tpl_vars['ec_wishlist'] = new Smarty_variable(array(), null, 0);?>
                                                    <?php if (!empty($_SESSION['wishlist']['products'])) {?>
                                                        <?php $_smarty_tpl->tpl_vars['ec_wishlist'] = new Smarty_variable(fn_array_value_to_key($_SESSION['wishlist']['products'],'product_id'), null, 0);?>
                                                    <?php }?>
                                                    <?php if (array_key_exists($_smarty_tpl->tpl_vars['product']->value['product_id'],$_smarty_tpl->tpl_vars['ec_wishlist']->value)) {?>
                                                        <?php $_smarty_tpl->tpl_vars['selected'] = new Smarty_variable('added', null, 0);?>
                                                    <?php } else { ?>
                                                        <?php $_smarty_tpl->tpl_vars['selected'] = new Smarty_variable('', null, 0);?>
                                                    <?php }?>
                                                    <div class="ec_save_later">
                                                        <a class="cm-post cm-ajax1 cm-ajax-full-render1 ec_save_for_latter <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['selected']->value, ENT_QUOTES, 'UTF-8');?>
"  href="<?php echo htmlspecialchars(fn_url("wishlist.add..".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."&product_data[".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])."][product_id]=".((string)$_smarty_tpl->tpl_vars['product']->value['product_id'])), ENT_QUOTES, 'UTF-8');?>
"><i class="ec-icon-wishlist"></i><span><?php echo $_smarty_tpl->__("ec_add_to_wishlist");?>
</span></a>
                                                    </div>
                                                <?php }?>
                                            </td>

                                            <td class="ec_order_search_div ec_middle hidden-phone" width="25%">
                                                <a href="<?php echo htmlspecialchars(fn_url("products.search?search_performed=Y&order_ids=".((string)$_smarty_tpl->tpl_vars['o_id']->value)), ENT_QUOTES, 'UTF-8');?>
">
                                                    <span><?php echo $_smarty_tpl->__("ec_search_product");?>
</span>
                                                    <span class="ec-icon-find-item-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></span>
                                                </a>
                                            </td>

                                            <td class="ec_cart_amount_div ec_middle">
                                                <span><?php echo $_smarty_tpl->__("ec_qty");?>
</span> <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'UTF-8');?>
</span>
                                            </td>

                                            <td class="ec_order_product_price ec_middle">
                                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['price'],'span_id'=>"product_subtotal_".((string)$_smarty_tpl->tpl_vars['key']->value),'class'=>"price"), 0);?>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php }?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            <!--cart_items--></div>

            <?php echo $_smarty_tpl->getSubTemplate ("buttons/continue_shopping.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"text",'but_meta'=>"ty-checkout-complete__button-vmid",'but_href'=>fn_url("index.index")), 0);?>

        </div>
        <div class="span4 ec_order_total">
            <?php if (!@constant('EC_MOBILE_APP')) {?>
            <div class="ec_print_order">
                <a href="<?php echo htmlspecialchars(fn_url("orders.print_invoice&order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])), ENT_QUOTES, 'UTF-8');?>
" class="cm-new-window">
                    <span><?php echo $_smarty_tpl->__("print_invoice");?>
</span>
                    <span class="ec-icon-print-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
                </a>
            </div>
            <?php }?>
            <div class="ec_order_total_container">
                <div class="ty-orders-summary__wrapper">
                    <div class="ec_order_item_count"><?php echo $_smarty_tpl->__("item_in_order",array("[count]"=>$_smarty_tpl->tpl_vars['total_order_count']->value));?>
</div>
                    <table class="ty-orders-summary__table">

                        <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['display_shipping_cost'])) {?>
                            <tr class="ty-orders-summary__row">
                                <td><?php echo $_smarty_tpl->__("shipping_cost");?>
:&nbsp;</td>
                                <td data-ct-orders-summary="summary-shipcost"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['display_shipping_cost']), 0);?>
</td>
                            </tr>
                        <?php }?>

                        <tr class="ty-orders-summary__row">
                            <td><?php echo $_smarty_tpl->__("subtotal");?>
:&nbsp;</td>
                            <td data-ct-orders-summary="summary-subtotal"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['display_subtotal']), 0);?>
</td>
                        </tr>

                        <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['discount'])) {?>
                        <tr class="ty-orders-summary__row">
                            <td class="ty-strong"><?php echo $_smarty_tpl->__("including_discount");?>
:</td>
                            <td class="ty-nowrap" data-ct-orders-summary="summary-discount">
                                <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['discount']), 0);?>

                            </td>
                        </tr>
                        <?php }?>

                        <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['subtotal_discount'])) {?>
                            <tr class="ty-orders-summary__row">
                                <td class="ty-strong"><?php echo $_smarty_tpl->__("order_discount");?>
:</td>
                                <td class="ty-nowrap" data-ct-orders-summary="summary-sub-discount">
                                    <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['subtotal_discount']), 0);?>

                                </td>
                            </tr>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['coupons']) {?>
                            <?php  $_smarty_tpl->tpl_vars["coupon"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["coupon"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['coupons']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["coupon"]->key => $_smarty_tpl->tpl_vars["coupon"]->value) {
$_smarty_tpl->tpl_vars["coupon"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["coupon"]->key;
?>
                                <tr class="ty-orders-summary__row">
                                    <td class="ty-nowrap"><?php echo $_smarty_tpl->__("coupon");?>
:</td>
                                    <td data-ct-orders-summary="summary-coupons"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
</td>
                                </tr>
                            <?php } ?>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['taxes']) {?>
                            <tr class="taxes">
                                <td><strong><?php echo $_smarty_tpl->__("taxes");?>
:</strong></td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php  $_smarty_tpl->tpl_vars['tax_data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['tax_data']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['order_info']->value['taxes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['tax_data']->key => $_smarty_tpl->tpl_vars['tax_data']->value) {
$_smarty_tpl->tpl_vars['tax_data']->_loop = true;
?>
                                <tr class="ty-orders-summary__row">
                                    <td class="ty-orders-summary__taxes-description">
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_data']->value['description'], ENT_QUOTES, 'UTF-8');?>

                                        <?php echo $_smarty_tpl->getSubTemplate ("common/modifier.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('mod_value'=>$_smarty_tpl->tpl_vars['tax_data']->value['rate_value'],'mod_type'=>$_smarty_tpl->tpl_vars['tax_data']->value['rate_type']), 0);?>

                                        <?php if ($_smarty_tpl->tpl_vars['tax_data']->value['price_includes_tax']=="Y"&&($_smarty_tpl->tpl_vars['settings']->value['Appearance']['cart_prices_w_taxes']!="Y"||$_smarty_tpl->tpl_vars['settings']->value['Checkout']['tax_calculation']=="subtotal")) {?>
                                            <?php echo $_smarty_tpl->__("included");?>

                                        <?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['tax_data']->value['regnumber']) {?>
                                            (<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tax_data']->value['regnumber'], ENT_QUOTES, 'UTF-8');?>
)
                                        <?php }?>
                                    </td>
                                    <td class="ty-orders-summary__taxes-description" data-ct-orders-summary="summary-tax-sub"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['tax_data']->value['tax_subtotal']), 0);?>
</td>
                                </tr>
                            <?php } ?>
                        <?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['tax_exempt']=="Y") {?>
                            <tr class="ty-orders-summary__row">
                                <td><?php echo $_smarty_tpl->__("tax_exempt");?>
</td>
                                <td>&nbsp;</td>
                            <tr>
                        <?php }?>

                        <?php if (floatval($_smarty_tpl->tpl_vars['order_info']->value['payment_surcharge'])&&!$_smarty_tpl->tpl_vars['take_surcharge_from_vendor']->value) {?>
                            <tr class="ty-orders-summary__row">
                                <td><?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['order_info']->value['payment_method']['surcharge_title'])===null||$tmp==='' ? $_smarty_tpl->__("payment_surcharge") : $tmp), ENT_QUOTES, 'UTF-8');?>
:&nbsp;</td>
                                <td data-ct-orders-summary="summary-surchange"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['payment_surcharge']), 0);?>
</td>
                            </tr>
                        <?php }?>
                        <tr class="ec_devide">
                            <td colspan="2">
                            <div></div>
                            </td>
                        </tr>
                        <tr class="ty-orders-summary__row">
                            <td class="ty-orders-summary__total"><?php echo $_smarty_tpl->__("total");?>
:&nbsp;</td>
                            <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['total']), 0);?>
</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($_smarty_tpl->tpl_vars['order_info']->value&&$_smarty_tpl->tpl_vars['settings']->value['Checkout']['allow_create_account_after_order']=="Y"&&!$_smarty_tpl->tpl_vars['auth']->value['user_id']) {?>
<div class="ty-checkout-complete__create-account">
    <h3 class="ty-subheader"><?php echo $_smarty_tpl->__("create_account");?>
</h3>
    <div class="ty-login">
        <form name="order_register_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
            <input type="hidden" name="order_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info_id']->value, ENT_QUOTES, 'UTF-8');?>
" />

            <div class="ty-control-group">
                <label for="password1" class="ty-control-group__label ty-login__filed-label cm-required cm-password"><?php echo $_smarty_tpl->__("password");?>
</label>
                <input type="password" id="password1" name="user_data[password1]" size="32" maxlength="32" value="" class="cm-autocomplete-off ty-login__input cm-focus" />
            </div>

            <div class="ty-control-group">
                <label for="password2" class="ty-control-group__label ty-login__filed-label cm-required cm-password"><?php echo $_smarty_tpl->__("confirm_password");?>
</label>
                <input type="password" id="password2" name="user_data[password2]" size="32" maxlength="32" value="" class="cm-autocomplete-off ty-login__input" />
            </div>

            <div class="buttons-container clearfix">
                <p><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[checkout.create_profile]",'but_text'=>$_smarty_tpl->__("create")), 0);?>
</p>
            </div>
        </form>
        </div>
    </div>
    <div class="ty-checkout-complete__login-info">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"checkout:payment_instruction")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"checkout:payment_instruction"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            <?php if ($_smarty_tpl->tpl_vars['order_info']->value['payment_method']['instructions']) {?>
                <div class="ty-login-info">
                    <h4 class="ty-subheader"><?php echo $_smarty_tpl->__("payment_instructions");?>
</h4>
                    <div class="ty-wysiwyg-content">
                        <?php echo $_smarty_tpl->tpl_vars['order_info']->value['payment_method']['instructions'];?>

                    </div>
                </div>
            <?php }?>
        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"checkout:payment_instruction"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
<?php } else { ?>
    <div class="ty-checkout-complete__login-info ty-checkout-complete_width_full">
        <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"checkout:payment_instruction")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"checkout:payment_instruction"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

            
        <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"checkout:payment_instruction"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

    </div>
<?php }?>

    
    

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
