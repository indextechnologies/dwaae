<?php /* Smarty version Smarty-3.1.21, created on 2022-02-23 05:42:58
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/clinic.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14349436606215912276efc5-69070369%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ec880314ca1d2d18901f0f68604675dd79906fbc' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/clinic.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '14349436606215912276efc5-69070369',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'language_direction' => 0,
    'uniqid' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621591227aa217_51560381',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621591227aa217_51560381')) {function content_621591227aa217_51560381($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
if (!is_callable('smarty_function_render_location')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.render_location.php';
if (!is_callable('smarty_block_scripts')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.scripts.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><!doctype html>

<html lang="<?php echo htmlspecialchars(@constant('CART_LANGUAGE'), ENT_QUOTES, 'UTF-8');?>
" dir="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language_direction']->value, ENT_QUOTES, 'UTF-8');?>
">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Clinic System</title>
    <?php $_smarty_tpl->tpl_vars['uniqid'] = new Smarty_variable(uniqid(), null, 0);?>
    
    <link href="js/addons/ec_emr/vendor/bootstrap4/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="design/themes/responsive/css/addons/ec_emr/master.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <link type="text/css" rel="stylesheet" href="design/themes/responsive/css/addons/ec_emr/auth.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <link href="js/addons/ec_emr/vendor/chartsjs/Chart.min.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="js/addons/ec_emr/vendor/flagiconcss3/css/flag-icon.min.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <link type="text/css" rel="stylesheet" href="js/addons/ec_emr/vendor/fontawesome5/css/all.min.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <link type="text/css" rel="stylesheet" href="js/addons/ec_emr/vendor/DataTables/datatables.min.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    
    
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"
            data-no-defer
    ><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-migrate-3.0.1.min.js"
            integrity="sha256-F0O1TmEa4I8N24nY0bya59eP6svWcshqX1uzwaWC4F4="
            crossorigin="anonymous"
            data-no-defer
    ><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 data-no-defer>
        if (!window.jQuery) {
            document.write('<?php echo smarty_function_script(array('src'=>"js/lib/jquery/jquery-3.3.1.min.js",'no-defer'=>true,'escape'=>true),$_smarty_tpl);?>
');
            document.write('<?php echo smarty_function_script(array('src'=>"js/lib/jquery/jquery-migrate-3.0.1.min.js",'no-defer'=>true,'escape'=>true),$_smarty_tpl);?>
');
        }
    <?php echo '</script'; ?>
>
   
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ("common/notification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <?php echo smarty_function_render_location(array(),$_smarty_tpl);?>


     <?php $_smarty_tpl->smarty->_tag_stack[] = array('scripts', array()); $_block_repeat=true; echo smarty_block_scripts(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

       
        <?php echo smarty_function_script(array('src'=>"js/lib/jqueryui/jquery-ui.custom.min.js",'no-defer'=>true),$_smarty_tpl);?>

        <?php echo smarty_function_script(array('src'=>"js/addons/ec_emr/bootstrap.min.js"),$_smarty_tpl);?>

        
        
        <?php echo smarty_function_script(array('src'=>"js/addons/ec_emr/js/script.js"),$_smarty_tpl);?>

        <?php echo '<script'; ?>
 src="js/addons/ec_emr/vendor/DataTables/datatables.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/addons/ec_emr/js/form-validator.js"><?php echo '</script'; ?>
>
        
        
        
        
        <?php echo '<script'; ?>
>
            $(document).ready(function(){
                 $(document).on('click', '.cm-notification-close', function () {
                    $(this).closest('.cm-notification-content').remove();
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    $('#body').toggleClass('active');
                });

                $('#dataTables-table').DataTable({
                    responsive: true,
                    pageLength: 20,
                    lengthChange: false,
                    searching: true,
                    ordering: true});
            });
        <?php echo '</script'; ?>
>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_scripts(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</body>
</html><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="clinic.tpl" id="<?php echo smarty_function_set_id(array('name'=>"clinic.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><!doctype html>

<html lang="<?php echo htmlspecialchars(@constant('CART_LANGUAGE'), ENT_QUOTES, 'UTF-8');?>
" dir="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language_direction']->value, ENT_QUOTES, 'UTF-8');?>
">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Clinic System</title>
    <?php $_smarty_tpl->tpl_vars['uniqid'] = new Smarty_variable(uniqid(), null, 0);?>
    
    <link href="js/addons/ec_emr/vendor/bootstrap4/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="design/themes/responsive/css/addons/ec_emr/master.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <link type="text/css" rel="stylesheet" href="design/themes/responsive/css/addons/ec_emr/auth.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <link href="js/addons/ec_emr/vendor/chartsjs/Chart.min.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="js/addons/ec_emr/vendor/flagiconcss3/css/flag-icon.min.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <link type="text/css" rel="stylesheet" href="js/addons/ec_emr/vendor/fontawesome5/css/all.min.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    <link type="text/css" rel="stylesheet" href="js/addons/ec_emr/vendor/DataTables/datatables.min.css?unq=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uniqid']->value, ENT_QUOTES, 'UTF-8');?>
">
    
    
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"
            data-no-defer
    ><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-migrate-3.0.1.min.js"
            integrity="sha256-F0O1TmEa4I8N24nY0bya59eP6svWcshqX1uzwaWC4F4="
            crossorigin="anonymous"
            data-no-defer
    ><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 data-no-defer>
        if (!window.jQuery) {
            document.write('<?php echo smarty_function_script(array('src'=>"js/lib/jquery/jquery-3.3.1.min.js",'no-defer'=>true,'escape'=>true),$_smarty_tpl);?>
');
            document.write('<?php echo smarty_function_script(array('src'=>"js/lib/jquery/jquery-migrate-3.0.1.min.js",'no-defer'=>true,'escape'=>true),$_smarty_tpl);?>
');
        }
    <?php echo '</script'; ?>
>
   
</head>
<body>
    <?php echo $_smarty_tpl->getSubTemplate ("common/notification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <?php echo smarty_function_render_location(array(),$_smarty_tpl);?>


     <?php $_smarty_tpl->smarty->_tag_stack[] = array('scripts', array()); $_block_repeat=true; echo smarty_block_scripts(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

       
        <?php echo smarty_function_script(array('src'=>"js/lib/jqueryui/jquery-ui.custom.min.js",'no-defer'=>true),$_smarty_tpl);?>

        <?php echo smarty_function_script(array('src'=>"js/addons/ec_emr/bootstrap.min.js"),$_smarty_tpl);?>

        
        
        <?php echo smarty_function_script(array('src'=>"js/addons/ec_emr/js/script.js"),$_smarty_tpl);?>

        <?php echo '<script'; ?>
 src="js/addons/ec_emr/vendor/DataTables/datatables.min.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="js/addons/ec_emr/js/form-validator.js"><?php echo '</script'; ?>
>
        
        
        
        
        <?php echo '<script'; ?>
>
            $(document).ready(function(){
                 $(document).on('click', '.cm-notification-close', function () {
                    $(this).closest('.cm-notification-content').remove();
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar').toggleClass('active');
                    $('#body').toggleClass('active');
                });

                $('#dataTables-table').DataTable({
                    responsive: true,
                    pageLength: 20,
                    lengthChange: false,
                    searching: true,
                    ordering: true});
            });
        <?php echo '</script'; ?>
>
    <?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_scripts(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</body>
</html><?php }?><?php }} ?>
