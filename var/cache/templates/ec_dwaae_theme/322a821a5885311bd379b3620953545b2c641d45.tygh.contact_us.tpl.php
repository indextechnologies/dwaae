<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:46:41
         compiled from "/home/dwaae/public_html/design/themes/responsive/templates/addons/d_custom/views/ec_pages/contact_us.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3600180156214a2f1886268-58917132%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '322a821a5885311bd379b3620953545b2c641d45' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/responsive/templates/addons/d_custom/views/ec_pages/contact_us.tpl',
      1 => 1618387358,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3600180156214a2f1886268-58917132',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214a2f18c7330_58647706',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214a2f18c7330_58647706')) {function content_6214a2f18c7330_58647706($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('contact_us','address','contact_info','store_locator.work_time','working_time_to_from','send_us_message','name','email','message','how_to_reach_us','how_to_reach_us_line_1','how_to_reach_us_line_2','how_to_reach_us_line_3','contact_us','address','contact_info','store_locator.work_time','working_time_to_from','send_us_message','name','email','message','how_to_reach_us','how_to_reach_us_line_1','how_to_reach_us_line_2','how_to_reach_us_line_3'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec-contact-us">
    <h3 class="ty-subheader ec-contact-header">
        <?php echo $_smarty_tpl->__("contact_us");?>

    </h3>
    <div class="ec-contact-header">

        <div class="contact-header-block">
            <div>
                <img src="images/delivery-location-2.svg" style="width:55px; height: 45px;"/>
            </div>
            <div>
                <div>
                    <?php echo $_smarty_tpl->__("address");?>

                </div>
                <div>
                    Musaffah M15, Street 11 opposite GTA Car Care <br/>Abu Dhabi, UAE
                </div>
            </div>
        </div>
        
        <div class="contact-vertical-line">
        </div>

        <div class="contact-header-block">
            <div>
                <img src="images/phone-1.svg" style="width:55px; height: 45px;"/>
            </div>
            <div>
                <div>
                    <?php echo $_smarty_tpl->__("contact_info");?>
.
                </div>
                <div>
                    care@dwaae.com
                </div>
            </div>
        </div>

        <div class="contact-vertical-line">
        </div>

        <div class="contact-header-block">
            <div>
                <img src="images/clock-1.svg" style="width:55px; height: 45px;"/>
            </div>
            <div>
                <div>
                    <?php echo $_smarty_tpl->__("store_locator.work_time");?>

                </div>
                <div>
                    <?php echo $_smarty_tpl->__("working_time_to_from");?>

                </div>
            </div>
        </div>
    </div>

    
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d468.4674588068069!2d54.488158471150165!3d24.376301920691834!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e41d3a2da3037%3A0x67fdd214df464d93!2sMECSTRA!5e0!3m2!1sen!2sin!4v1618387310438!5m2!1sen!2sin" width="100%" height="498" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    <div class="ec-contact-footer">
        <div class="ec-contact-form">
            <h3 class="ty-subheader">
                <?php echo $_smarty_tpl->__("send_us_message");?>

            </h3>
            <form name="contact_us_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
            <div class="first-two-field">
                <div class="ty-control-group">     
                    <label for="elm_name" class="ty-control-group__title cm-required "><?php echo $_smarty_tpl->__("name");?>
</label>
                    <input  type="text" id="elm_name" name="contact[name]" size="32" value="" class="ty-input-text   cm-focus">
                </div>
                <div class="ty-control-group">     
                    <label for="elm_email" class="ty-control-group__title cm-required cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
                    <input  type="text" id="elm_email" name="contact[email]" size="32" value="" class="ty-input-text   cm-focus">
                </div>
            </div>
            <div class="ty-control-group second-text-area">     
                <label for="elm_message" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("message");?>
</label>
                <textarea id="elm_message" name="contact[message]"></textarea>
            </div>
            <div class="third-right-button">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>"Send your message",'but_role'=>"action",'but_meta'=>"ty-btn__secondary",'but_target_form'=>"contact_us_form",'but_name'=>"dispatch[ec_pages.contact_us]"), 0);?>

            </div>
            </form>
        </div>

         <div class="contact-vertical-line">
        </div>
        
        <div class="ec-contact-directon-block">
            <h3 class="ty-subheader ec-direction-header">
               <?php echo $_smarty_tpl->__("how_to_reach_us");?>

            </h3>
            <ul>
                <li> <?php echo $_smarty_tpl->__("how_to_reach_us_line_1");?>
 </li>
                <li>  <?php echo $_smarty_tpl->__("how_to_reach_us_line_2");?>
</li>
                <li><?php echo $_smarty_tpl->__("how_to_reach_us_line_3");?>
 </li>
            </ul>
            <div>
                <img src="images/address-2.svg" style="  width: 195px;height: 195px;object-fit: contain;" />
            </div>
        </div>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/d_custom/views/ec_pages/contact_us.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/d_custom/views/ec_pages/contact_us.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec-contact-us">
    <h3 class="ty-subheader ec-contact-header">
        <?php echo $_smarty_tpl->__("contact_us");?>

    </h3>
    <div class="ec-contact-header">

        <div class="contact-header-block">
            <div>
                <img src="images/delivery-location-2.svg" style="width:55px; height: 45px;"/>
            </div>
            <div>
                <div>
                    <?php echo $_smarty_tpl->__("address");?>

                </div>
                <div>
                    Musaffah M15, Street 11 opposite GTA Car Care <br/>Abu Dhabi, UAE
                </div>
            </div>
        </div>
        
        <div class="contact-vertical-line">
        </div>

        <div class="contact-header-block">
            <div>
                <img src="images/phone-1.svg" style="width:55px; height: 45px;"/>
            </div>
            <div>
                <div>
                    <?php echo $_smarty_tpl->__("contact_info");?>
.
                </div>
                <div>
                    care@dwaae.com
                </div>
            </div>
        </div>

        <div class="contact-vertical-line">
        </div>

        <div class="contact-header-block">
            <div>
                <img src="images/clock-1.svg" style="width:55px; height: 45px;"/>
            </div>
            <div>
                <div>
                    <?php echo $_smarty_tpl->__("store_locator.work_time");?>

                </div>
                <div>
                    <?php echo $_smarty_tpl->__("working_time_to_from");?>

                </div>
            </div>
        </div>
    </div>

    
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d468.4674588068069!2d54.488158471150165!3d24.376301920691834!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5e41d3a2da3037%3A0x67fdd214df464d93!2sMECSTRA!5e0!3m2!1sen!2sin!4v1618387310438!5m2!1sen!2sin" width="100%" height="498" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    <div class="ec-contact-footer">
        <div class="ec-contact-form">
            <h3 class="ty-subheader">
                <?php echo $_smarty_tpl->__("send_us_message");?>

            </h3>
            <form name="contact_us_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
            <div class="first-two-field">
                <div class="ty-control-group">     
                    <label for="elm_name" class="ty-control-group__title cm-required "><?php echo $_smarty_tpl->__("name");?>
</label>
                    <input  type="text" id="elm_name" name="contact[name]" size="32" value="" class="ty-input-text   cm-focus">
                </div>
                <div class="ty-control-group">     
                    <label for="elm_email" class="ty-control-group__title cm-required cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
                    <input  type="text" id="elm_email" name="contact[email]" size="32" value="" class="ty-input-text   cm-focus">
                </div>
            </div>
            <div class="ty-control-group second-text-area">     
                <label for="elm_message" class="ty-control-group__title cm-required"><?php echo $_smarty_tpl->__("message");?>
</label>
                <textarea id="elm_message" name="contact[message]"></textarea>
            </div>
            <div class="third-right-button">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>"Send your message",'but_role'=>"action",'but_meta'=>"ty-btn__secondary",'but_target_form'=>"contact_us_form",'but_name'=>"dispatch[ec_pages.contact_us]"), 0);?>

            </div>
            </form>
        </div>

         <div class="contact-vertical-line">
        </div>
        
        <div class="ec-contact-directon-block">
            <h3 class="ty-subheader ec-direction-header">
               <?php echo $_smarty_tpl->__("how_to_reach_us");?>

            </h3>
            <ul>
                <li> <?php echo $_smarty_tpl->__("how_to_reach_us_line_1");?>
 </li>
                <li>  <?php echo $_smarty_tpl->__("how_to_reach_us_line_2");?>
</li>
                <li><?php echo $_smarty_tpl->__("how_to_reach_us_line_3");?>
 </li>
            </ul>
            <div>
                <img src="images/address-2.svg" style="  width: 195px;height: 195px;object-fit: contain;" />
            </div>
        </div>
    </div>
</div><?php }?><?php }} ?>
