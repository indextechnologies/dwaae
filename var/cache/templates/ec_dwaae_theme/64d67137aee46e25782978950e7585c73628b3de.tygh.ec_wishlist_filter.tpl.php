<?php /* Smarty version Smarty-3.1.21, created on 2022-02-23 05:40:57
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_wishlist_filter.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1224050964621590a9935649-96580149%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '64d67137aee46e25782978950e7585c73628b3de' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_wishlist_filter.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1224050964621590a9935649-96580149',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'products' => 0,
    'block' => 0,
    'ajax_classes' => 0,
    'filter_uid' => 0,
    'wishlist_filter' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621590a997e839_51966305',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621590a997e839_51966305')) {function content_621590a997e839_51966305($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('order_status','wf.all_products','wf.purchased','wf.not_purchased_yet','wf.available_in_stock','wf.prescription_required','wf.prescription_not_required','order_status','wf.all_products','wf.purchased','wf.not_purchased_yet','wf.available_in_stock','wf.prescription_required','wf.prescription_not_required'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['products']->value||$_REQUEST['wishlist_filter']) {?>
    <div id="wishlist_filter">
        <form name="ec_wishlist_filter_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=".((string)$_REQUEST['sort_by'])."&sort_order=".((string)$_REQUEST['sort_order'])), ENT_QUOTES, 'UTF-8');?>
" method="get">  
        <input type="hidden" name="result_ids" value="pagination_contents" /> 
        <?php $_smarty_tpl->tpl_vars["filter_uid"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['block']->value['block_id'])."_wishlist", null, 0);?>
        <div class="">
            <div id="sw_content_order_status" class="ty-order_filter__switch cm-combination-filter_order_status open cm-save-state hidden">
                <div class="order_filter_header">
                    <?php echo $_smarty_tpl->__("order_status");?>

                </div>
                <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                <i class="ec_icon_switch_minus ty-icon-up-open"></i>
            </div>
            <?php $_smarty_tpl->tpl_vars['wishlist_filter'] = new Smarty_variable((($tmp = @$_REQUEST['wishlist_filter'])===null||$tmp==='' ? array('all') : $tmp), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['ajax_classes'] = new Smarty_variable("cm-ajax cm-ajax-full-render", null, 0);?>
            <div class="ty-product-filters" id="content_order_status">
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="all" id="elm_all_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('all',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.all_products");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="purchased" id="elm_purchased_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('purchased',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.purchased");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="not_purchased" id="elm_not_purchased_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('not_purchased',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.not_purchased_yet");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="in_stock" id="elm_in_stock_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('in_stock',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.available_in_stock");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="prescription_required" id="elm_prescription_required_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('prescription_required',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.prescription_required");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="prescription_not_required" id="elm_prescription_not_required_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('prescription_not_required',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.prescription_not_required");?>
</label>
                </li>
            </div>
        </div>
        </form>
    <!--wishlist_filter--></div>
<?php }?>



<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/static_templates/ec_wishlist_filter.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/static_templates/ec_wishlist_filter.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['products']->value||$_REQUEST['wishlist_filter']) {?>
    <div id="wishlist_filter">
        <form name="ec_wishlist_filter_form" enctype="multipart/form-data" action="<?php echo htmlspecialchars(fn_url("wishlist.view?sort_by=".((string)$_REQUEST['sort_by'])."&sort_order=".((string)$_REQUEST['sort_order'])), ENT_QUOTES, 'UTF-8');?>
" method="get">  
        <input type="hidden" name="result_ids" value="pagination_contents" /> 
        <?php $_smarty_tpl->tpl_vars["filter_uid"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['block']->value['block_id'])."_wishlist", null, 0);?>
        <div class="">
            <div id="sw_content_order_status" class="ty-order_filter__switch cm-combination-filter_order_status open cm-save-state hidden">
                <div class="order_filter_header">
                    <?php echo $_smarty_tpl->__("order_status");?>

                </div>
                <i class="ec_icon_switch_plus ty-icon-down-open"></i>
                <i class="ec_icon_switch_minus ty-icon-up-open"></i>
            </div>
            <?php $_smarty_tpl->tpl_vars['wishlist_filter'] = new Smarty_variable((($tmp = @$_REQUEST['wishlist_filter'])===null||$tmp==='' ? array('all') : $tmp), null, 0);?>
            <?php $_smarty_tpl->tpl_vars['ajax_classes'] = new Smarty_variable("cm-ajax cm-ajax-full-render", null, 0);?>
            <div class="ty-product-filters" id="content_order_status">
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="all" id="elm_all_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('all',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.all_products");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="purchased" id="elm_purchased_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('purchased',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.purchased");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="not_purchased" id="elm_not_purchased_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('not_purchased',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.not_purchased_yet");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="in_stock" id="elm_in_stock_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('in_stock',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.available_in_stock");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="prescription_required" id="elm_prescription_required_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('prescription_required',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.prescription_required");?>
</label>
                </li>
                <li class="cm-product-filters-checkbox-container ty-product-filters__group">
                    <label><input class="cm-submit <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_classes']->value, ENT_QUOTES, 'UTF-8');?>
" type="radio" name="wishlist_filter[]" value="prescription_not_required" id="elm_prescription_not_required_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['filter_uid']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['wishlist_filter']->value&&in_array('prescription_not_required',$_smarty_tpl->tpl_vars['wishlist_filter']->value)) {?>checked="checked"<?php }?>><?php echo $_smarty_tpl->__("wf.prescription_not_required");?>
</label>
                </li>
            </div>
        </div>
        </form>
    <!--wishlist_filter--></div>
<?php }?>



<?php }?><?php }} ?>
