<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:19
         compiled from "/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_icon_wishlist.tpl" */ ?>
<?php /*%%SmartyHeaderCode:79792342062149c87858a79-45099889%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '271cfae871a36383badfee4abf9ea2aa6280180e' => 
    array (
      0 => '/home/dwaae/public_html/design/themes/ec_dwaae_theme/templates/blocks/static_templates/ec_icon_wishlist.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '79792342062149c87858a79-45099889',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'addons' => 0,
    'block' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149c8787d512_39491206',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149c8787d512_39491206')) {function content_62149c8787d512_39491206($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_count')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.count.php';
if (!is_callable('smarty_function_set_id')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ec_icon_wishlist_div <?php if (!isset($_smarty_tpl->tpl_vars['runtime']) || !is_array($_smarty_tpl->tpl_vars['runtime']->value)) $_smarty_tpl->createLocalArrayVariable('runtime');
if ($_smarty_tpl->tpl_vars['runtime']->value['controller'] = 'product_features'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='compare') {?>selected<?php }?>" id="comparision_list_icon">
    <a href="<?php echo htmlspecialchars(fn_url("product_features.compare"), ENT_QUOTES, 'UTF-8');?>
"><i class="ec-icon-compare"></i></a>
    <?php if (smarty_modifier_count($_SESSION['comparison_list'])>0) {?>
        <span class="ec_wishlist_total_count">
            <?php if (smarty_modifier_count($_SESSION['comparison_list'])>9) {?>
                9+
            <?php } else { ?>
                <?php echo htmlspecialchars(smarty_modifier_count($_SESSION['comparison_list']), ENT_QUOTES, 'UTF-8');?>

            <?php }?>
        </span>
    <?php }?>

<!--comparision_list_icon--></div>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['wishlist']['status']=='A') {?>
<div class="ec_icon_wishlist_div <?php if (!isset($_smarty_tpl->tpl_vars['runtime']) || !is_array($_smarty_tpl->tpl_vars['runtime']->value)) $_smarty_tpl->createLocalArrayVariable('runtime');
if ($_smarty_tpl->tpl_vars['runtime']->value['controller'] = 'wishlist'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view') {?>selected<?php }?>" id="wish_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
">
    <a href="<?php echo htmlspecialchars(fn_url("wishlist.view"), ENT_QUOTES, 'UTF-8');?>
"><i class="ec-icon-wishlist"></i></a>
    <?php if (smarty_modifier_count($_SESSION['wishlist']['products'])>0) {?> 
        <span class="ec_wishlist_total_count">
            <?php if (smarty_modifier_count($_SESSION['wishlist']['products'])>9) {?>
                9+
            <?php } else { ?>
                <?php echo htmlspecialchars(smarty_modifier_count($_SESSION['wishlist']['products']), ENT_QUOTES, 'UTF-8');?>

            <?php }?>
        </span>
    <?php }?>
<!--wish_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
--></div>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/static_templates/ec_icon_wishlist.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/static_templates/ec_icon_wishlist.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ec_icon_wishlist_div <?php if (!isset($_smarty_tpl->tpl_vars['runtime']) || !is_array($_smarty_tpl->tpl_vars['runtime']->value)) $_smarty_tpl->createLocalArrayVariable('runtime');
if ($_smarty_tpl->tpl_vars['runtime']->value['controller'] = 'product_features'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='compare') {?>selected<?php }?>" id="comparision_list_icon">
    <a href="<?php echo htmlspecialchars(fn_url("product_features.compare"), ENT_QUOTES, 'UTF-8');?>
"><i class="ec-icon-compare"></i></a>
    <?php if (smarty_modifier_count($_SESSION['comparison_list'])>0) {?>
        <span class="ec_wishlist_total_count">
            <?php if (smarty_modifier_count($_SESSION['comparison_list'])>9) {?>
                9+
            <?php } else { ?>
                <?php echo htmlspecialchars(smarty_modifier_count($_SESSION['comparison_list']), ENT_QUOTES, 'UTF-8');?>

            <?php }?>
        </span>
    <?php }?>

<!--comparision_list_icon--></div>
<?php if ($_smarty_tpl->tpl_vars['addons']->value['wishlist']['status']=='A') {?>
<div class="ec_icon_wishlist_div <?php if (!isset($_smarty_tpl->tpl_vars['runtime']) || !is_array($_smarty_tpl->tpl_vars['runtime']->value)) $_smarty_tpl->createLocalArrayVariable('runtime');
if ($_smarty_tpl->tpl_vars['runtime']->value['controller'] = 'wishlist'&&$_smarty_tpl->tpl_vars['runtime']->value['mode']=='view') {?>selected<?php }?>" id="wish_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
">
    <a href="<?php echo htmlspecialchars(fn_url("wishlist.view"), ENT_QUOTES, 'UTF-8');?>
"><i class="ec-icon-wishlist"></i></a>
    <?php if (smarty_modifier_count($_SESSION['wishlist']['products'])>0) {?> 
        <span class="ec_wishlist_total_count">
            <?php if (smarty_modifier_count($_SESSION['wishlist']['products'])>9) {?>
                9+
            <?php } else { ?>
                <?php echo htmlspecialchars(smarty_modifier_count($_SESSION['wishlist']['products']), ENT_QUOTES, 'UTF-8');?>

            <?php }?>
        </span>
    <?php }?>
<!--wish_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['snapping_id'], ENT_QUOTES, 'UTF-8');?>
--></div>
<?php }
}?><?php }} ?>
