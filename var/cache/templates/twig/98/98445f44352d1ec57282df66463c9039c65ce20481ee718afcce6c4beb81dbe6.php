<?php

/* __string_template__26b2e7cbad5a0312de565fa820f3aecbee14a276209505db6d49824cc7ce9dff */
class __TwigTemplate_3f8cb663d71c67af5fc18c239d781c743c086924ff17cf632ba89703580b63e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center; font-family: Helvetica, Arial, sans-serif;\"><strong style=\"font-weight: 600;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "display_subtotal", array());
        echo "</strong></p>";
    }

    public function getTemplateName()
    {
        return "__string_template__26b2e7cbad5a0312de565fa820f3aecbee14a276209505db6d49824cc7ce9dff";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center; font-family: Helvetica, Arial, sans-serif;"><strong style="font-weight: 600;">{{ p.display_subtotal }}</strong></p>*/
