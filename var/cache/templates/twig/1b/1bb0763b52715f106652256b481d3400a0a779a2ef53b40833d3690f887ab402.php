<?php

/* __string_template__44c0ace4ba42cdbad9646d518dff6e249a3686d7fa9bcc168e5fa201302b7ed0 */
class __TwigTemplate_b6931ec516ad49ad9c81211969708f838804d80d1ab734acd2bdbedddb504310 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.new_quote_created");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.quote_id");
        echo " #";
        echo $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "rfq_id", array());
    }

    public function getTemplateName()
    {
        return "__string_template__44c0ace4ba42cdbad9646d518dff6e249a3686d7fa9bcc168e5fa201302b7ed0";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("h_rfq.new_quote_created")}}: {{__("h_rfq.quote_id")}} #{{quote.rfq_id}}*/
