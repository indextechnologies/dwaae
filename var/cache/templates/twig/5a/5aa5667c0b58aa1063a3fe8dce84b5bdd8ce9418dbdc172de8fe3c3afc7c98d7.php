<?php

/* __string_template__249349d68c6bf5e0691165039545ec11f34f3dde5c7f6f378a266a96c8f6fad3 */
class __TwigTemplate_5ccac9e3331039d2c8c0d35204448f12781bfaa85b43ba027d884fcea75ebe18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "update_profile_notification")));
        // line 2
        if ($this->getAttribute((isset($context["user_data"]) ? $context["user_data"] : null), "firstname", array())) {
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello_name", array("[name]" => $this->getAttribute((isset($context["user_data"]) ? $context["user_data"] : null), "firstname", array())));
        } else {
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello");
            echo ",";
        }
        // line 3
        echo "    <br>";
        // line 4
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "update_profile_notification_header");
        echo "
    <br>
    <strong>";
        // line 7
        if (((isset($context["api_access_status"]) ? $context["api_access_status"] : null) == "enabled")) {
            // line 8
            echo "        <br>";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "api_access_has_been_enabled");
            echo "<br>";
        } elseif ((        // line 9
(isset($context["api_access_status"]) ? $context["api_access_status"] : null) == "disabled")) {
            // line 10
            echo "        <br>";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "api_access_has_been_disabled");
            echo "<br>";
        }
        // line 12
        echo "    </strong>
    <br>
    <h4 class=\"with-subline\">";
        // line 14
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "user_account_information");
        echo "</h4>
    <table border=\"0\" width=\"100%\" class=\"info\">
      <tr>
        <td><b>";
        // line 17
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "login_url");
        echo ":</b></td>
        <td><a href=\"";
        // line 18
        echo (isset($context["login_url"]) ? $context["login_url"] : null);
        echo "\">";
        echo $this->env->getExtension('tygh.core')->punyDecodeFilter((isset($context["login_url"]) ? $context["login_url"] : null));
        echo "</a></td>
      </tr>
      <tr>
        <td><b>";
        // line 21
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "email");
        echo ":</b></td>
        <td><a>";
        // line 22
        echo $this->getAttribute((isset($context["user_data"]) ? $context["user_data"] : null), "email", array());
        echo "</a></td>
      </tr>
      <tr>
        <td><b>";
        // line 25
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "password");
        echo ":</b></td>
        <td> *********** (<a href=\"";
        // line 26
        echo (isset($context["forgot_pass_url"]) ? $context["forgot_pass_url"] : null);
        echo "\">";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "forgot_password_question");
        echo "</a>)</td>
      </tr>
    </table>";
        // line 29
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__249349d68c6bf5e0691165039545ec11f34f3dde5c7f6f378a266a96c8f6fad3";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 29,  84 => 26,  80 => 25,  74 => 22,  70 => 21,  62 => 18,  58 => 17,  52 => 14,  48 => 12,  43 => 10,  41 => 9,  37 => 8,  35 => 7,  30 => 4,  28 => 3,  21 => 2,  19 => 1,);
    }
}
/* {{ snippet("header", {"title": __("update_profile_notification")} ) }}*/
/*     {% if user_data.firstname %} {{__("hello_name", {"[name]" : user_data.firstname})}} {% else %} {{ __("hello") }}, {% endif %}*/
/*     <br>*/
/*     {{ __("update_profile_notification_header") }}*/
/*     <br>*/
/*     <strong>*/
/*       {% if api_access_status == "enabled" %}*/
/*         <br>{{ __("api_access_has_been_enabled") }}<br>*/
/*       {% elseif api_access_status == "disabled" %}*/
/*         <br>{{ __("api_access_has_been_disabled") }}<br>*/
/*       {% endif %}*/
/*     </strong>*/
/*     <br>*/
/*     <h4 class="with-subline">{{ __("user_account_information") }}</h4>*/
/*     <table border="0" width="100%" class="info">*/
/*       <tr>*/
/*         <td><b>{{ __("login_url") }}:</b></td>*/
/*         <td><a href="{{ login_url }}">{{ login_url|puny_decode }}</a></td>*/
/*       </tr>*/
/*       <tr>*/
/*         <td><b>{{ __("email") }}:</b></td>*/
/*         <td><a>{{ user_data.email }}</a></td>*/
/*       </tr>*/
/*       <tr>*/
/*         <td><b>{{ __("password") }}:</b></td>*/
/*         <td> *********** (<a href="{{ forgot_pass_url }}">{{ __("forgot_password_question") }}</a>)</td>*/
/*       </tr>*/
/*     </table>*/
/*   {{ snippet("footer") }}*/
