<?php

/* __string_template__d3a50c0736282ee45b829ef77b60d03df7815a490fb8a4d836352208d6466918 */
class __TwigTemplate_9215dc7b3c52776e46ca609292b8bb07b664698431ea811a6666c631fa2074a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.prescription_order_assign_for_delivery");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
    }

    public function getTemplateName()
    {
        return "__string_template__d3a50c0736282ee45b829ef77b60d03df7815a490fb8a4d836352208d6466918";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("d_prescription.prescription_order_assign_for_delivery")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
