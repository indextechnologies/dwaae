<?php

/* __string_template__c1a3e2f7a9e2db12c05abf816cfa41fbe4a2e54e6c395d4e62ab6e43206fb30b */
class __TwigTemplate_7ab92f4ce02cff8ef979c5c63044c275e161e558dd61fa95f07ad0858a4c7c9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["company_name"]) ? $context["company_name"] : null);
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "recover_password_subj");
    }

    public function getTemplateName()
    {
        return "__string_template__c1a3e2f7a9e2db12c05abf816cfa41fbe4a2e54e6c395d4e62ab6e43206fb30b";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ company_name }}: {{ __("recover_password_subj") }}*/
