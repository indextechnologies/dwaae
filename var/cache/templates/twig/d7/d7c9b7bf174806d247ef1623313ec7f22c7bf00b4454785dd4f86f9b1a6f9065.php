<?php

/* __string_template__3b8a1fe3d4dedb33f44c357a9737eacc15f1f3a9ce5ed92bc4750c8e2fbb5535 */
class __TwigTemplate_667d18f6e486eabdad7d3e13a57101f2168aa48ab80a008a2a40867def01fb3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["company_name"]) ? $context["company_name"] : null);
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "usergroup_activated");
    }

    public function getTemplateName()
    {
        return "__string_template__3b8a1fe3d4dedb33f44c357a9737eacc15f1f3a9ce5ed92bc4750c8e2fbb5535";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ company_name }}: {{ __("usergroup_activated") }}*/
