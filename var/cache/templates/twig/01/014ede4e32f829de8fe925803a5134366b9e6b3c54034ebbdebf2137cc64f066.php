<?php

/* __string_template__61f93960ff84c210d241c48e9cc11405c81f90642e7a7f06d73713b1fe9764bd */
class __TwigTemplate_5e3ebba834fbd9c803d8f597ee092c46a69b5df84334a0e78853efa63092c475 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "usergroup_activated")));
        // line 2
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "text_usergroup_activated", array("[usergroups]" => (isset($context["usergroups"]) ? $context["usergroups"] : null)));
        // line 3
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__61f93960ff84c210d241c48e9cc11405c81f90642e7a7f06d73713b1fe9764bd";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  21 => 2,  19 => 1,);
    }
}
/* {{ snippet("header", {"title": __("usergroup_activated") } ) }}*/
/*     {{ __("text_usergroup_activated", {"[usergroups]": usergroups}) }}*/
/*   {{ snippet("footer") }}*/
