<?php

/* __string_template__f0e8ecc886e5606ccda9b9d9ff4c66e15c4b0b4e25aaaa2eb9f60ff193210a9f */
class __TwigTemplate_dc63425925b2b34697c700d7c0f44f7345d4ba9faf352d6bff08aae9952285bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_c_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__f0e8ecc886e5606ccda9b9d9ff4c66e15c4b0b4e25aaaa2eb9f60ff193210a9f";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("change_order_status_c_subj", {"[order]": order_info.order_id}) }}*/
