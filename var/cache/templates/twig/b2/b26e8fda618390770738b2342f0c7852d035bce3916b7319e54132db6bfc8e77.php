<?php

/* __string_template__3a61aeb4a814f1a4628c1371e469b2762f0d579f45b157709e1e45c1cb9cb201 */
class __TwigTemplate_8a789966c88bb1f00536929d8f122caa492a4c59a566a9d8e3463a64ab0cfdd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<br/>
<br/>
شكرا لك <br/>
<span class=\"ec-bold\">DWAAE Support Team</span>
<br/>
<br/>
<br/>
</td>

</tr>

<tr class=\"message-footer ec-message-footer\" style=\"border-top:0px;\">
<td style=\"padding-left: 30px; padding-right: 30px;\">
<table style=\"width: 100%; border-bottom: 1px solid #918a8a;margin-bottom: 5px;\">
<tr>
<td>";
        // line 17
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "customer_text_letter_footer");
        echo "
</td>
</tr>
</table>
<table class=\"info ty-email-footer-left-part\" width=\"250\" align=\"left\" >
<tr>
</tr>
  <tr>
    <td>
<a href=\"https://www.facebook.com/dwaae.ae\"><img src=\"http://dwaae.com/images/mailicon/facebook.png\" style=\"width:31px;height:31px;\" /></a>
<a href=\"https://twitter.com/Dwaae_com\"><img src=\"http://dwaae.com/images/mailicon/twitter.png\" style=\"width:31px;height:31px;\" /></a>
<a href=\"https://www.instagram.com/dwaae.ae/\"><img src=\"http://dwaae.com/images/mailicon/instagram.png\" style=\"width:31px;height:31px;\" /></a>
</td>
  </tr>
  </table>

<table class=\"info ty-email-footer-right-part\" width=\"250\" align=\"right\">
  <tr>
    <th class=\"\" style=\"text-align: right;\">
<a href=\"https://play.google.com/store/apps/details?id=com.dwaae.smartapp\"><img src=\"http://dwaae.com/images/mailicon/googleplay.png\" style=\"width:115px;height:34px;\" /></a>
<a href=\"https://apps.apple.com/in/app/dwaae-دوائي/id1522286328\"><img src=\"http://dwaae.com/images/mailicon/appstore.png\" style=\"width:98px;height:34px;\" /></a>
    </th>
  </tr>
  <tr>
    <td class=\"ty-email-footer-social-buttons footer-social\">
      <table cellspacing=\"0\" cellpadding=\"0\" align=\"center\">
        <tr>
          <td>
            
          </td>
         
        </tr>
      </table>
    </td>
  </tr>
</table>
</td>
</tr>";
        // line 55
        if ($this->getAttribute((isset($context["company_data"]) ? $context["company_data"] : null), "company_name", array())) {
            // line 56
            echo "<tr class=\"message-copyright\">
<td>
<table class=\"copyright\" width=\"100%\">
<tr>
  <td style=\"letter-spacing: 0px; color: #393939; opacity: 0.5; text-align: center;\">
<br/>
<br/>
    <span>This is an automatically generated email. Please do not reply.</span><br/>
<span>Phone: (+971) 2 644 8474, (+971) 2 666 3824  Email: info@digitalhealth.ae</span>
  </td>
  
</tr>
</table>
</td>
</tr>";
        }
        // line 72
        echo "</table>
<!-- content-wrapper -->
</td>
</tr>
</table>
<!-- main-wrapper -->
<script async src=\"https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4063429910709224\"
     crossorigin=\"anonymous\"></script>
<!-- click here -->
<ins class=\"adsbygoogle\"
     style=\"display:block\"
     data-ad-client=\"ca-pub-4063429910709224\"
     data-ad-slot=\"4146788712\"
     data-ad-format=\"auto\"
     data-full-width-responsive=\"true\"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "__string_template__3a61aeb4a814f1a4628c1371e469b2762f0d579f45b157709e1e45c1cb9cb201";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 72,  78 => 56,  76 => 55,  36 => 17,  19 => 1,);
    }
}
/* <br/>*/
/* <br/>*/
/* شكرا لك <br/>*/
/* <span class="ec-bold">DWAAE Support Team</span>*/
/* <br/>*/
/* <br/>*/
/* <br/>*/
/* </td>*/
/* */
/* </tr>*/
/* */
/* <tr class="message-footer ec-message-footer" style="border-top:0px;">*/
/* <td style="padding-left: 30px; padding-right: 30px;">*/
/* <table style="width: 100%; border-bottom: 1px solid #918a8a;margin-bottom: 5px;">*/
/* <tr>*/
/* <td>*/
/* {{ __("customer_text_letter_footer") }}*/
/* </td>*/
/* </tr>*/
/* </table>*/
/* <table class="info ty-email-footer-left-part" width="250" align="left" >*/
/* <tr>*/
/* </tr>*/
/*   <tr>*/
/*     <td>*/
/* <a href="https://www.facebook.com/dwaae.ae"><img src="http://dwaae.com/images/mailicon/facebook.png" style="width:31px;height:31px;" /></a>*/
/* <a href="https://twitter.com/Dwaae_com"><img src="http://dwaae.com/images/mailicon/twitter.png" style="width:31px;height:31px;" /></a>*/
/* <a href="https://www.instagram.com/dwaae.ae/"><img src="http://dwaae.com/images/mailicon/instagram.png" style="width:31px;height:31px;" /></a>*/
/* </td>*/
/*   </tr>*/
/*   </table>*/
/* */
/* <table class="info ty-email-footer-right-part" width="250" align="right">*/
/*   <tr>*/
/*     <th class="" style="text-align: right;">*/
/* <a href="https://play.google.com/store/apps/details?id=com.dwaae.smartapp"><img src="http://dwaae.com/images/mailicon/googleplay.png" style="width:115px;height:34px;" /></a>*/
/* <a href="https://apps.apple.com/in/app/dwaae-دوائي/id1522286328"><img src="http://dwaae.com/images/mailicon/appstore.png" style="width:98px;height:34px;" /></a>*/
/*     </th>*/
/*   </tr>*/
/*   <tr>*/
/*     <td class="ty-email-footer-social-buttons footer-social">*/
/*       <table cellspacing="0" cellpadding="0" align="center">*/
/*         <tr>*/
/*           <td>*/
/*             */
/*           </td>*/
/*          */
/*         </tr>*/
/*       </table>*/
/*     </td>*/
/*   </tr>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* {% if company_data.company_name %}*/
/* <tr class="message-copyright">*/
/* <td>*/
/* <table class="copyright" width="100%">*/
/* <tr>*/
/*   <td style="letter-spacing: 0px; color: #393939; opacity: 0.5; text-align: center;">*/
/* <br/>*/
/* <br/>*/
/*     <span>This is an automatically generated email. Please do not reply.</span><br/>*/
/* <span>Phone: (+971) 2 644 8474, (+971) 2 666 3824  Email: info@digitalhealth.ae</span>*/
/*   </td>*/
/*   */
/* </tr>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* {% endif %}*/
/* </table>*/
/* <!-- content-wrapper -->*/
/* </td>*/
/* </tr>*/
/* </table>*/
/* <!-- main-wrapper -->*/
/* <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4063429910709224"*/
/*      crossorigin="anonymous"></script>*/
/* <!-- click here -->*/
/* <ins class="adsbygoogle"*/
/*      style="display:block"*/
/*      data-ad-client="ca-pub-4063429910709224"*/
/*      data-ad-slot="4146788712"*/
/*      data-ad-format="auto"*/
/*      data-full-width-responsive="true"></ins>*/
/* <script>*/
/*      (adsbygoogle = window.adsbygoogle || []).push({});*/
/* </script>*/
/* </body>*/
/* </html>*/
