<?php

/* __string_template__cec4b885ea99518b0b92ec9f196365d63508aed6e455c0694ff768b24621f5f8 */
class __TwigTemplate_5d2195485b6eb2e0663e8cf8f558e1ab641bdcbc6cdbe298af81bc41cefdd1d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p style=\"text-align: center; font-family: Helvetica, Arial, sans-serif;\"><strong style=\"font-weight: 600;\">";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "original_price", array());
        echo "</strong></p>";
    }

    public function getTemplateName()
    {
        return "__string_template__cec4b885ea99518b0b92ec9f196365d63508aed6e455c0694ff768b24621f5f8";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p style="text-align: center; font-family: Helvetica, Arial, sans-serif;"><strong style="font-weight: 600;">{{ p.original_price }}</strong></p>*/
