<?php

/* __string_template__46acfa777f6220ae625ce56710a2172f1c85ff0b967c6a9b4532d74c0bc81754 */
class __TwigTemplate_0b985e27d8696f03ac6ae2f9a4a4fa9366d0fc93bdc2ac4a67c7a577d868eaf3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "change_order_status_d_subj", array("[order]" => $this->getAttribute((isset($context["order_info"]) ? $context["order_info"] : null), "order_id", array())));
    }

    public function getTemplateName()
    {
        return "__string_template__46acfa777f6220ae625ce56710a2172f1c85ff0b967c6a9b4532d74c0bc81754";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{ __("change_order_status_d_subj", {"[order]": order_info.order_id}) }}*/
