<?php

/* __string_template__40ce966e4968d2fa71d4e9d96ac4b40c638d6b6ff25a0ad696a5b4ffe6b1d251 */
class __TwigTemplate_fd995969c18b3278d75c4966b98a344f9aff591d4a091e00ab524047d6c165e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.new_order_created");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 5
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => (isset($context["title"]) ? $context["title"] : null)));
        echo "
            <p>";
        // line 7
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello_name", array("[name]" => $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "firstname", array())));
        echo "<br/>";
        // line 8
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.your_prescription_order_recived", array("[req_id]" => $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array())));
        echo "<br/>";
        // line 9
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.view_your_request", array("[link]" => (isset($context["link"]) ? $context["link"] : null)));
        echo "<br/>
            </p><br/>";
        // line 11
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__40ce966e4968d2fa71d4e9d96ac4b40c638d6b6ff25a0ad696a5b4ffe6b1d251";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 11,  38 => 9,  35 => 8,  32 => 7,  28 => 5,  21 => 3,  19 => 2,);
    }
}
/* */
/*             {% set title %}*/
/*             {{__("d_prescription.new_order_created")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
/*             {% endset %}*/
/*             {{ snippet("header", {"title": title }) }}*/
/*             <p>*/
/*                 {{__("hello_name", {"[name]" : prescription.firstname})}}<br/>*/
/*                 {{__("d_prescription.your_prescription_order_recived", {"[req_id]" : prescription.req_id})}}<br/>*/
/*                 {{__("d_prescription.view_your_request", {"[link]" : link})}}<br/>*/
/*             </p><br/>*/
/*                   {{ snippet("footer") }}*/
