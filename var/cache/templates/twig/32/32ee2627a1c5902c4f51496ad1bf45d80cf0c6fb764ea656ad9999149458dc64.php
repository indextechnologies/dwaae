<?php

/* __string_template__61cd6c75281a43f54e544a7cae48dec84a6c4e304cb663303773296674eff346 */
class __TwigTemplate_b2affb3cd02fa601e14512ee29277449924971dcdf44a6a0b4cf05b71170509a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo (isset($context["msg_subject"]) ? $context["msg_subject"] : null);
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 5
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => (isset($context["title"]) ? $context["title"] : null)));
        echo "
            <p>";
        // line 7
        echo (isset($context["message"]) ? $context["message"] : null);
        echo "
            </p><br/>";
        // line 9
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__61cd6c75281a43f54e544a7cae48dec84a6c4e304cb663303773296674eff346";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 9,  28 => 7,  24 => 5,  21 => 3,  19 => 2,);
    }
}
/* */
/*             {% set title %}*/
/*                 {{msg_subject}}*/
/*             {% endset %}*/
/*             {{ snippet("header", {"title": title }) }}*/
/*             <p>*/
/*                 {{message}}*/
/*             </p><br/>*/
/*                   {{ snippet("footer") }}*/
