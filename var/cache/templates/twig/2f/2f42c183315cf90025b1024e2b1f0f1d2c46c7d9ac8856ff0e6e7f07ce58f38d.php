<?php

/* __string_template__f1a7e40fa737bc055193f25bf9e2868c2bfb509d4efb7685b3a82d2bb8e34183 */
class __TwigTemplate_e3785da0edd9eb98e3a1911a8557231e9a48790de653fbfac472b3ccaaab72e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Contact By:";
        echo (isset($context["name"]) ? $context["name"] : null);
    }

    public function getTemplateName()
    {
        return "__string_template__f1a7e40fa737bc055193f25bf9e2868c2bfb509d4efb7685b3a82d2bb8e34183";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* Contact By: {{name}}*/
