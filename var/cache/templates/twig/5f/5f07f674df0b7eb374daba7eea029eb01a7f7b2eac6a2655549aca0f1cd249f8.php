<?php

/* __string_template__752d71ba49a27e29c90545c777871f77297e473a4764093112c837e593c9a5cc */
class __TwigTemplate_3d7e7ceae06b0f451f24446fa2bdac6ce73980047dd01e634cf955779cd13599 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table style=\"min-width: 800px; font-family: Helvetica, Arial, sans-serif; border-collapse: separate;\" border=\"0\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr style=\"vertical-align: top;\">
<td>
<table style=\"border-collapse: separate; font-family: Helvetica, Arial, sans-serif;\" border=\"0\" width=\"100%;\" cellspacing=\"0\">
<tbody>
<tr>
<td style=\"padding-bottom: 40px; vertical-align: top; font-family: Helvetica, Arial, sans-serif;\" width=\"66%\"><img src=\"http://dwaae.com/images/dwaae_orignal_logo3x.png\" alt=\"\" width=\"198\" height=\"113\" /></td>
<td style=\"padding-bottom: 40px; -webkit-print-color-adjust: exact; font-family: Helvetica, Arial, sans-serif;\" width=\"34%\">
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 3px; margin: 0px;\"><span style=\"color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase; line-height: 20px; font-size: 14pt;\">TAx Invoice</span></p>
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 3px; margin: 0px;\"> </p>
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 3px; margin: 0px;\"><span style=\"color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase; line-height: 20px;\">";
        // line 12
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "invoice_id_text", array());
        echo "</span></p>
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;\"><span style=\"color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;\">";
        // line 13
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "order_date");
        echo "</span>";
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "timestamp", array());
        echo "</p>
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;\"><span style=\"color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;\">";
        // line 14
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "payment");
        echo "</span>";
        echo $this->getAttribute((isset($context["p"]) ? $context["p"] : null), "payment", array());
        echo "</p>
<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;\"><span style=\"color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;\">";
        // line 15
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "shipping");
        echo "</span>";
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "shippings_method", array());
        echo "</p>";
        // line 16
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "tracking_number", array())) {
            // line 17
            echo "<p style=\"color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;\"><span style=\"color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "tracking_number");
            echo "</span>";
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "tracking_number", array());
            echo "</p>";
        }
        // line 18
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td colspan=\"2\">
<table style=\"border-collapse: separate; font-family: Helvetica, Arial, sans-serif;\" border=\"0\" width=\"100%;\" cellspacing=\"0\" cellpadding=\"0\">
<tbody>
<tr>
<td style=\"vertical-align: top; background-color: #f7f7f7; color: #444444; padding: 20px 10px; font-size: 14px; font-family: Helvetica, Arial, sans-serif; -webkit-print-color-adjust: exact;\" width=\"30%\">
<h1 style=\"margin: 0px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-transform: uppercase; padding-bottom: 20px; border-bottom: 3px solid #444444; margin-bottom: 20px;\">";
        // line 30
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "store");
        echo "</h1>
<p style=\"margin: 0px; padding-bottom: 10px;\"><strong>";
        // line 31
        echo $this->getAttribute((isset($context["c"]) ? $context["c"] : null), "name", array());
        echo "</strong></p>";
        // line 32
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "company_address");
        echo "</td>
<td style=\"vertical-align: top; padding: 20px 30px; color: #444444; font-size: 14px; font-family: Helvetica, Arial, sans-serif;\" width=\"36%\">";
        // line 33
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "bill_to");
        echo "</td>
<td style=\"vertical-align: top; padding: 20px 0px; color: #444444; font-size: 14px; font-family: Helvetica, Arial, sans-serif;\" width=\"34%\">";
        // line 34
        if ($this->getAttribute((isset($context["pickup_point"]) ? $context["pickup_point"] : null), "is_selected", array())) {
            echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "pickup_point");
        } else {
            echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "ship_to");
        }
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style=\"font-family: Helvetica, Arial, sans-serif;\">";
        // line 41
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "products_table");
        echo "</td>
</tr>
<tr>
<td style=\"border-top: 3px solid #444444; padding-top: 10px; font-family: Helvetica, Arial, sans-serif;\">
<table style=\"border-collapse: separate; font-family: Helvetica, Arial, sans-serif;\" width=\"100%\">
<tbody>
<tr>
<td style=\"font-size: 14px; font-family: Helvetica, Arial, sans-serif; line-height: 21px; color: #444444; padding-right: 30px; vertical-align: top;\" width=\"66%\">";
        // line 48
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "notes", array())) {
            // line 49
            echo "<h2 style=\"margin: 0px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-transform: uppercase; padding-bottom: 20px; border-bottom: 3px solid #e8e8e8; margin-bottom: 10px;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "customer_notes");
            echo "</h2>";
            // line 50
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "notes", array());
        }
        echo "</td>
<td style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\" width=\"34%\">
<table style=\"font-size: 14px; color: #444; font-family: Helvetica, Arial, sans-serif;\" width=\"100%;\">
<tbody>
<tr style=\"vertical-align: top;\">
<td style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 55
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "subtotal");
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 56
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "display_subtotal", array());
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"padding-bottom: 20px; text-transform: uppercase; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 59
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "tax_name", array());
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 60
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "tax_total", array());
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 63
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "shipping");
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 64
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "display_shipping_cost", array());
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 67
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "payment_surcharge", array())) {
            // line 68
            echo "<div style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "payment_surcharge");
            echo "</div>";
        }
        // line 69
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 70
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "payment_surcharge", array())) {
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "payment_surcharge", array());
        }
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td align=\"left\">";
        // line 73
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "coupon_code", array())) {
            // line 74
            echo "<div style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "coupon");
            echo "</div>";
        }
        // line 75
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 76
        if ($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "coupon_code", array())) {
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "coupon_code", array());
        }
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 79
        if ($this->getAttribute($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "raw", array()), "discount", array())) {
            // line 80
            echo "<div style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "including_discount");
            echo "</div>";
        }
        // line 81
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 82
        if ($this->getAttribute($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "raw", array()), "discount", array())) {
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "discount", array());
        }
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-family: Helvetica, Arial, sans-serif;\">
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 85
        if ($this->getAttribute($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "raw", array()), "subtotal_discount", array())) {
            // line 86
            echo "<div style=\"padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;\">";
            echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "order_discount");
            echo "</div>";
        }
        // line 87
        echo "</td>
<td style=\"font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 88
        if ($this->getAttribute($this->getAttribute((isset($context["o"]) ? $context["o"] : null), "raw", array()), "subtotal_discount", array())) {
            echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "subtotal_discount", array());
        }
        echo "</td>
</tr>
<tr style=\"vertical-align: top; font-size: 22px; font-family: Helvetica, Arial, sans-serif; font-weight: 600;\">
<td style=\"padding-top: 20px; border-top: 1px solid #e8e8e8; font-size: 22px; font-family: Helvetica, Arial, sans-serif;\" align=\"left\">";
        // line 91
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "total");
        echo "</td>
<td style=\"padding-top: 20px; border-top: 1px solid #e8e8e8; font-size: 22px; font-family: Helvetica, Arial, sans-serif;\" align=\"right\">";
        // line 92
        echo $this->getAttribute((isset($context["o"]) ? $context["o"] : null), "total", array());
        echo "</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>";
    }

    public function getTemplateName()
    {
        return "__string_template__752d71ba49a27e29c90545c777871f77297e473a4764093112c837e593c9a5cc";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 92,  232 => 91,  224 => 88,  221 => 87,  216 => 86,  214 => 85,  206 => 82,  203 => 81,  198 => 80,  196 => 79,  188 => 76,  185 => 75,  180 => 74,  178 => 73,  170 => 70,  167 => 69,  162 => 68,  160 => 67,  154 => 64,  150 => 63,  144 => 60,  140 => 59,  134 => 56,  130 => 55,  121 => 50,  117 => 49,  115 => 48,  105 => 41,  91 => 34,  87 => 33,  83 => 32,  80 => 31,  76 => 30,  62 => 18,  55 => 17,  53 => 16,  48 => 15,  42 => 14,  36 => 13,  32 => 12,  19 => 1,);
    }
}
/* <table style="min-width: 800px; font-family: Helvetica, Arial, sans-serif; border-collapse: separate;" border="0" width="100%;" cellspacing="0" cellpadding="0">*/
/* <tbody>*/
/* <tr style="vertical-align: top;">*/
/* <td>*/
/* <table style="border-collapse: separate; font-family: Helvetica, Arial, sans-serif;" border="0" width="100%;" cellspacing="0">*/
/* <tbody>*/
/* <tr>*/
/* <td style="padding-bottom: 40px; vertical-align: top; font-family: Helvetica, Arial, sans-serif;" width="66%"><img src="http://dwaae.com/images/dwaae_orignal_logo3x.png" alt="" width="198" height="113" /></td>*/
/* <td style="padding-bottom: 40px; -webkit-print-color-adjust: exact; font-family: Helvetica, Arial, sans-serif;" width="34%">*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 3px; margin: 0px;"><span style="color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase; line-height: 20px; font-size: 14pt;">TAx Invoice</span></p>*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 3px; margin: 0px;"> </p>*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 3px; margin: 0px;"><span style="color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase; line-height: 20px;">{{ o.invoice_id_text }}</span></p>*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;"><span style="color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;">{{__("order_date")}}</span> {{o.timestamp}}</p>*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;"><span style="color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;">{{__("payment")}}</span> {{p.payment}}</p>*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;"><span style="color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;">{{__("shipping")}}</span> {{o.shippings_method}}</p>*/
/* {% if o.tracking_number %}*/
/* <p style="color: #787878; font-size: 14px; font-family: Helvetica, Arial, sans-serif; padding-bottom: 5px; margin: 0px;"><span style="color: #000000; font-weight: 600; font-family: Helvetica, Arial, sans-serif; text-transform: uppercase;">{{__("tracking_number")}}</span> {{o.tracking_number}}</p>*/
/* {% endif %}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr>*/
/* <td colspan="2">*/
/* <table style="border-collapse: separate; font-family: Helvetica, Arial, sans-serif;" border="0" width="100%;" cellspacing="0" cellpadding="0">*/
/* <tbody>*/
/* <tr>*/
/* <td style="vertical-align: top; background-color: #f7f7f7; color: #444444; padding: 20px 10px; font-size: 14px; font-family: Helvetica, Arial, sans-serif; -webkit-print-color-adjust: exact;" width="30%">*/
/* <h1 style="margin: 0px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-transform: uppercase; padding-bottom: 20px; border-bottom: 3px solid #444444; margin-bottom: 20px;">{{__("store")}}</h1>*/
/* <p style="margin: 0px; padding-bottom: 10px;"><strong>{{c.name}}</strong></p>*/
/* {{ snippet("company_address") }}</td>*/
/* <td style="vertical-align: top; padding: 20px 30px; color: #444444; font-size: 14px; font-family: Helvetica, Arial, sans-serif;" width="36%">{{ snippet("bill_to") }}</td>*/
/* <td style="vertical-align: top; padding: 20px 0px; color: #444444; font-size: 14px; font-family: Helvetica, Arial, sans-serif;" width="34%">{% if pickup_point.is_selected %} {{ snippet("pickup_point") }} {% else %} {{ snippet("ship_to") }} {% endif %}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* <tr>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;">{{ snippet("products_table") }}</td>*/
/* </tr>*/
/* <tr>*/
/* <td style="border-top: 3px solid #444444; padding-top: 10px; font-family: Helvetica, Arial, sans-serif;">*/
/* <table style="border-collapse: separate; font-family: Helvetica, Arial, sans-serif;" width="100%">*/
/* <tbody>*/
/* <tr>*/
/* <td style="font-size: 14px; font-family: Helvetica, Arial, sans-serif; line-height: 21px; color: #444444; padding-right: 30px; vertical-align: top;" width="66%">{% if o.notes %}*/
/* <h2 style="margin: 0px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #444444; text-transform: uppercase; padding-bottom: 20px; border-bottom: 3px solid #e8e8e8; margin-bottom: 10px;">{{ __("customer_notes") }}</h2>*/
/* {{ o.notes }} {% endif %}</td>*/
/* <td style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;" width="34%">*/
/* <table style="font-size: 14px; color: #444; font-family: Helvetica, Arial, sans-serif;" width="100%;">*/
/* <tbody>*/
/* <tr style="vertical-align: top;">*/
/* <td style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;" align="left">{{ __("subtotal") }}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{{o.display_subtotal}}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="padding-bottom: 20px; text-transform: uppercase; font-family: Helvetica, Arial, sans-serif;" align="left">{{o.tax_name}}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{{o.tax_total}}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;" align="left">{{ __("shipping") }}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{{ o.display_shipping_cost }}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="left">{% if o.payment_surcharge %}*/
/* <div style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;">{{__("payment_surcharge")}}</div>*/
/* {% endif %}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{% if o.payment_surcharge %} {{o.payment_surcharge}} {% endif %}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td align="left">{% if o.coupon_code %}*/
/* <div style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;">{{ __("coupon") }}</div>*/
/* {% endif %}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{% if o.coupon_code %} {{o.coupon_code}} {% endif %}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="left">{% if o.raw.discount %}*/
/* <div style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;">{{ __("including_discount") }}</div>*/
/* {% endif %}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{% if o.raw.discount %} {{o.discount}} {% endif %}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-family: Helvetica, Arial, sans-serif;">*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="left">{% if o.raw.subtotal_discount %}*/
/* <div style="padding-bottom: 20px; font-family: Helvetica, Arial, sans-serif;">{{ __("order_discount") }}</div>*/
/* {% endif %}</td>*/
/* <td style="font-family: Helvetica, Arial, sans-serif;" align="right">{% if o.raw.subtotal_discount %} {{o.subtotal_discount}} {% endif %}</td>*/
/* </tr>*/
/* <tr style="vertical-align: top; font-size: 22px; font-family: Helvetica, Arial, sans-serif; font-weight: 600;">*/
/* <td style="padding-top: 20px; border-top: 1px solid #e8e8e8; font-size: 22px; font-family: Helvetica, Arial, sans-serif;" align="left">{{ __("total") }}</td>*/
/* <td style="padding-top: 20px; border-top: 1px solid #e8e8e8; font-size: 22px; font-family: Helvetica, Arial, sans-serif;" align="right">{{o.total}}</td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
/* </td>*/
/* </tr>*/
/* </tbody>*/
/* </table>*/
