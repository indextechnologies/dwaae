<?php

/* __string_template__bd5736414aa809662b2e91b3e58856948de5bff808628043857ee264fb3e78cb */
class __TwigTemplate_7577683e26679b97ddb337e45916db988805d09e198b1c5b08efaf5b6359371f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        ob_start();
        // line 3
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.new_quote_created");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.quote_id");
        echo " #";
        echo $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "rfq_id", array());
        $context["title"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 5
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "header", array("title" => (isset($context["title"]) ? $context["title"] : null)));
        echo "
            <p>";
        // line 7
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "hello_name", array("[name]" => $this->getAttribute((isset($context["quote"]) ? $context["quote"] : null), "firstname", array())));
        echo "<br/>";
        // line 8
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.thanks_for_create_the_quote");
        echo "<br/>";
        // line 9
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "h_rfq.view_your_request", array("[link]" => (isset($context["quote_link"]) ? $context["quote_link"] : null)));
        echo "<br/>
            </p><br/>";
        // line 11
        echo $this->env->getExtension('tygh.core')->snippetFunction($this->env, $context, "footer");
    }

    public function getTemplateName()
    {
        return "__string_template__bd5736414aa809662b2e91b3e58856948de5bff808628043857ee264fb3e78cb";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 11,  38 => 9,  35 => 8,  32 => 7,  28 => 5,  21 => 3,  19 => 2,);
    }
}
/* */
/*             {% set title %}*/
/*             {{__("h_rfq.new_quote_created")}}: {{__("h_rfq.quote_id")}} #{{quote.rfq_id}}*/
/*             {% endset %}*/
/*             {{ snippet("header", {"title": title }) }}*/
/*             <p>*/
/*                 {{__("hello_name", {"[name]" : quote.firstname})}}<br/>*/
/*                 {{__("h_rfq.thanks_for_create_the_quote")}}<br/>*/
/*                 {{__("h_rfq.view_your_request", {"[link]" : quote_link})}}<br/>*/
/*             </p><br/>*/
/*                   {{ snippet("footer") }}*/
