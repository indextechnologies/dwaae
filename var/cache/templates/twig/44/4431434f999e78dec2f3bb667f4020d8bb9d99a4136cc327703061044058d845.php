<?php

/* __string_template__53736233007e91f704f142f8fcbf4e3cc7b7b570a4fef19410f03e4b5ec5f80b */
class __TwigTemplate_3c94ac1127f0d6f456810cf1fdbe3114a7b42cccad18d9e9bf87bff6f3ec6c1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.new_order_created");
        echo ":";
        echo $this->env->getExtension('tygh.core')->translateFunction($this->env, $context, "d_prescription.req_id");
        echo " #";
        echo $this->getAttribute((isset($context["prescription"]) ? $context["prescription"] : null), "req_id", array());
    }

    public function getTemplateName()
    {
        return "__string_template__53736233007e91f704f142f8fcbf4e3cc7b7b570a4fef19410f03e4b5ec5f80b";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* {{__("d_prescription.new_order_created")}}: {{__("d_prescription.req_id")}} #{{prescription.req_id}}*/
