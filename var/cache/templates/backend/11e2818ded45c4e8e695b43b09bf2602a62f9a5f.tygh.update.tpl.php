<?php /* Smarty version Smarty-3.1.21, created on 2022-02-28 13:25:41
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_prescription/views/d_prescription/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1565036984621c9515eca611-95758672%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '11e2818ded45c4e8e695b43b09bf2602a62f9a5f' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_prescription/views/d_prescription/update.tpl',
      1 => 1642492101,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1565036984621c9515eca611-95758672',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'no_hide_input_if_shared_product' => 0,
    'is_clinic' => 0,
    'prescription' => 0,
    'settings' => 0,
    'link' => 0,
    'loc' => 0,
    'company_details' => 0,
    'p' => 0,
    'insurance_companies' => 0,
    'insurance_company' => 0,
    'delivery' => 0,
    'decimal' => 0,
    'product' => 0,
    'primary_currency' => 0,
    'currencies' => 0,
    'allow_to_update_details' => 0,
    'id' => 0,
    'auth' => 0,
    'config' => 0,
    'delivery_details' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621c95160b17b2_35522706',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621c95160b17b2_35522706')) {function content_621c95160b17b2_35522706($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_enum')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.enum.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_prescription.order_details','d_prescription.paid_by_card','d_prescription.transaction_id','d_prescription.cash_on_delivery_des','d_prescription.patient_details','d_prescription.firstname','d_prescription.lastname','email','phone','d_prescription.prescription_details','d_prescription.prescriptions_attachment','customer_note','d_prescription.e_prescription_no','d_prescription.doctor_emirates','d_prescription.insurance_company','delivery_address','d_prescription.emirates_id_and_insurance_card','d_prescription.insurance_card','d_prescription.no_insurance','d_prescription.emirate_id_is_insurance','d_prescription.seprate_insurance','d_prescription.customer_comment','d_prescription.copyament_percentage','d_prescription.pharmacy_discount','d_prescription.reponse_from_insurance_companies','h_rfq.total_vat','h_rfq.non_tax_price_total','h_rfq.tax_price_total','h_rfq.total_vat','h_rfq.non_tax_price_total','h_rfq.tax_price_total','h_rfq.add_product','totals','subtotal','shipping_cost','h_rfq.vat','total','h_rfq.add_product','d_prescription.print_invoice','d_prescription.print_invoice_pdf','d_prescription.clone_prescription','d_prescription.clone_prescription','d_prescription.prescription_request'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
    <style>
        .d-cancel_button{
            background-color: rgb(238, 64, 64);
            color: white;
            border: 1px solid rgb(238, 64, 64);
        }
    </style>
    <?php $_smarty_tpl->tpl_vars['allow_to_update_details'] = new Smarty_variable(true, null, 0);?>
    <?php if ($_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
        <?php $_smarty_tpl->tpl_vars['allow_to_update_details'] = new Smarty_variable(false, null, 0);?>
    <?php }?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("reject_block", null, null); ob_start(); ?>
        <form name="reject_order_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" class="form-horizontal form-edit ">
            <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_REQUEST['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
            <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label for="reject_comment" class="control-label cm-required">Reason to Reject:</label>
                <div class="controls">
                    <textarea name="reject_comment" id="reject_comment" style="width=100%"></textarea>
                </div>
            </div>
            <div class="buttons-container">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[d_prescription.cancel_order]",'cancel_action'=>"close",'but_text'=>"Reject Order"), 0);?>

            </div>
        </form>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("tabsbox", null, null); ob_start(); ?>
    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="prescription_info_form" id="prescription_info_form" class="form-horizontal form-edit " enctype="multipart/form-data">
        <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_REQUEST['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
        <input type="hidden" name="result_ids" value="content_general" />
        <input type="hidden" name="selected_section" value="<?php echo htmlspecialchars($_REQUEST['selected_section'], ENT_QUOTES, 'UTF-8');?>
" />
        <div id="content_detailed" class="hidden">
            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.order_details"),'target'=>"#acc_order_details"), 0);?>

            <div id="acc_order_details" class="collapse in collapse-visible">
            <?php if (!$_smarty_tpl->tpl_vars['is_clinic']->value) {?>
                <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']!=smarty_modifier_enum("PrescriptionStatus::NEW_REQUEST")) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("views/companies/components/company_field.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('name'=>"prescription[company_id]",'id'=>"prescription_company_id",'selected'=>$_smarty_tpl->tpl_vars['prescription']->value['company_id'],'tooltip'=>'','required'=>true,'zero_company_id_name_lang_var'=>"select_pharmacy"), 0);?>

                <?php } else { ?>
                    <small>First verify the order. If the order is not fake. Then click on Open Order to Assign Pharmacy.</small>
                <?php }?>
            <?php }?>
                <?php if (@constant('ACCOUNT_TYPE')=='admin') {?>
                <div class="control-group">
                    <div class="control-label">Date</div>
                    <div class="controls">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_date",'date_name'=>"prescription[create_time]",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['prescription']->value['create_time'])===null||$tmp==='' ? '' : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year']), 0);?>

                    </div>
                </div>
                <?php }?>

                <div class="control-group">
                    <div class="control-label">Payment Details</div>
                    <div class="controls">
                    <?php if ($_smarty_tpl->tpl_vars['prescription']->value['paid_by_card']=="Y") {?>
                        <?php echo $_smarty_tpl->__("d_prescription.paid_by_card");
if ($_smarty_tpl->tpl_vars['prescription']->value['payment_response']['transaction_id']) {?>&nbsp;&nbsp;(<?php echo $_smarty_tpl->__("d_prescription.transaction_id");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['payment_response']['transaction_id'], ENT_QUOTES, 'UTF-8');?>
)<?php }?>
                    <?php } else { ?>
                        <?php echo $_smarty_tpl->__("d_prescription.cash_on_delivery_des");?>

                    <?php }?>
                    </div>
                </div>
            </div>

            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['delivery_key']) {?>
                <div class="control-group">
                    <div class="control-label">Delivery Acknowledgement Link</div>
                    <div class="controls">
                        <?php $_smarty_tpl->tpl_vars['link'] = new Smarty_variable(fn_url("delivery_details.prescription_order&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&key=".((string)$_smarty_tpl->tpl_vars['prescription']->value['delivery_key']),"C"), null, 0);?>
                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                    </div>
                </div>

                <div>
                    <div style="    font-weight: bold;">Details for Driver:</div>
                    <div style="background: #f7f7f7;padding: 20px;border: 1px solid;" >
                    <span style="font-size:15px;">Customer Details:</span><br/>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_firstname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_lastname'], ENT_QUOTES, 'UTF-8');?>
<br/>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_address'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_address_2'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_city'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars(fn_get_state_name($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_state'],$_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_country']), ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_zipcode'], ENT_QUOTES, 'UTF-8');?>
 
                    <br/>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['s_phone'], ENT_QUOTES, 'UTF-8');?>

                    <br/>
                    <?php if ($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['ec_lat']&&$_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['ec_lng']) {?>
                        <?php $_smarty_tpl->tpl_vars['loc'] = new Smarty_variable("https://www.google.com/maps/search/?api=1&query=".((string)$_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['ec_lat']).",".((string)$_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']['ec_lng']), null, 0);?>
                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['loc']->value, ENT_QUOTES, 'UTF-8');?>
" target="_blank"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['loc']->value, ENT_QUOTES, 'UTF-8');?>
</a><br/>
                    <?php }?> 
                    <?php if ($_smarty_tpl->tpl_vars['company_details']->value['company_id']) {?>
                        <span style="font-size:15px;">Pickup Details:</span><br/>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_details']->value['company_full_name'], ENT_QUOTES, 'UTF-8');?>
<br/>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_details']->value['address'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_details']->value['city'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars(fn_get_state_name($_smarty_tpl->tpl_vars['company_details']->value['state'],$_smarty_tpl->tpl_vars['company_details']->value['country']), ENT_QUOTES, 'UTF-8');?>

                        , <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_details']->value['zipcode'], ENT_QUOTES, 'UTF-8');?>
<br/>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_details']->value['phone'], ENT_QUOTES, 'UTF-8');?>
<br/>
                        <?php if ($_smarty_tpl->tpl_vars['company_details']->value['latitude']&&$_smarty_tpl->tpl_vars['company_details']->value['longitude']) {?>
                        <?php $_smarty_tpl->tpl_vars['loc'] = new Smarty_variable("https://www.google.com/maps/search/?api=1&query=".((string)$_smarty_tpl->tpl_vars['company_details']->value['latitude']).",".((string)$_smarty_tpl->tpl_vars['company_details']->value['longitude']), null, 0);?>
                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['loc']->value, ENT_QUOTES, 'UTF-8');?>
" target="_blank"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['loc']->value, ENT_QUOTES, 'UTF-8');?>
</a><br/>
                        <?php }?> 
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['prescription']->value['delivery_key']) {?>
                    <span style="font-size:15px;">Acknowledgement Link: </span>
                    <?php $_smarty_tpl->tpl_vars['link'] = new Smarty_variable(fn_url("delivery_details.prescription_order&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&key=".((string)$_smarty_tpl->tpl_vars['prescription']->value['delivery_key']),"C"), null, 0);?>
                    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                    <?php }?>
                    </div>
                </div>
            <?php }?>

            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.patient_details"),'target'=>"#acc_patient_details"), 0);?>

            <div id="acc_patient_details" class="collapse in collapse-visible">
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="firstname" class="control-label cm-required"><?php echo $_smarty_tpl->__("d_prescription.firstname");?>
</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[firstname]" id="firstname" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="lastname" class="control-label cm-required"><?php echo $_smarty_tpl->__("d_prescription.lastname");?>
</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[lastname]" id="lastname" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="email" class="control-label cm-required cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[email]" id="email" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['email'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="phone" class="control-label cm-mask-phone-label"><?php echo $_smarty_tpl->__("phone");?>
</label>
                    <div class="controls">
                        <input class="input-large  cm-mask-phone" type="text" name="prescription[phone]" id="phone" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['phone'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
            </div>

            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.prescription_details"),'target'=>"#acc_prescription_details"), 0);?>

            <div id="acc_prescription_details" class="collapse in collapse-visible">
                <div class="control-group">
                    <label for="prescriptions_attachment" class="control-label"><?php echo $_smarty_tpl->__("d_prescription.prescriptions_attachment");?>
</label>
                    <div class="controls ec-download ">
                        <?php if ($_smarty_tpl->tpl_vars['is_clinic']->value&&!$_smarty_tpl->tpl_vars['prescription']->value['files']) {?>
                            <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"prescriptions[]",'multiupload'=>false,'label_id'=>"id_prescribed_attachment",'upload_file_text'=>$_smarty_tpl->__('ec_upload_prescription'),'upload_another_file_text'=>$_smarty_tpl->__('ec_upload_prescription')), 0);?>

                        <?php }?>
                        <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescription']->value['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                            
                            <div class="download">
                                <p>
                                    <a download href="<?php echo htmlspecialchars(fn_url("d_prescription.download_pres&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post">prescriptions</a>
                                </p>
                                <p class="download__instructions">
                                    <small>Typically takes 5 seconds to 15 seconds.</small>
                                </p>
                            </div>
                            
                        <?php } ?>
                    </div>
                </div>

                <div class="control-group">
                    <label for="customer_note" class="control-label"><?php echo $_smarty_tpl->__("customer_note");?>
</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[customer_note]" id="customer_note" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['customer_note'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>

                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="e_prescription_no" class="control-label"><?php echo $_smarty_tpl->__("d_prescription.e_prescription_no");?>
</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[e_prescription_no]" id="e_prescription_no" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['e_prescription_no'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="doctor_emirates" class="control-label"><?php echo $_smarty_tpl->__("d_prescription.doctor_emirates");?>
</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[doctor_emirates]" id="doctor_emirates" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['doctor_emirates'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="insurance_company" class="control-label"><?php echo $_smarty_tpl->__("d_prescription.insurance_company");?>
</label>
                    <div class="controls">
                        <select id="insurance_company" name="prescription[insurance_company]">
                        <option value="">--</option>
                        <?php  $_smarty_tpl->tpl_vars['insurance_company'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['insurance_company']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['insurance_companies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['insurance_company']->key => $_smarty_tpl->tpl_vars['insurance_company']->value) {
$_smarty_tpl->tpl_vars['insurance_company']->_loop = true;
?>
                            <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_company']->value['card_id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['prescription']->value['insurance_company']==$_smarty_tpl->tpl_vars['insurance_company']->value['card_id']) {?>selected="selected"<?php }?>><?php if (@constant('CART_LANGUAGE')=='en'||!$_smarty_tpl->tpl_vars['insurance_company']->value['name_ar']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_company']->value['name_en'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_company']->value['name_ar'], ENT_QUOTES, 'UTF-8');
}?>
                        <?php } ?>
                    </select>
                    </div>
                </div>
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="ec_mobile_no" class="control-label">Mobile No.</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[mobile_no]" id="ec_mobile_no" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['mobile_no'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="card_no" class="control-label">Card No.</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[card_no]" id="card_no" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['card_no'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
                <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                    <label for="mrn" class="control-label">Medical Record No.</label>
                    <div class="controls">
                        <input class="input-large" type="text" name="prescription[mrn]" id="mrn" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['mrn'], ENT_QUOTES, 'UTF-8');?>
" />
                    </div>
                </div>
                
            </div>

            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address']) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("delivery_address"),'target'=>"#delivery_address"), 0);?>

            <div id="delivery_address" class="collapse in collapse-visible">
                <div style="margin-left: 65px; font-size: 15px;">
                    <?php $_smarty_tpl->tpl_vars['delivery'] = new Smarty_variable($_smarty_tpl->tpl_vars['prescription']->value['order_data']['delivery_address'], null, 0);?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value['s_firstname'], ENT_QUOTES, 'UTF-8');?>
<br/>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value['s_address'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value['s_address_2'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value['s_city'], ENT_QUOTES, 'UTF-8');?>
<br/><?php echo htmlspecialchars(fn_get_state_name($_smarty_tpl->tpl_vars['delivery']->value['s_state'],$_smarty_tpl->tpl_vars['delivery']->value['s_country']), ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars(fn_get_country_name($_smarty_tpl->tpl_vars['delivery']->value['s_country']), ENT_QUOTES, 'UTF-8');?>
<br/>
                    <?php if ($_smarty_tpl->tpl_vars['delivery']->value['s_zipcode']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value['s_zipcode'], ENT_QUOTES, 'UTF-8');?>
<br/><?php }?>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value['s_phone'], ENT_QUOTES, 'UTF-8');?>
<br/>
                    <span style="text-transform: capitalize;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery']->value['s_address_type'], ENT_QUOTES, 'UTF-8');?>
 Address</span><br/>
                </div>
            </div>
            <?php }?>

            <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("d_prescription.emirates_id_and_insurance_card"),'target'=>"#emirates_id_and_insurance_card"), 0);?>

            <div id="emirates_id_and_insurance_card" class="collapse in collapse-visible">
                <div class="control-group">
                    <label for="insurance_card" class="control-label"><?php echo $_smarty_tpl->__("d_prescription.insurance_card");?>
:</label>
                    <div class="controls">
                        <input type="text" disabled value="<?php if ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='N') {
echo $_smarty_tpl->__("d_prescription.no_insurance");
} elseif ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='E') {
echo $_smarty_tpl->__("d_prescription.emirate_id_is_insurance");
} elseif ($_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='S') {
echo $_smarty_tpl->__("d_prescription.seprate_insurance");
}?>" />
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['prescription']->value['customer_comment']) {?>
                    <div class="control-group">
                        <label for="" class="control-label"><?php echo $_smarty_tpl->__("d_prescription.customer_comment");?>
:</label>
                        <div class="controls">
                            <input type="text" disabled value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['customer_comment'], ENT_QUOTES, 'UTF-8');?>
" style="width: 100%;"/>
                        </div>
                    </div>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['prescription']->value['order_data']['emirate_card']) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/emirate_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card_view_only'=>true,'card'=>$_smarty_tpl->tpl_vars['prescription']->value['order_data']['emirate_card']), 0);?>

                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['prescription']->value['order_data']['insurance_card']&&$_smarty_tpl->tpl_vars['prescription']->value['insurance_card']=='S') {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_customer/views/insurance_cards/components/card.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('card_view_only'=>true,'card'=>$_smarty_tpl->tpl_vars['prescription']->value['order_data']['insurance_card']), 0);?>

                <?php }?>
               
            </div>
            
        </div>
        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']&&$_smarty_tpl->tpl_vars['prescription']->value['status']!=smarty_modifier_enum("PrescriptionStatus::NEW_REQUEST")) {?>
        <div id="content_products" class="hidden">

             <div class="control-group">
                <label for="" class="control-label">Allow Copayment Percentage:</label>
                <div class="controls">
                    <input type="hidden" name="prescription[allow_copayment_percentage]" value="N" />
                    <input type="checkbox" name="prescription[allow_copayment_percentage]" value="Y" <?php if ($_smarty_tpl->tpl_vars['prescription']->value['allow_copayment_percentage']=='Y') {?>checked="checked"<?php }?>/>
                    <br/><small>Check this to apply the copayment in the prescription</small>
                </div>
            </div>

            <div class="control-group">
                <label for="" class="control-label"><?php echo $_smarty_tpl->__("d_prescription.copyament_percentage");?>
:</label>
                <div class="controls">
                    <input type="text" name="prescription[copayment]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['copayment'], ENT_QUOTES, 'UTF-8');?>
" /> %
                    <br/><small>It will apply only on the products which are covered under insurance</small>
                </div>
            </div>
            <?php $_smarty_tpl->tpl_vars['regex'] = new Smarty_variable("^[1-9][0-9]*([".((string)$_smarty_tpl->tpl_vars['decimal']->value)."][0-9]+)?"."$", null, 0);?>
            <div class="control-group">
                <label for="ec_pharmacy_disount" 
                    class="control-label cm-required cm-numeric">
                    <?php echo $_smarty_tpl->__("d_prescription.pharmacy_discount");?>
:
                </label>
                <div class="controls">
                    <input type="text" name="prescription[pharmacy_discount]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prescription']->value['pharmacy_discount'], ENT_QUOTES, 'UTF-8');?>
" id="ec_pharmacy_disount"/> %
                    <br/><small>First it will apply on the products</small>
                </div>
            </div>

            <div class="control-group">
                <label for="id_photos_insurance_response" class="control-label ">
                    <?php echo $_smarty_tpl->__("d_prescription.reponse_from_insurance_companies");?>

                </label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"insurance_responses[]",'multiupload'=>true,'label_id'=>"id_photos_insurance_response"), 0);?>

                        <?php if ($_smarty_tpl->tpl_vars['prescription']->value['insurance_response']) {?>
                            <div class="">
                                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescription']->value['insurance_response']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.download&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                                    <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                                        <br/>
                                    <?php }?>
                                <?php } ?>
                            </div>
                        <?php }?>
                </div>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['products']) {?>
                <div class="table-responsive-wrapper">
                    <table width="90%" class="table table-middle table--relative table-responsive h-rfq_table">
                    <thead>
                        <tr>
                            <th width="70%">&nbsp;</th>
                            <th class="center" width="10%" >&nbsp;<?php echo $_smarty_tpl->__("h_rfq.total_vat");?>
</th>
                            <th class="center" width="10%" >&nbsp;<?php echo $_smarty_tpl->__("h_rfq.non_tax_price_total");?>
</th>
                            <th class="center" width="10%" >&nbsp;<?php echo $_smarty_tpl->__("h_rfq.tax_price_total");?>
</th>
                        </tr>
                    </thead>
                    <?php  $_smarty_tpl->tpl_vars["product"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["product"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['prescription']->value['products']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["product"]->key => $_smarty_tpl->tpl_vars["product"]->value) {
$_smarty_tpl->tpl_vars["product"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["product"]->key;
?>
                    <tr>
                        <td width="70%"> 
                            <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>((string)$_smarty_tpl->tpl_vars['product']->value['prescription_pid']),'product'=>$_smarty_tpl->tpl_vars['product']->value,'name_var'=>"prescription[product]",'form_for'=>"prescription_order"), 0);?>

                        </td>
                        <td  width="10%" class="center" data-th="<?php echo $_smarty_tpl->__("h_rfq.total_vat");?>
"><span><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['total_vat']), 0);?>
</span></td>
                        <td  width="10%" class="center" data-th="<?php echo $_smarty_tpl->__("h_rfq.non_tax_price_total");?>
"><span><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['non_tax_price_total']), 0);?>
</span></td>
                        <td  width="10%" class="center" data-th="<?php echo $_smarty_tpl->__("h_rfq.tax_price_total");?>
"><span><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['product']->value['tax_price_total']), 0);?>
</span></td>
                    </tr>
                    <?php } ?>
                    </table>
                </div>
                <div class="h-rfq_add_product_before"></div>
                <div class="h-rfq_add_product">
                    <a href="javascript:;" class="btn btn__primary" id="h-rfq_add_new_product" data-form-for="prescription_order" data-name-var="prescription[product]"><?php echo $_smarty_tpl->__("h_rfq.add_product");?>
</a>
                </div>
                <div class="order-notes statistic">
                    <div class="clearfix">
                        <div class="table-wrapper">
                            <table class="pull-right">
                                <tr class="totals">
                                    <td class="totals-label">&nbsp;</td>
                                    <td class="totals" width="100px"><h4><?php echo $_smarty_tpl->__("totals");?>
</h4></td>
                                </tr>

                                <tr>
                                    <td class="statistic-label"><?php echo $_smarty_tpl->__("subtotal");?>
:</td>
                                    <td class="right" data-ct-totals="subtotal"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['subtotal']), 0);?>
</td>
                                </tr>

                                <tr>
                                    <td class="statistic-label"><?php echo $_smarty_tpl->__("shipping_cost");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
):</td>
                                    <td class="right" data-ct-totals="shipping_cost">
                                    <?php if ($_smarty_tpl->tpl_vars['allow_to_update_details']->value) {?>
                                        <input type="text" name="quotation[shipping_price]" id="elm_shipping_price_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" size="10" value="<?php echo htmlspecialchars(fn_format_price((($tmp = @$_smarty_tpl->tpl_vars['prescription']->value['shipping_price'])===null||$tmp==='' ? "0.00" : $tmp),$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" class="input-small" />
                                    <?php } else { ?>
                                        <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['shipping_non_tax_price']), 0);?>

                                    <?php }?>
                                    
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="statistic-label"><?php echo $_smarty_tpl->__("h_rfq.vat");?>
 (Product Tax + Shipping Tax + Surcharge Tax):</td>
                                    <td class="right"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['total_tax']), 0);?>
</td>
                                </tr>

                                <tr>
                                    <td class="statistic-label"><h4><?php echo $_smarty_tpl->__("total");?>
:</h4></td>
                                    <td class="price right" data-ct-totals="total"><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['prescription']->value['total']), 0);?>
</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('name_var'=>"prescription[product]",'form_for'=>"prescription_order",'required_values'=>false), 0);?>

                <div class="h-rfq_add_product_before"></div>
                <div class="h-rfq_add_product">
                    <a href="javascript:;" class="btn btn__primary" id="h-rfq_add_new_product" data-form-for="prescription_order" data-name-var="prescription[product]"><?php echo $_smarty_tpl->__("h_rfq.add_product");?>
</a>
                </div>
            <?php }?>
            
        </div>
        <?php }?>
       
        <?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
            <?php if ($_smarty_tpl->tpl_vars['is_clinic']->value&&!$_smarty_tpl->tpl_vars['prescription']->value['req_id']) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit-link",'but_name'=>"dispatch[d_prescription.update]",'but_target_form'=>"prescription_info_form"), 0);?>

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_SHIPMENT")||$_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_SHIPPED")||$_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_COMPLETE")) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("common/view_tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('url'=>"h_rfq.details&rfq_id="), 0);?>

                <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("d_prescription.print_invoice"),'href'=>"d_prescription_invoice.print_invoice?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id']),'class'=>"cm-new-window"));?>
</li>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("d_prescription.print_invoice_pdf"),'href'=>"d_prescription_invoice.print_invoice?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&format=pdf",'class'=>''));?>
</li>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>"Print Invoice of Products",'href'=>"d_prescription_invoice.print_invoice_new?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id']),'class'=>"cm-new-window"));?>
</li>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>"Print Invoice of Products (PDF)",'href'=>"d_prescription_invoice.print_invoice_new?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&format=pdf",'class'=>"cm-new-window"));?>
</li>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>"Print Invoice of Shipping",'href'=>"d_prescription_invoice.print_invoice_new.shipping?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id']),'class'=>"cm-new-window"));?>
</li>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>"Print Invoice of Shipping (PDF)",'href'=>"d_prescription_invoice.print_invoice_new.shipping?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])."&format=pdf",'class'=>"cm-new-window"));?>
</li>
                    <?php if (@constant('ACCOUNT_TYPE')=='admin') {?>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("d_prescription.clone_prescription"),'href'=>"d_prescription.clone?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id']),'class'=>"cm-new-window"));?>
</li>
                    <?php }?>
                <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

            <?php } else { ?>
                <?php if (@constant('ACCOUNT_TYPE')=='admin') {?>
                <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
                    <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("d_prescription.clone_prescription"),'href'=>"d_prescription.clone?req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id']),'class'=>"cm-new-window"));?>
</li>
                <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

                <?php }?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['runtime']->value['company_id']&&$_smarty_tpl->tpl_vars['prescription']->value['order_accept_by_vendor']!='Y'&&$_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_OPEN")) {?>
            
            <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"reject_prescription_popup",'act'=>"general",'link_class'=>"d-cancel_button",'text'=>"If you are not able to fullfill the order, click on Reject Prection button.",'link_text'=>"Reject Prescription",'content'=>Smarty::$_smarty_vars['capture']['reject_block']), 0);?>


            
            <?php ob_start();
echo htmlspecialchars(fn_url("d_prescription.accept_order&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])), ENT_QUOTES, 'UTF-8');
$_tmp1=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>"Accept Prescription",'title'=>"If you are not able to fullfill the order, please accept the order by click on that.",'but_role'=>"action",'but_href'=>$_tmp1,'but_meta'=>"btn-primary cm-post"), 0);?>

            <?php }?>
            
            <?php if (($_smarty_tpl->tpl_vars['auth']->value['user_id']==25||$_smarty_tpl->tpl_vars['auth']->value['user_id']==1)||$_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_OPEN")||$_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::NEW_REQUEST")) {?>
                <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::NEW_REQUEST")) {?>
                    <?php ob_start();
echo htmlspecialchars(fn_url("d_prescription.open_the_order&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])), ENT_QUOTES, 'UTF-8');
$_tmp2=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>"Open the order",'title'=>"Open the order, and assign the order to the vendor.",'but_role'=>"action",'but_href'=>$_tmp2,'but_meta'=>"btn-primary cm-post"), 0);?>

                <?php }?>
                <?php if (($_smarty_tpl->tpl_vars['runtime']->value['company_id']&&$_smarty_tpl->tpl_vars['prescription']->value['order_accept_by_vendor']=='Y')||!$_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_role'=>"submit-link",'but_name'=>"dispatch[d_prescription.update]",'but_target_form'=>"prescription_info_form",'save'=>$_smarty_tpl->tpl_vars['prescription']->value['req_id']), 0);?>

                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['prescription']->value['products']) {?>
                    
                    <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_OPEN")||$_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::NEW_REQUEST")) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>"Save & Proceed for delivery",'title'=>"Order Completed from your end.",'but_role'=>"submit-link",'but_target_form'=>"prescription_info_form",'but_name'=>"dispatch[d_prescription.update.order_complete]",'but_meta'=>"btn-primary cm-confirm"), 0);?>
    
                    <?php }?>  
                <?php }?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_PROCESSED")) {?>
                <?php ob_start();
echo htmlspecialchars(fn_url("d_prescription.assign_for_delivery&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])), ENT_QUOTES, 'UTF-8');
$_tmp3=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>"Assign For Delivery",'title'=>"Assign the Items for delivery, once you complete the packing and printed the invoice.",'but_role'=>"action",'but_href'=>$_tmp3,'but_meta'=>"btn-primary cm-post"), 0);?>

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_SHIPMENT")) {?>
                <?php ob_start();
echo htmlspecialchars(fn_url("d_prescription.order_picked&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])), ENT_QUOTES, 'UTF-8');
$_tmp4=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>"Order Picked(On the way)",'title'=>"Click on that button when order is picked by the delivery boy. (Order On The Way)",'but_role'=>"action",'but_href'=>$_tmp4,'but_meta'=>"btn-primary cm-post"), 0);?>

            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['prescription']->value['status']==smarty_modifier_enum("PrescriptionStatus::REQUEST_SHIPPED")) {?>
                <?php ob_start();
echo htmlspecialchars(fn_url("d_prescription.order_delivered&req_id=".((string)$_smarty_tpl->tpl_vars['prescription']->value['req_id'])), ENT_QUOTES, 'UTF-8');
$_tmp5=ob_get_clean();?><?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>"Order Delivered",'title'=>"Click on that after delivered the order.",'but_role'=>"action",'but_href'=>$_tmp5,'but_meta'=>"btn-primary cm-post"), 0);?>

            <?php }?>
        <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    </form>
    <div id="content_delivery" class="hidden">
        <form name="add_delivery_details" action="<?php echo htmlspecialchars(fn_url("d_prescription.delivery_details"), ENT_QUOTES, 'UTF-8');?>
" enctype="multipart/form-data" method="post" class="form-edit form-horizontal">
            <div class="btn-toolbar clearfix">
                <div class="pull-right">
                    <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('method'=>"post",'but_role'=>"submit",'but_text'=>"Save Delivery Details"), 0);?>

                </div>
            </div>
            <input type="hidden" name="req_id" value="<?php echo htmlspecialchars($_REQUEST['req_id'], ENT_QUOTES, 'UTF-8');?>
" />
            <input type="hidden" name="selected_section" value="delivery" />
            <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'UTF-8');?>
" />

            <?php if ($_smarty_tpl->tpl_vars['delivery_details']->value['signature']) {?>
            <div>
                <h4>Customer Signature</h4>
                <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_details']->value['signature'], ENT_QUOTES, 'UTF-8');?>
" style="width:250px;height:250px;" />
            </div>
            <?php }?>
            
            <div class="control-group ">
                <label for="delivery_boy_name" class="control-label">Delivery Boy Name</label>
                <div class="controls">
                    <input class="input-large" type="text" name="delivery_boy_name" id="delivery_boy_name" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_details']->value['delivery_boy_name'], ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>

            <div class="control-group">
                <label for="id_photos_delivered_order" class="control-label ">
                    Photos of delivered order
                </label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"delivery_photos[]",'multiupload'=>true,'label_id'=>"id_photos_delivered_order"), 0);?>

                        <?php if ($_smarty_tpl->tpl_vars['delivery_details']->value['photos']) {?>
                            <div class="">
                                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['delivery_details']->value['photos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.download&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                                    <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                                        <br/>
                                    <?php }?>
                                <?php } ?>
                            </div>
                        <?php }?>
                </div>
                 
            </div>
           

            <div class="control-group">
                <label for="id_acknowledge_reciept" class="control-label ">
                    Acknowledge/Signature
                </label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"reciepts[]",'multiupload'=>true,'label_id'=>"id_acknowledge_reciept"), 0);?>

                        <?php if ($_smarty_tpl->tpl_vars['delivery_details']->value['reciepts']) {?>
                            <div class="">
                                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['delivery_details']->value['reciepts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.download&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                                    <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                                        <br/>
                                    <?php }?>
                                <?php } ?>
                            </div>
                        <?php }?>
                </div>
            </div>

            <div class="control-group ">
                <label for="person_name" class="control-label">Person Name Who Collected the Order</label>
                <div class="controls">
                    <input class="input-large" type="text" name="person_name" id="person_name" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delivery_details']->value['person_name'], ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>

            <div class="control-group ">
                 <label for="id_person_emirate_id" class="control-label ">
                    Person EMIRATE ID Who Collected the Order
                </label>
                <div class="controls">
                    <?php echo $_smarty_tpl->getSubTemplate ("common/fileuploader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('var_name'=>"emirate_ids[]",'multiupload'=>true,'label_id'=>"id_person_emirate_id"), 0);?>

                        <?php if ($_smarty_tpl->tpl_vars['delivery_details']->value['emirate_ids']) {?>
                            <div class="">
                                <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['delivery_details']->value['emirate_ids']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                                    <a href="<?php echo htmlspecialchars(fn_url("d_prescription.download&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                                    <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                                        <br/>
                                    <?php }?>
                                <?php } ?>
                            </div>
                        <?php }?>
                </div>
            </div>
        </form>
    </div>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/tabsbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['tabsbox'],'group_name'=>$_smarty_tpl->tpl_vars['runtime']->value['controller'],'active_tab'=>$_REQUEST['selected_section'],'track'=>true), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->__("d_prescription.prescription_request",array('[req_id]'=>$_smarty_tpl->tpl_vars['prescription']->value['req_id']));?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>Smarty::$_smarty_vars['capture']['mainbox_title'],'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar'],'sidebar_position'=>"left",'sidebar_icon'=>"icon-user"), 0);?>

<?php }} ?>
