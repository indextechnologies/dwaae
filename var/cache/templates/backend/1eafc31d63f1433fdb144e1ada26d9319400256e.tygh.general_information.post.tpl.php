<?php /* Smarty version Smarty-3.1.21, created on 2022-03-02 15:28:17
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/companies/general_information.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1142548667621f54d1cc5fc6-32786247%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1eafc31d63f1433fdb144e1ada26d9319400256e' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/companies/general_information.post.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1142548667621f54d1cc5fc6-32786247',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'company_data' => 0,
    'insurance_companies' => 0,
    'insurance_company' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621f54d1cd8698_90767814',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621f54d1cd8698_90767814')) {function content_621f54d1cd8698_90767814($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_custom.24_hr_pharmacy','d_prescription.select_insurance_companies'));
?>
<div class="control-group">
    <label for="elm_24hr_parmacy" class="control-label"><?php echo $_smarty_tpl->__("d_custom.24_hr_pharmacy");?>
:</label>
    <div class="controls">
        <input type="hidden" value="N" name="company_data[all_time_available]" />
        <input type="checkbox" id="elm_24hr_parmacy" value="Y" name="company_data[all_time_available]" <?php if ($_smarty_tpl->tpl_vars['company_data']->value['all_time_available']=='Y') {?>checked="checked"<?php }?> />
    </div>
</div>

<div class="control-group">
    <label for="elm_insurance_companies" class="control-label"><?php echo $_smarty_tpl->__("d_prescription.select_insurance_companies");?>
:</label>
    <div class="controls">
       <select id="elm_insurance_companies" name="company_data[insurance_companies][]" multiple>
            <option value="">--</option>
            <?php  $_smarty_tpl->tpl_vars['insurance_company'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['insurance_company']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['insurance_companies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['insurance_company']->key => $_smarty_tpl->tpl_vars['insurance_company']->value) {
$_smarty_tpl->tpl_vars['insurance_company']->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_company']->value['card_id'], ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['company_data']->value['insurance_companies']&&in_array($_smarty_tpl->tpl_vars['insurance_company']->value['card_id'],$_smarty_tpl->tpl_vars['company_data']->value['insurance_companies'])) {?>selected="selected"<?php }?>><?php if (@constant('CART_LANGUAGE')=='en'||!$_smarty_tpl->tpl_vars['insurance_company']->value['name_ar']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_company']->value['name_en'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['insurance_company']->value['name_ar'], ENT_QUOTES, 'UTF-8');
}?>
            <?php } ?>
        </select>
    </div>
</div><?php }} ?>
