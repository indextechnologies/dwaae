<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:40:32
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/orders/payment_info.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14733355656214a180043005-15097689%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd9b805ab07216e23664e4892eaceaff819357bf7' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/orders/payment_info.post.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '14733355656214a180043005-15097689',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'prescriptions' => 0,
    'order_info' => 0,
    'p' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214a180049223_76868410',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214a180049223_76868410')) {function content_6214a180049223_76868410($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_custom.prescriptions'));
?>
<?php if ($_smarty_tpl->tpl_vars['prescriptions']->value) {?>
     <div class="control-group">
        <div class="control-label"><?php echo $_smarty_tpl->__("d_custom.prescriptions");?>
:</div>
        <div class="controls">
            <?php  $_smarty_tpl->tpl_vars['p'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['p']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prescriptions']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['p']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['p']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['p']->key => $_smarty_tpl->tpl_vars['p']->value) {
$_smarty_tpl->tpl_vars['p']->_loop = true;
 $_smarty_tpl->tpl_vars['p']->iteration++;
 $_smarty_tpl->tpl_vars['p']->last = $_smarty_tpl->tpl_vars['p']->iteration === $_smarty_tpl->tpl_vars['p']->total;
?>
                <a href="<?php echo htmlspecialchars(fn_url("d_prescriptions.download&order_id=".((string)$_smarty_tpl->tpl_vars['order_info']->value['order_id'])."&file=".((string)$_smarty_tpl->tpl_vars['p']->value)), ENT_QUOTES, 'UTF-8');?>
" class="cm-post"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['p']->value, ENT_QUOTES, 'UTF-8');?>
</a>
                <?php if (!$_smarty_tpl->tpl_vars['p']->last) {?>
                    ,&nbsp;
                <?php }?>
            <?php } ?>
        </div>
    </div>
<?php }?><?php }} ?>
