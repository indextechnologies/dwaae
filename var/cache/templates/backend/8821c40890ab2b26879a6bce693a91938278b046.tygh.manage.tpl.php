<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:54
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_booking/views/d_booking/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:48081039362149caa1431b6-12658672%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8821c40890ab2b26879a6bce693a91938278b046' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_booking/views/d_booking/manage.tpl',
      1 => 1645451889,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '48081039362149caa1431b6-12658672',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'search' => 0,
    'ajax_class' => 0,
    'c_url' => 0,
    'sort_sign' => 0,
    'bookings' => 0,
    'q' => 0,
    'settings' => 0,
    'current_redirect_url' => 0,
    'booking_statuses' => 0,
    'form_meta' => 0,
    'id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149caa1ae916_95493807',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149caa1ae916_95493807')) {function content_62149caa1ae916_95493807($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('delete','no_data'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
    <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
    <?php if ($_smarty_tpl->tpl_vars['search']->value['sort_order']=="asc") {?>
    <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"icon-down-dir\"></i>", null, 0);?>
    <?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"icon-up-dir\"></i>", null, 0);?>
    <?php }?>
    <?php if (!$_smarty_tpl->tpl_vars['config']->value['tweaks']['disable_dhtml']) {?>
        <?php $_smarty_tpl->tpl_vars["ajax_class"] = new Smarty_variable("cm-ajax", null, 0);?>

    <?php }?>
    <?php if (@constant('ACCOUNT_TYPE')!="admin") {?>
        <?php $_smarty_tpl->tpl_vars['not_main_admin'] = new Smarty_variable(true, null, 0);?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars['not_main_admin'] = new Smarty_variable(false, null, 0);?>
    <?php }?>
    
    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php $_smarty_tpl->tpl_vars['booking_statuses'] = new Smarty_variable(fn_d_booking_get_booking_statuses(), null, 0);?>
    <table class="table orders-search">
        <thead>
            <tr>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=booking_id&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents">Booking Id</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="booking_id") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=token_no&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents">Token No</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="token_no") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=test_type&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents">Test Type</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="test_type") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=create_time&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents">Create Date</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="create_time") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=booking_date&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents">Booking Date</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="booking_date") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th>Booking Slot</th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=payment_status&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents">Payment Status</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="payment_status") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=status&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents">Status</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="status") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
            </tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars["q"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["q"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['bookings']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["q"]->key => $_smarty_tpl->tpl_vars["q"]->value) {
$_smarty_tpl->tpl_vars["q"]->_loop = true;
?>
            <tr>
                <td class="orders-search__item"><a href="<?php echo htmlspecialchars(fn_url("d_booking.update&booking_id=".((string)$_smarty_tpl->tpl_vars['q']->value['booking_id'])), ENT_QUOTES, 'UTF-8');?>
">#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['booking_id'], ENT_QUOTES, 'UTF-8');?>
</a></td>
                <td class="orders-search__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['token_no'], ENT_QUOTES, 'UTF-8');?>
</td>
                <td class="orders-search__item"><?php if ($_smarty_tpl->tpl_vars['q']->value['payment_status']=='S') {?>Student PCR<?php } else {
echo htmlspecialchars(fn_d_booking_get_booking_type_status_description($_smarty_tpl->tpl_vars['q']->value['test_type']), ENT_QUOTES, 'UTF-8');
}?></td>
                <td class="orders-search__item">
                    <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['q']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>

                </td>
                <td class="orders-search__item">
                    <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['q']->value['booking_date'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>

                </td>
                <td class="orders-search__item">
                    <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['q']->value['booking_slot'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>

                </td>
                <td class="orders-search__item"><?php echo htmlspecialchars(fn_d_booking_get_payment_status_description($_smarty_tpl->tpl_vars['q']->value['payment_status']), ENT_QUOTES, 'UTF-8');?>
</td>
                <td>
                    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_items", null, null); ob_start(); ?>
                        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'href'=>"d_booking.update?booking_id=".((string)$_smarty_tpl->tpl_vars['q']->value['booking_id']),'text'=>"View"));?>
</li>
                        <?php $_smarty_tpl->tpl_vars["current_redirect_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
                        <li><?php ob_start();?><?php echo $_smarty_tpl->__("delete");?>
<?php $_tmp1=ob_get_clean();?><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'href'=>"d_booking.delete?booking_id=".((string)$_smarty_tpl->tpl_vars['q']->value['booking_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['current_redirect_url']->value),'class'=>"cm-confirm",'text'=>$_tmp1,'method'=>"POST"));?>
</li>
                    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                    <div class="hidden-tools">
                        <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_items']));?>

                    </div>
                </td>
                <td class="orders-search__item">
                    
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking_statuses']->value[$_smarty_tpl->tpl_vars['q']->value['status']], ENT_QUOTES, 'UTF-8');?>

                </td>
            </tr>
        <?php }
if (!$_smarty_tpl->tpl_vars["q"]->_loop) {
?>
            <tr class="table__no-items">
                <td colspan="7"><p class="no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p></td>
            </tr>
        <?php } ?>
    </table>
    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>



<?php $_smarty_tpl->_capture_stack[0][] = array("export_report", null, null); ob_start(); ?>
<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" name="export_report" method="get" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['form_meta']->value, ENT_QUOTES, 'UTF-8');?>
">
    <div class="group form-horizontal">
        <div class="control-group">
            <div class="controls">
                <?php echo $_smarty_tpl->getSubTemplate ("common/period_selector.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('period'=>$_smarty_tpl->tpl_vars['search']->value['period'],'form_name'=>"export_report"), 0);?>

            </div>
        </div>

        <div class="control-group">
            <label for="elm_student_pcr" class="control-label">Export Type:</label>
            <div class="controls">
                <select name="export_type" id="elm_student_pcr">
                    <option value="">--</option>
                    <option value="only_student_pcr">Only Student PCR's</option>
                    <option value="only_normal_bookings">Only Normal Bookings</option>
                    <option value="only_clinical_bookings">Only Clinical Bookings</option>
                    <option value="only_normal_vip">Only Normal VIP's booking</option>
                </select>
            </div>
        </div>

        

        
    </div>

    <div class="buttons-container">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[d_booking.export_monthly_report.period]",'but_text'=>"Export",'cancel_action'=>"close",'hide_first_button'=>false,'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

    </div>
</form>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("sidebar", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_booking/views/d_booking/components/search.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dispatch'=>"d_booking.manage"), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"export_report",'text'=>"Export Report",'title'=>"Export Report",'content'=>Smarty::$_smarty_vars['capture']['export_report'],'act'=>"general",'link_text'=>"Export",'icon'=>'','link_class'=>"btn-primary"), 0);?>

    
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>Bookings<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['mainbox'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'title'=>Smarty::$_smarty_vars['capture']['mainbox_title'],'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar']), 0);?>


<?php }} ?>
