<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:46:02
         compiled from "/home/dwaae/public_html/design/backend/templates/views/products/components/bulk_edit/actions.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3930529336214a2cad66879-30848478%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '32733b4686159bff0a66b146f1331f4ae939d542' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/views/products/components/bulk_edit/actions.tpl',
      1 => 1602586406,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3930529336214a2cad66879-30848478',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214a2cad77a52_95774106',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214a2cad77a52_95774106')) {function content_6214a2cad77a52_95774106($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('clone_selected','export_selected'));
?>
<li>
    <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>"Mark products Not Allow to sell",'dispatch'=>"dispatch[products.m_not_allow_to_sell]",'form'=>"manage_products_form"));?>

</li>

<li>
    <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>"Mark products Allow to sell",'dispatch'=>"dispatch[products.m_allow_to_sell]",'form'=>"manage_products_form"));?>

</li>

<li>
    <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("clone_selected"),'dispatch'=>"dispatch[products.m_clone]",'form'=>"manage_products_form"));?>

</li>

<li>
    <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("export_selected"),'dispatch'=>"dispatch[products.export_range]",'form'=>"manage_products_form"));?>

</li>

<li>
    <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"delete_selected",'dispatch'=>"dispatch[products.m_delete]",'form'=>"manage_products_form"));?>

</li>
<?php }} ?>
