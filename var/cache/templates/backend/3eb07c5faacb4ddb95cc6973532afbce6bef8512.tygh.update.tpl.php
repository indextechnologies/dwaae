<?php /* Smarty version Smarty-3.1.21, created on 2022-03-02 14:40:43
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_booking/views/d_clinic/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:652233620621f49ab50bab2-62111458%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3eb07c5faacb4ddb95cc6973532afbce6bef8512' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_booking/views/d_clinic/update.tpl',
      1 => 1631201413,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '652233620621f49ab50bab2-62111458',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'clinic' => 0,
    'is_form_readonly' => 0,
    'no_hide_input_if_shared_product' => 0,
    'id' => 0,
    'states' => 0,
    'state' => 0,
    'allow_save' => 0,
    'title_start' => 0,
    'title_end' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621f49ab59e3f7_57801721',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621f49ab59e3f7_57801721')) {function content_621f49ab59e3f7_57801721($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('information','clinic_name','email','password','address','address','city','state','phone_no','latitude','longitude','delete','ec_emr.editing_clinic','ec_emr.create_new_clinic'));
?>
<?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['clinic']->value['clinic_id'])===null||$tmp==='' ? 0 : $tmp), null, 0);?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
<form id="form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="clinic_update_form" class="form-horizontal form-edit  cm-disable-empty-files <?php if ($_smarty_tpl->tpl_vars['is_form_readonly']->value) {?>cm-hide-inputs<?php }?>" enctype="multipart/form-data"> 

    <input type="hidden" name="fake" value="1" />
    <input type="hidden" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" name="clinic_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />

    

    <div class="product-manage" id="content_detailed">

        
        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("information"),'target'=>"#acc_information"), 0);?>


        <div id="acc_information" class="collapse in collapse-visible">

            <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label for="elm_clinic_name" class="control-label cm-required"><?php echo $_smarty_tpl->__("clinic_name");?>
</label>
                <div class="controls">
                    <input class="input-large" form="form" type="text" name="clinic_data[name]" id="elm_clinic_name" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic']->value['name'], ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>

             <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label for="elm_clinic_email" class="control-label cm-required cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
                <div class="controls">
                    <input class="input-large" form="form" type="text" name="clinic_data[email]" id="elm_clinic_email" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic']->value['email'], ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>

             <div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
                <label for="elm_clinic_password" class="control-label cm-required"><?php echo $_smarty_tpl->__("password");?>
</label>
                <div class="controls">
                    <input class="input-large" form="form" type="password" name="clinic_data[password]" id="elm_clinic_password" size="55" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic']->value['password'], ENT_QUOTES, 'UTF-8');?>
" />
                </div>
            </div>
            
            

            <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/status_on_update.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('input_name'=>"clinic_data[status]",'id'=>"elm_clinic_status",'obj'=>$_smarty_tpl->tpl_vars['clinic']->value,'hidden'=>true), 0);?>

        </div>
        <hr>


        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("address"),'target'=>"#acc_address"), 0);?>

        <div class="collapse in" id="acc_address">

            <div class="control-group">
                <label class="control-label" for="elm_address"><?php echo $_smarty_tpl->__("address");?>
:</label>
                <div class="controls">
                    <input type="text" name="clinic_data[address]" id="elm_address" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic']->value['address'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_city"><?php echo $_smarty_tpl->__("city");?>
:</label>
                <div class="controls">
                    <input type="text" name="clinic_data[city]" id="elm_city" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic']->value['city'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_state"><?php echo $_smarty_tpl->__("state");?>
:</label>
                <div class="controls">
                    <select  name="clinic_data[state]">
                        <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                            <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                        <?php } ?>
                    </select>
                </div>
            </div>

             <div class="control-group">
                <label class="control-label" for="elm_phone_no"><?php echo $_smarty_tpl->__("phone_no");?>
:</label>
                <div class="controls">
                    <input type="text" name="clinic_data[phone_no]" id="elm_phone_no" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic']->value['phone_no'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_latitude"><?php echo $_smarty_tpl->__("latitude");?>
:</label>
                <div class="controls">
                    <input type="text" name="clinic_data[latitude]" id="elm_latitude" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic']->value['latitude'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_longitude"><?php echo $_smarty_tpl->__("longitude");?>
:</label>
                <div class="controls">
                    <input type="text" name="clinic_data[longitude]" id="elm_longitude" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic']->value['longitude'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
                </div>
            </div>

        </div>

        </div>
        <!--content_detailed--></div> 

    

    <?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php $_smarty_tpl->tpl_vars['allow_clone'] = new Smarty_variable(true, null, 0);?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/view_tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('url'=>"d_clinic.update?clinic_id="), 0);?>


        <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
                    <?php if ($_smarty_tpl->tpl_vars['allow_save']->value) {?>
                        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("delete"),'class'=>"cm-confirm",'href'=>"d_clinic.delete?product_id=".((string)$_smarty_tpl->tpl_vars['id']->value),'method'=>"POST"));?>
</li>
                    <?php }?>
            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

        <?php }?>
        <!-- the button goes here -->
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_meta'=>"cm-product-save-buttons",'but_role'=>"submit-link",'but_name'=>"dispatch[d_clinic.update]",'but_target_form'=>"clinic_update_form",'save'=>$_smarty_tpl->tpl_vars['id']->value), 0);?>

        <!-- the button goes there -->
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
</form>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
    <?php $_smarty_tpl->tpl_vars['title_start'] = new Smarty_variable($_smarty_tpl->__("ec_emr.editing_clinic"), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['title_end'] = new Smarty_variable(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['clinic']->value['name']), null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
        <?php echo $_smarty_tpl->__("ec_emr.create_new_clinic");?>

    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?>


<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title_start'=>$_smarty_tpl->tpl_vars['title_start']->value,'title_end'=>$_smarty_tpl->tpl_vars['title_end']->value,'title'=>Smarty::$_smarty_vars['capture']['mainbox_title'],'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'select_languages'=>false,'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons']), 0);?>
<?php }} ?>
