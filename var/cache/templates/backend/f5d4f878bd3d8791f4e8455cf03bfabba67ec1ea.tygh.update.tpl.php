<?php /* Smarty version Smarty-3.1.21, created on 2022-02-23 18:33:41
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_booking/views/d_booking/update.tpl" */ ?>
<?php /*%%SmartyHeaderCode:122260139062149e4c092653-20542354%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5d4f878bd3d8791f4e8455cf03bfabba67ec1ea' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_booking/views/d_booking/update.tpl',
      1 => 1645626820,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '122260139062149e4c092653-20542354',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149e4c0f08b3_03774799',
  'variables' => 
  array (
    'booking' => 0,
    'is_form_readonly' => 0,
    'no_hide_input_if_shared_product' => 0,
    'id' => 0,
    'is_student_pcr' => 0,
    'patient' => 0,
    'settings' => 0,
    'clinic_details' => 0,
    'basename' => 0,
    'ext' => 0,
    'extra_front' => 0,
    'extra_back' => 0,
    'title_start' => 0,
    'title_end' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149e4c0f08b3_03774799')) {function content_62149e4c0f08b3_03774799($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_enum')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.enum.php';
if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('information'));
?>
<?php $_smarty_tpl->tpl_vars['id'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['booking']->value['booking_id'])===null||$tmp==='' ? 0 : $tmp), null, 0);?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
<form id="form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="booking_update_form" class="form-horizontal form-edit  cm-disable-empty-files <?php if ($_smarty_tpl->tpl_vars['is_form_readonly']->value) {?>cm-hide-inputs<?php }?>" enctype="multipart/form-data"> 

    <input type="hidden" name="fake" value="1" />
    <input type="hidden" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" name="booking_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" />

    

    <div class="product-manage" id="content_detailed">
            
        <?php if ($_smarty_tpl->tpl_vars['booking']->value['status']==smarty_modifier_enum("BookingStatuses::CONFIRMED")&&($_smarty_tpl->tpl_vars['booking']->value['payment_status']==smarty_modifier_enum("BookingPaymentStatus::COMPLETE")||$_smarty_tpl->tpl_vars['is_student_pcr']->value)) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"Booking Report",'target'=>"#acc_report"), 0);?>

        <div id="acc_report" class="collapse in collapse-visible">
            <div class="control-group">
                <label for="elm_test_result" class="control-label cm-required">Report:</label>
                <div class="controls">
                   <input type="text" name="test_result" id="elm_test_result" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['test_result'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
                </div>
            </div>
        </div>
        <?php } elseif ($_smarty_tpl->tpl_vars['booking']->value['test_result']) {?>
           <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"Booking Report",'target'=>"#acc_report"), 0);?>

            <div id="acc_report" class="collapse in collapse-visible">
                <div class="control-group">
                    <label for="elm_test_result" class="control-label cm-required">Report:</label>
                    <div class="controls">
                    <input type="text" name="test_result" id="elm_test_result" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['test_result'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" readonly="true"/>
                    </div>
                </div>
            </div> 
        <?php }?>
        
        <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("information"),'target'=>"#acc_information"), 0);?>


        <div id="acc_information" class="collapse in collapse-visible">

            <div class="control-group">
                <label for="elm_test_type" class="control-label">Test Type:</label>
                <div class="controls">
                   <input type="text" name="" id="elm_test_type" size="100"  value="<?php if ($_smarty_tpl->tpl_vars['is_student_pcr']->value) {?>Student PCR<?php } else {
echo htmlspecialchars(fn_d_booking_get_booking_type_status_description($_smarty_tpl->tpl_vars['booking']->value['test_type']), ENT_QUOTES, 'UTF-8');
}?>" class="input-large" readonly="true" />
                </div>
            </div>

            
            <div class="control-group">
                <label for="elm_status" class="control-label">Booking Status:</label>
                <div class="controls">
                   <input type="text" name="" id="elm_status" size="100"  value="<?php echo htmlspecialchars(fn_d_booking_get_booking_status_description($_smarty_tpl->tpl_vars['booking']->value['status']), ENT_QUOTES, 'UTF-8');?>
" class="input-large" readonly="true" />
                </div>
            </div>
            

            <div class="control-group">
                <label for="elm_full_name" class="control-label">Patient Name:</label>
                <div class="controls">
                   <input type="text" name="" id="elm_full_name" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['patient']->value['full_name'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" readonly="true" />
                </div>
            </div>

            <div class="control-group">
                <label for="elm_emirate_id" class="control-label">Emirate Id/Passport No:</label>
                <div class="controls">
                   <input type="text" name="" id="elm_emirate_id" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['patient']->value['emirate_id'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" readonly="true" />
                </div>
            </div>

             <div class="control-group">
                <label for="elm_phone_no" class="control-label">Phone No.:</label>
                <div class="controls">
                   <input type="text" name="" id="elm_phone_no" size="100"  value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['patient']->value['phone_no'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" readonly="true" />
                </div>
            </div>

            <?php if ($_smarty_tpl->tpl_vars['is_student_pcr']->value&&$_smarty_tpl->tpl_vars['booking']->value['status']==smarty_modifier_enum("BookingStatuses::OPEN")) {?>
                <div class="control-group">
                    <label for="elm_booking_date" class="control-label cm-required">Booking Date/Slot:</label>
                    <div class="controls">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>"elm_booking_date",'date_name'=>"booking_date",'date_val'=>(($tmp = @$_smarty_tpl->tpl_vars['booking']->value['booking_date'])===null||$tmp==='' ? @constant('TIME') : $tmp),'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year']), 0);?>

                    </div>
                </div>
            <?php } else { ?>
            <div class="control-group">
                <label for="elm_booking_date" class="control-label">Booking Date/Slot:</label>
                <div class="controls">
                   <input type="text" name="" id="elm_booking_date" size="100"  value="<?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['booking']->value['booking_date'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>
" class="input-large" readonly="true" />
                </div>
            </div>
            <?php }?>

            
            <?php if ($_smarty_tpl->tpl_vars['booking']->value['last_booking_date']) {?>
                <div class="control-group">
                <label for="elm_last_booking_date" class="control-label">Last Booking Date:</label>
                <div class="controls">
                   <input type="text" name="" id="elm_last_booking_date" size="100"  value="<?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['booking']->value['last_booking_date'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>
" class="input-large" readonly="true" />
                </div>
            </div>
            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['booking']->value['clinic_id']) {?>
            <div class="control-group">
                <label for="elm_clinic_name" class="control-label">Clinic Details:</label>
                <div class="controls">
                   <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['name'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['address'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['city'], ENT_QUOTES, 'UTF-8');?>
<br/>
                   <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['email'], ENT_QUOTES, 'UTF-8');?>

                   <br/>
                   <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['phone_no'], ENT_QUOTES, 'UTF-8');?>

                   <?php if ($_smarty_tpl->tpl_vars['clinic_details']->value['latitude']&&$_smarty_tpl->tpl_vars['clinic_details']->value['longitude']) {?>
                    <br/>Open Gmap : <a href="https://maps.google.com/?q=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['latitude'], ENT_QUOTES, 'UTF-8');?>
,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['longitude'], ENT_QUOTES, 'UTF-8');?>
">https://maps.google.com/?q=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['latitude'], ENT_QUOTES, 'UTF-8');?>
,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['clinic_details']->value['longitude'], ENT_QUOTES, 'UTF-8');?>
</a>
                    <?php }?>
                </div>
            </div>
            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['booking']->value['address']) {?>
            <div class="control-group">
                <label for="elm_address" class="control-label">Address:</label>
                <div class="controls">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['address']['address'], ENT_QUOTES, 'UTF-8');?>
<br/>
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['address']['street'], ENT_QUOTES, 'UTF-8');?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['address']['region'], ENT_QUOTES, 'UTF-8');?>

                    <?php if ($_smarty_tpl->tpl_vars['booking']->value['address']['lat']&&$_smarty_tpl->tpl_vars['booking']->value['address']['long']) {?>
                    <br/>Open Gmap : <a href="https://maps.google.com/?q=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['address']['lat'], ENT_QUOTES, 'UTF-8');?>
,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['address']['long'], ENT_QUOTES, 'UTF-8');?>
">https://maps.google.com/?q=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['address']['lat'], ENT_QUOTES, 'UTF-8');?>
,<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['address']['long'], ENT_QUOTES, 'UTF-8');?>
</a>
                    <?php }?>
                </div>
            </div>
            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['booking']->value['extra']['alhosan_sh_path']) {?>
                <div class="control-group">
                    <label for="alohosan_sh" class="control-label">Download Alhosan Sh File:</label>
                    <div class="controls">
                    <a href="<?php echo htmlspecialchars(fn_url("d_customer.download_file&file=".((string)$_smarty_tpl->tpl_vars['booking']->value['extra']['alhosan_sh_path'])), ENT_QUOTES, 'UTF-8');?>
">Download</a>
                    </div>
                </div>
            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['booking']->value['extra']['e_card']) {?>
                <div class="control-group">
                    <label for="emirate_id" class="control-label">Download Emirates Id:</label>
                    <div class="controls">
                    <?php $_smarty_tpl->tpl_vars['extra_front'] = new Smarty_variable('', null, 0);?>
                    <?php $_smarty_tpl->tpl_vars['extra_back'] = new Smarty_variable('', null, 0);?>
                    <?php if ($_smarty_tpl->tpl_vars['patient']->value['emirate_id']) {?>
                        <?php $_smarty_tpl->tpl_vars['basename'] = new Smarty_variable(basename($_smarty_tpl->tpl_vars['booking']->value['extra']['e_card']['front_side_path']), null, 0);?>
                        <?php $_smarty_tpl->tpl_vars['ext'] = new Smarty_variable(pathinfo($_smarty_tpl->tpl_vars['basename']->value,@constant('PATHINFO_EXTENSION')), null, 0);?>
                        <?php $_smarty_tpl->tpl_vars['extra_front'] = new Smarty_variable("&name_of_download=".((string)$_smarty_tpl->tpl_vars['patient']->value['emirate_id'])."_front.".((string)$_smarty_tpl->tpl_vars['ext']->value), null, 0);?>
                        <?php $_smarty_tpl->tpl_vars['extra_back'] = new Smarty_variable("&name_of_download=".((string)$_smarty_tpl->tpl_vars['patient']->value['emirate_id'])."_back.".((string)$_smarty_tpl->tpl_vars['ext']->value), null, 0);?>
                    <?php }?>
                    
                    <a href="<?php echo htmlspecialchars(fn_url("d_customer.download_file".((string)$_smarty_tpl->tpl_vars['extra_front']->value)."&file=".((string)$_smarty_tpl->tpl_vars['booking']->value['extra']['e_card']['front_side_path'])), ENT_QUOTES, 'UTF-8');?>
">Download Front Side</a>
                    <br/>
                    <a href="<?php echo htmlspecialchars(fn_url("d_customer.download_file".((string)$_smarty_tpl->tpl_vars['extra_back']->value)."&file=".((string)$_smarty_tpl->tpl_vars['booking']->value['extra']['e_card']['back_side_path'])), ENT_QUOTES, 'UTF-8');?>
">Download Back Side</a>
                    </div>
                </div>
            <?php }?>
            

        </div>
       
       <?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"Service Details",'target'=>"#acc_service"), 0);?>


        <div id="acc_service" class="collapse in collapse-visible">
            
            
            <div class="control-group">
                <label for="elm_payment_status" class="control-label">Payment Status:</label>
                <div class="controls">
                   <input type="text" name="" id="elm_payment_status" size="100"  value="<?php echo htmlspecialchars(fn_d_booking_get_payment_status_description($_smarty_tpl->tpl_vars['booking']->value['payment_status']), ENT_QUOTES, 'UTF-8');?>
" class="input-large" readonly="true" />
                </div>
            </div>

            <div class="control-group">
                <label for="elm_test_price" class="control-label">Test Price:</label>
                <div class="controls">
                   <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['booking']->value['test_price']), 0);?>

                </div>
            </div>

            <div class="control-group">
                <label for="elm_delivery_charge" class="control-label">Service Charge:</label>
                <div class="controls">
                   <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['booking']->value['delivery_charge']), 0);?>

                </div>
            </div>

            <div class="control-group">
                <label for="elm_total" class="control-label">Total Payment:</label>
                <div class="controls">
                   <?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['booking']->value['total']), 0);?>

                </div>
            </div>

        </div>

    <!--content_detailed--></div> 

    

    <?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php $_smarty_tpl->tpl_vars['allow_clone'] = new Smarty_variable(true, null, 0);?>
        
        
        <!-- the button goes here -->
         
        <?php if ($_smarty_tpl->tpl_vars['booking']->value['status']==smarty_modifier_enum("BookingStatuses::OPEN")&&!$_smarty_tpl->tpl_vars['is_student_pcr']->value) {?>
            <a href="<?php echo htmlspecialchars(fn_url("d_booking.reject_booking&booking_id=".((string)$_smarty_tpl->tpl_vars['booking']->value['booking_id'])), ENT_QUOTES, 'UTF-8');?>
"  class="btn btn-primary cm-post cm-no-ajax cm-confirm" style="background: #ef6262;color: white;font-weight: bold; border: 1px solid green;">Reject Booking</a>
            
            <a data-ca-dispatch="dispatch[d_booking.confirm_booking]" data-ca-target-form="booking_update_form" class="btn btn-primary cm-post cm-submit cm-no-ajax dropdown-toggle" style="background: green;color: white;font-weight: bold; border: 1px solid green;">Confirm Booking</a>

        <?php } elseif ($_smarty_tpl->tpl_vars['is_student_pcr']->value&&$_smarty_tpl->tpl_vars['booking']->value['status']==smarty_modifier_enum("BookingStatuses::OPEN")) {?>

            <a href="<?php echo htmlspecialchars(fn_url("d_booking.reject_booking&booking_id=".((string)$_smarty_tpl->tpl_vars['booking']->value['booking_id'])), ENT_QUOTES, 'UTF-8');?>
"  class="btn btn-primary cm-post cm-no-ajax cm-confirm" style="background: #ef6262;color: white;font-weight: bold; border: 1px solid green;">Reject Booking</a>

            <a data-ca-dispatch="dispatch[d_booking.confirm_booking]" data-ca-target-form="booking_update_form" class="btn btn-primary cm-post cm-submit cm-no-ajax dropdown-toggle" style="background: green;color: white;font-weight: bold; border: 1px solid green;">Confirm Booking</a>

        <?php } elseif ($_smarty_tpl->tpl_vars['is_student_pcr']->value&&$_smarty_tpl->tpl_vars['booking']->value['status']==smarty_modifier_enum("BookingStatuses::CONFIRMED")) {?>
            <a data-ca-dispatch="dispatch[d_booking.complete_booking]" data-ca-target-form="booking_update_form" class="btn btn-primary cm-post cm-submit cm-no-ajax dropdown-toggle" style="background: green; border: 1px solid green;">Booking Complete</a>
        <?php } elseif (!$_smarty_tpl->tpl_vars['is_student_pcr']->value&&$_smarty_tpl->tpl_vars['booking']->value['status']==smarty_modifier_enum("BookingStatuses::CONFIRMED")&&$_smarty_tpl->tpl_vars['booking']->value['payment_status']!=smarty_modifier_enum("BookingPaymentStatus::COMPLETE")) {?>
            <a data-ca-dispatch="dispatch[d_booking.pay_booking]" data-ca-target-form="booking_update_form" class="btn btn-primary cm-post cm-submit cm-no-ajax dropdown-toggle">Booking Payed</a>
        <?php } elseif ($_smarty_tpl->tpl_vars['booking']->value['payment_status']==smarty_modifier_enum("BookingPaymentStatus::COMPLETE")&&$_smarty_tpl->tpl_vars['booking']->value['status']!=smarty_modifier_enum("BookingStatuses::COMPLETE")) {?>
            <a data-ca-dispatch="dispatch[d_booking.complete_booking]" data-ca-target-form="booking_update_form" class="btn btn-primary cm-post cm-submit cm-no-ajax dropdown-toggle" style="background: green; border: 1px solid green;">Booking Complete</a>
        <?php }?>
        <!-- the button goes there -->
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
</form>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>


<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
    Booking Details #<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['booking']->value['booking_id'], ENT_QUOTES, 'UTF-8');?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>


<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title_start'=>$_smarty_tpl->tpl_vars['title_start']->value,'title_end'=>$_smarty_tpl->tpl_vars['title_end']->value,'title'=>Smarty::$_smarty_vars['capture']['mainbox_title'],'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'select_languages'=>false,'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons']), 0);?>
<?php }} ?>
