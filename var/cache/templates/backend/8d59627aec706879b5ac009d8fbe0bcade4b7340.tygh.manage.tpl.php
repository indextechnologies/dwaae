<?php /* Smarty version Smarty-3.1.21, created on 2022-03-01 15:27:21
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_booking/views/d_clinic/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:374218884621e03198648a6-59361558%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d59627aec706879b5ac009d8fbe0bcade4b7340' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_booking/views/d_clinic/manage.tpl',
      1 => 1631688534,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '374218884621e03198648a6-59361558',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'search' => 0,
    'ajax_class' => 0,
    'c_url' => 0,
    'sort_sign' => 0,
    'clinics' => 0,
    'q' => 0,
    'settings' => 0,
    'current_redirect_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621e03198c9c18_10900894',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621e03198c9c18_10900894')) {function content_621e03198c9c18_10900894($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_emr.clinic_id','ec_emr.email','ec_emr.name','ec_emr.timestamp','ec_emr.status','delete','no_data','ec_emr.clinic'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
    <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
    <?php if ($_smarty_tpl->tpl_vars['search']->value['sort_order']=="asc") {?>
    <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"icon-down-dir\"></i>", null, 0);?>
    <?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"icon-up-dir\"></i>", null, 0);?>
    <?php }?>
    <?php if (!$_smarty_tpl->tpl_vars['config']->value['tweaks']['disable_dhtml']) {?>
        <?php $_smarty_tpl->tpl_vars["ajax_class"] = new Smarty_variable("cm-ajax", null, 0);?>

    <?php }?>
    <?php if (@constant('ACCOUNT_TYPE')!="admin") {?>
        <?php $_smarty_tpl->tpl_vars['not_main_admin'] = new Smarty_variable(true, null, 0);?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars['not_main_admin'] = new Smarty_variable(false, null, 0);?>
    <?php }?>
    
    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <table class="table orders-search">
        <thead>
            <tr>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=clinic_id&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("ec_emr.clinic_id");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="clinic_id") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=email&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("ec_emr.email");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="email") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=name&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("ec_emr.name");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="name") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=timestamp&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("ec_emr.timestamp");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="timestamp") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=status&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("ec_emr.status");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="status") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
            </tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars["q"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["q"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['clinics']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["q"]->key => $_smarty_tpl->tpl_vars["q"]->value) {
$_smarty_tpl->tpl_vars["q"]->_loop = true;
?>
            <tr>
                <td class="orders-search__item"><a href="<?php echo htmlspecialchars(fn_url("d_clinic.update?clinic_id=".((string)$_smarty_tpl->tpl_vars['q']->value['clinic_id'])), ENT_QUOTES, 'UTF-8');?>
"><strong>#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['clinic_id'], ENT_QUOTES, 'UTF-8');?>
</strong></a></td>
                <td class="orders-search__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['email'], ENT_QUOTES, 'UTF-8');?>
</td>
                <td class="orders-search__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['name'], ENT_QUOTES, 'UTF-8');?>
</td>
                <td class="orders-search__item">
                    <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['q']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>

                </td>
                <td>
                    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_items", null, null); ob_start(); ?>
                        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'href'=>"d_clinic.update?clinic_id=".((string)$_smarty_tpl->tpl_vars['q']->value['clinic_id']),'text'=>"View Details"));?>
</li>
                        <?php $_smarty_tpl->tpl_vars["current_redirect_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
                        <li><?php ob_start();?><?php echo $_smarty_tpl->__("delete");?>
<?php $_tmp1=ob_get_clean();?><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'href'=>"d_clinic.delete?clinic_id=".((string)$_smarty_tpl->tpl_vars['q']->value['clinic_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['current_redirect_url']->value),'class'=>"cm-confirm",'text'=>$_tmp1,'method'=>"POST"));?>
</li>
                    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                    <div class="hidden-tools">
                        <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_items']));?>

                    </div>
                </td>
                <td class="orders-search__item">
                    <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/status_on_manage.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('popup_additional_class'=>"dropleft",'id'=>$_smarty_tpl->tpl_vars['q']->value['clinic_id'],'status'=>$_smarty_tpl->tpl_vars['q']->value['status'],'hidden'=>true,'object_id_name'=>"clinic_id",'table'=>"d_clinic"), 0);?>

                </td>
            </tr>
        <?php }
if (!$_smarty_tpl->tpl_vars["q"]->_loop) {
?>
            <tr class="table__no-items">
                <td colspan="7"><p class="no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p></td>
            </tr>
        <?php } ?>
    </table>
    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("sidebar", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/d_booking/views/d_clinic/components/search.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dispatch'=>"d_clinic.manage"), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tool_href'=>"d_clinic.add",'prefix'=>"top",'title'=>"Add new clinic",'hide_tools'=>true,'icon'=>"icon-plus"), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("ec_emr.clinic");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['mainbox'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'title'=>Smarty::$_smarty_vars['capture']['mainbox_title'],'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar']), 0);?>


<?php }} ?>
