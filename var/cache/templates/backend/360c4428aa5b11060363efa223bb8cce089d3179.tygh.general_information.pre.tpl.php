<?php /* Smarty version Smarty-3.1.21, created on 2022-03-02 15:28:17
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/companies/general_information.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:399855624621f54d1be9626-52911726%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '360c4428aa5b11060363efa223bb8cce089d3179' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/companies/general_information.pre.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '399855624621f54d1be9626-52911726',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'company_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621f54d1bee7e0_71996537',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621f54d1bee7e0_71996537')) {function content_621f54d1bee7e0_71996537($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_custom.company_full_name'));
?>
<div class="control-group">
    <label for="elm_company_full_name" class="control-label cm-required"><?php echo $_smarty_tpl->__("d_custom.company_full_name");?>
:</label>
    <div class="controls">
        <input type="text" name="company_data[company_full_name]" id="elm_company_full_name" size="32" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['company_full_name'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
    </div>
</div><?php }} ?>
