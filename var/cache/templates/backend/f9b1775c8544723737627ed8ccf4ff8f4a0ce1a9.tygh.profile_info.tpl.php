<?php /* Smarty version Smarty-3.1.21, created on 2022-03-10 12:52:32
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/profile_info.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15819281176229bc50bf27b1-94104608%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f9b1775c8544723737627ed8ccf4ff8f4a0ce1a9' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/profile_info.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '15819281176229bc50bf27b1-94104608',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'quotation' => 0,
    'id' => 0,
    'location' => 0,
    'location_id' => 0,
    'settings' => 0,
    'countries' => 0,
    '_country' => 0,
    'code' => 0,
    'country' => 0,
    'states' => 0,
    '_state' => 0,
    'state' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6229bc50c1a6d1_95043567',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6229bc50c1a6d1_95043567')) {function content_6229bc50c1a6d1_95043567($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('h_rfq.firstname','h_rfq.lastname','h_rfq.address','h_rfq.country','h_rfq.state','h_rfq.city','h_rfq.zipcode','h_rfq.phone','shipping_address'));
?>
<?php $_smarty_tpl->tpl_vars['allow_edit'] = new Smarty_variable(false, null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['quotation']->value['locations']) {?>
    <?php $_smarty_tpl->tpl_vars['location'] = new Smarty_variable(reset($_smarty_tpl->tpl_vars['quotation']->value['locations']), null, 0);?>
<?php }?>

<?php $_smarty_tpl->_capture_stack[0][] = array("shipping_address", null, null); ob_start(); ?>
    <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
        <?php $_smarty_tpl->tpl_vars['location_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['location']->value['location_id'], null, 0);?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars['location_id'] = new Smarty_variable(uniqid(), null, 0);?>
    <?php }?>
    <input type="hidden" name="location_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
" />
    <?php $_smarty_tpl->tpl_vars['_country'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['location']->value['country'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_country'] : $tmp), null, 0);?>
    <?php $_smarty_tpl->tpl_vars['_state'] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['location']->value['state'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['settings']->value['Checkout']['default_state'] : $tmp), null, 0);?>
    <?php if ($_smarty_tpl->tpl_vars['id']->value) {?>
        <input name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][location_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
" type="hidden" />
    <?php }?>
    <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.firstname");?>
:
        <input type="text" name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][firstname]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
" class="input-small h-rfq_sidebar_input"  />
    </p>
    <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.lastname");?>
:
        <input type="text" name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][lastname]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
" class="input-small h-rfq_sidebar_input"  />
    </p>
    <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.address");?>
:
        <input type="text" name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][address]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value['address'], ENT_QUOTES, 'UTF-8');?>
" class="input-small h-rfq_sidebar_input"  />
    </p>
    <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.country");?>
:
        <select id="el_country_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
" class="ty-profile-field__select-country cm-country cm-location-shipping" name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][country]"  >
            <?php  $_smarty_tpl->tpl_vars["country"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["country"]->_loop = false;
 $_smarty_tpl->tpl_vars["code"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["country"]->key => $_smarty_tpl->tpl_vars["country"]->value) {
$_smarty_tpl->tpl_vars["country"]->_loop = true;
 $_smarty_tpl->tpl_vars["code"]->value = $_smarty_tpl->tpl_vars["country"]->key;
?>
            <option <?php if ($_smarty_tpl->tpl_vars['_country']->value==$_smarty_tpl->tpl_vars['code']->value) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['country']->value, ENT_QUOTES, 'UTF-8');?>
</option>
            <?php } ?>
        </select>
    </p>
    <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.state");?>
:
        <select id="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
"  name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][state]" class="input-small h-rfq_sidebar_input cm-state cm-location-shipping" >
             <?php if ($_smarty_tpl->tpl_vars['states']->value&&$_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]) {?>
                <?php  $_smarty_tpl->tpl_vars['state'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['state']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['states']->value[$_smarty_tpl->tpl_vars['_country']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['state']->key => $_smarty_tpl->tpl_vars['state']->value) {
$_smarty_tpl->tpl_vars['state']->_loop = true;
?>
                    <option <?php if ($_smarty_tpl->tpl_vars['_state']->value==$_smarty_tpl->tpl_vars['state']->value['code']) {?>selected="selected"<?php }?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['code'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['state']->value['state'], ENT_QUOTES, 'UTF-8');?>
</option>
                <?php } ?>
            <?php }?>
        </select>
        <input  type="text" id="el_state_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
_d" name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][state]" size="32" maxlength="64" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_state']->value, ENT_QUOTES, 'UTF-8');?>
" disabled="disabled" class="cm-state cm-location-shipping ty-input-text hidden"  />
    </p>
    <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.city");?>
:
        <input type="text" name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][city]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value['city'], ENT_QUOTES, 'UTF-8');?>
" class="input-small h-rfq_sidebar_input"  />
    </p>
    <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.zipcode");?>
:
        <input type="text" name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][zipcode]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value['zipcode'], ENT_QUOTES, 'UTF-8');?>
" class="input-small h-rfq_sidebar_input"  />
    </p>
    <p class="h-rfq_sidebar_field"><span class="h-rfq_sidebar_label"><?php echo $_smarty_tpl->__("h_rfq.phone");?>
:
        <input type="text" name="quotation[delivery_location][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location_id']->value, ENT_QUOTES, 'UTF-8');?>
][phone]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['location']->value['phone'], ENT_QUOTES, 'UTF-8');?>
" class="input-small h-rfq_sidebar_input"  />
    </p>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<hr class="profile-info-delim" />

 <div class="sidebar-row">
    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("shipping_address"),'target'=>"#acc_shipping_address",'meta'=>"h-rfq_sidebar_header",'compressed'=>true), 0);?>

    <div id="acc_shipping_address" class="h_rfq-sidebar collapse" >
        <?php echo Smarty::$_smarty_vars['capture']['shipping_address'];?>

    </div>
</div><?php }} ?>
