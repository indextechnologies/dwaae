<?php /* Smarty version Smarty-3.1.21, created on 2022-03-01 18:19:40
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/ec_homepage_layout/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:998738235621e2b7c1b10c9-53519236%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '114700495bcce2151a71becba77814ec6e5bbfa3' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/ec_homepage_layout/manage.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '998738235621e2b7c1b10c9-53519236',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'banners' => 0,
    'product_groups' => 0,
    'categories' => 0,
    'homepage_objects' => 0,
    'object' => 0,
    '_key' => 0,
    'no_hide_input_if_shared_product' => 0,
    '_options' => 0,
    'id' => 0,
    'name' => 0,
    'new_key' => 0,
    'title_start' => 0,
    'title_end' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621e2b7c226946_88906137',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621e2b7c226946_88906137')) {function content_621e2b7c226946_88906137($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/dwaae/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_function_cycle')) include '/home/dwaae/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_block_inline_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('type','smart_app.data','type','banner','products','categories','smart_app.data','none','none','type','banner','products','categories','data','none','none','ec_homepage_layout'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

<form name="homepage_objects" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post">
    <input type="hidden" id="banners-data" value="<?php echo htmlspecialchars(json_encode($_smarty_tpl->tpl_vars['banners']->value), ENT_QUOTES, 'UTF-8');?>
" />
    <input type="hidden" id="product_groups-data" value="<?php echo htmlspecialchars(json_encode($_smarty_tpl->tpl_vars['product_groups']->value), ENT_QUOTES, 'UTF-8');?>
" />
    <input type="hidden" id="categories-data" value="<?php echo htmlspecialchars(json_encode($_smarty_tpl->tpl_vars['categories']->value), ENT_QUOTES, 'UTF-8');?>
" />
    <div class="items-container cm-ecsortable ui-sortable" id="statuses_list" data-ca-sortable-table="ec_statuses" data-ca-sortable-id-name="status_id">
        <div class="table-responsive-wrapper">
            <table class="table table-middle table--relative table-responsive" width="100%">
            <thead class="cm-first-sibling">
            <tr>
                <th width="3%"></th>
                <th width="20%"><?php echo $_smarty_tpl->__("type");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>"Select type to for show different type of content on homepage"), 0);?>
</th>
                <th width="55%"><?php echo $_smarty_tpl->__("smart_app.data");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>"Select type data"), 0);?>
</th>
                <th width="">&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php  $_smarty_tpl->tpl_vars["object"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["object"]->_loop = false;
 $_smarty_tpl->tpl_vars["_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['homepage_objects']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["object"]->key => $_smarty_tpl->tpl_vars["object"]->value) {
$_smarty_tpl->tpl_vars["object"]->_loop = true;
 $_smarty_tpl->tpl_vars["_key"]->value = $_smarty_tpl->tpl_vars["object"]->key;
?>
            <?php if ($_smarty_tpl->tpl_vars['object']->value['data']) {?>
            <tr class="cm-row-item cm-sortable-row">
                <td class="no-padding-td">
                    <span class="handler cm-sortable-handle"></span>
                </td>
                <input type="hidden" name="objects[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][position]" value="" class="input-micro" />
                <td class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("type");?>
">
                    <select name="objects[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][type]" class="select-type">
                        <option value="banner" <?php if ($_smarty_tpl->tpl_vars['object']->value['type']=="banner") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("banner");?>
</option>
                        <option value="products" <?php if ($_smarty_tpl->tpl_vars['object']->value['type']=="products") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("products");?>
</option>
                        <option value="categories" <?php if ($_smarty_tpl->tpl_vars['object']->value['type']=="categories") {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->__("categories");?>
</option>
                    </select>
                </td>
                <td class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("smart_app.data");?>
">
                    <?php if ($_smarty_tpl->tpl_vars['object']->value['type']=="banner") {?>
                        <?php $_smarty_tpl->tpl_vars['_options'] = new Smarty_variable($_smarty_tpl->tpl_vars['banners']->value, null, 0);?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['object']->value['type']=="products") {?>
                        <?php $_smarty_tpl->tpl_vars['_options'] = new Smarty_variable($_smarty_tpl->tpl_vars['product_groups']->value, null, 0);?>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['object']->value['type']=="categories") {?>
                        <?php $_smarty_tpl->tpl_vars['_options'] = new Smarty_variable($_smarty_tpl->tpl_vars['categories']->value, null, 0);?>
                    <?php }?>
                    

                    <div class="object-selector ec-object-selector">
                        <select
                                class="cm-object-selector ec-object-selector"
                                name="objects[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][data][]"
                                data-ca-enable-search="false"
                                <?php if ($_smarty_tpl->tpl_vars['object']->value['type']=="banner"||$_smarty_tpl->tpl_vars['object']->value['type']=="categories") {?>multiple<?php }?>
                                data-ca-placeholder="-<?php echo $_smarty_tpl->__("none");?>
-"
                                data-ca-allow-clear="true"
                                >
                            <option value="">-<?php echo $_smarty_tpl->__("none");?>
-</option>
                            <?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['_options']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['name']->key;
?>
                                <option
                                    value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"
                                    <?php if (in_array($_smarty_tpl->tpl_vars['id']->value,$_smarty_tpl->tpl_vars['object']->value['data'])) {?> selected="selected"<?php }?>
                                ><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                            <?php } ?>
                            
                        </select>
                    </div>
                </td>
                <td class="nowrap <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
 right">
                    <?php echo $_smarty_tpl->getSubTemplate ("buttons/clone_delete.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dummy_href'=>true,'microformats'=>"cm-delete-row",'no_confirm'=>true), 0);?>

                </td>
            </tr>
            <?php }?>
            <?php } ?>
            <?php echo smarty_function_math(array('equation'=>"x+1",'x'=>(($tmp = @$_smarty_tpl->tpl_vars['_key']->value)===null||$tmp==='' ? 0 : $tmp),'assign'=>"new_key"),$_smarty_tpl);?>

            <tr class="<?php echo smarty_function_cycle(array('values'=>"table-row , ",'reset'=>1),$_smarty_tpl);
echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
 cm-row-item cm-sortable-row" id="box_add_home_object">
                <td class="no-padding-td">
                    <span class="handler cm-sortable-handle"></span>
                </td>

                <input type="hidden" name="objects[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][position]" value="" class="input-micro" />
                <td width="20%" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("type");?>
">
                    <select name="objects[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][type]" class="select-type">
                        <option value="banner" selected="selected"><?php echo $_smarty_tpl->__("banner");?>
</option>
                        <option value="products"><?php echo $_smarty_tpl->__("products");?>
</option>
                        <option value="categories"><?php echo $_smarty_tpl->__("categories");?>
</option>
                    </select>
                </td>
                <td class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("data");?>
">
                    

                    <div class="object-selector ec-object-selector">
                        <select
                                class="cm-object-selector ec-object-selector"
                                name="objects[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][data][]"
                                data-ca-enable-search="false"
                                multiple
                                data-ca-placeholder="-<?php echo $_smarty_tpl->__("none");?>
-"
                                data-ca-allow-clear="true"
                                >
                            <option value="">-<?php echo $_smarty_tpl->__("none");?>
-</option>
                            <?php  $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['name']->_loop = false;
 $_smarty_tpl->tpl_vars['id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['banners']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['name']->key => $_smarty_tpl->tpl_vars['name']->value) {
$_smarty_tpl->tpl_vars['name']->_loop = true;
 $_smarty_tpl->tpl_vars['id']->value = $_smarty_tpl->tpl_vars['name']->key;
?>
                                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['name']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                            <?php } ?>
                        </select>
                    </div>
                </td>
                <td class="right">
                    <?php echo $_smarty_tpl->getSubTemplate ("buttons/multiple_buttons.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('item_id'=>"add_home_object"), 0);?>

                </td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>
</form>

<?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
>
    var bannerDataID = "banners-data";
    var productsGroupID = "product_groups-data";
    var categoryGroupId = "categories-data";
    function getOptionsHtml(id){
        var o = $("#"+id).val();
        var options = JSON.parse(o);
        var optionHtml = '';
        $.each(options,function(k,v){
            optionHtml += '<option value="'+k+'">'+v+'</option>';
        });
        return optionHtml;
    }
    $(document).on('change',".select-type",function(){
        var v = $(this).val();
        if(v == "products"){
            var optionHtml = getOptionsHtml(productsGroupID);
            $(this).parent().next().find('select').attr('multiple',false).html(optionHtml);
            $(this).parent().next().find('.select2-selection--multiple').addClass('ec_change');
            $(this).parent().next().find('.select2-selection--multiple').addClass('select2-selection--single');
            $(this).parent().next().find('.select2-selection--multiple').removeClass('select2-selection--multiple');
        }else if(v == "banner"){
            var optionHtml = getOptionsHtml(bannerDataID);
            $(this).parent().next().find('select').attr('multiple',true).html(optionHtml);
            $(this).parent().next().find('.select2-selection--single').removeClass('ec_change');
            $(this).parent().next().find('.select2-selection--single').addClass('select2-selection--multiple');
            $(this).parent().next().find('.select2-selection--single').removeClass('select2-selection--single');
        }else if(v == "categories"){
            var optionHtml = getOptionsHtml(categoryGroupId);
            $(this).parent().next().find('select').attr('multiple',true).html(optionHtml);
            $(this).parent().next().find('.select2-selection--single').removeClass('ec_change');
            $(this).parent().next().find('.select2-selection--single').addClass('select2-selection--multiple');
            $(this).parent().next().find('.select2-selection--single').removeClass('select2-selection--single');
        }
    });



    /*
     *
     * Sortables
     *
     */
    (function($){
        var methods = {
            init: function(params) {
                return this.each(function() {
                    var params = params || {};
                    var self = $(this);

                    var sortable_params = {
                        accept: 'cm-sortable-row',
                        items: '.cm-row-item',
                        tolerance: 'pointer',
                        axis: 'y',
                        containment: 'parent',
                        opacity: '0.9',
                        update: function(event, ui) {
                            return true;
                        }
                    };

                    // If we have sortable handle, update default params
                    if ($('.cm-sortable-handle', self).length) {
                        sortable_params = $.extend(sortable_params, {
                            opacity: '0.5',
                            handle: '.cm-sortable-handle'
                        });
                    }

                    self.sortable(sortable_params);
                });
            }
        };

        $.fn.ceEcSortable = function(method) {
            if (methods[method]) {
                return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply(this, arguments);
            } else {
                $.error('ty.sortable: method ' +  method + ' does not exist');
            }
        };

        $.ceEvent('on', 'ce.commoninit', function(context) {
            $('.cm-ecsortable', context).ceEcSortable();
        });

    })($);
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>


<style>
    .ec-object-selector{
        width: 100%;
        margin: 0;
    }
    .object-selector.ec-object-selector{
        min-height: 100px;
        display: flex;
        align-items: center;
    }
    .ec-object-selector .ec-object-selector{
        max-width: 600px;
    }
    .ec-object-selector .select2-container{
        width: 100% !important;
    }
   
    .ec-object-selector .select2-selection--multiple{
        padding-left: 5px;
    }
    .ec-object-selector .select2-selection--single .select2-selection__clear{
        position: absolute;
        right: 20px;
    }
    .ec-object-selector .select2-selection--single.ec_change ul{
        list-style: none;
        margin-left: 0;
    }
        
    .ec-object-selector .select2-selection--single.ec_change .select2-selection__choice{
        font-size: 13px;
        margin-top: 4px;
    }
    .ec-object-selector .select2-selection--single.ec_change .select2-selection__choice__remove{
        display: none;
    }
    .ec-object-selector .select2-selection--single.ec_change .select2-search--inline{
        display: none;
    }
</style>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("buttons/save.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_meta'=>'','but_role'=>"submit-link",'but_name'=>"dispatch[ec_homepage_layout.manage]",'but_target_form'=>"homepage_objects"), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->__("ec_homepage_layout");?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title_start'=>$_smarty_tpl->tpl_vars['title_start']->value,'title_end'=>$_smarty_tpl->tpl_vars['title_end']->value,'title'=>Smarty::$_smarty_vars['capture']['mainbox_title'],'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'select_languages'=>$_smarty_tpl->tpl_vars['id']->value,'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons']), 0);?>
<?php }} ?>
