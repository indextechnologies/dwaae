<?php /* Smarty version Smarty-3.1.21, created on 2022-03-10 12:52:32
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/payment_details.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12554394376229bc50d4b883-48412903%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c1ffae300e57cf8aff3f5e0272689f281c2b88da' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/payment_details.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '12554394376229bc50d4b883-48412903',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'quotation' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6229bc50d63d28_73358404',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6229bc50d63d28_73358404')) {function content_6229bc50d63d28_73358404($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_enum')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.enum.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('h_rfq.remittance_advice','h_rfq.lpo','h_rfq.payment_reference','h_rfq.timestamp','h_rfq.result','h_rfq.response_code','h_rfq.amount','h_rfq.currency','h_rfq.reference_no','h_rfq.transaction_id','h_rfq.card_brand','h_rfq.card_first_six_digits','h_rfq.card_last_four_digits'));
?>
<div id="content_payment_details" class="">
    
    <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_details']) {?>
     <div class="table-responsive-wrapper">
    <table width="100%" class="table table-middle table--relative table-responsive">
        <?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename']) {?>
            <tr class="ty-orders-summary__row">
                <td class="ty-orders-summary__total"><?php if ($_smarty_tpl->tpl_vars['quotation']->value['payment_type']==smarty_modifier_enum("RfqPaymentTypes::CREDIT_AND_DEBIT_CARD")) {
echo $_smarty_tpl->__("h_rfq.remittance_advice");
} else {
echo $_smarty_tpl->__("h_rfq.lpo");
}?>:&nbsp;</td>
                <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                    <a href="<?php echo htmlspecialchars(fn_url("h_rfq_attachments.download_file&id=".((string)$_smarty_tpl->tpl_vars['quotation']->value['rfq_id'])."&filename=".((string)$_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename'])."&pre_path=".((string)$_smarty_tpl->tpl_vars['quotation']->value['payment_details']['pre_path'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['filename'], ENT_QUOTES, 'UTF-8');?>
</a>
                </td>
            </tr>
        <?php } else { ?>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.payment_reference");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['payment_reference'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.timestamp");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['timestamp'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.result");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['result'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.response_code");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['response_code'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.amount");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['amount'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.currency");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['currency'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.reference_no");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['reference_no'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.transaction_id");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['transaction_id'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.card_brand");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['card_brand'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.card_first_six_digits");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['card_first_six_digits'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
            <tr><td><?php echo $_smarty_tpl->__("h_rfq.card_last_four_digits");?>
:</td> <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['quotation']->value['payment_details']['verification_response']['card_last_four_digits'], ENT_QUOTES, 'UTF-8');?>
</td></tr>
        <?php }?>
    </table>
    </div>
    <?php }?>
</div><?php }} ?>
