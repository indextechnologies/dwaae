<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:40:22
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/hooks/orders/advanced_search.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4040028856214a1761d03c1-24644808%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aa8456e08854721dff2d6949f19e89fba0be267f' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/hooks/orders/advanced_search.post.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '4040028856214a1761d03c1-24644808',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'search' => 0,
    'search_slots' => 0,
    'slot' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214a1761da680_14032345',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214a1761da680_14032345')) {function content_6214a1761da680_14032345($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_smart_app.delivery_period','ec_smart_app.delivery_slot_time'));
?>
<div class="group form-horizontal">
    <div class="control-group">
        <label class="control-label"><?php echo $_smarty_tpl->__("ec_smart_app.delivery_period");?>
</label>
        <div class="controls">
            <?php echo $_smarty_tpl->getSubTemplate ("common/period_selector.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('period'=>$_smarty_tpl->tpl_vars['search']->value['delivery_period'],'form_name'=>"orders_search_form",'prefix'=>"delivery_"), 0);?>

        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="delivery_slot_time"><?php echo $_smarty_tpl->__("ec_smart_app.delivery_slot_time");?>
</label>
        <div class="controls">
        <select name="delivery_slot_time" id="delivery_slot_time">
            <option value="">--</option>
            <?php  $_smarty_tpl->tpl_vars['slot'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slot']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['search_slots']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slot']->key => $_smarty_tpl->tpl_vars['slot']->value) {
$_smarty_tpl->tpl_vars['slot']->_loop = true;
?>
            <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slot']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['search']->value['delivery_slot_time']==$_smarty_tpl->tpl_vars['slot']->value) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slot']->value, ENT_QUOTES, 'UTF-8');?>
</option>
            <?php } ?>
        </select>
        </div>
    </div>
</div><?php }} ?>
