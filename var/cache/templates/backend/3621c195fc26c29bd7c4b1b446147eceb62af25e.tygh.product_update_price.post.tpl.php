<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:46:09
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/products/product_update_price.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3496406306214a2d1342852-21068799%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3621c195fc26c29bd7c4b1b446147eceb62af25e' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/products/product_update_price.post.tpl',
      1 => 1604380631,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3496406306214a2d1342852-21068799',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'no_hide_input_if_shared_product' => 0,
    'primary_currency' => 0,
    'currencies' => 0,
    'product_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214a2d135bb15_67854511',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214a2d135bb15_67854511')) {function content_6214a2d135bb15_67854511($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('d_custom.price_to_pharmacy','d_custom.otc_product','d_custom.unit_price_to_pharmacy','d_custom.unit_price_to_public'));
?>
<div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
    <label for="elm_price_to_pharmacy" class="control-label"><?php echo $_smarty_tpl->__("d_custom.price_to_pharmacy");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
):</label>
    <div class="controls">
        <input type="text" name="product_data[price_to_pharmacy]" id="elm_price_to_pharmacy" size="10" value="<?php echo htmlspecialchars(fn_format_price((($tmp = @$_smarty_tpl->tpl_vars['product_data']->value['price_to_pharmacy'])===null||$tmp==='' ? "0.00" : $tmp),$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" class="input-long" />
    </div>
</div>




<?php if (@constant('ACCOUNT_TYPE')=='admin') {?>
<div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
    <label for="elm_not_allow_to_sell" class="control-label">Not Allow to Sell:</label>
    <div class="controls">
        <input type="hidden" name="product_data[not_allow_to_sell]" value="N" />
        <input type="checkbox" name="product_data[not_allow_to_sell]" id="elm_not_allow_to_sell" size="30" value="Y"  <?php if ($_smarty_tpl->tpl_vars['product_data']->value['not_allow_to_sell']=='Y') {?>checked="checked"<?php }?>/>
    </div>
</div>
<?php }?>

<div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
    <label for="elm_otc_product" class="control-label"><?php echo $_smarty_tpl->__("d_custom.otc_product");?>
 :</label>
    <div class="controls">
        <input type="hidden" name="product_data[otc_product]" value="N" />
        <input type="checkbox" name="product_data[otc_product]" id="elm_otc_product" size="30" value="Y"  <?php if ($_smarty_tpl->tpl_vars['product_data']->value['otc_product']=='Y') {?>checked="checked"<?php }?> <?php if ($_smarty_tpl->tpl_vars['product_data']->value['master_product_id']) {?>readonly<?php }?>/>
    </div>
</div>

<div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
    <label for="elm_unit_price_to_pharmacy" class="control-label"><?php echo $_smarty_tpl->__("d_custom.unit_price_to_pharmacy");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
):</label>
    <div class="controls">
        <input type="text" name="product_data[unit_price_to_pharmacy]" id="elm_unit_price_to_pharmacy" size="10" value="<?php echo htmlspecialchars(fn_format_price((($tmp = @$_smarty_tpl->tpl_vars['product_data']->value['unit_price_to_pharmacy'])===null||$tmp==='' ? "0.00" : $tmp),$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" class="input-long" />
    </div>
</div>

<div class="control-group <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
">
    <label for="elm_unit_price_to_public" class="control-label"><?php echo $_smarty_tpl->__("d_custom.unit_price_to_public");?>
 (<?php echo $_smarty_tpl->tpl_vars['currencies']->value[$_smarty_tpl->tpl_vars['primary_currency']->value]['symbol'];?>
):</label>
    <div class="controls">
        <input type="text" name="product_data[unit_price_to_public]" id="elm_unit_price_to_public" size="10" value="<?php echo htmlspecialchars(fn_format_price((($tmp = @$_smarty_tpl->tpl_vars['product_data']->value['unit_price_to_public'])===null||$tmp==='' ? "0.00" : $tmp),$_smarty_tpl->tpl_vars['primary_currency']->value,null,false), ENT_QUOTES, 'UTF-8');?>
" class="input-long" />
    </div>
</div>
<?php }} ?>
