<?php /* Smarty version Smarty-3.1.21, created on 2022-03-10 12:52:32
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/discussion.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15242769006229bc50c91055-63544984%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '251aa86eaa65f1644f48e1feb321c37408e482cf' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/discussion.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '15242769006229bc50c91055-63544984',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user_id' => 0,
    'object_company_id' => 0,
    'is_allowed_to_add_posts' => 0,
    'runtime' => 0,
    'is_owned_object' => 0,
    'is_company_reviews' => 0,
    'discussion' => 0,
    'post' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6229bc50ca7a91_72729129',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6229bc50ca7a91_72729129')) {function content_6229bc50ca7a91_72729129($_smarty_tpl) {?><?php if (!is_callable('smarty_function_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/function.script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('add_post','no_data'));
?>
<?php if (true) {?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/new_post.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('user_id'=>$_smarty_tpl->tpl_vars['user_id']->value,'object_company_id'=>$_smarty_tpl->tpl_vars['object_company_id']->value), 0);?>

    <?php $_smarty_tpl->tpl_vars['is_allowed_to_add_posts'] = new Smarty_variable(true, null, 0);?>
    <?php $_smarty_tpl->tpl_vars['is_allowed_to_update_posts'] = new Smarty_variable(true, null, 0);?>
    <?php $_smarty_tpl->tpl_vars['is_owned_object'] = new Smarty_variable(true, null, 0);?>
    <?php $_smarty_tpl->tpl_vars['is_company_reviews'] = new Smarty_variable(true, null, 0);?>
    <?php $_smarty_tpl->tpl_vars['allow_save'] = new Smarty_variable(true, null, 0);?>

    <div id="content_discussion">
    
        <div class="clearfix">
            <div class="buttons-container buttons-bg pull-right">
                <?php if ($_smarty_tpl->tpl_vars['is_allowed_to_add_posts']->value&&!($_smarty_tpl->tpl_vars['runtime']->value['company_id']&&(!$_smarty_tpl->tpl_vars['is_owned_object']->value||$_smarty_tpl->tpl_vars['is_company_reviews']->value))) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"add_new_post",'link_text'=>$_smarty_tpl->__("add_post"),'act'=>"general",'link_class'=>"cm-dialog-switch-avail"), 0);?>

                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['discussion']->value['posts']) {?>

                <?php }?>
            </div>
        </div><br>

        <?php if ($_smarty_tpl->tpl_vars['discussion']->value['posts']) {?>

            <?php echo smarty_function_script(array('src'=>"js/addons/discussion/discussion.js"),$_smarty_tpl);?>

            <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('save_current_page'=>true,'id'=>"pagination_discussion",'search'=>$_smarty_tpl->tpl_vars['discussion']->value['search']), 0);?>


            <div class="posts-container cm-hide-inputs">
                <?php  $_smarty_tpl->tpl_vars["post"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["post"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['discussion']->value['posts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["post"]->key => $_smarty_tpl->tpl_vars["post"]->value) {
$_smarty_tpl->tpl_vars["post"]->_loop = true;
?>
                    <div class="post-item <?php if ($_smarty_tpl->tpl_vars['discussion']->value['object_type']==@constant('DISCUSSION_OBJECT_TYPE_RFQ')) {
if ($_smarty_tpl->tpl_vars['post']->value['user_id']==$_smarty_tpl->tpl_vars['user_id']->value) {?>incoming<?php } else { ?>outgoing<?php }
}?>">
                        <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/post.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('post'=>$_smarty_tpl->tpl_vars['post']->value,'type'=>$_smarty_tpl->tpl_vars['discussion']->value['type']), 0);?>

                    </div>
                <?php } ?>
            </div>

            <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"pagination_discussion",'search'=>$_smarty_tpl->tpl_vars['discussion']->value['search']), 0);?>


        <?php } else { ?>
            <p class="no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p>
        <?php }?>

    </div>
<?php } else { ?>

<?php }?>

<?php }} ?>
