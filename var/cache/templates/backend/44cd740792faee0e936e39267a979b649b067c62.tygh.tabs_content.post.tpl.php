<?php /* Smarty version Smarty-3.1.21, created on 2022-03-02 15:28:18
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/sd_facebook_pixel/hooks/companies/tabs_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1737633125621f54d203eed4-54532469%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '44cd740792faee0e936e39267a979b649b067c62' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/sd_facebook_pixel/hooks/companies/tabs_content.post.tpl',
      1 => 1615794878,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1737633125621f54d203eed4-54532469',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'navigation' => 0,
    'company_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621f54d2045536_11530574',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621f54d2045536_11530574')) {function content_621f54d2045536_11530574($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('identifier_facebook_pixel'));
?>
<?php if ($_smarty_tpl->tpl_vars['navigation']->value['tabs']['sd_facebook_pixel']) {?>
    <div id="content_sd_facebook_pixel" class="hidden">
        <div class="control-group">
            <label class="control-label" for="elm_identifier_facebook_pixel"><?php echo $_smarty_tpl->__("identifier_facebook_pixel");?>
:</label>
            <div class="controls">
                <input type="text" name="company_data[identifier_facebook_pixel]" id="elm_identifier_facebook_pixel" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['identifier_facebook_pixel'], ENT_QUOTES, 'UTF-8');?>
"/>
            </div>
        </div>
    </div>
<?php }?>
<?php }} ?>
