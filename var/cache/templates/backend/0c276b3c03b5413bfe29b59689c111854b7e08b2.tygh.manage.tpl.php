<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:20:34
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_prescription/views/insurance_list/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5732369162149cd2763314-02564703%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0c276b3c03b5413bfe29b59689c111854b7e08b2' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_prescription/views/insurance_list/manage.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '5732369162149cd2763314-02564703',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'insurance_list' => 0,
    'var' => 0,
    'key' => 0,
    'runtime' => 0,
    'c_url' => 0,
    'search' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149cd27c5973_06624673',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149cd27c5973_06624673')) {function content_62149cd27c5973_06624673($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('insurance_card_type_in_en','insurance_card_type_in_ar','insurance_card_type_in_en','insurance_card_type_in_ar','delete','restore_default','no_data','insurance_card_type_in_en','insurance_card_type_in_ar','language_variable','value','tools','new_insurance_card','add_insurance_card','insurance_list'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

<div id="content_insurance_list">

<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="insurance_list_form">
<input type="hidden" name="q" value="<?php echo htmlspecialchars($_REQUEST['q'], ENT_QUOTES, 'UTF-8');?>
">
<input type="hidden" name="selected_section" value="<?php echo htmlspecialchars($_REQUEST['selected_section'], ENT_QUOTES, 'UTF-8');?>
">

<?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('save_current_page'=>true,'save_current_url'=>true), 0);?>

<?php $_smarty_tpl->tpl_vars['c_url'] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['insurance_list']->value) {?>
<div class="table-responsive-wrapper">
    <table class="table table--relative table-responsive" width="100%">
    <thead>
        <tr>
            <th width="1%"><?php echo $_smarty_tpl->getSubTemplate ("common/check_items.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</th>
            <th width="60%"><?php echo $_smarty_tpl->__("insurance_card_type_in_en");?>
</th>
            <th width="33%"><?php echo $_smarty_tpl->__("insurance_card_type_in_ar");?>
</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
    <?php  $_smarty_tpl->tpl_vars["var"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["var"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['insurance_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["var"]->key => $_smarty_tpl->tpl_vars["var"]->value) {
$_smarty_tpl->tpl_vars["var"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["var"]->key;
?>
    <tr>
        <td data-th="">
            <input type="checkbox" name="list[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['card_id'], ENT_QUOTES, 'UTF-8');?>
" class="checkbox cm-item">
        </td>
        <td data-th="<?php echo $_smarty_tpl->__("insurance_card_type_in_en");?>
">
            <input type="hidden" name="insurance_company[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
][card_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['card_id'], ENT_QUOTES, 'UTF-8');?>
" />
            <input type="text" name="insurance_company[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
][name_en]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['name_en'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
        </td>
        <td data-th="<?php echo $_smarty_tpl->__("insurance_card_type_in_ar");?>
">
            <input type="text" name="insurance_company[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
][name_ar]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['name_ar'], ENT_QUOTES, 'UTF-8');?>
" class="input-large" />
        </td>
        <td>
            <?php if (fn_allowed_for("ULTIMATE")&&!$_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/update_for_all.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('display'=>true,'object_id'=>$_smarty_tpl->tpl_vars['key']->value,'name'=>"insurance_company[".((string)$_smarty_tpl->tpl_vars['key']->value)."][overwrite]"), 0);?>

            <?php }?>
            <?php $_smarty_tpl->_capture_stack[0][] = array("tools_items", null, null); ob_start(); ?>
            <a class="cm-confirm cm-post" href="<?php echo htmlspecialchars(fn_url("insurance_list.delete_item?card_id=".((string)$_smarty_tpl->tpl_vars['var']->value['card_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['c_url']->value)), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo $_smarty_tpl->__("delete");?>
">
                <?php if (fn_allowed_for("ULTIMATE")) {?>
                    <?php if ($_smarty_tpl->tpl_vars['runtime']->value['company_id']) {?>
                        <?php echo $_smarty_tpl->__("restore_default");?>

                    <?php }?>
                <?php } else { ?>
                    <i class="icon-trash"></i>
                <?php }?>
            </a>
            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
            <div class="hidden-tools">
                <?php echo $_smarty_tpl->getSubTemplate ("common/table_tools_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('prefix'=>$_smarty_tpl->tpl_vars['var']->value['name'],'tools_list'=>Smarty::$_smarty_vars['capture']['tools_items']), 0);?>

            </div>
        </td>
    </tr>
    <?php } ?>
    </tbody>
    </table>
</div>
<?php } else { ?>
    <p class="no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p>
<?php }?>
<?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</form>

<?php if ($_smarty_tpl->tpl_vars['insurance_list']->value) {?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("delete_button", null, null); ob_start(); ?>
        <?php echo htmlspecialchars(Smarty::$_smarty_vars['capture']['delete_button'], ENT_QUOTES, 'UTF-8');?>

        <li class="cm-tab-tools" id="tools_insurance_list_delete_buttons">
            <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"delete_selected",'dispatch'=>"dispatch[insurance_list.m_delete_variables]",'form'=>"insurance_list_form"));?>

        </li>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

    <?php $_smarty_tpl->_capture_stack[0][] = array("add_button", null, null); ob_start(); ?>
        <?php echo htmlspecialchars(Smarty::$_smarty_vars['capture']['add_button'], ENT_QUOTES, 'UTF-8');?>

        <span class="cm-tab-tools btn-group" id="tools_insurance_list_save_button">
            <?php echo $_smarty_tpl->getSubTemplate ("buttons/save.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[insurance_list.manage]",'but_role'=>"action",'but_target_form'=>"insurance_list_form",'but_meta'=>"cm-submit"), 0);?>

        </span>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?>


<?php $_smarty_tpl->_capture_stack[0][] = array("add_insurance_company", null, null); ob_start(); ?>

<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="lang_add_var">
<input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['config']->value['current_url']), ENT_QUOTES, 'UTF-8');?>
" />

<div class="table-responsive-wrapper">
    <table class="table table--relative table-responsive">
    <thead>
        <tr class="cm-first-sibling">
            
            <th width="60%"><?php echo $_smarty_tpl->__("insurance_card_type_in_en");?>
</th>
            <th width="33%"><?php echo $_smarty_tpl->__("insurance_card_type_in_ar");?>
</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        <tr id="box_new_insurance_company" valign="top">
            <td data-th="<?php echo $_smarty_tpl->__("language_variable");?>
">
                <input type="text"
                       size="30"
                       name="new_company[0][name_en]"
                       value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['name_en'], ENT_QUOTES, 'UTF-8');?>
"
                />
            </td>
            <td data-th="<?php echo $_smarty_tpl->__("value");?>
">
                <input type="text"
                       size="30"
                       name="new_company[0][name_ar]"
                       value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['search']->value['name_ar'], ENT_QUOTES, 'UTF-8');?>
"
                />
            </td>
            <td data-th="<?php echo $_smarty_tpl->__("tools");?>
"><?php echo $_smarty_tpl->getSubTemplate ("buttons/multiple_buttons.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('item_id'=>"new_insurance_company"), 0);?>
</td>
        </tr>
    </tbody>
    </table>
</div>

<div class="buttons-container">
    <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[insurance_list.manage]",'cancel_action'=>"close"), 0);?>

</div>

</form>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

</div>

<?php echo Smarty::$_smarty_vars['capture']['popups'];?>


<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("sidebar", null, null); ob_start(); ?>
    
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/popupbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('id'=>"add_insurance_company",'text'=>$_smarty_tpl->__("new_insurance_card"),'title'=>$_smarty_tpl->__("add_insurance_card"),'content'=>Smarty::$_smarty_vars['capture']['add_insurance_company'],'act'=>"general",'icon'=>"icon-plus"), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
        <?php echo Smarty::$_smarty_vars['capture']['delete_button'];?>

    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>


    <?php echo Smarty::$_smarty_vars['capture']['add_button'];?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("insurance_list"),'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar'],'select_languages'=>false), 0);?>

<?php }} ?>
