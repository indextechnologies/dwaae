<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:20:36
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:165571526362149cd4a2c2c1-74978427%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'acdec6c89fa236348569c7aa94f52b1bb0d78675' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/manage.tpl',
      1 => 1602071803,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '165571526362149cd4a2c2c1-74978427',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'search' => 0,
    'ajax_class' => 0,
    'c_url' => 0,
    'sort_sign' => 0,
    'quotations' => 0,
    'q' => 0,
    'settings' => 0,
    'current_redirect_url' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149cd4a9c912_82342373',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149cd4a9c912_82342373')) {function content_62149cd4a9c912_82342373($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('h_rfq.rfq_id','h_rfq.enquiry_no','h_rfq.email','h_rfq.create_time','h_rfq.valid_till','h_rfq.status','h_rfq.view_details','delete','no_data','h_rfq.add_new_quote','h_rfq.quotations'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="quotation_form" id="quotation_form">
<input type="hidden" name="fake" value="1" />
    <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
    <?php if ($_smarty_tpl->tpl_vars['search']->value['sort_order']=="asc") {?>
    <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"icon-down-dir\"></i>", null, 0);?>
    <?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["sort_sign"] = new Smarty_variable("<i class=\"icon-up-dir\"></i>", null, 0);?>
    <?php }?>
    <?php if (!$_smarty_tpl->tpl_vars['config']->value['tweaks']['disable_dhtml']) {?>
        <?php $_smarty_tpl->tpl_vars["ajax_class"] = new Smarty_variable("cm-ajax", null, 0);?>

    <?php }?>
    <?php if (@constant('ACCOUNT_TYPE')!="admin") {?>
        <?php $_smarty_tpl->tpl_vars['not_main_admin'] = new Smarty_variable(true, null, 0);?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars['not_main_admin'] = new Smarty_variable(false, null, 0);?>
    <?php }?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    <table class="table orders-search">
        <thead>
            <tr>
                <th width="1%" class="left mobile-hide">
            <?php echo $_smarty_tpl->getSubTemplate ("common/check_items.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=rfq_id&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("h_rfq.rfq_id");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="rfq_id") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=enquiry_no&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("h_rfq.enquiry_no");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="enquiry_no") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=email&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("h_rfq.email");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="email") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=create_time&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("h_rfq.create_time");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="create_time") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=valid_till&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("h_rfq.valid_till");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="valid_till") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
                <th></th>
                <th><a class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['ajax_class']->value, ENT_QUOTES, 'UTF-8');?>
" href="<?php echo htmlspecialchars(fn_url(((string)$_smarty_tpl->tpl_vars['c_url']->value)."&sort_by=status&sort_order=".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])), ENT_QUOTES, 'UTF-8');?>
" data-ca-target-id="pagination_contents"><?php echo $_smarty_tpl->__("h_rfq.status");?>
</a><?php if ($_smarty_tpl->tpl_vars['search']->value['sort_by']=="status") {
echo $_smarty_tpl->tpl_vars['sort_sign']->value;
}?></th>
            </tr>
        </thead>
        <?php  $_smarty_tpl->tpl_vars["q"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["q"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['quotations']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["q"]->key => $_smarty_tpl->tpl_vars["q"]->value) {
$_smarty_tpl->tpl_vars["q"]->_loop = true;
?>
            <tr>
                <td class="left mobile-hide">
                    <input type="checkbox" name="rfq_ids[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
" class="cm-item" />
                </td>
                <td class="orders-search__item"><a href="<?php echo htmlspecialchars(fn_url("h_rfq.update?rfq_id=".((string)$_smarty_tpl->tpl_vars['q']->value['rfq_id'])), ENT_QUOTES, 'UTF-8');?>
"><strong>#<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['rfq_id'], ENT_QUOTES, 'UTF-8');?>
</strong></a></td>
                <td><?php if ($_smarty_tpl->tpl_vars['q']->value['enquiry_no']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['enquiry_no'], ENT_QUOTES, 'UTF-8');
} else { ?>--<?php }?></td>
                <td class="orders-search__item"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['q']->value['email'], ENT_QUOTES, 'UTF-8');?>
</td>
                <td class="orders-search__item">
                    <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['q']->value['create_time'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'UTF-8');?>

                </td>
                <td class="orders-search__item"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['q']->value['valid_till'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])." ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</td>
                <td>
                    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_items", null, null); ob_start(); ?>
                        <li><?php ob_start();?><?php echo $_smarty_tpl->__("h_rfq.view_details");?>
<?php $_tmp1=ob_get_clean();?><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'href'=>"h_rfq.update?rfq_id=".((string)$_smarty_tpl->tpl_vars['q']->value['rfq_id']),'text'=>$_tmp1));?>
</li>
                        <?php $_smarty_tpl->tpl_vars["current_redirect_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
                        <li><?php ob_start();?><?php echo $_smarty_tpl->__("delete");?>
<?php $_tmp2=ob_get_clean();?><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'href'=>"h_rfq.delete?rfq_id=".((string)$_smarty_tpl->tpl_vars['q']->value['rfq_id'])."&redirect_url=".((string)$_smarty_tpl->tpl_vars['current_redirect_url']->value),'class'=>"cm-confirm",'text'=>$_tmp2,'method'=>"POST"));?>
</li>
                    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                    <div class="hidden-tools">
                        <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_items']));?>

                    </div>
                </td>
                <td class="orders-search__item">
                <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/common/status.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('status'=>$_smarty_tpl->tpl_vars['q']->value['status'],'display'=>"view",'name'=>"update_order[status]"), 0);?>

                </td>
            </tr>
        <?php }
if (!$_smarty_tpl->tpl_vars["q"]->_loop) {
?>
            <tr class="table__no-items">
                <td colspan="7"><p class="no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p></td>
            </tr>
        <?php } ?>
    </table>
    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

</form>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_items", null, null); ob_start(); ?>
        <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"delete_selected",'dispatch'=>"dispatch[h_rfq.m_delete]",'form'=>"quotation_form"));?>
</li>
        <li><a href="<?php echo htmlspecialchars(fn_url("h_rfq.reset_auto_increment"), ENT_QUOTES, 'UTF-8');?>
">Reset Auto Increment Number</a></li>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_items'],'class'=>"mobile-hide"));?>


<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("sidebar", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("addons/h_rfq/views/h_rfq/components/search.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dispatch'=>"h_rfq.manage"), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tool_href'=>"h_rfq.add",'prefix'=>"top",'title'=>$_smarty_tpl->__("h_rfq.add_new_quote"),'hide_tools'=>true,'icon'=>"icon-plus"), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("h_rfq.quotations");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['mainbox'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'title'=>Smarty::$_smarty_vars['capture']['mainbox_title'],'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons']), 0);?>


<?php }} ?>
