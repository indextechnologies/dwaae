<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 13:24:39
         compiled from "/home/dwaae/public_html/design/backend/mail/templates/common/wrap_document.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5917769686214abd75a54b8-37849785%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fe421dc4152af7374f6cf1f76189c9a5562d4cfb' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/mail/templates/common/wrap_document.tpl',
      1 => 1600492178,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '5917769686214abd75a54b8-37849785',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'language_direction' => 0,
    'content' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214abd75c6394_38568261',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214abd75c6394_38568261')) {function content_6214abd75c6394_38568261($_smarty_tpl) {?><!DOCTYPE html>
<html dir="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language_direction']->value, ENT_QUOTES, 'UTF-8');?>
">
<head>

<style type="text/css" media="screen,print">
body {
    padding: 0;
    margin: 0;
    text-align: center;
}
a, a:link, a:visited, a:hover, a:active {
    color: #000000;
    text-decoration: underline;
}
a:hover {
    text-decoration: none;
}

#print-wrapp {
	max-width: 800px;
	width: 100%;
	margin: 0px auto;
	text-align: initial;
}

</style>

</head>

<body>
<?php echo $_smarty_tpl->getSubTemplate ("common/scripts.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<table id="print-wrapp">
	<tr>
		<td>
			<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

		</td>
	</tr>
</table>
</body>
</html><?php }} ?>
