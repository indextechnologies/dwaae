<?php /* Smarty version Smarty-3.1.21, created on 2022-03-01 18:22:56
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/smart_app_pages/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:908338599621e2c401727e3-28974070%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3713b6a4e6314b02e96204db104cab7dbe97194a' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/smart_app_pages/manage.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '908338599621e2c401727e3-28974070',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'search' => 0,
    'pages_data' => 0,
    'pages' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621e2c401eda72_90798032',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621e2c401eda72_90798032')) {function content_621e2c401eda72_90798032($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('title','ec_smart_app.selected_page','edit','delete','no_items','add_pages','smart_app_pages'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="manage_smart_app_pages_form" id="elm_smart_app_pages">
        <?php $_smarty_tpl->tpl_vars["return_current_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_icon"] = new Smarty_variable("<i class=\"exicon-".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])."\"></i>", null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_dummy"] = new Smarty_variable("<i class=\"exicon-dummy\"></i>", null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['pages_data']->value) {?>
            <table class="table table-middle sortable">
                <thead>
                <tr>
                    <th width="5%" class="center"><?php echo $_smarty_tpl->getSubTemplate ("common/check_items.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</th>
                    <th width="40%"><?php echo $_smarty_tpl->__("title");?>
</th>    
                    <th width="40%"><?php echo $_smarty_tpl->__("ec_smart_app.selected_page");?>
</th>    
                    <th width="15%"></th>
                </tr>
                </thead>
                <tbody>
                <?php  $_smarty_tpl->tpl_vars['pages'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pages']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pages_data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pages']->key => $_smarty_tpl->tpl_vars['pages']->value) {
$_smarty_tpl->tpl_vars['pages']->_loop = true;
?>
                    <tr class="cm-row-status cm-row-status-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['pages']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                        <td class="center">
                            <input type="checkbox" name="pages_ids[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pages']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="checkbox cm-item" />
                        </td>
                        <td class="row-status"><a class="row-status" href="<?php echo htmlspecialchars(fn_url("smart_app_pages.update?id=".((string)$_smarty_tpl->tpl_vars['pages']->value['id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['pages']->value['title'],40);?>
</a></td>
                        <td class="row-status"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pages']->value['selected_page_name'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td  class="nowrap">
                            <div class="hidden-tools">
                                <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
                                    <li>
                                        <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("edit"),'href'=>"smart_app_pages.update?id=".((string)$_smarty_tpl->tpl_vars['pages']->value['id'])));?>

                                    </li>
                                    <li>
                                        <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("delete"),'class'=>"cm-confirm cm-post",'href'=>"smart_app_pages.delete?id=".((string)$_smarty_tpl->tpl_vars['pages']->value['id'])));?>

                                    </li>
                                <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                                <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            </table>
        <?php } else { ?>
            <p class="no-items"><?php echo $_smarty_tpl->__("no_items");?>
</p>
        <?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tool_href'=>"smart_app_pages.update",'prefix'=>"top",'title'=>$_smarty_tpl->__("add_pages"),'hide_tools'=>true,'icon'=>"icon-plus"), 0);?>

    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
        <li>
            <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"delete_selected",'dispatch'=>"dispatch[smart_app_pages.m_delete]",'form'=>"manage_smart_app_pages_form"));?>

        </li>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("smart_app_pages"),'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'title_extra'=>Smarty::$_smarty_vars['capture']['title_extra'],'tools'=>Smarty::$_smarty_vars['capture']['tools'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'select_languages'=>true), 0);?>
<?php }} ?>
