<?php /* Smarty version Smarty-3.1.21, created on 2022-03-01 18:18:57
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/ec_order_map/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:317888532621e2b51ceb8b3-58479297%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3cc92213761f59f449748ad2dd8d40594c7111d2' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/ec_order_map/manage.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '317888532621e2b51ceb8b3-58479297',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'is_form_readonly' => 0,
    'order_mapped' => 0,
    '_key' => 0,
    '_item' => 0,
    'order_statuses' => 0,
    'key' => 0,
    'item' => 0,
    'new_key' => 0,
    'order_restrict' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621e2b51d6dc62_30429438',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621e2b51d6dc62_30429438')) {function content_621e2b51d6dc62_30429438($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/dwaae/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_function_cycle')) include '/home/dwaae/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.cycle.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_smart_app.caffiene_status','ec_smart_app.order_status','ec_caffiene_status','status','ec_caffiene_status','status','ec_order_restricted','tt_addons_ec_smart_app_views_ec_order_map_manage_ec_order_restricted','ec_smart_app.orders_status_tracking'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("tabsbox", null, null); ob_start(); ?>
        <form id="form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="order_map_form" class="form-horizontal form-edit  cm-disable-empty-files <?php if ($_smarty_tpl->tpl_vars['is_form_readonly']->value) {?>cm-hide-inputs<?php }?>" enctype="multipart/form-data"> 
            <div id="content_order_map" class="hidden"> 
                <div class="table-responsive-wrapper">
                    <table class="table table-middle table--relative table-responsive" width="100%">
                    <thead class="cm-first-sibling">
                    <tr>
                        <th width="5%"><?php echo $_smarty_tpl->__("ec_smart_app.caffiene_status");?>
</th>
                        <th width="20%"><?php echo $_smarty_tpl->__("ec_smart_app.order_status");?>
</th>
                        <th width="15%">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php  $_smarty_tpl->tpl_vars["_item"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["_item"]->_loop = false;
 $_smarty_tpl->tpl_vars["_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_mapped']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["_item"]->key => $_smarty_tpl->tpl_vars["_item"]->value) {
$_smarty_tpl->tpl_vars["_item"]->_loop = true;
 $_smarty_tpl->tpl_vars["_key"]->value = $_smarty_tpl->tpl_vars["_item"]->key;
?>
                    <tr class="cm-row-item">
                        <td width="5%" class="" data-th="<?php echo $_smarty_tpl->__("ec_caffiene_status");?>
">
                            <input type="text" name="order_map[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][caffeine_order]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_item']->value['caffeine_order'], ENT_QUOTES, 'UTF-8');?>
" class="" />
                        </td>
                        <td width="20%" class="" data-th="<?php echo $_smarty_tpl->__("status");?>
">
                            <select name="order_map[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][order_status]">
                                <?php  $_smarty_tpl->tpl_vars["item"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["item"]->key => $_smarty_tpl->tpl_vars["item"]->value) {
$_smarty_tpl->tpl_vars["item"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["item"]->key;
?>
                                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['key']->value==$_smarty_tpl->tpl_vars['_item']->value['order_status']) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                                <?php } ?>
                            </select>
                        </td>
                        <td width="15%" class="nowrap  right">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/clone_delete.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dummy_href'=>true,'microformats'=>"cm-delete-row",'no_confirm'=>true), 0);?>

                        </td>
                    </tr>
                    <?php } ?>
                    <?php echo smarty_function_math(array('equation'=>"x+1",'x'=>(($tmp = @$_smarty_tpl->tpl_vars['_key']->value)===null||$tmp==='' ? 0 : $tmp),'assign'=>"new_key"),$_smarty_tpl);?>

                    <tr class="<?php echo smarty_function_cycle(array('values'=>"table-row , ",'reset'=>1),$_smarty_tpl);?>
" id="box_map_order_status">
                        <td width="5%" data-th="<?php echo $_smarty_tpl->__("ec_caffiene_status");?>
">
                            <input type="text" name="order_map[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][caffeine_order]" value="" class="" />
                        </td>
                        <td width="20%" data-th="<?php echo $_smarty_tpl->__("status");?>
">
                            <select name="order_map[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][order_status]">
                                <?php  $_smarty_tpl->tpl_vars["item"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["item"]->key => $_smarty_tpl->tpl_vars["item"]->value) {
$_smarty_tpl->tpl_vars["item"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["item"]->key;
?>
                                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                                <?php } ?>
                            </select>
                        </td>
                        <td width="15%" class="right">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/multiple_buttons.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('item_id'=>"map_order_status"), 0);?>

                        </td>
                    </tr>
                    </tbody>
                    </table>
                </div>
            </div>
            <div id="content_order_restrict" class="hidden"> 
                <div class="control-group">
                    <label class="control-label" for="elm_ec_order_restricted"><?php echo $_smarty_tpl->__("ec_order_restricted");
echo $_smarty_tpl->getSubTemplate ("common/tooltip.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tooltip'=>$_smarty_tpl->__("tt_addons_ec_smart_app_views_ec_order_map_manage_ec_order_restricted")), 0);?>
:</label>
                    <div class="controls">
                        <select name="order_restrict[]" id="elm_ec_order_restricted" class="cm-object-selector" multiple>
                            <?php  $_smarty_tpl->tpl_vars["item"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item"]->_loop = false;
 $_smarty_tpl->tpl_vars["key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['order_statuses']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["item"]->key => $_smarty_tpl->tpl_vars["item"]->value) {
$_smarty_tpl->tpl_vars["item"]->_loop = true;
 $_smarty_tpl->tpl_vars["key"]->value = $_smarty_tpl->tpl_vars["item"]->key;
?>
                                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['key']->value, ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['order_restrict']->value&&in_array($_smarty_tpl->tpl_vars['key']->value,$_smarty_tpl->tpl_vars['order_restrict']->value)) {?>selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value, ENT_QUOTES, 'UTF-8');?>
</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <?php $_smarty_tpl->_capture_stack[0][] = array("buttons", null, null); ob_start(); ?>                
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/save_cancel.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_meta'=>"cm-product-save-buttons",'but_role'=>"submit-link",'but_name'=>"dispatch[ec_order_map.manage]",'but_target_form'=>"order_map_form",'save'=>1), 0);?>

            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
        </form>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/tabsbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('content'=>Smarty::$_smarty_vars['capture']['tabsbox'],'group_name'=>"domains",'active_tab'=>$_REQUEST['selected_section'],'track'=>true), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("ec_smart_app.orders_status_tracking"),'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons']), 0);?>
<?php }} ?>
