<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:46:10
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/hooks/products/tabs_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14843714856214a2d24e9d91-84362530%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ffa2531d13cdd20ca76f52f59ed670a7b105a713' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/hooks/products/tabs_content.post.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '14843714856214a2d24e9d91-84362530',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'product_data' => 0,
    'slot' => 0,
    'no_hide_input_if_shared_product' => 0,
    '_key' => 0,
    'new_key' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214a2d25122d9_15779980',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214a2d25122d9_15779980')) {function content_6214a2d25122d9_15779980($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/dwaae/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_function_cycle')) include '/home/dwaae/public_html/app/lib/vendor/smarty/smarty/libs/plugins/function.cycle.php';
if (!is_callable('smarty_block_inline_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('ec_smart_app.product_type_settings','ec_smart_app.product_type','ec_smart_app.normal','ec_smart_app.booking','ec_smart_app.booking_availability','ec_smart_app.booking_after','ec_smart_app.time_slots','position','from','to','capacity','position','from','to','capacity','position','from','to','capacity','ec_smart_app.payment_method_availability','ec_smart_app.credit_card_only'));
?>
<div class="hidden" id="content_time_settings">
    <h4 class="subheader"><?php echo $_smarty_tpl->__("ec_smart_app.product_type_settings");?>
</h4>
    <div class="control-group">
        <label class="control-label" for="ec_product_type"><?php echo $_smarty_tpl->__("ec_smart_app.product_type");?>
:</label>
        <div class="controls">
            <select name="product_data[ec_product_type]" id="ec_product_type">
                <option value="N" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['ec_product_type']=='N') {?>selected<?php }?>><?php echo $_smarty_tpl->__("ec_smart_app.normal");?>
</option> 
                <option value="K" <?php if ($_smarty_tpl->tpl_vars['product_data']->value['ec_product_type']=='K') {?>selected<?php }?>><?php echo $_smarty_tpl->__("ec_smart_app.booking");?>
</option> 
            </select>
        </div>
    </div>
    <div class="<?php if ($_smarty_tpl->tpl_vars['product_data']->value['ec_product_type']!='K') {?>hidden<?php }?>" id="booking_settings">
        <h4 class="subheader"><?php echo $_smarty_tpl->__("ec_smart_app.booking_availability");?>
</h4>
        <div class="control-group">
            <label class="control-label" for="booking_after"><?php echo $_smarty_tpl->__("ec_smart_app.booking_after");?>
:</label>
            <div class="controls">
                <input type="text" name="product_data[booking_after]" id="booking_after" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_data']->value['booking_after'], ENT_QUOTES, 'UTF-8');?>
" class="input-small cm-value-integer" />
            </div>
        </div>

        <h4 class="subheader"><?php echo $_smarty_tpl->__("ec_smart_app.time_slots");?>
</h4>
        <div class="table-responsive-wrapper">
            <table class="table table-middle table--relative table-responsive" width="100%">
                <thead class="cm-first-sibling">
                <tr>
                    <th width="10%"><?php echo $_smarty_tpl->__("position");?>
</th>
                    <th width="25%"><?php echo $_smarty_tpl->__("from");?>
</th>
                    <th width="25%"><?php echo $_smarty_tpl->__("to");?>
</th>
                    <th width="25%"><?php echo $_smarty_tpl->__("capacity");?>
</th>
                    <th width="15%">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    
                    <?php  $_smarty_tpl->tpl_vars["slot"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["slot"]->_loop = false;
 $_smarty_tpl->tpl_vars["_key"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product_data']->value['time_slots']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["slot"]->key => $_smarty_tpl->tpl_vars["slot"]->value) {
$_smarty_tpl->tpl_vars["slot"]->_loop = true;
 $_smarty_tpl->tpl_vars["_key"]->value = $_smarty_tpl->tpl_vars["slot"]->key;
?>
                    <?php if ($_smarty_tpl->tpl_vars['slot']->value['from']) {?>
                        <tr class="cm-row-item">
                            <td width="5%" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("position");?>
">
                                <input type="text" name="product_data[time_slots][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][position]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slot']->value['position'], ENT_QUOTES, 'UTF-8');?>
" class="input-micro" />
                            </td>
                            <td width="25%" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("from");?>
">
                                <input type="text" name="product_data[time_slots][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][from]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slot']->value['from'], ENT_QUOTES, 'UTF-8');?>
" class="input-medium" />
                            </td>
                            <td width="25%" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("to");?>
">
                                <input type="text" name="product_data[time_slots][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][to]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slot']->value['to'], ENT_QUOTES, 'UTF-8');?>
" class="input-medium" />
                            </td>
                            <td width="25%" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("capacity");?>
">
                                <input type="text" name="product_data[time_slots][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['_key']->value, ENT_QUOTES, 'UTF-8');?>
][capacity]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slot']->value['capacity'], ENT_QUOTES, 'UTF-8');?>
" class="input-medium cm-value-integer" />
                            </td>
                            <td width="15%" class="nowrap <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
 right">
                                <?php echo $_smarty_tpl->getSubTemplate ("buttons/clone_delete.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('dummy_href'=>true,'microformats'=>"cm-delete-row",'no_confirm'=>true), 0);?>

                            </td>
                        </tr>
                    <?php }?>
                    <?php } ?>
                    <?php echo smarty_function_math(array('equation'=>"x+1",'x'=>(($tmp = @$_smarty_tpl->tpl_vars['_key']->value)===null||$tmp==='' ? 0 : $tmp),'assign'=>"new_key"),$_smarty_tpl);?>

                    <tr class="<?php echo smarty_function_cycle(array('values'=>"table-row , ",'reset'=>1),$_smarty_tpl);
echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" id="box_add_time_slots">
                        <td width="5%" data-th="<?php echo $_smarty_tpl->__("position");?>
">
                            <input type="text" name="product_data[time_slots][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][position]" value="" class="input-micro" /></td>
                        <td width="25%" data-th="<?php echo $_smarty_tpl->__("from");?>
">
                            <input type="text" name="product_data[time_slots][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][from]" size="10" class="input-medium" /></td>
                        <td width="25%" data-th="<?php echo $_smarty_tpl->__("to");?>
">
                            <input type="text" name="product_data[time_slots][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][to]" size="10" class="input-medium" /></td>
                            <td width="25%" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['no_hide_input_if_shared_product']->value, ENT_QUOTES, 'UTF-8');?>
" data-th="<?php echo $_smarty_tpl->__("capacity");?>
">
                                <input type="text" name="product_data[time_slots][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['new_key']->value, ENT_QUOTES, 'UTF-8');?>
][capacity]" size="10" class="input-medium cm-value-integer" />
                            </td>
                        <td width="15%" class="right">
                            <?php echo $_smarty_tpl->getSubTemplate ("buttons/multiple_buttons.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('item_id'=>"add_time_slots"), 0);?>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h4 class="subheader"><?php echo $_smarty_tpl->__("ec_smart_app.payment_method_availability");?>
</h4>
        <div class="control-group">
            <label class="control-label" for="ec_credit_card_only"><?php echo $_smarty_tpl->__("ec_smart_app.credit_card_only");?>
:</label>
            <div class="controls">
                <input type="hidden" name="product_data[ec_credit_card_only]" value="N"/>
                <input type="checkbox" name="product_data[ec_credit_card_only]" id="ec_credit_card_only" value="Y"  <?php if ($_smarty_tpl->tpl_vars['product_data']->value['ec_credit_card_only']=='Y') {?>checked<?php }?>/>
            </div>
        </div>
    </div>
    <?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
>
    $(document).ready(function(){
        $('#ec_product_type').on('change',function(e){
            if ($(this).val() == 'K') {
                $('#booking_settings').show();
            }else{
                $('#booking_settings').hide();
            }
        });
    });
    <?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

</div><?php }} ?>
