<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:40:32
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/hooks/orders/shipping_info.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2211472286214a1800dd458-02123508%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '046a298db86e75cd589a1f3c23d9e76eebbae0f6' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/hooks/orders/shipping_info.pre.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '2211472286214a1800dd458-02123508',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'order_info' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214a1800e2f84_95398519',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214a1800e2f84_95398519')) {function content_6214a1800e2f84_95398519($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('selected_slot_for_delivery'));
?>

<?php if ($_smarty_tpl->tpl_vars['order_info']->value['delivery_timestamp']) {?>
<div class="control-group">
    <div class="control-label"><?php echo $_smarty_tpl->__("selected_slot_for_delivery");?>
:</div>
    <div id="tygh_shipping_info" class="controls">
        <?php if ($_smarty_tpl->tpl_vars['order_info']->value['delivery_timestamp']) {
echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['order_info']->value['delivery_timestamp']), ENT_QUOTES, 'UTF-8');?>
,<?php }?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order_info']->value['slot'], ENT_QUOTES, 'UTF-8');?>

    </div>
</div>
<?php }?><?php }} ?>
