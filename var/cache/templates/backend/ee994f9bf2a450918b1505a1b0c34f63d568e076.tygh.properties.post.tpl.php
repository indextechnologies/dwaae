<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:47:52
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/hooks/product_options/properties.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4070107086214a3384d3772-05222770%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ee994f9bf2a450918b1505a1b0c34f63d568e076' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/hooks/product_options/properties.post.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '4070107086214a3384d3772-05222770',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'option_data' => 0,
    'id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6214a3384dfe08_84298209',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6214a3384dfe08_84298209')) {function content_6214a3384dfe08_84298209($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_enum')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.enum.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('is_quantity_selector','ec_text_modifier'));
?>
<div class="control-group <?php if ($_smarty_tpl->tpl_vars['option_data']->value['option_type']!=smarty_modifier_enum("ProductOptionTypes::INPUT")) {?>hidden<?php }?>" >
    <label class="control-label" for="elm_is_quantity_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("is_quantity_selector");?>
</label>
    <div class="controls">
    <label class="checkbox">
    <input type="hidden" name="option_data[is_quantity]" value="N" /><input type="checkbox" id="elm_is_quantity_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="option_data[is_quantity]" value="Y" <?php if ($_smarty_tpl->tpl_vars['option_data']->value['is_quantity']=="Y") {?>checked="checked"<?php }?>/>
    </label>
    </div>
</div>

<div class="control-group <?php if ($_smarty_tpl->tpl_vars['option_data']->value['option_type']!=smarty_modifier_enum("ProductOptionTypes::INPUT")) {?>hidden<?php }?>" >
    <label class="control-label" for="elm_text_modifier_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
"><?php echo $_smarty_tpl->__("ec_text_modifier");?>
</label>
    <div class="controls">
        <input type="text" id="elm_text_modifier_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'UTF-8');?>
" name="option_data[text_modifier]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['option_data']->value['text_modifier'], ENT_QUOTES, 'UTF-8');?>
" />
    </div>
</div><?php }} ?>
