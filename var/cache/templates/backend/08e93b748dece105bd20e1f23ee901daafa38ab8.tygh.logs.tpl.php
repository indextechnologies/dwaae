<?php /* Smarty version Smarty-3.1.21, created on 2022-03-10 12:52:32
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/logs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6444327906229bc50d689d5-57411406%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '08e93b748dece105bd20e1f23ee901daafa38ab8' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/h_rfq/views/h_rfq/components/logs.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '6444327906229bc50d689d5-57411406',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'logs_search' => 0,
    'logs' => 0,
    'log' => 0,
    'user_short_info' => 0,
    'settings' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_6229bc50d7a976_56112412',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6229bc50d7a976_56112412')) {function content_6229bc50d7a976_56112412($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.date_format.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('content','user','time','content','user','time','no_data'));
?>
<div id="content_logs" class="">

    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('search'=>$_smarty_tpl->tpl_vars['logs_search']->value), 0);?>


    <?php if ($_smarty_tpl->tpl_vars['logs']->value) {?>
    <div class="table-responsive-wrapper">
        <table class="table table--relative table-responsive">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->__("content");?>
</th>
                <th><?php echo $_smarty_tpl->__("user");?>
</th>
                <th><?php echo $_smarty_tpl->__("time");?>
</th>
            </tr>
        </thead>
        <tbody>
        <?php  $_smarty_tpl->tpl_vars["log"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["log"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['logs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["log"]->key => $_smarty_tpl->tpl_vars["log"]->value) {
$_smarty_tpl->tpl_vars["log"]->_loop = true;
?>
        <tr>
            <td width="70%" class="wrap" data-th="<?php echo $_smarty_tpl->__("content");?>
">
                <b style="text-transform: uppercase;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['log']->value['type'], ENT_QUOTES, 'UTF-8');?>
: &nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['log']->value['action'], ENT_QUOTES, 'UTF-8');?>
</b> <br>
                <div><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['log']->value['note'], ENT_QUOTES, 'UTF-8');?>
</div>
            </td>
            <td data-th="<?php echo $_smarty_tpl->__("user");?>
">
                <?php $_smarty_tpl->tpl_vars['user_short_info'] = new Smarty_variable(fn_get_user_short_info($_smarty_tpl->tpl_vars['log']->value['user_id']), null, 0);?>
                <?php if ($_smarty_tpl->tpl_vars['log']->value['user_id']) {?>
                    <a href="<?php echo htmlspecialchars(fn_url("profiles.update?user_id=".((string)$_smarty_tpl->tpl_vars['log']->value['user_id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_short_info']->value['lastname'], ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['user_short_info']->value['firstname']||$_smarty_tpl->tpl_vars['user_short_info']->value['lastname']) {?>&nbsp;<?php }
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_short_info']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
</a>
                <?php } else { ?>
                    &mdash;
                <?php }?>
            </td>
            <td data-th="<?php echo $_smarty_tpl->__("time");?>
">
                <span class="nowrap"><?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['log']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format']).", ".((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['time_format'])), ENT_QUOTES, 'UTF-8');?>
</span>
            </td>
        </tr>
        <?php } ?>
        </tbody>
        </table>
    </div>
    <?php } else { ?>
        <p class="no-items"><?php echo $_smarty_tpl->__("no_data");?>
</p>
    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('search'=>$_smarty_tpl->tpl_vars['logs_search']->value), 0);?>

</div><?php }} ?>
