<?php /* Smarty version Smarty-3.1.21, created on 2022-03-02 14:49:24
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/smart_app_banner/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:928475577621f4bb433ca64-55574108%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cf59198068997444fef721ecf6087bdb4b99faf3' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/smart_app_banner/manage.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '928475577621f4bb433ca64-55574108',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'search' => 0,
    'banners' => 0,
    'banner' => 0,
    'product_name' => 0,
    'category_name' => 0,
    'company_name' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621f4bb43cc5e2_88124887',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621f4bb43cc5e2_88124887')) {function content_621f4bb43cc5e2_88124887($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('image','name','position','ec_smart_app.location_type','ec_smart_app.location_id_name','product','category','vendor','edit','delete','no_items','add_banner','smart_app_banner'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
	<form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="manage_smart_app_banner_form" id="elm_smart_app_banner">
		<input type="hidden" name="fake" value="1" />
		<?php $_smarty_tpl->tpl_vars["return_current_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
		<?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
		<?php $_smarty_tpl->tpl_vars["c_icon"] = new Smarty_variable("<i class=\"exicon-".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])."\"></i>", null, 0);?>
		<?php $_smarty_tpl->tpl_vars["c_dummy"] = new Smarty_variable("<i class=\"exicon-dummy\"></i>", null, 0);?>
		<?php if ($_smarty_tpl->tpl_vars['banners']->value) {?>
		<table class="table table-middle sortable">
			<thead>
			<tr>
				<th width="1%" class="center"><?php echo $_smarty_tpl->getSubTemplate ("common/check_items.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</th>
				<th width="15%"><span><?php echo $_smarty_tpl->__("image");?>
</span></th>
				<th width="20%"><?php echo $_smarty_tpl->__("name");?>
</th>
				<th width="15%"><?php echo $_smarty_tpl->__("position");?>
</th>
				<th width="25%"><?php echo $_smarty_tpl->__("ec_smart_app.location_type");?>
</th>
				<th width="20%"><?php echo $_smarty_tpl->__("ec_smart_app.location_id_name");?>
</th>
				<th class="right"></th>
			</tr>
			</thead>
			<tbody>
			<?php  $_smarty_tpl->tpl_vars['banner'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['banner']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['banners']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['banner']->key => $_smarty_tpl->tpl_vars['banner']->value) {
$_smarty_tpl->tpl_vars['banner']->_loop = true;
?>
			<tr class="cm-row-status">
				<td class="center">
			        <input type="checkbox" name="banner_ids[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="checkbox cm-item" />
			    </td>
			    <td>
			        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image'=>(($tmp = @$_smarty_tpl->tpl_vars['banner']->value['main_pair']['icon'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['banner']->value['main_pair']['detailed'] : $tmp),'image_id'=>$_smarty_tpl->tpl_vars['banner']->value['main_pair']['image_id'],'image_width'=>50,'href'=>fn_url("smart_app_banner.update?id=".((string)$_smarty_tpl->tpl_vars['banner']->value['id']))), 0);?>

			    </td>
				<td class="row-status"><a class="row-status" href="<?php echo htmlspecialchars(fn_url("smart_app_banner.update?id=".((string)$_smarty_tpl->tpl_vars['banner']->value['id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['banner']->value['name'],40);?>
</a></td>
				<td class="row-status"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['banner']->value['position'], ENT_QUOTES, 'UTF-8');?>
</td>
				<td class="row-status"><?php if ($_smarty_tpl->tpl_vars['banner']->value['type']=='P') {
echo $_smarty_tpl->__("product");
} elseif ($_smarty_tpl->tpl_vars['banner']->value['type']=='C') {
echo $_smarty_tpl->__("category");
} elseif ($_smarty_tpl->tpl_vars['banner']->value['type']=='V') {
echo $_smarty_tpl->__("vendor");
}?></td>
				<td class="row-status">
				<?php if ($_smarty_tpl->tpl_vars['banner']->value['type']=='P') {?>
				<?php $_smarty_tpl->tpl_vars['product_name'] = new Smarty_variable(fn_get_product_name($_smarty_tpl->tpl_vars['banner']->value['location_id']), null, 0);?>
				<a href="<?php echo htmlspecialchars(fn_url("products.update?product_id=".((string)$_smarty_tpl->tpl_vars['banner']->value['location_id'])), ENT_QUOTES, 'UTF-8');?>
" class="underlined"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_name']->value, ENT_QUOTES, 'UTF-8');?>
</a>
				<?php } elseif ($_smarty_tpl->tpl_vars['banner']->value['type']=='C') {?>
				<?php $_smarty_tpl->tpl_vars['category_name'] = new Smarty_variable(fn_get_category_name($_smarty_tpl->tpl_vars['banner']->value['location_id']), null, 0);?>
				<a href="<?php echo htmlspecialchars(fn_url("categories.update?category_id=".((string)$_smarty_tpl->tpl_vars['banner']->value['location_id'])), ENT_QUOTES, 'UTF-8');?>
" class="underlined"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_name']->value, ENT_QUOTES, 'UTF-8');?>
</a>
				<?php } elseif ($_smarty_tpl->tpl_vars['banner']->value['type']=='V') {?>
				<?php $_smarty_tpl->tpl_vars['company_name'] = new Smarty_variable(fn_get_company_name($_smarty_tpl->tpl_vars['banner']->value['location_id']), null, 0);?>
				<a href="<?php echo htmlspecialchars(fn_url("companies.update&company_id=".((string)$_smarty_tpl->tpl_vars['banner']->value['location_id'])), ENT_QUOTES, 'UTF-8');?>
" class="underlined"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_name']->value, ENT_QUOTES, 'UTF-8');?>
</a>
				<?php }?>
				</td>
				<!-- update&category_id=167 -->
				<td class="nowrap right">
		            <?php $_smarty_tpl->_capture_stack[0][] = array("tools_items", null, null); ob_start(); ?>
						<li>
							<?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("edit"),'href'=>"smart_app_banner.update?id=".((string)$_smarty_tpl->tpl_vars['banner']->value['id'])));?>

						</li>
		                <li><?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'class'=>"cm-confirm",'href'=>"smart_app_banner.delete?banner_id=".((string)$_smarty_tpl->tpl_vars['banner']->value['id']),'text'=>$_smarty_tpl->__("delete")));?>
</li>
		            <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
		            <div class="hidden-tools">
		                <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_items']));?>

		            </div>
        		</td>  
			</tr>
		<?php } ?>
		</tbody>
		</table>
        <?php } else { ?>
            <p class="no-items"><?php echo $_smarty_tpl->__("no_items");?>
</p>
        <?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
	<?php echo $_smarty_tpl->getSubTemplate ("common/tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tool_href'=>"smart_app_banner.update",'prefix'=>"top",'title'=>$_smarty_tpl->__("add_banner"),'hide_tools'=>true,'icon'=>"icon-plus"), 0);?>

    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
        <li>
        	<?php smarty_template_function_btn($_smarty_tpl,array('type'=>"delete_selected",'dispatch'=>"dispatch[smart_app_banner.m_delete]",'form'=>"manage_smart_app_banner_form"));?>

        </li>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("smart_app_banner"),'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'title_extra'=>Smarty::$_smarty_vars['capture']['title_extra'],'tools'=>Smarty::$_smarty_vars['capture']['tools'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'select_languages'=>true), 0);?>
<?php }} ?>
