<?php /* Smarty version Smarty-3.1.21, created on 2022-02-22 12:19:18
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/index/scripts.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:87548434962149c86714bb3-72583330%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6b6a52d7219e268605667308aee62cf9aebb2637' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/d_custom/hooks/index/scripts.post.tpl',
      1 => 1604827789,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '87548434962149c86714bb3-72583330',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_62149c867196f8_26212503',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_62149c867196f8_26212503')) {function content_62149c867196f8_26212503($_smarty_tpl) {?><?php if (!is_callable('smarty_block_inline_script')) include '/home/dwaae/public_html/app/functions/smarty_plugins/block.inline_script.php';
?><?php $_smarty_tpl->smarty->_tag_stack[] = array('inline_script', array()); $_block_repeat=true; echo smarty_block_inline_script(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<?php echo '<script'; ?>
>
    
        $.beep = (function () {
            var ctxClass = window.audioContext || window.AudioContext || window.AudioContext || window.webkitAudioContext
            var ctx = new ctxClass();
            return function (duration, type, finishedCallback) {

                duration = +duration;

                // Only 0-4 are valid types.
                type = (type % 5) || 0;

                if (typeof finishedCallback != "function") {
                    finishedCallback = function () {};
                }

                var osc = ctx.createOscillator();

                osc.type = type;
                //osc.type = "sine";

                osc.connect(ctx.destination);
                if (osc.noteOn) osc.noteOn(0);
                if (osc.start) osc.start();

                setTimeout(function () {
                    if (osc.noteOff) osc.noteOff(0);
                    if (osc.stop) osc.stop();
                    finishedCallback();
                }, duration);

            };
        })();

        var eventHandler = setInterval(function() {
            try{
                $.ceAjax('request', fn_url('ec_events.check&user_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['auth']->value['user_id'], ENT_QUOTES, 'UTF-8');?>
'), {
    
                        callback: function(data) {
                            if(typeof(data.text) !== 'undefined'){
                                var d = JSON.parse(data.text);
                                if(d.upload_prescription){
                                    var uporders = localStorage.getItem("uporders");
                                    if(!uporders){
                                        uporders = {};
                                    }else{
                                        uporders = JSON.parse(uporders);
                                    }
                                    for (i in d.req_id) {
                                        var req_id = d.req_id[i];
                                        if(Object.values(uporders).indexOf(req_id) > -1){
                                            
                                        } else{
                                            $.beep(5000, 2, {});
                                            $.ceNotification('show', {
                                                type: 'I',
                                                title: 'New Prescription Order',
                                                message: 'You recived new prescription order, plz check!!!'
                                            });
                                            uporders[req_id] = req_id;
                                        }
                                    }
                                    try{
                                        localStorage.setItem("uporders", JSON.stringify(uporders));
                                    }catch(e){
                                        console.log('local storage is full');
                                    }
                                }
                            }
                        },
                        method: 'get',
                        hidden: true
                });
            }catch(e){

            }
          
        }, 60 * 1000);
    
<?php echo '</script'; ?>
><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_inline_script(array(), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
<?php }} ?>
