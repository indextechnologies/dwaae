<?php /* Smarty version Smarty-3.1.21, created on 2022-02-24 17:15:13
         compiled from "/home/dwaae/public_html/design/backend/templates/views/email_templates/components/import_summary.tpl" */ ?>
<?php /*%%SmartyHeaderCode:454826744621784e118fb70-08889309%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ce990d094dd25954011a7f331382ad6addc77edd' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/views/email_templates/components/import_summary.tpl',
      1 => 1600492179,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '454826744621784e118fb70-08889309',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'import_result' => 0,
    'error' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621784e11b9208_42365073',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621784e11b9208_42365073')) {function content_621784e11b9208_42365073($_smarty_tpl) {?><?php
\Tygh\Languages\Helper::preloadLangVars(array('email_template_import_success_msg','close'));
?>
<div class="import-summary">
    <?php if (!empty($_smarty_tpl->tpl_vars['import_result']->value['errors'])) {?>
        <h4 class="text-error"><?php echo $_smarty_tpl->__('import_errors');?>
</h4>
        <table width="100%" class="table table-no-hover table--relative">
            <?php  $_smarty_tpl->tpl_vars['error'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['error']->_loop = false;
 $_smarty_tpl->tpl_vars['code'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['import_result']->value['errors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['error']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['error']->key => $_smarty_tpl->tpl_vars['error']->value) {
$_smarty_tpl->tpl_vars['error']->_loop = true;
 $_smarty_tpl->tpl_vars['code']->value = $_smarty_tpl->tpl_vars['error']->key;
 $_smarty_tpl->tpl_vars['error']->index++;
 $_smarty_tpl->tpl_vars['error']->first = $_smarty_tpl->tpl_vars['error']->index === 0;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["errors"]['first'] = $_smarty_tpl->tpl_vars['error']->first;
?>
                <tr <?php if ($_smarty_tpl->getVariable('smarty')->value['foreach']['errors']['first']) {?> class="no-border"<?php }?>>
                    <td class="text-error"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['error']->value, ENT_QUOTES, 'UTF-8');?>
</td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <div class="alert alert-success">
            <p><?php echo $_smarty_tpl->__("email_template_import_success_msg");?>
</p>
        </div>
    <?php }?>
    <table width="100%" class="table table-no-hover table--relative">
        <tr class="no-border">
            <td width="60%"><strong><?php echo $_smarty_tpl->__('count_email_template_successfully_imported');?>
</strong></td>
            <td align="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['import_result']->value['count_success_templates'], ENT_QUOTES, 'UTF-8');?>
</td>
        </tr>
        <tr>
            <td width="60%"><strong><?php echo $_smarty_tpl->__('count_email_template_fail_imported');?>
</strong></td>
            <td align="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['import_result']->value['count_fail_templates'], ENT_QUOTES, 'UTF-8');?>
</td>
        </tr>
        <tr>
            <td width="60%"><strong><?php echo $_smarty_tpl->__('count_snippet_successfully_imported');?>
</strong></td>
            <td align="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['import_result']->value['count_success_snippets'], ENT_QUOTES, 'UTF-8');?>
</td>
        </tr>
        <tr>
            <td width="60%"><strong><?php echo $_smarty_tpl->__('count_snippet_fail_imported');?>
</strong></td>
            <td align="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['import_result']->value['count_fail_snippets'], ENT_QUOTES, 'UTF-8');?>
</td>
        </tr>
    </table>
    <div>
        <a class="btn cm-notification-close pull-right"><?php echo $_smarty_tpl->__("close");?>
</a>
    </div>
</div><?php }} ?>
