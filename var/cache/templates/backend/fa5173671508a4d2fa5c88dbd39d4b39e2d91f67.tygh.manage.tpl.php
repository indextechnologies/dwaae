<?php /* Smarty version Smarty-3.1.21, created on 2022-03-02 14:49:27
         compiled from "/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/smart_app_homepage/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1676159964621f4bb7196bc1-98740301%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa5173671508a4d2fa5c88dbd39d4b39e2d91f67' => 
    array (
      0 => '/home/dwaae/public_html/design/backend/templates/addons/ec_smart_app/views/smart_app_homepage/manage.tpl',
      1 => 1627201567,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1676159964621f4bb7196bc1-98740301',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'config' => 0,
    'search' => 0,
    'homepage_grid_list' => 0,
    'homepage' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_621f4bb720cc35_75563514',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_621f4bb720cc35_75563514')) {function content_621f4bb720cc35_75563514($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/home/dwaae/public_html/app/functions/smarty_plugins/modifier.truncate.php';
?><?php
\Tygh\Languages\Helper::preloadLangVars(array('title','position','type','ec_smart_app.no_of_products_show','ec_smart_app.manual','newest','ec_smart_app.onsale','ec_smart_app.popular','rating','edit','delete','no_items','ec_smart_app.add_homepage','smart_app_homepage'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>
    <form action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'UTF-8');?>
" method="post" name="manage_smart_app_homepage_form" id="elm_smart_app_homepage">
        <?php $_smarty_tpl->tpl_vars["return_current_url"] = new Smarty_variable(rawurlencode($_smarty_tpl->tpl_vars['config']->value['current_url']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_url"] = new Smarty_variable(fn_query_remove($_smarty_tpl->tpl_vars['config']->value['current_url'],"sort_by","sort_order"), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_icon"] = new Smarty_variable("<i class=\"exicon-".((string)$_smarty_tpl->tpl_vars['search']->value['sort_order_rev'])."\"></i>", null, 0);?>
        <?php $_smarty_tpl->tpl_vars["c_dummy"] = new Smarty_variable("<i class=\"exicon-dummy\"></i>", null, 0);?>
        <?php if ($_smarty_tpl->tpl_vars['homepage_grid_list']->value) {?>
            <table class="table table-middle sortable">
                <thead>
                <tr>
                    <th width="1%" class="center"><?php echo $_smarty_tpl->getSubTemplate ("common/check_items.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
</th>
                    <th width="30%"><?php echo $_smarty_tpl->__("title");?>
</th>  
                    <th width="20%"><?php echo $_smarty_tpl->__("position");?>
</th>    
                    <th width="25%"><?php echo $_smarty_tpl->__("type");?>
</th>
                    <th width="20%"><?php echo $_smarty_tpl->__("ec_smart_app.no_of_products_show");?>
</th>
                    <th width="10%"></th>
                </tr>
                </thead>
                <tbody>
                <?php  $_smarty_tpl->tpl_vars['homepage'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['homepage']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['homepage_grid_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['homepage']->key => $_smarty_tpl->tpl_vars['homepage']->value) {
$_smarty_tpl->tpl_vars['homepage']->_loop = true;
?>
                    <tr class="cm-row-status cm-row-status-<?php echo htmlspecialchars(mb_strtolower($_smarty_tpl->tpl_vars['homepage']->value['status'], 'UTF-8'), ENT_QUOTES, 'UTF-8');?>
">
                        <td class="center">
                            <input type="checkbox" name="homepage_ids[]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['homepage']->value['id'], ENT_QUOTES, 'UTF-8');?>
" class="checkbox cm-item" />
                        </td>
                        <td class="row-status"><a class="row-status" href="<?php echo htmlspecialchars(fn_url("smart_app_homepage.update?id=".((string)$_smarty_tpl->tpl_vars['homepage']->value['id'])), ENT_QUOTES, 'UTF-8');?>
"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['homepage']->value['title'],40);?>
</a></td>
                        <td class="row-status"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['homepage']->value['position'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td class="row-status">
                            <?php if ($_smarty_tpl->tpl_vars['homepage']->value['type']=='M') {
echo $_smarty_tpl->__("ec_smart_app.manual");
}?>
                            <?php if ($_smarty_tpl->tpl_vars['homepage']->value['type']=='N') {
echo $_smarty_tpl->__("newest");
}?>
                            <?php if ($_smarty_tpl->tpl_vars['homepage']->value['type']=='O') {
echo $_smarty_tpl->__("ec_smart_app.onsale");
}?>
                            <?php if ($_smarty_tpl->tpl_vars['homepage']->value['type']=='P') {
echo $_smarty_tpl->__("ec_smart_app.popular");
}?>
                            <?php if ($_smarty_tpl->tpl_vars['homepage']->value['type']=='R') {
echo $_smarty_tpl->__("rating");
}?>
                        </td>
                        <td class="row-status"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['homepage']->value['no_of_count'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td  class="nowrap">
                            <div class="hidden-tools">
                                <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
                                    <li>
                                        <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("edit"),'href'=>"smart_app_homepage.update?id=".((string)$_smarty_tpl->tpl_vars['homepage']->value['id'])));?>

                                    </li>
                                    <li>
                                        <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"list",'text'=>$_smarty_tpl->__("delete"),'class'=>"cm-confirm cm-post",'href'=>"smart_app_homepage.delete?id=".((string)$_smarty_tpl->tpl_vars['homepage']->value['id'])));?>

                                    </li>
                                <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
                                <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

                            </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            </table>
        <?php } else { ?>
            <p class="no-items"><?php echo $_smarty_tpl->__("no_items");?>
</p>
        <?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php $_smarty_tpl->_capture_stack[0][] = array("adv_buttons", null, null); ob_start(); ?>
    <?php echo $_smarty_tpl->getSubTemplate ("common/tools.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('tool_href'=>"smart_app_homepage.update",'prefix'=>"top",'title'=>$_smarty_tpl->__("ec_smart_app.add_homepage"),'hide_tools'=>true,'icon'=>"icon-plus"), 0);?>

    <?php $_smarty_tpl->_capture_stack[0][] = array("tools_list", null, null); ob_start(); ?>
        <li>
            <?php smarty_template_function_btn($_smarty_tpl,array('type'=>"delete_selected",'dispatch'=>"dispatch[smart_app_homepage.m_delete]",'form'=>"manage_smart_app_homepage_form"));?>

        </li>
    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    <?php smarty_template_function_dropdown($_smarty_tpl,array('content'=>Smarty::$_smarty_vars['capture']['tools_list']));?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->__("smart_app_homepage"),'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'title_extra'=>Smarty::$_smarty_vars['capture']['title_extra'],'tools'=>Smarty::$_smarty_vars['capture']['tools'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'select_languages'=>true), 0);?>
<?php }} ?>
