{if $facebook_pixel_ids && $addons.sd_facebook_pixel.complete_registration_facebook_pixel == "Y"}
    <script type="text/javascript">
        (function(_, $) {
            fbq('track', 'CompleteRegistration', {
                value: {$addons.sd_facebook_pixel.complete_registration_value_facebook_pixel|default:0},
                {if $addons.sd_facebook_pixel.complete_registration_currency_facebook_pixel}
                    currency: '{$addons.sd_facebook_pixel.complete_registration_currency_facebook_pixel|escape:javascript}'
                {else}
                    currency: '{$primary_currency|escape:javascript}'
                {/if}
            });
        }(Tygh, Tygh.$));
    </script>
{/if}
