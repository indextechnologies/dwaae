{if $field.field_type == "L" && $field.field_name == "ec_lat"}
    <div class="ty-control-group">
        <label for="{$id_prefix}elm_{$field.field_id}" class="cm-required hidden">{__("ec_google_pin_address.get_address")}</label>
        <div class="ec-google-pin-button">
            <a href="" id="google_map_btn_follow" class="cm-dialog-opener cm-dialog-auto-size ty-btn ty-btn__secondary" rel="nofollow" data-ca-target-id="ec_google_map_btn_follow" >{__("ec_google_pin_address.use_google_map")}
                <?xml version="1.0" encoding="UTF-8"?>
                <svg style="vertical-align:bottom" width="14px" height="20px" viewBox="0 0 14 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <path d="M7,0 C3.13,0 0,3.13 0,7 C0,12.25 7,20 7,20 C7,20 14,12.25 14,7 C14,3.13 10.87,0 7,0 Z M7,9.5 C5.62,9.5 4.5,8.38 4.5,7 C4.5,5.62 5.62,4.5 7,4.5 C8.38,4.5 9.5,5.62 9.5,7 C9.5,8.38 8.38,9.5 7,9.5 Z" id="path-1"></path>
                    </defs>
                    <g id="icon/maps/place_24px" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <mask id="mask-2" fill="white">
                            <use xlink:href="#path-1"></use>
                        </mask>
                        <use fill="red" fill-rule="nonzero" xlink:href="#path-1"></use>
                    </g>
                </svg>
            </a>
        </div>
        <div id="ec_google_map_btn_follow" class="hidden" title="{__("ec_google_pin_address.use_map")}">
            <div class="ec_map_container">
                <div style="padding: 10px 0px;"><input id="pac-input" class="ty-input-text" type="text" placeholder="{__('ec_google_pin_address.search_placeholder')}" style="width: 100%;"></div>
                <div id="ec-google-map" style="height: 350px;" data-map-key="{$addons.ec_google_pin_address.ec_google_api_key}" class="cm-ec-geo-map-container" data-ca-lat="" data-ca-lng=""></div>
            </div>      
            <div class="buttons-container">
                <div class="ty-float-right">
                    {include file="buttons/button.tpl" but_id="ec_button_map" but_text=__("save") but_role="text" but_meta="cm-dialog-closer ty-btn ty-btn__secondary"}
                </div>
            </div>
        </div> 
    </div>
    <input
        type="hidden"
        id="{$id_prefix}elm_{$field.field_id}"
        name="{$data_name}[{$data_id}]"
        size="32"
        value="{$value}"
        class="ty-input-text ec_lat_val" 
    />   
    {* </div> *}
{/if}
{if $field.field_type == "L" && $field.field_name == "ec_lng"}
    <input
        type="hidden"
        id="{$id_prefix}elm_{$field.field_id}"
        name="{$data_name}[{$data_id}]"
        size="32"
        value="{$value}"
        class="ty-input-text ec_lng_val"
    />
{/if}