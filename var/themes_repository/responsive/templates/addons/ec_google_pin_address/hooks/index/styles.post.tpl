<style>
    @media screen and (min-width: 768px) {
        #ec-google-map {
            width:743px;
        }
        .pac-container {
            z-index: 10000 !important;
        }
    }
    
    .object-container {
        min-height:350px !important;
    }
    .ec-google-pin-address {
        border: 1px solid #80808094;
        padding: 15px 4px;
        position: relative;
        border-radius: 3px;
    }
    .ec-google-inner-address {
        display: inline-block;
        margin-left: 12px;
        margin-right: 26%;
    }
</style>