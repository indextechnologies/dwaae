{include
    file="views/checkout/components/customer/location.tpl"
    profile_fields=$items
    section="ProfileFieldSections::SHIPPING_ADDRESS"|enum
    block_title=$block.name
}

{assign var=enable_map value=$addons.ec_google_pin_address.ec_enable_auto}
{if $enable_map == 'Y'}
    <div class="ec-google-pin-address">
        <div class="ec-google-inner-address">
            {__("ec_google_pin_use_map")}
        </div>
        <div class="ec-google-pin-button">
            <a href="" id="google_map_btn_follow" class="cm-dialog-opener cm-dialog-auto-size ty-btn ty-btn__secondary" rel="nofollow" data-ca-target-id="ec_google_map_btn_follow" >{__("use_google_map")}
                <?xml version="1.0" encoding="UTF-8"?>
                <svg style="vertical-align:bottom" width="14px" height="20px" viewBox="0 0 14 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <path d="M7,0 C3.13,0 0,3.13 0,7 C0,12.25 7,20 7,20 C7,20 14,12.25 14,7 C14,3.13 10.87,0 7,0 Z M7,9.5 C5.62,9.5 4.5,8.38 4.5,7 C4.5,5.62 5.62,4.5 7,4.5 C8.38,4.5 9.5,5.62 9.5,7 C9.5,8.38 8.38,9.5 7,9.5 Z" id="path-1"></path>
                    </defs>
                    <g id="icon/maps/place_24px" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <mask id="mask-2" fill="white">
                            <use xlink:href="#path-1"></use>
                        </mask>
                        <use fill="red" fill-rule="nonzero" xlink:href="#path-1"></use>
                    </g>
                </svg>
            </a>
        </div>
    </div>
{/if}

{capture name="google_pin_address_btn_`$obj_id`"}

    <div class="ec_map_container">
        <div style="padding: 10px 0px;"><input id="pac-input" class="ty-input-text" type="text" placeholder="{__('ec_search_placeholder')}" style="width: 100%;"></div>
        <div id="map" style="height: 350px;"></div>
    
        {if $user_data.ec_location_detail}
            {$ec_location_detail = unserialize($user_data.ec_location_detail)}
        {/if}
        <input type='hidden' name='lat' id='lat' value="{$ec_location_detail.lat|default:51.508742}">  
        <input type='hidden' name='lng' id='lng' value="{$ec_location_detail.lng|default:-0.120850}"> 
    </div>      
    
   
    {assign var=country_restriction value=$addons.ec_google_pin_address.ec_google_restrict_country|json_encode}
    {assign var=enable_map value=$addons.ec_google_pin_address.ec_enable_auto}
    <script>
        var restric=[]; 
        var country_restriction=JSON.parse('{$country_restriction nofilter}');
        var i=0;
        
        $.each(country_restriction, function (index, value) {
            restric[i]=index;
            i=i+1;
        });

        function getAddress(place) {
            var lengt=place.address_components.length;
            var country = '';
            var state = '';
            var city = '';
            var address = '';
            var zip_code = '';
            var i=0;
            for(i=0;i < lengt;++i) {
                if(place.address_components[i].types.indexOf("country") > -1){
                    country=place.address_components[i]. short_name;
                    var country1=place.address_components[i]. long_name;
                }
                if(place.address_components[i].types.indexOf("administrative_area_level_1") > -1){
                    state=place.address_components[i]. short_name;
                    var state1=place.address_components[i]. long_name;
                }
                if(place.address_components[i].types.indexOf("locality") > -1){
                    city=place.address_components[i]. short_name;
                    var city1=place.address_components[i]. long_name;
                }
                if(place.address_components[i].types.indexOf("postal_code") > -1){
                    zip_code=place.address_components[i]. short_name;

                }
            } 
            var data={};
            var formatted_address=place.formatted_address;
            var ret =formatted_address.replace(country,'');
            var ret =ret.replace(country1,'');
            var ret =ret.replace(state1,'');
            var ret =ret.replace(state,'');
            var ret =ret.replace(city1,'');
            var ret =ret.replace(city,'');
            var ret =ret.replace(zip_code,'');
            var ret =ret.replace(/,/g,'');
            $('#pac-input').val(place.formatted_address);
            localStorage.address= ret;
            localStorage.country= country;
            localStorage.state= state;
            localStorage.city= city;
            localStorage.zip_code= zip_code;
        }

        function initMap() {
            var lat = document.getElementById('lat');
            var lng = document.getElementById('lng');
            {literal}   
            var myLatlng = new google.maps.LatLng(lat.value,lng.value);
            var mapProp = {
                center:myLatlng,
                zoom:9,
                mapTypeId:google.maps.MapTypeId.ROADMAP    
            };
            
            var map=new google.maps.Map(document.getElementById("map"), mapProp);
                var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable:true  
            });

            var geocoder = new google.maps.Geocoder()

            // marker drag event
            google.maps.event.addListener(marker,'drag',function(event) {
                lat.value = event.latLng.lat();
                lng.value = event.latLng.lng();
                geocoder.geocode({
                    'latLng': event.latLng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            getAddress(results[0]);     
                        }
                    }
                });
            });

            //marker drag event end
            google.maps.event.addListener(marker,'dragend',function(event) {
                lat.value = event.latLng.lat();
                lng.value = event.latLng.lng();
                geocoder.geocode({
                    'latLng': event.latLng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            getAddress(results[0]);     
                        }
                    }
                });
            });

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var ec_option = {
                componentRestrictions: { country: restric }
            }

            var searchBox = new google.maps.places.SearchBox(input,ec_option);

            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });
            var markers = [];
            var new_marker;
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }
                marker.setMap(null);
                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });

                if(new_marker != undefined) {
                    new_marker.setMap(null);
                }

                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    lat.value = place.geometry.location.lat();
                    lng.value = place.geometry.location.lng();

                    geocoder.geocode({
                            'latLng': place.geometry.location
                            }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    getAddress(results[0]);     
                                }
                            }
                    });
                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        title: place.name,
                        position: place.geometry.location,
                        draggable:true  
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });

                map.fitBounds(bounds);
                var test = function(event) {
                        new_marker = new google.maps.Marker({
                        position: event.latLng,
                        map: map,
                        draggable:true 
                        });

                    google.maps.event.addListener(new_marker,'drag',function(event) {
                        lat.value = event.latLng.lat();
                        lng.value = event.latLng.lng();
                        geocoder.geocode({
                            'latLng': event.latLng
                            }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    getAddress(results[0]);     
                                }
                            }
                        });
                    });
                    google.maps.event.addListener(new_marker,'dragend',function(event) {
                        lat.value = event.latLng.lat();
                        lng.value = event.latLng.lng();
                        geocoder.geocode({
                            'latLng': event.latLng
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    getAddress(results[0]);     
                                }
                            }
                        });
                    });

                    google.maps.event.addListener(new_marker,'click',function(event) {
                        lat.value = event.latLng.lat();
                        lng.value = event.latLng.lng();
                        geocoder.geocode({
                            'latLng': event.latLng
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    getAddress(results[0]);     
                                }
                            }
                        });
                    });
                    
                }
                markers.forEach(function(marker) {
                    google.maps.event.addListener(marker,'drag',function(event) {
                            markers.forEach(function(markersa) {
                                markersa.setMap(null);
                            });
                            markers = [];
                            test(event);
                        lat.value = event.latLng.lat();
                        lng.value = event.latLng.lng();
                            geocoder.geocode({
                            'latLng': event.latLng
                            }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    getAddress(results[0]);     
                                }
                            }
                        });
                    });
                    google.maps.event.addListener(marker,'dragend',function(event) {
                        lat.value = event.latLng.lat();
                        lng.value = event.latLng.lng();
                        geocoder.geocode({
                            'latLng': event.latLng
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    getAddress(results[0]);     
                                }
                            }
                        });
                    });
                    google.maps.event.addListener(marker,'click',function(event) {
                            markers.forEach(function(markersa) {
                                markersa.setMap(null);
                            });
                            markers = [];
                            test(event);
                                lat.value = event.latLng.lat();
                            lng.value = event.latLng.lng();
                            geocoder.geocode({
                                'latLng': event.latLng
                            }, function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    if (results[0]) {
                                        getAddress(results[0]);     
                                    }
                                }
                            });
                    });
                });
            });
        {/literal}
            
        }

        Tygh.$('#pac-input').on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) { 
                e.preventDefault();
                return false;
            }
        });

        Tygh.$('#pac-input').on('keyup keypress', function(e) {
            var keyCodeb = e.keyCode || e.which;
            if (keyCodeb === 13) { 
                e.preventDefault();
                return false;
            }
        });

        Tygh.$('#ec_button_map').on('click',function(){
            if(localStorage.address != 'undefined') {
                ret = localStorage.address;
                country= localStorage.country;
                state= localStorage.state;
                city = localStorage.city;
                zip_code = localStorage.zip_code;
                if(Tygh.$("input[name='user_data[s_address]']").length) {
                    Tygh.$("input[name='user_data[s_address]']").val(ret);
                }
                if(Tygh.$("input[name='user_data[b_address]']").length) {
                    Tygh.$("input[name='user_data[b_address]']").val(ret);
                }
                if(Tygh.$("input[name='user_data[s_zipcode]']").length) {
                    Tygh.$("input[name='user_data[s_zipcode]']").val(zip_code);
                }
                if(Tygh.$("input[name='user_data[b_zipcode]']").length) {
                    Tygh.$("input[name='user_data[b_zipcode]']").val(zip_code);
                }
                if(Tygh.$("#litecheckout_city").length) {
                    Tygh.$("#litecheckout_city").val(city);
                }
                if(Tygh.$("input[name='user_data[b_city]']").length) {
                    Tygh.$("input[name='user_data[b_city]']").val(city);
                }
                if(Tygh.$("#litecheckout_country").length) {
                    Tygh.$("#litecheckout_country").val(country);
                    $('#shipping_rates_list').addClass('litecheckout__overlay--active');
                }
                if(Tygh.$("select[name='user_data[b_country]']").length) {
                    Tygh.$("select[name='user_data[b_country]']").val(country);
                }
                    
                Tygh.$("select[data-ca-lite-checkout-field='user_data.s_state']").find('option').remove().end();
                var data = {};
                data['code']=country;
                Tygh.$.ceAjax('request', fn_url('ec_google_pin_address.ec_auto_state'), {
                    data: data,
                    method: 'post',
                    callback: function(dataes) {
                        var code=dataes.data;
                        if(code == undefined) {
                            $("select[data-ca-lite-checkout-field='user_data.s_state']").addClass('hidden');
                            $("input[data-ca-lite-checkout-field='user_data.s_state']").removeClass('hidden');
                            $("input[data-ca-lite-checkout-field='user_data.s_state']").show();
                            $("input[data-ca-lite-checkout-field='user_data.s_state']").val(state);
                        }
                        else {
                            var j = 0;
                            var id = Tygh.$("select[data-ca-lite-checkout-field='user_data.s_state']").attr('id');
                            var div = document.getElementById(id);
                            for (var p in code) { 
                                if(p == state)
                                    j++;

                                div.innerHTML +='<option value="' + p + '">' + code[p] + '</option>';
                            }
                            if(j == 0) {
                                $("select[data-ca-lite-checkout-field='user_data.s_state']").addClass('hidden');
                                $("input[data-ca-lite-checkout-field='user_data.s_state']").removeClass('hidden');
                                $("input[data-ca-lite-checkout-field='user_data.s_state']").show();
                                $("input[data-ca-lite-checkout-field='user_data.s_state']").val(state);
                            }
                            else {
                                $("input[data-ca-lite-checkout-field='user_data.s_state']").addClass('hidden');
                                $("select[data-ca-lite-checkout-field='user_data.s_state']").removeClass('hidden');
                                $("select[data-ca-lite-checkout-field='user_data.s_state']").removeAttr('disabled');
                                $("select[data-ca-lite-checkout-field='user_data.s_state']").show();
                                div.value=state;
                                Tygh.$("select[data-ca-lite-checkout-field='user_data.s_state']").val(state);   
                            }
                        }
                    }
                });

                if(Tygh.$("select[name='user_data[b_state]']").length) {
                    Tygh.$("select[name='user_data[b_state]']").find('option').remove().end();
                    var data = {};
                    data['code']=country;
                    Tygh.$.ceAjax('request', fn_url('ec_google_pin_address.ec_auto_state'), {
                        data: data,
                        method: 'post',
                        callback: function(dataes) {
                            var code=dataes.data;
                            if(code == undefined) {
                                $("#litecheckout_b_state").addClass('hidden');
                                $('#litecheckout_b_state_d').show();
                                document.getElementById('litecheckout_b_state_d').value=state;
                            }
                            else {
                                var i = 0;
                                var div = document.getElementById('litecheckout_b_state');
                                for (var p in code) {
                                    if(p == state)
                                        i++; 
                                    div.innerHTML +='<option value="' + p + '">' + code[p] + '</option>';
                                }

                                if(i == 0) {
                                    $("#litecheckout_b_state").addClass('hidden');
                                    $('#litecheckout_b_state_d').show();
                                    document.getElementById('litecheckout_b_state_d').value=state;
                                }
                                else {
                                    $("#litecheckout_b_state_d").addClass('hidden');
                                    div.value=state;
                                    Tygh.$("#litecheckout_b_state").val(state);   
                                }
                            }
                        }
                    });
                }
            }
            else {
                var err_msg = _.tr('please_select_map');
                Tygh.$.ceNotification('show', {
                    type: 'E',
                    title: _.tr('error'),
                    message: err_msg
                });
            }
        });

    </script>

    {if $addons.ec_google_pin_address.status == 'A'}
        {if $enable_map == 'Y'}
            {if $addons.ec_google_pin_address.ec_google_api_key}
                {assign var=map_api_key value=$addons.ec_google_pin_address.ec_google_api_key|replace:' ':''} 
                <script async defer
                src="https://maps.googleapis.com/maps/api/js?key={$map_api_key}&callback=initMap&libraries=places">
                </script>
            {/if}
        {/if}
    {/if}      

    <div class="clearfix  buttons-container">
        <div class="ty-float-right">
            {include file="buttons/button.tpl" but_id="ec_button_map" but_text=__("ec_google_map_save") but_role="text" but_meta="cm-dialog-closer ty-btn ty-btn__primary"}
        </div>
    </div>
{/capture}

<div id="ec_google_map_btn_follow"   hidden title="{__("map")}">
    {assign var="capture_name" value="google_pin_address_btn_`$obj_id`"}
    {$smarty.capture.$capture_name nofilter}
</div> 
<style>
    @media screen and (min-width: 768px) {
        #map {
            width:743px;
        }
        .pac-container {
            z-index: 10000 !important;
        }
    }
   
    .object-container {
        min-height:360px !important;
    }
    .ec-google-pin-button {
        position: absolute;
        right: 19px;
        top: 12%;
        display: inline-block;
    }
    .ec-google-pin-address {
        border: 1px solid #80808094;
        padding: 15px 4px;
        position: relative;
        border-radius: 3px;
        width: 100%;
    }
    .ec-google-inner-address {
        display: inline-block;
        margin-left: 12px;
        margin-right: 26%;
    }
</style>
