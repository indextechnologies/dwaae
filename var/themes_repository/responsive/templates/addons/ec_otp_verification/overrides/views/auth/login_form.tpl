
{assign var="id" value=$id|default:"main_login"}    
<script type="text/javascript">
    (function(_, $) {
            $(document).ready(function(){
                $(document).on('click change', '#login_with_otp_{$id}', function(event) {
                    if ($(this).is(':checked')) {
                        $(".ty-password-forgot_{$id}, .email_login_div_{$id}").hide();
                        $(".phone_login_div_{$id}").show();
                        $(".ec_otp_phone_{$id}").attr("disabled", false);
                        $(".ec_phone_lbl_{$id}").addClass("cm-required");
                        $(".ec_email_lbl_{$id}, .ec_psw_lbl_{$id}").removeClass("cm-required");
                        $(".ec_otp_email_{$id}, .ec_otp_psw_{$id}").attr('disabled', true);
                        $('.return_url').attr('disabled', true);
                    }else{
                        $(".ty-password-forgot_{$id}, .email_login_div_{$id}").show();
                        $(".phone_login_div_{$id}").hide();
                        $(".ec_phone_lbl_{$id}").removeClass("cm-required");
                        $(".ec_otp_phone_{$id}").attr("disabled", true);
                        $(".ec_email_lbl_{$id}, .ec_psw_lbl_{$id}").addClass("cm-required");
                        $(".ec_otp_email_{$id}, .ec_otp_psw_{$id}").attr('disabled', false);
                        $('.return_url').attr('disabled', false);
                    }
                });
            });
    }(Tygh, Tygh.$));
</script>

{capture name="login"}
    <form name="{$id}_form" action="{""|fn_url}" method="post">  
    <input type="hidden" name="return_url" class="return_url" value="{$smarty.request.return_url|default:$config.current_url}" />
    <input type="hidden" name="redirect_url"  class="return_url" value="{$config.current_url}" />
    <input type="hidden" name="retURL" value="{$smarty.request.return_url|default:$config.current_url}" />
    <input type="hidden" name="redURL" value="{$config.current_url}" />
        {if $style == "checkout"}
            <div class="ty-checkout-login-form">{include file="common/subheader.tpl" title=__("returning_customer")}
        {/if}
        
        <div class="ty-control-group email_login_div_{$id}">
            <label for="login_{$id}" class="ty-login__filed-label ty-control-group__label cm-required cm-trim ec_email_lbl_{$id}">{__("email")}</label>
            <input type="text" id="login_{$id}" name="user_login" size="30" value="{$config.demo_username}" class="ty-login__input cm-focus ec_otp_email_{$id} cm-email" />
        </div>
        <div class="ty-control-group phone_login_div_{$id} hidden">
            <label for="phone_login_{$id}" class="ty-login__filed-label ty-control-group__label cm-trim ec_phone_lbl_{$id}">{__("phone")}</label>
            <input type="text" id="phone_login_{$id}" name="otp_phone" size="30" value="{$config.demo_username}" class="ty-login__input cm-focus ec_otp_phone_{$id} cm-mask-phone cm-otp-mask-phone" />
        </div>
         <div class="ty-control-group ty-password-forgot_{$id}">
            <label for="psw_{$id}" class="ty-login__filed-label ty-control-group__label ty-password-forgot__label ec_psw_lbl_{$id}cm-required">{__("password")}</label>
            <input type="password" id="psw_{$id}" name="password" size="30" value="{$config.demo_password}" class="ty-login__input ec_otp_psw_{$id}" maxlength="32" />
            <a href="{"auth.recover_password"|fn_url}" class="ty-password-forgot__a"  tabindex="5">{__("forgot_password_question")}</a>
        </div>
        <div class="ty-login__with-otp">
              <label for="login_with_otp_{$id}" class="ty-login__with-otp-label"><input class="checkbox login_via_otp" type="checkbox" name="login_with_otp" id="login_with_otp_{$id}" value="Y" />{__("ec_otp_verification.login_with_otp")}</label>
        </div>
             
        {include file="common/image_verification.tpl" option="login" align="left"}

        {if $style == "checkout"}
            </div>
        {/if}
        {if $style == "popup"}
        <div class="ty-login-reglink ty-center">
                <a class="ty-login-reglink__a" href="{"profiles.add"|fn_url}" rel="nofollow">{__("register_new_account")}</a>
            </div>
        {/if}
        {hook name="index:login_buttons"}
            <div class="buttons-container clearfix">
                <div class="ty-float-right">
                    {include file="buttons/login.tpl" but_name="dispatch[auth.login]" but_role="submit"}
                </div>
                <div class="ty-login__remember-me">
                    <label for="remember_me_{$id}" class="ty-login__remember-me-label"><input class="checkbox" type="checkbox" name="remember_me" id="remember_me_{$id}" value="Y" />{__("remember_me")}</label>
                </div>
            </div>
        {/hook}
    </form>
{/capture}

{if $style == "popup"}
    {$smarty.capture.login nofilter}
{else}
    <div class="ty-login">
        {$smarty.capture.login nofilter}
    </div>

    {capture name="mainbox_title"}{__("sign_in")}{/capture}
{/if}
