<form name="change_number_form" action="{""|fn_url}" method="post">
	{if $previous_data}
		<div class="previous_data">
			{foreach from=$previous_data item=value key=k}
			{if $k != 'user_data' && $k != 'all_mailing_lists'}
				<input type="hidden" name="{$k}" value="{$value}" /> 
			{/if} 
			{/foreach}
			{foreach from=$previous_data.user_data item=value key=k}
			{if $k != 'user_data'}
				<input type="hidden" name="user_data[{$k}]" value="{$value}" />    
			{/if} 
			{/foreach}
		</div>
	{/if}
	<div class="ty-control-group" >
		<p><b>{__("ec_otp_verification._you_can_change_your_number_here_and_otp_will_be_send_on_updated_number")}</b></p>
		<p><b>{__("ec_otp_verification.pre_no")}{$previous_data.user_data.phone}</b></p>
	</div>
	<div class="ty-control-group" >
		<label for="new_number_{$id}" class="ty-login__filed-label ty-control-group__label cm-required cm-profile-field cm-mask-phone-label">{__("ec_otp_verification.enter_your_new_number")}</label>
		<input type="text" id="new_number_{$id}" name="user_data[new_number]" size="" value="" class="ty-input-text cm-focus cm-mask-phone cm-focus cm-otp-mask-phone" />
	</div>
	<div  class="ty-product-block__button">         
		{include file="buttons/button.tpl" but_meta="ty-btn__secondary" but_href="profiles.add" but_text=__("ec_otp_verification.start_over") but_role="tool" but_id="back_button"}	   
		{include file="buttons/button.tpl" but_name="dispatch[otp_verify_register.register]" but_text=__("submit") but_role="submit" but_meta="ty-btn__primary ty-btn__big cm-form-dialog-closer ty-btn"}
	</div>
</form>