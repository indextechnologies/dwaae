{script src="js/lib/maskedinput/jquery.maskedinput.min.js"}
{script src="js/lib/inputmask/jquery.inputmask.min.js"}
{script src="js/lib/jquery-bind-first/jquery.bind-first-0.2.3.js"}
{script src="js/lib/inputmask-multi/jquery.inputmask-multi.js"}
<script type="text/javascript">
    (function(_, $) {
        _.otp_phone_mask = '{$addons.ec_otp_verification.otp_phone_mask}'
    }(Tygh, Tygh.$));
</script>

{script src="js/addons/ec_otp_verification/otp_phone_verify.js?v=1.12"}
