{if $field.field_name == "phone" && $field.section == 'C' && $auth.user_id}
    {assign var="is_verified" value=fn_check_is_number_verified()}
    {if $is_verified}
        <span style="color: green" class="phone-verification-status">{__("ec_otp_verification.verified")} {$value}</span>
    {else}
        <span style="color: red" class="phone-verification-status">{__("ec_otp_verification.not_verified")}</span>
    {/if}
    <script type="text/javascript">
        (function(_, $) {
            $('.phone-verification-status').appendTo($('[name="user_data[phone]"]').parent());	
        })(Tygh,Tygh.$);
    </script>
{/if}
