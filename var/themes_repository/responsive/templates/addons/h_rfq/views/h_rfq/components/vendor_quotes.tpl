<div class="vendor-quotes-block" id="{if $container_id}{$container_id}{else}content_vendor_quotes{/if}">
    {include file="common/subheader.tpl" title=$title}

    {foreach $vendor_quotes as $vendor_id => $quotes}
    <div class="vendor-quote-block">
        <h4>{$vendor_id|fn_get_company_name}</h4>
        <table class="ty-orders-detail__table ty-table">
            <thead>
                <tr>
                    <th class="ty-orders-detail__table-product">{__("h_rfq.product_name")}</th>
                    <th class="ty-orders-detail__table-price">{__("h_rfq.selling_price")}</th>
                    <th class="ty-orders-detail__table-quantity">{__("h_rfq.product_qty")}</th>
                    <th class="ty-orders-detail__table-subtotal">{__("subtotal")}</th>
                </tr>
            </thead>
            {$total=0}
            {foreach $quotes as $quote_id => $quote}
                {$rfq_product_id=$quote.rfq_product_id}
                {$product=$products[$rfq_product_id]}
                <tr class="ty-valign-top">
                    <td>
                        <div class="clearfix">
                            <div class="ty-overflow-hidden ty-orders-detail__table-description-wrapper">
                                <div class="ty-ml-s ty-orders-detail__table-description">
                                    {if $product.is_accessible}<a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{/if}
                                        <strong>{$product.product_name nofilter}</strong>
                                        <div class="h-rfq_details_category">{__("h_rfq.product_category")}:&nbsp;{$product.product_category}</div>
                                        <div class="cm-show_hide_text h-rfq_details_description" data-ca-charqty="150" data-ca-ellipse-text=".." data-ca-more-text="{__("h_rfq.read_more")}" data-ca-less-text="{__("h_rfq.read_hide")}">{__("h_rfq.product_description")}:&nbsp;{$product.description}</div>
                                    {if $product.is_accessible}</a>{/if}  
                                    {if $product.product_code}
                                        <div class="ty-orders-detail__table-code">{__("sku")}:&nbsp;{$product.product_code}</div>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="ty-right">
                        {if $quote.price > 0}{include file="common/price.tpl" value=$quote.price}{else}--{/if}
                    </td>
                    <td class="ty-center">&nbsp;{$product.quantity} {$product.quantity_unit}</td>
                    <td class="ty-right">
                        {$subtotal=$product.quantity*$quote.price}
                        {$total=$total+$subtotal}
                        {if $subtotal}&nbsp;{include file="common/price.tpl" value=$subtotal}{else}--{/if}
                    </td>
                </tr>
            {/foreach}
            <tr>
                <td class="ty-right ty-strong">{__("total")}</td>
                <td></td>
                <td></td>
                <td class="ty-right">{include file="common/price.tpl" value=$total}</td>
            </tr>
        </table>
        <div class="buttons-container vendeor-quote-button-container ty-right">
            {if $quotation.selected_seller}
                {if $quotation.selected_seller == $vendor_id}
                    <div class="vendor-btn vendor-approved-btn">
                        {__("h_rfq.approved_vendor")}
                    </div>
                {else}
                    <div class="vendor-btn vendor-rejected-btn">
                        {__("h_rfq.rejected_vendor")}
                    </div>
                {/if}
            {else}
                {include
                    file="buttons/button.tpl"
                    but_href=fn_url("h_rfq.approve_quote&vendor_id=`$vendor_id`&rfq_id=`$quotation.rfq_id`")
                    but_text=__("h_rfq.approve")
                    but_title=__("h_rfq.approve")
                    but_role="link"
                    but_meta="ty-btn__primary cm-post"
                    but_rel="nofollow"}
            {/if}
        </div>
    </div>
    {foreachelse}
        <p class="ty-no-items">{__("no_data")}</p>
    {/foreach}
</div>