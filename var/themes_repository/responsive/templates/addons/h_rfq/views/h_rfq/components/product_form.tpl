{if !$sqid}
    {$sqid = rand(100000,999999)}
{/if}
<div class="h-rfq_product" data-sqid="{$sqid}">
    <span class="h_remove_product h_cross" data-target-sqid="{$sqid}">X</span>
    <div class="ty-control-group">
        <label for="el_product_name_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.product_name")}</label>
        <input type="text" id="el_product_name_{$sqid}" name="{$name}[{$sqid}][product_name]" size="100" maxlength="200" value="" class="ty-input-text cm-product-list-search" page-no="1" placeholder="{__("h_rfq.product_name_placeholder")}">
    </div>
    <div class="ty-control-group">
        <label for="el_product_category_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.product_category")}</label>
        <select id="el_product_category_{$sqid}" name="{$name}[{$sqid}][product_category]">
            {foreach from=$formatCategories item=cname key=cid}
                <option value="{$cid}">{$cname}</option>
            {/foreach}
        </select>
        {* <input type="text" id="el_product_category_{$sqid}" name="{$name}[{$sqid}][product_category]" size="30" maxlength="40" value="" class="ty-input-text cm-input-product-categories"> *}
    </div>
    <div class="ty-control-group">
        <label for="el_product_qty_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.product_qty")}</label>
        <input type="number" min="1" id="el_product_qty_{$sqid}" name="{$name}[{$sqid}][quantity]" size="20" maxlength="20" value="1" class="ty-input-text" placeholder="{__("h_rfq.product_qty_placeholder")}">
        <select name="{$name}[{$sqid}][quantity_unit]" class="cm-quanity-unit-selectbox h-rfq_selecbox_placeholder">
            <option value="" disabled selected hidden>{__("h_rfq.select_quantity_unit")}</option>
            <option value="">{__("none")}</option>
            {foreach $quantity_unit as $unit}
                <option value="{$unit}">{$unit}</option>
            {/foreach}
        </select>
        {* <input type="text" name="{$name}[{$sqid}][quantity_unit]" size="30" maxlength="40" value="" class="ty-input-text cm-quantity-units"> *}
    </div>
    <div class="ty-control-group">
        <label for="el_product_desc_{$sqid}" class="ty-control-group__title">{__("h_rfq.product_description")}</label>
        <textarea id="el_product_desc_{$sqid}" rows="4" cols="50" name="{$name}[{$sqid}][description]" style="width:100%;"></textarea>
    </div>
    <div class="ty-control-group">
        <label for="el_product_attachments_{$sqid}" class="ty-control-group__title">{__("h_rfq.product_attachments")}</label>
        {include file="common/fileuploader.tpl" 
            var_name="products_attachment[$sqid]"
            multiupload=false
            label_id="el_product_attachments_{$sqid}"}
    </div>
</div>