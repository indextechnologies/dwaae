{capture name="mainbox_title"}
    {__("h_rfq.quotation_detail_header", ['[id]' => $rfq_id])}</bdi>
    <em class="ty-date">({$quotation.create_time|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"})</em>
    <em class="ty-status">{__("status")}: {include file="addons/h_rfq/common/status.tpl" status=$quotation.status display="view" name="update_order[status]"}</em>
{/capture}

<div class="ty-orders-detail">
    {if $quotation}
        {capture name="quotations_actions"}
            {if $view_only != "Y"}
                <div class="ty-orders__actions">
                    {hook name="h_rfq:details_tools"}
                        {assign var="print_order" value=__("print_invoice")}
                        {assign var="print_pdf_order" value=__("print_pdf_invoice")}

                        {if $status_settings.appearance_type == "C" && $order_info.doc_ids[$status_settings.appearance_type]}
                            {assign var="print_order" value=__("print_credit_memo")}
                            {assign var="print_pdf_order" value=__("print_pdf_credit_memo")}
                        {elseif $status_settings.appearance_type == "O"}
                            {assign var="print_order" value=__("print_order_details")}
                            {assign var="print_pdf_order" value=__("print_pdf_order_details")}
                        {/if}

                        {include file="buttons/button.tpl" but_role="text" but_text=$print_order but_href="orders.print_invoice?order_id=`$order_info.order_id`" but_meta="cm-new-window ty-btn__text" but_icon="ty-icon-print orders-print__icon"}

                        {include file="buttons/button.tpl" but_role="text" but_meta="orders-print__pdf ty-btn__text cm-no-ajax" but_text=$print_pdf_order but_href="orders.print_invoice?order_id=`$order_info.order_id`&format=pdf" but_icon="ty-icon-doc-text orders-print__icon"}
                    {/hook}

                    <div class="ty-orders__actions-right">
                        {if $view_only != "Y"}
                            {hook name="hrfq:details_bullets"}
                            {/hook}
                        {/if}

                    </div>

                </div>
            {/if}
        {/capture}

        {capture name="tabsbox"}
            <div id="content_general" class="{if $selected_section && $selected_section != "general"}hidden{/if}">

                <div class="orders-customer">
                    {include file="common/subheader.tpl" title=__("h_rfq.customer_information")}
                    <div class="ty-profiles-info">
                        <div class="ty-profiles-info__item ty-profiles-info__billing">
                            <h5 class="ty-profiles-info__title">{__("h_rfq.buyer_details")}</h5>
                            <div class="ty-profiles-info__field">
                                <div class="ty-info-field">
                                    <bdi>{$quotation.firstname} {$quotation.lastname}</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi>{$quotation.email}</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi>{$quotation.phone}</bdi>
                                </div>
                            </div>
                        </div>

                        {$delivery_location=reset($locations)}
                        <div class="ty-profiles-info__item ty-profiles-info__shipping">
                            <h5 class="ty-profiles-info__title">{__("h_rfq.delivery_location")}</h5>
                            <div class="ty-profiles-info__field">
                                <div class="ty-info-field">
                                    <bdi>{$delivery_location.address}</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi>{$delivery_location.city}</bdi>
                                </div>
                                <div class="ty-info-field">
                                    <bdi>{fn_get_state_name($delivery_location.state,$delivery_location.country)}</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi>{fn_get_country_name($delivery_location.country)}</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi>{$delivery_location.zipcode}</bdi>
                                </div>
                                 <div class="ty-info-field">
                                    <bdi>{$delivery_location.phone}</bdi>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {capture name="group"}
                    {include file="common/subheader.tpl" title=__("h_rfq.products")}
                    <table class="ty-orders-detail__table ty-table">
                        {hook name="h_rfq:items_list_header"}
                            <thead>
                                <tr>
                                    <th class="ty-orders-detail__table-product">{__("h_rfq.product_name")}</th>
                                    <th class="ty-orders-detail__table-price">{__("h_rfq.selling_price")}</th>
                                    <th class="ty-orders-detail__table-quantity">{__("h_rfq.product_qty")}</th>
                                    <th class="ty-orders-detail__table-subtotal">{__("subtotal")}</th>
                                </tr>
                            </thead>
                        {/hook}
                        {foreach from=$products item="product" key="key"}
                            {hook name="h_rfq:items_list_row"}
                                {if $product}
                                    <tr class="ty-valign-top">
                                        <td>
                                            <div class="clearfix">

                                                <div class="ty-overflow-hidden ty-orders-detail__table-description-wrapper">
                                                    <div class="ty-ml-s ty-orders-detail__table-description">
                                                        {if $product.is_accessible}<a href="{"products.view?product_id=`$product.product_id`"|fn_url}">{/if}
                                                            <strong>{$product.product_name nofilter}</strong>
                                                            <div class="h-rfq_details_category">{__("h_rfq.product_category")}:&nbsp;{$product.product_category}</div>
                                                            <div class="cm-show_hide_text h-rfq_details_description" data-ca-charqty="150" data-ca-ellipse-text=".." data-ca-more-text="{__("h_rfq.read_more")}" data-ca-less-text="{__("h_rfq.read_hide")}">{__("h_rfq.product_description")}:&nbsp;{$product.description}</div>
                                                        {if $product.is_accessible}</a>{/if}  
                                                        {if $product.product_code}
                                                            <div class="ty-orders-detail__table-code">{__("sku")}:&nbsp;{$product.product_code}</div>
                                                        {/if}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="ty-right">
                                            {if $product.selling_price > 0}{include file="common/price.tpl" value=$product.selling_price}{else}--{/if}
                                        </td>
                                        <td class="ty-center">&nbsp;{$product.quantity} {$product.quantity_unit}</td>
                                        <td class="ty-right">
                                            {if $product.display_subtotal}&nbsp;{include file="common/price.tpl" value=$product.display_subtotal}{else}--{/if}
                                        </td>
                                    </tr>
                                {/if}
                            {/hook}
                        {/foreach}

                        {hook name="h_rfq:extra_list"}
                            {* {assign var="colsp" value=5}
                            {if $order_info.taxes && $settings.Checkout.tax_calculation != "subtotal"}{assign var="colsp" value=$colsp+1}{/if} *}
                        {/hook}

                    </table>

                    <div class="ty-quotation-attachments clearfix">
                        {include file="common/subheader.tpl" title=__("h_rfq.attachments")}
                        <div class="h-rfq_download_attachments">
                        {foreach $attachments as $attachment}
                            <div class="h-rfq_download_attachment">
                                <a href="{"h_rfq_attachments.download?file=`$attachment.file`"|fn_url}" title="{$attachment.name}">{$attachment.name|truncate:30}</a>
                                {* {include file="common/tooltip.tpl" tooltip="{$attachment.name}"} *}
                            </div>
                        {/foreach}
                        </div>
                    </div>

                    <div class="ty-orders-summary clearfix">
                        {include file="common/subheader.tpl" title=__("summary")}

                        <div class="ty-orders-summary__right">
                            {hook name="h_rfq:info"}{/hook}
                        </div>

                        <div class="ty-orders-summary__wrapper">
                            <table class="ty-orders-summary__table">
                                {hook name="h_rfq:totals"}
                                    {if $order_info.payment_id}
                                        <tr class="ty-orders-summary__row">
                                            <td>{__("payment_method")}:</td>
                                            <td style="width: 57%" data-ct-orders-summary="summary-payment">
                                                {hook name="h_rfq:totals_payment"}
                                                    {$order_info.payment_method.payment} {if $order_info.payment_method.description}({$order_info.payment_method.description}){/if}
                                                {/hook}
                                            </td>
                                        </tr>
                                    {/if}

                                    {* <tr class="ty-orders-summary__row">
                                        <td>{__("subtotal")}:&nbsp;</td>
                                        <td data-ct-orders-summary="summary-subtotal">{include file="common/price.tpl" value=$order_info.display_subtotal}</td>
                                    </tr> *}
                                    {* {if $order_info.display_shipping_cost|floatval}
                                        <tr class="ty-orders-summary__row">
                                            <td>{__("shipping_cost")}:&nbsp;</td>
                                            <td data-ct-orders-summary="summary-shipcost">{include file="common/price.tpl" value=$order_info.display_shipping_cost}</td>
                                        </tr>
                                    {/if} *}
   
                                    {hook name="h_rfq:order_total"}
                                        <tr class="ty-orders-summary__row">
                                            <td class="ty-orders-summary__total">{__("total")}:&nbsp;</td>
                                            <td class="ty-orders-summary__total" data-ct-orders-summary="summary-total">
                                            {if $order_info.total}
                                                {include file="common/price.tpl" value=$order_info.total}
                                            {else}
                                                --
                                            {/if}
                                            </td>
                                        </tr>
                                    {/hook}
                                {/hook}
                            </table>
                        </div>
                    </div>
                {/capture}

                <div class="ty-orders-detail__products orders-product">
                    {include file="common/group.tpl"  content=$smarty.capture.group}
                </div>
            </div>

            {include file="addons/h_rfq/views/h_rfq/components/discussion.tpl"
                object_id=$discussion.object_id
                object_type=$smarty.const.DISCUSSION_OBJECT_TYPE_RFQ
                title=__("discussion_title_order")
                quotation=$quotation 
                discussion=$discussion
                wrap=true}

            {include file="addons/h_rfq/views/h_rfq/components/vendor_quotes.tpl"
                title=__("h_rfq.vendor_quotes")
                quotation=$quotation 
                vendor_quotes=$vendor_quotes}

            {hook name="h_rfq:tabs"}
            {/hook}

        {/capture}
        {include file="common/tabsbox.tpl" top_order_actions=$smarty.capture.order_actions content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section}
    {/if}
</div>

{hook name="h_rfq:details"}
{/hook}

{* {include file="common/subheader.tpl" title=__("h_rfq.products") class="h-rfq_subheader"}

<div class="h_rfq-details_product">
    {foreach $products as $product}
        <div class="h_rfq-details_product">
            <div class="h_rfq-details_content"><span class="ty-strong">{__("h_rfq.product_name")}: </span>{$product.product_name}</div>
            <div class="h_rfq-details_content"><span class="ty-strong">{__("h_rfq.product_category")}: </span>{$product.product_category}</div>
            <div class="h_rfq-details_content"><span class="ty-strong">{__("h_rfq.selling_price")}: </span>{include file="common/price.tpl" value=$product.selling_price}</div>
            <div class="h_rfq-details_content"><span class="ty-strong">{__("h_rfq.product_qty")}: </span>{$product.quantity} {$product.quantity_unit}</div>
            <div class="h_rfq-details_content"><span class="ty-strong">{__("h_rfq.product_description")}: </span>{$product.description}</div>
        </div>
    {/foreach}
</div> *}