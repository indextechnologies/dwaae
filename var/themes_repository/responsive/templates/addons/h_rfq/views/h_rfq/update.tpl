{include file="common/subheader.tpl" title="{__('h_rfq.create_header')}"}
<form name="rfq_from" enctype="multipart/form-data" action="{""|fn_url}" method="post">

    {* <input type="hidden" class="data-implode-categories" value="{implode(',',$formatCategories)}" /> *}
    <div id="smartwizard" class="h-rfq_smartwizard">
        <ul>
            <li><a href="#step-1">{__("h_rfq.product_details_title")}<br /><small>{__("h_rfq.product_details_title_desc")}</small></a></li>
            <li><a href="#step-2">{__("h_rfq.delivery_location_title")}<br /><small>{__("h_rfq.delivery_location_title_desc")}</small></a></li>
            <li><a href="#step-3">{__("h_rfq.buyer_details_title")}<br /><small>{__("h_rfq.buyer_details_title_desc")}</small></a></li>
            <li><a href="#step-4">{__("h_rfq.add_extra_title")}<br /><small>{__("h_rfq.add_exta_data_desc")}</small></a></li>
        </ul>

        <div>
        <div id="step-1" class="">
            <div class="h-rfq_products">
                {include file="common/subheader.tpl" title="{__('h_rfq.create_products_header')}" class="h-rfq_subheader"}
                {include file="addons/h_rfq/views/h_rfq/components/product_form.tpl" sqid=0 name="quotation[product]"}
                <div class="h-rfq_add_product">
                    <a href="javascript:;" class="ty-btn ty-btn__primary" id="h-rfq_add_new_product">{__("h_rfq.add_product")}</a>
                </div>
            </div>
        </div>

        <div id="step-2" class="">
            <div class="h-rfq_delivery_locations">
                {include file="common/subheader.tpl" title="{__('h_rfq.create_delivery_location')}" class="h-rfq_subheader"}
                {include file="addons/h_rfq/views/h_rfq/components/delivery_location.tpl" sqid=0 name="quotation[delivery_location]"}
            </div>
        </div>

        <div id="step-3" class="">
            <div class="h-rfq_buyer_details">
                {include file="common/subheader.tpl" title="{__('h_rfq.buyer_details')}" class="h-rfq_subheader"}
                {include file="addons/h_rfq/views/h_rfq/components/buyer_details.tpl" name="quotation[buyer_details]"}
            </div>
        </div>

        <div id="step-4" class="">
            <div class="h-rfq_attachments">
                {include file="common/subheader.tpl" title="{__('h_rfq.attachments')}" class="h-rfq_subheader"}
                <input type="hidden" name="quotation[attachments]" value="" />
                <div id="upload-widget" class="fallback dropzone" >
                    <input type="file" name="file" multiple class="hidden"/>
                </div>
            </div>
        </div>

        </div>
    </div>

    <div class="buttons-container ty-right">
        {include file="buttons/button.tpl"
            but_name="dispatch[h_rfq.update]"
            but_text="{__("h_rfq.submit_quote")}"
            but_target_form="rfq_from"
            but_meta="ty-btn__primary"}
    </div>

</form>

{$rand=uniqid()}
{script src="js/addons/h_rfq/dropzonejs.js"}
{script src="js/addons/h_rfq/jquery.smartWizard.js?rand=`$rand`"}
{script src="js/addons/h_rfq/script.js?rand=`$rand`"}
{script src="js/addons/h_rfq/dropzone_handle.js?rand=`$rand`"}
<script>
   
</script>
