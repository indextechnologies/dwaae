{if !$sqid}
    {$sqid = rand(100000,999999)}
{/if}
{$_country = $quotation.country|default:$settings.Checkout.default_country}
{$_state = $quotation.state|default:$settings.Checkout.default_state}
{$countries=fn_get_simple_countries(true, $smarty.const.CART_LANGUAGE)}

<div class="h-rfq_delivery_location">
    <div class="ty-control-group">
        <label for="el_address_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.address")}</label>
        <input type="text" id="el_address_{$sqid}" name="{$name}[{$sqid}][address]" size="100" maxlength="200" value="{$quotation.address}" class="ty-input-text" />
    </div>
    <div class="ty-control-group">
        <label for="el_state_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.country")}</label>
        <select id="el_country_{$sqid}" class="ty-profile-field__select-country cm-country cm-location-shipping" name="{$name}[{$sqid}][country]">
            {foreach from=$countries item="country" key="code"}
            <option {if $_country == $code}selected="selected"{/if} value="{$code}">{$country}</option>
            {/foreach}
        </select>
    </div>
    <div class="ty-control-group">
        <label for="el_state_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.state")}</label>
        <select id="el_state_{$sqid}" class="ty-profile-field__select-state cm-state cm-location-shipping" name="{$name}[{$sqid}][state]">
            {if $states && $states.$_country}
                {foreach from=$states.$_country item=state}
                    <option {if $_state == $state.code}selected="selected"{/if} value="{$state.code}">{$state.state}</option>
                {/foreach}
            {/if}
        </select>
        <input  type="text" id="el_state_{$sqid}_d" name="{$name}[{$sqid}][state]" size="32" maxlength="64" value="{$_state}" disabled="disabled" class="cm-state cm-location-shipping ty-input-text hidden"/>
    </div>
    <div class="ty-control-group">
        <label for="el_city_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.city")}</label>
        <input type="text" id="el_city_{$sqid}" name="{$name}[{$sqid}][city]" size="100" maxlength="200" value="{$quotation.city}" class="ty-input-text" />
    </div>
    <div class="ty-control-group">
        <label for="el_zipcode_{$sqid}" class="ty-control-group__title">{__("h_rfq.zipcode")}</label>
        <input type="text" id="el_zipcode_{$sqid}" name="{$name}[{$sqid}][zipcode]" size="100" maxlength="200" value="{$quotation.zipcode}" class="ty-input-text" />
    </div>
</div>

<script type="text/javascript">
(function(_, $) {
    $.ceRebuildStates('init', {
        default_country: '{$settings.Checkout.default_country|escape:javascript}',
        states: {$states|json_encode nofilter}
    });
}(Tygh, Tygh.$));
</script>