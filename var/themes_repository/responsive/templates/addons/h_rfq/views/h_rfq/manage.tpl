{assign var="c_url" value=$config.current_url|fn_query_remove:"sort_by":"sort_order"}
{if $search.sort_order == "asc"}
{assign var="sort_sign" value="<i class=\"ty-icon-down-dir\"></i>"}
{else}
{assign var="sort_sign" value="<i class=\"ty-icon-up-dir\"></i>"}
{/if}
{if !$config.tweaks.disable_dhtml}
    {assign var="ajax_class" value="cm-ajax"}

{/if}

{include file="common/pagination.tpl"}

<table class="ty-table ty-orders-search">
    <thead>
        <tr>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=rfq_id&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("h_rfq.rfq_id")}</a>{if $search.sort_by == "rfq_id"}{$sort_sign nofilter}{/if}</th>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=email&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("h_rfq.email")}</a>{if $search.sort_by == "email"}{$sort_sign nofilter}{/if}</th>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=create_time&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("h_rfq.create_time")}</a>{if $search.sort_by == "create_time"}{$sort_sign nofilter}{/if}</th>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=valid_till&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("h_rfq.valid_till")}</a>{if $search.sort_by == "valid_till"}{$sort_sign nofilter}{/if}</th>
            <th><a class="{$ajax_class}" href="{"`$c_url`&sort_by=status&sort_order=`$search.sort_order_rev`"|fn_url}" data-ca-target-id="pagination_contents">{__("h_rfq.status")}</a>{if $search.sort_by == "status"}{$sort_sign nofilter}{/if}</th>
            <th></th>
        </tr>
    </thead>
    {foreach from=$quotations item="q"}
        <tr>
            <td class="ty-orders-search__item"><a href="{"h_rfq.details?rfq_id=`$q.rfq_id`"|fn_url}"><strong>#{$q.rfq_id}</strong></a></td>
            <td class="ty-orders-search__item">{$q.email}</td>
            <td class="ty-orders-search__item">
                {$q.create_time|date_format:"`$settings.Appearance.date_format`"}
            </td>
            <td class="ty-orders-search__item">{$q.valid_till|date_format:"`$settings.Appearance.date_format` `$settings.Appearance.time_format`"}</td>
            <td class="ty-orders-search__item">
            {include file="addons/h_rfq/common/status.tpl" status=$q.status display="view" name="update_order[status]"}
            </td>
            <td><a href="{"h_rfq.details?rfq_id=`$q.rfq_id`"|fn_url}">{__("h_rfq.view_details")}</a></td>
        </tr>
    {foreachelse}
        <tr class="ty-table__no-items">
            <td colspan="7"><p class="ty-no-items">{__("no_data")}</p></td>
        </tr>
    {/foreach}
</table>

{include file="common/pagination.tpl"}

{capture name="mainbox_title"}{__("h_rfq.quotations")}{/capture}