{if !$sqid}
    {$sqid = rand(100000,999999)}
{/if}

<div class="h_rfq-buyer_information">
    <div class="ty-control-group">
        <label for="el_company_{$sqid}" class="ty-control-group__title">{__("h_rfq.company")}</label>
        <input type="text" id="el_company_{$sqid}" name="{$name}[company]" size="32" value="{$quotation.company|default:$user_data.company}" class="ty-input-text" />
    </div>

    <div class="ty-control-group">
        <label for="el_firstname_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.firstname")}</label>
        <input type="text" id="el_firstname_{$sqid}" name="{$name}[firstname]" size="32" value="{$quotation.firstname|default:$user_data.firstname}" class="ty-input-text" />
    </div>
    <div class="ty-control-group">
        <label for="el_lastname_{$sqid}" class="ty-control-group__title cm-required">{__("h_rfq.lastname")}</label>
        <input type="text" id="el_lastname_{$sqid}" name="{$name}[lastname]" size="32" value="{$quotation.lastname|default:$user_data.lastname}" class="ty-input-text" />
    </div>
     <div class="ty-control-group">
        <label for="el_email_{$sqid}" class="ty-control-group__title cm-email cm-required">{__("h_rfq.email")}</label>
        {$email=$quotation.email|default:$user_data.email}
        <input type="text" id="el_email_{$sqid}" name="{$name}[email]" size="32" value="{$email}" class="ty-input-text" {if $email}disabled{/if}/>
    </div>

    <div class="ty-control-group">
        <label for="el_phone_{$sqid}" class="ty-control-group__title cm-mask-phone-label cm-required">{__("h_rfq.phone")}</label>
        <input type="text" id="el_phone_{$sqid}" name="{$name}[phone]" size="32" value="{$quotation.phone|default:$user_data.phone}" class="ty-input-text cm-mask-phone" />
    </div>

    <div class="ty-control-group">
        <label for="el_url_{$sqid}" class="ty-control-group__title">{__("h_rfq.url")}</label>
        <input type="text" id="el_url_{$sqid}" name="{$name}[url]" size="32" value="{$quotation.url|default:$user_data.url}" class="ty-input-text" />
    </div>

    <div class="ty-control-group">
        <label for="el_fax_{$sqid}" class="ty-control-group__title">{__("h_rfq.fax")}</label>
        <input type="text" id="el_fax_{$sqid}" name="{$name}[fax]" size="32" value="{$quotation.fax|default:$user_data.fax}" class="ty-input-text" />
    </div>
</div>