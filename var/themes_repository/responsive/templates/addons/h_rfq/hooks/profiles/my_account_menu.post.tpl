{if $auth.user_id}
   <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined" href="{"h_rfq.update"|fn_url}" rel="nofollow">{__("h_rfq.request_for_quotation")}</a></li>
   <li class="ty-account-info__item ty-dropdown-box__item"><a class="ty-account-info__a underlined" href="{"h_rfq.manage"|fn_url}" rel="nofollow">{__("h_rfq.my_quotations")}</a></li>
{/if}