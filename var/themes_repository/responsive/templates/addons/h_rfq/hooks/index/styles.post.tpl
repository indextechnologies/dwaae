{style src="addons/h_rfq/style.less?rand=32322323"}
{style src="addons/h_rfq/dropzone.less"}
{style src="addons/h_rfq/smartwizard/smart_wizard.css"}
{style src="addons/h_rfq/smartwizard/smart_wizard_theme_arrows.less?tessst=1133"}

<style>
    .h_cross {
        float: right;
        border: 1px solid;
        border-radius: 50%;
        padding: 3px 7px;
        background: gray;
        color: white;
        text-transform: lowercase;
        cursor: pointer;
    }

    input[disabled] {
        background: rgb(237, 237, 237);
        cursor: not-allowed!important;
        color:black!important;
    }
</style>
