{if $addons.redesk_connection.allow_order_page == "Y"}
	{include file="buttons/button.tpl" but_meta="ty-btn__text" but_role="text" but_text=__("redesk_connection_create_ticket") but_href="redesk_connection.contact?order_id=`$order_info.order_id`" but_icon="ty-orders__actions-icon ty-icon-chat"}
{/if}