<div class="mainbox-container clearfix redesk-form">
	<h1 class="mainbox-title">
		<span>{__('re_desk_contact_us')}</span>
	</h1>
	<div class="mainbox-body">
		{if !$smarty.request.result}
		<div class="cm-notification-container"></div>
		<div class="wysiwyg-content">
			{__('re_desk_you_can_send')}
			<p>&nbsp;</p>

			<div class="form-wrap form-wrap-default">
				<div class="form-wrap-l"></div>
				<div class="form-wrap-r"></div>
				<form action="" method="post" name="forms_form" enctype="multipart/form-data">
					<input type="hidden" name="fake" value="1">
					<div class="ty-control-group">
						<label for="elm_2" class="ty-control-group__title cm-required cm-email">{__('email')}</label>
						<input id="elm_2" class="input-text" size="50" type="text" name="ticket[email]" value="{if $smarty.request.ticket}{$smarty.request.ticket.email}{else}{$order_info.email}{/if}">
					</div>

					<div class="ty-control-group">
						<label for="elm_5" class="ty-control-group__title cm-required">{__('re_desk_department')}</label>
						<select name="ticket[department]" id="elm_5">
							<option value=""></option>
							{if $departments}
								{foreach from=$departments item="department"}
									<option value="{$department.id}" {if $smarty.request.ticket.department == $department.id}selected="selected"{/if}>{$department.name}</option>
								{/foreach}
							{/if}
						</select>
					</div>

					<div class="ty-control-group">
						<label for="elm_1" class="ty-control-group__title cm-required">{__('re_desk_full_name')}</label>
						<input id="elm_1" class="input-text" size="50" type="text" name="ticket[full_name]" value="{if $smarty.request.ticket}{$smarty.request.ticket.full_name}{else}{$order_info.b_firstname} {$order_info.b_lastname}{/if}">
					</div>

					<div class="ty-control-group">
						<label for="elm_6" class="ty-control-group__title cm-required">{__('subject')}</label>
						<input id="elm_6" class="input-text" size="50" type="text" name="ticket[subject]" value="{$smarty.request.ticket.subject}">
					</div>

					<div class="ty-control-group">
						<label for="elm_7" class="ty-control-group__title">{__('order_id')}</label>
						<input id="elm_7" class="input-text" size="50" type="text" name="ticket[order_id]" value="{if $smarty.request.ticket}{$smarty.request.ticket.subject}{elseif $order_info}{$order_info.order_id}{/if}">
					</div>

					<div class="ty-control-group">
						<label for="elm_3" class="ty-control-group__title cm-required">{__('body')}</label>
						<textarea id="elm_3" class="input-textarea" name="ticket[body]" cols="67" rows="10">{$smarty.request.ticket.body}</textarea>
					</div>

					{if $settings.Image_verification.use_for_re_desk == "Y"}
						{include file="common/image_verification.tpl" option="use_for_re_desk"}
					{/if}

					<div class="ty-control-group">
						{script src="js/fileuploader_scripts.js"}
						{include file="common/fileuploader.tpl" var_name="redesk_files[]" multiupload=true}
					</div>

					<div class="buttons-container">
						<span class="button-submit button-wrap-left"><span class="button-submit button-wrap-right"><input type="submit" name="dispatch[pages.send_form]" value="{__('submit')}"></span></span>
					</div>
				</form>
			</div>
		</div>
		{else}
			<strong>{__('re_desk_thank_you')}
		{/if}
	</div>
</div>